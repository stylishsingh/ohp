package com.ohp.notifications;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ohp.R;
import com.ohp.commonviews.activities.LoginActivity;
import com.ohp.commonviews.presenter.ChattingFragmentPresenter;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import org.json.JSONObject;

import static com.ohp.utils.Constants.NAVIGATE_FROM;
import static com.ohp.utils.Constants.NOTIFICATIONS;
import static com.ohp.utils.Constants.NOTIFY_DEACTIVATED_BY_ADMIN;
import static com.ohp.utils.Constants.NOTIFY_NEW_MESSAGE;


/**
 * @author Amanpal Singh.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    public static int notificationID = 0;

    /**
     * @param remoteMessage return the data sent by FCM.
     */

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            System.out.println("data " + remoteMessage.getData());
            String meta_data = remoteMessage.getData().get("meta_data");
            final JSONObject object = new JSONObject(meta_data);
            notificationID++;

            switch (object.getString(Constants.TYPE)) {
                case NOTIFY_DEACTIVATED_BY_ADMIN:
                    Utils.getInstance().logout(remoteMessage.getNotification().getBody(), this);
                    generateNotification(remoteMessage.getNotification().getBody(), new Intent(this, LoginActivity.class));
                    break;

                case NOTIFY_NEW_MESSAGE:
                    if (ChattingFragmentPresenter.isRegistered) {
                        Intent broadcastIntent = new Intent(Constants.GET_MESSAGE_LIST);
                        broadcastIntent.putExtra(NAVIGATE_FROM, NOTIFICATIONS);
                        broadcastIntent.putExtra(Constants.DESTINATION, Constants.CLICK_CHAT);
                        broadcastIntent.putExtra(Constants.MESSAGE, remoteMessage.getNotification().getBody());
                        broadcastIntent.putExtra(Constants.SEND_BY_NAME, object.getString(Constants.SEND_BY_NAME));
                        broadcastIntent.putExtra(Constants.SEND_BY_USERID, String.valueOf(object.getInt(Constants.SEND_BY_USERID)));
                        sendBroadcast(broadcastIntent);
                    } else
                        generateNotification(remoteMessage.getNotification().getBody(), NotificationIntent.getInstance().createIntent(this, object, false));
                    break;

                default:
                    generateNotification(remoteMessage.getNotification().getBody(), NotificationIntent.getInstance().createIntent(this, object, false));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void generateNotification(String messageBody, Intent intent) {

        try {
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                    .setSmallIcon(getNotificationIcon())
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setContentIntent(pendingIntent);
            if (Utils.getInstance().isEqualLollipop()) {
                notification.setFullScreenIntent(pendingIntent, false);
            }

            System.out.println("MyFirebaseMessagingService.generateNotification----" + notificationID);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(notificationID, notification.build());

//            notificationManager.cancel(notificationID);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // function to get the notification icon based on version lollipop or not
    private int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return whiteIcon ? R.drawable.ic_launcher_white : R.mipmap.ic_launcher;
    }

}
