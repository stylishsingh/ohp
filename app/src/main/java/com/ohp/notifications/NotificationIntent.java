package com.ohp.notifications;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import org.json.JSONObject;

import static com.ohp.utils.Constants.BG_APP;
import static com.ohp.utils.Constants.NAVIGATE_FROM;
import static com.ohp.utils.Constants.NOTIFICATIONS;
import static com.ohp.utils.Constants.NOTIFY_APPLIANCE_EXPIRE_NOTIFICATION;
import static com.ohp.utils.Constants.NOTIFY_APPLIANCE_EXTENDED_NOTIFICATION;
import static com.ohp.utils.Constants.NOTIFY_BID_ACCEPTED;
import static com.ohp.utils.Constants.NOTIFY_BID_ADDED;
import static com.ohp.utils.Constants.NOTIFY_BID_REJECTED;
import static com.ohp.utils.Constants.NOTIFY_CANCEL_PROJECT;
import static com.ohp.utils.Constants.NOTIFY_COMPLETE_TASK_TO_MEMBER;
import static com.ohp.utils.Constants.NOTIFY_COMPLETE_TASK_TO_OWNER;
import static com.ohp.utils.Constants.NOTIFY_COMPLETE_TASK_TO_USER;
import static com.ohp.utils.Constants.NOTIFY_END_PROJECT;
import static com.ohp.utils.Constants.NOTIFY_EVENT_REMINDER_TO_OWNER;
import static com.ohp.utils.Constants.NOTIFY_EVENT_REMINDER_TO_PROVIDER;
import static com.ohp.utils.Constants.NOTIFY_EVENT_REMINDER_TO_USER;
import static com.ohp.utils.Constants.NOTIFY_EVENT_TO_OWNER;
import static com.ohp.utils.Constants.NOTIFY_EVENT_TO_PROVIDER;
import static com.ohp.utils.Constants.NOTIFY_MARK_AS_COMPLETE_TASK_USER;
import static com.ohp.utils.Constants.NOTIFY_MARK_PROJECT;
import static com.ohp.utils.Constants.NOTIFY_NEW_MESSAGE;
import static com.ohp.utils.Constants.NOTIFY_NEW_PROJECT_ADDED;
import static com.ohp.utils.Constants.NOTIFY_REJECT_COMPLETED_PROJECT;
import static com.ohp.utils.Constants.NOTIFY_REOPEN_TASK_TO_MEMBER;
import static com.ohp.utils.Constants.NOTIFY_REOPEN_TASK_TO_USER;
import static com.ohp.utils.Constants.NOTIFY_TASK_REMINDER_TO_MEMBER;
import static com.ohp.utils.Constants.NOTIFY_TASK_REMINDER_TO_OWNER;
import static com.ohp.utils.Constants.NOTIFY_TASK_REMINDER_TO_USER;
import static com.ohp.utils.Constants.NOTIFY_TASK_TO_OWNER;
import static com.ohp.utils.Constants.NOTIFY_TASK_TO_PROVIDER;
import static com.ohp.utils.Constants.NOTIFY_TEAM_MEMBER_INVITED;

/**
 * @author Amanpal Singh.
 */

public class NotificationIntent {

    private static NotificationIntent instance = null;

    private NotificationIntent() {

    }

    /**
     * @return instance of this class
     */

    public static NotificationIntent getInstance() {
        return instance == null ? new NotificationIntent() : instance;
    }

    /**
     * @param context  A Context of the application package implementing this class.
     * @param object   getting the data from notification
     * @param appState is true if app is in background else false
     * @return intent which will be executed after tap on notification.
     */

    public Intent createIntent(Context context, JSONObject object, boolean appState) {
        Intent intent = new Intent(context, HolderActivity.class);
        Intent broadcastIntent, listIntent;

        try {
            switch (object.getString(Constants.TYPE)) {
                // This case will occur when property owner will add project, then push notification goes to all the service providers.
                case NOTIFY_NEW_PROJECT_ADDED:
                    intent.putExtra(Constants.DESTINATION, Constants.BROWSE_PROJECTS);
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(object.getInt(Constants.PROJECT_ID)));
                    break;
                // This case will occur when property owner will reject bid, then push notification goes to the service provider who bid for this project.
                case NOTIFY_BID_REJECTED:
                    broadcastIntent = new Intent(Constants.PROJECT_DETAIL);
                    broadcastIntent.putExtra(NAVIGATE_FROM, NOTIFICATIONS);
                    broadcastIntent.putExtra(Constants.PROJECT_ID, String.valueOf(object.getInt(Constants.PROJECT_ID)));
                    context.sendBroadcast(broadcastIntent);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_BID);
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(object.getInt(Constants.PROJECT_ID)));
                    intent.putExtra(Constants.BID_ID, String.valueOf(object.getInt(Constants.BID_ID)));
                    break;
                case NOTIFY_TEAM_MEMBER_INVITED:
                    // to be done later
                    break;
                // This case will occur when service provider will add event for awarded project, then push notification goes to property owner.
                case NOTIFY_EVENT_TO_OWNER:
                    // This case will occur when date of event created by service provider will be nearby then push notification goes to property owner.
                    // This case will occur when service provider will add reminder for event for awarded project, then push notification goes to property owner.
                case NOTIFY_EVENT_REMINDER_TO_OWNER:
                    // This case will occur when date of event created by service provider will be nearby then push notification goes to team member.
                    // This case will occur when service provider will add reminder for event, then push notification goes to team member.
                    final String[] parseDateTime, parseDate;
                    parseDateTime = object.getString("event_time_from").split(" ");
                    parseDate = parseDateTime[0].split("-");
                    broadcastIntent = new Intent(Constants.GET_EVENT_DETAILS);
                    broadcastIntent.putExtra("year", Integer.parseInt(parseDate[0]));
                    broadcastIntent.putExtra("month", Integer.parseInt(parseDate[1]));
                    broadcastIntent.putExtra("day", Integer.parseInt(parseDate[2]));
                    broadcastIntent.putExtra(NAVIGATE_FROM, NOTIFICATIONS);
                    broadcastIntent.putExtra(Constants.EVENT_ID, String.valueOf(object.getInt(Constants.EVENT_ID)));
                    context.sendBroadcast(broadcastIntent);
                    intent.putExtra(Constants.OCCURRENCE, false);
                    if (object.getString(Constants.EVENT_REPEAT).isEmpty())
                        intent.putExtra(Constants.EVENT_REPEAT_EMPTY, true);
                    else
                        intent.putExtra(Constants.EVENT_REPEAT_EMPTY, false);
                    intent.putExtra(Constants.EVENT_ID, String.valueOf(object.getInt(Constants.EVENT_ID)));
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_OWNER_VIEW_EVENT);
                    intent.putExtra(Constants.EVENT_TYPE, object.getString(Constants.EVENT_STATUS));
                    break;
                // This case will occur when property owner will add event for awarded project which assigned to service provider, then push notification goes to service provider.
                case NOTIFY_EVENT_TO_PROVIDER:
                    // This case will occur when date of event created by service provider will be nearby then push notification goes to service provider.
                case NOTIFY_EVENT_REMINDER_TO_USER:

                case NOTIFY_EVENT_REMINDER_TO_PROVIDER:
                    broadcastIntent = new Intent(Constants.GET_EVENT_DETAILS);
                    broadcastIntent.putExtra(NAVIGATE_FROM, NOTIFICATIONS);
                    broadcastIntent.putExtra(Constants.EVENT_ID, String.valueOf(object.getInt(Constants.EVENT_ID)));
                    context.sendBroadcast(broadcastIntent);
                    intent.putExtra(Constants.OCCURRENCE, false);
                    if (object.getString(Constants.EVENT_REPEAT).isEmpty())
                        intent.putExtra(Constants.EVENT_REPEAT_EMPTY, true);
                    else
                        intent.putExtra(Constants.EVENT_REPEAT_EMPTY, false);
                    intent.putExtra(Constants.EVENT_ID, String.valueOf(object.getInt(Constants.EVENT_ID)));
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_SP_VIEW_EVENT);
                    intent.putExtra(Constants.EVENT_TYPE, object.getString(Constants.EVENT_STATUS));
                    break;

                // This case will occur when service provider will add task for awarded project, then push notification goes to property owner.
                case NOTIFY_TASK_TO_OWNER:
                    // This case will occur when service provider will add task for awarded project as well as my projects, then push notification goes to service provider.
                case NOTIFY_TASK_TO_PROVIDER:
                    // This case will occur when date of task created by service provider will be nearby then push notification goes to service provider.
                case NOTIFY_TASK_REMINDER_TO_USER:
                    // This case will occur when date of task created by service provider will be nearby then push notification goes to property owner.
                    // This case will occur when service provider will add reminder for task for awarded project, then push notification goes to property owner.
                case NOTIFY_TASK_REMINDER_TO_OWNER:
                    // This case will occur when date of task created by service provider will be nearby then push notification goes to team member.
                    // This case will occur when service provider will add reminder for task, then push notification goes to team member.
                case NOTIFY_TASK_REMINDER_TO_MEMBER:
                    // This case will occur when team member will complete the task, then push notification goes to service provider.
                case NOTIFY_MARK_AS_COMPLETE_TASK_USER:
                    // This case will occur when property owner will reopen the task, then push notification goes to service provider.
                case NOTIFY_REOPEN_TASK_TO_USER:
                    // This case will occur when property owner or service provider will reopen the task, then push notification goes to team member.
                case NOTIFY_REOPEN_TASK_TO_MEMBER:
                    // This case will occur when task has been done by service provider, then push notification goes to property owner.
                case NOTIFY_COMPLETE_TASK_TO_OWNER:
                    // This case will occur when task has been done by team member or property owner, then push notification goes to service provider.
                case NOTIFY_COMPLETE_TASK_TO_USER:
                    // This case will occur when task has been done by service provider or property owner, then push notification goes to team member.
                case NOTIFY_COMPLETE_TASK_TO_MEMBER:
                    listIntent = new Intent(Constants.TASK_LIST_UPDATE);
                    context.sendBroadcast(listIntent);
                    broadcastIntent = new Intent(Constants.GET_TASK_DETAIL);
                    broadcastIntent.putExtra(NAVIGATE_FROM, NOTIFICATIONS);
                    broadcastIntent.putExtra(Constants.TASK_ID, String.valueOf(object.getInt(Constants.TASK_ID)));
                    context.sendBroadcast(broadcastIntent);
                    intent.putExtra(Constants.OCCURRENCE, false);
                    if (object.getString(Constants.TASK_REPEAT).isEmpty())
                        intent.putExtra(Constants.TASK_REPEAT_EMPTY, true);
                    else
                        intent.putExtra(Constants.TASK_REPEAT_EMPTY, false);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_TASK);
                    intent.putExtra(Constants.TASK_ID, String.valueOf(object.getInt(Constants.TASK_ID)));
                    intent.putExtra(Constants.TASK_TYPE, object.getString(Constants.TASK_TYPE));
                    break;
                // This case will occur when service provider will add bid for project, then push notification goes to property owner who created the project.
                case NOTIFY_BID_ADDED:
                    // This case will occur when service provider will mark complete the project, then push notification goes to property owner.
                case NOTIFY_MARK_PROJECT:
                    broadcastIntent = new Intent(Constants.PROJECT_DETAIL);
                    broadcastIntent.putExtra(NAVIGATE_FROM, NOTIFICATIONS);
                    broadcastIntent.putExtra(Constants.PROJECT_ID, object.getString(Constants.PROJECT_ID));
                    context.sendBroadcast(broadcastIntent);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_PROJECT);
                    intent.putExtra(Constants.PROJECT_STATUS, String.valueOf(object.getInt(Constants.PROJECT_STATUS)));
                    intent.putExtra(Constants.PROJECT_ID, object.getString(Constants.PROJECT_ID));
                    break;
                // This case will occur when property owner will chat with service provider or vice-versa.
                case NOTIFY_NEW_MESSAGE:
                    broadcastIntent = new Intent(Constants.GET_MESSAGE_LIST);
                    broadcastIntent.putExtra(NAVIGATE_FROM, NOTIFICATIONS);
                    broadcastIntent.putExtra(Constants.SEND_BY_USERID, String.valueOf(object.getInt(Constants.SEND_BY_USERID)));
                    context.sendBroadcast(broadcastIntent);


                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_CHAT);
                    intent.putExtra(Constants.SEND_BY_NAME, object.getString(Constants.SEND_BY_NAME));
                    intent.putExtra(Constants.SEND_BY_USERID, String.valueOf(object.getInt(Constants.SEND_BY_USERID)));


                    break;
                // This case will occur when appliance expiration date will be nearby, then push notification goes to property owner.
                case NOTIFY_APPLIANCE_EXPIRE_NOTIFICATION:
                    // This case will occur when appliance warranty extended has been extended, then push notification goes to property owner.
                case NOTIFY_APPLIANCE_EXTENDED_NOTIFICATION:
                    broadcastIntent = new Intent(Constants.GET_APPLIANCE_DETAIL);
                    broadcastIntent.putExtra(NAVIGATE_FROM, NOTIFICATIONS);
                    broadcastIntent.putExtra(Constants.APPLIANCE_ID, object.getString(Constants.APPLIANCE_ID));
                    context.sendBroadcast(broadcastIntent);
                    intent.putExtra(Constants.PROPERTY_ID, String.valueOf(object.getInt(Constants.PROPERTY_ID)));
                    intent.putExtra(Constants.APPLIANCE_ID, String.valueOf(object.getInt(Constants.APPLIANCE_ID)));
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_APPLIANCE);
                    break;
                // This case will occur when property owner will accept bid, then push notification goes to the service provider who bid for this project.
                case NOTIFY_BID_ACCEPTED:
                    // This case will occur when property owner will end the project as well as accept the mark complete the project, then push notification goes to service provider
                case NOTIFY_END_PROJECT:
                    // This case will occur when property owner will cancel the project, then push notification goes to service provider
                case NOTIFY_CANCEL_PROJECT:
                    // This case will occur when property owner will reject the mark completed project, then push notification goes to service provider
                case NOTIFY_REJECT_COMPLETED_PROJECT:
                    listIntent = new Intent(Constants.GET_PROJECTS_SP);
                    context.sendBroadcast(listIntent);
                    broadcastIntent = new Intent(Constants.PROJECT_DETAIL);
                    broadcastIntent.putExtra(NAVIGATE_FROM, NOTIFICATIONS);
                    broadcastIntent.putExtra(Constants.PROJECT_ID, String.valueOf(object.getInt(Constants.PROJECT_ID)));
                    context.sendBroadcast(broadcastIntent);
                    intent.putExtra(Constants.DESTINATION, Constants.PROJECT_DETAIL_SERVICE_PROVIDER);
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(object.getInt(Constants.PROJECT_ID)));
                    break;

            }

            intent.putExtra(NAVIGATE_FROM, NOTIFICATIONS);
            intent.putExtra(BG_APP, appState);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return intent;
    }

    //It is same as we did in earlier posts
    public void generateNotification(String messageBody, Intent intent, Context context, int notificationID) {

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(context)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setSmallIcon(getNotificationIcon())
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setTicker(messageBody)
                .setSound(defaultSoundUri)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setContentIntent(pendingIntent);
        if (Utils.getInstance().isEqualLollipop()) {
            notification.setFullScreenIntent(pendingIntent, false);
        }

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationID, notification.build());
    }

    // function to get the notification icon based on version lollipop or not
    private int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return whiteIcon ? R.mipmap.ic_launcher : R.drawable.ic_launcher_white;
    }


}
