/*
 *
 *
 *  * Copyright © 2016, Mobilyte Inc. and/or its affiliates. All rights reserved.
 *  *
 *  * Redistribution and use in source and binary forms, with or without
 *  * modification, are permitted provided that the following conditions are met:
 *  *
 *  * - Redistributions of source code must retain the above copyright
 *  *    notice, this list of conditions and the following disclaimer.
 *  *
 *  * - Redistributions in binary form must reproduce the above copyright
 *  * notice, this list of conditions and the following disclaimer in the
 *  * documentation and/or other materials provided with the distribution.
 *
 * /
 */
package com.ohp.api;

import com.ohp.enums.ApiName;
import com.ohp.utils.Constants;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Anuj Sharma.
 */
public class APIHandler implements APIResponseInterface {
    private OnCallableResponse listener;


    public APIHandler(OnCallableResponse listener) {
        this.listener = listener;
    }

    public void checkLogin(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().checkLogin(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void updateRole(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().updateRole(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void register(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().register(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void sendVerificationCode(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().sendVerificationCode(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void confirmVerificationCode(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().confirmVerificationCode(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void resetPassword(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().resetPassword(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getUserProfile(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getUserProfile(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void saveUserProfile(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().saveUserProfile(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void saveUserProfileWithPic(Map<String, String> param, String profile_path, ApiName apiName) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), new File(profile_path));
        MultipartBody.Part body = MultipartBody.Part.createFormData("profile_avatar", "user_profile.jpg", requestBody);

        HashMap<String, RequestBody> bodyParam = new HashMap<>();
        for (Map.Entry<String, String> obj : param.entrySet()) {
            bodyParam.put(obj.getKey(), RequestBody.create(MediaType.parse("multipart/form-data"), obj.getValue()));
        }

        Call call = RetrofitClient.getRetrofitClient().saveUserProfileWithPic(bodyParam, body, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getResources(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getResources(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getTeamMember(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getTeamMember(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getResourcesJobAndExpertise(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getResourcesJobTypeExpertise(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getProviderList(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getProviderSp(param, Constants.KEY);
        callable(listener, call, apiName);
    }


    public void getPropertiesList(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getPropertiesList(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getApplianceList(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getApplianceList(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void deleteAppliance(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().deleteAppliance(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void addEditPropertiesWithPic(Map<String, String> param, String profile_path, ApiName apiName) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), new File(profile_path));
        MultipartBody.Part body = MultipartBody.Part.createFormData("property_avatar", "property_image.jpg", requestBody);

        HashMap<String, RequestBody> bodyParam = new HashMap<>();
        for (Map.Entry<String, String> obj : param.entrySet()) {
            bodyParam.put(obj.getKey(), RequestBody.create(MediaType.parse("multipart/form-data"), obj.getValue()));
        }

        Call call = RetrofitClient.getRetrofitClient().addEditPropertiesWithPic(bodyParam, body, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void addEditProperties(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().addEditProperties(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void addTeamMember(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().addTeamMember(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void addEditTeamMember(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().addEditTeamMember(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void addEditTask(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().addEditTask(param, Constants.KEY);
        callable(listener, call, apiName);
    }


    public void addEdit_Project(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().addEditProject(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void AddEditContact(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().addEditContact(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void addEdit_Project_Service_Provider(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().addEditProject_Service_Provider(param, Constants.KEY);
        callable(listener, call, apiName);
    }


    public void getProjectDetail(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().projectDetail(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getProjectDetailServiceProvider(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().projectDetailServiceProvider(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getAddEditBidServiceProvider(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().addeditbidServiceProvider(param, Constants.KEY);
        callable(listener, call, apiName);
    }


    public void getAllNearestProjects(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().getNearestProjects(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void deleteProject(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().deleteProject(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void deleteTask(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().deleteTask(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void deleteContact(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().deleteContact(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void deleteMyBid(Map<String, String> param, ApiName apiName) {

        Call call = RetrofitClient.getRetrofitClient().deleteBid(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void deleteProperty(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().deleteProperty(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getPropertyDetails(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getPropertyDetail(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getProperty(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getProperty(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getAllProjects(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getProjects(param, Constants.KEY);
        callable(listener, call, apiName);
    }


    public void getAllProjectsServiceProvider(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getServiceProviderProjects(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getContactList(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getContactList(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void changePassword(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().ChangePassword(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getTaskListing(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getTaskListing(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getChatUserList(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getChatUserList(param, Constants.KEY);
        callable(listener, call, apiName);
    }


    public void getMessages(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getMessages(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void sendMessages(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().sendMessages(param, Constants.KEY);
        callable(listener, call, apiName);
    }


    public void getAppliance(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getAppliance(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void addEditAppliance(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().addEditAppliance(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getApplianceDetail(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getApplianceDetail(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getDocumentList(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getDocumentList(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void deleteDocument(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().deleteDocument(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getDocumentDetail(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getDocumentDetail(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void saveDocument(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().saveDocument(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getProjectListing(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getProjectListing(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getTaskDetail(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getTaskDetail(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getEventDetails(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getEventDetails(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getEventsList(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getEventsList(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void addEditEvents(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().addEditEvents(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void deleteEvent(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().deleteEvent(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void serviceProviderBids(Map<String, String> params, ApiName api) {
        Call call = RetrofitClient.getRetrofitClient().getMyBid(params, Constants.KEY);
        callable(listener, call, api);
    }

    /*************Service Provider Apis************/
    public void getSpProfile(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getSpProfile(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void saveSpProfile(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().saveSpProfile(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void browseServiceProvider(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().browseServiceProvider(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void bidAcceptReject(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().bidAcceptReject(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getTeamMembers(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getTeamMembers(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void saveSpProfileWithPic(Map<String, String> param, String profile_path, ApiName apiName) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), new File(profile_path));
        MultipartBody.Part body = MultipartBody.Part.createFormData("profile_avatar", "user_profile.jpg", requestBody);

        HashMap<String, RequestBody> bodyParam = new HashMap<>();
        for (Map.Entry<String, String> obj : param.entrySet()) {
            bodyParam.put(obj.getKey(), RequestBody.create(MediaType.parse("multipart/form-data"), obj.getValue()));
        }

        Call call = RetrofitClient.getRetrofitClient().saveSpProfileWithPic(bodyParam, body, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void deleteTeamMember(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().deleteTeamMember(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void endProject(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().endProject(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void cancelProject(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().cancelProject(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void getProjectBidsProviders(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().getProjectBidsProviders(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void markCompletedProject(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().markCompletedProject(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void markNotCompletedProject(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().markNotCompletedProject(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void markCompletedTask(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().markCompletedTask(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void reviews(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().reviews(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void completeTask(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().completeTask(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    public void reopenTask(Map<String, String> param, ApiName apiName) {
        Call call = RetrofitClient.getRetrofitClient().reopenTask(param, Constants.KEY);
        callable(listener, call, apiName);
    }

    @Override
    public <T> void callable(OnCallableResponse onCallableResponse, Call<T> call, final ApiName api) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                listener.onSuccess(response, api);
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {

                listener.onFailure(t, api);
            }
        });
    }


}
