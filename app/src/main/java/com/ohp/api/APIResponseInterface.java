/*
 *
 *
 *  * Copyright © 2016, Mobilyte Inc. and/or its affiliates. All rights reserved.
 *  *
 *  * Redistribution and use in source and binary forms, with or without
 *  * modification, are permitted provided that the following conditions are met:
 *  *
 *  * - Redistributions of source code must retain the above copyright
 *  *    notice, this list of conditions and the following disclaimer.
 *  *
 *  * - Redistributions in binary form must reproduce the above copyright
 *  * notice, this list of conditions and the following disclaimer in the
 *  * documentation and/or other materials provided with the distribution.
 *
 * /
 */
package com.ohp.api;

import com.library.mvp.IBaseInterActor;
import com.ohp.enums.ApiName;

import retrofit2.Call;
import retrofit2.Response;

/**
 * @author Amanpal Singh.
 */
public interface APIResponseInterface extends IBaseInterActor {
    <T> void callable(OnCallableResponse onCallableResponse, Call<T> call, ApiName api);

    interface OnCallableResponse {
        void onSuccess(Response response, ApiName api);

        void onFailure(Throwable t, ApiName api);
    }

}
