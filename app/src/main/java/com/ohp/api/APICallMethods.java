/*
 *
 *
 *  * Copyright © 2016, Mobilyte Inc. and/or its affiliates. All rights reserved.
 *  *
 *  * Redistribution and use in source and binary forms, with or without
 *  * modification, are permitted provided that the following conditions are met:
 *  *
 *  * - Redistributions of source code must retain the above copyright
 *  *    notice, this list of conditions and the following disclaimer.
 *  *
 *  * - Redistributions in binary form must reproduce the above copyright
 *  * notice, this list of conditions and the following disclaimer in the
 *  * documentation and/or other materials provided with the distribution.
 *
 * /
 */
package com.ohp.api;


import com.ohp.commonviews.beans.AddEditModel;
import com.ohp.commonviews.beans.AddProjectModelResponse;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.beans.GetChatListResponse;
import com.ohp.commonviews.beans.GetMessagesResponse;
import com.ohp.commonviews.beans.GetPropertyResponse;
import com.ohp.commonviews.beans.JobTypeAndExpertise;
import com.ohp.commonviews.beans.LoginResponse;
import com.ohp.commonviews.beans.ProjectDetailResponse;
import com.ohp.commonviews.beans.RegisterResponse;
import com.ohp.commonviews.beans.ResourceBean;
import com.ohp.commonviews.beans.ReviewModel;
import com.ohp.commonviews.beans.ServiceProvidersModel;
import com.ohp.ownerviews.beans.AllContactListResponse;
import com.ohp.ownerviews.beans.ApplianceDetailModel;
import com.ohp.ownerviews.beans.ApplianceListModel;
import com.ohp.ownerviews.beans.DocumentDetailModel;
import com.ohp.ownerviews.beans.DocumentListModel;
import com.ohp.ownerviews.beans.EventDetailModel;
import com.ohp.ownerviews.beans.EventListModel;
import com.ohp.ownerviews.beans.OwnerAllProjectsModel;
import com.ohp.ownerviews.beans.ProfileResponse;
import com.ohp.ownerviews.beans.PropertiesListModel;
import com.ohp.ownerviews.beans.PropertyDetailsModel;
import com.ohp.serviceproviderviews.beans.AddEditBidResponse;
import com.ohp.serviceproviderviews.beans.AddEditProjectSpResponse;
import com.ohp.serviceproviderviews.beans.AddEditTaskResponse;
import com.ohp.serviceproviderviews.beans.GetAllProjectsServiceProviderResponse;
import com.ohp.serviceproviderviews.beans.GetNearestProjectsResponse;
import com.ohp.serviceproviderviews.beans.GetTaskDetailResponse;
import com.ohp.serviceproviderviews.beans.GetTaskListingResponse;
import com.ohp.serviceproviderviews.beans.GetTeamMemberResponse;
import com.ohp.serviceproviderviews.beans.MessageStatusResponse;
import com.ohp.serviceproviderviews.beans.MyBidsResponse;
import com.ohp.serviceproviderviews.beans.ProvidersListResponse;
import com.ohp.serviceproviderviews.beans.SpProfileModel;
import com.ohp.serviceproviderviews.beans.TeamMembersModel;
import com.ohp.utils.Constants;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by Mobilyte on 2/17/2016.
 */
public interface APICallMethods {

    @FormUrlEncoded
    @POST(Constants.LOGIN_API)
    Call<LoginResponse> checkLogin(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.UPDATE_ROLE_API)
    Call<GenericBean> updateRole(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.REGISTER_API)
    Call<RegisterResponse> register(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.SEND_VERIFICATION_CODE)
    Call<LoginResponse> sendVerificationCode(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.CONFIRM_VERIFICATION_CODE)
    Call<LoginResponse> confirmVerificationCode(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.RESET_PWD)
    Call<LoginResponse> resetPassword(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_USER_PROFILE)
    Call<ProfileResponse> getUserProfile(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.SAVE_USER_PROFILE)
    Call<ProfileResponse> saveUserProfile(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @Multipart
    @POST(Constants.SAVE_USER_PROFILE)
    Call<ProfileResponse> saveUserProfileWithPic(@PartMap Map<String, RequestBody> options, @Part MultipartBody.Part profile_pic, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_PROPERTIES_LIST)
    Call<PropertiesListModel> getPropertiesList(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_APPLIANCE_LIST)
    Call<ApplianceListModel> getApplianceList(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.DELETE_PROPERTY)
    Call<GenericBean> deleteProperty(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_PROPERTY_DETAIL)
    Call<PropertyDetailsModel> getPropertyDetail(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @Multipart
    @POST(Constants.ADD_EDIT_PROPERTY)
    Call<GenericBean> addEditPropertiesWithPic(@PartMap Map<String, RequestBody> options, @Part MultipartBody.Part property_image, @Header("key") String accessToken);


    @FormUrlEncoded
    @POST(Constants.ADD_EDIT_PROPERTY)
    Call<GenericBean> addEditProperties(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.ADD_TEAM_MEMBER)
    Call<GenericBean> addTeamMember(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.ADD_EDIT_TEAM_MEMBER)
    Call<GenericBean> addEditTeamMember(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.ADD_EDIT_TASK_API)
    Call<AddEditTaskResponse> addEditTask(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.ADD_EDIT_PROJECT)
    Call<AddProjectModelResponse> addEditProject(@FieldMap Map<String, String> options, @Header("key") String accessToken);


    @FormUrlEncoded
    @POST(Constants.ADD_EDIT_CONTACT)
    Call<GenericBean> addEditContact(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.ADD_EDIT_PROJECT_SP)
    Call<AddEditProjectSpResponse> addEditProject_Service_Provider(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.PROJECT_DETAIL)
    Call<ProjectDetailResponse> projectDetail(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.PROJECT_DETAIL)
    Call<ProjectDetailResponse> projectDetailServiceProvider(@FieldMap Map<String, String> options, @Header("key") String accessToken);


    @FormUrlEncoded
    @POST(Constants.ADD_EDIT_BID)
    Call<AddEditBidResponse> addeditbidServiceProvider(@FieldMap Map<String, String> options, @Header("key") String accessToken);


    @FormUrlEncoded
    @POST(Constants.DELETE_PROJECTS)
    Call<GenericBean> deleteProject(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.DELETE_TASK)
    Call<GenericBean> deleteTask(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.DELETE_CONTACT)
    Call<GenericBean> deleteContact(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.DELETE_BID)
    Call<GenericBean> deleteBid(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_NEARES_PROJECTS)
    Call<GetNearestProjectsResponse> getNearestProjects(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_RESOURCES)
    Call<JobTypeAndExpertise> getResourcesJobTypeExpertise(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_PROVIDER_LIST)
    Call<ProvidersListResponse> getProviderSp(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_TEAM_MEMBER)
    Call<GetTeamMemberResponse> getTeamMember(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_RESOURCES)
    Call<ResourceBean> getResources(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_PROPERTY)
    Call<GetPropertyResponse> getProperty(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_PROJECTS)
    Call<OwnerAllProjectsModel> getProjects(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_PROJECTS)
    Call<GetAllProjectsServiceProviderResponse> getServiceProviderProjects(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_CONTACT_LIST)
    Call<AllContactListResponse> getContactList(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.CHANGE_PASSWORD)
    Call<GenericBean> ChangePassword(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_TASK_LIST)
    Call<GetTaskListingResponse> getTaskListing(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_CHAT_USERS)
    Call<GetChatListResponse> getChatUserList(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_MESSAGE_LIST)
    Call<GetMessagesResponse> getMessages(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.SEND_MESSAGE)
    Call<MessageStatusResponse> sendMessages(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_APPLIANCE)
    Call<GetPropertyResponse> getAppliance(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.ADD_EDIT_APPLIANCE)
    Call<AddEditModel> addEditAppliance(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @Multipart
    @POST(Constants.UPLOAD_APPLIANCE_IMAGE)
    Call<GenericBean> uploadApplianceImages(@PartMap Map<String, RequestBody> options, @Part MultipartBody.Part[] images, @Header("key") String accessToken);

    @Multipart
    @POST(Constants.UPLOAD_DOCUMENT_IMAGE)
    Call<GenericBean> uploadDocumentImages(@PartMap Map<String, RequestBody> options, @Part MultipartBody.Part[] images, @Header("key") String accessToken);

    @Multipart
    @POST(Constants.UPLOAD_PROJECT_IMAGE)
    Call<GenericBean> uploadProjectImages(@PartMap Map<String, RequestBody> options, @Part MultipartBody.Part[] images, @Header("key") String accessToken);

    @Multipart
    @POST(Constants.UPLOAD_TASK_IMAGE)
    Call<GenericBean> uploadTaskImages(@PartMap Map<String, RequestBody> options, @Part MultipartBody.Part[] images, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_APPLIANCE_DETAIL)
    Call<ApplianceDetailModel> getApplianceDetail(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.DELETE_APPLIANCE)
    Call<GenericBean> deleteAppliance(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_DOCUMENTS_LIST)
    Call<DocumentListModel> getDocumentList(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_DOCUMENTS_DETAIL)
    Call<DocumentDetailModel> getDocumentDetail(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.DELETE_DOCUMENTS)
    Call<GenericBean> deleteDocument(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.ADD_EDIT_DOCUMENT)
    Call<AddEditModel> saveDocument(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_PROJECTS_LISTING)
    Call<GetPropertyResponse> getProjectListing(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_TASK_DETAIL)
    Call<GetTaskDetailResponse> getTaskDetail(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_EVENT_DETAILS)
    Call<EventDetailModel> getEventDetails(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_EVENTS)
    Call<EventListModel> getEventsList(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.ADD_EDIT_EVENTS)
    Call<AddEditModel> addEditEvents(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.DELETE_EVENT)
    Call<GenericBean> deleteEvent(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.MY_BIDS)
    Call<MyBidsResponse> getMyBid(@FieldMap Map<String, String> options, @Header("Key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_SP_PROFILE)
    Call<SpProfileModel> getSpProfile(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.SAVE_SP_PROFILE)
    Call<SpProfileModel> saveSpProfile(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @Multipart
    @POST(Constants.SAVE_SP_PROFILE)
    Call<SpProfileModel> saveSpProfileWithPic(@PartMap Map<String, RequestBody> options, @Part MultipartBody.Part profile_pic, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.BROWSE_SERVICE_PROVIDER)
    Call<ServiceProvidersModel> browseServiceProvider(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.BID_ACCEPT_REJECT)
    Call<GenericBean> bidAcceptReject(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_TEAM_MEMBERS)
    Call<TeamMembersModel> getTeamMembers(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.DELETE_TEAM_MEMBER)
    Call<GenericBean> deleteTeamMember(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.END_PROJECT)
    Call<GenericBean> endProject(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.CANCEL_PROJECT)
    Call<GenericBean> cancelProject(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_PROJECT_BIDS_PROVIDERS)
    Call<GetPropertyResponse> getProjectBidsProviders(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.MARK_COMPLETED_PROJECT)
    Call<GenericBean> markCompletedProject(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.MARK_NOT_COMPLETED_PROJECT)
    Call<GenericBean> markNotCompletedProject(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.MARK_COMPLETED_TASK)
    Call<GenericBean> markCompletedTask(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_USER_REVIEWS)
    Call<ReviewModel> reviews(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.COMPLETE_TASK)
    Call<GenericBean> completeTask(@FieldMap Map<String, String> options, @Header("key") String accessToken);

    @FormUrlEncoded
    @POST(Constants.REOPEN_TASK)
    Call<GenericBean> reopenTask(@FieldMap Map<String, String> options, @Header("key") String accessToken);

}
