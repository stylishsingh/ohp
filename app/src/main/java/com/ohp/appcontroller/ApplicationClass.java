package com.ohp.appcontroller;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.ohp.notifications.MyFirebaseMessagingService;
import com.squareup.leakcanary.LeakCanary;

/**
 * Created by Anuj Sharma on 2/28/2017.
 */

public class ApplicationClass extends MultiDexApplication {
    private static Context context;

    public static synchronized Context getGlobalContext() {
        return ApplicationClass.context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        LeakCanary.install(this);
        if (ApplicationClass.context == null) {
            ApplicationClass.context = getApplicationContext();
        }
        FacebookSdk.sdkInitialize(getApplicationContext()); //Initialize Facebook SDK
        AppEventsLogger.activateApp(this);
        MyFirebaseMessagingService.notificationID = 0;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
