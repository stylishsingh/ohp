package com.ohp.dataHolder;

import com.ohp.commonviews.beans.ThumbnailBean;
import com.ohp.service.ImagesResultReceiver;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class DataHolder {

    private static DataHolder instance = null;
    private static String distance = "", rating = "", licensed = "0", expertise = "";
    private List<ThumbnailBean> imagesList = new ArrayList<>();
    private List<ThumbnailBean> filesList = new ArrayList<>();
    private ImagesResultReceiver receiver;

    private DataHolder() {

    }

    public synchronized static DataHolder getInstance() {
        return instance == null ? new DataHolder() : instance;
    }

    public synchronized static void clearInstance() {
        instance = null;
    }

    public List<ThumbnailBean> getImagesList() {
        return imagesList;
    }

    public void setImagesList(List<ThumbnailBean> imagesList) {
        this.imagesList = imagesList;
    }

    public List<ThumbnailBean> getFilesList() {
        return filesList;
    }

    public void setFilesList(List<ThumbnailBean> filesList) {
        this.filesList = filesList;
    }

    public ImagesResultReceiver getReceiver() {
        return receiver;
    }

    public void setReceiver(ImagesResultReceiver receiver) {
        this.receiver = receiver;
    }

    public String getExpertise() {
        return expertise;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getLicensed() {
        return licensed;
    }

    public void setLicensed(String licensed) {
        this.licensed = licensed;
    }

}
