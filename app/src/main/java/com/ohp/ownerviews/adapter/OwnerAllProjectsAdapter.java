package com.ohp.ownerviews.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.library.basecontroller.AppBaseCompatActivity;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.beans.OwnerAllProjectsModel;
import com.ohp.ownerviews.presenter.AllProjectsListFragmentPresenterImpl;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.List;

import retrofit2.Response;


/**
 * @author Amanpal Singh.
 */

public class OwnerAllProjectsAdapter extends RecyclerView.Adapter<OwnerAllProjectsAdapter.ViewHolder> implements APIHandler.OnCallableResponse, SwipeAdapterInterface {
    public SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);
    public int position;
    private List<OwnerAllProjectsModel.DataBean> listItems;
    private Context context;
    private AllProjectsListFragmentPresenterImpl presenter;

    public OwnerAllProjectsAdapter(AppBaseCompatActivity activity, List<OwnerAllProjectsModel.DataBean> listItems, AllProjectsListFragmentPresenterImpl presenter) {
        context = activity;
        this.listItems = listItems;
        this.presenter = presenter;
    }

    public void clearAll() {
        listItems = null;
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_owner_project, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txt_Project_name.setText(listItems.get(position).getProjectName());
        holder.txt_Property_name.setText(listItems.get(position).getPropertyName());
        holder.txt_JobType_name.setText(listItems.get(position).getProjectJobtype().getJobtypeTitle().trim());
        if (listItems.get(position).getProjectExpertise().size() > 0) {
            StringBuilder expertise = new StringBuilder();
            for (int i = 0; i < listItems.get(position).getProjectExpertise().size(); i++) {
                if (i < (listItems.get(position).getProjectExpertise().size() - 1)) {
                    expertise.append(listItems.get(position).getProjectExpertise().get(i).getExpertiseTitle()).append(", ");
                } else {
                    expertise.append(listItems.get(position).getProjectExpertise().get(i).getExpertiseTitle());
                }
            }
            holder.txt_expertise.setText(expertise.toString());
        }

        switch (listItems.get(position).getProjectStatus()) {
            case "1":
                holder.item.setLeftSwipeEnabled(true);
                holder.statusView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                break;
            case "2":
                holder.item.setLeftSwipeEnabled(false);
                holder.statusView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.holo_green_dark));
                break;
            case "3":
                holder.item.setLeftSwipeEnabled(false);
                holder.statusView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                break;
            case "5":
                holder.item.setLeftSwipeEnabled(false);
                holder.statusView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
                break;
            case "6":
                holder.item.setLeftSwipeEnabled(false);
                holder.statusView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.holo_green_dark));
                break;
        }
        if (!listItems.get(position).getCreated().isEmpty())
            holder.txt_created_time.setText(Utils.getInstance().getYMDFormattedTime(listItems.get(position).getCreated()));
        mItemManger.bind(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return listItems == null ? 0 : listItems.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_item;
    }

    @Override
    public void notifyDTChanged() {

    }

    @Override
    public void onSuccess(Response response, ApiName api) {

    }

    @Override
    public void onFailure(Throwable t, ApiName api) {

    }

    public void update(List<OwnerAllProjectsModel.DataBean> listItems) {
        this.listItems = listItems;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_Project_name, txt_Property_name, txt_JobType_name, txt_created_time, txt_expertise;
        private CardView cardView;
        private View statusView;
        private ImageView iv_edit, iv_delete;
        private SwipeLayout item;

        public ViewHolder(View itemView) {
            super(itemView);
            item = (SwipeLayout) itemView.findViewById(R.id.swipe_item);
            item.setShowMode(SwipeLayout.ShowMode.PullOut);
            item.addDrag(SwipeLayout.DragEdge.Left, item.findViewById(R.id.bottom_wrapper));
            item.addDrag(SwipeLayout.DragEdge.Right, item.findViewById(R.id.bottom_wrapper_2));
            txt_Project_name = (TextView) itemView.findViewById(R.id.txt_project_name);
            txt_Property_name = (TextView) itemView.findViewById(R.id.txt_property_detail);
            txt_created_time = (TextView) itemView.findViewById(R.id.txt_created_date_detail);
            txt_JobType_name = (TextView) itemView.findViewById(R.id.txt_Job_Type_detail);
            txt_expertise = (TextView) itemView.findViewById(R.id.txt_expertise_detail);
            cardView = (CardView) itemView.findViewById(R.id.parent_cardView);
            iv_edit = (ImageView) itemView.findViewById(R.id.iv_edit);
            iv_delete = (ImageView) itemView.findViewById(R.id.iv_delete);
            statusView = itemView.findViewById(R.id.view_status);
            iv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().confirmDialog(context, "Are you sure, you want to delete this project?", "delete", new Utils.ConfirmDialogCallbackInterface() {
                        @Override
                        public void onYesClick(String tag) {
                            mItemManger.closeAllItems();
                            presenter.deleteProject(listItems.get(getAdapterPosition()).getProjectId(), getAdapterPosition());
                        }

                        @Override
                        public void onNoClick(String tag) {
                            mItemManger.closeAllItems();
                        }
                    });
                }
            });
            iv_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_EDIT_PROJECT);
                    intent.putExtra(Constants.PROJECT_STATUS, "");
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(listItems.get(getAdapterPosition()).getProjectId()));
                    context.startActivity(intent);
                    mItemManger.closeAllItems();
                }
            });
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_PROJECT);
                    intent.putExtra(Constants.PROJECT_STATUS, listItems.get(getAdapterPosition()).getProjectStatus());
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(listItems.get(getAdapterPosition()).getProjectId()));
                    context.startActivity(intent);
                    mItemManger.closeAllItems();
                }
            });
        }
    }
}
