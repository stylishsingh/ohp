package com.ohp.ownerviews.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.ohp.R;
import com.ohp.enums.ScreenNavigation;
import com.ohp.ownerviews.beans.PropertiesListModel;
import com.ohp.ownerviews.presenter.PropertiesListFragmentPresenterImpl;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PropertiesListAdapter extends RecyclerView.Adapter<PropertiesListAdapter.ViewHolder> implements SwipeAdapterInterface {

    private final Context context;
    public SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);
    private List<PropertiesListModel.DataBean> listItems;
    private PropertiesListFragmentPresenterImpl presenter;
    private SwipeLayout item;

    public PropertiesListAdapter(List<PropertiesListModel.DataBean> items, Context context, PropertiesListFragmentPresenterImpl presenter) {
        listItems = items;
        this.context = context;
        this.presenter = presenter;
    }

    public void update(List<PropertiesListModel.DataBean> items) {
        listItems = items;
        notifyDataSetChanged();
    }

    public void clearAll() {
        listItems = null;
        notifyDataSetChanged();
    }

    public void deleteItem(int deletedItemPos) {
        notifyItemRemoved(deletedItemPos);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate your layout and pass it to view holder
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_properties, parent, false);
        item = (SwipeLayout) view.findViewById(R.id.swipe_item);
        item.setShowMode(SwipeLayout.ShowMode.PullOut);
        item.addDrag(SwipeLayout.DragEdge.Left, item.findViewById(R.id.bottom_wrapper));
        item.addDrag(SwipeLayout.DragEdge.Right, item.findViewById(R.id.bottom_wrapper_2));
        return new ViewHolder(view);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        System.out.println("ATTACHED");
    }

    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        System.out.println("View Attached");
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        try {
            PropertiesListModel.DataBean dataObj = listItems.get(position);

            holder.tvPropertyName.setText(dataObj.getPropertyName());
            holder.tvPropertyLocation.setText(dataObj.getAddress());
            holder.tvDocumentsCount.setText(String.valueOf(dataObj.getPropertyDocuments()));
            holder.tvAppliancesCount.setText(String.valueOf(dataObj.getPropertyAppliances()));
            holder.tvEventsCount.setText(String.valueOf(dataObj.getPropertyReminders()));
            holder.tvProjectsCount.setText(String.valueOf(dataObj.getPropertyProjects()));

            if (!TextUtils.isEmpty(dataObj.getPropertyImage200X200())) {
                Picasso.with(context).load(dataObj.getPropertyImage200X200()).resize(300, 300).centerCrop().into(holder.ivPropertyImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        holder.progressBar.setVisibility(View.GONE);
                        holder.ivPropertyImage.setImageResource(R.drawable.default_property);
                    }
                });
            } else {
                holder.ivPropertyImage.setImageResource(R.drawable.default_property);
            }
            mItemManger.bind(holder.itemView, position);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return listItems == null ? 0 : listItems.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_item;
    }

    @Override
    public void notifyDTChanged() {

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivEdit, ivDelete, ivPropertyImage;
        private CardView cvPropertyItem;
        private TextView tvPropertyName, tvPropertyLocation, tvProjectsCount, tvAppliancesCount,
                tvDocumentsCount, tvEventsCount;
        private ProgressBar progressBar;

        public ViewHolder(View view) {
            super(view);

            ivEdit = (ImageView) view.findViewById(R.id.iv_edit);
            ivDelete = (ImageView) view.findViewById(R.id.iv_delete);
            ivPropertyImage = (ImageView) view.findViewById(R.id.avatar);
            cvPropertyItem = (CardView) view.findViewById(R.id.cv_property_item);
            tvPropertyName = (TextView) view.findViewById(R.id.tv_property_name);
            tvDocumentsCount = (TextView) view.findViewById(R.id.tv_documents_count);
            tvAppliancesCount = (TextView) view.findViewById(R.id.tv_appliances_count);
            tvProjectsCount = (TextView) view.findViewById(R.id.tv_projects_count);
            tvEventsCount = (TextView) view.findViewById(R.id.tv_reminders_count);
            tvPropertyLocation = (TextView) view.findViewById(R.id.tv_property_location);
            progressBar = (ProgressBar) view.findViewById(R.id.progress);

            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.DELETE_ITEM);
                    mItemManger.closeAllItems();
                }
            });

            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.EDIT_ITEM);
                    mItemManger.closeAllItems();
                }
            });

            cvPropertyItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.DETAIL_ITEM, ivPropertyImage);
                }
            });
        }

    }
}
