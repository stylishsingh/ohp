package com.ohp.ownerviews.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.ohp.R;
import com.ohp.enums.ScreenNavigation;
import com.ohp.ownerviews.beans.EventListModel;
import com.ohp.ownerviews.presenter.OwnerEventListFragmentPresenterImpl;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class OwnerEventListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements SwipeAdapterInterface {

    private final Context context;
    public SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);
    private List<EventListModel.DataBean> listItems;
    private OwnerEventListFragmentPresenterImpl presenter;

    public OwnerEventListAdapter(List<EventListModel.DataBean> items, Context context, OwnerEventListFragmentPresenterImpl presenter) {
        listItems = items;
        this.context = context;
        this.presenter = presenter;
    }

    public void update(List<EventListModel.DataBean> items) {
        listItems = items;
        notifyDataSetChanged();
    }

    public void clearAll() {
        listItems = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void deleteItem(int deletedItemPos) {
        notifyItemRemoved(deletedItemPos);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;
        switch (i) {
            case 0:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.list_item_empty_view, viewGroup, false);
                return new EmptyViewHolder(view);
            case 1:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.list_item_events, viewGroup, false);
                return new ViewHolder(view);


            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        try {

            switch (viewHolder.getItemViewType()) {
                case 1:
                    if (viewHolder instanceof ViewHolder) {
                        ViewHolder dataHolder = (ViewHolder) viewHolder;
                        EventListModel.DataBean dataObj = listItems.get(i);
                        dataHolder.tvName.setText(dataObj.getEventName());
                        dataHolder.tvNotes.setText(dataObj.getEventNotes());
                        dataHolder.tvTime.setText(Utils.getInstance().getYMDEventFormattedDateTime(dataObj.getEventTimeFrom().replace("+00:00", "")));
                        dataHolder.tvDuration.setText(dataObj.getEventRepeat());

                        switch (dataObj.getEventRepeat().toLowerCase()) {
                            case "weekly":
                                dataHolder.tvDuration.setCompoundDrawablesWithIntrinsicBounds(R.drawable.red_dot, 0, 0, 0);
                                break;
                            case "monthly":
                                dataHolder.tvDuration.setCompoundDrawablesWithIntrinsicBounds(R.drawable.orange_dot, 0, 0, 0);
                                break;
                            case "yearly":
                                dataHolder.tvDuration.setCompoundDrawablesWithIntrinsicBounds(R.drawable.green_dot, 0, 0, 0);
                                break;
                            default:
                                dataHolder.tvDuration.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        }

                        switch (dataObj.getEventStatus()) {
                            case 1:
                                dataHolder.item.setRightSwipeEnabled(true);
                                dataHolder.item.setLeftSwipeEnabled(true);
                                break;
                            case 2:
                                dataHolder.item.setRightSwipeEnabled(false);
                                dataHolder.item.setLeftSwipeEnabled(false);
                                break;
                        }

                        mItemManger.bind(viewHolder.itemView, i);
                    }
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return listItems == null ? 0 : listItems.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_item;
    }

    @Override
    public void notifyDTChanged() {

    }

    @Override
    public int getItemViewType(int position) {
        return listItems.get(position) == null ? 0 : 1;
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder {

        public EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvNotes, tvTime, tvDuration;
        private ImageView ivEdit, ivDelete;
        private CardView cvItem;
        private SwipeLayout item;

        public ViewHolder(View itemView) {
            super(itemView);
            item = (SwipeLayout) itemView.findViewById(R.id.swipe_item);
            item.setShowMode(SwipeLayout.ShowMode.PullOut);
            item.addDrag(SwipeLayout.DragEdge.Left, item.findViewById(R.id.bottom_wrapper));
            item.addDrag(SwipeLayout.DragEdge.Right, item.findViewById(R.id.bottom_wrapper_2));
            ivEdit = (ImageView) itemView.findViewById(R.id.iv_edit);
            ivDelete = (ImageView) itemView.findViewById(R.id.iv_delete);
            tvName = (TextView) itemView.findViewById(R.id.tv_event_name);
            tvNotes = (TextView) itemView.findViewById(R.id.tv_event_notes);
            tvTime = (TextView) itemView.findViewById(R.id.tv_event_time);
            tvDuration = (TextView) itemView.findViewById(R.id.tv_event_duration);

            cvItem = (CardView) itemView.findViewById(R.id.cv_item);

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.DELETE_ITEM);
                    mItemManger.closeAllItems();
                }
            });

            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.EDIT_ITEM);
                    mItemManger.closeAllItems();
                }
            });

            cvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.DETAIL_ITEM);
                }
            });

        }
    }
}
