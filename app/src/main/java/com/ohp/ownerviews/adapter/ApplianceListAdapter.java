package com.ohp.ownerviews.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.ohp.R;
import com.ohp.enums.ScreenNavigation;
import com.ohp.ownerviews.beans.ApplianceListModel;
import com.ohp.ownerviews.presenter.AppliancesListFragmentPresenterImpl;
import com.ohp.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class ApplianceListAdapter extends RecyclerView.Adapter<ApplianceListAdapter.ViewHolder> implements SwipeAdapterInterface {

    private final Context context;
    public SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);
    private List<ApplianceListModel.DataBean> listItems;
    private AppliancesListFragmentPresenterImpl presenter;
    private SwipeLayout item;

    public ApplianceListAdapter(List<ApplianceListModel.DataBean> items, Context context, AppliancesListFragmentPresenterImpl presenter) {
        listItems = items;
        this.context = context;
        this.presenter = presenter;
    }

    public void update(List<ApplianceListModel.DataBean> items) {
        listItems = items;
        notifyDataSetChanged();
    }

    public void clearAll() {
        listItems = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void deleteItem(int deletedItemPos) {
        notifyItemRemoved(deletedItemPos);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate your layout and pass it to view holder
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_appliances, parent, false);
        item = (SwipeLayout) view.findViewById(R.id.swipe_item);
        item.setShowMode(SwipeLayout.ShowMode.PullOut);
        item.addDrag(SwipeLayout.DragEdge.Left, item.findViewById(R.id.bottom_wrapper));
        item.addDrag(SwipeLayout.DragEdge.Right, item.findViewById(R.id.bottom_wrapper_2));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        try {
            ApplianceListModel.DataBean dataObj = listItems.get(position);

            holder.tvApplianceName.setText(dataObj.getApplianceName());

            Spannable spannablePropertyName = new SpannableString(context.getString(R.string.property_name) + ": " + dataObj.getPropertyName());
            spannablePropertyName.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.light_gray)), 0, 14, 0);

            holder.tvPropertyName.setText(spannablePropertyName);

            Spannable spannablePurchasedDate;
            if (!dataObj.getPurchaseDate().isEmpty())
                spannablePurchasedDate = new SpannableString(context.getString(R.string.purchased_on) + " "
                        + Utils.getInstance().getFormattedDate(dataObj.getPurchaseDate()));
            else
                spannablePurchasedDate = new SpannableString(context.getString(R.string.purchased_on) + " ");
            spannablePurchasedDate.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.light_gray)), 0, 13, 0);

            holder.tvPurchasedOn.setText(spannablePurchasedDate);

            Spannable spannableExpirationDate;
            if (!dataObj.getWarrantyExpirationDate().isEmpty())
                spannableExpirationDate = new SpannableString(context.getString(R.string.expired_on) + " " + Utils.getInstance().getFormattedDate(dataObj.getWarrantyExpirationDate()));
            else
                spannableExpirationDate = new SpannableString(context.getString(R.string.expired_on) + " ");
            spannableExpirationDate.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.light_gray)), 0, 14, 0);

            holder.tvExpiredOn.setText(spannableExpirationDate);

            if (!TextUtils.isEmpty(dataObj.getApplianceAvatar())) {
                Picasso.with(context).load(dataObj.getApplianceAvatar()).resize(300, 300).centerCrop().into(holder.ivApplianceImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        holder.progressBar.setVisibility(View.GONE);
                        holder.ivApplianceImage.setImageResource(R.drawable.ic_doc_appliance);
                    }
                });
            } else {
                holder.ivApplianceImage.setImageResource(R.drawable.ic_doc_appliance);
            }
            mItemManger.bind(holder.itemView, position);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return listItems == null ? 0 : listItems.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_item;
    }

    @Override
    public void notifyDTChanged() {

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivEdit, ivDelete, ivApplianceImage;
        private CardView cvPropertyItem;
        private TextView tvApplianceName, tvPropertyName, tvPurchasedOn, tvExpiredOn;
        private ProgressBar progressBar;

        public ViewHolder(View view) {
            super(view);

            ivEdit = (ImageView) view.findViewById(R.id.iv_edit);
            ivDelete = (ImageView) view.findViewById(R.id.iv_delete);
            ivApplianceImage = (ImageView) view.findViewById(R.id.appliance_avatar);
            cvPropertyItem = (CardView) view.findViewById(R.id.cv_appliance_item);
            tvPropertyName = (TextView) view.findViewById(R.id.tv_property_name);
            tvApplianceName = (TextView) view.findViewById(R.id.tv_document_name);
            tvPurchasedOn = (TextView) view.findViewById(R.id.tv_appliance_name);
            tvExpiredOn = (TextView) view.findViewById(R.id.tv_created_on);
            progressBar = (ProgressBar) view.findViewById(R.id.progress);
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.DELETE_ITEM, ivApplianceImage);
                    mItemManger.closeAllItems();
                }
            });

            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.EDIT_ITEM, ivApplianceImage);
                    mItemManger.closeAllItems();
                }
            });

            cvPropertyItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.DETAIL_ITEM, ivApplianceImage);
                }
            });
        }

    }

}
