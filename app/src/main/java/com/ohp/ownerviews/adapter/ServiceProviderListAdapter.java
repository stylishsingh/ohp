package com.ohp.ownerviews.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.ServiceProvidersModel;
import com.ohp.enums.ScreenNavigation;
import com.ohp.ownerviews.presenter.ServiceProviderListPresenterImpl;
import com.ohp.utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * @author Amanpal Singh.
 */


public class ServiceProviderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private List<ServiceProvidersModel.DataBean> listItems;
    private ServiceProviderListPresenterImpl presenter;

    public ServiceProviderListAdapter(List<ServiceProvidersModel.DataBean> items, Context context, ServiceProviderListPresenterImpl presenter) {
        listItems = items;
        this.context = context;
        this.presenter = presenter;
    }

    public void update(List<ServiceProvidersModel.DataBean> items) {
        listItems = items;
        notifyDataSetChanged();
    }

    public void clearAll() {
        listItems = null;
        notifyDataSetChanged();
    }

    public void deleteItem(int deletedItemPos) {
        notifyItemRemoved(deletedItemPos);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate your layout and pass it to view holder
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_service_providers, parent, false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        try {
            if (holder instanceof ViewHolder) {
                final ViewHolder dataHolder = (ViewHolder) holder;
                ServiceProvidersModel.DataBean dataObj = listItems.get(position);
                dataHolder.tvProviderName.setText(dataObj.getFullName());
                if (dataObj.getAvgRating() != 0)
                    dataHolder.rbProvider.setRating(Float.parseFloat(String.valueOf(dataObj.getAvgRating())));
                String expertise = "";
                for (int i = 0; i < dataObj.getExpertise().size(); i++) {
                    if (i == 0) {
                        expertise = dataObj.getExpertise().get(i).getExpertiseTitle();
                    } else {
                        expertise = expertise + "," + dataObj.getExpertise().get(i).getExpertiseTitle();
                    }
                }

                Spannable spannableExpertise = new SpannableString("Expertise in: " + expertise);
                spannableExpertise.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.light_gray)), 0, 14, 0);

                dataHolder.tvExpertise.setText(spannableExpertise);

                dataHolder.tvLocation.setText(TextUtils.isEmpty(dataObj.getAwayInKms()) ? "0 Miles Away" : dataObj.getAwayInKms() + " Miles Away");

                if (!TextUtils.isEmpty(dataObj.getProfileImage200X200())) {
                    Picasso.with(context).load(dataObj.getProfileImage200X200()).resize(300, 300).centerCrop().into(dataHolder.ivProviderImage, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                            dataHolder.ivProviderImage.setImageResource(R.drawable.profile_img);
                        }
                    });
                } else {
                    dataHolder.ivProviderImage.setImageResource(R.drawable.profile_img);
                }

                if (dataObj.getIsLicensed() == 1) {
                    dataHolder.tvLicensed.setText(dataObj.getLicenseNo());
                    dataHolder.tvLicensed.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_green_small, 0, 0, 0);
                } else {
                    dataHolder.tvLicensed.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_licensed, 0, 0, 0);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return listItems == null ? 0 : listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvProviderName, tvExpertise, tvLocation, tvLicensed;
        private RatingBar rbProvider;
        private CardView cvItem;
        private ImageView ivProviderImage, ivProviderMessageImage;

        public ViewHolder(View view) {
            super(view);
            tvProviderName = (TextView) view.findViewById(R.id.tv_provider_name);
            tvExpertise = (TextView) view.findViewById(R.id.tv_expertise);
            tvLocation = (TextView) view.findViewById(R.id.tv_location);
            tvLicensed = (TextView) view.findViewById(R.id.tv_licensed);
            rbProvider = (RatingBar) view.findViewById(R.id.rating);
            cvItem = (CardView) view.findViewById(R.id.cv_item);
            ivProviderImage = (ImageView) view.findViewById(R.id.avatar);
            ivProviderMessageImage = (ImageView) view.findViewById(R.id.img_sendMessage);
            ivProviderMessageImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_CHAT);
                    intent.putExtra(Constants.SEND_BY_NAME, listItems.get(getAdapterPosition()).getFullName());
                    intent.putExtra(Constants.SEND_BY_USERID, String.valueOf(listItems.get(getAdapterPosition()).getId()));
                    // intent.putExtra(Constants.MESSAGE_OBJECT, listChatResponse.get(getLayoutPosition()));
                    context.startActivity(intent);
                }
            });

            cvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.DETAIL_ITEM, ivProviderImage);
                }
            });
        }

    }
}

