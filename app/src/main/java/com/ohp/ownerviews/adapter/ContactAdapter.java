package com.ohp.ownerviews.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.ownerviews.beans.AllContactListResponse;
import com.ohp.ownerviews.presenter.EmergencyListFragmentPresenterImpl;
import com.ohp.utils.Constants;
import com.ohp.utils.RecyclerViewFastScroller;
import com.ohp.utils.Utils;

import java.util.List;
import java.util.Random;

/**
 * Created by Vishal Sharma.
 */

public class ContactAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RecyclerViewFastScroller.BubbleTextGetter, SwipeAdapterInterface {
    public SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);
    private List<AllContactListResponse.DataBean> contactList;
    private Context context;
    private SwipeLayout item;
    private ContactSelectListener listener;
    private EmergencyListFragmentPresenterImpl emergencyPresenter;

    private int[] colorArray = {R.color.green, R.color.colorAppRed, R.color.colorAppBlue,
            R.color.colorAppYellow, R.color.colorSeaGreen, R.color.app_hintcolor, R.color.light_gray};

    @Override
    public String getTextToShowInBubble(int pos) {
        return (contactList == null) ? "" : Character.toString(contactList.get(pos).getFullName().charAt(pos));
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_item;
    }

    @Override
    public void notifyDTChanged() {

    }

    public interface ContactSelectListener {
        void onMemberSelect(AllContactListResponse obj);

        void onInviteBtnClick(AllContactListResponse contactObj);
    }

    public ContactAdapter(Context context, List<AllContactListResponse.DataBean> list, ContactSelectListener obj, EmergencyListFragmentPresenterImpl emergencyPresenter) {
//        List<String> items = new ArrayList<>();
//        java.util.Random r = new java.util.Random();
//        for (int i = 0; i < list.size(); i++)
//            items.add(((char) ('A' + r.nextInt('Z' - 'A'))) + " " + Integer.toString(i));
//        java.util.Collections.sort(items);


        this.context = context;
        this.contactList = list;
        this.listener = obj;
        this.emergencyPresenter = emergencyPresenter;
    }

    public void updateList(List<AllContactListResponse.DataBean> list) {
        this.contactList = list;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_contact, parent, false);
        item = (SwipeLayout) view.findViewById(R.id.swipe_item);
        item.setShowMode(SwipeLayout.ShowMode.PullOut);
        item.addDrag(SwipeLayout.DragEdge.Left, item.findViewById(R.id.bottom_wrapper));
        item.addDrag(SwipeLayout.DragEdge.Right, item.findViewById(R.id.bottom_wrapper_2));
        return new MemberHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MemberHolder) {
            AllContactListResponse.DataBean obj = contactList.get(position);
            MemberHolder vh = (MemberHolder) holder;
            char[] stringArray = null;
            if (!TextUtils.isEmpty(obj.getFullName())) {
                vh.memberName.setText(obj.getFullName());
                stringArray = new char[obj.getFullName().length()];
                stringArray = String.valueOf(obj.getFullName()).toCharArray();

                if (stringArray != null) {
                    vh.alternateImg.setText(String.valueOf(stringArray[0]));
                }

                ShapeDrawable biggerCircle = new ShapeDrawable(new OvalShape());
                biggerCircle.setIntrinsicHeight(60);
                biggerCircle.setIntrinsicWidth(60);
                biggerCircle.setBounds(new Rect(30, 30, 30, 30));
                biggerCircle.getPaint().setColor(colorArray[2]);//you can give any color here
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    vh.alternateImg.setBackground(biggerCircle);
                }

                //change random background color
                Drawable background = vh.alternateImg.getBackground();
                if (background instanceof ShapeDrawable) {
                    ShapeDrawable shapeDrawable = (ShapeDrawable) background;
                    Random Dice = new Random();
                    int n = Dice.nextInt(7);
                    shapeDrawable.getPaint().setColor(ContextCompat.getColor(context, colorArray[n]));
                }


            }
        }
        mItemManger.bind(holder.itemView, position);
    }

    public void clearAll() {
        contactList = null;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return (contactList == null) ? 0 : contactList.size();
    }

    private class MemberHolder extends RecyclerView.ViewHolder {
        private CardView parentLayout;
        private TextView memberName, alternateImg;
        private ImageView ivDelete, ivEdit;

        public MemberHolder(View itemView) {
            super(itemView);
            parentLayout = (CardView) itemView.findViewById(R.id.parent_member);
            memberName = (TextView) itemView.findViewById(R.id.contact_name);
            alternateImg = (TextView) itemView.findViewById(R.id.contact_alternate_img);
            ivDelete = (ImageView) itemView.findViewById(R.id.iv_delete);
            ivEdit = (ImageView) itemView.findViewById(R.id.iv_edit);
            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_EDIT_CONTACT);
                    intent.putExtra(Constants.CONTACT_LIST_OBJECT, contactList.get(getLayoutPosition()));
                    context.startActivity(intent);
                    mItemManger.closeAllItems();
                }
            });
            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().confirmDialog(context, "Are you sure, you want to delete this Contact?", "delete", new Utils.ConfirmDialogCallbackInterface() {
                        @Override
                        public void onYesClick(String tag) {
                            mItemManger.closeAllItems();
                            emergencyPresenter.deleteProject(contactList.get(getAdapterPosition()).getContactId(), getAdapterPosition());
                        }

                        @Override
                        public void onNoClick(String tag) {
                            mItemManger.closeAllItems();
                        }
                    });
                    mItemManger.closeAllItems();
                }
            });

            parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_CONTACT);
                    intent.putExtra(Constants.CONTACT_LIST_OBJECT, contactList.get(getLayoutPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}