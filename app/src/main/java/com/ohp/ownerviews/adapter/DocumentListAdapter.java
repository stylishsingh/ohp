package com.ohp.ownerviews.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.ohp.R;
import com.ohp.enums.ScreenNavigation;
import com.ohp.ownerviews.beans.DocumentListModel;
import com.ohp.ownerviews.presenter.DocumentListFragmentPresenterImpl;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * @author Amanpal Singh
 */

public class DocumentListAdapter extends RecyclerView.Adapter<DocumentListAdapter.ViewHolder> implements SwipeAdapterInterface {

    private final Context context;
    public SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);
    private List<DocumentListModel.DataBean> listItems;
    private DocumentListFragmentPresenterImpl presenter;

    public DocumentListAdapter(List<DocumentListModel.DataBean> items, Context context, DocumentListFragmentPresenterImpl presenter) {
        listItems = items;
        this.context = context;
        this.presenter = presenter;
    }

    public void update(List<DocumentListModel.DataBean> items) {
        listItems = items;
        notifyDataSetChanged();
    }

    public void clearAll() {
        listItems = null;
        notifyDataSetChanged();
    }

    public void deleteItem(int deletedItemPos) {
        listItems.remove(deletedItemPos);
        notifyItemRemoved(deletedItemPos);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate your layout and pass it to view holder
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_document, parent, false);
        SwipeLayout item = (SwipeLayout) view.findViewById(R.id.swipe_item);
        item.setShowMode(SwipeLayout.ShowMode.PullOut);
        item.addDrag(SwipeLayout.DragEdge.Left, item.findViewById(R.id.bottom_wrapper));
        item.addDrag(SwipeLayout.DragEdge.Right, item.findViewById(R.id.bottom_wrapper_2));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            DocumentListModel.DataBean dataObj = listItems.get(position);

            holder.tvDocumentName.setText(dataObj.getDocumentName());

            Spannable spannableApplianceName = new SpannableString(context.getString(R.string.title_appliance_name) + " " + dataObj.getApplianceName());
            spannableApplianceName.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.light_gray)), 0, 15, 0);

            holder.tvApplianceName.setText(spannableApplianceName);

            Spannable spannablePropertyName = new SpannableString(context.getString(R.string.title_property_name) + " " + dataObj.getPropertyName());
            spannablePropertyName.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.light_gray)), 0, 14, 0);

            holder.tvPropertyName.setText(spannablePropertyName);

            Spannable spannableCreatedOn = new SpannableString(context.getString(R.string.title_created_on) + " " + dataObj.getCreated());
            spannableCreatedOn.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.light_gray)), 0, 11, 0);

            holder.tvCreatedOn.setText(spannableCreatedOn);

            if (!TextUtils.isEmpty(dataObj.getDocumentAvatar())) {
                Picasso.with(context).load(dataObj.getDocumentAvatar()).resize(300, 300).centerCrop().into(holder.ivDocumentImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        holder.progressBar.setVisibility(View.GONE);
                        holder.ivDocumentImage.setImageResource(R.drawable.ic_doc_appliance);
                    }
                });
            } else {
                holder.ivDocumentImage.setImageResource(R.drawable.ic_doc_appliance);
            }

            mItemManger.bind(holder.itemView, position);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return listItems==null?0:listItems.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_item;
    }

    @Override
    public void notifyDTChanged() {

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivEdit, ivDelete, ivDocumentImage;
        private CardView cvDocumentItem;
        private TextView tvApplianceName, tvPropertyName, tvCreatedOn, tvDocumentName;
        private ProgressBar progressBar;

        public ViewHolder(View view) {
            super(view);

            ivEdit = (ImageView) view.findViewById(R.id.iv_edit);
            ivDelete = (ImageView) view.findViewById(R.id.iv_delete);
            ivDocumentImage = (ImageView) view.findViewById(R.id.document_avatar);
            cvDocumentItem = (CardView) view.findViewById(R.id.cv_document_item);
            tvPropertyName = (TextView) view.findViewById(R.id.tv_property_name);
            tvApplianceName = (TextView) view.findViewById(R.id.tv_appliance_name);
            tvCreatedOn = (TextView) view.findViewById(R.id.tv_created_on);
            tvDocumentName = (TextView) view.findViewById(R.id.tv_document_name);
            progressBar = (ProgressBar) view.findViewById(R.id.progress);

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.DELETE_ITEM, ivDocumentImage);
                    mItemManger.closeAllItems();
                }
            });

            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.EDIT_ITEM, ivDocumentImage);
                    mItemManger.closeAllItems();
                }
            });

            cvDocumentItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.DETAIL_ITEM, ivDocumentImage);
                }
            });
        }

    }


}