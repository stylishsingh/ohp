package com.ohp.ownerviews.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.ProjectDetailResponse;
import com.ohp.enums.ScreenNavigation;
import com.ohp.ownerviews.presenter.AddEditProjectFragmentPresenterImpl;
import com.ohp.utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * @author Vishal Sharma.
 */

public class BidsServiceAdapter extends RecyclerView.Adapter<BidsServiceAdapter.ViewHolder> {
    private Context context;
    private List<ProjectDetailResponse.DataBean.BidsBean> listItems;
    private AddEditProjectFragmentPresenterImpl presenter;

    public BidsServiceAdapter(Context activity, List<ProjectDetailResponse.DataBean.BidsBean> listItems, AddEditProjectFragmentPresenterImpl presenter) {
        this.context = activity;
        this.listItems = listItems;
        this.presenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_bids, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        try {
            viewHolder.bidder_name.setText(listItems.get(position).getFullName());
            viewHolder.txt_bidAmount.setText(String.format("%s (USD)", String.valueOf(listItems.get(position).getBidAmount())));
            viewHolder.txt_distance.setText(String.format("%s miles", String.valueOf(listItems.get(position).getAwayInKms())));

            if (listItems.get(position).getAvgRating() != 0)
                viewHolder.rbBidProvider.setRating(Float.parseFloat(String.valueOf(listItems.get(position).getAvgRating())));

            if (listItems.get(position).getIsLicensed() == 1) {
                viewHolder.txt_licence.setVisibility(View.VISIBLE);
                viewHolder.txt_licence.setText(listItems.get(position).getLicenseNo());
                viewHolder.txt_licence.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_green, 0, 0, 0);
            } else {
//                viewHolder.txt_licence.setVisibility(View.GONE);
                viewHolder.txt_licence.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_gray, 0, 0, 0);
            }
            if (listItems.get(position).getExpertise().size() > 0) {
                StringBuilder expertise = new StringBuilder();
                for (int i = 0; i < listItems.get(position).getExpertise().size(); i++) {

                    if (i < (listItems.get(position).getExpertise().size() - 1)) {
                        expertise.append(listItems.get(position).getExpertise().get(i).getExpertiseTitle()).append(", ");
                    } else {
                        expertise.append(listItems.get(position).getExpertise().get(i).getExpertiseTitle());
                    }
                }
                viewHolder.txt_expertise.setText(expertise);
            }

            if (!TextUtils.isEmpty(listItems.get(position).getProfileImage200X200())) {
                // set image
                Picasso.with(context).load(listItems.get(position).getProfileImage200X200()).resize(300, 300).into(viewHolder.circleImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                        viewHolder.circleImageView.setImageResource(R.drawable.profile_img);
                    }
                });
            } else {
                //show default image
                viewHolder.circleImageView.setImageResource(R.drawable.profile_img);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return listItems == null ? 0 : listItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView circleImageView;
        private TextView bidder_name, txt_expertise, txt_distance, txt_licence, txt_bidAmount;
        private CardView cvItem;
        private RatingBar rbBidProvider;
        private ImageView ivAccept, ivReject;
        private FloatingActionButton fb_sendMessage;

        public ViewHolder(View itemView) {
            super(itemView);
            rbBidProvider = (RatingBar) itemView.findViewById(R.id.ratingBar);
            bidder_name = (TextView) itemView.findViewById(R.id.txt_name);
            txt_distance = (TextView) itemView.findViewById(R.id.txt_provider_distance);
            txt_expertise = (TextView) itemView.findViewById(R.id.txt_expertise_detail);
            txt_licence = (TextView) itemView.findViewById(R.id.txt_txt_provider_licensed);
            txt_bidAmount = (TextView) itemView.findViewById(R.id.txt_txt_provider_amount);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.profile_img);
            ivAccept = (ImageView) itemView.findViewById(R.id.iv_accept);
            ivReject = (ImageView) itemView.findViewById(R.id.iv_reject);
            fb_sendMessage = (FloatingActionButton) itemView.findViewById(R.id.message);
            fb_sendMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_CHAT);
                    intent.putExtra(Constants.USER_NAME, listItems.get(getAdapterPosition()).getFullName());
                    intent.putExtra(Constants.USER_ID, String.valueOf(listItems.get(getAdapterPosition()).getUserId()));
                    // intent.putExtra(Constants.MESSAGE_OBJECT, listChatResponse.get(getLayoutPosition()));
                    context.startActivity(intent);
                }
            });

            cvItem = (CardView) itemView.findViewById(R.id.cv_item);

            cvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.DETAIL_ITEM, circleImageView, bidder_name, txt_distance);
                }
            });


            ivAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.ACCEPT, circleImageView, bidder_name, txt_distance);
                }
            });

            ivReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.REJECT, circleImageView, bidder_name, txt_distance);
                }
            });
        }
    }
}
