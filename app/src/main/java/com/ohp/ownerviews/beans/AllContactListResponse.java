package com.ohp.ownerviews.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vishal.sharma on 9/1/2017.
 */

public class AllContactListResponse extends GenericBean implements Parcelable {

    /**
     * totalRecord : 7
     * data : [{"contact_id":6,"full_name":"Test name1","designation":"","phone":2147483647,"email":"devdev@yopmail.com","category":2},{"contact_id":2,"full_name":"Test name","designation":"vddggtr","phone":2147483647,"email":"sukhd@yopmail.com","category":2},{"contact_id":4,"full_name":"Dev","designation":"","phone":2147483647,"email":"dev@yopmail.com","category":1},{"contact_id":5,"full_name":"Dev1","designation":"","phone":2147483647,"email":"dev1@yopmail.com","category":1},{"contact_id":9,"full_name":"Test name","designation":"","phone":2147483647,"email":"sukhd@yopmail.com","category":2},{"contact_id":10,"full_name":"Test name","designation":"","phone":2147483647,"email":"sukhd@yopmail.com","category":2},{"contact_id":11,"full_name":"Test name","designation":"","phone":2147483647,"email":"sukhd@yopmail.com","category":2}]
     */

    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * contact_id : 6
         * full_name : Test name1
         * designation :
         * phone : 2147483647
         * email : devdev@yopmail.com
         * category : 2
         */

        @SerializedName("contact_id")
        private int contactId;
        @SerializedName("full_name")
        private String fullName;
        @SerializedName("designation")
        private String designation;
        @SerializedName("phone")
        private long phone;
        @SerializedName("email")
        private String email;
        @SerializedName("category")
        private int category;

        public int getContactId() {
            return contactId;
        }

        public void setContactId(int contactId) {
            this.contactId = contactId;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public long getPhone() {
            return phone;
        }

        public void setPhone(long phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public int getCategory() {
            return category;
        }

        public void setCategory(int category) {
            this.category = category;
        }

        public DataBean() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.contactId);
            dest.writeString(this.fullName);
            dest.writeString(this.designation);
            dest.writeLong(this.phone);
            dest.writeString(this.email);
            dest.writeInt(this.category);
        }

        protected DataBean(Parcel in) {
            this.contactId = in.readInt();
            this.fullName = in.readString();
            this.designation = in.readString();
            this.phone = in.readLong();
            this.email = in.readString();
            this.category = in.readInt();
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.totalRecord);
        dest.writeList(this.data);
    }


    protected AllContactListResponse(Parcel in) {
        this.totalRecord = in.readInt();
        this.data = new ArrayList<DataBean>();
        in.readList(this.data, DataBean.class.getClassLoader());
    }

    public static final Creator<AllContactListResponse> CREATOR = new Creator<AllContactListResponse>() {
        @Override
        public AllContactListResponse createFromParcel(Parcel source) {
            return new AllContactListResponse(source);
        }

        @Override
        public AllContactListResponse[] newArray(int size) {
            return new AllContactListResponse[size];
        }
    };
}
