package com.ohp.ownerviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class ApplianceDetailModel extends GenericBean {


    /**
     * data : {"id":1,"appliance_name":"Second Test Appliance","appliance_type":"Fridge","appliance_brand":"J Jvj","purchase_date":"2017-06-01T00:00:00+00:00","created_date":"2017-06-08","warranty_expiration_date":"2017-06-08T00:00:00+00:00","extended_warranty_date":"2","property":{"id":3,"property_name":"testing property","property_avatar":"http://onehomeportal.mobilytedev.com/img/property_picture/original_image/LsZLWnBdPGQUqFn.jpg"},"property_appliance_images":[{"appliance_image_id":2,"appliance_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/qTOf11L3mvodbi0.jpg"},{"appliance_image_id":3,"appliance_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/DZv1s9c1fwlTiDC.jpg"},{"appliance_image_id":4,"appliance_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/A4OWMxtMqFt7EEs.jpg"},{"appliance_image_id":5,"appliance_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/BVBELuEMNF9hFiM.jpg"},{"appliance_image_id":6,"appliance_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/2IFGuh4NTjjcgNH.jpg"}],"property_appliance_doc":[{"appliance_doc_id":2,"appliance_doc_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/qTOf11L3mvodbi0.jpg"},{"appliance_doc_id":3,"appliance_doc_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/DZv1s9c1fwlTiDC.jpg"},{"appliance_doc_id":4,"appliance_doc_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/A4OWMxtMqFt7EEs.jpg"},{"appliance_doc_id":5,"appliance_doc_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/BVBELuEMNF9hFiM.jpg"},{"appliance_doc_id":6,"appliance_doc_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/2IFGuh4NTjjcgNH.jpg"}]}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * appliance_name : Second Test Appliance
         * appliance_type : Fridge
         * appliance_brand : J Jvj
         * purchase_date : 2017-06-01T00:00:00+00:00
         * created_date : 2017-06-08
         * warranty_expiration_date : 2017-06-08T00:00:00+00:00
         * extended_warranty_date : 2
         * property : {"id":3,"property_name":"testing property","property_avatar":"http://onehomeportal.mobilytedev.com/img/property_picture/original_image/LsZLWnBdPGQUqFn.jpg"}
         * property_appliance_images : [{"appliance_image_id":2,"appliance_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/qTOf11L3mvodbi0.jpg"},{"appliance_image_id":3,"appliance_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/DZv1s9c1fwlTiDC.jpg"},{"appliance_image_id":4,"appliance_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/A4OWMxtMqFt7EEs.jpg"},{"appliance_image_id":5,"appliance_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/BVBELuEMNF9hFiM.jpg"},{"appliance_image_id":6,"appliance_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/2IFGuh4NTjjcgNH.jpg"}]
         * property_appliance_doc : [{"appliance_doc_id":2,"appliance_doc_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/qTOf11L3mvodbi0.jpg"},{"appliance_doc_id":3,"appliance_doc_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/DZv1s9c1fwlTiDC.jpg"},{"appliance_doc_id":4,"appliance_doc_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/A4OWMxtMqFt7EEs.jpg"},{"appliance_doc_id":5,"appliance_doc_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/BVBELuEMNF9hFiM.jpg"},{"appliance_doc_id":6,"appliance_doc_avatar":"http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/2IFGuh4NTjjcgNH.jpg"}]
         */

        @SerializedName("id")
        private int id;
        @SerializedName("appliance_name")
        private String applianceName;
        @SerializedName("appliance_type")
        private String applianceType;
        @SerializedName("appliance_serial_no")
        private String applianceSerialNo;
        @SerializedName("appliance_model")
        private String applianceModel;
        @SerializedName("appliance_brand")
        private String applianceBrand;
        @SerializedName("purchase_date")
        private String purchaseDate;
        @SerializedName("created_date")
        private String createdDate;
        @SerializedName("warranty_expiration_date")
        private String warrantyExpirationDate;
        @SerializedName("extended_warranty_date")
        private String extendedWarrantyDate;
        @SerializedName("property")
        private PropertyBean property;
        @SerializedName("property_appliance_images")
        private List<PropertyApplianceImagesBean> propertyApplianceImages;
        @SerializedName("property_appliance_doc")
        private List<PropertyApplianceDocBean> propertyApplianceDoc;

        public String getApplianceSerialNo() {
            return applianceSerialNo;
        }

        public void setApplianceSerialNo(String applianceSerialNo) {
            this.applianceSerialNo = applianceSerialNo;
        }

        public String getApplianceModel() {
            return applianceModel;
        }

        public void setApplianceModel(String applianceModel) {
            this.applianceModel = applianceModel;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getApplianceName() {
            return applianceName;
        }

        public void setApplianceName(String applianceName) {
            this.applianceName = applianceName;
        }

        public String getApplianceType() {
            return applianceType;
        }

        public void setApplianceType(String applianceType) {
            this.applianceType = applianceType;
        }

        public String getApplianceBrand() {
            return applianceBrand;
        }

        public void setApplianceBrand(String applianceBrand) {
            this.applianceBrand = applianceBrand;
        }

        public String getPurchaseDate() {
            return purchaseDate;
        }

        public void setPurchaseDate(String purchaseDate) {
            this.purchaseDate = purchaseDate;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getWarrantyExpirationDate() {
            return warrantyExpirationDate;
        }

        public void setWarrantyExpirationDate(String warrantyExpirationDate) {
            this.warrantyExpirationDate = warrantyExpirationDate;
        }

        public String getExtendedWarrantyDate() {
            return extendedWarrantyDate;
        }

        public void setExtendedWarrantyDate(String extendedWarrantyDate) {
            this.extendedWarrantyDate = extendedWarrantyDate;
        }

        public PropertyBean getProperty() {
            return property;
        }

        public void setProperty(PropertyBean property) {
            this.property = property;
        }

        public List<PropertyApplianceImagesBean> getPropertyApplianceImages() {
            return propertyApplianceImages;
        }

        public void setPropertyApplianceImages(List<PropertyApplianceImagesBean> propertyApplianceImages) {
            this.propertyApplianceImages = propertyApplianceImages;
        }

        public List<PropertyApplianceDocBean> getPropertyApplianceDoc() {
            return propertyApplianceDoc;
        }

        public void setPropertyApplianceDoc(List<PropertyApplianceDocBean> propertyApplianceDoc) {
            this.propertyApplianceDoc = propertyApplianceDoc;
        }

        public static class PropertyBean {
            /**
             * id : 3
             * property_name : testing property
             * property_avatar : http://onehomeportal.mobilytedev.com/img/property_picture/original_image/LsZLWnBdPGQUqFn.jpg
             */

            @SerializedName("id")
            private int id;
            @SerializedName("property_name")
            private String propertyName;
            @SerializedName("property_avatar")
            private String propertyAvatar;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPropertyName() {
                return propertyName;
            }

            public void setPropertyName(String propertyName) {
                this.propertyName = propertyName;
            }

            public String getPropertyAvatar() {
                return propertyAvatar;
            }

            public void setPropertyAvatar(String propertyAvatar) {
                this.propertyAvatar = propertyAvatar;
            }
        }

        public static class PropertyApplianceImagesBean {
            /**
             * appliance_image_id : 2
             * appliance_avatar : http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/qTOf11L3mvodbi0.jpg
             */

            @SerializedName("appliance_image_id")
            private int applianceImageId;
            @SerializedName("appliance_avatar")
            private String applianceAvatar;

            public int getApplianceImageId() {
                return applianceImageId;
            }

            public void setApplianceImageId(int applianceImageId) {
                this.applianceImageId = applianceImageId;
            }

            public String getApplianceAvatar() {
                return applianceAvatar;
            }

            public void setApplianceAvatar(String applianceAvatar) {
                this.applianceAvatar = applianceAvatar;
            }
        }

        public static class PropertyApplianceDocBean {
            /**
             * appliance_doc_id : 2
             * appliance_doc_avatar : http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/qTOf11L3mvodbi0.jpg
             */

            @SerializedName("appliance_doc_id")
            private int applianceDocId;
            @SerializedName("appliance_doc_avatar")
            private String applianceDocAvatar;

            public int getApplianceDocId() {
                return applianceDocId;
            }

            public void setApplianceDocId(int applianceDocId) {
                this.applianceDocId = applianceDocId;
            }

            public String getApplianceDocAvatar() {
                return applianceDocAvatar;
            }

            public void setApplianceDocAvatar(String applianceDocAvatar) {
                this.applianceDocAvatar = applianceDocAvatar;
            }
        }
    }
}
