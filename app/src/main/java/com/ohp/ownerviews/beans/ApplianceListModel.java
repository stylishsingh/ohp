package com.ohp.ownerviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class ApplianceListModel extends GenericBean {

    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * appliance_name : Chchjvj
         * appliance_brand : J Jvj
         * appliance_type : Fridge
         * purchase_date : 2017-06-01T00:00:00+00:00
         * created_date : 2017-06-08
         * warranty_expiration_date : 2017-06-08T00:00:00+00:00
         * extended_warranty_date : 2
         * property_id : 3
         * property_name : ,fuxifixiig
         * property_avatar : http://onehomeportal.mobilytedev.com/img/property_picture/original_image/LsZLWnBdPGQUqFn.jpg
         * appliance_image_id : 2
         * appliance_avatar : http://onehomeportal.mobilytedev.com/img/appliance_picture/original_image/qTOf11L3mvodbi0.jpg
         */

        @SerializedName("id")
        private int id;
        @SerializedName("appliance_name")
        private String applianceName;
        @SerializedName("appliance_brand")
        private String applianceBrand;
        @SerializedName("appliance_type")
        private String applianceType;
        @SerializedName("purchase_date")
        private String purchaseDate;
        @SerializedName("created_date")
        private String createdDate;
        @SerializedName("warranty_expiration_date")
        private String warrantyExpirationDate;
        @SerializedName("extended_warranty_date")
        private String extendedWarrantyDate;
        @SerializedName("property_id")
        private int propertyId;
        @SerializedName("property_name")
        private String propertyName;
        @SerializedName("property_avatar")
        private String propertyAvatar;
        @SerializedName("appliance_image_id")
        private int applianceImageId;
        @SerializedName("appliance_avatar")
        private String applianceAvatar;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getApplianceName() {
            return applianceName;
        }

        public void setApplianceName(String applianceName) {
            this.applianceName = applianceName;
        }

        public String getApplianceBrand() {
            return applianceBrand;
        }

        public void setApplianceBrand(String applianceBrand) {
            this.applianceBrand = applianceBrand;
        }

        public String getApplianceType() {
            return applianceType;
        }

        public void setApplianceType(String applianceType) {
            this.applianceType = applianceType;
        }

        public String getPurchaseDate() {
            return purchaseDate;
        }

        public void setPurchaseDate(String purchaseDate) {
            this.purchaseDate = purchaseDate;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getWarrantyExpirationDate() {
            return warrantyExpirationDate;
        }

        public void setWarrantyExpirationDate(String warrantyExpirationDate) {
            this.warrantyExpirationDate = warrantyExpirationDate;
        }

        public String getExtendedWarrantyDate() {
            return extendedWarrantyDate;
        }

        public void setExtendedWarrantyDate(String extendedWarrantyDate) {
            this.extendedWarrantyDate = extendedWarrantyDate;
        }

        public int getPropertyId() {
            return propertyId;
        }

        public void setPropertyId(int propertyId) {
            this.propertyId = propertyId;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public String getPropertyAvatar() {
            return propertyAvatar;
        }

        public void setPropertyAvatar(String propertyAvatar) {
            this.propertyAvatar = propertyAvatar;
        }

        public int getApplianceImageId() {
            return applianceImageId;
        }

        public void setApplianceImageId(int applianceImageId) {
            this.applianceImageId = applianceImageId;
        }

        public String getApplianceAvatar() {
            return applianceAvatar;
        }

        public void setApplianceAvatar(String applianceAvatar) {
            this.applianceAvatar = applianceAvatar;
        }
    }
}
