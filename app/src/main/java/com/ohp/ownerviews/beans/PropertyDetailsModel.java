package com.ohp.ownerviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class PropertyDetailsModel extends GenericBean {

    /**
     * data : {"id":3,"property_name":",fuxifixiig","resource":[{"title":"bedroom","value":"3"},{"title":"bathroom","value":"4"},{"title":"car_parking","value":"yes"},{"title":"garden","value":"yes"}],"area":"1480","construction_year":"1970-01-01","address":"Shaheed Kanshi Ram College Rd, Punjab 140413, India","city":"","state":"Punjab","zip":"140413","latitude":"30.754227500000006","longitude":"76.62019140625003","description":"vj,fzfu,I'm,&88,,&8*88,8","property_image_100X100":"http://onehomeportal.mobilytedev.com/img/property_picture/100X100/LsZLWnBdPGQUqFn.jpg","property_image_200X200":"http://onehomeportal.mobilytedev.com/img/property_picture/original_image/LsZLWnBdPGQUqFn.jpg","appliance_count":0,"document_count":0,"project_count":0,"reminder_count":0}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 3
         * property_name : ,fuxifixiig
         * resource : [{"title":"bedroom","value":"3"},{"title":"bathroom","value":"4"},{"title":"car_parking","value":"yes"},{"title":"garden","value":"yes"}]
         * area : 1480
         * construction_year : 1970-01-01
         * address : Shaheed Kanshi Ram College Rd, Punjab 140413, India
         * city :
         * state : Punjab
         * zip : 140413
         * latitude : 30.754227500000006
         * longitude : 76.62019140625003
         * description : vj,fzfu,I'm,&88,,&8*88,8
         * property_image_100X100 : http://onehomeportal.mobilytedev.com/img/property_picture/100X100/LsZLWnBdPGQUqFn.jpg
         * property_image_200X200 : http://onehomeportal.mobilytedev.com/img/property_picture/original_image/LsZLWnBdPGQUqFn.jpg
         * appliance_count : 0
         * document_count : 0
         * project_count : 0
         * reminder_count : 0
         */

        @SerializedName("id")
        private int id;
        @SerializedName("property_name")
        private String propertyName;
        @SerializedName("area")
        private String area;
        @SerializedName("construction_year")
        private String constructionYear;
        @SerializedName("address")
        private String address;
        @SerializedName("city")
        private String city;
        @SerializedName("state")
        private String state;
        @SerializedName("zip")
        private String zip;
        @SerializedName("latitude")
        private String latitude;
        @SerializedName("longitude")
        private String longitude;
        @SerializedName("description")
        private String description;
        @SerializedName("property_image_100X100")
        private String propertyImage100X100;
        @SerializedName("property_image_200X200")
        private String propertyImage200X200;
        @SerializedName("property_appliances")
        private int applianceCount;
        @SerializedName("property_documents")
        private int documentCount;
        @SerializedName("property_projects")
        private int projectCount;
        @SerializedName("property_reminders")
        private int reminderCount;
        @SerializedName("resource")
        private List<ResourceBean> resource;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getConstructionYear() {
            return constructionYear;
        }

        public void setConstructionYear(String constructionYear) {
            this.constructionYear = constructionYear;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPropertyImage100X100() {
            return propertyImage100X100;
        }

        public void setPropertyImage100X100(String propertyImage100X100) {
            this.propertyImage100X100 = propertyImage100X100;
        }

        public String getPropertyImage200X200() {
            return propertyImage200X200;
        }

        public void setPropertyImage200X200(String propertyImage200X200) {
            this.propertyImage200X200 = propertyImage200X200;
        }

        public int getApplianceCount() {
            return applianceCount;
        }

        public void setApplianceCount(int applianceCount) {
            this.applianceCount = applianceCount;
        }

        public int getDocumentCount() {
            return documentCount;
        }

        public void setDocumentCount(int documentCount) {
            this.documentCount = documentCount;
        }

        public int getProjectCount() {
            return projectCount;
        }

        public void setProjectCount(int projectCount) {
            this.projectCount = projectCount;
        }

        public int getReminderCount() {
            return reminderCount;
        }

        public void setReminderCount(int reminderCount) {
            this.reminderCount = reminderCount;
        }

        public List<ResourceBean> getResource() {
            return resource;
        }

        public void setResource(List<ResourceBean> resource) {
            this.resource = resource;
        }

        public static class ResourceBean {
            /**
             * title : bedroom
             * value : 3
             */

            @SerializedName("title")
            private String title;
            @SerializedName("value")
            private String value;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }
}

