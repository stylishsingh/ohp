package com.ohp.ownerviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

/**
 * @author Amanpal Singh.
 */

public class EventDetailModel extends GenericBean {


    /**
     * status : 200
     * data : {"event_reminder_repeat":"","event_repeat":"","event_id":34,"property_id":1,"appliance_id":3,"project_id":2,"provider_id":2,"event_name":"Test event name","event_notes":"Test event notes","event_date":"07-07-2017","event_time_from":"2017-07-07T16:21:08+00:00","event_time_to":"2017-07-07T16:21:08+00:00","event_reminder":"2017-07-07T16:21:08+00:00","property_name":"justin property","appliance_name":"fan","project_name":"Test new project1","provider_name":""}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * event_reminder_repeat :
         * event_repeat :
         * event_id : 34
         * property_id : 1
         * appliance_id : 3
         * project_id : 2
         * provider_id : 2
         * event_name : Test event name
         * event_notes : Test event notes
         * event_date : 07-07-2017
         * event_time_from : 2017-07-07T16:21:08+00:00
         * event_time_to : 2017-07-07T16:21:08+00:00
         * event_reminder : 2017-07-07T16:21:08+00:00
         * property_name : justin property
         * appliance_name : fan
         * project_name : Test new project1
         * provider_name :
         */

        @SerializedName("event_reminder_repeat")
        private String eventReminderRepeat;
        @SerializedName("event_repeat")
        private String eventRepeat;
        @SerializedName("event_id")
        private int eventId;
        @SerializedName("property_id")
        private int propertyId;
        @SerializedName("appliance_id")
        private int applianceId;
        @SerializedName("project_id")
        private int projectId;
        @SerializedName("provider_id")
        private int providerId;
        @SerializedName("event_name")
        private String eventName;
        @SerializedName("event_notes")
        private String eventNotes;
        @SerializedName("event_date")
        private String eventDate;
        @SerializedName("event_time_from")
        private String eventTimeFrom;
        @SerializedName("event_time_to")
        private String eventTimeTo;
        @SerializedName("event_reminder")
        private String eventReminder;
        @SerializedName("property_name")
        private String propertyName;
        @SerializedName("appliance_name")
        private String applianceName;
        @SerializedName("project_name")
        private String projectName;
        @SerializedName("provider_name")
        private String providerName;

        public String getEventReminderRepeat() {
            return eventReminderRepeat;
        }

        public void setEventReminderRepeat(String eventReminderRepeat) {
            this.eventReminderRepeat = eventReminderRepeat;
        }

        public String getEventRepeat() {
            return eventRepeat;
        }

        public void setEventRepeat(String eventRepeat) {
            this.eventRepeat = eventRepeat;
        }

        public int getEventId() {
            return eventId;
        }

        public void setEventId(int eventId) {
            this.eventId = eventId;
        }

        public int getPropertyId() {
            return propertyId;
        }

        public void setPropertyId(int propertyId) {
            this.propertyId = propertyId;
        }

        public int getApplianceId() {
            return applianceId;
        }

        public void setApplianceId(int applianceId) {
            this.applianceId = applianceId;
        }

        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }

        public int getProviderId() {
            return providerId;
        }

        public void setProviderId(int providerId) {
            this.providerId = providerId;
        }

        public String getEventName() {
            return eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        public String getEventNotes() {
            return eventNotes;
        }

        public void setEventNotes(String eventNotes) {
            this.eventNotes = eventNotes;
        }

        public String getEventDate() {
            return eventDate;
        }

        public void setEventDate(String eventDate) {
            this.eventDate = eventDate;
        }

        public String getEventTimeFrom() {
            return eventTimeFrom;
        }

        public void setEventTimeFrom(String eventTimeFrom) {
            this.eventTimeFrom = eventTimeFrom;
        }

        public String getEventTimeTo() {
            return eventTimeTo;
        }

        public void setEventTimeTo(String eventTimeTo) {
            this.eventTimeTo = eventTimeTo;
        }

        public String getEventReminder() {
            return eventReminder;
        }

        public void setEventReminder(String eventReminder) {
            this.eventReminder = eventReminder;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public String getApplianceName() {
            return applianceName;
        }

        public void setApplianceName(String applianceName) {
            this.applianceName = applianceName;
        }

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getProviderName() {
            return providerName;
        }

        public void setProviderName(String providerName) {
            this.providerName = providerName;
        }
    }
}
