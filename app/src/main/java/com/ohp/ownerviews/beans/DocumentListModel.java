package com.ohp.ownerviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class DocumentListModel extends GenericBean {

    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * document_name : Pankha
         * created : 2017-06-08
         * appliance_id : 1
         * appliance_name : Chchjvj
         * property_id : 3
         * property_name : ,fuxifixiig
         * property_avatar : http://onehomeportal.mobilytedev.com/img/property_picture/original_image/LsZLWnBdPGQUqFn.jpg
         * document_image_id : 1
         * document_avatar : http://onehomeportal.mobilytedev.com/img/document_picture/original_image/kZrhnFREluogmaa.jpg
         */

        @SerializedName("id")
        private int id;
        @SerializedName("document_name")
        private String documentName;
        @SerializedName("created")
        private String created;
        @SerializedName("appliance_id")
        private int applianceId;
        @SerializedName("appliance_name")
        private String applianceName;
        @SerializedName("property_id")
        private int propertyId;
        @SerializedName("property_name")
        private String propertyName;
        @SerializedName("property_avatar")
        private String propertyAvatar;
        @SerializedName("document_image_id")
        private int documentImageId;
        @SerializedName("document_avatar")
        private String documentAvatar;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDocumentName() {
            return documentName;
        }

        public void setDocumentName(String documentName) {
            this.documentName = documentName;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public int getApplianceId() {
            return applianceId;
        }

        public void setApplianceId(int applianceId) {
            this.applianceId = applianceId;
        }

        public String getApplianceName() {
            return applianceName;
        }

        public void setApplianceName(String applianceName) {
            this.applianceName = applianceName;
        }

        public int getPropertyId() {
            return propertyId;
        }

        public void setPropertyId(int propertyId) {
            this.propertyId = propertyId;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public String getPropertyAvatar() {
            return propertyAvatar;
        }

        public void setPropertyAvatar(String propertyAvatar) {
            this.propertyAvatar = propertyAvatar;
        }

        public int getDocumentImageId() {
            return documentImageId;
        }

        public void setDocumentImageId(int documentImageId) {
            this.documentImageId = documentImageId;
        }

        public String getDocumentAvatar() {
            return documentAvatar;
        }

        public void setDocumentAvatar(String documentAvatar) {
            this.documentAvatar = documentAvatar;
        }
    }
}
