package com.ohp.ownerviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * Created by Anuj Sharma on 6/6/2017.
 */

public class DocumentDetailModel extends GenericBean {

    /**
     * data : {"id":35,"document_name":"Pankha","created":"2017-06-07","appliance":{"appliance_id":195,"appliance_name":"love property"},"property":{"property_id":195,"property_name":"love property","property_avatar":"http://onehomeportal.mobilytedev.com/img/property_picture/original_image/pwzRgN5N7mqsMhn.jpg"},"property_document_images":[{"document_image_id":35,"document_avatar":"http://onehomeportal.mobilytedev.com/img/document_picture/original_image/oJ9QmVw11nTEfLW.jpg"},{"document_image_id":36,"document_avatar":"http://onehomeportal.mobilytedev.com/img/document_picture/original_image/gJWw5h3yaH8l8RP.png"},{"document_image_id":37,"document_avatar":"http://onehomeportal.mobilytedev.com/img/document_picture/original_image/8XlcZdW1zsp5gpn.jpg"},{"document_image_id":38,"document_avatar":"http://onehomeportal.mobilytedev.com/img/document_picture/original_image/1XN78S7V7qJ1HaJ.png"}]}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 35
         * document_name : Pankha
         * created : 2017-06-07
         * appliance : {"appliance_id":195,"appliance_name":"love property"}
         * property : {"property_id":195,"property_name":"love property","property_avatar":"http://onehomeportal.mobilytedev.com/img/property_picture/original_image/pwzRgN5N7mqsMhn.jpg"}
         * property_document_images : [{"document_image_id":35,"document_avatar":"http://onehomeportal.mobilytedev.com/img/document_picture/original_image/oJ9QmVw11nTEfLW.jpg"},{"document_image_id":36,"document_avatar":"http://onehomeportal.mobilytedev.com/img/document_picture/original_image/gJWw5h3yaH8l8RP.png"},{"document_image_id":37,"document_avatar":"http://onehomeportal.mobilytedev.com/img/document_picture/original_image/8XlcZdW1zsp5gpn.jpg"},{"document_image_id":38,"document_avatar":"http://onehomeportal.mobilytedev.com/img/document_picture/original_image/1XN78S7V7qJ1HaJ.png"}]
         */

        @SerializedName("id")
        private int id;
        @SerializedName("document_name")
        private String documentName;
        @SerializedName("created")
        private String created;
        @SerializedName("appliance")
        private ApplianceBean appliance;
        @SerializedName("property")
        private PropertyBean property;
        @SerializedName("property_document_images")
        private List<PropertyDocumentImagesBean> propertyDocumentImages;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDocumentName() {
            return documentName;
        }

        public void setDocumentName(String documentName) {
            this.documentName = documentName;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public ApplianceBean getAppliance() {
            return appliance;
        }

        public void setAppliance(ApplianceBean appliance) {
            this.appliance = appliance;
        }

        public PropertyBean getProperty() {
            return property;
        }

        public void setProperty(PropertyBean property) {
            this.property = property;
        }

        public List<PropertyDocumentImagesBean> getPropertyDocumentImages() {
            return propertyDocumentImages;
        }

        public void setPropertyDocumentImages(List<PropertyDocumentImagesBean> propertyDocumentImages) {
            this.propertyDocumentImages = propertyDocumentImages;
        }

        public static class ApplianceBean {
            /**
             * appliance_id : 195
             * appliance_name : love property
             */

            @SerializedName("appliance_id")
            private int applianceId;
            @SerializedName("appliance_name")
            private String applianceName;

            public int getApplianceId() {
                return applianceId;
            }

            public void setApplianceId(int applianceId) {
                this.applianceId = applianceId;
            }

            public String getApplianceName() {
                return applianceName;
            }

            public void setApplianceName(String applianceName) {
                this.applianceName = applianceName;
            }
        }

        public static class PropertyBean {
            /**
             * property_id : 195
             * property_name : love property
             * property_avatar : http://onehomeportal.mobilytedev.com/img/property_picture/original_image/pwzRgN5N7mqsMhn.jpg
             */

            @SerializedName("property_id")
            private int propertyId;
            @SerializedName("property_name")
            private String propertyName;
            @SerializedName("property_avatar")
            private String propertyAvatar;

            public int getPropertyId() {
                return propertyId;
            }

            public void setPropertyId(int propertyId) {
                this.propertyId = propertyId;
            }

            public String getPropertyName() {
                return propertyName;
            }

            public void setPropertyName(String propertyName) {
                this.propertyName = propertyName;
            }

            public String getPropertyAvatar() {
                return propertyAvatar;
            }

            public void setPropertyAvatar(String propertyAvatar) {
                this.propertyAvatar = propertyAvatar;
            }
        }

        public static class PropertyDocumentImagesBean {
            /**
             * document_image_id : 35
             * document_avatar : http://onehomeportal.mobilytedev.com/img/document_picture/original_image/oJ9QmVw11nTEfLW.jpg
             */

            @SerializedName("document_image_id")
            private int documentImageId;
            @SerializedName("document_avatar")
            private String documentAvatar;

            public int getDocumentImageId() {
                return documentImageId;
            }

            public void setDocumentImageId(int documentImageId) {
                this.documentImageId = documentImageId;
            }

            public String getDocumentAvatar() {
                return documentAvatar;
            }

            public void setDocumentAvatar(String documentAvatar) {
                this.documentAvatar = documentAvatar;
            }
        }
    }
}
