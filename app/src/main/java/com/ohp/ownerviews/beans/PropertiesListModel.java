package com.ohp.ownerviews.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Amanpal Singh.
 */


public class PropertiesListModel extends GenericBean {
    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };



        /**
         * id : 3
         * property_name : ,fuxifixiig
         * bedroom : 3
         * bathroom : 4
         * car_parking : no
         * garden : yes
         * area : 1480
         * construction_year : 1970-01-01
         * address : Shaheed Kanshi Ram College Rd, Punjab 140413, India
         * city :
         * state : Punjab
         * zip : 140413
         * latitude : 30.754227500000006
         * longitude : 76.62019140625003
         * description : vj,fzfu,I'm,&88,,&8*88,8
         * property_image_100X100 : http://onehomeportal.mobilytedev.com/img/property_picture/100X100/LsZLWnBdPGQUqFn.jpg
         * property_image_200X200 : http://onehomeportal.mobilytedev.com/img/property_picture/original_image/LsZLWnBdPGQUqFn.jpg
         * property_appliances : 1
         * property_documents : 1
         * property_projects : 0
         * property_reminders : 0
         */

        @SerializedName("id")
        private int id;
        @SerializedName("property_name")
        private String propertyName;
        @SerializedName("bedroom")
        private String bedroom;
        @SerializedName("bathroom")
        private String bathroom;
        @SerializedName("car_parking")
        private String carParking;
        @SerializedName("garden")
        private String garden;
        @SerializedName("area")
        private String area;
        @SerializedName("construction_year")
        private String constructionYear;
        @SerializedName("address")
        private String address;
        @SerializedName("city")
        private String city;
        @SerializedName("state")
        private String state;
        @SerializedName("zip")
        private String zip;
        @SerializedName("latitude")
        private String latitude;
        @SerializedName("longitude")
        private String longitude;
        @SerializedName("description")
        private String description;
        @SerializedName("property_image_100X100")
        private String propertyImage100X100;
        @SerializedName("property_image_200X200")
        private String propertyImage200X200;
        @SerializedName("property_appliances")
        private int propertyAppliances;
        @SerializedName("property_documents")
        private int propertyDocuments;
        @SerializedName("property_projects")
        private int propertyProjects;
        @SerializedName("property_reminders")
        private int propertyReminders;
        @SerializedName("created")
        private String createdDate;

        protected DataBean(Parcel in) {
            id = in.readInt();
            propertyName = in.readString();
            bedroom = in.readString();
            bathroom = in.readString();
            carParking = in.readString();
            garden = in.readString();
            area = in.readString();
            constructionYear = in.readString();
            address = in.readString();
            city = in.readString();
            state = in.readString();
            zip = in.readString();
            latitude = in.readString();
            longitude = in.readString();
            description = in.readString();
            propertyImage100X100 = in.readString();
            propertyImage200X200 = in.readString();
            propertyAppliances = in.readInt();
            propertyDocuments = in.readInt();
            propertyProjects = in.readInt();
            propertyReminders = in.readInt();
            createdDate = in.readString();
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public Date getDate() {
            Calendar calendar = Calendar.getInstance();
            try {
                System.out.println("DataBean.getDate---"+getCreatedDate());
                if (!getCreatedDate().isEmpty()) {
                    String[] parseString = getCreatedDate().split(" ");
                    String[] dateString = parseString[0].split("-");
                    String[] timeString = parseString[1].split(":");

                    calendar.set(Integer.valueOf(dateString[0]), Integer.valueOf(dateString[1]) - 1, Integer.valueOf(dateString[2]),
                            Integer.parseInt(timeString[0]), Integer.parseInt(timeString[1]),Integer.parseInt(timeString[2]));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return calendar.getTime();
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public String getBedroom() {
            return bedroom;
        }

        public void setBedroom(String bedroom) {
            this.bedroom = bedroom;
        }

        public String getBathroom() {
            return bathroom;
        }

        public void setBathroom(String bathroom) {
            this.bathroom = bathroom;
        }

        public String getCarParking() {
            return carParking;
        }

        public void setCarParking(String carParking) {
            this.carParking = carParking;
        }

        public String getGarden() {
            return garden;
        }

        public void setGarden(String garden) {
            this.garden = garden;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getConstructionYear() {
            return constructionYear;
        }

        public void setConstructionYear(String constructionYear) {
            this.constructionYear = constructionYear;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPropertyImage100X100() {
            return propertyImage100X100;
        }

        public void setPropertyImage100X100(String propertyImage100X100) {
            this.propertyImage100X100 = propertyImage100X100;
        }

        public String getPropertyImage200X200() {
            return propertyImage200X200;
        }

        public void setPropertyImage200X200(String propertyImage200X200) {
            this.propertyImage200X200 = propertyImage200X200;
        }

        public int getPropertyAppliances() {
            return propertyAppliances;
        }

        public void setPropertyAppliances(int propertyAppliances) {
            this.propertyAppliances = propertyAppliances;
        }

        public int getPropertyDocuments() {
            return propertyDocuments;
        }

        public void setPropertyDocuments(int propertyDocuments) {
            this.propertyDocuments = propertyDocuments;
        }

        public int getPropertyProjects() {
            return propertyProjects;
        }

        public void setPropertyProjects(int propertyProjects) {
            this.propertyProjects = propertyProjects;
        }

        public int getPropertyReminders() {
            return propertyReminders;
        }

        public void setPropertyReminders(int propertyReminders) {
            this.propertyReminders = propertyReminders;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(propertyName);
            dest.writeString(bedroom);
            dest.writeString(bathroom);
            dest.writeString(carParking);
            dest.writeString(garden);
            dest.writeString(area);
            dest.writeString(constructionYear);
            dest.writeString(address);
            dest.writeString(city);
            dest.writeString(state);
            dest.writeString(zip);
            dest.writeString(latitude);
            dest.writeString(longitude);
            dest.writeString(description);
            dest.writeString(propertyImage100X100);
            dest.writeString(propertyImage200X200);
            dest.writeInt(propertyAppliances);
            dest.writeInt(propertyDocuments);
            dest.writeInt(propertyProjects);
            dest.writeInt(propertyReminders);
            dest.writeString(createdDate);
        }
    }
}


