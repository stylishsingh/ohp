package com.ohp.ownerviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class OwnerAllProjectsModel extends GenericBean {


    /**
     * totalRecord : 1
     * data : [{"project_id":223,"project_name":"BZBZVH","project_owner":"Mr Jain","project_status":"upcoming","property_name":"Bznssn","bids_count":0,"created":"28-07-2017","provider_name":"","project_jobtype":{"jobtype_id":1,"jobtype_title":"Repair"},"project_expertise":[{"expertise_id":1,"expertise_title":"Cleaning Services"}],"away_in_kms":""}]
     */

    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * project_id : 223
         * project_name : BZBZVH
         * project_owner : Mr Jain
         * project_status : upcoming
         * property_name : Bznssn
         * bids_count : 0
         * created : 28-07-2017
         * provider_name :
         * project_jobtype : {"jobtype_id":1,"jobtype_title":"Repair"}
         * project_expertise : [{"expertise_id":1,"expertise_title":"Cleaning Services"}]
         * away_in_kms :
         */

        @SerializedName("project_id")
        private int projectId;
        @SerializedName("project_name")
        private String projectName;
        @SerializedName("project_owner")
        private String projectOwner;
        @SerializedName("project_status")
        private String projectStatus;
        @SerializedName("property_name")
        private String propertyName;
        @SerializedName("bids_count")
        private int bidsCount;
        @SerializedName("created")
        private String created;
        @SerializedName("provider_name")
        private String providerName;
        @SerializedName("project_jobtype")
        private ProjectJobtypeBean projectJobtype;
        @SerializedName("away_in_kms")
        private String awayInKms;
        @SerializedName("project_expertise")
        private List<ProjectExpertiseBean> projectExpertise;

        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getProjectOwner() {
            return projectOwner;
        }

        public void setProjectOwner(String projectOwner) {
            this.projectOwner = projectOwner;
        }

        public String getProjectStatus() {
            return projectStatus;
        }

        public void setProjectStatus(String projectStatus) {
            this.projectStatus = projectStatus;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public int getBidsCount() {
            return bidsCount;
        }

        public void setBidsCount(int bidsCount) {
            this.bidsCount = bidsCount;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getProviderName() {
            return providerName;
        }

        public void setProviderName(String providerName) {
            this.providerName = providerName;
        }

        public ProjectJobtypeBean getProjectJobtype() {
            return projectJobtype;
        }

        public void setProjectJobtype(ProjectJobtypeBean projectJobtype) {
            this.projectJobtype = projectJobtype;
        }

        public String getAwayInKms() {
            return awayInKms;
        }

        public void setAwayInKms(String awayInKms) {
            this.awayInKms = awayInKms;
        }

        public List<ProjectExpertiseBean> getProjectExpertise() {
            return projectExpertise;
        }

        public void setProjectExpertise(List<ProjectExpertiseBean> projectExpertise) {
            this.projectExpertise = projectExpertise;
        }

        public static class ProjectJobtypeBean {
            /**
             * jobtype_id : 1
             * jobtype_title : Repair
             */

            @SerializedName("jobtype_id")
            private int jobtypeId;
            @SerializedName("jobtype_title")
            private String jobtypeTitle;

            public int getJobtypeId() {
                return jobtypeId;
            }

            public void setJobtypeId(int jobtypeId) {
                this.jobtypeId = jobtypeId;
            }

            public String getJobtypeTitle() {
                return jobtypeTitle;
            }

            public void setJobtypeTitle(String jobtypeTitle) {
                this.jobtypeTitle = jobtypeTitle;
            }
        }

        public static class ProjectExpertiseBean {
            /**
             * expertise_id : 1
             * expertise_title : Cleaning Services
             */

            @SerializedName("expertise_id")
            private int expertiseId;
            @SerializedName("expertise_title")
            private String expertiseTitle;

            public int getExpertiseId() {
                return expertiseId;
            }

            public void setExpertiseId(int expertiseId) {
                this.expertiseId = expertiseId;
            }

            public String getExpertiseTitle() {
                return expertiseTitle;
            }

            public void setExpertiseTitle(String expertiseTitle) {
                this.expertiseTitle = expertiseTitle;
            }
        }
    }
}


