package com.ohp.ownerviews.view;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.library.mvp.BaseView;
import com.ohp.utils.CustomViewPager;

/**
 * @author Amanpal Singh.
 */

public interface OwnerDashboardFragmentView extends BaseView {


    TabLayout getTabLayout();

    CustomViewPager getViewPager();

    View getRootView();

}
