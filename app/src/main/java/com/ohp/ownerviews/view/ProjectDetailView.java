package com.ohp.ownerviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.library.mvp.BaseView;

import de.hdodenhof.circleimageview.CircleImageView;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

/**
 * Created by vishal.sharma on 7/12/2017.
 */

public interface ProjectDetailView extends BaseView {
    View getRootView();

    NestedScrollView getNestedScrollView();

    Toolbar getToolbar();

    TextView getToolbarTitle();

    String getPropertyID();

    ProgressBar getProgressBar();

    ImageView getPropertyImage();

    MultiSelectSpinner getExpertiseSpinnerView();

    TextInputEditText getPropertyName();

    TextInputEditText getPropertyAddress();

    TextInputEditText getProjectName();

    TextInputEditText getProjectCreatedOn();

    TextInputEditText getProjectJopType();

    TextInputEditText getProjectDescriptionEdit();

    TextInputEditText getProjectExpertise();

    TextInputEditText getProjectDescription();

    TextInputEditText getApplianceDescription();

    TextInputEditText getProjectNameEdit();

    AppCompatSpinner getJobType();

    TextView getExpertise();

    AppCompatSpinner getProperty();

    AppCompatSpinner getApplianceName();

    View getSaveProject();

    View getEditProject();

    LinearLayout getLayout();

    RecyclerView getProjectDetailRecycler();

    RecyclerView getEditProjectDetailRecycler();

    RecyclerView getSpBidsRecyclerView();

    TextView NoImages();

    TextView NoBids();

    TextView getAwardProviderNameView();

    TextView getAwardProviderExpertiseView();

    TextView getAwardProviderDistanceView();

    TextView getAwardProviderLicenceView();

    TextView getAwardProviderBidAmountView();

    TextView getAwardProviderHeader();

    RatingBar getAwardedProviderRatingView();

    TextView getTVEndView();

    TextView getTVCancelView();

    CardView getAwardProviderView();

    View getBidLayoutView();

    View getCancelEndLayoutView();

    View getAcceptRejectLayoutView();

    CircleImageView getAwardProviderImage();

    MenuItem getEditMenuView();

    MenuItem getSaveMenuView();

    MenuItem getEndMenuView();

}
