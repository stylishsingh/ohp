package com.ohp.ownerviews.view;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by Anuj Sharma on 5/30/2017.
 */

public interface OwnerProfileActivityView extends BaseView {
    Toolbar getToolbarView();

    TextView getToolbarTitleView();
}
