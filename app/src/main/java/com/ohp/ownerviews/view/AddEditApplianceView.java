package com.ohp.ownerviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.library.mvp.BaseView;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * @author Amanpal Singh.
 */

public interface AddEditApplianceView extends BaseView {
    View getRootView();

    Toolbar getToolbar();

    TextView getToolbarTitle();

    TextView getHeaderPropertyNameView();

    TextView getHeaderPropertyImageView();

    TextView getHeaderApplianceImageView();

    TextView getHeaderApplianceDocView();

    TextView getHeaderBrandNameView();

    TextView getHeaderApplianceTypeView();

    LinearLayout getDetailTopContainer();

    ImageView getPropertyImage();

    ProgressBar getImageProgress();

    LinearLayout getEditImageContainer();

    LinearLayout getDetailImageContainer();

    ViewPager getImageViewPager();

    ViewPager getDocsViewPager();

    CirclePageIndicator getImgIndicator();

    CirclePageIndicator getDocsIndicator();

    AppCompatSpinner getPropertySpinner();

    AppCompatSpinner getSpinnerBrandNameView();

    AppCompatSpinner getSpinnerApplianceTypeView();

    TextInputEditText getApplianceName();

    TextInputEditText getApplianceNameDetail();

    TextInputEditText getModelName();

    TextInputEditText getModelNameDetail();

    TextInputEditText getSerialNumber();

    TextInputEditText getSerialNumberDetail();

    TextInputEditText getPurchaseDate();

    TextInputEditText getWarrantyExpDate();

    TextInputEditText getExtendedWarrantyExpDate();

    RecyclerView getApplianceImageRecycler();

    RecyclerView getApplianceDocsRecycler();

    MenuItem getEditMenuView();

    MenuItem getSaveMenuView();

    String getScreenCheck();

    TextInputLayout getTILApplianceView();

    TextInputLayout getTILApplianceDetailView();

    TextInputLayout getTILModelNameView();

    TextInputLayout getTILModelNameDetailView();

    TextInputLayout getTILSerialNumberView();

    TextInputLayout getTILSerialNumberDetailView();

    View getApplianceTypeLineView();

    View getPropertyNameLineView();

    View getBrandNameLineView();


}
