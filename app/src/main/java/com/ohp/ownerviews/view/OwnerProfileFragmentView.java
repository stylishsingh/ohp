package com.ohp.ownerviews.view;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.library.mvp.BaseView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Anuj Sharma on 5/25/2017.
 */

public interface OwnerProfileFragmentView extends BaseView {
    View getRootView();

    NestedScrollView getScrollView();

    ImageView getCoverImg();

    CircleImageView getProfileImg();

    TextView tvHeaderGender();

    FloatingActionButton getEditProfile();

    ProgressBar getProfileProgress();

    TextInputEditText getName();

    TextInputEditText getEmail();

    TextInputEditText getAddress();

    RadioGroup getRadioGroup();

    RadioButton getMaleRadio();

    RadioButton getFemaleRadio();

    MenuItem getEditMenu();

    MenuItem getSaveMenu();

    TextInputLayout getFullNameLayoutView();
}
