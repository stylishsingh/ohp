package com.ohp.ownerviews.view;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * @author Amanpal Singh.
 */

public interface SpDetailFragmentView extends BaseView {

    View getRootView();

    Toolbar getToolbar();

    TextView getToolbarTitle();

    TextView getTVMilesView();

    TextView getTVProviderNameView();

    TextView getTVViewReviews();

    TextView getTVBusinessNameView();

    TextView getTVBusinessAddressView();

    TextView getTVWebsiteView();

    TextView getTVNotesView();

    TextView getTVExpertiseView();

    TextView getTVInsuredView();

    TextView getTVLicensedView();

    TextView getTVBondedView();

    TextView getTVBidAmountView();

    ImageView getIVAcceptView();

    ImageView getIVRejectView();

    ImageView getIVAvatar();

    View getParentBidAcceptRejectView();

    View getParentNotesView();

    View getParentExpertiseView();

    RatingBar getRBProviderView();

}
