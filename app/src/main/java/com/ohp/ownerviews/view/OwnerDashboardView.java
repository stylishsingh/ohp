package com.ohp.ownerviews.view;

import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * @author Amanpal Singh.
 */

public interface OwnerDashboardView extends BaseView {
    Toolbar getToolbarView();

    DrawerLayout getDrawerView();

    void backPress();

    ActionBarDrawerToggle getDrawerToggleView();

    NavigationView getNavigationView();

    TextView getToolbarTitleView();
}
