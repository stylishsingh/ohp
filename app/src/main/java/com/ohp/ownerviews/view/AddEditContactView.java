package com.ohp.ownerviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by vishal.sharma on 8/31/2017.
 */

public interface AddEditContactView extends BaseView {
    TextInputEditText getName();


    TextInputEditText getEmail();

    TextInputEditText getDesignnatiom();

    TextInputEditText getContactNumber();

    TextView getToolbarTitle();

    View getRootView();

    Toolbar getToolbar();

    AppCompatSpinner getCategory();

    TextInputLayout getNameTitle();

    TextInputLayout getEmailTitle();

    TextInputLayout getDesignationTitle();

    TextInputLayout getContactNumberTitle();

    TextView getCategoryTitle();
}

