package com.ohp.ownerviews.view;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by vishal.sharma on 7/18/2017.
 */

public interface AllUpcomingProjectsView extends BaseView {
    View getRootView();

    Toolbar getToolbar();

    NestedScrollView getNSRecyclerView();

    TextView getToolbarTitle();

    RecyclerView getUpcominglist();

    LinearLayout getCloseBtn();

    RelativeLayout getEmptyView();
    EditText getSearchEditText();


}
