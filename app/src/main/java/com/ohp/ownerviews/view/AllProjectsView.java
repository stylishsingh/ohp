package com.ohp.ownerviews.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.library.mvp.BaseView;

/**
 * @author Amanpal Singh.
 */

public interface AllProjectsView extends BaseView {

    RecyclerView getOnGoingRecyclerView();

    RecyclerView getCompletedRecyclerView();

    RecyclerView getUpComingRecyclerView();

    View getRootView();

}
