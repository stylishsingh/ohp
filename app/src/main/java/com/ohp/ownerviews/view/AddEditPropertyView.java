package com.ohp.ownerviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by Anuj Sharma on 5/26/2017.
 */

public interface AddEditPropertyView extends BaseView {
    View getRootView();

    Toolbar getToolbar();

    NestedScrollView getScrollView();

    TextView getToolbarTitle();

    TextView getResourcesView();

    TextView gettvPropertyView();

    TextView gettvInfo();

    ImageView getPropertyImage();

    ImageView getAddIconView();

    RecyclerView getResourceRecycler();

    TextInputEditText getPropertyName();

    TextInputEditText getPropertyArea();

    TextInputEditText getPropertyConstYear();

    TextInputEditText getAddress();

    TextInputEditText getDescription();

    MenuItem getEditMenu();

    MenuItem getSaveMenu();

    String getScreenCheck();

    String getPropertyID();

    TextInputLayout getPropertyNameLayoutView();

    TextInputLayout getPropertyAddressLayoutView();

    TextInputLayout getPropertyDescriptionLayoutView();

    TextView getPropertyNameTextView();

    TextView getPropertyAddressTextView();

    TextView getPropertyDescriptionTextView();

    TextView getDocumentCountView();

    TextView getProjectCountView();

    TextView getApplianceCountView();

    TextView getRemindersCountView();

    View getLlPropertyNameView();

    View getLlPropertyAddressView();

    View getLlPropertyDescriptionView();

    View getLlAdditionalInfoView();

}
