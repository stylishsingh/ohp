package com.ohp.ownerviews.view;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by vishal.sharma on 7/25/2017.
 */

public interface ProjectDetailPropertyView extends BaseView {
    View getRootView();
    TextView getOngoing();
    TextView getupcoming();
    TextView getpast();
    TextView getNoOngoing();
    TextView getNoupcoming();
    TextView getNopast();
    RecyclerView getUpcomingList();
    RecyclerView getOngoingList();
    RecyclerView getPastList();

    TextView getToolbarTitle();
    Toolbar getToolbar();
}
