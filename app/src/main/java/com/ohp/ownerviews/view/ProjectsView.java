package com.ohp.ownerviews.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.library.mvp.BaseView;

/**
 * Created by root on 17/7/17.
 */

public interface ProjectsView extends BaseView {

    View getRootView();

    RecyclerView getRecyclerView();

}
