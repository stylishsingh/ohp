package com.ohp.ownerviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by Anuj Sharma on 6/6/2017.
 */

public interface AddEditDocumentView extends BaseView {
    View getRootView();

    Toolbar getToolbar();

    TextView getToolbarTitle();

    TextView getHeaderAppliance();

    TextView getDocImagesHeaderView();

    TextView tvHeaderPropertyImage();

    TextView getHeaderProperty();

    LinearLayout getDetailTopContainer();

    ImageView getPropertyImage();

    ProgressBar getImageProgress();

    AppCompatSpinner getPropertySpinner();

    LinearLayout getApplianceSpinnerView();

    AppCompatSpinner getApplianceSpinner();

    TextInputEditText getDocumentName();

    TextInputEditText getDocumentNameDetail();

    TextInputEditText getCreationDate();

    RecyclerView getDocumentRecycler();

    MenuItem getSaveMenu();

    MenuItem getEditMenu();

    TextInputLayout getTilCreatedOnView();

    TextInputLayout getTilDocumentNameDetailView();

    TextInputLayout getTilDocumentNameView();

    View getApplianceLineView();

    View getPropertyLineView();


}
