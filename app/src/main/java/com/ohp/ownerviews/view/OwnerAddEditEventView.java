package com.ohp.ownerviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * @author Amanpal Singh.
 */

public interface OwnerAddEditEventView extends BaseView {

    NestedScrollView getNestedScrollView();

    TextInputEditText getEtEventTitleView();

    TextInputEditText getEtEventTitleDetailView();

    TextInputEditText getEtAddNotesView();

    TextInputEditText getEtNotesDetailView();

    TextInputEditText getEtDateTimeFromView();

    TextInputEditText getEtDateTimeToView();

    TextInputEditText getEtReminderView();

    AppCompatSpinner getSpinnerPropertyView();

    AppCompatSpinner getSpinnerApplianceView();

    AppCompatSpinner getSpinnerProviderView();

    AppCompatSpinner getSpinnerProjectView();


    AppCompatSpinner getSpinnerRepeatEventView();

    View getRootView();

    String getCurrentScreen();

    int getEventType();

    Toolbar getToolbar();

    TextView getToolbarTitleView();

    TextView getHeaderPropertyNameView();

    TextView getHeaderProviderNameView();

    TextView getHeaderProjectNameView();

    TextView getHeaderApplianceNameView();

    TextView getHeaderEventRepeatNameView();

    TextView getHeaderRepeatReminderName();

    TextView getRepeatReminderView();

    TextView getTVHeaderEventFromTimeView();

    TextView getTVEventFromTimeView();

    TextView getTVHeaderEventToView();

    TextView getTVEventToTimeView();

    TextInputLayout getTILEventTitleView();

    TextInputLayout getTILEventTitleDetailView();

    TextInputLayout getTILNotesView();

    TextInputLayout getTILNotesDetailView();

    View getProviderNameLineView();

    View getProjectNameLineView();

    View getApplianceNameLineView();

    View getPropertyNameLineView();

    View getRepeatEventLineView();

    View getReminderTimeLineView();

    View getEventFromLineView();

    View getEventToLineView();


}
