package com.ohp.ownerviews.view;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.library.mvp.BaseView;

/**
 * @author Amanpal Singh.
 */

public interface PropertiesListView extends BaseView {

    RecyclerView getRecyclerView();

    NestedScrollView getNSRecyclerView();

    View getRootView();

    RelativeLayout getEmptyView();

    EditText getSearchEditText();

    LinearLayout getCloseBtn();

}
