package com.ohp.ownerviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * @author Amanpal Singh.
 */

public interface AddEditProjectView extends BaseView{
    View getRootView();
    Toolbar getToolbar();

    TextView getToolbarTitle();

    LinearLayout getDetailTopContainer();
    LinearLayout getEditViewContainer();
    LinearLayout getDetailViewContainer();

    RecyclerView getRecyclerView();

    TextInputEditText getProjectName();
    TextInputEditText getProjectDesc();
    AppCompatSpinner getPropertySpinner();
    TextInputEditText getApplianceName();
    TextInputEditText getJobType();
    TextInputEditText getExpertise();

    TextInputEditText getPropertyNameDetail();
    TextInputEditText getProjectNameDetail();
    TextView getProjectDescDetail();
    TextInputEditText getCreatedDetail();
    TextInputEditText getJobTypeDetail();
    TextInputEditText getExpertiseDetail();
}
