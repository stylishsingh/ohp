package com.ohp.ownerviews.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.library.mvp.BaseView;

/**
 * Created by vishal.sharma on 7/13/2017.
 */

public interface ProjectListView extends BaseView {
    RecyclerView getRecyclerView();

    View getRootView();

    RelativeLayout getEmptyView();

    EditText getSearchEditText();
    LinearLayout getCloseBtn();
}
