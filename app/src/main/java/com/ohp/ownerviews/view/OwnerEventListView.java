package com.ohp.ownerviews.view;

import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.library.mvp.BaseView;
import com.roomorama.caldroid.CaldroidFragment;

/**
 * @author Amanpal Singh.
 */

public interface OwnerEventListView extends BaseView {

    void setCaldroid(CaldroidFragment fragment);

    View getRootView();

    CaldroidFragment getCaldroidFragmentView();

    RecyclerView getRecyclerView();

    LinearLayoutManager getLinearLayoutManagerView();

    View getEmptyView();

    TextView getTVTodayView();

    TextView getTVTimeFromView();

    TextView getTVTimeFromToView();

    TextView getTVEventTitleView();

    TextView getTVNotesView();

    TextView getTVRepeatView();

    TextView getTVPropertyNameView();

    TextView getTVProjectNameView();

    CardView getCVCurrentDateView();

    String getNavigateFrom();

    String getPropertyID();

    Toolbar getToolbarView();

    TextView getToolbarTitle();

    LinearLayout getLLCalendarView();
}
