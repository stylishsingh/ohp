package com.ohp.ownerviews.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.ownerviews.presenter.AddEditAppliancePresenterImpl;
import com.ohp.ownerviews.view.AddEditApplianceView;
import com.ohp.utils.Constants;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * @author Amanpal Singh.
 */

public class AddEditApplianceFragment extends BaseFragment<AddEditAppliancePresenterImpl> implements AddEditApplianceView {
    private View rootView;
    private Toolbar mToolbar;
    private TextView toolbarTitle, tvHeaderPropertyName, tvHeaderPropertyImage,
            tvHeaderApplianceImage, tvHeaderApplianceDoc, tvHeaderBrandName, tvHeaderApplianceType;
    private AppCompatSpinner spinnerPropertyName, spinnerBrandName, spinnerApplianceType;
    private TextInputEditText etApplianceName, etPurchaseDate,
            etWarrantyDate, etExtendWarrantyDate, etApplianceNameDetail, etModelName, etSerialNumber, etModelNameDetail, etSerialNumberDetail;
    private RecyclerView recyclerAddImages, recyclerAddDocs;
    private LinearLayout detailTopView, detailImageContainer, editImageContainer;
    private ImageView propertyImg;
    private ProgressBar progress;
    private ViewPager vpApplianceImg, vpApplianceDocs;
    private CirclePageIndicator indicatorImg, indicatorDocs;
    private String currentScreen = "";
    private MenuItem editMenu, saveMenu;
    private TextInputLayout tilApplianceName, tilApplianceNameDetail, tilModelName, tilSerialNumber, tilModelNameDetail, tilSerialNumberDetail;
    private View applianceTypeView, propertyNameView, brandNameView;

    @Override
    protected AddEditAppliancePresenterImpl onAttachPresenter() {
        return new AddEditAppliancePresenterImpl(this, getActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_appliance, menu);
        editMenu = menu.findItem(R.id.appliance_edit);
        saveMenu = menu.findItem(R.id.appliance_save);
        switch (currentScreen) {
            case Constants.CLICK_VIEW_APPLIANCE:
                editMenu.setVisible(true);
                saveMenu.setVisible(false);
                break;
            case Constants.CLICK_ADD_APPLIANCE:
                editMenu.setVisible(false);
                saveMenu.setVisible(true);
                break;

            case Constants.ADD_EDIT_PROPERTY:
                editMenu.setVisible(false);
                saveMenu.setVisible(true);
                break;
            case Constants.CLICK_EDIT_APPLIANCE:
                editMenu.setVisible(false);
                saveMenu.setVisible(true);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (getActivity() instanceof HolderActivity) {
            ((HolderActivity) getActivity()).hideKeyboard();
        }
        switch (item.getItemId()) {
            case R.id.appliance_edit:
                editMenu.setVisible(false);
                saveMenu.setVisible(true);
                getPresenter().enableDisableUI(true);
                getPresenter().showHideDetailView(false);
                getToolbarTitle().setText(R.string.title_edit_appliance);
                break;
            case R.id.appliance_save:
//                Utils.getInstance().showToast("save clicked");
                if (getPresenter().validateApplianceInfo()) {

                }
                break;
        }
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_edit_appliance, container, false);
        return rootView;
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((HolderActivity) getActivity()).setSupportActionBar(mToolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        tvHeaderPropertyImage = (TextView) view.findViewById(R.id.tvHeaderPropertyImage);
        tvHeaderPropertyName = (TextView) view.findViewById(R.id.tvHeaderPropertyName);
        tvHeaderApplianceImage = (TextView) view.findViewById(R.id.tvApplianceImage);
        tvHeaderApplianceDoc = (TextView) view.findViewById(R.id.tvApplianceDoc);
        tvHeaderBrandName = (TextView) view.findViewById(R.id.tvHeaderBrandName);
        tvHeaderApplianceType = (TextView) view.findViewById(R.id.tvHeaderApplianceType);
        spinnerBrandName = (AppCompatSpinner) view.findViewById(R.id.spinner_brand_name);
        spinnerApplianceType = (AppCompatSpinner) view.findViewById(R.id.spinner_appliance_type);
        applianceTypeView = view.findViewById(R.id.appliance_type_view);
        propertyNameView = view.findViewById(R.id.property_name_view);
        brandNameView = view.findViewById(R.id.brand_name_view);

        detailTopView = (LinearLayout) view.findViewById(R.id.detail_top_container);
        editImageContainer = (LinearLayout) view.findViewById(R.id.appliance_editimage_container);
        detailImageContainer = (LinearLayout) view.findViewById(R.id.appliance_detailimage_container);
        propertyImg = (ImageView) view.findViewById(R.id.avatar);
        progress = (ProgressBar) view.findViewById(R.id.progress);
        vpApplianceImg = (ViewPager) view.findViewById(R.id.viewpager_image);
        vpApplianceDocs = (ViewPager) view.findViewById(R.id.viewpager_docs);
        indicatorImg = (CirclePageIndicator) view.findViewById(R.id.indicator_img);
        indicatorDocs = (CirclePageIndicator) view.findViewById(R.id.indicator_docs);

        tilApplianceName = (TextInputLayout) view.findViewById(R.id.til_appliance_name);
        tilApplianceNameDetail = (TextInputLayout) view.findViewById(R.id.til_appliance_name_detail);
        tilModelName = (TextInputLayout) view.findViewById(R.id.til_model_name);
        tilModelNameDetail = (TextInputLayout) view.findViewById(R.id.til_model_name_detail);
        tilSerialNumber = (TextInputLayout) view.findViewById(R.id.til_serial_number);
        tilSerialNumberDetail = (TextInputLayout) view.findViewById(R.id.til_serial_number_detail);

        spinnerPropertyName = (AppCompatSpinner) view.findViewById(R.id.spinner_property_name);
        etApplianceName = (TextInputEditText) view.findViewById(R.id.et_appliance_name);
        etApplianceNameDetail = (TextInputEditText) view.findViewById(R.id.et_appliance_name_detail);
        etPurchaseDate = (TextInputEditText) view.findViewById(R.id.et_purchase_date);
        etWarrantyDate = (TextInputEditText) view.findViewById(R.id.et_warranty_expire_date);
        etExtendWarrantyDate = (TextInputEditText) view.findViewById(R.id.et_extend_warranty_date);
        etModelName = (TextInputEditText) view.findViewById(R.id.et_model_name);
        etModelNameDetail = (TextInputEditText) view.findViewById(R.id.et_model_name_detail);
        etSerialNumber = (TextInputEditText) view.findViewById(R.id.et_serial_number);
        etSerialNumberDetail = (TextInputEditText) view.findViewById(R.id.et_serial_number_detail);

        recyclerAddImages = (RecyclerView) view.findViewById(R.id.recycler_appliance_images);
        recyclerAddDocs = (RecyclerView) view.findViewById(R.id.recycler_appliance_documents);

        //set click listeners
        etPurchaseDate.setOnClickListener(getPresenter());
        etWarrantyDate.setOnClickListener(getPresenter());
        spinnerPropertyName.setOnItemSelectedListener(getPresenter());
        spinnerBrandName.setOnItemSelectedListener(getPresenter());
        spinnerApplianceType.setOnItemSelectedListener(getPresenter());
        spinnerBrandName.setOnTouchListener(getPresenter());
        spinnerApplianceType.setOnTouchListener(getPresenter());
        propertyImg.setOnClickListener(getPresenter());

        //fetch arguments
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            currentScreen = bundle.getString(Constants.DESTINATION, "");
            getPresenter().saveBundleInfo(bundle);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        getPresenter().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public TextView getHeaderPropertyNameView() {
        return tvHeaderPropertyName;
    }

    @Override
    public TextView getHeaderPropertyImageView() {
        return tvHeaderPropertyImage;
    }

    @Override
    public TextView getHeaderApplianceImageView() {
        return tvHeaderApplianceImage;
    }

    @Override
    public TextView getHeaderApplianceDocView() {
        return tvHeaderApplianceDoc;
    }

    @Override
    public TextView getHeaderBrandNameView() {
        return tvHeaderBrandName;
    }

    @Override
    public TextView getHeaderApplianceTypeView() {
        return tvHeaderApplianceType;
    }

    @Override
    public LinearLayout getDetailTopContainer() {
        return detailTopView;
    }

    @Override
    public ImageView getPropertyImage() {
        return propertyImg;
    }

    @Override
    public ProgressBar getImageProgress() {
        return progress;
    }

    @Override
    public LinearLayout getEditImageContainer() {
        return editImageContainer;
    }

    @Override
    public LinearLayout getDetailImageContainer() {
        return detailImageContainer;
    }

    @Override
    public ViewPager getImageViewPager() {
        return vpApplianceImg;
    }

    @Override
    public ViewPager getDocsViewPager() {
        return vpApplianceDocs;
    }

    @Override
    public CirclePageIndicator getImgIndicator() {
        return indicatorImg;
    }

    @Override
    public CirclePageIndicator getDocsIndicator() {
        return indicatorDocs;
    }

    @Override
    public AppCompatSpinner getPropertySpinner() {
        return spinnerPropertyName;
    }

    @Override
    public AppCompatSpinner getSpinnerBrandNameView() {
        return spinnerBrandName;
    }

    @Override
    public AppCompatSpinner getSpinnerApplianceTypeView() {
        return spinnerApplianceType;
    }

    @Override
    public TextInputEditText getApplianceName() {
        return etApplianceName;
    }

    @Override
    public TextInputEditText getApplianceNameDetail() {
        return etApplianceNameDetail;
    }

    @Override
    public TextInputEditText getModelName() {
        return etModelName;
    }

    @Override
    public TextInputEditText getModelNameDetail() {
        return etModelNameDetail;
    }

    @Override
    public TextInputEditText getSerialNumber() {
        return etSerialNumber;
    }

    @Override
    public TextInputEditText getSerialNumberDetail() {
        return etSerialNumberDetail;
    }


    @Override
    public TextInputEditText getPurchaseDate() {
        return etPurchaseDate;
    }

    @Override
    public TextInputEditText getWarrantyExpDate() {
        return etWarrantyDate;
    }


    @Override
    public TextInputEditText getExtendedWarrantyExpDate() {
        return etExtendWarrantyDate;
    }

    @Override
    public RecyclerView getApplianceImageRecycler() {
        return recyclerAddImages;
    }

    @Override
    public RecyclerView getApplianceDocsRecycler() {
        return recyclerAddDocs;
    }

    @Override
    public MenuItem getEditMenuView() {
        return editMenu;
    }

    @Override
    public MenuItem getSaveMenuView() {
        return saveMenu;
    }

    @Override
    public String getScreenCheck() {
        return currentScreen;
    }

    @Override
    public TextInputLayout getTILApplianceView() {
        return tilApplianceName;
    }

    @Override
    public TextInputLayout getTILApplianceDetailView() {
        return tilApplianceNameDetail;
    }

    @Override
    public TextInputLayout getTILModelNameView() {
        return tilModelName;
    }

    @Override
    public TextInputLayout getTILModelNameDetailView() {
        return tilModelNameDetail;
    }

    @Override
    public TextInputLayout getTILSerialNumberView() {
        return tilSerialNumber;
    }

    @Override
    public TextInputLayout getTILSerialNumberDetailView() {
        return tilSerialNumberDetail;
    }

    @Override
    public View getApplianceTypeLineView() {
        return applianceTypeView;
    }

    @Override
    public View getPropertyNameLineView() {
        return propertyNameView;
    }

    @Override
    public View getBrandNameLineView() {
        return brandNameView;
    }

}
