package com.ohp.ownerviews.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.ownerviews.presenter.AddEditPropertyFragmentPresenterImpl;
import com.ohp.ownerviews.view.AddEditPropertyView;
import com.ohp.utils.Constants;

/**
 * @author Amanpal Singh
 */

public class AddEditPropertyFragment extends BaseFragment<AddEditPropertyFragmentPresenterImpl> implements AddEditPropertyView {
    private View rootView;
    private Toolbar mToolbar;
    private TextView toolbarTitle, tvPropertyName, tvPropertyAddress, tvPropertyDescription,
            tvDocumentCount, tvProjectCount, tvApplianceCount, tvRemindersCount, tvHeaderPropertyImage, tvResources, tvAdditionalInfo;
    private NestedScrollView propertyNestedScroll;
    private ImageView propertyImage, ivAddIcon, ivAddDocuments, ivAddAppliances, ivAddProjects, ivAddReminders;
    private TextInputEditText etPropertyName, etArea, etConstructionYear, etSearchAddress, etDescription;
    private RecyclerView resourceRecycler;
    private String currentScreen = "", propertyID = "";
    private MenuItem editMenu, saveMenu;
    private TextInputLayout tilPropertyName, tilPropertyAddress, tilPropertyDescription;
    private View llPropertyName, llPropertyAddress, llPropertyDescription, llAdditionalInformation, llDocumentInfo, llApplianceInfo, llEventInfo, llProjectInfo;

    @Override
    protected AddEditPropertyFragmentPresenterImpl onAttachPresenter() {
        return new AddEditPropertyFragmentPresenterImpl(this, getActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            currentScreen = getArguments().getString(Constants.DESTINATION, "");
            switch (currentScreen) {
                case Constants.CLICK_EDIT_PROPERTY:
                    propertyID = getArguments().getString(Constants.PROPERTY_ID, "");
                    break;
                case Constants.CLICK_VIEW_PROPERTY:
                    propertyID = getArguments().getString(Constants.PROPERTY_ID, "");
                    break;
            }
        }
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_property, menu);
        editMenu = menu.findItem(R.id.profile_edit);
        saveMenu = menu.findItem(R.id.profile_save);

        //check if view has to be edit mode or save mode
        if (editMenu != null && saveMenu != null) {
            switch (currentScreen) {
                case Constants.CLICK_ADD_PROPERTY:
                    saveMenu.setVisible(true);
                    editMenu.setVisible(false);
                    break;
                case Constants.CLICK_EDIT_PROPERTY:
                    saveMenu.setVisible(true);
                    editMenu.setVisible(false);
                    break;
                case Constants.CLICK_VIEW_PROPERTY:
                    saveMenu.setVisible(false);
                    editMenu.setVisible(true);
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.profile_edit:
                toolbarTitle.setText(R.string.title_edit_property);
                //Make add view editable and show Save menu item
                editMenu.setVisible(false);
                saveMenu.setVisible(true);
                if (Constants.CLICK_VIEW_PROPERTY.equals(currentScreen)) {
                    getPresenter().enabledFields();
                }
                return true;
            case R.id.profile_save:

                if (Constants.CLICK_EDIT_PROPERTY.equals(currentScreen)) {
                    getPresenter().addEditProperty();
                } else if (Constants.CLICK_ADD_PROPERTY.equals(currentScreen)) {
                    getPresenter().addEditProperty();
                } else {
                    toolbarTitle.setText(R.string.title_property_detail);
                    // Hit api and make all view non-editable and show Edit menu item
                    getPresenter().addEditProperty();
                }
                return true;
        }
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_edit_property, container, false);
        return rootView;
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        tvHeaderPropertyImage = (TextView) view.findViewById(R.id.tvHeaderPropertyImage);
        tvResources = (TextView) view.findViewById(R.id.tvResources);
        tvAdditionalInfo = (TextView) view.findViewById(R.id.tvAdditionalInfo);
        resourceRecycler = (RecyclerView) view.findViewById(R.id.resource_recycler);
        propertyNestedScroll = (NestedScrollView) view.findViewById(R.id.property_nested_scroll);
        propertyImage = (ImageView) view.findViewById(R.id.avatar);
        ivAddIcon = (ImageView) view.findViewById(R.id.iv_add_icon);
        tilPropertyName = (TextInputLayout) view.findViewById(R.id.til_property_name);
        tilPropertyAddress = (TextInputLayout) view.findViewById(R.id.til_search_address);
        tilPropertyDescription = (TextInputLayout) view.findViewById(R.id.til_description);
        etPropertyName = (TextInputEditText) view.findViewById(R.id.et_property_name);
        etArea = (TextInputEditText) view.findViewById(R.id.et_property_area);
        etConstructionYear = (TextInputEditText) view.findViewById(R.id.et_construction_year);
        etSearchAddress = (TextInputEditText) view.findViewById(R.id.et_search_address);
        etDescription = (TextInputEditText) view.findViewById(R.id.et_description);

//        etConstructionYear.setOnClickListener(getPresenter());

        llPropertyName = view.findViewById(R.id.ll_property_name);
        llPropertyAddress = view.findViewById(R.id.ll_search_address);
        llPropertyDescription = view.findViewById(R.id.ll_description);
        llAdditionalInformation = view.findViewById(R.id.layout_additional_info);
        llDocumentInfo = view.findViewById(R.id.ll_document);
        llApplianceInfo = view.findViewById(R.id.ll_appliances);
        llProjectInfo=view.findViewById(R.id.allProjects);
        llEventInfo=view.findViewById(R.id.ll_events);

        tvPropertyName = (TextView) view.findViewById(R.id.tv_property_name);
        tvPropertyAddress = (TextView) view.findViewById(R.id.tv_search_address_name);
        tvPropertyDescription = (TextView) view.findViewById(R.id.tv_description_name);
        tvDocumentCount = (TextView) view.findViewById(R.id.tv_documents_count);
        tvProjectCount = (TextView) view.findViewById(R.id.tv_projects_count);
        tvApplianceCount = (TextView) view.findViewById(R.id.tv_appliances_count);
        tvRemindersCount = (TextView) view.findViewById(R.id.tv_reminder_count);

        /*Additional info view reference*/

        ivAddDocuments = (ImageView) view.findViewById(R.id.iv_add_documents);
        ivAddAppliances = (ImageView) view.findViewById(R.id.iv_add_appliances);
        ivAddProjects = (ImageView) view.findViewById(R.id.iv_add_projects);
        ivAddReminders = (ImageView) view.findViewById(R.id.iv_add_reminders);

        propertyImage.setOnClickListener(getPresenter());
        etSearchAddress.setOnClickListener(getPresenter());

        ivAddDocuments.setOnClickListener(getPresenter());
        ivAddAppliances.setOnClickListener(getPresenter());
        ivAddProjects.setOnClickListener(getPresenter());
        ivAddReminders.setOnClickListener(getPresenter());
        ivAddIcon.setOnClickListener(getPresenter());
        llDocumentInfo.setOnClickListener(getPresenter());
        llApplianceInfo.setOnClickListener(getPresenter());
        llProjectInfo.setOnClickListener(getPresenter());
        llEventInfo.setOnClickListener(getPresenter());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        getPresenter().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public NestedScrollView getScrollView() {
        return propertyNestedScroll;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public TextView getResourcesView() {
        return tvResources;
    }

    @Override
    public TextView gettvPropertyView() {
        return tvHeaderPropertyImage;
    }

    @Override
    public TextView gettvInfo() {
        return tvAdditionalInfo;
    }

    @Override
    public ImageView getPropertyImage() {
        return propertyImage;
    }


    @Override
    public ImageView getAddIconView() {
        return ivAddIcon;
    }

    @Override
    public RecyclerView getResourceRecycler() {
        return resourceRecycler;
    }

    @Override
    public TextInputEditText getPropertyName() {
        return etPropertyName;
    }

    @Override
    public TextInputEditText getPropertyArea() {
        return etArea;
    }

    @Override
    public TextInputEditText getPropertyConstYear() {
        return etConstructionYear;
    }

    @Override
    public TextInputEditText getAddress() {
        return etSearchAddress;
    }

    @Override
    public TextInputEditText getDescription() {
        return etDescription;
    }

    @Override
    public MenuItem getEditMenu() {
        return editMenu;
    }

    @Override
    public MenuItem getSaveMenu() {
        return saveMenu;
    }

    @Override
    public String getScreenCheck() {
        return currentScreen;
    }

    @Override
    public String getPropertyID() {
        return propertyID;
    }

    @Override
    public TextInputLayout getPropertyNameLayoutView() {
        return tilPropertyName;
    }

    @Override
    public TextInputLayout getPropertyAddressLayoutView() {
        return tilPropertyAddress;
    }

    @Override
    public TextInputLayout getPropertyDescriptionLayoutView() {
        return tilPropertyDescription;
    }

    @Override
    public TextView getPropertyNameTextView() {
        return tvPropertyName;
    }

    @Override
    public TextView getPropertyAddressTextView() {
        return tvPropertyAddress;
    }

    @Override
    public TextView getPropertyDescriptionTextView() {
        return tvPropertyDescription;
    }

    @Override
    public TextView getDocumentCountView() {
        return tvDocumentCount;
    }

    @Override
    public TextView getProjectCountView() {
        return tvProjectCount;
    }

    @Override
    public TextView getApplianceCountView() {
        return tvApplianceCount;
    }

    @Override
    public TextView getRemindersCountView() {
        return tvRemindersCount;
    }

    @Override
    public View getLlPropertyNameView() {
        return llPropertyName;
    }

    @Override
    public View getLlPropertyAddressView() {
        return llPropertyAddress;
    }

    @Override
    public View getLlPropertyDescriptionView() {
        return llPropertyDescription;
    }

    @Override
    public View getLlAdditionalInfoView() {
        return llAdditionalInformation;
    }

}