package com.ohp.ownerviews.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.ownerviews.presenter.AllProjectsListFragmentPresenterImpl;
import com.ohp.ownerviews.view.AllProjectsDetailView;
import com.ohp.utils.Constants;

/**
 * @author Amanpal Singh.
 */

public class AllProjectsFragments extends BaseFragment<AllProjectsListFragmentPresenterImpl> implements AllProjectsDetailView {

    private View rootView;
    private RecyclerView recyclerView;
    private FloatingActionButton fabAdd;
    private RelativeLayout emptyView;
    private EditText etSearch;
    private LinearLayout btnClear;
    private NestedScrollView nsRecyclerView;
    private String propertyId = "", navigateFrom = "";
    private Toolbar mToolbar;
    private TextView toolbarTitle;
    private boolean isMenu = false;

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getArguments() != null) {
            if (getArguments().getString(Constants.PROPERTY_ID) != null) {
                propertyId = getArguments().getString(Constants.PROPERTY_ID);
                isMenu = true;
            } else {
                isMenu = false;
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (isMenu) {
            menu.clear();
            inflater.inflate(R.menu.menu_filter_projects, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_filter_projects:
                showPopup();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_all_projects, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    /**
     * Filter view of project list which will be shown  after tap on filter icon.
     */
    public void showPopup() {
        try {
            PopupMenu popup;
            if (isMenu)
                popup = new PopupMenu(getActivity(), getActivity().findViewById(R.id.item_filter_projects));
            else popup = new PopupMenu(getActivity(), getActivity().findViewById(R.id.item_filter));
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.menu_project_list, popup.getMenu());
            popup.getMenu().findItem(R.id.action_current).setChecked(false);
            popup.getMenu().findItem(R.id.action_completed).setChecked(false);
            popup.getMenu().findItem(R.id.action_all).setChecked(false);
            switch (getPresenter().projectType) {
                case "current":
                    popup.getMenu().findItem(R.id.action_current).setChecked(true);
                    break;
                case "completed":
                    popup.getMenu().findItem(R.id.action_completed).setChecked(true);
                    break;
                case "":
                    popup.getMenu().findItem(R.id.action_all).setChecked(true);
                    break;
            }
            popup.show();
            popup.setOnMenuItemClickListener(getPresenter());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setProjectType() {
        getPresenter().projectType = "";
        getPresenter().getAllProjects();
    }

    @Override
    protected AllProjectsListFragmentPresenterImpl onAttachPresenter() {
        return new AllProjectsListFragmentPresenterImpl(this, getActivity());
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        fabAdd = (FloatingActionButton) view.findViewById(R.id.fab_add);
        nsRecyclerView = (NestedScrollView) view.findViewById(R.id.ns_recycler_view);
        emptyView = (RelativeLayout) view.findViewById(R.id.empty_list_layout);
        etSearch = (EditText) view.findViewById(R.id.et_search);
        btnClear = (LinearLayout) view.findViewById(R.id.btn_clear);
        btnClear.setOnClickListener(getPresenter());
        fabAdd.setOnClickListener(getPresenter());
        recyclerView.setAdapter(getPresenter().initAdapter());
        if (getArguments() != null) {
            if (getArguments().getString(Constants.PROPERTY_ID) != null) {
                propertyId = getArguments().getString(Constants.PROPERTY_ID);
                getPresenter().setToolbar();
                mToolbar.setVisibility(View.VISIBLE);
            } else {
                mToolbar.setVisibility(View.GONE);
            }
        } else {
            mToolbar.setVisibility(View.GONE);
        }
    }

    @Override
    public String propertyId() {
        return propertyId;
    }

    @Override
    public String navigateFrom() {
        return navigateFrom;
    }

    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public NestedScrollView getNSRecyclerView() {
        return nsRecyclerView;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public RelativeLayout getEmptyView() {
        return emptyView;
    }

    @Override
    public EditText getSearchEditText() {
        return etSearch;
    }

    @Override
    public LinearLayout getCloseBtn() {
        return btnClear;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }
}
