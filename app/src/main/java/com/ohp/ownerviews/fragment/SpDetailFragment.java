package com.ohp.ownerviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.ownerviews.presenter.SpDetailFragmentPresenterImpl;
import com.ohp.ownerviews.view.SpDetailFragmentView;

/**
 * @author Amanpal Singh.
 */

public class SpDetailFragment extends BaseFragment<SpDetailFragmentPresenterImpl> implements SpDetailFragmentView {

    private View rootView;
    private Toolbar mToolbar;
    private TextView toolbarTitle, tvMiles, tvProviderName, tvViewReviews, tvBusinessName, tvBusinessAddress, tvWebsite,
            tvNotes, tvInsured, tvLicensed, tvBonded, tvBidAmount, tvExpertise;
    private ImageView ivAccept, ivReject, ivAvatar;
    private View viewBidAcceptReject, viewNotes, viewExpertise;
    private RatingBar rbProvider;

    @Override
    protected SpDetailFragmentPresenterImpl onAttachPresenter() {
        return new SpDetailFragmentPresenterImpl(this, getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sp_detail, container, false);
        return rootView;
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((HolderActivity) getActivity()).setSupportActionBar(mToolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        tvProviderName = (TextView) view.findViewById(R.id.tv_provider_name);
        tvProviderName.setOnClickListener(getPresenter());
        tvMiles = (TextView) view.findViewById(R.id.tv_miles);

        rbProvider = (RatingBar) view.findViewById(R.id.rating);
        tvViewReviews = (TextView) view.findViewById(R.id.tv_view_reviews);
        tvBidAmount = (TextView) view.findViewById(R.id.tv_bid_amount);
        tvBusinessName = (TextView) view.findViewById(R.id.tv_business_name);
        tvBusinessAddress = (TextView) view.findViewById(R.id.tv_business_address);
        tvWebsite = (TextView) view.findViewById(R.id.tv_website);
        tvLicensed = (TextView) view.findViewById(R.id.tv_licensed);
        tvInsured = (TextView) view.findViewById(R.id.tv_insured);
        tvBonded = (TextView) view.findViewById(R.id.tv_bonded);
        tvNotes = (TextView) view.findViewById(R.id.tv_notes);
        tvExpertise = (TextView) view.findViewById(R.id.tv_expertise);
        ivAccept = (ImageView) view.findViewById(R.id.iv_accept);
        ivReject = (ImageView) view.findViewById(R.id.iv_reject);
        ivAvatar = (ImageView) view.findViewById(R.id.avatar);
        viewBidAcceptReject = view.findViewById(R.id.bid_accept_reject);
        viewNotes = view.findViewById(R.id.ll_notes);
        viewExpertise = view.findViewById(R.id.ll_expertise);
        ivReject.setOnClickListener(getPresenter());
        ivAccept.setOnClickListener(getPresenter());
        ivAvatar.setOnClickListener(getPresenter());
        tvViewReviews.setOnClickListener(getPresenter());

        if (getArguments() != null) {
            getPresenter().saveBundle(getArguments());
        }
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public TextView getTVMilesView() {
        return tvMiles;
    }

    @Override
    public TextView getTVProviderNameView() {
        return tvProviderName;
    }

    @Override
    public TextView getTVViewReviews() {
        return tvViewReviews;
    }

    @Override
    public TextView getTVBusinessNameView() {
        return tvBusinessName;
    }

    @Override
    public TextView getTVBusinessAddressView() {
        return tvBusinessAddress;
    }

    @Override
    public TextView getTVWebsiteView() {
        return tvWebsite;
    }

    @Override
    public TextView getTVNotesView() {
        return tvNotes;
    }

    @Override
    public TextView getTVExpertiseView() {
        return tvExpertise;
    }

    @Override
    public TextView getTVInsuredView() {
        return tvInsured;
    }

    @Override
    public TextView getTVLicensedView() {
        return tvLicensed;
    }

    @Override
    public TextView getTVBondedView() {
        return tvBonded;
    }

    @Override
    public TextView getTVBidAmountView() {
        return tvBidAmount;
    }

    @Override
    public ImageView getIVAcceptView() {
        return ivAccept;
    }

    @Override
    public ImageView getIVRejectView() {
        return ivReject;
    }

    @Override
    public ImageView getIVAvatar() {
        return ivAvatar;
    }

    @Override
    public View getParentBidAcceptRejectView() {
        return viewBidAcceptReject;
    }

    @Override
    public View getParentNotesView() {
        return viewNotes;
    }

    @Override
    public View getParentExpertiseView() {
        return viewExpertise;
    }

    @Override
    public RatingBar getRBProviderView() {
        return rbProvider;
    }
}
