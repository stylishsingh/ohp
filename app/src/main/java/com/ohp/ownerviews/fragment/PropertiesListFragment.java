package com.ohp.ownerviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.presenter.PropertiesListFragmentPresenterImpl;
import com.ohp.ownerviews.view.PropertiesListView;

public class PropertiesListFragment extends BaseFragment<PropertiesListFragmentPresenterImpl> implements PropertiesListView {

    private RecyclerView recyclerView;
    private RelativeLayout emptyView;
    private EditText etSearch;
    private LinearLayout btnClear;
    private NestedScrollView nsRecyclerView;
    private View rootView;

    private FloatingActionButton fabAddProperties;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setHasOptionsMenu(true);
    }

    /**
     * Filter view of properties list which will be shown  after tap on filter icon.
     */
    public void showPopup() {
        final PopupMenu popup = new PopupMenu(getActivity(), getActivity().findViewById(R.id.item_filter));
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_filter, popup.getMenu());
        switch (OwnerDashboardActivity.sort_order) {
            case "ASC":
                popup.getMenu().findItem(R.id.action_asc).setChecked(true);
                break;
            case "DESC":
                popup.getMenu().findItem(R.id.action_des).setChecked(true);
                break;
            default:
                popup.getMenu().findItem(R.id.action_des).setChecked(true);
        }
        popup.show();
        popup.setOnMenuItemClickListener(getPresenter());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_properties, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected PropertiesListFragmentPresenterImpl onAttachPresenter() {
        return new PropertiesListFragmentPresenterImpl(this, getActivity());
    }

    @Override
    protected void initUI(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        nsRecyclerView = (NestedScrollView) view.findViewById(R.id.ns_recycler_view);
        emptyView = (RelativeLayout) view.findViewById(R.id.empty_list_layout);
        fabAddProperties = (FloatingActionButton) view.findViewById(R.id.fab_add_properties);
        etSearch = (EditText) view.findViewById(R.id.et_search);
        btnClear = (LinearLayout) view.findViewById(R.id.btn_clear);
        btnClear.setOnClickListener(getPresenter());
        fabAddProperties.setOnClickListener(getPresenter());
    }


    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public NestedScrollView getNSRecyclerView() {
        return nsRecyclerView;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public RelativeLayout getEmptyView() {
        return emptyView;
    }

    @Override
    public EditText getSearchEditText() {
        return etSearch;
    }

    @Override
    public LinearLayout getCloseBtn() {
        return btnClear;
    }
}
