package com.ohp.ownerviews.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.ownerviews.presenter.OwnerAddEditEventFragmentPresenterImpl;
import com.ohp.ownerviews.view.OwnerAddEditEventView;
import com.ohp.utils.Constants;

/**
 * @author Amanpal Singh.
 */

public class OwnerAddEditEventFragment extends BaseFragment<OwnerAddEditEventFragmentPresenterImpl> implements OwnerAddEditEventView {

    private View rootView;
    private TextInputEditText etEventTitle, etEventTitleDetail, etAddNotes, etNotesDetail, etDateTimeFrom, etDateTimeTo, etReminder;
    private AppCompatSpinner spinnerProperty, spinnerProject, spinnerAppliance, spinnerRepeatEvent, spinnerProvider;
    private String currentScreen = "";
    private NestedScrollView nestedScrollView;
    private MenuItem editMenu, saveMenu;
    private Toolbar mToolbar;
    private TextView toolbarTitle, tvHeaderPropertyName, tvHeaderProviderName, tvHeaderProjectName, tvHeaderApplianceName,
            tvHeaderEventRepeatName, tvHeaderRepeatReminderName, tvRepeatReminder, tvHeaderEventFromTime,
            tvEventFromTime, tvHeaderEventToTime, tvEventToTime;
    private TextInputLayout tilEventTitle, tilEventTitleDetail, tilNotes, tilNotesDetail;
    private View viewProviderName, viewProjectName, viewApplianceName, viewPropertyName, viewRepeatEvent,
            viewReminderTime, viewEventFrom, viewEventTo;
    private int eventType = 0;

    @Override
    protected OwnerAddEditEventFragmentPresenterImpl onAttachPresenter() {
        return new OwnerAddEditEventFragmentPresenterImpl(this, getActivity());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_owner_add_edit_event, container, false);
        return rootView;
    }


    @Override
    protected void initUI(View view) {

        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        tvHeaderProviderName = (TextView) view.findViewById(R.id.tvHeaderProviderName);
        tvHeaderPropertyName = (TextView) view.findViewById(R.id.tvHeaderPropertyName);
        tvHeaderProjectName = (TextView) view.findViewById(R.id.tvHeaderProjectName);
        tvHeaderApplianceName = (TextView) view.findViewById(R.id.tvHeaderApplianceName);
        tvHeaderEventRepeatName = (TextView) view.findViewById(R.id.label_repeat_event);
        tvHeaderRepeatReminderName = (TextView) view.findViewById(R.id.label_repeat_reminder_time);
        tvRepeatReminder = (TextView) view.findViewById(R.id.tv_repeat_reminder_time);
        tvHeaderEventFromTime = (TextView) view.findViewById(R.id.label_date_time_from);
        tvHeaderEventToTime = (TextView) view.findViewById(R.id.label_date_time_to);
        tvEventFromTime = (TextView) view.findViewById(R.id.tv_date_time_from);
        tvEventToTime = (TextView) view.findViewById(R.id.tv_date_time_to);

        viewProviderName = view.findViewById(R.id.view_provider_name);
        viewPropertyName = view.findViewById(R.id.view_property_name);
        viewProjectName = view.findViewById(R.id.view_project_name);
        viewApplianceName = view.findViewById(R.id.view_appliance_name);
        viewEventFrom = view.findViewById(R.id.view_event_from);
        viewEventTo = view.findViewById(R.id.view_event_to);
        viewReminderTime = view.findViewById(R.id.view_reminder_time);
        viewRepeatEvent = view.findViewById(R.id.view_repeat_event);

        nestedScrollView = (NestedScrollView) view.findViewById(R.id.nested_scroll_view);

        tilEventTitle = (TextInputLayout) view.findViewById(R.id.til_event_title);
        tilEventTitleDetail = (TextInputLayout) view.findViewById(R.id.til_event_title_detail);
        tilNotes = (TextInputLayout) view.findViewById(R.id.til_add_notes);
        tilNotesDetail = (TextInputLayout) view.findViewById(R.id.til_notes_detail);

        etEventTitle = (TextInputEditText) view.findViewById(R.id.et_event_title);
        etEventTitleDetail = (TextInputEditText) view.findViewById(R.id.et_event_title_detail);

        etAddNotes = (TextInputEditText) view.findViewById(R.id.et_add_notes);
        etNotesDetail = (TextInputEditText) view.findViewById(R.id.et_notes_detail);
        etDateTimeFrom = (TextInputEditText) view.findViewById(R.id.et_date_time_from);
        etDateTimeTo = (TextInputEditText) view.findViewById(R.id.et_date_time_to);
        etReminder = (TextInputEditText) view.findViewById(R.id.et_reminder);
        spinnerAppliance = (AppCompatSpinner) view.findViewById(R.id.spinner_appliance_name);
        spinnerProject = (AppCompatSpinner) view.findViewById(R.id.spinner_project_name);
        spinnerProperty = (AppCompatSpinner) view.findViewById(R.id.spinner_property_name);
        spinnerRepeatEvent = (AppCompatSpinner) view.findViewById(R.id.spinner_repeat_event);
        spinnerProvider = (AppCompatSpinner) view.findViewById(R.id.spinner_provider_name);

        spinnerAppliance.setOnItemSelectedListener(getPresenter());
        spinnerProject.setOnItemSelectedListener(getPresenter());
        spinnerProperty.setOnItemSelectedListener(getPresenter());
        spinnerRepeatEvent.setOnItemSelectedListener(getPresenter());
        spinnerProvider.setOnItemSelectedListener(getPresenter());
        etDateTimeFrom.setOnClickListener(getPresenter());
        etDateTimeTo.setOnClickListener(getPresenter());
        etReminder.setOnClickListener(getPresenter());
        tvRepeatReminder.setOnClickListener(getPresenter());
        tvEventFromTime.setOnClickListener(getPresenter());
        tvEventToTime.setOnClickListener(getPresenter());

        /*Touch Listener*/
        tvRepeatReminder.setOnTouchListener(getPresenter());
        tvEventFromTime.setOnTouchListener(getPresenter());
        tvEventToTime.setOnTouchListener(getPresenter());


        if (getArguments() != null) {
            getPresenter().saveBundleInfo(getArguments());
            currentScreen = getArguments().getString(Constants.DESTINATION, "");
            eventType = getArguments().getInt(Constants.EVENT_TYPE);
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_event, menu);
        editMenu = menu.findItem(R.id.event_edit);
        saveMenu = menu.findItem(R.id.event_save);
        if (eventType != 2)
            switch (currentScreen) {
                case Constants.CLICK_OWNER_VIEW_EVENT:
                    editMenu.setVisible(true);
                    saveMenu.setVisible(false);
                    break;
                case Constants.CLICK_OWNER_ADD_EVENT:
                    editMenu.setVisible(false);
                    saveMenu.setVisible(true);
                    break;
                case Constants.ADD_EDIT_PROPERTY:
                    editMenu.setVisible(false);
                    saveMenu.setVisible(true);
                    break;
                case Constants.CLICK_OWNER_EDIT_EVENT:
                    editMenu.setVisible(false);
                    saveMenu.setVisible(true);
                    break;
            }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.event_edit:
                if (!getPresenter().isEventRepeat) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String[] options = {"Edit Following Occurrences", "Edit Current Occurrence", "Cancel"};
                    builder.setTitle("Choose Option")
                            .setItems(options, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    switch (which) {
                                        case 0:
                                            editMenu.setVisible(false);
                                            saveMenu.setVisible(true);
                                            toolbarTitle.setText(getString(R.string.title_edit_event));
                                            getPresenter().isOccurrence = false;
                                            getPresenter().enableDisableUI(true);
                                            break;
                                        case 1:
                                            editMenu.setVisible(false);
                                            saveMenu.setVisible(true);
                                            toolbarTitle.setText(getString(R.string.title_edit_event));
                                            getPresenter().isOccurrence = true;
                                            getPresenter().enableDisableUI(true);
                                            break;
                                        case 2:
                                            dialog.dismiss();
                                            break;

                                    }
                                }
                            });
                    builder.setCancelable(false);
                    builder.show();
                } else {
                    editMenu.setVisible(false);
                    saveMenu.setVisible(true);
                    toolbarTitle.setText(getString(R.string.title_edit_event));
                    getPresenter().isOccurrence = true;
                    getPresenter().enableDisableUI(true);
                }
                return true;
            case R.id.event_save:
                if (currentScreen.equals(Constants.CLICK_VIEW_APPLIANCE)) {
                    saveMenu.setVisible(false);
                    editMenu.setVisible(true);
                }
                if (getPresenter().addEditEvent()) {
//                    getPresenter().enableDisableUI(false);
                }

                return true;
        }
        return false;
    }

    @Override
    public NestedScrollView getNestedScrollView() {
        return nestedScrollView;
    }

    @Override
    public TextInputEditText getEtEventTitleView() {
        return etEventTitle;
    }

    @Override
    public TextInputEditText getEtEventTitleDetailView() {
        return etEventTitleDetail;
    }

    @Override
    public TextInputEditText getEtAddNotesView() {
        return etAddNotes;
    }

    @Override
    public TextInputEditText getEtNotesDetailView() {
        return etNotesDetail;
    }

    @Override
    public TextInputEditText getEtDateTimeFromView() {
        return etDateTimeFrom;
    }

    @Override
    public TextInputEditText getEtDateTimeToView() {
        return etDateTimeTo;
    }

    @Override
    public TextInputEditText getEtReminderView() {
        return etReminder;
    }

    @Override
    public AppCompatSpinner getSpinnerPropertyView() {
        return spinnerProperty;
    }

    @Override
    public AppCompatSpinner getSpinnerApplianceView() {
        return spinnerAppliance;
    }

    @Override
    public AppCompatSpinner getSpinnerProviderView() {
        return spinnerProvider;
    }

    @Override
    public AppCompatSpinner getSpinnerProjectView() {
        return spinnerProject;
    }

    @Override
    public AppCompatSpinner getSpinnerRepeatEventView() {
        return spinnerRepeatEvent;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public String getCurrentScreen() {
        return currentScreen;
    }

    @Override
    public int getEventType() {
        return eventType;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public TextView getToolbarTitleView() {
        return toolbarTitle;
    }

    @Override
    public TextView getHeaderPropertyNameView() {
        return tvHeaderPropertyName;
    }

    @Override
    public TextView getHeaderProviderNameView() {
        return tvHeaderProviderName;
    }

    @Override
    public TextView getHeaderProjectNameView() {
        return tvHeaderProjectName;
    }

    @Override
    public TextView getHeaderApplianceNameView() {
        return tvHeaderApplianceName;
    }

    @Override
    public TextView getHeaderEventRepeatNameView() {
        return tvHeaderEventRepeatName;
    }

    @Override
    public TextView getHeaderRepeatReminderName() {
        return tvHeaderRepeatReminderName;
    }

    @Override
    public TextView getRepeatReminderView() {
        return tvRepeatReminder;
    }

    @Override
    public TextView getTVHeaderEventFromTimeView() {
        return tvHeaderEventFromTime;
    }

    @Override
    public TextView getTVEventFromTimeView() {
        return tvEventFromTime;
    }

    @Override
    public TextView getTVHeaderEventToView() {
        return tvHeaderEventToTime;
    }

    @Override
    public TextView getTVEventToTimeView() {
        return tvEventToTime;
    }

    @Override
    public TextInputLayout getTILEventTitleView() {
        return tilEventTitle;
    }

    @Override
    public TextInputLayout getTILEventTitleDetailView() {
        return tilEventTitleDetail;
    }

    @Override
    public TextInputLayout getTILNotesView() {
        return tilNotes;
    }

    @Override
    public TextInputLayout getTILNotesDetailView() {
        return tilNotesDetail;
    }

    @Override
    public View getProviderNameLineView() {
        return viewProviderName;
    }

    @Override
    public View getProjectNameLineView() {
        return viewProjectName;
    }

    @Override
    public View getApplianceNameLineView() {
        return viewApplianceName;
    }

    @Override
    public View getPropertyNameLineView() {
        return viewPropertyName;
    }

    @Override
    public View getRepeatEventLineView() {
        return viewRepeatEvent;
    }

    @Override
    public View getReminderTimeLineView() {
        return viewReminderTime;
    }

    @Override
    public View getEventFromLineView() {
        return viewEventFrom;
    }

    @Override
    public View getEventToLineView() {
        return viewEventTo;
    }
}
