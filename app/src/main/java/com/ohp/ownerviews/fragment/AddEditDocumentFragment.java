package com.ohp.ownerviews.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.ownerviews.presenter.AddEditDocumentPresenterImpl;
import com.ohp.ownerviews.view.AddEditDocumentView;
import com.ohp.utils.Constants;

/**
 * Created by Anuj Sharma on 6/6/2017.
 */

public class AddEditDocumentFragment extends BaseFragment<AddEditDocumentPresenterImpl> implements AddEditDocumentView {
    private View rootView;
    private Toolbar mToolbar;
    private TextView toolbarTitle, tvHeaderAppliance, tvHeaderProperty, tvDocImages, tvHeaderPropertyImage;
    private LinearLayout detailTopView, applianceSpinnerView;
    private ImageView propertyImage;
    private ProgressBar progressBar;
    private TextInputEditText etDocumentName, etDocumentNameDetail, etApplianceName, etCreationDate;
    private AppCompatSpinner spinnerPropertyName, spinnerApplianceName;
    private RecyclerView recyclerDocuments;
    private String currentScreen = "";
    private MenuItem editMenu, saveMenu;
    private TextInputLayout tilCreatedOn, tilDocumentName, tilDocumentNameDetail;
    private View applianceView, propertyView;

    @Override
    protected AddEditDocumentPresenterImpl onAttachPresenter() {
        return new AddEditDocumentPresenterImpl(this, getActivity());
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_document, menu);
        editMenu = menu.findItem(R.id.document_edit);
        saveMenu = menu.findItem(R.id.document_save);
        switch (currentScreen) {
            case Constants.CLICK_VIEW_DOCUMENT:
                editMenu.setVisible(true);
                saveMenu.setVisible(false);
                break;
            case Constants.CLICK_ADD_DOCUMENT:
                editMenu.setVisible(false);
                saveMenu.setVisible(true);
                break;

            case Constants.ADD_EDIT_PROPERTY:
                editMenu.setVisible(false);
                saveMenu.setVisible(true);
                break;
            case Constants.CLICK_EDIT_DOCUMENT:
                editMenu.setVisible(false);
                saveMenu.setVisible(true);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.document_edit:
                editMenu.setVisible(false);
                saveMenu.setVisible(true);
                getPresenter().enableDisableUI(true);
                getPresenter().showHideDetailView(false);
                toolbarTitle.setText(getString(R.string.title_edit_document));
                return true;
            case R.id.document_save:
                if (currentScreen.equals(Constants.CLICK_VIEW_APPLIANCE)) {
                    saveMenu.setVisible(false);
                    editMenu.setVisible(true);
                }
                if (getPresenter().validateDocumentInfo()) {
//                    getPresenter().enableDisableUI(false);
                }

                return true;
        }
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_edit_document, container, false);
        return rootView;
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        tvHeaderAppliance = (TextView) view.findViewById(R.id.header_appliance);
        tvHeaderPropertyImage = (TextView) view.findViewById(R.id.tvHeaderPropertyImage);
        tvDocImages = (TextView) view.findViewById(R.id.tvDocImages);
        tvHeaderProperty = (TextView) view.findViewById(R.id.header_property);
        detailTopView = (LinearLayout) view.findViewById(R.id.detail_top_container);
        applianceSpinnerView = (LinearLayout) view.findViewById(R.id.view_appliance_spinner);
        propertyImage = (ImageView) view.findViewById(R.id.avatar);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        applianceView = view.findViewById(R.id.appliance_name_view);
        propertyView = view.findViewById(R.id.property_name_view);

        etDocumentName = (TextInputEditText) view.findViewById(R.id.et_document_name);
        etDocumentNameDetail = (TextInputEditText) view.findViewById(R.id.et_document_name_detail);
        spinnerPropertyName = (AppCompatSpinner) view.findViewById(R.id.spinner_property_name);
        spinnerApplianceName = (AppCompatSpinner) view.findViewById(R.id.spinner_appliance_name);
        tilCreatedOn = (TextInputLayout) view.findViewById(R.id.til_created_on);
        tilDocumentNameDetail = (TextInputLayout) view.findViewById(R.id.til_document_name_detail);
        tilDocumentName = (TextInputLayout) view.findViewById(R.id.til_document_name);

        etApplianceName = (TextInputEditText) view.findViewById(R.id.et_appliance_name);
        etCreationDate = (TextInputEditText) view.findViewById(R.id.et_document_create_date);
        recyclerDocuments = (RecyclerView) view.findViewById(R.id.recycler_documents);

        propertyImage.setOnClickListener(getPresenter());

        if (getArguments() != null) {
            getPresenter().saveBundleInfo(getArguments());
            currentScreen = getArguments().getString(Constants.DESTINATION, "");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getPresenter().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public TextView getHeaderAppliance() {
        return tvHeaderAppliance;
    }

    @Override
    public TextView getDocImagesHeaderView() {
        return tvDocImages;
    }

    @Override
    public TextView tvHeaderPropertyImage() {
        return tvHeaderPropertyImage;
    }

    @Override
    public TextView getHeaderProperty() {
        return tvHeaderProperty;
    }

    @Override
    public LinearLayout getDetailTopContainer() {
        return detailTopView;
    }

    @Override
    public ImageView getPropertyImage() {
        return propertyImage;
    }

    @Override
    public ProgressBar getImageProgress() {
        return progressBar;
    }

    @Override
    public AppCompatSpinner getPropertySpinner() {
        return spinnerPropertyName;
    }

    @Override
    public LinearLayout getApplianceSpinnerView() {
        return applianceSpinnerView;
    }

    @Override
    public AppCompatSpinner getApplianceSpinner() {
        return spinnerApplianceName;
    }

    @Override
    public TextInputEditText getDocumentName() {
        return etDocumentName;
    }

    @Override
    public TextInputEditText getDocumentNameDetail() {
        return etDocumentNameDetail;
    }

    @Override
    public TextInputEditText getCreationDate() {
        return etCreationDate;
    }

    @Override
    public RecyclerView getDocumentRecycler() {
        return recyclerDocuments;
    }

    @Override
    public MenuItem getSaveMenu() {
        return saveMenu;
    }

    @Override
    public MenuItem getEditMenu() {
        return editMenu;
    }

    @Override
    public TextInputLayout getTilCreatedOnView() {
        return tilCreatedOn;
    }

    @Override
    public TextInputLayout getTilDocumentNameDetailView() {
        return tilDocumentNameDetail;
    }

    @Override
    public TextInputLayout getTilDocumentNameView() {
        return tilDocumentName;
    }

    @Override
    public View getApplianceLineView() {
        return applianceView;
    }

    @Override
    public View getPropertyLineView() {
        return propertyView;
    }

}
