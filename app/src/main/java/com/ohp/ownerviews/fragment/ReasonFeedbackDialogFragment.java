package com.ohp.ownerviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.ownerviews.presenter.AddEditProjectFragmentPresenterImpl;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

/**
 * @author Amanpal Singh.
 */

public class ReasonFeedbackDialogFragment extends DialogFragment implements View.OnClickListener {
    private View rootView;
    private RatingBar userRating;
    private String reasonFeedback = "", rating = "1";
    private boolean isFeedback = false;
    private EditText etReasonFeedback;
    private TextView tvCancel, tvSubmit, tvTitle;
    private AddEditProjectFragmentPresenterImpl presenter;


    public static synchronized ReasonFeedbackDialogFragment getInstance() {
        return new ReasonFeedbackDialogFragment();
    }


    public void setParams(AddEditProjectFragmentPresenterImpl presenter) {
        this.presenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_cancel_end, container, false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getDialog().getWindow() != null)
            getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    private void initUI() {
        tvCancel = (TextView) rootView.findViewById(R.id.tv_cancel);
        tvSubmit = (TextView) rootView.findViewById(R.id.tv_submit);
        tvTitle = (TextView) rootView.findViewById(R.id.tv_title);
        userRating = (RatingBar) rootView.findViewById(R.id.rating);

        etReasonFeedback = (EditText) rootView.findViewById(R.id.et_reason_feedback);

        if (getArguments() != null) {
            isFeedback = getArguments().getBoolean(Constants.IS_FEEDBACK, false);
        }

        if (isFeedback) {
            tvTitle.setText(R.string.title_feedback);
            etReasonFeedback.setHint(R.string.leave_a_feedback);
            userRating.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setText(R.string.title_reason);
            etReasonFeedback.setHint(R.string.leave_a_reason);
        }


        userRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                int userRating = (int) rating;
                ReasonFeedbackDialogFragment.this.rating = String.valueOf(userRating);
            }
        });
        tvSubmit.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Utils.getInstance().hideSoftKeyboard(getActivity(), etReasonFeedback);
        reasonFeedback = etReasonFeedback.getText().toString().trim();
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.tv_submit:
                if (isFeedback) {
                    presenter.endProject(rating, reasonFeedback);
                } else {
                    presenter.cancelProject(reasonFeedback);
                }
                dismiss();
                break;

        }
    }
}