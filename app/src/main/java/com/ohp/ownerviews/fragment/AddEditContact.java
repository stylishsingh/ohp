package com.ohp.ownerviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.ownerviews.presenter.AddEditContactPresenterImpl;
import com.ohp.ownerviews.view.AddEditContactView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

/**
 * Created by vishal.sharma on 8/31/2017.
 */

public class AddEditContact extends BaseFragment<AddEditContactPresenterImpl> implements AddEditContactView {
    private View rootView;
    private TextInputEditText txt_fullname, txt_Email, txt_Designation, txt_ContactNumber;
    private AppCompatSpinner categorySpinner;
    private Toolbar mToolbar;
    private TextView toolbarTitle, titleCategory;
    private String currentScreen = "";
    private MenuItem editMenu, saveMenu;
    private TextInputLayout titleName, titleEmail, titleDesignation, titlePhoneNumber;

    @Override
    protected AddEditContactPresenterImpl onAttachPresenter() {
        return new AddEditContactPresenterImpl(this, getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_edit_contact, container, false);
        return rootView;
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        txt_fullname = (TextInputEditText) view.findViewById(R.id.et_name);
        txt_Email = (TextInputEditText) view.findViewById(R.id.et_email);
        txt_ContactNumber = (TextInputEditText) view.findViewById(R.id.et_contactNumber);
        txt_Designation = (TextInputEditText) view.findViewById(R.id.et_designation);
        categorySpinner = (AppCompatSpinner) view.findViewById(R.id.spinner_team);
        titleName = (TextInputLayout) view.findViewById(R.id.til_name);
        titleEmail = (TextInputLayout) view.findViewById(R.id.til_email);
        titleDesignation = (TextInputLayout) view.findViewById(R.id.til_designation);
        titlePhoneNumber = (TextInputLayout) view.findViewById(R.id.til_contactNumber);
        titleCategory = (TextView) view.findViewById(R.id.til_category);
        if (getArguments() != null) {
            //  getPresenter().getResource();
            getPresenter().saveBundleInfo(getArguments());
            currentScreen = getArguments().getString(Constants.DESTINATION, "");
        }
    }

    @Override
    public TextInputEditText getName() {
        return txt_fullname;
    }


    @Override
    public TextInputEditText getEmail() {
        return txt_Email;
    }

    @Override
    public TextInputEditText getDesignnatiom() {
        return txt_Designation;
    }

    @Override
    public TextInputEditText getContactNumber() {

        return txt_ContactNumber;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public AppCompatSpinner getCategory() {
        return categorySpinner;
    }

    @Override
    public TextInputLayout getNameTitle() {
        return titleName;
    }

    @Override
    public TextInputLayout getEmailTitle() {
        return titleEmail;
    }

    @Override
    public TextInputLayout getDesignationTitle() {
        return titleDesignation;
    }

    @Override
    public TextInputLayout getContactNumberTitle() {
        return titlePhoneNumber;
    }

    @Override
    public TextView getCategoryTitle() {
        return titleCategory;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_event, menu);
        editMenu = menu.findItem(R.id.event_edit);
        saveMenu = menu.findItem(R.id.event_save);

        switch (currentScreen) {
            case Constants.CLICK_VIEW_CONTACT:
                editMenu.setVisible(true);
                saveMenu.setVisible(false);
                toolbarTitle.setText("Contact Details");
                getPresenter().hideViewForViewContact();
                break;
            case Constants.CLICK_EDIT_CONTACT:
                toolbarTitle.setText("Edit Contact");
                editMenu.setVisible(false);
                saveMenu.setVisible(true);
                getPresenter().hideViewForEditContactDetail();
                break;
            case Constants.CLICK_ADD_CONTACT:
                toolbarTitle.setText("Add Contact");
                editMenu.setVisible(false);
                saveMenu.setVisible(true);

                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.event_edit:
                editMenu.setVisible(false);
                saveMenu.setVisible(true);
                toolbarTitle.setText("Edit Contact");
                getPresenter().hideViewForEditContactDetail();
                break;
            case R.id.event_save:
                Utils.getInstance().hideSoftKeyboard(getActivity(), rootView);
                getPresenter().AddEditContact();
                break;
        }
        return true;
    }


}
