package com.ohp.ownerviews.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.ownerviews.presenter.OwnerProfileFragmentPresenterImpl;
import com.ohp.ownerviews.view.OwnerProfileFragmentView;
import com.ohp.utils.Utils;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * @author Anuj Sharma.
 */

public class OwnerProfileFragment extends BaseFragment<OwnerProfileFragmentPresenterImpl> implements OwnerProfileFragmentView {
    public MenuItem editMenu, saveMenu;
    private View rootView;
    private NestedScrollView profileNestedScroll;
    private ImageView coverImage;
    private FloatingActionButton editProfile;
    private CircleImageView profileImg;
    private ProgressBar progressBar;
    private TextInputEditText etName, etEmail;
    private TextInputEditText etAddress;
    private RadioGroup radioGroup;
    private RadioButton maleRadio, femaleRadio;
    private TextInputLayout tilFullName;
    private boolean isFirstTime;
    private TextView tvHeaderGender;

    @Override
    protected OwnerProfileFragmentPresenterImpl onAttachPresenter() {
        return new OwnerProfileFragmentPresenterImpl(this, getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_profile, menu);
        editMenu = menu.findItem(R.id.profile_edit);
        saveMenu = menu.findItem(R.id.profile_save);
//        if (isFirstTime) {
            editMenu.setVisible(false);
            saveMenu.setVisible(true);
//        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        editMenu = menu.findItem(R.id.profile_edit);
        saveMenu = menu.findItem(R.id.profile_save);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Utils.getInstance().hideSoftKeyboard(getActivity(), rootView);
        switch (item.getItemId()) {
            case R.id.profile_edit:
                saveMenu.setVisible(true);
                editMenu.setVisible(false);
                getPresenter().enableDisableUI(true);
                return true;
            case R.id.profile_save:
                if (getPresenter().saveProfileInfo()) {
                    getPresenter().enableDisableUI(false);
                    saveMenu.setVisible(false);
                    editMenu.setVisible(true);
                }
                return true;
        }
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_owner_profile, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            getPresenter().saveBundleInfo(bundle);

        }
    }

    @Override
    protected void initUI(View view) {
        profileNestedScroll = (NestedScrollView) view.findViewById(R.id.profile_nested_scroll);
        coverImage = (ImageView) view.findViewById(R.id.cover_image);
        tvHeaderGender= (TextView) view.findViewById(R.id.tvHeaderGender);

        profileImg = (CircleImageView) view.findViewById(R.id.profile_img);
        editProfile = (FloatingActionButton) view.findViewById(R.id.edit_profile);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        tilFullName = (TextInputLayout) view.findViewById(R.id.til_name);
        etName = (TextInputEditText) view.findViewById(R.id.et_name);
        etEmail = (TextInputEditText) view.findViewById(R.id.et_email);
        etAddress = (TextInputEditText) view.findViewById(R.id.et_address);
        radioGroup = (RadioGroup) view.findViewById(R.id.radio_group);
        maleRadio = (RadioButton) view.findViewById(R.id.radio_male);
        femaleRadio = (RadioButton) view.findViewById(R.id.radio_female);

        editProfile.setOnClickListener(getPresenter());
        etAddress.setOnClickListener(getPresenter());
        profileImg.setOnClickListener(getPresenter());

        if (getArguments() != null) {
            Bundle bundle = getArguments();
            getPresenter().saveBundleInfo(bundle);
            isFirstTime = true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        getPresenter().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public NestedScrollView getScrollView() {
        return profileNestedScroll;
    }

    @Override
    public ImageView getCoverImg() {
        return coverImage;
    }

    @Override
    public CircleImageView getProfileImg() {
        return profileImg;
    }

    @Override
    public TextView tvHeaderGender() {
        return tvHeaderGender;
    }

    @Override
    public FloatingActionButton getEditProfile() {
        return editProfile;
    }

    @Override
    public ProgressBar getProfileProgress() {
        return progressBar;
    }

    @Override
    public TextInputEditText getName() {
        return etName;
    }

    @Override
    public TextInputEditText getEmail() {
        return etEmail;
    }

    @Override
    public TextInputEditText getAddress() {
        return etAddress;
    }

    @Override
    public RadioGroup getRadioGroup() {
        return radioGroup;
    }

    @Override
    public RadioButton getMaleRadio() {
        return maleRadio;
    }

    @Override
    public RadioButton getFemaleRadio() {
        return femaleRadio;
    }

    @Override
    public MenuItem getEditMenu() {
        return editMenu;
    }

    @Override
    public MenuItem getSaveMenu() {
        return saveMenu;
    }

    @Override
    public TextInputLayout getFullNameLayoutView() {
        return tilFullName;
    }
}
