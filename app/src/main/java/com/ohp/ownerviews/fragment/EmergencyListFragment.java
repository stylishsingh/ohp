package com.ohp.ownerviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.ownerviews.presenter.EmergencyListFragmentPresenterImpl;
import com.ohp.ownerviews.view.EmergencyListFragmentView;

/**
 * @author Amanpal Singh.
 */

public class EmergencyListFragment extends BaseFragment<EmergencyListFragmentPresenterImpl> implements EmergencyListFragmentView {
    private View rootView;
    private FloatingActionButton floatingActionButton;
    private RecyclerView recyclerView;
    private RelativeLayout relativeLayout;
    private EditText etSearch;
    private LinearLayout btnClear;
    private boolean isMenu = false;
    private NestedScrollView nsRecyclerView;
    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
    }

    @Override
    protected EmergencyListFragmentPresenterImpl onAttachPresenter() {
        return new EmergencyListFragmentPresenterImpl(this, getActivity());
    }

    @Override
    protected void initUI(View view) {
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fab_add);
        floatingActionButton.setOnClickListener(getPresenter());
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.empty_list_layout);
        nsRecyclerView = (NestedScrollView) view.findViewById(R.id.ns_recycler_view);
        etSearch = (EditText) view.findViewById(R.id.et_search);
        btnClear = (LinearLayout) view.findViewById(R.id.btn_clear);
        btnClear.setOnClickListener(getPresenter());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_emergency, container, false);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public RecyclerView getRecylerContactList() {
        return recyclerView;
    }

    @Override
    public RelativeLayout getEmptyLayout() {
        return relativeLayout;
    }

    @Override
    public NestedScrollView getNSRecyclerView() {
        return nsRecyclerView;
    }

    @Override
    public EditText getSearchEditText() {
        return etSearch;
    }

    @Override
    public LinearLayout getCloseBtn() {
        return btnClear;
    }

   /* @Override
    public RecyclerViewFastScroller getRecyclerViewFastScrollerAlphabets() {
        return fastScroller;
    }*/
}