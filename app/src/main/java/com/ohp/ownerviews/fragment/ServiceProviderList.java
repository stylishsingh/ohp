package com.ohp.ownerviews.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.ownerviews.presenter.ServiceProviderListPresenterImpl;
import com.ohp.ownerviews.view.ServiceProviderListView;
import com.ohp.serviceproviderviews.fragment.ProviderFilterDialogFragment;

/**
 * @author Amanpal Singh.
 */

public class ServiceProviderList extends BaseFragment<ServiceProviderListPresenterImpl> implements ServiceProviderListView {

    private RecyclerView recyclerView;
    private RelativeLayout emptyView;
    private EditText etSearch;
    private LinearLayout btnClear;
    private NestedScrollView nsRecyclerView;
    private View rootView;
    private MenuItem itemFilter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_dashboard, menu);
        itemFilter = menu.findItem(R.id.item_filter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_filter:
                // Create and show the dialog.
                showFilterDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void showFilterDialog() {
        try {
            ProviderFilterDialogFragment fragment = ProviderFilterDialogFragment.getInstance();
            if (fragment.isAdded()) {
                return; //or return false/true, based on where you are calling from
            }
            fragment.show(getActivity().getSupportFragmentManager(), "dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected ServiceProviderListPresenterImpl onAttachPresenter() {
        return new ServiceProviderListPresenterImpl(this, getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_service_providers, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    protected void initUI(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        nsRecyclerView = (NestedScrollView) view.findViewById(R.id.ns_recycler_view);
        emptyView = (RelativeLayout) view.findViewById(R.id.empty_list_layout);
        etSearch = (EditText) view.findViewById(R.id.et_search);
        btnClear = (LinearLayout) view.findViewById(R.id.btn_clear);
        btnClear.setOnClickListener(getPresenter());
    }


    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public NestedScrollView getNSRecyclerView() {
        return nsRecyclerView;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public RelativeLayout getEmptyView() {
        return emptyView;
    }

    @Override
    public EditText getSearchEditText() {
        return etSearch;
    }

    @Override
    public LinearLayout getCloseBtn() {
        return btnClear;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        getPresenter().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }
}
