package com.ohp.ownerviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.ownerviews.presenter.AppliancesListFragmentPresenterImpl;
import com.ohp.ownerviews.view.AppliancesView;
import com.ohp.utils.Constants;

/**
 * @author Amanpal Singh.
 */

public class ApplianceListFragment extends BaseFragment<AppliancesListFragmentPresenterImpl> implements AppliancesView {
    private RecyclerView recyclerView;
    private RelativeLayout emptyView;
    private View rootView;
    private NestedScrollView nsRecyclerView;
    private FloatingActionButton fabAddAppliance;
    private EditText etSearch;
    private LinearLayout btnClear;
    private String navigateFrom = "", propertyId = "";
    private Toolbar toolbar;
    private TextView toolbarTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_appliances, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected AppliancesListFragmentPresenterImpl onAttachPresenter() {
        return new AppliancesListFragmentPresenterImpl(this, getActivity());
    }

    @Override
    protected void initUI(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        nsRecyclerView = (NestedScrollView) view.findViewById(R.id.ns_recycler_view);
        emptyView = (RelativeLayout) view.findViewById(R.id.empty_list_layout);
        etSearch = (EditText) view.findViewById(R.id.et_search);
        btnClear = (LinearLayout) view.findViewById(R.id.btn_clear);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        fabAddAppliance = (FloatingActionButton) view.findViewById(R.id.fab_add_appliance);
        fabAddAppliance.setOnClickListener(getPresenter());
        recyclerView.setNestedScrollingEnabled(false);
        btnClear.setOnClickListener(getPresenter());
        //fetch arguments
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            navigateFrom = bundle.getString(Constants.DESTINATION, "");
            if (navigateFrom.equalsIgnoreCase(Constants.ADD_EDIT_PROPERTY)) {
                toolbar.setVisibility(View.VISIBLE);
                propertyId = bundle.getString(Constants.PROPERTY_ID, "");
            } else
                toolbar.setVisibility(View.GONE);
        }
    }


    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public NestedScrollView getNSRecyclerView() {
        return nsRecyclerView;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public RelativeLayout getEmptyView() {
        return emptyView;
    }

    @Override
    public EditText getSearchEditText() {
        return etSearch;
    }

    @Override
    public LinearLayout getCloseBtn() {
        return btnClear;
    }

    @Override
    public Toolbar getToolbarView() {
        return toolbar;
    }

    @Override
    public String getNavigateFrom() {
        return navigateFrom;
    }

    @Override
    public String getPropertyID() {
        return propertyId;
    }
}
