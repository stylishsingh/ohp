package com.ohp.ownerviews.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.ownerviews.presenter.AddEditProjectFragmentPresenterImpl;
import com.ohp.ownerviews.view.ProjectDetailView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import de.hdodenhof.circleimageview.CircleImageView;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

/**
 * @author Vishal Sharma.
 */

public class AddEditProjectFragment extends BaseFragment<AddEditProjectFragmentPresenterImpl> implements ProjectDetailView {
    private View rootView, SaveProject, EditProject, bidLayoutView, cancelEndView, acceptRejectView;
    private Toolbar mToolbar;
    private TextView toolbarTitle, tvExpertise, txt_noProjectImages, txt_noBids, tvAwardProviderHeader,
            tvAwardProviderName, tvAwardProviderExpertise, tvAwardProviderDistance, tvAwardProviderLicence, tvAwardProviderBidAmount,
            tvCancel, tvEnd, tvReject, tvAccept;
    private CardView cvItem;
    private CircleImageView circleImageView;
    private TextInputEditText etPropertyName, etPropertyAddress, etProjectName, etProjectCreatedOn, etProjectJobType, etProjectDescription, et_projectName_edit, et_project_expertise, etProjectDescription_edit, et_appliance_name_edit;
    private AppCompatSpinner mSpinnerJobType, mSpinnerProperty, mSpinnerApplianceName;
    private RecyclerView mRecyclerProjectImages, recyclerEditProject, mRecyclerServiceProviderBidDetail;
    private MenuItem editMenu, saveMenu, endMenu;
    private NestedScrollView nestedScrollView;
    private String currentScreen = "", propertyID = "";
    private ImageView Img_propertyImage;
    private ProgressBar progressBar;
    private RatingBar rbAwardedProvider;
    private MultiSelectSpinner expertiseSpinner;
    private LinearLayout parentLinearLayout;
    private String CurrentScreen, projectStatus = "";
    private FloatingActionButton fabAwardedChat;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_project_main, container, false);
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        propertyID = getArguments().getString(Constants.PROPERTY_ID, "");
    }


    @Override
    protected void initUI(View view) {
        nestedScrollView = (NestedScrollView) view.findViewById(R.id.nested_scroll_view);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        txt_noProjectImages = (TextView) view.findViewById(R.id.txt_Noimages);
        txt_noBids = (TextView) view.findViewById(R.id.txt_NoBidList);
        etProjectName = (TextInputEditText) view.findViewById(R.id.et_project_name);
        et_projectName_edit = (TextInputEditText) view.findViewById(R.id.et_project_name_edit);
        et_appliance_name_edit = (TextInputEditText) view.findViewById(R.id.et_appliance_name_edit);
        etPropertyName = (TextInputEditText) view.findViewById(R.id.et_property_name);
        etPropertyAddress = (TextInputEditText) view.findViewById(R.id.et_property_address);
        etProjectCreatedOn = (TextInputEditText) view.findViewById(R.id.et_project_created_on);
        etProjectJobType = (TextInputEditText) view.findViewById(R.id.et_project_job_type);
        etProjectDescription = (TextInputEditText) view.findViewById(R.id.et_project_description);
        etProjectDescription_edit = (TextInputEditText) view.findViewById(R.id.etProjectDescription_edit);
        et_project_expertise = (TextInputEditText) view.findViewById(R.id.et_project_expertise);
        Img_propertyImage = (ImageView) view.findViewById(R.id.avatar);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        mSpinnerJobType = (AppCompatSpinner) view.findViewById(R.id.spinner_job_type);
        mSpinnerApplianceName = (AppCompatSpinner) view.findViewById(R.id.spinner_appliance_name);
        expertiseSpinner = (MultiSelectSpinner) view.findViewById(R.id.spinner_expertise);
        mSpinnerProperty = (AppCompatSpinner) view.findViewById(R.id.spinner_property);
        mSpinnerProperty.setOnItemSelectedListener(getPresenter());
        mRecyclerProjectImages = (RecyclerView) view.findViewById(R.id.recycler_project_images);
        recyclerEditProject = (RecyclerView) view.findViewById(R.id.recycler_project_images_save);
        mRecyclerServiceProviderBidDetail = (RecyclerView) view.findViewById(R.id.recycler_service_provider_detail);
        parentLinearLayout = (LinearLayout) view.findViewById(R.id.detail_top_container);
        SaveProject = view.findViewById(R.id.fragment_Save_project);
        EditProject = view.findViewById(R.id.fragment_save_edit_project);
        bidLayoutView = view.findViewById(R.id.rl_bid_layout);
        cancelEndView = view.findViewById(R.id.rl_cancel_end);
        acceptRejectView = view.findViewById(R.id.rl_accept_reject);

        tvAwardProviderName = (TextView) view.findViewById(R.id.txt_name);
        rbAwardedProvider = (RatingBar) view.findViewById(R.id.ratingBar);
        tvAwardProviderDistance = (TextView) view.findViewById(R.id.txt_provider_distance);
        tvAwardProviderExpertise = (TextView) view.findViewById(R.id.txt_expertise_detail);
        tvAwardProviderLicence = (TextView) view.findViewById(R.id.txt_txt_provider_licensed);
        tvAwardProviderBidAmount = (TextView) view.findViewById(R.id.txt_txt_provider_amount);
        tvAwardProviderHeader = (TextView) view.findViewById(R.id.tv_header_provider);
        tvCancel = (TextView) view.findViewById(R.id.tv_cancel);
        tvEnd = (TextView) view.findViewById(R.id.tv_end);
        fabAwardedChat = (FloatingActionButton) view.findViewById(R.id.message);

        tvAccept = (TextView) view.findViewById(R.id.tv_accept);
        tvReject = (TextView) view.findViewById(R.id.tv_reject);


        circleImageView = (CircleImageView) view.findViewById(R.id.profile_img);

        cvItem = (CardView) view.findViewById(R.id.rl_sp_award);

        if (getArguments() != null) {
            getPresenter().saveBundleInfo(getArguments());
            currentScreen = getArguments().getString(Constants.DESTINATION, "");
            projectStatus = getArguments().getString(Constants.PROJECT_STATUS, "");
            if (getArguments().getString(Constants.PROJECT_ID) != null) {
                String Project_ID = getArguments().getString(Constants.PROJECT_ID);
                getPresenter().getOnGoingObject(Project_ID);
            }
        }

        cvItem.setOnClickListener(getPresenter());
        tvCancel.setOnClickListener(getPresenter());
        tvEnd.setOnClickListener(getPresenter());
        tvAccept.setOnClickListener(getPresenter());
        tvReject.setOnClickListener(getPresenter());
        fabAwardedChat.setOnClickListener(getPresenter());

        expertiseSpinner.setListener(getPresenter().expertiseSpinnerListener);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_project, menu);
        editMenu = menu.findItem(R.id.project_edit);
        saveMenu = menu.findItem(R.id.project_save);
        endMenu = menu.findItem(R.id.project_end);
//        saveMenu.setVisible(true);
//        editMenu.setVisible(false);
//        endMenu.setVisible(false);


        if (editMenu != null && saveMenu != null) {
            switch (currentScreen) {
                case Constants.CLICK_ADD_PROJECT:
                    saveMenu.setVisible(true);
                    editMenu.setVisible(false);
                    break;
                case Constants.CLICK_EDIT_PROJECT:
                    saveMenu.setVisible(true);
                    editMenu.setVisible(false);
                    break;
                case Constants.CLICK_VIEW_PROJECT:
                    if (!projectStatus.equalsIgnoreCase("2")) {
                        saveMenu.setVisible(false);
                        editMenu.setVisible(true);
                    }
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.project_save:
                getPresenter().addProject();
                Utils.getInstance().hideSoftKeyboard(getActivity(), rootView);
                break;
            case R.id.project_edit:
                toolbarTitle.setText(R.string.edit_project);
                saveMenu.setVisible(true);
                editMenu.setVisible(false);
                getPresenter().visibilityGoneUiForSaveProject();
                break;
        }
        return true;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getPresenter().onRequestPermissionResult(requestCode, grantResults);
    }


    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public NestedScrollView getNestedScrollView() {
        return nestedScrollView;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public String getPropertyID() {
        return propertyID;
    }

    @Override
    public ProgressBar getProgressBar() {
        return progressBar;
    }

    @Override
    public ImageView getPropertyImage() {
        return Img_propertyImage;
    }

    @Override
    public MultiSelectSpinner getExpertiseSpinnerView() {
        return expertiseSpinner;
    }

    @Override
    public TextInputEditText getPropertyName() {
        return etPropertyName;
    }

    @Override
    public TextInputEditText getPropertyAddress() {
        return etPropertyAddress;
    }

    @Override
    public TextInputEditText getProjectName() {
        return etProjectName;
    }

    @Override
    public TextInputEditText getProjectCreatedOn() {
        return etProjectCreatedOn;
    }

    @Override
    public TextInputEditText getProjectJopType() {
        return etProjectJobType;
    }

    @Override
    public TextInputEditText getProjectDescriptionEdit() {
        return etProjectDescription_edit;
    }

    @Override
    public TextInputEditText getProjectExpertise() {
        return et_project_expertise;
    }

    @Override
    public TextInputEditText getProjectDescription() {
        return etProjectDescription;
    }

    @Override
    public TextInputEditText getApplianceDescription() {
        return et_appliance_name_edit;
    }

    @Override
    public TextInputEditText getProjectNameEdit() {
        return et_projectName_edit;
    }


    @Override
    public AppCompatSpinner getJobType() {
        return mSpinnerJobType;
    }

    @Override
    public TextView getExpertise() {
        return tvExpertise;
    }

    @Override
    public AppCompatSpinner getProperty() {
        return mSpinnerProperty;
    }

    @Override
    public AppCompatSpinner getApplianceName() {
        return mSpinnerApplianceName;
    }

    @Override
    public View getSaveProject() {
        return SaveProject;
    }

    @Override
    public View getEditProject() {
        return EditProject;
    }

    @Override
    public LinearLayout getLayout() {
        return parentLinearLayout;
    }


    @Override
    public RecyclerView getProjectDetailRecycler() {
        return mRecyclerProjectImages;
    }

    @Override
    public RecyclerView getEditProjectDetailRecycler() {
        return recyclerEditProject;
    }

    @Override
    public RecyclerView getSpBidsRecyclerView() {
        return mRecyclerServiceProviderBidDetail;
    }

    @Override
    public TextView NoImages() {
        return txt_noProjectImages;
    }

    @Override
    public TextView NoBids() {
        return txt_noBids;
    }

    @Override
    public TextView getAwardProviderNameView() {
        return tvAwardProviderName;
    }

    @Override
    public TextView getAwardProviderExpertiseView() {
        return tvAwardProviderExpertise;
    }

    @Override
    public TextView getAwardProviderDistanceView() {
        return tvAwardProviderDistance;
    }

    @Override
    public TextView getAwardProviderLicenceView() {
        return tvAwardProviderLicence;
    }

    @Override
    public TextView getAwardProviderBidAmountView() {
        return tvAwardProviderBidAmount;
    }

    @Override
    public TextView getAwardProviderHeader() {
        return tvAwardProviderHeader;
    }

    @Override
    public RatingBar getAwardedProviderRatingView() {
        return rbAwardedProvider;
    }

    @Override
    public TextView getTVEndView() {
        return tvEnd;
    }

    @Override
    public TextView getTVCancelView() {
        return tvCancel;
    }

    @Override
    public CardView getAwardProviderView() {
        return cvItem;
    }

    @Override
    public View getBidLayoutView() {
        return bidLayoutView;
    }

    @Override
    public View getCancelEndLayoutView() {
        return cancelEndView;
    }

    @Override
    public View getAcceptRejectLayoutView() {
        return acceptRejectView;
    }

    @Override
    public CircleImageView getAwardProviderImage() {
        return circleImageView;
    }

    @Override
    public MenuItem getEditMenuView() {
        return editMenu;
    }

    @Override
    public MenuItem getSaveMenuView() {
        return saveMenu;
    }

    @Override
    public MenuItem getEndMenuView() {
        return endMenu;
    }

    @Override
    protected AddEditProjectFragmentPresenterImpl onAttachPresenter() {
        return new AddEditProjectFragmentPresenterImpl(this, getActivity());
    }

}
