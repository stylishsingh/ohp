package com.ohp.ownerviews.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.view.MenuItem;
import android.view.View;

import com.library.mvp.ActivityPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.enums.CurrentScreen;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.view.OwnerDashboardView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

/**
 * @author Amanpal Singh.
 */

public class OwnerDashboardActivityPresenterImpl extends ActivityPresenter<OwnerDashboardView, APIHandler> implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private CurrentScreen currentScreen;

    public OwnerDashboardActivityPresenterImpl(OwnerDashboardView ownerDashboardView, Context context) {
        super(ownerDashboardView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return null;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Bundle bundle = null;
        switch (item.getItemId()) {
            /* Navigate to dashboard screen. */
            case R.id.nav_dashboard:
                if (getActivity() != null && currentScreen != CurrentScreen.PROPERTY_OWNER_DASHBOARD_SCREEN) {
                    currentScreen = CurrentScreen.PROPERTY_OWNER_DASHBOARD_SCREEN;
                    ((OwnerDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.title_dashboard);
                    ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.owner_dashboard_container, CurrentScreen.PROPERTY_OWNER_DASHBOARD_SCREEN, false, false, null);
                }
                break;

               /*Navigate to browse Service Provider List screen.*/
            case R.id.nav_service_providers:
                if (getActivity() != null && currentScreen != CurrentScreen.BROWSER_SERVICE_PROVIDER) {
                    currentScreen = CurrentScreen.BROWSER_SERVICE_PROVIDER;
                    ((OwnerDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.title_service_provider);
                    ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.owner_dashboard_container, CurrentScreen.BROWSER_SERVICE_PROVIDER, false, false, bundle);
                }
                break;
                /* Navigate to Document List screen. */
            case R.id.nav_documents:
                if (getActivity() != null && currentScreen != CurrentScreen.DOCUMENT_LIST_SCREEN) {
                    currentScreen = CurrentScreen.DOCUMENT_LIST_SCREEN;
                    bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    ((OwnerDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.title_document_listing);
                    ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.owner_dashboard_container, CurrentScreen.DOCUMENT_LIST_SCREEN, false, false, bundle);
                }
                break;
            /*Navigate to Appliance List screen*/
            case R.id.nav_appliances:
                if (getActivity() != null && currentScreen != CurrentScreen.APPLIANCE_LIST_SCREEN) {
                    currentScreen = CurrentScreen.APPLIANCE_LIST_SCREEN;
                    ((OwnerDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.title_appliance_list);
                    bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.owner_dashboard_container, CurrentScreen.APPLIANCE_LIST_SCREEN, false, false, bundle);
                }
                break;

            /*Navigate to Task List screen.*/
            case R.id.nav_tasks:
                if (getActivity() != null && currentScreen != CurrentScreen.TASK_LIST_SCREEN) {
                    currentScreen = CurrentScreen.TASK_LIST_SCREEN;
                    ((OwnerDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.title_task_list);
                    bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.owner_dashboard_container, CurrentScreen.TASK_LIST_SCREEN, false, false, bundle);
                }
                break;

            /*Navigate to Message List screen.*/
            case R.id.nav_messages:
                if (getActivity() != null && currentScreen != CurrentScreen.MESSAGE_LIST_SCREEN) {
                    currentScreen = CurrentScreen.MESSAGE_LIST_SCREEN;
                    ((OwnerDashboardActivity) getActivity()).getToolbarTitleView().setText("Message Listing");
                    bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.owner_dashboard_container, CurrentScreen.MESSAGE_LIST_SCREEN, false, false, bundle);
                }
                break;
            /*Navigate to Change password screen.*/
            case R.id.nav_change_password:
                if (getActivity() != null && currentScreen != CurrentScreen.CHANGE_PASSWORD) {
                    currentScreen = CurrentScreen.CHANGE_PASSWORD;
                    ((OwnerDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.title_change_password);
                    bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.owner_dashboard_container, CurrentScreen.CHANGE_PASSWORD, false, false, bundle);
                }
                break;

            case R.id.nav_about:
                if (getActivity() != null && currentScreen != CurrentScreen.ABOUT_SCREEN) {
                    currentScreen = CurrentScreen.ABOUT_SCREEN;
                    ((OwnerDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.about_ohp);
                    bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    bundle.putString(Constants.URL, Constants.URL_ABOUT_US);
                    ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.owner_dashboard_container, CurrentScreen.SETTING, false, false, bundle);
                }
                break;
            case R.id.nav_privacy_policies:
                if (getActivity() != null && currentScreen != CurrentScreen.PRIVACY_POLICY_SCREEN) {
                    currentScreen = CurrentScreen.PRIVACY_POLICY_SCREEN;
                    ((OwnerDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.privacy_policies);
                    bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    bundle.putString(Constants.URL, Constants.URL_PRIVACY_POLICY);
                    ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.owner_dashboard_container, CurrentScreen.SETTING, false, false, bundle);
                }
                break;
            case R.id.nav_terms_and_conditions:
                if (getActivity() != null && currentScreen != CurrentScreen.TERMS_AND_CONDITIONS_SCREEN) {
                    currentScreen = CurrentScreen.TERMS_AND_CONDITIONS_SCREEN;
                    ((OwnerDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.terms_and_conditions);
                    bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    bundle.putString(Constants.URL, Constants.URL_TERMS_AND_CONDITIONS);
                    ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.owner_dashboard_container, CurrentScreen.SETTING, false, false, bundle);
                }
                break;


            /*Open confirmation dialog which will take input from user to logout from app or not.*/
            case R.id.nav_logout:
                Utils.getInstance().confirmDialog(getActivity(), "Are you sure you want to Sign out?", "Sign Out", new Utils.ConfirmDialogCallbackInterface() {
                    @Override
                    public void onYesClick(String tag) {
                        if (isViewAttached()) {
                            ((OwnerDashboardActivity) getView()).logout("invalid");
                        }
                    }

                    @Override
                    public void onNoClick(String tag) {

                    }
                });
                break;
        }
        if (isViewAttached()) {
            getView().getDrawerView().closeDrawer(GravityCompat.START);
        }
        return true;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentScreen = CurrentScreen.PROPERTY_OWNER_DASHBOARD_SCREEN;
        ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.owner_dashboard_container, CurrentScreen.PROPERTY_OWNER_DASHBOARD_SCREEN, false, false, null);
    }


    public void onBackPressed() {
        if (isViewAttached() && getView().getDrawerView().isDrawerOpen(GravityCompat.START)) {
            getView().getDrawerView().closeDrawer(GravityCompat.START);
        } else {
            getView().backPress();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.header_layout:
                if (getActivity() != null && isViewAttached()) {
                    if (currentScreen != CurrentScreen.OWNER_PROFILE_SCREEN) {
                        currentScreen = CurrentScreen.OWNER_PROFILE_SCREEN;
                        ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.owner_dashboard_container, CurrentScreen.OWNER_PROFILE_SCREEN, false, false, null);
                    }
                    getView().getDrawerView().closeDrawer(GravityCompat.START);
                }
                break;
            case R.id.tv_name:
                if (getActivity() != null && isViewAttached()) {
                    if (currentScreen != CurrentScreen.OWNER_PROFILE_SCREEN) {
                        currentScreen = CurrentScreen.OWNER_PROFILE_SCREEN;
                        ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.owner_dashboard_container, CurrentScreen.OWNER_PROFILE_SCREEN, false, false, null);
                    }
                    getView().getDrawerView().closeDrawer(GravityCompat.START);
                }
                break;
        }
    }


}
