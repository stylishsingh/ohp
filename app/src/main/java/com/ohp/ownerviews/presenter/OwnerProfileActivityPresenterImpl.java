package com.ohp.ownerviews.presenter;

import android.content.Context;
import android.os.Bundle;

import com.library.mvp.ActivityPresenter;
import com.library.mvp.BaseView;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.enums.CurrentScreen;
import com.ohp.ownerviews.activities.OwnerProfileActivity;
import com.ohp.ownerviews.view.OwnerProfileActivityView;

/**
 * Created by Anuj Sharma on 5/25/2017.
 */

public class OwnerProfileActivityPresenterImpl extends ActivityPresenter<OwnerProfileActivityView, APIHandler> {

    public OwnerProfileActivityPresenterImpl(OwnerProfileActivityView view, Context context) {
        super(view, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Receive Intent and proceed
        /*if(getActivity().getIntent()!=null){

        }else{

        }*/
        Bundle bundle = new Bundle();
        if (getActivity().getIntent() != null) {
            bundle.putBoolean("is_edit", getActivity().getIntent().getBooleanExtra("is_edit", false));
        }
        if (bundle == null)
            ((OwnerProfileActivity) getActivity()).changeScreen(R.id.profile_container, CurrentScreen.OWNER_PROFILE_SCREEN, false, false, null);
        else
            ((OwnerProfileActivity) getActivity()).changeScreen(R.id.profile_container, CurrentScreen.OWNER_PROFILE_SCREEN, false, false, bundle);
    }
}
