package com.ohp.ownerviews.presenter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.activities.OwnerProfileActivity;
import com.ohp.ownerviews.beans.ProfileResponse;
import com.ohp.ownerviews.fragment.OwnerProfileFragment;
import com.ohp.ownerviews.view.OwnerProfileFragmentView;
import com.ohp.utils.Constants;
import com.ohp.utils.FileUtils;
import com.ohp.utils.PermissionsAndroid;
import com.ohp.utils.SharedPreferencesHandler;
import com.ohp.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * @author Amanpal Singh.
 */

public class OwnerProfileFragmentPresenterImpl extends FragmentPresenter<OwnerProfileFragmentView, APIHandler> implements
        APIHandler.OnCallableResponse, View.OnClickListener, ImagePickerCallback {
    private boolean isFirstTimeUser = false, isImageSelected = false, cameraSelected = false, gallerySelected = false;
    private String profileImagePath;
    private Place place;
    private String gender;
    private int PLACE_PICKER_REQUEST = 1;
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private String pickerPath;

    public OwnerProfileFragmentPresenterImpl(OwnerProfileFragmentView view, Context context) {
        super(view, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("picker_path")) {
                pickerPath = savedInstanceState.getString("picker_path");
            }
        }

        if (getActivity() != null && getView() != null) {
            if (getActivity() instanceof OwnerDashboardActivity) {
                ((OwnerDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.title_profile);
            } else if (getActivity() instanceof OwnerProfileActivity) {
                ((OwnerProfileActivity) getActivity()).getToolbarTitleView().setText(R.string.title_profile);
            }

            getView().getEmail().setEnabled(false);
            getView().getEmail().setVisibility(View.VISIBLE);

            if (isFirstTimeUser) {
                enableDisableUI(true);
            } else {
                enableDisableUI(true);
            }

            //set radio group click listener
            getView().getRadioGroup().setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                    switch (checkedId) {
                        case R.id.radio_male:
                            getView().getMaleRadio().setChecked(true);
                            getView().getFemaleRadio().setChecked(false);
                            gender = "male";
                            break;
                        case R.id.radio_female:
                            getView().getMaleRadio().setChecked(false);
                            getView().getFemaleRadio().setChecked(true);
                            gender = "female";
                            break;
                    }
                }
            });

            /*
            Hit API to get Profile Detail
             */
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                getActivity().showProgressDialog();
                getInterActor().getUserProfile(param, ApiName.GET_USER_PROFILE_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    /*
    Save BUndle Info
     */
    public void saveBundleInfo(Bundle bundle) {
        isFirstTimeUser = bundle.getBoolean("is_edit");
        if (isFirstTimeUser) {
            //din't show back button
        }
    }

    public boolean saveProfileInfo() {
        if (getActivity() != null && isViewAttached() && Utils.getInstance().isInternetAvailable(getActivity())) {
            if (getView().getName().getText().toString().trim().length() == 0) {
                Utils.getInstance().showToast("Please enter full name");
//                getActivity().showSnakBar(getView().getRootView(), "Please enter full name");
            } else if (isImageSelected && !TextUtils.isEmpty(profileImagePath)) {
                //Profile Image is selected by user
                String fullName = getView().getName().getText().toString().replaceAll("\\s+", " ");
                Map<String, String> param = new HashMap<>();
                param.put("access_token", Utils.getInstance().getAccessToken(getActivity()));
                param.put("full_name", fullName);
                if (place != null) {
                    param.put("address", place.getAddress().toString());
                    if (place.getLocale() != null) {
                        param.put("city", place.getLocale().getDisplayName());
                        param.put("state", "dummy state");
                    }
                    if (place.getLatLng() != null) {
                        param.put("latitude", String.valueOf(place.getLatLng().latitude));
                        param.put("longitude", String.valueOf(place.getLatLng().longitude));
                    }
                }
                if (TextUtils.isEmpty(gender))
                    param.put("gender", "male");
                else
                    param.put("gender", gender);

                getActivity().showProgressDialog();
                getInterActor().saveUserProfileWithPic(param, profileImagePath, ApiName.SAVE_USER_PROFILE_API);

            } else {
                //Profile Image is selected by user
                String fullName = getView().getName().getText().toString().replaceAll("\\s+", " ");

                Map<String, String> param = new HashMap<>();
                param.put("access_token", Utils.getInstance().getAccessToken(getActivity()));
                param.put("full_name", fullName);
                if (place != null) {
                    param.put("address", place.getAddress().toString());
                    if (place.getLocale() != null) {
                        param.put("city", place.getLocale().getDisplayName());
                        param.put("state", "dummy state");
                    }
                    if (place.getLatLng() != null) {
                        param.put("latitude", String.valueOf(place.getLatLng().latitude));
                        param.put("longitude", String.valueOf(place.getLatLng().longitude));
                    }
                }
                if (TextUtils.isEmpty(gender))
                    param.put("gender", "male");
                else
                    param.put("gender", gender);

                getActivity().showProgressDialog();
                getInterActor().saveUserProfile(param, ApiName.SAVE_USER_PROFILE_API);

                return true;
            }
        } else {
            if (getActivity() != null && isViewAttached())
                getActivity().showSnackBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
        }
        return false;
    }


    @Override
    public void onClick(View v) {
        if (getActivity() != null && getView() != null) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.edit_profile:
                    // show picker for image capture
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String[] options = {"Camera", "Gallery", "Cancel"};
                    final Bundle bundle = new Bundle();
                    builder.setTitle("Choose Option")
                            .setItems(options, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    switch (which) {
                                        case 0:
                                            cameraSelected = true;
                                            gallerySelected = false;
                                            checkCameraPermission();
                                            break;
                                        case 1:
                                            cameraSelected = false;
                                            gallerySelected = true;
                                            checkCameraPermission();
                                            break;
                                        case 2:
                                            dialog.dismiss();
                                            break;
                                    }
                                }
                            });
                    builder.setCancelable(false);
                    builder.show();
                    break;
                case R.id.profile_img:

                    break;
                case R.id.et_address:
                    //Move to Search Address Fragment
                    getActivity().showProgressDialog();
                    try {
                        PlacePicker.IntentBuilder placeBuilder = new PlacePicker.IntentBuilder();
                        getActivity().startActivityForResult(placeBuilder.build(getActivity()), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                        getActivity().hideProgressDialog();
                    }
                    break;

            }
        }

    }

    //  @TargetApi(Build.VERSION_CODES.M)
    private void checkCameraPermission() {
        boolean isExternalStorage = PermissionsAndroid.getInstance().checkCameraPermission(getActivity());
        if (!isExternalStorage) {
            PermissionsAndroid.getInstance().requestForCameraPermission(((OwnerProfileFragment) getView()));
        } else if (cameraSelected) {
            takePicture();
        } else if (gallerySelected) {
            pickImageSingle();
        }
    }


    private void takePicture() {
        cameraPicker = new CameraImagePicker(getActivity());
//        cameraPicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        cameraPicker.setImagePickerCallback(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        pickerPath = cameraPicker.pickImage();
    }

    private void pickImageSingle() {
        imagePicker = new ImagePicker(getActivity());
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (getActivity() != null && isViewAttached()) {
            getActivity().hideProgressDialog();
            if (resultCode == RESULT_OK) {
                if (requestCode == PLACE_PICKER_REQUEST) {
                    place = PlacePicker.getPlace(data, getActivity());
                    if (place != null) {
                        getView().getAddress().setText(place.getAddress());
                    }
                } else if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                    if (imagePicker == null) {
                        imagePicker = new ImagePicker(getActivity());
                    }
                    imagePicker.submit(data);
                } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                    if (cameraPicker == null) {
                        cameraPicker = new CameraImagePicker(getActivity());
                        cameraPicker.setImagePickerCallback(this);
                        cameraPicker.reinitialize(pickerPath);
                    }
                    cameraPicker.submit(data);
                }
            } else {
//                Utils.getInstance().showToast("Request cancelled");
            }
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_USER_PROFILE_API:
                        if (response.isSuccessful()) {
                            ProfileResponse profileObj = (ProfileResponse) response.body();

                            switch (profileObj.getStatus()) {
                                case Constants.STATUS_200:
                                    updateView(profileObj);
                                    break;
                                case Constants.STATUS_201:
                                    Utils.getInstance().showToast(profileObj.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    if (isViewAttached()) {
                                        if (getActivity() instanceof OwnerProfileActivity)
                                            ((OwnerProfileActivity) getActivity()).logout(profileObj.getMessage());
                                        else if (getActivity() instanceof OwnerDashboardActivity)
                                            ((OwnerDashboardActivity) getActivity()).logout(profileObj.getMessage());
                                    }
                                    break;
                            }
                        }
                        break;
                    case SAVE_USER_PROFILE_API:
                        if (response.isSuccessful()) {
                            ProfileResponse saveObj = (ProfileResponse) response.body();

                            switch (saveObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(saveObj);
                                    break;
                                case Constants.STATUS_201:
                                    ((OwnerProfileFragment) getView()).editMenu.setVisible(false);
                                    ((OwnerProfileFragment) getView()).saveMenu.setVisible(true);
                                    break;
                                case Constants.STATUS_202:
                                    if (isViewAttached()) {
                                        if (getActivity() instanceof OwnerProfileActivity)
                                            ((OwnerProfileActivity) getActivity()).logout(saveObj.getMessage());
                                        else if (getActivity() instanceof OwnerDashboardActivity)
                                            ((OwnerDashboardActivity) getActivity()).logout(saveObj.getMessage());
                                    }
                                    break;
                            }

                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(ProfileResponse saveObj) {
        SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_fullname), saveObj.getData().getFullName());
        SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_email), saveObj.getData().getEmail());
        SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_profile_pic), saveObj.getData().getProfileImage200X200());
        //Change Menu option and make all fields non-editable
        getView().getName().setText(saveObj.getData().getFullName());
        if (isFirstTimeUser) {
            //Navigate to property owner Dashboard
            Intent intent = new Intent(getActivity(), OwnerDashboardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            getActivity().startActivity(intent);
        }
        if (getActivity() instanceof OwnerDashboardActivity) {
            ((OwnerDashboardActivity) getActivity()).showUserInfo();
            enableDisableUI(false);
            ((OwnerProfileFragment) getView()).editMenu.setVisible(true);
            ((OwnerProfileFragment) getView()).saveMenu.setVisible(false);
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t != null && t.getMessage() != null)
                    Utils.getInstance().showToast(t.getMessage());
//                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    Utils.getInstance().showToast(getActivity().getString(R.string.error_api));
//                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param isEditable if true make all fields editable else non-editable
     */
    public void enableDisableUI(boolean isEditable) {
        if (getActivity() != null && isViewAttached()) {
            //apply animation
            if (Utils.getInstance().isEqualLollipop()) {
                TransitionManager.beginDelayedTransition((ViewGroup) getView().getRootView(), new AutoTransition());
            }
            if (isEditable) {
                getView().getName().setEnabled(true);
                getView().getAddress().setEnabled(true);
                getView().getProfileImg().setEnabled(true);
                getView().getEditProfile().show();
                getView().getRadioGroup().setEnabled(true);
                getView().getMaleRadio().setEnabled(true);
                getView().getFemaleRadio().setEnabled(true);
                getView().getFullNameLayoutView().setEnabled(true);
                getView().tvHeaderGender().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getEmail().setVisibility(View.VISIBLE);
            } else {
                getView().getName().setEnabled(false);
                getView().getAddress().setEnabled(false);
                getView().getProfileImg().setEnabled(false);
                getView().getEditProfile().hide();
                getView().getRadioGroup().setEnabled(false);
                getView().getMaleRadio().setEnabled(false);
                getView().getFemaleRadio().setEnabled(false);
                getView().getFullNameLayoutView().setEnabled(false);
                getView().getEmail().setVisibility(View.VISIBLE);
                getView().tvHeaderGender().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            }
        }
    }

    private void updateView(ProfileResponse profileObj) {
        if (getActivity() != null && isViewAttached()) {
            if (!TextUtils.isEmpty(profileObj.getData().getProfileImage200X200())) {
                // set image
                Picasso.with(getActivity()).load(profileObj.getData().getProfileImage200X200()).into(getView().getProfileImg(), new Callback() {
                    @Override
                    public void onSuccess() {
                        if (getActivity() != null && isViewAttached())
                            getView().getProfileProgress().setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        if (getActivity() != null && isViewAttached()) {
                            getView().getProfileImg().setImageResource(R.drawable.profile_img);
                            getView().getProfileProgress().setVisibility(View.GONE);
                        }
                    }
                });
            } else {
                //show default image
                getView().getProfileImg().setImageResource(R.drawable.profile_img);
            }
            getView().getName().setText(profileObj.getData().getFullName());
            getView().getEmail().setText(profileObj.getData().getEmail());

            //set gender selectable
            if (!TextUtils.isEmpty(profileObj.getData().getGender())) {
                if (profileObj.getData().getGender().equals("male")) {
                    getView().getMaleRadio().setChecked(true);
                    getView().getFemaleRadio().setChecked(false);
                    gender = "male";
                } else if (profileObj.getData().getGender().equals("female")) {
                    getView().getMaleRadio().setChecked(false);
                    getView().getFemaleRadio().setChecked(true);
                    gender = "female";
                }
            }
            if (!TextUtils.isEmpty(profileObj.getData().getAddress()))
                getView().getAddress().setText(profileObj.getData().getAddress());
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        String extension = "";
        int i = list.get(0).getDisplayName().lastIndexOf('.');
        if (i > 0) {
            extension = list.get(0).getDisplayName().substring(i + 1);
        }
        if (!extension.equals("png") && !extension.equals("jpg") && !extension.equals("jpeg")) {
            getActivity().showSnakBar(getView().getRootView(), "you can choose only images.");
            return;
        }
        ChosenImage image = list.get(0);
        isImageSelected = true;
        profileImagePath = FileUtils.getPath(getActivity(), Uri.parse(image.getQueryUri()));
        getView().getProfileProgress().setVisibility(View.VISIBLE);
        Picasso.with(getActivity()).load(new File(profileImagePath)).into(getView().getProfileImg(), new Callback() {
            @Override
            public void onSuccess() {
                getView().getProfileProgress().setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                getView().getProfileProgress().setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onError(String s) {

    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 102: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Show option to update profile
                    if (cameraSelected) {
                        takePicture();
                    } else if (gallerySelected) {
                        pickImageSingle();
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Utils.getInstance().showToast("Camera/Storage permission Denied");
                }
                return;
            }

        }
    }
}
