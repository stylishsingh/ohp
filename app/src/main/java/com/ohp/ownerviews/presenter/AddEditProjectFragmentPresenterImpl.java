package com.ohp.ownerviews.presenter;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.FilePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.FilePickerCallback;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenFile;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.ApplianceSpinnerAdapter;
import com.ohp.commonviews.adapter.JobTypeExpertiseSpinnerAdapter;
import com.ohp.commonviews.adapter.PropertySpinnerAdapter;
import com.ohp.commonviews.adapter.ThumbImageAdapter;
import com.ohp.commonviews.adapter.ViewPagerSubCatAdapter;
import com.ohp.commonviews.beans.AddProjectModelResponse;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.beans.GetPropertyResponse;
import com.ohp.commonviews.beans.JobTypeAndExpertise;
import com.ohp.commonviews.beans.ProjectDetailResponse;
import com.ohp.commonviews.beans.ThumbnailBean;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.adapter.BidsServiceAdapter;
import com.ohp.ownerviews.fragment.AddEditProjectFragment;
import com.ohp.ownerviews.fragment.ReasonFeedbackDialogFragment;
import com.ohp.ownerviews.view.ProjectDetailView;
import com.ohp.service.ImagesResultReceiver;
import com.ohp.service.ImagesService;
import com.ohp.utils.Constants;
import com.ohp.utils.FileUtils;
import com.ohp.utils.PermissionsAndroid;
import com.ohp.utils.Utils;
import com.ohp.utils.Validations;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.ohp.R.id.spinner_property;
import static com.ohp.enums.ApiName.GET_APPLIANCE;
import static com.ohp.enums.ApiName.GET_PROPERTIES_LIST;
import static com.ohp.enums.ApiName.GET_RESOURCES_API;
import static com.ohp.utils.Constants.CLICK_ADD_PROJECT;
import static com.ohp.utils.Constants.CLICK_EDIT_PROJECT;
import static com.ohp.utils.Constants.CLICK_VIEW_PROJECT;
import static com.ohp.utils.Constants.NAVIGATE_FROM;

/**
 * @author Vishal Sharma.
 */

public class AddEditProjectFragmentPresenterImpl extends FragmentPresenter<ProjectDetailView, APIHandler> implements APIHandler.OnCallableResponse,
        ImagePickerCallback, AdapterView.OnItemSelectedListener, ImagesResultReceiver.Receiver, View.OnClickListener, FilePickerCallback {
    private GetPropertyResponse propertyResponse;
    private int selectedJobType = 0;
    private boolean bgApp = false;
    private List<ProjectDetailResponse.DataBean.BidsBean> listBids = new ArrayList<>();
    private String pickerPath, mProjectName = "", mProjectDescription = "", mProjectAppliance = "", mProjectType = "", mProjectJobType = "", mProjectCreatedOn = "", mProjectExpertise = "", mProjectSelectProperty = ";";
    private PropertySpinnerAdapter propertyAdapter;
    private JobTypeExpertiseSpinnerAdapter jobTypeExpertiseAdapter;
    private List<JobTypeAndExpertise.DataBean.InfoBean> listExpertise = new ArrayList<>(), listJobType = new ArrayList<>();
    private GetPropertyResponse listModel;
    private ApplianceSpinnerAdapter adapter;
    private String projectID;
    private final BroadcastReceiver projectDetailReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null && intent.getExtras().containsKey(Constants.NAVIGATE_FROM)
                    && Constants.NOTIFICATIONS.equalsIgnoreCase(intent.getExtras().getString(NAVIGATE_FROM))) {
                if (projectID.equalsIgnoreCase(intent.getExtras().getString(Constants.PROJECT_ID)))
                    getProjectDetail(projectID);
            } else
                getProjectDetail(projectID);
            Intent listIntent = new Intent(Constants.GET_PROJECTS);
            context.sendBroadcast(listIntent);

        }
    };
    private String selectedExpertiseName = "";
    private String currentScreen = "";
    private boolean[] arraySelectionExpertise;
    private ArrayAdapter expertiseAdapter;
    private String mSelectedExpertise = "";
    private String[] detailExpertise;
    private String expertiseName = "";
    public MultiSelectSpinner.MultiSpinnerListener expertiseSpinnerListener = new MultiSelectSpinner.MultiSpinnerListener() {

        public void onItemsSelected(boolean[] selected) {
            mSelectedExpertise = "";
            expertiseName = "";
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    if (mSelectedExpertise.trim().isEmpty()) {
                        mSelectedExpertise = String.valueOf(listExpertise.get(i).getId());
                        expertiseName = listExpertise.get(i).getValue();
                    } else {
                        mSelectedExpertise = mSelectedExpertise + "," + String.valueOf(listExpertise.get(i).getId());
                        expertiseName = expertiseName + "," + listExpertise.get(i).getValue();
                    }
                }
            }

            if (getActivity() != null && isViewAttached()) {
                if (mSelectedExpertise.isEmpty()) {
                    mSelectedExpertise = String.valueOf(listExpertise.get(0).getId());
                    expertiseName = listExpertise.get(0).getValue();
                    arraySelectionExpertise[0] = true;
                    getView().getExpertiseSpinnerView().setAllCheckedText(expertiseName);
                    getView().getExpertiseSpinnerView().selectItem(0, arraySelectionExpertise[0]);
                } else {
                    if (getView().getExpertiseSpinnerView().isSelectAll())
                        getView().getExpertiseSpinnerView().setAllCheckedText(expertiseName);
                }
            }
        }
    };
    private List<GetPropertyResponse.DataBean> applianceList = new ArrayList<>();
    private List<GetPropertyResponse.DataBean> propertyList = new ArrayList<>();
    private ThumbImageAdapter detailImageAdapter, editImageAdapter;
    private List<ThumbnailBean> detailImageList = new ArrayList<>();
    private List<ThumbnailBean> uploadImageList = new ArrayList<>();
    private List<String> deletedImageIds = new ArrayList<>();
    private List<ThumbnailBean> tempUploadImageList = new ArrayList<>();
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private String propertyID;
    private String mApplianceID = "", mPropertyId = "";
    private String propertyId = "";
    private boolean isCameraSelected = false, isGallerySelected = false, forDocumentSection = false;
    private FilePicker filePicker;
    private boolean isImageSelected = false;
    private ViewPagerSubCatAdapter imgAdapter;
    private BidsServiceAdapter bidsServiceAdapter;
    private ProjectDetailResponse.DataBean.ProjectProviderBean providerObj;

    public AddEditProjectFragmentPresenterImpl(ProjectDetailView projectDetailView, Context context) {
        super(projectDetailView, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(projectDetailReceiver, new IntentFilter(Constants.PROJECT_DETAIL));
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");

            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    if (bgApp)
                        ((HolderActivity) getActivity()).moveToParentActivity();
                    else
                        ((HolderActivity) getActivity()).oneStepBack();
                }
            });
        }

        //Initialize RecyclerView for Detail View
        if (getActivity() != null && isViewAttached()) {
            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getProjectDetailRecycler().setLayoutManager(lm);
            detailImageAdapter = new ThumbImageAdapter(getActivity(), null, this, true, false);
            getView().getProjectDetailRecycler().setAdapter(detailImageAdapter);


            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getSpBidsRecyclerView().setLayoutManager(linearLayoutManager);


            if (uploadImageList == null) uploadImageList = new ArrayList<>();
            LinearLayoutManager lm1 = new LinearLayoutManager(getContext());
            lm1.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getEditProjectDetailRecycler().setLayoutManager(lm1);

            ThumbnailBean obj = new ThumbnailBean();
            obj.setAddView(true);
            obj.setFile(false);
            obj.setImagePath("");
            uploadImageList.add(obj);
            editImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, true);
            getView().getEditProjectDetailRecycler().setAdapter(editImageAdapter);


            if (detailImageList == null) detailImageList = new ArrayList<>();
            ThumbnailBean obj1 = new ThumbnailBean();
            obj1.setAddView(true);
            obj1.setFile(false);
            obj1.setImagePath("");
            detailImageList.add(obj1);
            detailImageAdapter = new ThumbImageAdapter(getActivity(), detailImageList, this, false, false);
            getView().getProjectDetailRecycler().setAdapter(detailImageAdapter);


            adapter = new ApplianceSpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, applianceList);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getApplianceName().setAdapter(adapter);

        }

        //set scroll listener on address field
        getView().getProjectDescription().setOnTouchListener(new View.OnTouchListener() {  //Register a callback to be invoked when a touch event is sent to this view.
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Called when a touch event is dispatched to a view. This allows listeners to get a chance to respond before the target view.
                getView().getNestedScrollView().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        getView().getJobType().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                return false;
            }
        });
        getView().getProperty().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                return false;
            }
        });
        getView().getApplianceName().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                return false;
            }
        });
        getView().getExpertiseSpinnerView().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                return false;
            }
        });
    }

    public void visibilityGoneUiForSaveProject() {
        getView().getSaveProject().setVisibility(View.VISIBLE);
        getView().getEditProject().setVisibility(View.GONE);
    }

    private void visibilityGoneUiForEditProject() {
        getView().getSaveProject().setVisibility(View.GONE);
        getView().getEditProject().setVisibility(View.VISIBLE);
    }


    private void getProjectDetail(String projectID) {
        try {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> params = new HashMap<>();
                params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                params.put(Constants.PROJECT_ID, projectID);
                getInterActor().getProjectDetail(params, ApiName.GET_PROJECT_DETAIL);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            getActivity().hideProgressDialog();
            switch (api) {
                case GET_PROPERTIES_LIST:
                    if (response.isSuccessful()) {
                        getActivity().hideProgressDialog();
                        propertyResponse = (GetPropertyResponse) response.body();
                        switch (propertyResponse.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(propertyResponse, GET_PROPERTIES_LIST);
                                break;
                            case Constants.STATUS_201:
                                if (propertyAdapter != null) {
                                    getView().getProperty().setVisibility(View.VISIBLE);
                                    propertyAdapter.updateList(getActivity(), propertyList);
                                }
                                break;
                            case Constants.STATUS_202:
                                ((HolderActivity) getActivity()).logout(propertyResponse.getMessage());
                                break;
                        }
                    }
                    break;
                case GET_RESOURCES_API:
                    if (response.isSuccessful()) {
                        getActivity().hideProgressDialog();
                        JobTypeAndExpertise resourcesObj = (JobTypeAndExpertise) response.body();
                        switch (resourcesObj.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(resourcesObj);
                                break;
                            case Constants.STATUS_201:
                                if (jobTypeExpertiseAdapter != null) {
                                    jobTypeExpertiseAdapter.updateList(getActivity(), listJobType);
                                }
                                break;
                            case Constants.STATUS_202:
                                ((HolderActivity) getActivity()).logout(resourcesObj.getMessage());
                                break;
                        }
                    }
                    break;
                case GET_APPLIANCE:
                    if (response.isSuccessful()) {
                        getActivity().hideProgressDialog();
                        listModel = (GetPropertyResponse) response.body();
                        switch (listModel.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(listModel, GET_APPLIANCE);
                                break;
                            case Constants.STATUS_201:
                                // getActivity().showSnakBar(getView().getRootView(), listModel.getMessage());
                                if (adapter != null) {
                                    getView().getApplianceName().setVisibility(View.VISIBLE);
                                    adapter.updateList(getActivity(), listModel.getData());
                                }
                                break;
                            case Constants.STATUS_202:
                                if (getActivity() instanceof OwnerDashboardActivity)
                                    ((OwnerDashboardActivity) getActivity()).logout(listModel.getMessage());
                                else if (getActivity() instanceof HolderActivity)
                                    ((HolderActivity) getActivity()).logout(listModel.getMessage());
                                break;
                        }
                    }
                    break;
                case ADD_EDIT_PROJECT:
                    if (response.isSuccessful()) {
                        getActivity().hideProgressDialog();
                        AddProjectModelResponse addProjectModelResponse = (AddProjectModelResponse) response.body();
                        switch (addProjectModelResponse.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(addProjectModelResponse);
                                break;
                            case Constants.STATUS_201:
                                break;
                            case Constants.STATUS_202:
                                ((HolderActivity) getActivity()).logout(addProjectModelResponse.getMessage());
                                break;
                            case Constants.STATUS_203:
                                getActivity().showSnackBar(getView().getRootView(), addProjectModelResponse.getMessage());
                                break;
                        }
                    }
                    break;
                case GET_PROJECT_DETAIL:
                    if (response.isSuccessful()) {
                        ProjectDetailResponse projectDetailResponse = (ProjectDetailResponse) response.body();
                        switch (projectDetailResponse.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(projectDetailResponse);
                                break;
                            case Constants.STATUS_201:
                                break;
                            case Constants.STATUS_202:
                                ((HolderActivity) getActivity()).logout(projectDetailResponse.getMessage());
                                break;
                        }
                        propertiesList();
                        getResource();
                    }
                    break;
                case BID_ACCEPT_REJECT:
                    if (response.isSuccessful()) {
                        GenericBean dataResponse = (GenericBean) response.body();
                        switch (dataResponse.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(dataResponse);
                                break;
                            case Constants.STATUS_201:
                                break;
                            case Constants.STATUS_202:
                                ((HolderActivity) getActivity()).logout(dataResponse.getMessage());
                                break;
                        }
                    }
                    break;

                case CANCEL_PROJECT:
                    if (response.isSuccessful()) {
                        GenericBean dataResponse = (GenericBean) response.body();
                        switch (dataResponse.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(dataResponse);
                                break;
                            case Constants.STATUS_201:
                                break;
                            case Constants.STATUS_202:
                                ((HolderActivity) getActivity()).logout(dataResponse.getMessage());
                                break;
                        }
                    }
                    break;
                case END_PROJECT:
                    if (response.isSuccessful()) {
                        GenericBean dataResponse = (GenericBean) response.body();
                        switch (dataResponse.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(dataResponse);
                                break;
                            case Constants.STATUS_201:
                                break;
                            case Constants.STATUS_202:
                                ((HolderActivity) getActivity()).logout(dataResponse.getMessage());
                                break;
                        }
                    }
                    break;

                case MARK_NOT_COMPLETED_PROJECT:
                    if (response.isSuccessful()) {
                        GenericBean dataResponse = (GenericBean) response.body();
                        switch (dataResponse.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(dataResponse);
                                break;
                            case Constants.STATUS_201:
                                break;
                            case Constants.STATUS_202:
                                ((HolderActivity) getActivity()).logout(dataResponse.getMessage());
                                break;
                        }
                    }
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(GenericBean dataResponse) {
        if (getActivity() != null && isViewAttached()) {
            clearData();
            Intent intent = new Intent(Constants.PROJECT_DETAIL);
            getActivity().sendBroadcast(intent);
        }
    }

    private void onSuccess(final ProjectDetailResponse dataObj) {
        //set data to detail page view
        try {
            if (getActivity() != null && isViewAttached()) {

                if (listJobType != null && listJobType.size() > 0) {
                    for (int i = 0; i < listJobType.size(); i++) {
                        if (listJobType.get(i).getId() == selectedJobType) {
                            getView().getJobType().setSelection(i);
                        }
                    }
                }

                getView().getPropertyName().setText(dataObj.getData().getProperty().getPropertyName());
                getView().getPropertyAddress().setText(dataObj.getData().getProperty().getPropertyAddress());
                getView().getProjectDescriptionEdit().setText(dataObj.getData().getProjectDescription());
                getView().getProjectNameEdit().setText(dataObj.getData().getProjectName());
                System.out.println("project name" + dataObj.getData().getProjectName());
                getView().getProjectCreatedOn().setText(Utils.getInstance().getYMDProjectFormattedDateTime(dataObj.getData().getCreated().replace("+00:00", "")));
                getView().getProjectJopType().setText(dataObj.getData().getProjectJobtype().getJobtypeTitle());
                if (TextUtils.isEmpty(dataObj.getData().getProjectAppliance().getApplianceName())) {
                    getView().getApplianceDescription().setText("No Appliance");
                } else {
                    getView().getApplianceDescription().setText(dataObj.getData().getProjectAppliance().getApplianceName());
                }

                selectedJobType = dataObj.getData().getProjectJobtype().getJobtypeId();

                detailExpertise = new String[dataObj.getData().getProjectExpertise().size()];
                for (int i = 0; i < dataObj.getData().getProjectExpertise().size(); i++) {
                    detailExpertise[i] = String.valueOf(dataObj.getData().getProjectExpertise().get(i).getExpertiseId());
                }


                if (TextUtils.isEmpty(dataObj.getData().getProperty().getPropertyImage())) {
                    getView().getProgressBar().setVisibility(View.GONE);
                    getView().getPropertyImage().setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.default_property));
                } else {
                    Target target = new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            if (null != bitmap && (dataObj.getData().getProperty().getPropertyImage() != null))
                                getView().getPropertyImage().setImageBitmap(bitmap);
                            getView().getProgressBar().setVisibility(View.GONE);
                            System.out.println("Bitmap Loaded successfully");
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            getView().getProgressBar().setVisibility(View.GONE);
                            System.out.println("Bitmap Load failed");
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            getView().getProgressBar().setVisibility(View.GONE);
                            System.out.println("Bitmap Loading");
                        }
                    };
                    getView().getPropertyImage().setTag(target);
                    System.out.println("image>>>>>" + dataObj.getData().getProperty().getPropertyImage());
                    Picasso.with(getContext()).load(dataObj.getData().getProperty().getPropertyImage()).into(target);
                }

                if (dataObj.getData().getProjectImages() != null && dataObj.getData().getProjectImages().size() > 0) {
                    if (detailImageList == null) detailImageList = new ArrayList<>();
                    if (detailImageList.size() > 0) detailImageList.clear();
                    if (!uploadImageList.isEmpty()) {
                        uploadImageList.clear();
                    }

                    for (int i = 0; i < dataObj.getData().getProjectImages().size(); i++) {
                        ThumbnailBean obj = new ThumbnailBean();
                        obj.setAddView(false);
                        obj.setImageID(String.valueOf(dataObj.getData().getProjectImages().get(i).getProjectImageId()));
                        obj.setFile(false);
                        obj.setCheck(ScreenNavigation.OLD_ITEM);
                        obj.setImagePath(dataObj.getData().getProjectImages().get(i).getProjectImageAvatar());
                        detailImageList.add(obj);
                        uploadImageList.add(obj);
                    }

                    // Add View after All Info in uploadImageList for adding data in edit view
                    ThumbnailBean obj = new ThumbnailBean();
                    obj.setAddView(true);
                    obj.setFile(false);
                    obj.setImagePath("");
                    obj.setCheck(ScreenNavigation.DUMMY_ITEM);
                    uploadImageList.add(obj);

                    System.out.println("Images List" + detailImageList.size());
                    System.out.println("Upload Images List" + uploadImageList.size());
                    detailImageAdapter.updateList(detailImageList);
                    editImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, true);
                    getView().getEditProjectDetailRecycler().setAdapter(editImageAdapter);
                } else {
                    getView().NoImages().setVisibility(View.VISIBLE);
                }

                //set data to edit page view
                getView().getProjectName().setText(dataObj.getData().getProjectName());
                getView().getProjectDescription().setText(dataObj.getData().getProjectDescription());


                //show selected property
                propertyID = String.valueOf(dataObj.getData().getProperty().getPropertyId());

                if (propertyList != null && propertyList.size() > 0) {
                    for (int i = 0; i < propertyList.size(); i++) {
                        if ((String.valueOf(propertyList.get(i).getValue())).equals(propertyID)) {

                            getView().getProperty().setSelection(i);
                        }
                    }
                }

                //show selected appliance
                int applianceID = dataObj.getData().getProjectAppliance().getApplianceId();
                if (applianceList != null && applianceList.size() > 0) {
                    for (int i = 0; i < applianceList.size(); i++) {
                        if (applianceList.get(i).getValue() == applianceID) {
                            getView().getApplianceName().setSelection(i);
                        }
                    }
                }


                //Bids data....
                if (dataObj.getData().getBids().size() > 0) {
                    getView().getBidLayoutView().setVisibility(View.VISIBLE);
                    getView().NoBids().setVisibility(View.GONE);
                    if (listBids == null)
                        listBids = new ArrayList<>();
                    listBids = dataObj.getData().getBids();

                    bidsServiceAdapter = new BidsServiceAdapter(getActivity(), listBids, this);
                    getView().getSpBidsRecyclerView().setAdapter(bidsServiceAdapter);

                } else {
                    switch (dataObj.getData().getProjectStatus()) {
                        case 2:
                            showProviderDetail(dataObj);
                            getView().getCancelEndLayoutView().setVisibility(View.VISIBLE);
                            getView().getAcceptRejectLayoutView().setVisibility(View.GONE);
                            break;
                        case 3:
                            showProviderDetail(dataObj);
                            getView().getCancelEndLayoutView().setVisibility(View.GONE);
                            getView().getAcceptRejectLayoutView().setVisibility(View.GONE);
                            break;
                        case 5:
                            showProviderDetail(dataObj);
                            getView().getCancelEndLayoutView().setVisibility(View.GONE);
                            getView().getAcceptRejectLayoutView().setVisibility(View.GONE);
                            break;
                        case 6:
                            showProviderDetail(dataObj);
                            getView().getCancelEndLayoutView().setVisibility(View.GONE);
                            getView().getAcceptRejectLayoutView().setVisibility(View.VISIBLE);
                            break;
                        default:
                            getView().getAwardProviderHeader().setVisibility(View.GONE);
                            getView().getAwardProviderView().setVisibility(View.GONE);
                            getView().getBidLayoutView().setVisibility(View.GONE);
                            getView().getCancelEndLayoutView().setVisibility(View.GONE);
                            getView().getAcceptRejectLayoutView().setVisibility(View.GONE);
                            getView().NoBids().setVisibility(View.VISIBLE);
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProviderDetail(ProjectDetailResponse dataObj) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getView().getEditMenuView().setVisible(false);
                getView().getSaveMenuView().setVisible(false);
                providerObj = dataObj.getData().getProjectProvider();
                getView().NoBids().setVisibility(View.GONE);
                getView().getAwardProviderHeader().setVisibility(View.VISIBLE);
                if (dataObj.getData().getProjectProvider().getAvgRating() != 0)
                    getView().getAwardedProviderRatingView().setRating(Float.parseFloat(String.valueOf(dataObj.getData().getProjectProvider().getAvgRating())));
                getView().getBidLayoutView().setVisibility(View.GONE);
                getView().getAwardProviderView().setVisibility(View.VISIBLE);
                getView().getAwardProviderNameView().setText(dataObj.getData().getProjectProvider().getFullName());
                getView().getAwardProviderBidAmountView().setText(String.format("%s (USD)", String.valueOf(dataObj.getData().getProjectProvider().getBidAmount())));
                getView().getAwardProviderDistanceView().setText(String.format("%s miles", String.valueOf(dataObj.getData().getProjectProvider().getAwayInKms())));
                if (dataObj.getData().getProjectProvider().getIsLicensed() == 1) {
                    getView().getAwardProviderLicenceView().setVisibility(View.VISIBLE);
                    getView().getAwardProviderLicenceView().setText(dataObj.getData().getProjectProvider().getLicenseNo());
                    getView().getAwardProviderLicenceView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_green, 0, 0, 0);
                } else {
                    getView().getAwardProviderLicenceView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_gray, 0, 0, 0);
                }
                if (dataObj.getData().getProjectProvider().getExpertise().size() > 0) {
                    StringBuilder expertise = new StringBuilder();
                    for (int i = 0; i < dataObj.getData().getProjectProvider().getExpertise().size(); i++) {

                        if (i < (dataObj.getData().getProjectProvider().getExpertise().size() - 1)) {
                            expertise.append(dataObj.getData().getProjectProvider().getExpertise().get(i).getExpertiseTitle()).append(", ");
                        } else {
                            expertise.append(dataObj.getData().getProjectProvider().getExpertise().get(i).getExpertiseTitle());
                        }
                    }
                    getView().getAwardProviderExpertiseView().setText(expertise);
                }

                if (!TextUtils.isEmpty(dataObj.getData().getProjectProvider().getProfileImage200X200())) {
                    // set image
                    Picasso.with(getActivity()).load(dataObj.getData().getProjectProvider().getProfileImage200X200()).resize(300, 300).into(getView().getAwardProviderImage(), new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                            if (isViewAttached())
                                getView().getAwardProviderImage().setImageResource(R.drawable.profile_img);
                        }
                    });
                } else {
                    //show default image
                    if (isViewAttached())
                        getView().getAwardProviderImage().setImageResource(R.drawable.profile_img);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void uploadApplianceImages(String projectId) {
        try {
            System.out.println("upload Images" + uploadImageList.size());
            uploadImageList.remove(uploadImageList.size() - 1);
            getActivity().startService(createCallingIntent(projectId));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Intent createCallingIntent(String projectId) {
        Intent i = new Intent(getActivity(), ImagesService.class);
        ImagesResultReceiver receiver = new ImagesResultReceiver(new Handler());
        receiver.setReceiver(this);
        i.putExtra(Constants.RECEIVER, receiver);
        i.putExtra(Constants.SCREEN, Constants.CLICK_ADD_PROJECT);
        i.putParcelableArrayListExtra(Constants.IMAGE_LIST, (ArrayList<? extends Parcelable>) tempUploadImageList);
        i.putExtra(Constants.PROJECT_ID, projectId);
        return i;
    }

    private void onSuccess(AddProjectModelResponse addProjectModelResponse) {
        try {
            if (getActivity() != null && isViewAttached()) {
                Intent listIntent = new Intent(Constants.GET_PROJECTS);
                getActivity().sendBroadcast(listIntent);

                Intent intent = new Intent(Constants.ADD_EDIT_PROPERTY);
                getActivity().sendBroadcast(intent);

                Intent intent1 = new Intent(Constants.BROADCAST_FOR_UPCOMING);
                getActivity().sendBroadcast(intent1);

                uploadApplianceImages(String.valueOf(addProjectModelResponse.getData().getProject_id()));
                ((HolderActivity) getActivity()).oneStepBack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(GetPropertyResponse listModel, ApiName check) {
        if (getActivity() != null && isViewAttached()) {
            switch (check) {
                case GET_APPLIANCE:
                    applianceList.clear();
                    if (listModel.getData() != null && listModel.getData().size() > 0) {

                        applianceList.addAll(listModel.getData());
                        // adapter.updateList(getActivity(), applianceList);
                    } else {
                        GetPropertyResponse.DataBean bean = new GetPropertyResponse.DataBean();
                        bean.setTitle("No Appliances");
                        bean.setValue(0);
                        applianceList.add(bean);
                    }
                    adapter = new ApplianceSpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, applianceList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    getView().getApplianceName().setAdapter(adapter);
                    break;
                case GET_PROPERTIES_LIST:
                    System.out.println("propertyListing" + listModel.getData().size());
                    if (listModel.getData().size() > 0) {
                        propertyList.addAll(listModel.getData());
                    } else {
                        GetPropertyResponse.DataBean bean = new GetPropertyResponse.DataBean();
                        bean.setTitle("No Properties");
                        bean.setValue(0);
                        propertyList.add(bean);
                    }
                    propertyAdapter = new PropertySpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, propertyList);
                    propertyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    getView().getProperty().setAdapter(propertyAdapter);

                    //set selected property
                    if (!TextUtils.isEmpty(propertyID)) {
                        if (propertyList.size() > 0) {
                            for (int i = 0; i < propertyList.size(); i++) {
                                if ((String.valueOf(propertyList.get(i).getValue())).equals(propertyID)) {
                                    getView().getProperty().setSelection(i);
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }

    private void onSuccess(JobTypeAndExpertise resourcesObj) {
        try {
            if (getActivity() != null && isViewAttached()) {

                listJobType = new ArrayList<>();
                if (!listJobType.isEmpty())
                    listJobType.clear();

                listJobType.addAll(resourcesObj.getData().get(0).get(0).getInfo());

                jobTypeExpertiseAdapter = new JobTypeExpertiseSpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listJobType);
                jobTypeExpertiseAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                getView().getJobType().setAdapter(jobTypeExpertiseAdapter);

                if (listJobType != null && listJobType.size() > 0) {
                    for (int i = 0; i < listJobType.size(); i++) {
                        if (listJobType.get(i).getId() == selectedJobType) {
                            getView().getJobType().setSelection(i);
                        }
                    }
                }

                listExpertise = new ArrayList<>();
                if (!listExpertise.isEmpty())
                    listExpertise.clear();

                List<String> listExpert = new ArrayList<>();

                listExpertise.addAll(resourcesObj.getData().get(1).get(0).getInfo());
                for (int i = 0; i < listExpertise.size(); i++) {
                    listExpert.add(listExpertise.get(i).getValue());
                }

                expertiseAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_multiple_choice, listExpert);
                getView().getExpertiseSpinnerView().setListAdapter(expertiseAdapter).setAllCheckedText(selectedExpertiseName);

                if (!listExpertise.isEmpty()) {
                    arraySelectionExpertise = new boolean[listExpertise.size()];
                    for (int j = 0; j < listExpertise.size(); j++) {
                        if (detailExpertise != null) {

                            for (int i = 0; i < detailExpertise.length; i++) {
                                if (detailExpertise[i].equals(String.valueOf(listExpertise.get(j).getId()))) {
                                    arraySelectionExpertise[j] = true;
                                    if (expertiseName.trim().isEmpty())
                                        expertiseName = String.valueOf(listExpertise.get(j).getValue());
                                    else
                                        expertiseName = expertiseName + "," + String.valueOf(listExpertise.get(j).getValue());

                                    if (mSelectedExpertise.trim().isEmpty())
                                        mSelectedExpertise = String.valueOf(listExpertise.get(j).getId());
                                    else
                                        mSelectedExpertise = mSelectedExpertise + "," + String.valueOf(listExpertise.get(j).getId());

                                }
                            }
                            getView().getProjectExpertise().setText(expertiseName);
                        }

                    }

                    switch (currentScreen) {
                        case CLICK_ADD_PROJECT:
                            mSelectedExpertise = String.valueOf(listExpertise.get(0).getId());
                            expertiseName = listExpertise.get(0).getValue();
                            arraySelectionExpertise[0] = true;
                            break;
                    }


                    getView().getExpertiseSpinnerView().setAllCheckedText(expertiseName);
                    for (int k = 0; k < arraySelectionExpertise.length; k++) {
                        System.out.println(k + "K----boolean" + arraySelectionExpertise[k]);
                        getView().getExpertiseSpinnerView().selectItem(k, arraySelectionExpertise[k]);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
    }

    private void getResource() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                getActivity().showProgressDialog();

                param.put(Constants.TYPE, "jobtype_expertise");
                getInterActor().getResourcesJobAndExpertise(param, GET_RESOURCES_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    public void saveBundleInfo(Bundle bundle) {
        try {
            if (getActivity() != null && isViewAttached() && bundle != null) {
                currentScreen = bundle.getString(Constants.DESTINATION, "");
                projectID = bundle.getString(Constants.PROJECT_ID);
                bgApp = bundle.getBoolean(Constants.BG_APP);
                propertyID = bundle.getString(Constants.PROPERTY_ID);
                switch (currentScreen) {
                    case CLICK_VIEW_PROJECT:
                        getView().getToolbarTitle().setText(R.string.Project_Detail);
                        visibilityGoneUiForEditProject();
                        break;
                    case CLICK_EDIT_PROJECT:
                        getView().getToolbarTitle().setText(R.string.edit_project);
                        visibilityGoneUiForSaveProject();
                        break;
                    case CLICK_ADD_PROJECT:
                        getView().getToolbarTitle().setText(R.string.add_project);
                        visibilityGoneUiForSaveProject();
                        getResource();
                        propertiesList();
                        break;

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addProject() {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    mProjectName = getView().getProjectName().getText().toString().trim();
                    mProjectDescription = getView().getProjectDescription().getText().toString();
                    mProjectJobType = listJobType.size() == 0 ? "" : String.valueOf(listJobType.get(getView().getJobType().getSelectedItemPosition()).getId());
                    mApplianceID = applianceList.size() == 0 ? "" : String.valueOf(applianceList.get(getView().getApplianceName().getSelectedItemPosition()).getValue());
                    mProjectAppliance = applianceList.size() == 0 ? "" : applianceList.get(getView().getApplianceName().getSelectedItemPosition()).getTitle();
                    mProjectSelectProperty = propertyList.size() == 0 ? "" : String.valueOf(propertyList.get(getView().getProperty().getSelectedItemPosition()).getTitle());
                    if (propertyList.get(getView().getProperty().getSelectedItemPosition()).getValue() == 0) {
                        mPropertyId = "";
                    } else {
                        mPropertyId = String.valueOf(propertyList.get(getView().getProperty().getSelectedItemPosition()).getValue());
                    }

                    String response = Validations.getInstance().validateAddProjectFields(mProjectName, mPropertyId, mSelectedExpertise, mProjectJobType);
                    if (!response.trim().isEmpty()) {
                        getActivity().showSnakBar(getView().getRootView(), response);
                    } else {
                        getActivity().showProgressDialog();
                        Map<String, String> params = new HashMap<>();
                        params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));

                        if (currentScreen.equals(CLICK_EDIT_PROJECT) || currentScreen.equals(CLICK_VIEW_PROJECT))
                            params.put(Constants.PROJECT_ID, projectID);

                        String imageIds = "";
                        for (int i = 0; i < deletedImageIds.size(); i++) {
                            if (!deletedImageIds.get(i).trim().isEmpty())
                                if (i == 0) {
                                    imageIds = deletedImageIds.get(i);
                                } else {
                                    imageIds = imageIds + "," + deletedImageIds.get(i);
                                }
                        }

                        if (!tempUploadImageList.isEmpty()) {
                            tempUploadImageList.clear();
                        }
                        if (currentScreen.equals(Constants.CLICK_EDIT_PROJECT) || currentScreen.equals(Constants.CLICK_VIEW_PROJECT)) {
                            for (int i = 0; i < uploadImageList.size(); i++) {
                                if (uploadImageList.get(i).getCheck().equals(ScreenNavigation.ADD_ITEM)) {
                                    ThumbnailBean beanObj = new ThumbnailBean();
                                    beanObj.setAddView(uploadImageList.get(i).isAddView());
                                    beanObj.setCheck(uploadImageList.get(i).getCheck());
                                    beanObj.setExtension(uploadImageList.get(i).getExtension());
                                    beanObj.setFile(uploadImageList.get(i).isFile());
                                    beanObj.setName(uploadImageList.get(i).getName());
                                    beanObj.setImagePath(uploadImageList.get(i).getImagePath());
                                    tempUploadImageList.add(beanObj);
                                }
                            }
                        } else {
                            if (uploadImageList.size() > 0) {
                                tempUploadImageList.addAll(uploadImageList);
                                tempUploadImageList.remove(tempUploadImageList.size() - 1);
                            }
                        }
                        params.put(Constants.PROPERTY_ID, mPropertyId);
                        params.put(Constants.APPLIANCE_ID, mApplianceID);
                        params.put(Constants.PROJECT_NAME, Utils.getInstance().capitalize(mProjectName));
                        params.put(Constants.PROJECT_DESCRIPTION, Utils.getInstance().capitalize(mProjectDescription));
                        params.put(Constants.APPLIANCE_NAME, mProjectAppliance);
                        params.put(Constants.PROPERTY_ADDRESS, mProjectSelectProperty);
                        params.put(Constants.EXPERTISE, mSelectedExpertise);
                        params.put(Constants.JOB_TYPE, mProjectJobType);
                        params.put(Constants.IS_DELETED, imageIds);


                        getInterActor().addEdit_Project(params, ApiName.ADD_EDIT_PROJECT);
                    }
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void propertiesList() {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    getActivity().showProgressDialog();
                    Map<String, String> params = new HashMap<>();
                    params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    getInterActor().getProperty(params, ApiName.GET_PROPERTIES_LIST);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getApplianceList(String property_Id) {
        if (getActivity() != null && isViewAttached()) {

            try {
                propertyId = property_Id;
                if (getActivity() != null && isViewAttached()) {
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        getActivity().showProgressDialog();
                        Map<String, String> param = new HashMap<>();
                        param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                        param.put(Constants.PROPERTY_ID, property_Id);
                        getInterActor().getAppliance(param, ApiName.GET_APPLIANCE);
                    } else {
                        getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (getActivity() != null && isViewAttached()) {
            switch (parent.getId()) {
                case spinner_property:
                    propertyId = String.valueOf(propertyList.get(position).getValue());
                    getApplianceList(propertyId);
                    break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void onAddOptionClick() {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Choose from file", "Cancel"};
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Choose Option");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            //choose camera
                            try {
                                isCameraSelected = true;
                                isGallerySelected = false;
                                checkExternalStoragePermission();
                            } catch (ActivityNotFoundException e) {
                                e.printStackTrace();
                                String errorMessage = "Your device doesn't support capturing images";
                                Utils.getInstance().showToast(errorMessage);
                            }
                            break;
                        case 1:
                            //choose gallery
                            try {
                                isCameraSelected = false;
                                isGallerySelected = true;
                                checkExternalStoragePermission();
                            } catch (Exception e) {
                                String errorMessage = "There are no images!";
                                Utils.getInstance().showToast(errorMessage);
                            }
                            break;
                        case 2:
                            try {
                                isCameraSelected = false;
                                isGallerySelected = false;
                                forDocumentSection = true;
                                checkExternalStoragePermission();
                            } catch (Exception e) {
                                String errorMessage = "There are no images!";
                                Utils.getInstance().showToast(errorMessage);
                            }
                            break;
                        case 3:
                            dialog.dismiss();
                            break;
                    }
                }
            });
            builder.show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(getActivity());
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(getActivity());
                    cameraPicker.setImagePickerCallback(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            } else if (requestCode == Picker.PICK_FILE) {
                filePicker.submit(data);
            }
        } else if (resultCode == RESULT_CANCELED) {
            Utils.getInstance().showToast("Request Cancelled");
        }
    }

    private void checkExternalStoragePermission() {
        boolean isExternalStorage = PermissionsAndroid.getInstance().checkCameraPermission(getActivity());
        if (!isExternalStorage) {
            PermissionsAndroid.getInstance().requestForCameraPermission(((AddEditProjectFragment) getView()));
        } else if (isCameraSelected) {
            takePicture();
        } else if (isGallerySelected) {
            pickImageSingle();
        } else if (forDocumentSection) {
            pickFilesSingle();
        }
    }


    /*
   Image handling for this class
    */
    private void pickFilesSingle() {
        filePicker = new FilePicker(getActivity());
        filePicker.setFilePickerCallback(this);
        filePicker.setFolderName(getActivity().getString(R.string.app_name));
        filePicker.pickFile();
    }

    private void takePicture() {
        cameraPicker = new CameraImagePicker(getActivity());
//        cameraPicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        cameraPicker.setImagePickerCallback(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        pickerPath = cameraPicker.pickImage();
    }

    public void onRequestPermissionResult(int requestCode, int[] grantResults) {
        switch (requestCode) {
            case 102: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Show option to update profile
                    if (isCameraSelected) {
                        takePicture();
                    } else if (isGallerySelected) {
                        pickImageSingle();
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Utils.getInstance().showToast("Camera/Storage permission Denied");
                }
                return;
            }

        }
    }

    private void pickImageSingle() {
        imagePicker = new ImagePicker(getActivity());
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        ChosenImage image = list.get(0);
        isImageSelected = true;
        String extension = "";
        int i = list.get(0).getDisplayName().lastIndexOf('.');
        if (i > 0) {
            extension = list.get(0).getDisplayName().substring(i + 1);
        }
        //Add Images info to list
        if (!extension.equals("png") && !extension.equals("jpg") && !extension.equals("jpeg")) {
            getActivity().showSnakBar(getView().getRootView(), "you can choose only images.");
            return;
        }
        if (uploadImageList.size() > 0) {
            uploadImageList.remove(uploadImageList.size() - 1);
            editImageAdapter.notifyItemRemoved(uploadImageList.size());
        }
        ThumbnailBean obj = new ThumbnailBean();
        obj.setExtension(extension);
        obj.setImagePath(FileUtils.getPath(getActivity(), Uri.parse(image.getQueryUri())));
        obj.setImageID("");
        obj.setName(image.getDisplayName());
        obj.setFile(true);
        obj.setCheck(ScreenNavigation.ADD_ITEM);
        obj.setAddView(false);
        uploadImageList.add(obj);

        if (editImageAdapter != null) {
            ThumbnailBean obj1 = new ThumbnailBean();
            obj1.setAddView(true);
            obj1.setFile(false);
            obj1.setImagePath("");
            obj1.setFile(false);
            uploadImageList.add(obj1);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    editImageAdapter.notifyDataSetChanged();
                }
            });
            getView().getEditProjectDetailRecycler().smoothScrollToPosition(uploadImageList.size() - 1);
        }
    }

    @Override
    public void onError(String s) {
        Utils.getInstance().showToast(s);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case 200:
                break;
        }
    }

    public void getOnGoingObject(String Project_ID) {
        getProjectDetail(Project_ID);
    }

    public void deleteImage(final int position) {
        if (getActivity() != null && isViewAttached()) {

            Utils.getInstance().confirmDialog(getActivity(), "Are you sure, you want to delete this image?", "delete", new Utils.ConfirmDialogCallbackInterface() {
                @Override
                public void onYesClick(String tag) {
                    if (deletedImageIds == null) deletedImageIds = new ArrayList<>();
                    deletedImageIds.add(uploadImageList.get(position).getImageID());
                    uploadImageList.remove(position);
                    getView().getEditProjectDetailRecycler().setAdapter(editImageAdapter);
                }

                @Override
                public void onNoClick(String tag) {

                }
            });
        }
    }

    /*navigate to view service provider profile*/
    public void onItemClicked(final int layoutPosition, ScreenNavigation click, ImageView imageView, TextView tvName, TextView tvMiles) {
        Intent intent = new Intent(getActivity(), HolderActivity.class);
        switch (click) {
            case DETAIL_ITEM:
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_PROVIDER);
                intent.putExtra(Constants.NAVIGATE_FROM, Constants.ADD_EDIT_PROJECT);
                intent.putParcelableArrayListExtra(Constants.PROVIDER_LIST, (ArrayList<? extends Parcelable>) listBids);
                intent.putExtra(Constants.POSITION, layoutPosition);
                if (Utils.getInstance().isEqualLollipop()) {
                    Pair<View, String> pairImage = Pair.create((View) imageView, Constants.TRANS_PROPERTY_IMAGE);
                    Pair<View, String> pairName = Pair.create((View) tvName, Constants.TRANS_NAME);
                    Pair<View, String> pairMiles = Pair.create((View) tvMiles, Constants.TRANS_MILES);
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairImage, pairName, pairMiles);
                    getActivity().startActivity(intent, options.toBundle());
                } else {
                    getActivity().startActivity(intent);
                }
                break;

            case ACCEPT:
                Utils.getInstance().confirmDialog(getActivity(), "Are you sure you want to accept bid?", "Sign Out", new Utils.ConfirmDialogCallbackInterface() {
                    @Override
                    public void onYesClick(String tag) {
                        if (isViewAttached()) {
                            bidAcceptReject("1", listBids.get(layoutPosition).getBidId());
                        }
                    }

                    @Override
                    public void onNoClick(String tag) {

                    }
                });
                break;
            case REJECT:
                Utils.getInstance().confirmDialog(getActivity(), "Are you sure you want to reject bid?", "Sign Out", new Utils.ConfirmDialogCallbackInterface() {
                    @Override
                    public void onYesClick(String tag) {
                        if (isViewAttached()) {
                            bidAcceptReject("2", listBids.get(layoutPosition).getBidId());
                        }
                    }

                    @Override
                    public void onNoClick(String tag) {

                    }
                });
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null && isViewAttached()) {
            getActivity().unregisterReceiver(projectDetailReceiver);
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Intent intent = new Intent(getActivity(), HolderActivity.class);
            switch (v.getId()) {
                case R.id.rl_sp_award:
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_PROVIDER);
                    intent.putExtra(Constants.NAVIGATE_FROM, Constants.ADD_EDIT_PROJECT);
                    intent.putExtra(Constants.WON_PROVIDER, providerObj);
                    if (Utils.getInstance().isEqualLollipop()) {
                        Pair<View, String> pairImage = Pair.create((View) getView().getAwardProviderImage(), Constants.TRANS_PROPERTY_IMAGE);
                        Pair<View, String> pairName = Pair.create((View) getView().getAwardProviderNameView(), Constants.TRANS_NAME);
                        Pair<View, String> pairMiles = Pair.create((View) getView().getAwardProviderDistanceView(), Constants.TRANS_MILES);
                        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairImage, pairName, pairMiles);
                        getActivity().startActivity(intent, options.toBundle());
                    } else {
                        getActivity().startActivity(intent);
                    }
                    break;

                case R.id.tv_cancel:
                    try {
                        ReasonFeedbackDialogFragment fragment = ReasonFeedbackDialogFragment.getInstance();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(Constants.IS_FEEDBACK, false);
                        fragment.setArguments(bundle);
                        fragment.setParams(this);
                        fragment.show(getActivity().getSupportFragmentManager(), "dialog");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case R.id.tv_end:
                    try {
                        ReasonFeedbackDialogFragment fragment = ReasonFeedbackDialogFragment.getInstance();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(Constants.IS_FEEDBACK, true);
                        fragment.setArguments(bundle);
                        fragment.setParams(this);
                        fragment.show(getActivity().getSupportFragmentManager(), "dialog");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.tv_reject:
                    markNotCompleted();
                    break;
                case R.id.tv_accept:
                    try {
                        ReasonFeedbackDialogFragment fragment = ReasonFeedbackDialogFragment.getInstance();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(Constants.IS_FEEDBACK, true);
                        fragment.setArguments(bundle);
                        fragment.setParams(this);
                        fragment.show(getActivity().getSupportFragmentManager(), "dialog");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case R.id.message:
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_CHAT);
                    intent.putExtra(Constants.SEND_BY_NAME, providerObj.getFullName());
                    intent.putExtra(Constants.SEND_BY_USERID, String.valueOf(providerObj.getUserId()));
                    // intent.putExtra(Constants.MESSAGE_OBJECT, listChatResponse.get(getLayoutPosition()));
                    getActivity().startActivity(intent);
                    break;
            }
        }
    }

    private void bidAcceptReject(String status, int bidID) {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    //hit API to get Property ID
                    getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    param.put(Constants.STATUS, status);
                    param.put(Constants.BID_ID, String.valueOf(bidID));
                    getInterActor().bidAcceptReject(param, ApiName.BID_ACCEPT_REJECT);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clearData() {
        if (getActivity() != null && isViewAttached()) {
            detailExpertise = null;
            expertiseName = "";
            mSelectedExpertise = "";
        }
    }

    public void endProject(String rating, String feedback) {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    //hit API to get Property ID
                    getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    param.put(Constants.PROJECT_ID, projectID);
                    param.put(Constants.FEEDBACK, feedback);
                    param.put(Constants.RATING, rating);
                    getInterActor().endProject(param, ApiName.END_PROJECT);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelProject(String reason) {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    //hit API to get Property ID
                    getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    param.put(Constants.PROJECT_ID, projectID);
                    param.put(Constants.REASON, reason);
                    getInterActor().cancelProject(param, ApiName.CANCEL_PROJECT);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void markNotCompleted() {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    //hit API to get Property ID
                    getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    param.put(Constants.PROJECT_ID, projectID);
                    getInterActor().markNotCompletedProject(param, ApiName.MARK_NOT_COMPLETED_PROJECT);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFilesChosen(List<ChosenFile> list) {
        ChosenFile file = list.get(0);
        String extension = "";
        int i = list.get(0).getDisplayName().lastIndexOf('.');
        if (i > 0) {
            extension = list.get(0).getDisplayName().substring(i + 1);
        }
        if (extension.equals("pdf") || extension.equals("doc") || extension.equals("docx")) {
            long fileSize = file.getSize() / 1024; // convert bytes into Kb
            if (fileSize >= 5120) {
                Utils.getInstance().showToast("File size must be less than 5 MB");
                return;
            }
            manageFileList(file, null, extension);
        } else {
            getActivity().showSnakBar(getView().getRootView(), "you can choose only pdf or doc files.");
        }
    }

    private void manageFileList(ChosenFile file, ChosenImage image, String extension) {
        //Add Images info to list
        if (uploadImageList == null) uploadImageList = new ArrayList<>();
        if (uploadImageList.size() > 0) {
            uploadImageList.remove(uploadImageList.size() - 1);
            editImageAdapter.notifyItemRemoved(uploadImageList.size());
        }
        ThumbnailBean obj = new ThumbnailBean();
        obj.setExtension(extension);
        if (file != null) {
            obj.setImagePath(file.getOriginalPath());
            obj.setName(file.getDisplayName());
        } else if (image != null) {
            obj.setImagePath(image.getThumbnailPath());
            obj.setName(image.getDisplayName());
        }
        obj.setCheck(ScreenNavigation.ADD_ITEM);
        obj.setImageID("");
        obj.setFile(true);
        obj.setAddView(false);
        uploadImageList.add(obj);

        if (editImageAdapter != null) {
            ThumbnailBean obj1 = new ThumbnailBean();
            obj1.setAddView(true);
            obj1.setFile(false);
            obj1.setImagePath("");
            obj1.setImageID("");
            uploadImageList.add(obj1);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    editImageAdapter.notifyDataSetChanged();
                }
            });
            getView().getEditProjectDetailRecycler().smoothScrollToPosition(uploadImageList.size() - 1);
        }
    }

}
