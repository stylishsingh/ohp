package com.ohp.ownerviews.presenter;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.view.ProjectListView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import retrofit2.Response;

/**
 * Created by vishal.sharma on 7/13/2017.
 */

public class ProjectListFragmentPresenterImpl extends FragmentPresenter<ProjectListView,APIHandler> implements  APIResponseInterface.OnCallableResponse,View.OnClickListener {


    public ProjectListFragmentPresenterImpl(ProjectListView projectListView, Context context) {
        super(projectListView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return  new APIHandler(this);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {

    }

    @Override
    public void onFailure(Throwable t, ApiName api) {

    }
    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
          Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.fab_add_projects:
                    Intent intent = new Intent(getActivity(), HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_ADD_PROJECT);
                    getActivity().startActivity(intent);
                    break;

            }
        }

    }
}
