package com.ohp.ownerviews.presenter;

import android.app.ActivityOptions;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.FilePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.FilePickerCallback;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenFile;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.ApplianceSpinnerAdapter;
import com.ohp.commonviews.adapter.PropertySpinnerAdapter;
import com.ohp.commonviews.adapter.ThumbImageAdapter;
import com.ohp.commonviews.beans.AddEditModel;
import com.ohp.commonviews.beans.GetPropertyResponse;
import com.ohp.commonviews.beans.ThumbnailBean;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.beans.DocumentDetailModel;
import com.ohp.ownerviews.fragment.AddEditDocumentFragment;
import com.ohp.ownerviews.view.AddEditDocumentView;
import com.ohp.service.ImagesResultReceiver;
import com.ohp.service.ImagesService;
import com.ohp.utils.Constants;
import com.ohp.utils.FileUtils;
import com.ohp.utils.PermissionsAndroid;
import com.ohp.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.ohp.utils.Constants.ADD_EDIT_PROPERTY;
import static com.ohp.utils.Constants.BROWSE_PIC;
import static com.ohp.utils.Constants.CLICK_ADD_DOCUMENT;
import static com.ohp.utils.Constants.CLICK_EDIT_DOCUMENT;
import static com.ohp.utils.Constants.CLICK_VIEW_DOCUMENT;

/**
 * @author Anuj sharma.
 */

public class AddEditDocumentPresenterImpl extends FragmentPresenter<AddEditDocumentView, APIHandler> implements APIHandler.OnCallableResponse,
        AdapterView.OnItemSelectedListener, ImagePickerCallback, ImagesResultReceiver.Receiver, FilePickerCallback, View.OnClickListener {

    private DocumentDetailModel detailObj;
    private String currentScreen = "", applianceID = "", propertyId = "", documentID = "", pickerPath;
    private boolean isCameraSelected = false;
    private boolean forDocumentSection = false;
    private boolean isGallerySelected = false;
    //    private boolean forDocumentSection = false;
    private List<ThumbnailBean> uploadImageList;
    private List<ThumbnailBean> tempUploadImageList = new ArrayList<>();
    private List<String> deletedImageIds = new ArrayList<>();
    private ThumbImageAdapter thumbImageAdapter;
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private PropertySpinnerAdapter propertyAdapter;
    private ApplianceSpinnerAdapter applianceAdapter;
    private GetPropertyResponse propertyObj, applianceObj;
    private FilePicker filePicker;
    private boolean isImageSelected = false;

    public AddEditDocumentPresenterImpl(AddEditDocumentView addEditDocumentView, Context context) {
        super(addEditDocumentView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("picker_path")) {
                pickerPath = savedInstanceState.getString("picker_path");
            }
        }
        if (getActivity() != null && getView() != null) {

            if (getActivity() instanceof OwnerDashboardActivity) {
                //hide Toolbar
                getView().getToolbar().setVisibility(View.GONE);
            } else if (getActivity() instanceof HolderActivity) {
                //show Toolbar
                getActivity().setSupportActionBar(getView().getToolbar());
                if (getActivity().getSupportActionBar() != null)
                    getActivity().getSupportActionBar().setTitle("");
                getView().getToolbar().setVisibility(View.VISIBLE);
                getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
                getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        ((HolderActivity) getActivity()).oneStepBack();
                    }
                });
            }

            //insert add option in detail view by default in detail view
            if (uploadImageList == null) uploadImageList = new ArrayList<>();
            //Initialize RecyclerView for images
            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getDocumentRecycler().setLayoutManager(lm);

            //initialize spinners
            getView().getPropertySpinner().setOnItemSelectedListener(this);
            propertyAdapter = new PropertySpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, null);
            getView().getPropertySpinner().setAdapter(propertyAdapter);

            getView().getApplianceSpinner().setOnItemSelectedListener(this);
            applianceAdapter = new ApplianceSpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, null);
            getView().getApplianceSpinner().setAdapter(applianceAdapter);


        }
    }

    private void getPropertiesList() {
        //hit API to get Property ID
        try {
            if (getActivity() != null && isViewAttached() && Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put("access_token", Utils.getInstance().getAccessToken(getActivity()));
                getInterActor().getProperty(param, ApiName.GET_PROPERTY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveBundleInfo(Bundle bundle) {
        try {
            if (getActivity() != null && isViewAttached() && bundle != null) {
                currentScreen = bundle.getString(Constants.DESTINATION, "");
                propertyId = bundle.getString(Constants.PROPERTY_ID, "");
                documentID = bundle.getString(Constants.DOCUMENT_ID, "");
                switch (currentScreen) {
                    case CLICK_ADD_DOCUMENT:
                        getView().getTilDocumentNameView().setVisibility(View.VISIBLE);
                        getView().getToolbarTitle().setText(R.string.title_add_document);
                        showHideDetailView(false);
                        getPropertiesList();
                        uploadImageList = new ArrayList<>();
                        ThumbnailBean obj = new ThumbnailBean();
                        obj.setAddView(true);
                        obj.setFile(false);
                        obj.setImagePath("");
                        uploadImageList.add(obj);
                        thumbImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, true);
                        getView().getDocumentRecycler().setAdapter(thumbImageAdapter);
                        break;
                    case ADD_EDIT_PROPERTY:
                        getView().getTilDocumentNameView().setVisibility(View.VISIBLE);
                        getView().getToolbarTitle().setText(R.string.title_add_document);
                        showHideDetailView(false);
                        getPropertiesList();
                        uploadImageList = new ArrayList<>();
                        ThumbnailBean obj1 = new ThumbnailBean();
                        obj1.setAddView(true);
                        obj1.setFile(false);
                        obj1.setImagePath("");
                        uploadImageList.add(obj1);
                        thumbImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, true);
                        getView().getDocumentRecycler().setAdapter(thumbImageAdapter);
                        break;
                    case CLICK_EDIT_DOCUMENT:
                        getDocumentDetail();
                        getView().getToolbarTitle().setText(R.string.title_edit_document);
                        showHideDetailView(false);
                        enableDisableUI(true);
                        break;
                    case CLICK_VIEW_DOCUMENT:
                        getDocumentDetail();
                        getView().getToolbarTitle().setText(R.string.title_detail_document);
                        showHideDetailView(true);
                        enableDisableUI(false);
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean validateDocumentInfo() {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                if (getView().getDocumentName().getText().toString().trim().length() == 0) {
                    getActivity().showSnakBar(getView().getRootView(), "Please enter document name");
                } else if (TextUtils.isEmpty(propertyId)) {
                    getActivity().showSnakBar(getView().getRootView(), "Error while getting properties list");
                } /*else if (TextUtils.isEmpty(applianceID)) {
                getActivity().showSnakBar(getView().getRootView(), "Error while getting appliances list");
            }  else if (uploadImageList.size() == 1) {
                getActivity().showSnakBar(getView().getRootView(), "Please addA at least 1 photo");
            }*/ else {
                    //check if document Id is available , then update otherwise add
                    getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    param.put(Constants.DOCUMENT_NAME, Utils.getInstance().capitalize(getView().getDocumentName().getText().toString()));
                    if (currentScreen.equals(Constants.CLICK_EDIT_DOCUMENT) ||
                            currentScreen.equals(Constants.CLICK_VIEW_DOCUMENT))
                        param.put(Constants.DOCUMENT_ID, documentID);
                    param.put(Constants.PROPERTY_ID, propertyId);
                    param.put(Constants.APPLIANCE_ID, applianceID);
                    if (TextUtils.isEmpty(documentID)) {
                        //Add Document
                        param.put(Constants.DOCUMENT_ID, documentID);
                    }

                    String imageIds = "";
                    for (int i = 0; i < deletedImageIds.size(); i++) {
                        if (!deletedImageIds.get(i).trim().isEmpty())
                            if (i == 0) {
                                imageIds = deletedImageIds.get(i);
                            } else {
                                imageIds = imageIds + "," + deletedImageIds.get(i);
                            }
                    }

                    param.put(Constants.IS_DELETED, imageIds);

                    if (!tempUploadImageList.isEmpty()) {
                        tempUploadImageList.clear();
                    }


                    if (currentScreen.equals(Constants.CLICK_EDIT_DOCUMENT) ||
                            currentScreen.equals(Constants.CLICK_VIEW_DOCUMENT)) {
                        for (int i = 0; i < uploadImageList.size(); i++) {
                            if (uploadImageList.get(i).getCheck().equals(ScreenNavigation.ADD_ITEM)) {
                                ThumbnailBean beanObj = new ThumbnailBean();
                                beanObj.setAddView(uploadImageList.get(i).isAddView());
                                beanObj.setCheck(uploadImageList.get(i).getCheck());
                                beanObj.setExtension(uploadImageList.get(i).getExtension());
                                beanObj.setFile(uploadImageList.get(i).isFile());
                                beanObj.setName(uploadImageList.get(i).getName());
                                beanObj.setImagePath(uploadImageList.get(i).getImagePath());
                                tempUploadImageList.add(beanObj);
                            }
                        }
                    } else {
                        tempUploadImageList.addAll(uploadImageList);
                        tempUploadImageList.remove(tempUploadImageList.size() - 1);
                    }

                    getInterActor().saveDocument(param, ApiName.ADD_EDIT_DOCUMENT_API);
                    return true;
                }
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }

        return false;
    }

    private Intent createCallingIntent(String documentID) {
        Intent i = new Intent(getActivity(), ImagesService.class);
        ImagesResultReceiver receiver = new ImagesResultReceiver(new Handler());
        receiver.setReceiver(this);
        i.putExtra(Constants.RECEIVER, receiver);
        i.putExtra(Constants.SCREEN, Constants.CLICK_DOCUMENT);
        i.putParcelableArrayListExtra(Constants.IMAGE_LIST, (ArrayList<? extends Parcelable>) tempUploadImageList);
        i.putExtra(Constants.DOCUMENT_ID, documentID);
        return i;
    }

    public void showHideDetailView(boolean isDetailView) {
        if (isDetailView) {
            //show detail view
            getView().getDetailTopContainer().setVisibility(View.VISIBLE);
        } else {
            //show edit view
            //hide detail views and show edit image contaienr
            getView().getDetailTopContainer().setVisibility(View.GONE);
        }
    }

    public void enableDisableUI(boolean isEdit) {
        //apply animation
        if (Utils.getInstance().isEqualLollipop()) {
            TransitionManager.beginDelayedTransition((ViewGroup) getView().getRootView(), new AutoTransition());
        }
        if (isEdit) {
            //make items editable
            getView().getDocumentName().setEnabled(true);
            getView().getPropertySpinner().setEnabled(true);
            getView().getDocumentName().requestFocus();

            getView().getHeaderAppliance().setText(R.string.header_select_appliance);
            getView().getHeaderProperty().setText(R.string.header_select_property);

            getView().getTilDocumentNameView().setVisibility(View.VISIBLE);
            getView().getTilDocumentNameDetailView().setVisibility(View.GONE);

            getView().getDocImagesHeaderView().setText(R.string.title_upload_documents);
            getView().getDocImagesHeaderView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            getView().getHeaderAppliance().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            getView().getHeaderProperty().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            getView().tvHeaderPropertyImage().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            getView().getApplianceLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().getPropertyLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));

            getView().getApplianceSpinner().setEnabled(true);
            getView().getTilCreatedOnView().setVisibility(View.GONE);

            //enable recycler view delete button and add add image option
            if (uploadImageList == null) uploadImageList = new ArrayList<>();
            if (uploadImageList.size() == 0) {
                ThumbnailBean obj = new ThumbnailBean();
                obj.setAddView(true);
                obj.setFile(false);
                obj.setImagePath("");
                uploadImageList.add(obj);
                thumbImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, true);
                getView().getDocumentRecycler().setAdapter(thumbImageAdapter);
            } else if (uploadImageList.size() != 0 && !uploadImageList.get(uploadImageList.size() - 1).isAddView()) {
                ThumbnailBean obj = new ThumbnailBean();
                obj.setAddView(true);
                obj.setFile(false);
                obj.setImagePath("");
                uploadImageList.add(obj);
                thumbImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, true);
                getView().getDocumentRecycler().setAdapter(thumbImageAdapter);
            }
        } else {
            //make items non-editable
            getView().getDocumentNameDetail().setEnabled(false);
            getView().getPropertySpinner().setEnabled(false);
            getView().getApplianceSpinner().setEnabled(false);
            getView().getCreationDate().setEnabled(false);
            getView().getCreationDate().setFocusable(false);
            getView().getDocumentNameDetail().setFocusable(false);

            getView().getHeaderAppliance().setText(R.string.header_appliance);
            getView().getHeaderProperty().setText(R.string.header_property);


            getView().getDocImagesHeaderView().setText(R.string.title_document_detail);
            getView().getDocImagesHeaderView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().getHeaderAppliance().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().getHeaderProperty().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().tvHeaderPropertyImage().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));

            getView().getTilCreatedOnView().setVisibility(View.VISIBLE);

            getView().getApplianceLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
            getView().getPropertyLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));

            getView().getTilDocumentNameView().setVisibility(View.GONE);
            getView().getTilDocumentNameDetailView().setVisibility(View.VISIBLE);

            //disable recyclerview delete button
            thumbImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, AddEditDocumentPresenterImpl.this, false, false);
            getView().getDocumentRecycler().setAdapter(thumbImageAdapter);


        }
    }

    private void getDocumentDetail() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.DOCUMENT_ID, documentID);
                getInterActor().getDocumentDetail(param, ApiName.GET_DOCUMENT_DETAIL_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_PROPERTY:
                        if (response.isSuccessful()) {
                            propertyObj = (GetPropertyResponse) response.body();
                            switch (propertyObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(propertyObj);
                                    break;
                                case Constants.STATUS_201:
//                                    getActivity().showSnakBar(getView().getRootView(), propertyObj.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(propertyObj.getMessage());
                                    break;
                            }
                        }
                        break;
                    case GET_APPLIANCE:
                        if (response.isSuccessful()) {
                            applianceObj = (GetPropertyResponse) response.body();
                            switch (applianceObj.getStatus()) {
                                case Constants.STATUS_200:
                                    if (applianceAdapter != null) {
                                        getView().getApplianceSpinnerView().setVisibility(View.VISIBLE);
                                        applianceAdapter.updateList(getActivity(), applianceObj.getData());
                                        for (int i = 0; i < applianceObj.getData().size(); i++) {

                                            if (detailObj != null && detailObj.getData().getAppliance().getApplianceId()
                                                    == applianceObj.getData().get(i).getValue()) {
                                                getView().getApplianceSpinner().setSelection(i + 1);
                                                return;
                                            }
                                        }
                                    }
                                    break;
                                case Constants.STATUS_201:
                                    applianceID = "";
                                    //hide appliance spinner
                                    if (applianceAdapter != null) {
                                        getView().getApplianceSpinnerView().setVisibility(View.VISIBLE);
                                        applianceAdapter.updateList(getActivity(), applianceObj.getData());
                                    }
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(propertyObj.getMessage());
                                    break;
                            }
                        }
                        break;
                    case ADD_EDIT_DOCUMENT_API:
                        if (response.isSuccessful()) {
                            AddEditModel addEditModel = (AddEditModel) response.body();
                            switch (addEditModel.getStatus()) {
                                case Constants.STATUS_200:
                                    deletedImageIds = new ArrayList<>();
                                    if (!tempUploadImageList.isEmpty())
                                        getActivity().startService(createCallingIntent(String.valueOf(addEditModel.getData().getId())));

                                    Intent listIntent;
                                    if (currentScreen.equals(ADD_EDIT_PROPERTY))
                                        listIntent = new Intent(Constants.ADD_EDIT_PROPERTY);
                                    else
                                        listIntent = new Intent(Constants.GET_DOCUMENTS_LIST);
                                    getActivity().sendBroadcast(listIntent);
                                    ((HolderActivity) getActivity()).oneStepBack();
//                                    }
                                    break;
                                case Constants.STATUS_201:
//                                    getActivity().showSnakBar(getView().getRootView(), addEditModel.getMessage());
                                    break;
                                case Constants.STATUS_203:
                                    getActivity().showSnackBar(getView().getRootView(), addEditModel.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(addEditModel.getMessage());
                                    break;
                            }
                        }
                        break;
                    case GET_DOCUMENT_DETAIL_API:
                        if (response.isSuccessful()) {
                            detailObj = (DocumentDetailModel) response.body();
                            switch (detailObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(detailObj);
                                    getPropertiesList();
                                    if (currentScreen.equals(Constants.CLICK_VIEW_DOCUMENT))
                                        enableDisableUI(false);
                                    else
                                        enableDisableUI(true);
                                    break;
                                case Constants.STATUS_201:
//                                    getActivity().showSnakBar(getView().getRootView(), detailObj.getMessage());
                                    getPropertiesList();
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(detailObj.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(GetPropertyResponse propertyObj) {
        if (propertyObj.getData() != null && propertyObj.getData().size() > 0) {
            /************update property spinner**********/
            if (getActivity() != null && isViewAttached()) {
                if (propertyAdapter != null) {
                    propertyAdapter.updateList(getActivity(), propertyObj.getData());
                }
                if (detailObj != null && detailObj.getData().getProperty().getPropertyId() != 0) {
                    propertyId = String.valueOf(detailObj.getData().getProperty().getPropertyId());
                }

                for (int i = 0; i < propertyObj.getData().size(); i++) {
                    if (propertyId.equals(String.valueOf(propertyObj.getData().get(i).getValue()))) {
                        getView().getPropertySpinner().setSelection(i);
                        return;
                    }
                }
            }

        }
    }

    private void onSuccess(DocumentDetailModel detailObj) {
        getView().getDocumentName().setText(detailObj.getData().getDocumentName());
        getView().getDocumentNameDetail().setText(detailObj.getData().getDocumentName());
        getView().getCreationDate().setText(detailObj.getData().getCreated());
        //set property image
        if (detailObj.getData().getProperty() != null && detailObj.getData().getProperty().getPropertyAvatar() != null
                && !TextUtils.isEmpty(detailObj.getData().getProperty().getPropertyAvatar())) {
            Picasso.with(getActivity()).load(detailObj.getData().getProperty().getPropertyAvatar())
                    .resize(500, 500).centerCrop().into(getView().getPropertyImage(), new Callback() {
                @Override
                public void onSuccess() {
                    getView().getImageProgress().setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    getView().getPropertyImage().setImageResource(R.drawable.default_property);
                    getView().getImageProgress().setVisibility(View.GONE);
                }
            });
        } else {
            //show dummy property image
            getView().getPropertyImage().setImageResource(R.drawable.default_property);
            getView().getImageProgress().setVisibility(View.GONE);
        }

        //set document images if there is alreay images in array
        if (detailObj.getData().getPropertyDocumentImages() != null && detailObj.getData().getPropertyDocumentImages().size() > 0) {
            // images are there, show in list
            if (uploadImageList == null) uploadImageList = new ArrayList<>();
            if (uploadImageList.size() > 0) {
                uploadImageList.remove(uploadImageList.size() - 1);
                thumbImageAdapter.notifyItemRemoved(uploadImageList.size());
            }
            ThumbnailBean obj = null;
            for (int i = 0; i < detailObj.getData().getPropertyDocumentImages().size(); i++) {
                obj = new ThumbnailBean();
                obj.setExtension("jpg");
                obj.setImagePath(detailObj.getData().getPropertyDocumentImages().get(i).getDocumentAvatar());
                obj.setName("server side images");
                obj.setFile(false);
                obj.setAddView(false);
                obj.setCheck(ScreenNavigation.OLD_ITEM);
                obj.setImageID(String.valueOf(detailObj.getData().getPropertyDocumentImages().get(i).getDocumentImageId()));
                uploadImageList.add(obj);
            }

            if (currentScreen.equals(Constants.CLICK_EDIT_DOCUMENT)) {
                enableDisableUI(true);
            }

        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    Image Handeling
     */

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        ChosenImage image = list.get(0);
        isImageSelected = true;
        String extension = "";
        int i = list.get(0).getDisplayName().lastIndexOf('.');
        if (i > 0) {
            extension = list.get(0).getDisplayName().substring(i + 1);
        }
        if (!forDocumentSection) {
            //Add Images info to list
            if (!extension.equals("png") && !extension.equals("jpg") && !extension.equals("jpeg")) {
                getActivity().showSnakBar(getView().getRootView(), "you can choose only images.");
                return;
            }
            if (uploadImageList == null) uploadImageList = new ArrayList<>();
            if (uploadImageList.size() > 0) {
                uploadImageList.remove(uploadImageList.size() - 1);
                thumbImageAdapter.notifyItemRemoved(uploadImageList.size());
            }
            ThumbnailBean obj = new ThumbnailBean();
            obj.setExtension(extension);
            obj.setImagePath(FileUtils.getPath(getActivity(), Uri.parse(image.getQueryUri())));
            obj.setName(image.getDisplayName());
            obj.setFile(true);
            obj.setAddView(false);
            obj.setImageID("");
            obj.setCheck(ScreenNavigation.ADD_ITEM);
            uploadImageList.add(obj);

            if (thumbImageAdapter != null) {
                ThumbnailBean obj1 = new ThumbnailBean();
                obj1.setAddView(true);
                obj1.setFile(false);
                obj1.setImagePath("");
                obj1.setFile(false);
                uploadImageList.add(obj1);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        thumbImageAdapter.notifyDataSetChanged();
                    }
                });
                getView().getDocumentRecycler().smoothScrollToPosition(uploadImageList.size() - 1);
            }
        } else {
            //image selected for document section.
            manageFileList(null, image, extension);
        }

    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onFilesChosen(List<ChosenFile> list) {
        ChosenFile file = list.get(0);
        String extension = "";
        int i = list.get(0).getDisplayName().lastIndexOf('.');
        if (i > 0) {
            extension = list.get(0).getDisplayName().substring(i + 1);
        }
        if (extension.equals("pdf") || extension.equals("doc") || extension.equals("docx")) {
            long fileSize = file.getSize() / 1024; // convert bytes into Kb
            if (fileSize >= 5120) {
                Utils.getInstance().showToast("File size must be less than 5 MB");
                return;
            }
            manageFileList(file, null, extension);
        } else {
            getActivity().showSnakBar(getView().getRootView(), "you can choose only pdf or doc files.");
        }
    }


    private void manageFileList(ChosenFile file, ChosenImage image, String extension) {
        //Add Images info to list
        if (uploadImageList == null) uploadImageList = new ArrayList<>();
        if (uploadImageList.size() > 0) {
            uploadImageList.remove(uploadImageList.size() - 1);
            thumbImageAdapter.notifyItemRemoved(uploadImageList.size());
        }
        ThumbnailBean obj = new ThumbnailBean();
        obj.setExtension(extension);
        if (file != null) {
            obj.setImagePath(file.getOriginalPath());
            obj.setName(file.getDisplayName());
        } else if (image != null) {
            obj.setImagePath(image.getThumbnailPath());
            obj.setName(image.getDisplayName());
        }
        obj.setCheck(ScreenNavigation.ADD_ITEM);
        obj.setImageID("");
        obj.setFile(true);
        obj.setAddView(false);
        uploadImageList.add(obj);

        if (thumbImageAdapter != null) {
            ThumbnailBean obj1 = new ThumbnailBean();
            obj1.setAddView(true);
            obj1.setFile(false);
            obj1.setImagePath("");
            obj1.setImageID("");
            uploadImageList.add(obj1);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    thumbImageAdapter.notifyDataSetChanged();
                }
            });
            getView().getDocumentRecycler().smoothScrollToPosition(uploadImageList.size() - 1);
        }
    }

    public void onAddOptionClick() {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Choose from file", "Cancel"};
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Choose Option");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            //choose camera
                            try {
                                isCameraSelected = true;
                                isGallerySelected = false;
                                forDocumentSection = false;
                                checkExternalStoragePermission();
                            } catch (ActivityNotFoundException e) {
                                e.printStackTrace();
                                String errorMessage = "Your device doesn't support capturing images";
                                Utils.getInstance().showToast(errorMessage);
                            }
                            break;
                        case 1:
                            //choose gallery
                            try {
                                isCameraSelected = false;
                                isGallerySelected = true;
                                forDocumentSection = false;
                                checkExternalStoragePermission();
                            } catch (Exception e) {
                                String errorMessage = "There are no images!";
                                Utils.getInstance().showToast(errorMessage);
                            }
                            break;
                        case 2:
                            //chose file
                            try {
                                isCameraSelected = false;
                                isGallerySelected = false;
                                forDocumentSection = true;
                                checkExternalStoragePermission();
                            } catch (Exception e) {
                                String errorMessage = "There are no files!";
                                Utils.getInstance().showToast(errorMessage);
                            }
                            break;
                        case 3:
                            dialog.dismiss();
                            break;
                    }
                }
            });
            builder.show();
        }
    }

    private void checkExternalStoragePermission() {
        boolean isExternalStorage = PermissionsAndroid.getInstance().checkCameraPermission(getActivity());
        if (!isExternalStorage) {
            PermissionsAndroid.getInstance().requestForCameraPermission(((AddEditDocumentFragment) getView()));
        } else if (isCameraSelected) {
            takePicture();
        } else if (isGallerySelected) {
            pickImageSingle();
        } else if (forDocumentSection) {
            pickFilesSingle();
        }
    }

    /*
    Image handling for this class
     */
    private void pickFilesSingle() {
        filePicker = new FilePicker(getActivity());
        filePicker.setFilePickerCallback(this);
        filePicker.setFolderName(getActivity().getString(R.string.app_name));
        filePicker.pickFile();
    }

    private void takePicture() {
        cameraPicker = new CameraImagePicker(getActivity());
//        cameraPicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        cameraPicker.setImagePickerCallback(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        pickerPath = cameraPicker.pickImage();
    }

    private void pickImageSingle() {
        imagePicker = new ImagePicker(getActivity());
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(getActivity());
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(getActivity());
                    cameraPicker.setImagePickerCallback(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            } else if (requestCode == Picker.PICK_FILE) {
                filePicker.submit(data);
            }
        } else if (resultCode == RESULT_CANCELED) {
//            Utils.getInstance().showToast("Request Cancelled");
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 102: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Show option to update profile
                    if (isCameraSelected) {
                        takePicture();
                    } else if (isGallerySelected) {
                        pickImageSingle();
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Utils.getInstance().showToast("Camera/Storage permission Denied");
                }
                return;
            }

        }
    }

    public void deleteImage(final int position) {
        Utils.getInstance().confirmDialog(getActivity(), "Are you sure, you want to delete this file?", "delete", new Utils.ConfirmDialogCallbackInterface() {
            @Override
            public void onYesClick(String tag) {
                deletedImageIds.add(uploadImageList.get(position).getImageID());
                uploadImageList.remove(position);
                getView().getDocumentRecycler().setAdapter(thumbImageAdapter);
            }

            @Override
            public void onNoClick(String tag) {

            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spinner_property_name:
                if (propertyObj != null && propertyObj.getData() != null && propertyObj.getData().size() > 0) {
                    propertyId = String.valueOf(propertyObj.getData().get(position).getValue());
                    //hit api to get appliance name on basis of property id
                    if (getActivity() != null && isViewAttached() && Utils.getInstance().isInternetAvailable(getActivity())) {
                        getActivity().showProgressDialog();
                        Map<String, String> param = new HashMap<>();
                        param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                        param.put(Constants.PROPERTY_ID, String.valueOf(propertyObj.getData().get(position).getValue()));
                        getInterActor().getAppliance(param, ApiName.GET_APPLIANCE);
                    }
                }
                break;
            case R.id.spinner_appliance_name:
                if (applianceObj != null && applianceObj.getData() != null && applianceObj.getData().size() > 0) {
                    if (position != 0) {
                        applianceID = String.valueOf(applianceObj.getData().get(position - 1).getValue());
                    } else {
                        applianceID = "";
                    }
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case 200:
                final String message = resultData.getString("message");
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                    }
                }, 1000);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.avatar:
                if (detailObj != null && !detailObj.getData().getProperty().getPropertyAvatar().isEmpty()) {
                    Intent profilePicIntent = new Intent(getActivity(), HolderActivity.class);
                    profilePicIntent.putExtra(Constants.DESTINATION, BROWSE_PIC);
                    profilePicIntent.putExtra(Constants.PIC_TITLE, Constants.TITLE_PROPERTY_IMAGE);
                    profilePicIntent.putExtra(BROWSE_PIC, detailObj.getData().getProperty().getPropertyAvatar());
                    if (Utils.getInstance().isEqualLollipop()) {
                        Pair<View, String> p1 = Pair.create((View) getView().getPropertyImage(), Constants.TRANS_PROPERTY_IMAGE);
                        ActivityOptions options =
                                ActivityOptions.makeSceneTransitionAnimation(getActivity(), p1);
                        getActivity().startActivity(profilePicIntent, options.toBundle());
                    } else {
                        getActivity().startActivity(profilePicIntent);
                    }
                }
                break;
        }
    }

    public void onItemClick(int layoutPosition, ImageView imageView) {
        if (getActivity() != null && isViewAttached()) {
            /*Bundle bundle = new Bundle();
            bundle.putInt(Constants.POSITION, layoutPosition);
            bundle.putBoolean(IS_SLIDER, true);
            bundle.putParcelableArrayList(Constants.IMAGE_LIST, (ArrayList<? extends Parcelable>) uploadImageList);
            ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.IMAGE_SLIDER_SCREEN, true, true, bundle);*/
            /***************************************/
            /*Intent intent = new Intent(getActivity(), HolderActivity.class);
            intent.putExtra(Constants.DESTINATION, BROWSE_SLIDER_PIC);
            intent.putExtra(Constants.POSITION, layoutPosition);
            intent.putExtra(IS_SLIDER, true);
            intent.putExtra(Constants.IMAGE_LIST, (ArrayList<? extends Parcelable>) uploadImageList);
            if (Utils.getInstance().isEqualLollipop()) {
                Pair<View, String> p1 = Pair.create((View) imageView, Constants.TRANS_PROPERTY_IMAGE);
                ActivityOptions options =
                        ActivityOptions.makeSceneTransitionAnimation(getActivity(), p1);
                getActivity().startActivity(intent, options.toBundle());
            } else {
                getActivity().startActivity(intent);
            }*/
        }
    }
}
