package com.ohp.ownerviews.presenter;

import android.app.LoaderManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.interfaces.AdapterPresenter;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.adapter.OwnerEventListAdapter;
import com.ohp.ownerviews.beans.EventListModel;
import com.ohp.ownerviews.view.OwnerEventListView;
import com.ohp.utils.Constants;
import com.ohp.utils.SearchText;
import com.ohp.utils.Utils;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static com.ohp.utils.Constants.ADD_EDIT_PROPERTY;

/**
 * @author Amanpal Singh.
 */

public class OwnerEventListFragmentPresenterImpl extends FragmentPresenter<OwnerEventListView, APIHandler>
        implements APIResponseInterface.OnCallableResponse, View.OnClickListener, AdapterPresenter<OwnerEventListAdapter,
        EventListModel.DataBean>, PopupMenu.OnMenuItemClickListener, LoaderManager.LoaderCallbacks<Object> {

    public String eventType = "";
    private OwnerEventListAdapter ownerEventListAdapter;
    private List<EventListModel.DataBean> listEvents = new ArrayList<>();
    private List<EventListModel.DataBean> listCurrentEvents = new ArrayList<>();
    private List<EventListModel.DataBean> tempListEvents = new ArrayList<>();
    private boolean isSelected = false;
    private EventListModel listModel;
    private int page = 0;
    private String eventDate = "", deleteEventId, changeInSeries = "0";
    // Setup listener
    final public CaldroidListener listener = new CaldroidListener() {

        @Override
        public void onSelectDate(Date date, View view) {
            Calendar cal = Calendar.getInstance();
            Calendar todayCal = Calendar.getInstance();

            cal.setTime(date);
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH) + 1;
            int day = cal.get(Calendar.DAY_OF_MONTH);
            String updatedMonth = "", updatedDay = "", updatedDate = "";

            if (todayCal.get((Calendar.MONTH) + 1) == month && todayCal.get((Calendar.DAY_OF_MONTH)) == day && year == todayCal.get(Calendar.YEAR)) {
                getView().getCVCurrentDateView().setVisibility(View.VISIBLE);
            } else {
                getView().getCVCurrentDateView().setVisibility(View.GONE);
            }


            if (month < 10) {
                updatedMonth = "0" + String.valueOf(month);
            } else {
                updatedMonth = String.valueOf(month);
            }
            if (day < 10) {
                updatedDay = "0" + String.valueOf(day);
            } else {
                updatedDay = String.valueOf(day);
            }

            isSelected = true;

            updatedDate = updatedDay + "-" + updatedMonth + "-" + String.valueOf(year);

            System.out.println("OwnerEventListFragmentPresenterImpl.onSelectDate----" + updatedDate);

            if (!listEvents.isEmpty()) {
                List<EventListModel.DataBean> searchList = new ArrayList<>();
                searchList.addAll(listEvents);
                searchList.remove(searchList.size() - 1);
                SearchText.getInstance().searchText(OwnerEventListFragmentPresenterImpl.this, updatedDate, searchList);
            }

        }

        @Override
        public void onChangeMonth(int month, int year) {
            String text = "month: " + month + " year: " + year;
            eventDate = "01-" + month + "-" + year;
            isSelected = false;
            System.out.println("OwnerEventListFragmentPresenterImpl.onChangeMonth---->" + eventDate);
            listEvents.clear();
            ownerEventListAdapter.update(listEvents);
            getEventList(eventDate);
        }

        @Override
        public void onLongClickDate(Date date, View view) {
        }

        @Override
        public void onCaldroidViewCreated() {

            if (isViewAttached() && getView().getCaldroidFragmentView().getLeftArrowButton() != null) {
            }
        }

    };
    private int deletedPosition = 0;
    private BroadcastReceiver eventsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("DashboardActivityPresenterImpl.onReceive");
            try {
                if (getActivity() != null && isViewAttached()) {
                    page = 0;
                    listEvents = new ArrayList<>();
                    if (intent.hasExtra("DeleteProperty")) {
                        getEventList(eventDate);
                    } else {
                        int month = intent.getIntExtra("month", 0);
                        int year = intent.getIntExtra("year", 0);
                        int day = intent.getIntExtra("day", 0);

                        if (month == 0 || year == 0 || day == 0) {
                            //set current date in
                            System.out.println("month year date error ");
                            year = Calendar.getInstance().get(Calendar.YEAR);
                            month = Calendar.getInstance().get(Calendar.MONTH) + 1;

                        }

                        eventDate = "01-" + month + "-" + year;
                        eventType = "";


                        moveFragment(year, month, day);

                       /* Calendar moveCalendar = Calendar.getInstance();
                        moveCalendar.set(year, month - 1, day);
                        getView().getCaldroidFragmentView().moveToDate(moveCalendar.getTime());
                        getEventList(eventDate);*/
                        Intent listIntent = new Intent(Constants.ADD_EDIT_PROPERTY);
                        context.sendBroadcast(listIntent);
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private boolean isFilterChanged = false;

    public OwnerEventListFragmentPresenterImpl(OwnerEventListView ownerEventListView, Context context) {
        super(ownerEventListView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onDestroy() {
        if (getActivity() != null && isViewAttached())
            getActivity().unregisterReceiver(eventsReceiver);
        super.onDestroy();

    }

    @Override
    public void onSuccess(Response response, ApiName api) {

        if (getActivity() != null && isViewAttached()) {
            switch (api) {
                case GET_EVENT_LIST:
                    if (response.isSuccessful()) {
                        EventListModel responseObj = (EventListModel) response.body();
                        switch (responseObj.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(responseObj);
                                break;
                            case Constants.STATUS_201:
                                break;
                            case Constants.STATUS_202:
                                break;
                        }
                    }
                    break;

                case DELETE_EVENT:
                    if (response.isSuccessful()) {
                        GenericBean responseObj = (GenericBean) response.body();
                        try {
                            switch (responseObj.getStatus()) {
                                case Constants.STATUS_200:
                                    System.out.println("listEvents before = " + listEvents.size());
                                    if (!isSelected) {
                                        if (ownerEventListAdapter != null) {
                                            Calendar cal = Calendar.getInstance();
                                            String[] selectedDate = listEvents.get(deletedPosition).getEventDate().split("-");
                                            cal.set(Integer.parseInt(selectedDate[2]), Integer.parseInt(selectedDate[1]) - 1, Integer.parseInt(selectedDate[0]));
                                            if (getView().getCaldroidFragmentView() != null) {
                                                getView().getCaldroidFragmentView().setTextColorForDate(R.color.black, cal.getTime());

                                            }
                                            listEvents.remove(deletedPosition);
                                            ownerEventListAdapter.notifyDataSetChanged();
                                            ownerEventListAdapter.deleteItem(deletedPosition);
                                        }
                                    } else {
                                        if (tempListEvents != null && tempListEvents.size() > 0) {
                                            getView().getEmptyView().setVisibility(View.GONE);
                                        } else {
                                            getView().getEmptyView().setVisibility(View.VISIBLE);
                                        }
                                        if (ownerEventListAdapter != null) {
                                            tempListEvents.remove(deletedPosition);
                                            ownerEventListAdapter.notifyDataSetChanged();
                                            ownerEventListAdapter.deleteItem(deletedPosition);
                                        }
                                        for (int i = 0; i < listEvents.size() - 1; i++) {
                                            if (deleteEventId.equals(String.valueOf(listEvents.get(i).getEventId()))) {
                                                Calendar cal = Calendar.getInstance();
                                                String[] selectedDate = listEvents.get(deletedPosition).getEventDate().split("-");
                                                cal.set(Integer.parseInt(selectedDate[2]), Integer.parseInt(selectedDate[1]) - 1, Integer.parseInt(selectedDate[0]));
                                                if (getView().getCaldroidFragmentView() != null) {
                                                    getView().getCaldroidFragmentView().setTextColorForDate(R.color.black, cal.getTime());

                                                }
                                                listEvents.remove(i);
                                                return;
                                            }
                                        }
                                    }
                                    System.out.println("listEvents after = " + listEvents.size());
                                    if (listEvents != null && listEvents.size() > 0) {
                                        getView().getEmptyView().setVisibility(View.GONE);
                                    } else {
                                        getView().getEmptyView().setVisibility(View.VISIBLE);
                                    }
                                    setCustomResourceForDates();
                                    if (getView().getNavigateFrom().equals(ADD_EDIT_PROPERTY)) {
                                        Intent listIntent = new Intent(Constants.ADD_EDIT_PROPERTY);
                                        getActivity().sendBroadcast(listIntent);
                                    }
                                    reloadFragment();
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    if (getActivity() instanceof OwnerDashboardActivity)
                                        ((OwnerDashboardActivity) getActivity()).logout(listModel.getMessage());
                                    else if (getActivity() instanceof HolderActivity)
                                        ((HolderActivity) getActivity()).logout(listModel.getMessage());
                                    break;
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }

    }

    private void onSuccess(EventListModel responseObj) {
        try {
            if (getActivity() != null && isViewAttached()) {
                listModel = responseObj;
                if (listModel.getData() != null && listModel.getData().size() > 0) {
                    getView().getEmptyView().setVisibility(View.GONE);
                    if (listEvents == null)
                        listEvents = new ArrayList<>();

                    listEvents = listModel.getData();
                    listEvents.add(null);
                    ownerEventListAdapter.update(listEvents);
                    setCustomResourceForDates();
                } else {
                    getView().getEmptyView().setVisibility(View.VISIBLE);
                    getView().getCVCurrentDateView().setVisibility(View.GONE);
                    ownerEventListAdapter.clearAll();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), HolderActivity.class);
        if (getActivity() != null && isViewAttached()) {
            switch (v.getId()) {
                case R.id.fab_add:
                    ownerEventListAdapter.mItemManger.closeAllItems();
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_OWNER_ADD_EVENT);
                    intent.putExtra(Constants.EVENT_ID, "");
                    System.out.println("getView().getPropertyID() = " + getView().getPropertyID());
                    intent.putExtra(Constants.EVENT_REPEAT_EMPTY, true);
                    intent.putExtra(Constants.OCCURRENCE, true);
                    intent.putExtra(Constants.EVENT_TYPE, 0);
                    intent.putExtra(Constants.PROPERTY_ID, getView().getPropertyID());
                    getActivity().startActivity(intent);
                    break;
                case R.id.cv_current_date:
                    intent.putExtra(Constants.EVENT_ID, String.valueOf(listCurrentEvents.get(0).getEventId()));
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_OWNER_VIEW_EVENT);
                    intent.putExtra(Constants.OCCURRENCE, false);
                    if (listCurrentEvents.get(0).getEventRepeat().isEmpty())
                        intent.putExtra(Constants.EVENT_REPEAT_EMPTY, true);
                    else
                        intent.putExtra(Constants.EVENT_REPEAT_EMPTY, false);
                    intent.putExtra(Constants.OCCURRENCE, false);
                    intent.putExtra(Constants.EVENT_TYPE, listEvents.get(0).getEventStatus());
                    intent.putExtra(Constants.PROPERTY_ID, String.valueOf(listCurrentEvents.get(0).getPropertyId()));
                    getActivity().startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().registerReceiver(eventsReceiver,
                        new IntentFilter(Constants.GET_EVENTS));
                Bundle args = new Bundle();
                Calendar cal = Calendar.getInstance();
                args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
                args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
                args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
                args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
                getView().getCaldroidFragmentView().setArguments(args);

                // Attach to the activity
                FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
                t.replace(R.id.calendar1, getView().getCaldroidFragmentView());
                t.commitAllowingStateLoss();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void reloadFragment() {
        if (getActivity() != null && isViewAttached()) {
            try {
                getView().setCaldroid(new CaldroidFragment());
                Bundle args = new Bundle();
                Calendar cal = Calendar.getInstance();
                args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
                args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
                args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
                args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
                getView().getCaldroidFragmentView().setArguments(args);

                // Attach to the activity
                FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
                t.replace(R.id.calendar1, getView().getCaldroidFragmentView());
                t.commitAllowingStateLoss();
                getView().getCaldroidFragmentView().setCaldroidListener(listener);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    private void moveFragment(int year, int month, int day) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getView().setCaldroid(new CaldroidFragment());
                Bundle args = new Bundle();
                args.putInt(CaldroidFragment.MONTH, month);
                args.putInt(CaldroidFragment.YEAR, year);
                args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
                args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
                getView().getCaldroidFragmentView().setArguments(args);

                // Attach to the activity
                FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
                t.replace(R.id.calendar1, getView().getCaldroidFragmentView());
                t.commitAllowingStateLoss();
                getView().getCaldroidFragmentView().setCaldroidListener(listener);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void setCustomResourceForDates() {
        if (getActivity() != null && isViewAttached()) {
            Calendar cal, calendarCurrentDate, calendarServerDate;
            List<Date> selectedDates = new ArrayList<>();
            if (!listCurrentEvents.isEmpty())
                listCurrentEvents.clear();
            getView().getCaldroidFragmentView().clearSelectedDates();
            getView().getCaldroidFragmentView().refreshView();
            if (listEvents != null && !listEvents.isEmpty()) {
                List<EventListModel.DataBean> dateLists = new ArrayList<>();
                dateLists.addAll(listEvents);
                dateLists.remove(dateLists.size() - 1);
                calendarCurrentDate = Calendar.getInstance();
                calendarServerDate = Calendar.getInstance();
                for (int i = 0; i < dateLists.size(); i++) {
                    cal = Calendar.getInstance();
                    int year = calendarCurrentDate.get(Calendar.YEAR);
                    int month = calendarCurrentDate.get(Calendar.MONTH) + 1;
                    int day = calendarCurrentDate.get(Calendar.DAY_OF_MONTH);

                    EventListModel.DataBean dataObj = dateLists.get(i);
                    String[] selectedDate = dataObj.getEventDate().split("-");
                    cal.set(Integer.parseInt(selectedDate[2]), Integer.parseInt(selectedDate[1]) - 1, Integer.parseInt(selectedDate[0]));
                    selectedDates.add(cal.getTime());
                    System.out.println("PARSED DATE----" + dataObj.getEventTimeFrom().substring(0, dataObj.getEventTimeFrom().lastIndexOf("+")));

                    if (listCurrentEvents.isEmpty() && year == Integer.parseInt(selectedDate[2]) && month == Integer.parseInt(selectedDate[1])
                            && day == Integer.parseInt(selectedDate[0])) {

                        String parseString = dataObj.getEventTimeFrom().substring(0, dataObj.getEventTimeFrom().lastIndexOf("+"));

                        String[] arrayParseString = parseString.split("T");
                        String[] arrayDate = arrayParseString[0].split("-");
                        String[] arrayTime = arrayParseString[1].split(":");

                        calendarServerDate.set(Integer.parseInt(arrayDate[0]), Integer.parseInt(arrayDate[1]), Integer.parseInt(arrayDate[2]),
                                Integer.parseInt(arrayTime[0]), Integer.parseInt(arrayTime[1]), Integer.parseInt(arrayTime[2]));
                        if (calendarCurrentDate.before(calendarServerDate)) {
                            setCurrentDayEvent(dateLists.get(i));
                            listCurrentEvents.add(dateLists.get(i));
                            continue;
                        }
                    }

                    if (getView().getCaldroidFragmentView() != null) {
                        getView().getCaldroidFragmentView().setTextColorForDate(R.color.colorAccent, selectedDates.get(i));
                    }
                }
            } else {
                getView().getEmptyView().setVisibility(View.VISIBLE);
            }

            if (listCurrentEvents.isEmpty()) {
                getView().getCVCurrentDateView().setVisibility(View.GONE);
            }
            getView().getCaldroidFragmentView().refreshView();

        }
    }

    private void setCurrentDayEvent(EventListModel.DataBean dataBean) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getView().getCVCurrentDateView().setVisibility(View.VISIBLE);
                getView().getTVEventTitleView().setText(dataBean.getEventName());
                getView().getTVNotesView().setText(dataBean.getEventNotes());

                Spannable spannablePropertyName = new SpannableString("For " + dataBean.getPropertyName());
                spannablePropertyName.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.disabled_color)), 0, 4, 0);

                getView().getTVPropertyNameView().setText(spannablePropertyName);
                getView().getTVProjectNameView().setText(dataBean.getProjectName());
                getView().getTVTimeFromView().setText(Utils.getInstance().getYMDCalendarTimeFormattedTime(dataBean.getEventTimeFrom().replace("+00:00", "")));
                getView().getTVRepeatView().setText(dataBean.getEventRepeat());
                getView().getTVTimeFromToView().setText(Utils.getInstance().getYMDCalendarTimeFormattedTime(dataBean.getEventTimeFrom().replace("+00:00", ""))
                        + " - " + Utils.getInstance().getYMDCalendarTimeFormattedTime(dataBean.getEventTimeTo().replace("+00:00", "")));

                switch (dataBean.getEventRepeat().toLowerCase()) {
                    case "weekly":
                        getView().getTVRepeatView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.red_dot, 0, 0, 0);
                        break;
                    case "monthly":
                        getView().getTVRepeatView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.orange_dot, 0, 0, 0);
                        break;
                    case "yearly":
                        getView().getTVRepeatView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.green_dot, 0, 0, 0);
                        break;
                    default:
                        getView().getTVRepeatView().setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getEventList(String eventDate) {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {

                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    param.put(Constants.EVENT_DATE, eventDate);
                    param.put(Constants.PROPERTY_ID, getView().getPropertyID());
                    param.put(Constants.EVENT_TYPE, eventType);
                    param.put(Constants.PAGE_PARAM, String.valueOf(page));

                    getInterActor().getEventsList(param, ApiName.GET_EVENT_LIST);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public OwnerEventListAdapter initAdapter() {
        if (getActivity() != null && isViewAttached()) {
            ownerEventListAdapter = new OwnerEventListAdapter(listEvents, getActivity(), this);
            getView().getRecyclerView().setNestedScrollingEnabled(false);

        }

        return ownerEventListAdapter;
    }

    @Override
    public void updateList(List<EventListModel.DataBean> filterList) {

        tempListEvents = filterList;
        if (tempListEvents.size() > 0)
            tempListEvents.add(null);
        if (tempListEvents != null && tempListEvents.size() == 0) {
            getView().getEmptyView().setVisibility(View.VISIBLE);
            ownerEventListAdapter.update(tempListEvents);
        } else {
            getView().getEmptyView().setVisibility(View.GONE);
            ownerEventListAdapter.update(tempListEvents);
        }


    }

    public void onItemClicked(final int layoutPosition, ScreenNavigation click) {
        final Intent intent = new Intent(getActivity(), HolderActivity.class);
        switch (click) {
            case DETAIL_ITEM:
                intent.putExtra(Constants.OCCURRENCE, false);
                if (listEvents.get(layoutPosition).getEventRepeat().isEmpty())
                    intent.putExtra(Constants.EVENT_REPEAT_EMPTY, true);
                else
                    intent.putExtra(Constants.EVENT_REPEAT_EMPTY, false);
                intent.putExtra(Constants.EVENT_ID, String.valueOf(listEvents.get(layoutPosition).getEventId()));
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_OWNER_VIEW_EVENT);
                intent.putExtra(Constants.EVENT_TYPE, listEvents.get(layoutPosition).getEventStatus());
                getActivity().startActivity(intent);
                break;
            case EDIT_ITEM:
                if (!listEvents.get(layoutPosition).getEventRepeat().isEmpty()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String[] options = {"Edit Following Occurrences", "Edit Current Occurrence", "Cancel"};
                    builder.setTitle("Choose option")
                            .setItems(options, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    switch (which) {
                                        case 0:
                                            intent.putExtra(Constants.EVENT_REPEAT_EMPTY, false);
                                            intent.putExtra(Constants.OCCURRENCE, false);
                                            intent.putExtra(Constants.EVENT_ID, String.valueOf(listEvents.get(layoutPosition).getEventId()));
                                            intent.putExtra(Constants.DESTINATION, Constants.CLICK_OWNER_EDIT_EVENT);
                                            intent.putExtra(Constants.EVENT_TYPE, listEvents.get(layoutPosition).getEventStatus());
                                            getActivity().startActivity(intent);
                                            break;
                                        case 1:
                                            intent.putExtra(Constants.EVENT_REPEAT_EMPTY, false);
                                            intent.putExtra(Constants.OCCURRENCE, true);
                                            intent.putExtra(Constants.EVENT_ID, String.valueOf(listEvents.get(layoutPosition).getEventId()));
                                            intent.putExtra(Constants.DESTINATION, Constants.CLICK_OWNER_EDIT_EVENT);
                                            intent.putExtra(Constants.EVENT_TYPE, listEvents.get(layoutPosition).getEventStatus());
                                            getActivity().startActivity(intent);
                                            break;
                                        case 2:
                                            dialog.dismiss();
                                            break;

                                    }
                                }
                            });
                    builder.setCancelable(false);
                    builder.show();
                } else {
                    intent.putExtra(Constants.EVENT_REPEAT_EMPTY, true);
                    intent.putExtra(Constants.OCCURRENCE, false);
                    intent.putExtra(Constants.EVENT_ID, String.valueOf(listEvents.get(layoutPosition).getEventId()));
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_OWNER_EDIT_EVENT);
                    intent.putExtra(Constants.EVENT_TYPE, listEvents.get(layoutPosition).getEventStatus());
                    getActivity().startActivity(intent);
                }

                break;
            case DELETE_ITEM:
                if (listEvents.get(layoutPosition).getEventRepeat().isEmpty()) {
                    Utils.getInstance().confirmDialog(getActivity(), "Are you sure, you want to delete this event?", "delete", new Utils.ConfirmDialogCallbackInterface() {
                        @Override
                        public void onYesClick(String tag) {
                            deletedPosition = layoutPosition;
                            deleteEvent(String.valueOf(listEvents.get(layoutPosition).getEventId()));
                        }

                        @Override
                        public void onNoClick(String tag) {

                        }
                    });
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String[] options = {"Delete Following Occurrences", "Delete Current Occurrence", "No"};
                    builder.setTitle("Are you sure, you want to delete this event?")
                            .setItems(options, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    switch (which) {
                                        case 0:
                                            changeInSeries = "1";
                                            deletedPosition = layoutPosition;
                                            deleteEvent(String.valueOf(listEvents.get(layoutPosition).getEventId()));
                                            break;
                                        case 1:
                                            changeInSeries = "0";
                                            deletedPosition = layoutPosition;
                                            deleteEvent(String.valueOf(listEvents.get(layoutPosition).getEventId()));
                                            break;
                                        case 2:
                                            dialog.dismiss();
                                            break;

                                    }
                                }
                            });
                    builder.setCancelable(false);
                    builder.show();
                }
                break;
        }
    }

    private void deleteEvent(String eventID) {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    deleteEventId = eventID;
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    param.put(Constants.EVENT_ID, eventID);
                    param.put(Constants.CHANGE_IN_SERIES, changeInSeries);

                    getInterActor().deleteEvent(param, ApiName.DELETE_EVENT);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setToolbar() {
        if (getActivity() != null && isViewAttached()) {
            // Calculate ActionBar height
            TypedValue tv = new TypedValue();
            int actionBarHeight = 0;
            if (getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getActivity().getResources().getDisplayMetrics());
            }
            CollapsingToolbarLayout.LayoutParams param = new CollapsingToolbarLayout.LayoutParams(CollapsingToolbarLayout.LayoutParams.MATCH_PARENT,
                    CollapsingToolbarLayout.LayoutParams.WRAP_CONTENT);
            param.setMargins(0, actionBarHeight, 0, 0);
            getView().getLLCalendarView().setLayoutParams(param);
            getActivity().setSupportActionBar(getView().getToolbarView());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            getView().getToolbarView().setTitle("");
            getView().getToolbarTitle().setText(R.string.title_events_list);
            getView().getToolbarView().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbarView().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    ((HolderActivity) getActivity()).oneStepBack();
                }
            });
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        isFilterChanged = true;
        listEvents = new ArrayList<>();
        listCurrentEvents = new ArrayList<>();
        tempListEvents = new ArrayList<>();
        ownerEventListAdapter.clearAll();
        switch (menuItem.getItemId()) {
            case R.id.action_my_event:
                eventType = "my";
                // Prepare the loader.  Either re-connect with an existing one,
                // or start a new one.
//                getActivity().getLoaderManager().initLoader(0, null, this);
                reloadFragment();
                break;
            case R.id.action_assigned_event:
                eventType = "assigned";
//                getActivity().getLoaderManager().initLoader(0, null, this);
                reloadFragment();
                break;
            case R.id.action_all_event:
                eventType = "";
                reloadFragment();
//                getActivity().getLoaderManager().initLoader(0, null, this);
                break;
        }
        return true;
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        System.out.println("OwnerEventListFragmentPresenterImpl.onCreateLoader");
        reloadFragment();
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        System.out.println("OwnerEventListFragmentPresenterImpl.onLoadFinished");
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {
        System.out.println("OwnerEventListFragmentPresenterImpl.onLoaderReset");
    }
}
