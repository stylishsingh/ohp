package com.ohp.ownerviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.interfaces.AdapterPresenter;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.adapter.ApplianceListAdapter;
import com.ohp.ownerviews.beans.ApplianceListModel;
import com.ohp.ownerviews.view.AppliancesView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static com.ohp.utils.Constants.ADD_EDIT_PROPERTY;

/**
 * @author Amanpal Singh.
 */

public class AppliancesListFragmentPresenterImpl extends FragmentPresenter<AppliancesView, APIHandler>
        implements APIResponseInterface.OnCallableResponse, AdapterPresenter<ApplianceListAdapter, ApplianceListModel>, View.OnClickListener {

    private int page = 1;   //for pagination
    private String searchQuery = "";  //for search query
    private LinearLayoutManager linearLayoutManager;
    private int pastVisibleItems, visibleItemCount, totalItemCount;
    private boolean loading = true, isSearched = false;
    private ApplianceListAdapter adapter;
    private List<ApplianceListModel.DataBean> applianceList = new ArrayList<>();
    private int deletedAppliancePosition;
    private String propertyId = "", applianceId = "";
    private BroadcastReceiver appliancesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("DashboardActivityPresenterImpl.onReceive");
            page = 1;
            applianceList = new ArrayList<>();
            getApplianceList();
            Intent listIntent = new Intent(Constants.ADD_EDIT_PROPERTY);
            context.sendBroadcast(listIntent);
        }
    };
    private ApplianceListModel listModel;

    public AppliancesListFragmentPresenterImpl(AppliancesView appliancesView, Context context) {
        super(appliancesView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //set listener for recyclerview pagination

        if (getActivity() != null && isViewAttached()) {

            if (getView().getNavigateFrom().equals(ADD_EDIT_PROPERTY)) {
                propertyId = getView().getPropertyID();
                getActivity().setSupportActionBar(getView().getToolbarView());
                if (getActivity().getSupportActionBar() != null)
                    getActivity().getSupportActionBar().setTitle("");
                getView().getToolbarView().setTitle("");
                getView().getToolbarTitle().setText(R.string.title_appliance_list);
                getView().getToolbarView().setNavigationIcon(R.drawable.ic_navigation_back);
                getView().getToolbarView().setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        ((HolderActivity) getActivity()).oneStepBack();
                    }
                });
            }

            getActivity().registerReceiver(appliancesReceiver,
                    new IntentFilter(Constants.GET_APPLIANCE_LIST));
            linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getRecyclerView().setLayoutManager(linearLayoutManager);
            getView().getRecyclerView().setAdapter(initAdapter());

            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

                            if (listModel != null && listModel.getTotalRecord() > applianceList.size()) {
                                page += 1;
                                getApplianceList();
                            }

                            System.out.println("scrollY = " + scrollY);
                            System.out.println("(v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) = " + (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight()));

                            System.out.println("oldScrollY = " + oldScrollY);
                            System.out.println((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                    scrollY > oldScrollY);

                        }
                    }
                }
            });

            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });
        }

        getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0) {
                    getView().getCloseBtn().setVisibility(View.VISIBLE);
                    Handler handler = new Handler();
                        /*handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getPropertiesList(s.toString());
                            }
                        },500);*/
                    searchQuery = s.toString();
                    page = 1;
                    getApplianceList();
                    isSearched = true;

                } else {
                    page = 1;
                    searchQuery = "";
                    isSearched = true;
                    applianceList.clear();
                    adapter.update(applianceList);
                    getView().getCloseBtn().setVisibility(View.GONE);
                    getApplianceList();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        getApplianceList();
    }

    @Override
    public void onResume() {
        super.onResume();
//        getApplianceList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null && isViewAttached())
            getActivity().unregisterReceiver(appliancesReceiver);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_APPLIANCE_LIST:
                        if (response.isSuccessful()) {
                            listModel = (ApplianceListModel) response.body();
                            switch (listModel.getStatus()) {
                                case Constants.STATUS_200:
                                    loading = true;
                                    onSuccess(listModel);
                                    break;
                                case Constants.STATUS_201:
                                    if (applianceList == null)
                                        applianceList = new ArrayList<>();
                                    if (!applianceList.isEmpty())
                                        applianceList.clear();
                                    adapter.update(applianceList);
//                                    getActivity().showSnakBar(getView().getRootView(), listModel.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    if (getActivity() instanceof OwnerDashboardActivity)
                                        ((OwnerDashboardActivity) getActivity()).logout(listModel.getMessage());
                                    else if (getActivity() instanceof HolderActivity)
                                        ((HolderActivity) getActivity()).logout(listModel.getMessage());
                                    break;
                            }
                        }
                        break;

                    case DELETE_APPLIANCE_LIST:
                        if (response.isSuccessful()) {
                            GenericBean genericBean = (GenericBean) response.body();
                            loading = false;
                            switch (genericBean.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(genericBean);
                                    break;
                                case Constants.STATUS_201:
//                                    getActivity().showSnakBar(getView().getRootView(), genericBean.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    if (getActivity() instanceof OwnerDashboardActivity)
                                        ((OwnerDashboardActivity) getActivity()).logout(genericBean.getMessage());
                                    else if (getActivity() instanceof HolderActivity)
                                        ((HolderActivity) getActivity()).logout(genericBean.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(GenericBean genericBean) {
        if (getActivity() != null && isViewAttached()) {
            if (adapter != null) {
                applianceList.remove(deletedAppliancePosition);
                adapter.deleteItem(deletedAppliancePosition);
            }
            if (applianceList != null && applianceList.size() > 0) {
                getView().getEmptyView().setVisibility(View.GONE);
            } else {
                getView().getEmptyView().setVisibility(View.VISIBLE);
            }

            if (getView().getNavigateFrom().equals(ADD_EDIT_PROPERTY)) {
                Intent listIntent = new Intent(Constants.ADD_EDIT_PROPERTY);
                getActivity().sendBroadcast(listIntent);
            }
        }

    }

    private void onSuccess(ApplianceListModel listModel) {
        if (listModel.getData() != null && listModel.getData().size() > 0) {
            getView().getEmptyView().setVisibility(View.GONE);
            if (applianceList == null)
                applianceList = new ArrayList<>();
            if (isSearched && searchQuery.length() != 0) {
                applianceList = new ArrayList<>();
                applianceList.addAll(listModel.getData());
            } else if (listModel.getTotalRecord() > applianceList.size()) {
                applianceList.addAll(listModel.getData());
            }/*else if(isFilterChanged){
                                    propertiesList = listModel.getData();
                                }*/
            adapter.update(applianceList);
        } else {
            if (applianceList.isEmpty()) {
                adapter.clearAll();
                getView().getEmptyView().setVisibility(View.VISIBLE);
            } else {
                if (isSearched || searchQuery.length() != 0) {
                    adapter.clearAll();
                    getView().getEmptyView().setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onItemClicked(final int layoutPosition, ScreenNavigation click, ImageView imageView) {
        Intent intent = new Intent(getActivity(), HolderActivity.class);
        switch (click) {
            case DETAIL_ITEM:
                intent.putExtra(Constants.PROPERTY_ID, String.valueOf(applianceList.get(layoutPosition).getPropertyId()));
                intent.putExtra(Constants.APPLIANCE_ID, String.valueOf(applianceList.get(layoutPosition).getId()));
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_APPLIANCE);
//                if (Utils.getInstance().isEqualLollipop()) {
//                    Pair<View, String> pair = Pair.create((View) imageView, Constants.TRANS_PROPERTY_IMAGE);
//                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pair);
//                    getActivity().startActivity(intent, options.toBundle());
//                } else {
                getActivity().startActivity(intent);
//                }
                break;
            case EDIT_ITEM:
                intent.putExtra(Constants.PROPERTY_ID, String.valueOf(applianceList.get(layoutPosition).getPropertyId()));
                intent.putExtra(Constants.APPLIANCE_ID, String.valueOf(applianceList.get(layoutPosition).getId()));
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_EDIT_APPLIANCE);
                getActivity().startActivity(intent);
                break;
            case DELETE_ITEM:
                Utils.getInstance().confirmDialog(getActivity(), "Are you sure, you want to delete this appliance?", "delete", new Utils.ConfirmDialogCallbackInterface() {
                    @Override
                    public void onYesClick(String tag) {
                        deletedAppliancePosition = layoutPosition;
                        deleteApplianceList(String.valueOf(applianceList.get(layoutPosition).getId()));
                    }

                    @Override
                    public void onNoClick(String tag) {

                    }
                });

                break;
        }
    }

    @Override
    public ApplianceListAdapter initAdapter() {
        if (isViewAttached()) {
            adapter = new ApplianceListAdapter(applianceList, getActivity(), this);
        }
        return adapter;
    }

    @Override
    public void updateList(List<ApplianceListModel> filterList) {

    }


    public void getApplianceList() {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    if (TextUtils.isEmpty(searchQuery))
                        getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    if (!TextUtils.isEmpty(searchQuery))
                        param.put(Constants.SEARCH_PARAM, searchQuery);
                    param.put(Constants.PAGE_PARAM, String.valueOf(page));
                    param.put(Constants.PROPERTY_ID, propertyId);
                    getInterActor().getApplianceList(param, ApiName.GET_APPLIANCE_LIST);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteApplianceList(String applianceId) {
        try {
            if (getActivity() != null) {
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.APPLIANCE_ID, applianceId);
                getInterActor().deleteAppliance(param, ApiName.DELETE_APPLIANCE_LIST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_add_appliance:
                adapter.mItemManger.closeAllItems();
                Intent intent = new Intent(getActivity(), HolderActivity.class);
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_ADD_APPLIANCE);
                intent.putExtra(Constants.PROPERTY_ID, propertyId);
                getActivity().startActivity(intent);

                break;
            case R.id.btn_clear:
                searchQuery = "";
                page = 1;
                getView().getSearchEditText().setText("");
                break;
        }
    }

}
