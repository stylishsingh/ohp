package com.ohp.ownerviews.presenter;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.beans.ProjectDetailResponse;
import com.ohp.commonviews.beans.ServiceProvidersModel;
import com.ohp.enums.ApiName;
import com.ohp.enums.CurrentScreen;
import com.ohp.ownerviews.view.SpDetailFragmentView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Response;

import static com.ohp.utils.Constants.ADD_EDIT_PROJECT;
import static com.ohp.utils.Constants.BROWSE_PIC;
import static com.ohp.utils.Constants.BROWSE_SERVICE_PROVIDER;

/**
 * @author Amanpal Singh.
 */

public class SpDetailFragmentPresenterImpl extends FragmentPresenter<SpDetailFragmentView, APIHandler> implements APIResponseInterface.OnCallableResponse, View.OnClickListener {

    private List<ServiceProvidersModel.DataBean> listServiceProvider = new ArrayList<>();
    private List<ProjectDetailResponse.DataBean.BidsBean> listBidsServiceProvider = new ArrayList<>();
    private ProjectDetailResponse.DataBean.ProjectProviderBean providerBean;
    private int position = 0;
    private String navigateFrom = "", profilePicUrl = "", bidID = "", providerID = "", providerName = "";

    public SpDetailFragmentPresenterImpl(SpDetailFragmentView spDetailFragmentView, Context context) {
        super(spDetailFragmentView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    public void saveBundle(Bundle bundle) {
        try {
            if (getActivity() != null && isViewAttached()) {
                position = bundle.getInt(Constants.POSITION, 0);
                navigateFrom = bundle.getString(Constants.NAVIGATE_FROM, "");

                switch (navigateFrom) {
                    case BROWSE_SERVICE_PROVIDER:
                        listServiceProvider = bundle.getParcelableArrayList(Constants.PROVIDER_LIST);
                        if (listServiceProvider != null)
                            updateView(listServiceProvider.get(position));
                        break;
                    case ADD_EDIT_PROJECT:
                        if (bundle.containsKey(Constants.WON_PROVIDER)) {
                            getView().getParentBidAcceptRejectView().setVisibility(View.VISIBLE);
                            getView().getIVAcceptView().setVisibility(View.GONE);
                            getView().getIVRejectView().setVisibility(View.GONE);
                            providerBean = bundle.getParcelable(Constants.WON_PROVIDER);
                            if (providerBean != null)
                                updateView(providerBean);
                        } else if (bundle.containsKey(Constants.PROVIDER_LIST)) {
                            getView().getParentBidAcceptRejectView().setVisibility(View.VISIBLE);
                            getView().getParentNotesView().setVisibility(View.VISIBLE);
                            listBidsServiceProvider = bundle.getParcelableArrayList(Constants.PROVIDER_LIST);
                            if (listBidsServiceProvider != null)
                                updateView(listBidsServiceProvider.get(position));
                        }
                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateView(ProjectDetailResponse.DataBean.ProjectProviderBean dataBean) {
        try {
            if (getActivity() != null && isViewAttached()) {
                providerID = String.valueOf(dataBean.getUserId());
                bidID = String.valueOf(dataBean.getBidId());
                providerName = dataBean.getFullName();
                if (dataBean.getAvgRating() != 0)
                    getView().getRBProviderView().setRating(Float.parseFloat(String.valueOf(dataBean.getAvgRating())));
                getView().getTVProviderNameView().setText(dataBean.getFullName());
                getView().getTVMilesView().setText(dataBean.getAwayInKms() == 0 ? "0 Miles Away" : String.format("%s Miles Away", String.valueOf(dataBean.getAwayInKms())));
                getView().getTVBidAmountView().setText(String.format(Locale.ENGLISH, "%d Bid (USD)", dataBean.getBidAmount()));
                getView().getTVNotesView().setText(dataBean.getBidNote().equalsIgnoreCase("") ? "N/A" : dataBean.getBidNote());
                getView().getTVBusinessNameView().setText(dataBean.getBusinessName());
                getView().getTVBusinessAddressView().setText(dataBean.getAddress().equalsIgnoreCase("") ? "N/A" : dataBean.getAddress());
                getView().getTVWebsiteView().setText(dataBean.getWebsite().equalsIgnoreCase("") ? "N/A" : dataBean.getWebsite());

                if (dataBean.getGender().equalsIgnoreCase("male")) {
                    getView().getTVProviderNameView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_male_small, 0, R.drawable.ic_chat_small, 0);
                } else {
                    getView().getTVProviderNameView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_female_small, 0, R.drawable.ic_chat_small, 0);
                }


                profilePicUrl = dataBean.getProfileImage200X200();
                if (!TextUtils.isEmpty(dataBean.getProfileImage200X200())) {
                    Picasso.with(getActivity()).load(dataBean.getProfileImage200X200()).resize(300, 300).centerCrop().into(getView().getIVAvatar(), new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                            getView().getIVAvatar().setImageResource(R.drawable.profile_img);
                        }
                    });
                } else {
                    getView().getIVAvatar().setImageResource(R.drawable.profile_img);
                }
                String expertise = "";
                for (int i = 0; i < dataBean.getExpertise().size(); i++) {
                    if (i == 0)
                        expertise = dataBean.getExpertise().get(i).getExpertiseTitle();
                    else
                        expertise = expertise + "," + dataBean.getExpertise().get(i).getExpertiseTitle();
                }

                getView().getTVExpertiseView().setText(expertise);

                if (dataBean.getIsLicensed() == 1) {
                    getView().getTVLicensedView().setText(dataBean.getLicenseNo());
                    getView().getTVLicensedView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_green, 0, 0, 0);
                } else {
                    getView().getTVLicensedView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_gray, 0, 0, 0);
                }

                if (dataBean.getInsured() == 1) {
                    getView().getTVInsuredView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_green, 0, 0, 0);
                } else {
                    getView().getTVInsuredView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_gray, 0, 0, 0);
                }

                if (dataBean.getBonded() == 1) {
                    getView().getTVBondedView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_green, 0, 0, 0);
                } else {
                    getView().getTVBondedView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_gray, 0, 0, 0);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateView(ProjectDetailResponse.DataBean.BidsBean dataBean) {
        try {
            if (getActivity() != null && isViewAttached()) {
                providerID = String.valueOf(dataBean.getUserId());
                bidID = String.valueOf(dataBean.getBidId());
                providerName = dataBean.getFullName();
                if (dataBean.getAvgRating() != 0)
                    getView().getRBProviderView().setRating(Float.parseFloat(String.valueOf(dataBean.getAvgRating())));
                getView().getTVProviderNameView().setText(dataBean.getFullName());
                getView().getTVMilesView().setText(dataBean.getAwayInKms() == 0 ? "0 Miles Away" : String.format("%s Miles Away", String.valueOf(dataBean.getAwayInKms())));
                getView().getTVBidAmountView().setText(String.format(Locale.ENGLISH, "%d Bid (USD)", dataBean.getBidAmount()));
                getView().getTVNotesView().setText(dataBean.getBidNote().equalsIgnoreCase("") ? "N/A" : dataBean.getBidNote());
                getView().getTVBusinessNameView().setText(dataBean.getBusinessName());
                getView().getTVBusinessAddressView().setText(dataBean.getAddress().equalsIgnoreCase("") ? "N/A" : dataBean.getAddress());
                getView().getTVWebsiteView().setText(dataBean.getWebsite().equalsIgnoreCase("") ? "N/A" : dataBean.getWebsite());

                if (dataBean.getGender().equalsIgnoreCase("male")) {
                    getView().getTVProviderNameView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_male_small, 0, R.drawable.ic_chat_small, 0);
                } else {
                    getView().getTVProviderNameView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_female_small, 0, R.drawable.ic_chat_small, 0);
                }


                profilePicUrl = dataBean.getProfileImage200X200();
                if (!TextUtils.isEmpty(dataBean.getProfileImage200X200())) {
                    Picasso.with(getActivity()).load(dataBean.getProfileImage200X200()).resize(300, 300).centerCrop().into(getView().getIVAvatar(), new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                            getView().getIVAvatar().setImageResource(R.drawable.profile_img);
                        }
                    });
                } else {
                    getView().getIVAvatar().setImageResource(R.drawable.profile_img);
                }
                String expertise = "";
                for (int i = 0; i < dataBean.getExpertise().size(); i++) {
                    if (i == 0)
                        expertise = dataBean.getExpertise().get(i).getExpertiseTitle();
                    else
                        expertise = expertise + "," + dataBean.getExpertise().get(i).getExpertiseTitle();
                }

                getView().getTVExpertiseView().setText(expertise);

                if (dataBean.getIsLicensed() == 1) {
                    getView().getTVLicensedView().setText(dataBean.getLicenseNo());
                    getView().getTVLicensedView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_green, 0, 0, 0);
                } else {
                    getView().getTVLicensedView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_gray, 0, 0, 0);
                }

                if (dataBean.getInsured() == 1) {
                    getView().getTVInsuredView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_green, 0, 0, 0);
                } else {
                    getView().getTVInsuredView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_gray, 0, 0, 0);
                }

                if (dataBean.getBonded() == 1) {
                    getView().getTVBondedView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_green, 0, 0, 0);
                } else {
                    getView().getTVBondedView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_gray, 0, 0, 0);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateView(ServiceProvidersModel.DataBean dataBean) {
        try {
            if (getActivity() != null && isViewAttached()) {
                providerName = dataBean.getFullName();
                providerID = String.valueOf(dataBean.getId());
                if (dataBean.getAvgRating() != 0)
                    getView().getRBProviderView().setRating(Float.parseFloat(String.valueOf(dataBean.getAvgRating())));
                getView().getTVProviderNameView().setText(dataBean.getFullName());
                getView().getTVMilesView().setText(TextUtils.isEmpty(dataBean.getAwayInKms()) ? "0 Miles Away" : String.format("%s Miles Away", String.valueOf(dataBean.getAwayInKms())));
                getView().getTVBusinessNameView().setText(dataBean.getBusinessName());
                getView().getTVBusinessAddressView().setText(dataBean.getAddress().equalsIgnoreCase("") ? "N/A" : dataBean.getAddress());
                getView().getTVWebsiteView().setText(dataBean.getWebsite().equalsIgnoreCase("") ? "N/A" : dataBean.getWebsite());

                if (dataBean.getGender().equalsIgnoreCase("male")) {
                    getView().getTVProviderNameView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_male_small, 0, R.drawable.ic_chat_small, 0);
                } else {
                    getView().getTVProviderNameView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_female_small, 0, R.drawable.ic_chat_small, 0);
                }

                profilePicUrl = dataBean.getProfileImage200X200();
                if (!TextUtils.isEmpty(dataBean.getProfileImage200X200())) {
                    Picasso.with(getActivity()).load(dataBean.getProfileImage200X200()).resize(300, 300).centerCrop().into(getView().getIVAvatar(), new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                            if (getActivity() != null && isViewAttached())
                                getView().getIVAvatar().setImageResource(R.drawable.profile_img);
                        }
                    });
                } else {
                    getView().getIVAvatar().setImageResource(R.drawable.profile_img);
                }
                String expertise = "";
                for (int i = 0; i < dataBean.getExpertise().size(); i++) {
                    if (i == 0)
                        expertise = dataBean.getExpertise().get(i).getExpertiseTitle();
                    else
                        expertise = expertise + "," + dataBean.getExpertise().get(i).getExpertiseTitle();
                }
                getView().getTVExpertiseView().setText(expertise);

                if (dataBean.getIsLicensed() == 1) {
                    getView().getTVLicensedView().setText(dataBean.getLicenseNo());
                    getView().getTVLicensedView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_green, 0, 0, 0);
                } else {
                    getView().getTVLicensedView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_gray, 0, 0, 0);
                }

                if (dataBean.getInsured() == 1) {
                    getView().getTVInsuredView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_green, 0, 0, 0);
                } else {
                    getView().getTVInsuredView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_gray, 0, 0, 0);
                }

                if (dataBean.getBonded() == 1) {
                    getView().getTVBondedView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_green, 0, 0, 0);
                } else {
                    getView().getTVBondedView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lib_gray, 0, 0, 0);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            getView().getToolbar().setVisibility(View.VISIBLE);
            getView().getToolbarTitle().setText(R.string.title_service_provider_profile);
            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    ((HolderActivity) getActivity()).oneStepBack();
                }
            });
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    getActivity().hideProgressDialog();
                }
                switch (api) {
                    case BID_ACCEPT_REJECT:
                        if (response.isSuccessful()) {
                            GenericBean dataResponse = (GenericBean) response.body();
                            switch (dataResponse.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(dataResponse);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(dataResponse.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onSuccess(GenericBean dataResponse) {
        if (getActivity() != null && isViewAttached()) {
            getView().getIVAcceptView().setVisibility(View.GONE);
            getView().getIVRejectView().setVisibility(View.GONE);
            Intent intent = new Intent(Constants.PROJECT_DETAIL);
            getActivity().sendBroadcast(intent);
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.avatar:
                if (!profilePicUrl.isEmpty()) {
                    Intent profilePicIntent = new Intent(getActivity(), HolderActivity.class);
                    profilePicIntent.putExtra(Constants.DESTINATION, BROWSE_PIC);
                    profilePicIntent.putExtra(Constants.PIC_TITLE, Constants.TITLE_SERVICE_PROVIDER_IMAGE);
                    profilePicIntent.putExtra(BROWSE_PIC, profilePicUrl);
                    if (Utils.getInstance().isEqualLollipop()) {
                        Pair<View, String> p1 = Pair.create((View) getView().getIVAvatar(), Constants.TRANS_PROPERTY_IMAGE);
                        ActivityOptions options =
                                ActivityOptions.makeSceneTransitionAnimation(getActivity(), p1);
                        getActivity().startActivity(profilePicIntent, options.toBundle());
                    } else {
                        getActivity().startActivity(profilePicIntent);
                    }
                }
                break;
            case R.id.iv_accept:
                Utils.getInstance().confirmDialog(getActivity(), "Are you sure you want to accept bid?", "", new Utils.ConfirmDialogCallbackInterface() {
                    @Override
                    public void onYesClick(String tag) {
                        if (isViewAttached()) {
                            bidAcceptReject("1");
                        }
                    }

                    @Override
                    public void onNoClick(String tag) {

                    }
                });
                break;
            case R.id.iv_reject:
                Utils.getInstance().confirmDialog(getActivity(), "Are you sure you want to reject bid?", "", new Utils.ConfirmDialogCallbackInterface() {
                    @Override
                    public void onYesClick(String tag) {
                        if (isViewAttached()) {
                            bidAcceptReject("2");
                        }
                    }

                    @Override
                    public void onNoClick(String tag) {

                    }
                });
                break;

            case R.id.tv_view_reviews:
                Bundle bundle = new Bundle();
                bundle.putString(Constants.USER_ID, providerID);
                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.REVIEWS_SCREEN, false, true, bundle);
                break;
            case R.id.tv_provider_name:
                Intent intent = new Intent(getActivity(), HolderActivity.class);
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_CHAT);
                intent.putExtra(Constants.SEND_BY_NAME, providerName);
                intent.putExtra(Constants.SEND_BY_USERID, providerID);
                getActivity().startActivity(intent);
                break;
        }
    }

    private void bidAcceptReject(String status) {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    //hit API to get Property ID
                    getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    param.put(Constants.STATUS, status);
                    param.put(Constants.BID_ID, bidID);
                    getInterActor().bidAcceptReject(param, ApiName.BID_ACCEPT_REJECT);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
