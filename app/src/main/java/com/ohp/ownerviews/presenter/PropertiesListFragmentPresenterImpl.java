package com.ohp.ownerviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.interfaces.AdapterPresenter;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.adapter.PropertiesListAdapter;
import com.ohp.ownerviews.beans.PropertiesListModel;
import com.ohp.ownerviews.beans.PropertiesListModel.DataBean;
import com.ohp.ownerviews.view.PropertiesListView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * @author Amanpal Singh.
 */

public class PropertiesListFragmentPresenterImpl extends FragmentPresenter<PropertiesListView, APIHandler>
        implements View.OnClickListener, APIResponseInterface.OnCallableResponse,
        AdapterPresenter<PropertiesListAdapter, DataBean>, PopupMenu.OnMenuItemClickListener {

    public boolean isFilterChanged = false;
    public int page = 1;   //for pagination
    private PropertiesListModel listModel;
    private boolean isSearched = false;
    private String searchQuery = "";  //for search query
    private PropertiesListAdapter adapter;
    private List<DataBean> propertiesList = new ArrayList<>();
    private int deletedPropertyPosition;
    private BroadcastReceiver propertiesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("DashboardActivityPresenterImpl.onReceive");
            page = 1;
            propertiesList = new ArrayList<>();
            getPropertiesList();
        }
    };

    public PropertiesListFragmentPresenterImpl(PropertiesListView propertiesView, Context context) {
        super(propertiesView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.fab_add_properties:
                    adapter.mItemManger.closeAllItems();
                    Intent intent = new Intent(getActivity(), HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_ADD_PROPERTY);
                    getActivity().startActivity(intent);
                    break;
                case R.id.btn_clear:
                    searchQuery = "";
                    page = 1;
                    getView().getSearchEditText().setText("");
                    break;
            }
        }

    }

    @Override
    public PropertiesListAdapter initAdapter() {
        if (isViewAttached()) {
            adapter = new PropertiesListAdapter(null, getActivity(), this);
            getView().getRecyclerView().setAdapter(adapter);
        }
        return adapter;
    }

    @Override
    public void updateList(List<DataBean> propertiesList) {

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //add search textWatcher
        if (getActivity() != null && isViewAttached()) {

            // register broadcast receiver to update property list.
            getActivity().registerReceiver(propertiesReceiver,
                    new IntentFilter(Constants.GET_PROPERTIES_LIST));


            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getRecyclerView().setLayoutManager(linearLayoutManager);
            getView().getRecyclerView().setAdapter(initAdapter());
            getView().getRecyclerView().setNestedScrollingEnabled(false);

            /*implemented listener to nested scroll view for pagination*/
            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

                            if (listModel != null && listModel.getTotalRecord() > propertiesList.size()) {
                                page += 1;
                                getPropertiesList();
                            }

                        }
                    }
                }
            });

            /*implemented listener to search text for searching data*/
            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });

            /*implemented listener to search text for searching data*/
            getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(final CharSequence s, int start, int before, int count) {
                    if (s != null && s.length() > 0) {
                        getView().getCloseBtn().setVisibility(View.VISIBLE);
                        searchQuery = s.toString();
                        page = 1;
                        isSearched = true;
                        getPropertiesList();


                    } else {
                        searchQuery = "";
                        page = 1;
                        isSearched = true;
                        propertiesList.clear();
                        adapter.update(propertiesList);
                        getPropertiesList();
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        getView().getCloseBtn().setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
        getPropertiesList();
    }

    /**
     * @param isAscending if true then sort list in ascending else descending.
     */
    private void sortPropertiesList(boolean isAscending) {
        if (getActivity() != null && isViewAttached()) {
            if (isAscending) {
                page = 1;
                getPropertiesList();
            } else {
                page = 1;
                getPropertiesList();
            }
        }
    }

    /**
     * unregister receiver after activity finished.
     */
    @Override
    public void onDestroy() {
        if (getActivity() != null && isViewAttached())
            getActivity().unregisterReceiver(propertiesReceiver);
        super.onDestroy();

    }

    /*
        Fetch Property List by Hitting API
         */
    private void getPropertiesList() {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    if (TextUtils.isEmpty(searchQuery))
                        getActivity().showProgressDialog();

                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    if (!TextUtils.isEmpty(OwnerDashboardActivity.sort_order))
                        param.put(Constants.SORT_ORDER, OwnerDashboardActivity.sort_order);
                    if (!TextUtils.isEmpty(searchQuery))
                        param.put(Constants.SEARCH_PARAM, searchQuery);
                    param.put(Constants.PAGE_PARAM, String.valueOf(page));

                    getInterActor().getPropertiesList(param, ApiName.GET_PROPERTIES_LIST);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @param layoutPosition returns clicked item position
     * @param click          it differentiate that which action user want to perform either detail or edit or delete.
     */

    public void onItemClicked(final int layoutPosition, ScreenNavigation click) {
        Intent intent = new Intent(getActivity(), HolderActivity.class);
        switch (click) {
            case DETAIL_ITEM:
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_PROPERTY);
                intent.putExtra(Constants.PROPERTY_ID, String.valueOf(propertiesList.get(layoutPosition).getId()));
                getActivity().startActivity(intent);
                break;
            case EDIT_ITEM:
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_EDIT_PROPERTY);
                intent.putExtra(Constants.PROPERTY_ID, String.valueOf(propertiesList.get(layoutPosition).getId()));
                getActivity().startActivity(intent);
                break;
            case DELETE_ITEM:
                Utils.getInstance().confirmDialog(getActivity(), "Are you sure, you want to delete this property?", "delete", new Utils.ConfirmDialogCallbackInterface() {
                    @Override
                    public void onYesClick(String tag) {
                        deletedPropertyPosition = layoutPosition;
                        deleteProperty(String.valueOf(propertiesList.get(layoutPosition).getId()));
                    }

                    @Override
                    public void onNoClick(String tag) {

                    }
                });

                break;
        }
    }

    /**
     * @param layoutPosition returns clicked item position
     * @param click          it differentiate that which action user want to perform either detail or something else.
     * @param imageView      returns clicked item imageView.
     */
    public void onItemClicked(final int layoutPosition, ScreenNavigation click, ImageView imageView) {
        Intent intent = new Intent(getActivity(), HolderActivity.class);
        switch (click) {
            case DETAIL_ITEM:
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_PROPERTY);
                intent.putExtra(Constants.PROPERTY_ID, String.valueOf(propertiesList.get(layoutPosition).getId()));
                if (Utils.getInstance().isEqualLollipop()) {
                    Pair<View, String> pair = Pair.create((View) imageView, Constants.TRANS_PROPERTY_IMAGE);
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pair);
                    getActivity().startActivity(intent, options.toBundle());
                } else {
                    getActivity().startActivity(intent);
                }
                break;
        }
    }

    /**
     * @param response returns the api response.
     * @param api      differentiate that which type response is getting from server.
     */

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_PROPERTIES_LIST:
                        if (response.isSuccessful()) {
                            listModel = (PropertiesListModel) response.body();
                            switch (listModel.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(listModel);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((OwnerDashboardActivity) getActivity()).logout(listModel.getMessage());
                                    break;
                            }
                        }
                        break;

                    case DELETE_PROPERTY_LIST:
                        if (response.isSuccessful()) {
                            GenericBean genericBean = (GenericBean) response.body();
                            switch (genericBean.getStatus()) {
                                case Constants.STATUS_200:
                                    if (adapter != null) {
                                        propertiesList.remove(deletedPropertyPosition);
                                        adapter.deleteItem(deletedPropertyPosition);
                                    }
                                    if (propertiesList != null && propertiesList.size() > 0) {
                                        getView().getEmptyView().setVisibility(View.GONE);
                                    } else {
                                        getView().getEmptyView().setVisibility(View.VISIBLE);
                                    }
                                    Intent listIntentEvents = new Intent(Constants.GET_EVENTS);
                                    Intent listIntentProjects = new Intent(Constants.GET_PROJECTS);
                                    listIntentEvents.putExtra("DeleteProperty", "");
                                    listIntentProjects.putExtra("DeleteProperty", "");
                                    getActivity().sendBroadcast(listIntentEvents);
                                    getActivity().sendBroadcast(listIntentProjects);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((OwnerDashboardActivity) getActivity()).logout(genericBean.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update property list.
     *
     * @param listModel provide response of property list from server.
     */

    private void onSuccess(PropertiesListModel listModel) {
        if (listModel.getData() != null && listModel.getData().size() > 0) {
            getView().getEmptyView().setVisibility(View.GONE);
            if (propertiesList == null) propertiesList = new ArrayList<>();

            if (isSearched || searchQuery.length() != 0) {
                propertiesList = new ArrayList<>();
                propertiesList.addAll(listModel.getData());
                isSearched = false;
            } else if (listModel.getTotalRecord() > propertiesList.size()) {
                propertiesList.addAll(listModel.getData());
            }
            if (isFilterChanged) {
                propertiesList = listModel.getData();
                isFilterChanged = false;
            }

            adapter.update(propertiesList);
        } else {
            if (propertiesList.isEmpty()) {
                adapter.clearAll();
                getView().getEmptyView().setVisibility(View.VISIBLE);
            } else {
                if (isSearched || searchQuery.length() != 0) {
                    adapter.clearAll();
                    getView().getEmptyView().setVisibility(View.VISIBLE);
                }
            }
        }
    }

    /**
     * Call api to delete the property.
     *
     * @param propertyID gives propertyID which user want to delete.
     */
    private void deleteProperty(String propertyID) {
        try {
            if (getActivity() != null) {
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.PROPERTY_ID, propertyID);
                getInterActor().deleteProperty(param, ApiName.DELETE_PROPERTY_LIST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {

        isFilterChanged = true;
        page = 1;
        switch (item.getItemId()) {
            case R.id.action_asc:
                OwnerDashboardActivity.sort_order = "ASC";
                sortPropertiesList(true);
//                        fabFilterProperties.callOnClick();
                break;
            case R.id.action_des:
                OwnerDashboardActivity.sort_order = "DESC";
                sortPropertiesList(false);
//                        fabFilterProperties.callOnClick();
                break;
        }
        return true;


    }
}
