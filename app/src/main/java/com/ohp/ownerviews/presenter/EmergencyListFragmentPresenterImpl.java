package com.ohp.ownerviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.adapter.ContactAdapter;
import com.ohp.ownerviews.beans.AllContactListResponse;
import com.ohp.ownerviews.view.EmergencyListFragmentView;
import com.ohp.serviceproviderviews.activities.SPDashboardActivity;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * Created by vishal.sharma on 8/31/2017.
 */

public class EmergencyListFragmentPresenterImpl extends FragmentPresenter<EmergencyListFragmentView, APIHandler> implements APIResponseInterface.OnCallableResponse,
        View.OnClickListener, ContactAdapter.ContactSelectListener {
    private int page = 1;
    private String searchQuery = "";
    private boolean isSearched = false, isFilterChanged = false;
    private ContactAdapter adapter;
    private List<AllContactListResponse.DataBean> contactListResponse = new ArrayList<>();
    private BroadcastReceiver contactListReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("DashboardActivityPresenterImpl.onReceive");
            if (getActivity() != null && isViewAttached()) {
                page = 1;
                contactListResponse = new ArrayList<>();
                getContactList();
            }

        }
    };
    private AllContactListResponse allContactListResponse;
    private int adapterPosition = 0;

    public EmergencyListFragmentPresenterImpl(EmergencyListFragmentView emergencyListFragmentView, Context context) {
        super(emergencyListFragmentView, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(contactListReciever, new IntentFilter(Constants.CONTACT_LIST_UPDATE));


            LinearLayoutManager lm = new LinearLayoutManager(getActivity());
            lm.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getRecylerContactList().setLayoutManager(lm);
            adapter = new ContactAdapter(getActivity(), null, this, this);
            getView().getRecylerContactList().setNestedScrollingEnabled(false);
            getView().getRecylerContactList().setAdapter(adapter);
            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

                            if (allContactListResponse != null && allContactListResponse.getTotalRecord() > contactListResponse.size()) {

                                System.out.println("total Records" + allContactListResponse.getTotalRecord());
                                page += 1;
                                getContactList();
                            }
                        }
                    }
                }
            });
            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });
            getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s != null && s.length() > 0) {
                        getView().getCloseBtn().setVisibility(View.VISIBLE);
                        searchQuery = s.toString();
                        page = 1;
                        isSearched = true;
                        getContactList();

                    } else {
                        searchQuery = "";
                        page = 1;
                        isSearched = true;
                        getContactList();
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        getView().getCloseBtn().setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });


        }
        getContactList();
    }

    @Override
    public void onDestroy() {
        if (getActivity() != null && isViewAttached())
            getActivity().unregisterReceiver(contactListReciever);
        super.onDestroy();

    }

    private void getContactList() {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        if (Utils.getInstance().isInternetAvailable(getActivity())) {
                            if (TextUtils.isEmpty(searchQuery))
                                getActivity().showProgressDialog();
                            Map<String, String> params = new HashMap<>();
                            params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                            if (!TextUtils.isEmpty(searchQuery))
                                params.put(Constants.SEARCH_PARAM, searchQuery);
                            params.put(Constants.PAGE_PARAM, String.valueOf(page));
                            getInterActor().getContactList(params, ApiName.GET_CONTACT_LISTING);

                        } else {
                            getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            switch (v.getId()) {
                case R.id.fab_add:
                    Intent intent = new Intent(getActivity(), HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_ADD_CONTACT);
                    getActivity().startActivity(intent);
                    break;
                case R.id.btn_clear:
                    searchQuery = "";
                    page = 1;
                    getView().getSearchEditText().setText("");
                    break;
            }
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_CONTACT_LISTING:
                        if (response.isSuccessful()) {
                            allContactListResponse = (AllContactListResponse) response.body();
                            switch (allContactListResponse.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(allContactListResponse);
                                    break;
                                case Constants.STATUS_201:
                                    Utils.getInstance().showToast(allContactListResponse.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(allContactListResponse.getMessage());
                                    break;
                            }
                        }

                        break;
                    case DELETE_CONTACT:
                        if (response.isSuccessful()) {
                            GenericBean genericBean = (GenericBean) response.body();
                            switch (genericBean.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(genericBean);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    if (getActivity() instanceof OwnerDashboardActivity)
                                        ((OwnerDashboardActivity) getActivity()).logout(genericBean.getMessage());
                                    else if (getActivity() instanceof SPDashboardActivity)
                                        ((SPDashboardActivity) getActivity()).logout(genericBean.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(GenericBean genericBean) {
        if (getActivity() != null && isViewAttached()) {
            contactListResponse.remove(adapterPosition);
            adapter.notifyDataSetChanged();

            Intent listIntent = new Intent(Constants.CONTACT_LIST_UPDATE);
            getActivity().sendBroadcast(listIntent);
        }
    }

    private void onSuccess(AllContactListResponse allContactListResponse) {

        if (allContactListResponse.getData() != null && allContactListResponse.getData().size() > 0) {
            getView().getEmptyLayout().setVisibility(View.GONE);
            if (isSearched || searchQuery.length() != 0) {
                contactListResponse = new ArrayList<>();
                contactListResponse.addAll(allContactListResponse.getData());
                isSearched = false;
            } else if (allContactListResponse.getTotalRecord() > contactListResponse.size()) {
                contactListResponse.addAll(allContactListResponse.getData());
            }
            if (isFilterChanged) {
                contactListResponse = allContactListResponse.getData();
                isFilterChanged = false;
            }
            adapter.updateList(contactListResponse);

        } else {
            if (contactListResponse.isEmpty()) {
                adapter.clearAll();
                getView().getEmptyLayout().setVisibility(View.VISIBLE);
            } else {
                if (isSearched || searchQuery.length() != 0) {
                    adapter.clearAll();
                    getView().getEmptyLayout().setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMemberSelect(AllContactListResponse obj) {

    }

    @Override
    public void onInviteBtnClick(AllContactListResponse contactObj) {

    }

    public void deleteProject(int contactId, int position) {
        try {
            if (getActivity() != null && isViewAttached()) {
                adapterPosition = position;
                getActivity().showProgressDialog();
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Map<String, String> params = new HashMap<>();
                    params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    params.put(Constants.CONTACT_ID, String.valueOf(contactId));
                    getInterActor().deleteContact(params, ApiName.DELETE_CONTACT);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
