package com.ohp.ownerviews.presenter;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.ResourcesAdapter;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.beans.ResourceBean;
import com.ohp.commonviews.beans.ResourcesValueModel;
import com.ohp.enums.ApiName;
import com.ohp.interfaces.OnFragmentInteractionListener;
import com.ohp.ownerviews.beans.PropertyDetailsModel;
import com.ohp.ownerviews.fragment.AddEditPropertyFragment;
import com.ohp.ownerviews.view.AddEditPropertyView;
import com.ohp.utils.CalendarPicker;
import com.ohp.utils.Constants;
import com.ohp.utils.FileUtils;
import com.ohp.utils.PermissionsAndroid;
import com.ohp.utils.Utils;
import com.ohp.utils.Validations;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.ohp.utils.Constants.BROWSE_PIC;

/**
 * @author Amanpal Singh.
 */

public class AddEditPropertyFragmentPresenterImpl extends FragmentPresenter<AddEditPropertyView, APIHandler> implements APIResponseInterface.OnCallableResponse,
        View.OnClickListener, ImagePickerCallback {
    private ResourcesAdapter resourceAdapter;

    private boolean isImageSelected = false, cameraSelected = false, gallerySelected = false;
    private String propertyImagePath = "";
    private int PLACE_PICKER_REQUEST = 1;
    private Place place;
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private List<ResourcesValueModel> listResourcesValue = new ArrayList<>();
    private String pickerPath, mPropertyName = "", mPropertyArea = "", mPropertyConstructionYear = "",
            mPropertyAddress = "", mPropertyDescription = "", latitude = "", longitude = "";
    private ResourceBean resourceObj;
    private OnFragmentInteractionListener mListener;

    private BroadcastReceiver addEditPropertyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("DashboardActivityPresenterImpl.onReceive");
            Intent listIntent = new Intent(Constants.GET_PROPERTIES_LIST);
            context.sendBroadcast(listIntent);
            if (isViewAttached())
                getPropertyDetails(getView().getPropertyID());
        }
    };
    private PropertyDetailsModel dataObj;

    public AddEditPropertyFragmentPresenterImpl(AddEditPropertyView addEditPropertyView, Context context) {
        super(addEditPropertyView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HolderActivity) {
            mListener = ((HolderActivity) context).getPresenter();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getResources();
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("picker_path")) {
                pickerPath = savedInstanceState.getString("picker_path");
            }
        }

        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(addEditPropertyReceiver,
                    new IntentFilter(Constants.ADD_EDIT_PROPERTY));
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");

            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    backPress();
                }
            });

            //set scroll listener on address field
            getView().getPropertyName().setOnTouchListener(new View.OnTouchListener() {  //Register a callback to be invoked when a touch event is sent to this view.
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    //Called when a touch event is dispatched to a view. This allows listeners to get a chance to respond before the target view.
                    getView().getScrollView().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });

            getView().getDescription().setOnTouchListener(new View.OnTouchListener() {  //Register a callback to be invoked when a touch event is sent to this view.
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    //Called when a touch event is dispatched to a view. This allows listeners to get a chance to respond before the target view.
                    getView().getScrollView().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });

            switch (getView().getScreenCheck()) {
                case Constants.CLICK_VIEW_PROPERTY:
                    getView().getLlAdditionalInfoView().setVisibility(View.VISIBLE);
                    disabledFields();
                    getView().getToolbarTitle().setText(R.string.title_property_detail);
                    getPropertyDetails(getView().getPropertyID());
                    break;
                case Constants.CLICK_EDIT_PROPERTY:
                    getView().getPropertyNameLayoutView().setVisibility(View.VISIBLE);
                    getView().getToolbarTitle().setText(R.string.title_edit_property);
                    getPropertyDetails(getView().getPropertyID());
                    break;
                case Constants.CLICK_ADD_PROPERTY:
                    getView().getPropertyNameLayoutView().setVisibility(View.VISIBLE);
                    getView().getToolbarTitle().setText(R.string.title_add_property);
                    break;
            }
        }


        //Add Dummy Data to resource recyclerView
        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        lm.setOrientation(LinearLayoutManager.HORIZONTAL);
        getView().getResourceRecycler().setLayoutManager(lm);
        resourceAdapter = new ResourcesAdapter(getActivity(), null, this, listResourcesValue, true);
        getView().getResourceRecycler().setAdapter(resourceAdapter);

    }

    private void getPropertyDetails(String propertyID) {
        try {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Map<String, String> params = new HashMap<>();
                params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                params.put(Constants.PROPERTY_ID, propertyID);
                getInterActor().getPropertyDetails(params, ApiName.PROPERTY_DETAILS);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getResources() {
        try {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                //fetch resources
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                getActivity().showProgressDialog();
                getInterActor().getResources(param, ApiName.GET_RESOURCES_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void backPress() {
        ((HolderActivity) getActivity()).oneStepBack();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {

                case R.id.avatar:
                    if (propertyImagePath != null && !propertyImagePath.isEmpty()) {
                        Intent profilePicIntent = new Intent(getActivity(), HolderActivity.class);
                        profilePicIntent.putExtra(Constants.DESTINATION, BROWSE_PIC);
                        profilePicIntent.putExtra(BROWSE_PIC, propertyImagePath);
                        profilePicIntent.putExtra(Constants.PIC_TITLE, Constants.TITLE_PROPERTY_IMAGE);
                        if (Utils.getInstance().isEqualLollipop()) {
                            Pair<View, String> p1 = Pair.create((View) getView().getPropertyImage(), Constants.TRANS_PROPERTY_IMAGE);
                            ActivityOptions options =
                                    ActivityOptions.makeSceneTransitionAnimation(getActivity(), p1);
                            getActivity().startActivity(profilePicIntent, options.toBundle());
                        } else {
                            getActivity().startActivity(profilePicIntent);
                        }
                    } else if (dataObj != null && !dataObj.getData().getPropertyImage200X200().isEmpty()) {
                        Intent profilePicIntent = new Intent(getActivity(), HolderActivity.class);
                        profilePicIntent.putExtra(Constants.DESTINATION, BROWSE_PIC);
                        profilePicIntent.putExtra(Constants.PIC_TITLE, Constants.TITLE_PROPERTY_IMAGE);
                        profilePicIntent.putExtra(BROWSE_PIC, dataObj.getData().getPropertyImage200X200());
                        if (Utils.getInstance().isEqualLollipop()) {
                            Pair<View, String> p1 = Pair.create((View) getView().getPropertyImage(), Constants.TRANS_PROPERTY_IMAGE);
                            ActivityOptions options =
                                    ActivityOptions.makeSceneTransitionAnimation(getActivity(), p1);
                            getActivity().startActivity(profilePicIntent, options.toBundle());
                        } else {
                            getActivity().startActivity(profilePicIntent);
                        }
                    }

                    break;

                case R.id.iv_add_icon:
                    // show picker for image capture
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String[] options = {"Camera", "Gallery", "Cancel"};
                    builder.setTitle("Choose Option")
                            .setItems(options, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    switch (which) {
                                        case 0:
                                            cameraSelected = true;
                                            gallerySelected = false;
                                            checkCameraPermission();
                                            break;
                                        case 1:


                                            cameraSelected = false;
                                            gallerySelected = true;
                                            checkCameraPermission();
                                            break;
                                        case 2:
                                            dialog.dismiss();
                                            break;
                                    }
                                }
                            });
                    builder.setCancelable(false);
                    builder.show();


                    break;
                case R.id.et_search_address:
                    //Move to Search Address Fragment
                    getActivity().showProgressDialog();
                    try {
                        PlacePicker.IntentBuilder placeBuilder = new PlacePicker.IntentBuilder();
                        getActivity().startActivityForResult(placeBuilder.build(getActivity()), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                        getActivity().hideProgressDialog();
                    }
                    break;

                case R.id.et_construction_year:
                    CalendarPicker.getInstance().showCalendar(getActivity(), this, v.getId(), "");
                    break;
                case R.id.iv_add_documents:
                    intent = new Intent(getActivity(), HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.ADD_EDIT_PROPERTY);
                    intent.putExtra(Constants.PROPERTY_ID, getView().getPropertyID());
                    intent.putExtra(Constants.NAVIGATE_SCREEN, Constants.ADD_EDIT_DOCUMENT_SCREEN);
                    getActivity().startActivity(intent);
                    /*bundle = new Bundle();
                    bundle.putString(Constants.DESTINATION, Constants.ADD_EDIT_PROPERTY);
                    bundle.putString(Constants.PROPERTY_ID, getView().getPropertyID());
                    mListener.navigateTo(CurrentScreen.ADD_EDIT_DOCUMENT_SCREEN, bundle);*/
                    break;

                case R.id.ll_document:
                    intent = new Intent(getActivity(), HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.ADD_EDIT_PROPERTY);
                    intent.putExtra(Constants.PROPERTY_ID, getView().getPropertyID());
                    intent.putExtra(Constants.NAVIGATE_SCREEN, Constants.DOCUMENT_LIST_SCREEN);
                    getActivity().startActivity(intent);
                    /*bundle = new Bundle();
                    bundle.putString(Constants.DESTINATION, Constants.ADD_EDIT_PROPERTY);
                    bundle.putString(Constants.PROPERTY_ID, getView().getPropertyID());
                    mListener.navigateTo(CurrentScreen.DOCUMENT_LIST_SCREEN, bundle);*/
                    break;
                case R.id.ll_events:
                    intent = new Intent(getActivity(), HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.ADD_EDIT_PROPERTY);
                    intent.putExtra(Constants.PROPERTY_ID, getView().getPropertyID());
                    intent.putExtra(Constants.NAVIGATE_SCREEN, Constants.EVENT_LIST_SCREEN);
                    getActivity().startActivity(intent);
                    /*bundle = new Bundle();
                    bundle.putString(Constants.DESTINATION, Constants.ADD_EDIT_PROPERTY);
                    bundle.putString(Constants.PROPERTY_ID, getView().getPropertyID());
                    mListener.navigateTo(CurrentScreen.OWNER_EVENT_LIST_SCREEN, bundle);*/
                    break;
                case R.id.iv_add_appliances:
                    intent = new Intent(getActivity(), HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.ADD_EDIT_PROPERTY);
                    intent.putExtra(Constants.PROPERTY_ID, getView().getPropertyID());
                    intent.putExtra(Constants.NAVIGATE_SCREEN, Constants.ADD_EDIT_APPLIANCE_SCREEN);
                    getActivity().startActivity(intent);
                    /*bundle = new Bundle();
                    bundle.putString(Constants.DESTINATION, Constants.ADD_EDIT_PROPERTY);
                    bundle.putString(Constants.PROPERTY_ID, getView().getPropertyID());
                    mListener.navigateTo(CurrentScreen.ADD_EDIT_APPLIANCE_SCREEN, bundle);*/
                    break;
                case R.id.ll_appliances:
                    intent = new Intent(getActivity(), HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.ADD_EDIT_PROPERTY);
                    intent.putExtra(Constants.PROPERTY_ID, getView().getPropertyID());
                    intent.putExtra(Constants.NAVIGATE_SCREEN, Constants.APPLIANCE_LIST_SCREEN);
                    getActivity().startActivity(intent);
                    /*bundle = new Bundle();
                    bundle.putString(Constants.DESTINATION, Constants.ADD_EDIT_PROPERTY);
                    bundle.putString(Constants.PROPERTY_ID, getView().getPropertyID());
                    mListener.navigateTo(CurrentScreen.APPLIANCE_LIST_SCREEN, bundle);*/
                    break;
                case R.id.iv_add_projects:
                    intent = new Intent(getActivity(), HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_ADD_PROJECT);
                    intent.putExtra(Constants.PROPERTY_ID, getView().getPropertyID());
                    getActivity().startActivity(intent);
                    /*bundle = new Bundle();
                    bundle.putString(Constants.DESTINATION, Constants.CLICK_ADD_PROJECT);
                    bundle.putString(Constants.PROPERTY_ID, getView().getPropertyID());
                    mListener.navigateTo(CurrentScreen.ADD_PROJECT_SCREEN, bundle);*/
                    break;

                case R.id.allProjects:
                    intent = new Intent(getActivity(), HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_ALL_PROJECTS);
                    intent.putExtra(Constants.PROPERTY_ID, getView().getPropertyID());
                    getActivity().startActivity(intent);
                    break;
                case R.id.iv_add_reminders:
                    intent = new Intent(getActivity(), HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.ADD_EDIT_PROPERTY);
                    intent.putExtra(Constants.PROPERTY_ID, getView().getPropertyID());
                    intent.putExtra(Constants.NAVIGATE_SCREEN, Constants.ADD_EDIT_EVENT_SCREEN);
                    getActivity().startActivity(intent);
                    /*bundle = new Bundle();
                    bundle.putString(Constants.DESTINATION, Constants.ADD_EDIT_PROPERTY);
                    bundle.putString(Constants.PROPERTY_ID, getView().getPropertyID());
                    mListener.navigateTo(CurrentScreen.OWNER_ADD_EDIT_EVENT_SCREEN, bundle);*/
                    break;
            }
        }
    }

    //  @TargetApi(Build.VERSION_CODES.M)
    private void checkCameraPermission() {
        boolean isExternalStorage = PermissionsAndroid.getInstance().checkCameraPermission(getActivity());
        if (!isExternalStorage) {
            PermissionsAndroid.getInstance().requestForCameraPermission(((AddEditPropertyFragment) getView()));
        } else if (cameraSelected) {
            takePicture();
        } else if (gallerySelected) {
            pickImageSingle();
        }
    }

    private void takePicture() {
        cameraPicker = new CameraImagePicker(getActivity());
//        cameraPicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        cameraPicker.setImagePickerCallback(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        pickerPath = cameraPicker.pickImage();
    }

    private void pickImageSingle() {
        imagePicker = new ImagePicker(getActivity());
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        if (getActivity() != null && isViewAttached()) {
            String extension = "";
            int i = list.get(0).getDisplayName().lastIndexOf('.');
            if (i > 0) {
                extension = list.get(0).getDisplayName().substring(i + 1);
            }
            if (!extension.equals("png") && !extension.equals("jpg") && !extension.equals("jpeg")) {
                getActivity().showSnakBar(getView().getRootView(), "you can choose only images.");
                return;
            }
            ChosenImage image = list.get(0);
            isImageSelected = true;
            propertyImagePath = FileUtils.getPath(getActivity(), Uri.parse(image.getQueryUri()));

            Picasso.with(getActivity()).load(new File(propertyImagePath)).resize(400, 400).centerCrop().memoryPolicy(MemoryPolicy.NO_CACHE).into(getView().getPropertyImage());

        }

    }

    @Override
    public void onError(String s) {

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (getActivity() != null && isViewAttached()) {
            getActivity().hideProgressDialog();
            if (resultCode == RESULT_OK) {
                if (requestCode == PLACE_PICKER_REQUEST) {
                    place = PlacePicker.getPlace(data, getActivity());
                    if (place != null) {
                        getView().getAddress().setText(place.getAddress());
                    }
                } else if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                    if (imagePicker == null) {
                        imagePicker = new ImagePicker(getActivity());
                    }
                    imagePicker.submit(data);
                } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                    if (cameraPicker == null) {
                        cameraPicker = new CameraImagePicker(getActivity());
                        cameraPicker.setImagePickerCallback(this);
                        cameraPicker.reinitialize(pickerPath);
                    }
                    cameraPicker.submit(data);
                }
            } else {
//                getActivity().showSnakBar(getView().getRootView(), "Request cancelled");
//                Utils.getInstance().showToast("Request cancelled");
            }

        }
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 102: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Show option to update profile
                    if (cameraSelected) {
                        takePicture();
                    } else if (gallerySelected) {
                        pickImageSingle();
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Utils.getInstance().showToast("Camera/Storage permission Denied");
                }
                return;
            }

        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_RESOURCES_API:
                        if (response.isSuccessful()) {
                            ResourceBean resourceObj = (ResourceBean) response.body();
                            switch (resourceObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(resourceObj);
                                    break;
                                case Constants.STATUS_201:
                                    getActivity().showSnakBar(getView().getRootView(), resourceObj.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(resourceObj.getMessage());
                                    break;
                            }
                        }
                        break;
                    case ADD_EDIT_PROPERTY:
                        if (response.isSuccessful()) {
                            GenericBean genericBeanObj = (GenericBean) response.body();
                            switch (genericBeanObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(genericBeanObj);
                                    break;
                                case Constants.STATUS_201:
                                    getActivity().showSnackBar(getView().getRootView(), genericBeanObj.getMessage());
                                    break;
                                case Constants.STATUS_203:
                                    getActivity().showSnackBar(getView().getRootView(), genericBeanObj.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(genericBeanObj.getMessage());
                                    break;
                            }
                        }
                        break;

                    case PROPERTY_DETAILS:
                        if (response.isSuccessful()) {
                            PropertyDetailsModel propertyDetailsObj = (PropertyDetailsModel) response.body();
                            switch (propertyDetailsObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(propertyDetailsObj);
                                    break;
                                case Constants.STATUS_201:
                                    getActivity().showSnakBar(getView().getRootView(), propertyDetailsObj.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(propertyDetailsObj.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(PropertyDetailsModel dataObj) {
        this.dataObj = dataObj;
        try {
            if (getActivity() != null && isViewAttached()) {
                latitude = dataObj.getData().getLatitude();
                longitude = dataObj.getData().getLongitude();
                getView().getPropertyName().setText(dataObj.getData().getPropertyName());
                getView().getPropertyNameTextView().setText(dataObj.getData().getPropertyName());
                getView().getDescription().setText(dataObj.getData().getDescription());
                getView().getPropertyDescriptionTextView().setText(dataObj.getData().getDescription());
                getView().getPropertyArea().setText(dataObj.getData().getArea());
                getView().getAddress().setText(dataObj.getData().getAddress());
                getView().getPropertyAddressTextView().setText(dataObj.getData().getAddress());
                getView().getPropertyConstYear().setText(dataObj.getData().getConstructionYear());

                getView().getDocumentCountView().setText(String.valueOf(dataObj.getData().getDocumentCount()));
                getView().getProjectCountView().setText(String.valueOf(dataObj.getData().getProjectCount()));
                getView().getApplianceCountView().setText(String.valueOf(dataObj.getData().getApplianceCount()));
                getView().getRemindersCountView().setText(String.valueOf(dataObj.getData().getReminderCount()));
                getView().getProjectCountView().setText(String.valueOf(dataObj.getData().getProjectCount()));
                if (!TextUtils.isEmpty(dataObj.getData().getPropertyImage200X200())) {
                    Picasso.with(getActivity()).load(dataObj.getData().getPropertyImage200X200()).
                            into(getView().getPropertyImage(), new Callback() {
                                @Override
                                public void onSuccess() {
//                                    getView().getAddIconView().setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
//                                    getView().getAddIconView().setVisibility(View.GONE);
                                    getView().getPropertyImage().setImageResource(R.drawable.default_property);
                                }
                            });
                } else {
                    getView().getPropertyImage().setImageResource(R.drawable.default_property);
                }

                for (int i = 0; i < listResourcesValue.size(); i++) {
                    if (listResourcesValue.get(i).getTitle().equals(dataObj.getData().getResource().get(i).getTitle())) {
                        ResourcesValueModel dataModel = new ResourcesValueModel();
                        dataModel.setTitle(dataObj.getData().getResource().get(i).getTitle());
                        dataModel.setCount(dataObj.getData().getResource().get(i).getValue());
                        listResourcesValue.set(i, dataModel);
                    }
                }

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        resourceAdapter.updateResourceValues(listResourcesValue);
                    }
                });


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void disabledFields() {
        if (getActivity() != null && isViewAttached()) {
            getView().getLlAdditionalInfoView().setVisibility(View.VISIBLE);
            getView().getPropertyName().setEnabled(false);
            getView().getAddress().setEnabled(false);
            getView().getDescription().setEnabled(false);
            getView().getPropertyArea().setEnabled(false);
            getView().getPropertyConstYear().setEnabled(false);
            getView().getPropertyConstYear().setOnClickListener(null);
            getView().getAddress().setOnClickListener(null);
//            getView().getPropertyImage().setOnClickListener(null);
            getView().getPropertyNameLayoutView().setEnabled(false);
            getView().getAddIconView().setVisibility(View.GONE);

            getView().gettvPropertyView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().getResourcesView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().gettvInfo().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));

            //hide and show textview layouts and textinput layouts
            getView().getPropertyName().setVisibility(View.GONE);
            getView().getPropertyNameLayoutView().setVisibility(View.GONE);
            getView().getPropertyAddressLayoutView().setVisibility(View.GONE);
            getView().getPropertyDescriptionLayoutView().setVisibility(View.GONE);
            getView().getAddress().setVisibility(View.GONE);
            getView().getDescription().setVisibility(View.GONE);
            getView().getLlPropertyNameView().setVisibility(View.VISIBLE);
            getView().getLlPropertyAddressView().setVisibility(View.VISIBLE);
            getView().getLlPropertyDescriptionView().setVisibility(View.VISIBLE);
            if (resourceObj != null)
                resourceAdapter = new ResourcesAdapter(getActivity(), resourceObj.getData(), this, listResourcesValue, false);
            getView().getResourceRecycler().setAdapter(resourceAdapter);
        }
    }

    public void enabledFields() {
        if (getActivity() != null && isViewAttached()) {
            getView().getLlAdditionalInfoView().setVisibility(View.GONE);
            getView().getPropertyName().setEnabled(true);
            getView().getAddIconView().setVisibility(View.VISIBLE);
            getView().getDescription().setEnabled(true);
            getView().getPropertyArea().setEnabled(true);
            getView().getAddress().setEnabled(true);
            getView().getPropertyConstYear().setEnabled(true);
//            getView().getPropertyConstYear().setOnClickListener(this);
            getView().getAddress().setOnClickListener(this);
            getView().getPropertyImage().setOnClickListener(this);
            getView().getPropertyNameLayoutView().setEnabled(true);

            //hide and show textview layouts and textinput layouts
            getView().getPropertyName().setVisibility(View.VISIBLE);
            getView().getPropertyName().setFocusable(true);
            getView().getPropertyName().setFocusableInTouchMode(true);
            getView().getPropertyName().requestFocus();

            getView().gettvPropertyView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            getView().getResourcesView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            getView().gettvInfo().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));

            getView().getPropertyNameLayoutView().setVisibility(View.VISIBLE);
            getView().getPropertyAddressLayoutView().setVisibility(View.VISIBLE);
            getView().getPropertyDescriptionLayoutView().setVisibility(View.VISIBLE);
            getView().getAddress().setVisibility(View.VISIBLE);
            getView().getDescription().setVisibility(View.VISIBLE);
            getView().getLlPropertyNameView().setVisibility(View.GONE);
            getView().getLlPropertyAddressView().setVisibility(View.GONE);
            getView().getLlPropertyDescriptionView().setVisibility(View.GONE);
            if (resourceObj != null)
                resourceAdapter = new ResourcesAdapter(getActivity(), resourceObj.getData(), this, listResourcesValue, true);
            getView().getResourceRecycler().setAdapter(resourceAdapter);
        }
    }

    private void onSuccess(GenericBean genericBeanObj) {
        if (getActivity() != null && isViewAttached()) {
            Intent listIntent = new Intent(Constants.GET_PROPERTIES_LIST);
            getActivity().sendBroadcast(listIntent);
            switch (getView().getScreenCheck()) {
                /*case Constants.CLICK_VIEW_PROPERTY:
                    getView().getEditMenu().setVisible(true);
                    getView().getSaveMenu().setVisible(false);
                    disabledFields();
                    break;*/
                default:
                    ((HolderActivity) getActivity()).oneStepBack();
                    break;
            }
        }


    }

    private void onSuccess(ResourceBean resourceObj) {

        if (isViewAttached()) {
            if (resourceAdapter != null) {
                this.resourceObj = resourceObj;
                resourceAdapter.updateResource(resourceObj.getData());
                if (!listResourcesValue.isEmpty()) {
                    listResourcesValue.clear();
                }
                for (int i = 0; i < resourceObj.getData().size(); i++) {
                    ResourcesValueModel dataObj = new ResourcesValueModel();
                    dataObj.setTitle(resourceObj.getData().get(i).getTitle());
                    dataObj.setCount("");
                    listResourcesValue.add(dataObj);
                }
                if (getView().getScreenCheck().equals(Constants.CLICK_VIEW_PROPERTY))
                    resourceAdapter = new ResourcesAdapter(getActivity(), resourceObj.getData(), this, listResourcesValue, false);
                else
                    resourceAdapter = new ResourcesAdapter(getActivity(), resourceObj.getData(), this, listResourcesValue, true);
                getView().getResourceRecycler().setAdapter(resourceAdapter);
            }
            switch (getView().getScreenCheck()) {
                case Constants.CLICK_VIEW_PROPERTY:
                    getPropertyDetails(getView().getPropertyID());
                    break;
                case Constants.CLICK_EDIT_PROPERTY:
                    getPropertyDetails(getView().getPropertyID());
                    break;
            }
        }

    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addEditProperty() {
        try {
            if (getActivity() != null) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    mPropertyName = getView().getPropertyName().getText().toString().trim();
                    mPropertyArea = getView().getPropertyArea().getText().toString().trim();
                    mPropertyAddress = getView().getAddress().getText().toString();
                    mPropertyDescription = getView().getDescription().getText().toString().trim();
                    mPropertyConstructionYear = getView().getPropertyConstYear().getText().toString().trim();

                    String response = Validations.getInstance().validateAddPropertyFields(mPropertyName,
                            mPropertyArea, mPropertyAddress, mPropertyDescription, mPropertyConstructionYear,
                            propertyImagePath, place, isImageSelected,
                            latitude, longitude, getView().getScreenCheck());

                    if (!response.trim().isEmpty()) {
                        getActivity().showSnackBar(getView().getRootView(), response);
                    } else {
                        getActivity().showProgressDialog();
                        // set editext to textview (proeprty name, address, description)
                        getView().getPropertyNameTextView().setText(mPropertyName);
                        getView().getPropertyAddressTextView().setText(mPropertyAddress);
                        getView().getPropertyDescriptionTextView().setText(mPropertyDescription);


                        Map<String, String> params = new HashMap<>();
                        if (getView().getScreenCheck().equals(Constants.CLICK_EDIT_PROPERTY) ||
                                getView().getScreenCheck().equals(Constants.CLICK_VIEW_PROPERTY)) {
                            params.put(Constants.PROPERTY_ID, getView().getPropertyID());
                            params.put(Constants.LATITUDE, latitude);
                            params.put(Constants.LONGITUDE, longitude);
                        } else {
                            params.put(Constants.LATITUDE, String.valueOf(place.getLatLng().latitude));
                            params.put(Constants.LONGITUDE, String.valueOf(place.getLatLng().longitude));
                        }

                        params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                        params.put(Constants.PROPERTY_NAME, Utils.getInstance().capitalize(mPropertyName));
                        params.put(Constants.AREA, mPropertyArea);
                        params.put(Constants.CONSTRUCTION_YEAR, mPropertyConstructionYear);
                        params.put(Constants.ADDRESS, mPropertyAddress);
                        params.put(Constants.DESCRIPTION, Utils.getInstance().capitalize(mPropertyDescription));


                        for (int i = 0; i < listResourcesValue.size(); i++) {
                            params.put(listResourcesValue.get(i).getTitle(),
                                    listResourcesValue.get(i).getCount());
                        }
                        if (isImageSelected)
                            getInterActor().addEditPropertiesWithPic(params, propertyImagePath, ApiName.ADD_EDIT_PROPERTY);
                        else
                            getInterActor().addEditProperties(params, ApiName.ADD_EDIT_PROPERTY);
                    }
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setResources(String title, String count) {

        for (int i = 0; i < listResourcesValue.size(); i++) {
            if (listResourcesValue.get(i).getTitle().equals(title)) {
                ResourcesValueModel dataModel = new ResourcesValueModel();
                dataModel.setTitle(title);
                dataModel.setCount(count);
                listResourcesValue.set(i, dataModel);
                return;
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null && isViewAttached())
            getActivity().unregisterReceiver(addEditPropertyReceiver);
        mListener = null;
    }


    public void setSelectedDate(String selectedDate) {
        if (isViewAttached()) {
            getView().getPropertyConstYear().setText(selectedDate);
        }
    }

}
