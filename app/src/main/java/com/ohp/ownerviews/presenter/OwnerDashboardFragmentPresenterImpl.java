package com.ohp.ownerviews.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.commonviews.adapter.DashboardPagerAdapter;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.fragment.AllProjectsFragments;
import com.ohp.ownerviews.fragment.EmergencyListFragment;
import com.ohp.ownerviews.fragment.OwnerEventsListFragment;
import com.ohp.ownerviews.fragment.PropertiesListFragment;
import com.ohp.ownerviews.view.OwnerDashboardFragmentView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class OwnerDashboardFragmentPresenterImpl extends FragmentPresenter<OwnerDashboardFragmentView, APIHandler> implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener {

    public DashboardPagerAdapter pagerAdapter;

    public OwnerDashboardFragmentPresenterImpl(OwnerDashboardFragmentView ownerDashboardFragmentView, Context context) {
        super(ownerDashboardFragmentView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return null;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            ((OwnerDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.title_dashboard);
        }
        setDashboardPagerAdapter();
    }

    private void setDashboardPagerAdapter() {
        //Creating our pager adapter and adding fragments for tabs with name.
        if (getActivity() != null && isViewAttached()) {
            String titles[] = new String[]{"Properties", "Projects", "Events", "Emergency"};
            List<Fragment> fragmentList = new ArrayList<>();
            fragmentList.add(new PropertiesListFragment());
            fragmentList.add(new AllProjectsFragments());
            fragmentList.add(new OwnerEventsListFragment());
            fragmentList.add(new EmergencyListFragment());
            DashboardPagerAdapter pagerAdapter = new DashboardPagerAdapter(getActivity().getSupportFragmentManager(), fragmentList, titles);
            getView().getViewPager().setAdapter(pagerAdapter);
            this.pagerAdapter = pagerAdapter;
        }

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
