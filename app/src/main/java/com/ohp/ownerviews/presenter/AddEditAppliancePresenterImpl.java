package com.ohp.ownerviews.presenter;

import android.app.ActivityOptions;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.FilePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.FilePickerCallback;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenFile;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.ApplianceTypeAndBrandAdapter;
import com.ohp.commonviews.adapter.PropertySpinnerAdapter;
import com.ohp.commonviews.adapter.ThumbImageAdapter;
import com.ohp.commonviews.adapter.ViewPagerSubCatAdapter;
import com.ohp.commonviews.beans.AddEditModel;
import com.ohp.commonviews.beans.GetPropertyResponse;
import com.ohp.commonviews.beans.ResourceBean;
import com.ohp.commonviews.beans.ThumbnailBean;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.beans.ApplianceDetailModel;
import com.ohp.ownerviews.fragment.AddEditApplianceFragment;
import com.ohp.ownerviews.view.AddEditApplianceView;
import com.ohp.service.ImagesResultReceiver;
import com.ohp.service.ImagesService;
import com.ohp.utils.Constants;
import com.ohp.utils.FileUtils;
import com.ohp.utils.PermissionsAndroid;
import com.ohp.utils.Utils;
import com.ohp.utils.Validations;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.ohp.enums.ApiName.GET_RESOURCES_API;
import static com.ohp.enums.ApiName.GET_RESOURCES_BRANDS;
import static com.ohp.utils.Constants.ADD_EDIT_PROPERTY;
import static com.ohp.utils.Constants.BROWSE_PIC;
import static com.ohp.utils.Constants.CLICK_ADD_APPLIANCE;
import static com.ohp.utils.Constants.CLICK_EDIT_APPLIANCE;
import static com.ohp.utils.Constants.CLICK_VIEW_APPLIANCE;
import static com.ohp.utils.Constants.NAVIGATE_FROM;

/**
 * @author Amanpal Singh.
 */

public class AddEditAppliancePresenterImpl extends FragmentPresenter<AddEditApplianceView, APIHandler> implements
        APIResponseInterface.OnCallableResponse, View.OnClickListener, FilePickerCallback, ImagePickerCallback,
        AdapterView.OnItemSelectedListener, ImagesResultReceiver.Receiver, View.OnTouchListener {

    private final BroadcastReceiver applianceDetailReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getExtras().containsKey(Constants.NAVIGATE_FROM)
                        && Constants.NOTIFICATIONS.equalsIgnoreCase(intent.getExtras().getString(NAVIGATE_FROM))) {
                    if (applianceId.equalsIgnoreCase(intent.getExtras().getString(Constants.APPLIANCE_ID)))
                        getApplianceDetail();
                } else
                    getApplianceDetail();
                Intent listIntent;
                if (currentScreen.equals(Constants.ADD_EDIT_PROPERTY))
                    listIntent = new Intent(ADD_EDIT_PROPERTY);
                else listIntent = new Intent(Constants.GET_APPLIANCE_LIST);
                context.sendBroadcast(listIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };
    private GetPropertyResponse propertyObj;
    private boolean bgApp = false;
    private ApplianceDetailModel applianceObj;
    private ResourceBean resourcesObj, resourceBrandObj;
    private ThumbImageAdapter thumbImageAdapter, thumbFileAdapter;
    private List<ThumbnailBean> uploadFileList;
    private List<ResourceBean.DataBean.InfoBean> listBrands = new ArrayList<>(), listApplianceType = new ArrayList<>();
    private List<ThumbnailBean> tempUploadFileList = new ArrayList<>();
    private List<ThumbnailBean> uploadImageList;
    private List<ThumbnailBean> tempUploadImageList = new ArrayList<>();
    private List<String> deletedImageIds = new ArrayList<>();
    private List<String> deletedFileIds = new ArrayList<>();
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private FilePicker filePicker;
    private String pickerPath, mApplianceName = "", mApplianceBrandId = "", mApplianceType = "",
            mPurchaseDate = "", mWarrantyDate = "", mExtendedWarrantyDate = "", applianceId = "", propertyId = "", currentScreen = "",
            modelName = "", serialNumber = "";
    private PropertySpinnerAdapter propertyAdapter;
    private ApplianceTypeAndBrandAdapter brandAdapter, applianceTypeAdapter;
    private ViewPagerSubCatAdapter imgAdapter, docsAdapter;
    /**
     * Show date picker for Purchase Date
     */
    private int purchaseDate = 0, purchaseMonth = 0, purchaseYear = 0;
    /**
     * Show date picker for Warranty Date
     */
    private int warrantyDate = 0, warrantyMonth = 0, warrantyYear = 0;
    //    private int imageType;
    private boolean isCameraSelected = false;
    private boolean isGallerySelected = false;
    private boolean forDocumentSection = false;

    public AddEditAppliancePresenterImpl(AddEditApplianceView addEditApplianceView, Context context) {
        super(addEditApplianceView, context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null && isViewAttached()) {
            getActivity().unregisterReceiver(applianceDetailReceiver);
        }
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("picker_path")) {
                pickerPath = savedInstanceState.getString("picker_path");
            }
        }
        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(applianceDetailReceiver, new IntentFilter(Constants.GET_APPLIANCE_DETAIL));
            switch (getView().getScreenCheck()) {
                case Constants.CLICK_VIEW_APPLIANCE:
                    getView().getToolbarTitle().setText(R.string.title_detail_appliance);
                    break;
                case Constants.CLICK_EDIT_APPLIANCE:
                    getView().getToolbarTitle().setText(R.string.title_edit_appliance);
                    break;
                case Constants.CLICK_ADD_APPLIANCE:
                    getView().getToolbarTitle().setText(R.string.title_add_appliance);
                    break;
                case Constants.ADD_EDIT_PROPERTY:
                    getView().getToolbarTitle().setText(R.string.title_add_appliance);
                    break;
            }

            if (getActivity() instanceof OwnerDashboardActivity) {
                //hide Toolbar
                getView().getToolbar().setVisibility(View.GONE);
            } else if (getActivity() instanceof HolderActivity) {
                //show Toolbar
                getActivity().setSupportActionBar(getView().getToolbar());
                if (getActivity().getSupportActionBar() != null)
                    getActivity().getSupportActionBar().setTitle("");
                getView().getToolbar().setVisibility(View.VISIBLE);
                getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
                getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        if (bgApp)
                            ((HolderActivity) getActivity()).moveToParentActivity();
                        else
                            ((HolderActivity) getActivity()).oneStepBack();
                    }
                });
            }

            propertyAdapter = new PropertySpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, null);
            propertyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getPropertySpinner().setAdapter(propertyAdapter);

            brandAdapter = new ApplianceTypeAndBrandAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, null);
            brandAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerBrandNameView().setAdapter(brandAdapter);

            applianceTypeAdapter = new ApplianceTypeAndBrandAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, null);
            applianceTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerApplianceTypeView().setAdapter(applianceTypeAdapter);


            //get appliance type by hitting resource api
            if (uploadFileList == null) uploadFileList = new ArrayList<>();
            if (uploadImageList == null) uploadImageList = new ArrayList<>();
            //Initialize RecyclerView for images
            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getApplianceImageRecycler().setLayoutManager(lm);

            ThumbnailBean obj = new ThumbnailBean();
            obj.setAddView(true);
            obj.setFile(false);
            obj.setImagePath("");
            uploadImageList.add(obj);

            thumbImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, true);
            getView().getApplianceImageRecycler().setAdapter(thumbImageAdapter);

            //Initialize RecyclerView for docs
            LinearLayoutManager lm1 = new LinearLayoutManager(getContext());
            lm1.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getApplianceDocsRecycler().setLayoutManager(lm1);

            ThumbnailBean obj1 = new ThumbnailBean();
            obj1.setAddView(true);
            obj1.setFile(false);
            obj1.setImagePath("");
            uploadFileList.add(obj);

            thumbFileAdapter = new ThumbImageAdapter(getActivity(), uploadFileList, this, true, true);
            getView().getApplianceDocsRecycler().setAdapter(thumbFileAdapter);
        }
    }

    private void getPropertiesAndApplianceType() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                getActivity().showProgressDialog();
                getInterActor().getProperty(param, ApiName.GET_PROPERTY);

                param.put(Constants.TYPE, Constants.RESOURCE_APPLIANCE);
                getInterActor().getResources(param, GET_RESOURCES_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    private void getBrands() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                getActivity().showProgressDialog();
                param.put(Constants.TYPE, Constants.RESOURCE_BRAND);
                getInterActor().getResources(param, GET_RESOURCES_BRANDS);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    private void getApplianceDetail() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.APPLIANCE_ID, applianceId);
                getInterActor().getApplianceDetail(param, ApiName.GET_APPLIANCE_DETAIL);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    public void saveBundleInfo(Bundle bundle) {
        try {
            if (getActivity() != null && isViewAttached() && bundle != null) {
                currentScreen = bundle.getString(Constants.DESTINATION, "");
                bgApp = bundle.getBoolean(Constants.BG_APP);
                propertyId = bundle.getString(Constants.PROPERTY_ID, "");
                applianceId = bundle.getString(Constants.APPLIANCE_ID, "");
                switch (currentScreen) {
                    case CLICK_ADD_APPLIANCE:
                        getView().getTILApplianceView().setVisibility(View.VISIBLE);
                        getView().getTILModelNameView().setVisibility(View.VISIBLE);
                        getView().getTILSerialNumberView().setVisibility(View.VISIBLE);
                        getView().getToolbarTitle().setText(R.string.title_add_appliance);
                        showHideDetailView(false);
                        getBrands();
                        //get Property listing for spinner
                        getPropertiesAndApplianceType();
                        break;

                    case CLICK_EDIT_APPLIANCE:
                        getView().getToolbarTitle().setText(R.string.title_edit_appliance);
                        getApplianceDetail();
                        showHideDetailView(false);
                        break;

                    case CLICK_VIEW_APPLIANCE:
                        getView().getToolbarTitle().setText(R.string.title_detail_appliance);
                        getApplianceDetail();
                        showHideDetailView(true);
                        enableDisableUI(false);
                        break;
                    case ADD_EDIT_PROPERTY:
                        getView().getTILApplianceView().setVisibility(View.VISIBLE);
                        getView().getTILModelNameView().setVisibility(View.VISIBLE);
                        getView().getTILSerialNumberView().setVisibility(View.VISIBLE);
                        getView().getToolbarTitle().setText(R.string.title_add_appliance);
                        showHideDetailView(false);
                        getBrands();
                        //get Property listing for spinner
                        getPropertiesAndApplianceType();
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showHideDetailView(boolean isDetailView) {
        if (isDetailView) {
            //show detail view
            getView().getEditImageContainer().setVisibility(View.GONE);
            getView().getDetailTopContainer().setVisibility(View.VISIBLE);
            getView().getDetailImageContainer().setVisibility(View.VISIBLE);
        } else {
            //show edit view
            //hide detail views and show edit image contaienr
            getView().getEditImageContainer().setVisibility(View.VISIBLE);
            getView().getDetailTopContainer().setVisibility(View.GONE);
            getView().getDetailImageContainer().setVisibility(View.GONE);
        }
    }

    public boolean validateApplianceInfo() {
        if (getActivity() != null && isViewAttached()) {

            mApplianceName = getView().getApplianceName().getText().toString().trim();
            mPurchaseDate = getView().getPurchaseDate().getText().toString().trim();
            mWarrantyDate = getView().getWarrantyExpDate().getText().toString().trim();
            mExtendedWarrantyDate = getView().getExtendedWarrantyExpDate().getText().toString().trim();
            modelName = getView().getModelName().getText().toString().trim();
            serialNumber = getView().getSerialNumber().getText().toString().trim();
            String response = Validations.getInstance().addEditAppliance(propertyObj, mApplianceName,
                    mApplianceBrandId, mApplianceType, mPurchaseDate, mWarrantyDate, mExtendedWarrantyDate, uploadImageList, uploadFileList,
                    modelName, serialNumber);

            if (response.isEmpty()) {
                addEditAppliance();
            } else {
                getActivity().showSnakBar(getView().getRootView(), response);
            }
        }
        return false;
    }


    private void addEditAppliance() {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    getActivity().hideKeyboard();
                    getActivity().showProgressDialog();
                    Map<String, String> params = new HashMap<>();
                    if (currentScreen.equals(Constants.CLICK_VIEW_APPLIANCE) ||
                            currentScreen.equals(Constants.CLICK_EDIT_APPLIANCE))
                        params.put(Constants.APPLIANCE_ID, applianceId);
                    params.put(Constants.PROPERTY_ID, propertyId);
                    params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    params.put(Constants.APPLIANCE_NAME, Utils.getInstance().capitalize(mApplianceName));
                    params.put(Constants.APPLIANCE_BRAND, mApplianceBrandId);
                    params.put(Constants.APPLIANCE_TYPE, mApplianceType);
                    params.put(Constants.PURCHASE_DATE, mPurchaseDate);
                    params.put(Constants.EXTENDED_WARRANTY_DATE, mExtendedWarrantyDate);
                    params.put(Constants.WARRANTY_EXPIRATION_DATE, mWarrantyDate);
                    params.put(Constants.MODEL_NAME, modelName);
                    params.put(Constants.SERIAL_NUMBER, serialNumber);


                    String imageIds = "";
                    for (int i = 0; i < deletedImageIds.size(); i++) {
                        if (!deletedImageIds.get(i).trim().isEmpty())
                            if (i == 0) {
                                imageIds = deletedImageIds.get(i);
                            } else {
                                imageIds = imageIds + "," + deletedImageIds.get(i);
                            }
                    }

                    String docIds = "";
                    for (int i = 0; i < deletedFileIds.size(); i++) {
                        if (!deletedFileIds.get(i).trim().isEmpty())
                            if (i == 0) {
                                docIds = deletedFileIds.get(i);
                            } else {
                                docIds = docIds + "," + deletedFileIds.get(i);
                            }
                    }

                    params.put(Constants.IS_DELETED, imageIds);
                    params.put(Constants.IS_DOC_DELETED, docIds);

                    if (!tempUploadImageList.isEmpty()) {
                        tempUploadImageList.clear();
                    }
                    if (!tempUploadFileList.isEmpty()) {
                        tempUploadFileList.clear();
                    }

                    if (currentScreen.equals(Constants.CLICK_EDIT_APPLIANCE) ||
                            currentScreen.equals(Constants.CLICK_VIEW_APPLIANCE)) {
                        for (int i = 0; i < uploadImageList.size(); i++) {
                            if (uploadImageList.get(i).getCheck().equals(ScreenNavigation.ADD_ITEM)) {
                                ThumbnailBean beanObj = new ThumbnailBean();
                                beanObj.setAddView(uploadImageList.get(i).isAddView());
                                beanObj.setCheck(uploadImageList.get(i).getCheck());
                                beanObj.setExtension(uploadImageList.get(i).getExtension());
                                beanObj.setFile(uploadImageList.get(i).isFile());
                                beanObj.setName(uploadImageList.get(i).getName());
                                beanObj.setImagePath(uploadImageList.get(i).getImagePath());
                                tempUploadImageList.add(beanObj);
                            }
                        }

                        for (int i = 0; i < uploadFileList.size(); i++) {
                            if (uploadFileList.get(i).getCheck().equals(ScreenNavigation.ADD_ITEM)) {
                                ThumbnailBean beanObj = new ThumbnailBean();
                                beanObj.setAddView(uploadFileList.get(i).isAddView());
                                beanObj.setCheck(uploadFileList.get(i).getCheck());
                                beanObj.setExtension(uploadFileList.get(i).getExtension());
                                beanObj.setFile(uploadFileList.get(i).isFile());
                                beanObj.setName(uploadFileList.get(i).getName());
                                beanObj.setImagePath(uploadFileList.get(i).getImagePath());
                                tempUploadFileList.add(beanObj);
                            }
                        }
                    } else {
                        tempUploadImageList.addAll(uploadImageList);
                        tempUploadFileList.addAll(uploadFileList);
                        tempUploadImageList.remove(tempUploadImageList.size() - 1);
                        tempUploadFileList.remove(tempUploadFileList.size() - 1);
                    }
                    getInterActor().addEditAppliance(params, ApiName.ADD_EDIT_APPLIANCE);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
            }
        } catch (Exception e)

        {
            e.printStackTrace();
        }

    }

    private void uploadApplianceImages(String applianceId) {
        try {
            uploadFileList.remove(uploadFileList.size() - 1);
            getActivity().startService(createCallingIntent(applianceId));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Intent createCallingIntent(String applianceId) {
        Intent i = new Intent(getActivity(), ImagesService.class);
        ImagesResultReceiver receiver = new ImagesResultReceiver(new Handler());
        receiver.setReceiver(this);
        i.putExtra(Constants.RECEIVER, receiver);
        i.putExtra(Constants.SCREEN, Constants.CLICK_APPLIANCE);
        i.putParcelableArrayListExtra(Constants.IMAGE_LIST, (ArrayList<? extends Parcelable>) tempUploadImageList);
        i.putParcelableArrayListExtra(Constants.FILE_LIST, (ArrayList<? extends Parcelable>) tempUploadFileList);
        i.putExtra(Constants.APPLIANCE_ID, applianceId);
        return i;
    }

    public void enableDisableUI(boolean isEdit) {
        //apply animation
        if (Utils.getInstance().isEqualLollipop()) {
            TransitionManager.beginDelayedTransition((ViewGroup) getView().getRootView(), new AutoTransition());
        }

        if (isEdit) {
            //make items editable
            getView().getPropertySpinner().setEnabled(true);
            getView().getApplianceName().setEnabled(true);
            getView().getModelName().setEnabled(true);
            getView().getSerialNumber().setEnabled(true);

            getView().getModelName().setFocusable(true);
            getView().getModelName().setFocusableInTouchMode(true);
            getView().getModelName().requestFocus();

            getView().getSerialNumber().setFocusable(true);
            getView().getSerialNumber().setFocusableInTouchMode(true);
            getView().getSerialNumber().requestFocus();

            getView().getExtendedWarrantyExpDate().setFocusable(true);
            getView().getExtendedWarrantyExpDate().setFocusableInTouchMode(true);
            getView().getExtendedWarrantyExpDate().requestFocus();

            getView().getApplianceName().setFocusable(true);
            getView().getApplianceName().setFocusableInTouchMode(true);
            getView().getApplianceName().requestFocus();

            getView().getHeaderPropertyImageView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            getView().getHeaderPropertyNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            getView().getHeaderApplianceImageView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            getView().getHeaderApplianceDocView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            getView().getHeaderBrandNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            getView().getHeaderBrandNameView().setText(R.string.select_brand_name);

            getView().getHeaderApplianceTypeView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            getView().getHeaderApplianceTypeView().setText(R.string.header_appliance_type);

            getView().getApplianceTypeLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().getPropertyNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().getBrandNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));

            getView().getTILApplianceView().setVisibility(View.VISIBLE);
            getView().getTILModelNameView().setVisibility(View.VISIBLE);
            getView().getTILSerialNumberView().setVisibility(View.VISIBLE);
            getView().getTILModelNameDetailView().setVisibility(View.GONE);
            getView().getTILSerialNumberDetailView().setVisibility(View.GONE);
            getView().getTILApplianceDetailView().setVisibility(View.GONE);

            getView().getSpinnerBrandNameView().setEnabled(true);
            getView().getSpinnerApplianceTypeView().setEnabled(true);
            getView().getPurchaseDate().setEnabled(true);
            getView().getWarrantyExpDate().setEnabled(true);
            getView().getExtendedWarrantyExpDate().setEnabled(true);

            //enable recyclerView delete button and add add image option
            if (uploadImageList == null) uploadImageList = new ArrayList<>();
            if (uploadImageList.size() == 0) {
                ThumbnailBean obj = new ThumbnailBean();
                obj.setAddView(true);
                obj.setFile(false);
                obj.setImagePath("");
                uploadImageList.add(obj);
                thumbImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, true);
                getView().getApplianceImageRecycler().setAdapter(thumbImageAdapter);
            } else if (uploadImageList.size() != 0 && !uploadImageList.get(uploadImageList.size() - 1).isAddView()) {
                ThumbnailBean obj = new ThumbnailBean();
                obj.setAddView(true);
                obj.setFile(false);
                obj.setImagePath("");
                uploadImageList.add(obj);
                thumbImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, true);
                getView().getApplianceImageRecycler().setAdapter(thumbImageAdapter);
            }


            if (uploadFileList == null) uploadFileList = new ArrayList<>();

            if (uploadFileList.size() == 0) {
                ThumbnailBean objFile = new ThumbnailBean();
                objFile.setAddView(true);
                objFile.setFile(false);
                objFile.setImagePath("");
                uploadFileList.add(objFile);

                thumbFileAdapter = new ThumbImageAdapter(getActivity(), uploadFileList, this, true, true);
                getView().getApplianceDocsRecycler().setAdapter(thumbFileAdapter);
            } else if (uploadFileList.size() != 0 && !uploadFileList.get(uploadFileList.size() - 1).isAddView()) {
                ThumbnailBean objFile = new ThumbnailBean();
                objFile.setAddView(true);
                objFile.setFile(false);
                objFile.setImagePath("");
                uploadFileList.add(objFile);
                thumbFileAdapter = new ThumbImageAdapter(getActivity(), uploadFileList, this, true, true);
                getView().getApplianceDocsRecycler().setAdapter(thumbFileAdapter);
            }


        } else {
            //make items non-editable
            getView().getPropertySpinner().setEnabled(false);
            getView().getApplianceName().setEnabled(false);
            getView().getModelName().setEnabled(false);
            getView().getSerialNumber().setEnabled(false);
            getView().getApplianceNameDetail().setEnabled(false);
            getView().getModelNameDetail().setEnabled(false);
            getView().getSerialNumberDetail().setEnabled(false);

            getView().getApplianceName().setFocusable(false);
            getView().getModelName().setFocusable(false);
            getView().getSerialNumber().setFocusable(false);

            getView().getSpinnerBrandNameView().setFocusable(false);
            getView().getExtendedWarrantyExpDate().setFocusable(false);
            getView().getApplianceNameDetail().setFocusable(false);
            getView().getWarrantyExpDate().setFocusable(false);

            getView().getTILApplianceView().setVisibility(View.GONE);
            getView().getTILModelNameView().setVisibility(View.GONE);
            getView().getTILSerialNumberView().setVisibility(View.GONE);
            getView().getTILModelNameDetailView().setVisibility(View.VISIBLE);
            getView().getTILSerialNumberDetailView().setVisibility(View.VISIBLE);
            getView().getTILApplianceDetailView().setVisibility(View.VISIBLE);

            getView().getHeaderPropertyImageView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().getHeaderPropertyNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().getHeaderApplianceImageView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().getHeaderApplianceDocView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().getHeaderBrandNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().getHeaderBrandNameView().setText(R.string.brand_name_detail);

            getView().getHeaderApplianceTypeView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
            getView().getHeaderApplianceTypeView().setText(R.string.header_appliance_type_detail);

            getView().getApplianceTypeLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
            getView().getPropertyNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
            getView().getBrandNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));

            getView().getSpinnerApplianceTypeView().setEnabled(false);
            getView().getPurchaseDate().setEnabled(false);
            getView().getWarrantyExpDate().setEnabled(false);
            getView().getExtendedWarrantyExpDate().setEnabled(false);
            getView().getSpinnerBrandNameView().setEnabled(false);

            //disable recyclerView delete button
            thumbImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, false);
            getView().getApplianceImageRecycler().setAdapter(thumbImageAdapter);

            //disable recyclerView delete button
            thumbFileAdapter = new ThumbImageAdapter(getActivity(), uploadFileList, this, true, true);
            getView().getApplianceDocsRecycler().setAdapter(thumbFileAdapter);


            if (imgAdapter != null && !uploadImageList.isEmpty()) {
                imgAdapter = new ViewPagerSubCatAdapter(getActivity(),
                        uploadImageList, this);
                getView().getImageViewPager().setAdapter(imgAdapter);
                imgAdapter.notifyDataSetChanged();
                getView().getImgIndicator().setViewPager(getView().getImageViewPager());
            }

            if (docsAdapter != null && !uploadFileList.isEmpty()) {
                docsAdapter = new ViewPagerSubCatAdapter(getActivity(),
                        uploadFileList, this);
                // Binds the Adapter to the ViewPager
                getView().getDocsViewPager().setAdapter(docsAdapter);
                docsAdapter.notifyDataSetChanged();

                getView().getDocsIndicator().setViewPager(getView().getDocsViewPager());
            }
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_PROPERTY:
                        if (response.isSuccessful()) {
                            propertyObj = (GetPropertyResponse) response.body();
                            switch (propertyObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(propertyObj);
                                    break;
                                case Constants.STATUS_201:
//                                    getActivity().showSnackBar(getView().getRootView(), propertyObj.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(propertyObj.getMessage());
                                    break;
                            }
                        }
                        break;
                    case GET_RESOURCES_API:
                        if (response.isSuccessful()) {
                            resourcesObj = (ResourceBean) response.body();
                            switch (resourcesObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(resourcesObj, GET_RESOURCES_API);
                                    break;
                                case Constants.STATUS_201:
//                                    getActivity().showSnackBar(getView().getRootView(), resourcesObj.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(resourcesObj.getMessage());
                                    break;
                            }
                        }
                        break;

                    case GET_RESOURCES_BRANDS:
                        if (response.isSuccessful()) {
                            resourceBrandObj = (ResourceBean) response.body();
                            switch (resourceBrandObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(resourceBrandObj, GET_RESOURCES_BRANDS);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(resourceBrandObj.getMessage());
                                    break;
                            }
                        }
                        break;
                    case ADD_EDIT_APPLIANCE:
                        if (response.isSuccessful()) {
                            AddEditModel addEditModel = (AddEditModel) response.body();
                            switch (addEditModel.getStatus()) {
                                case Constants.STATUS_200:
                                    enableDisableUI(false);
                                    showHideDetailView(true);
                                    uploadApplianceImages(String.valueOf(addEditModel.getData().getId()));
                                    deletedFileIds = new ArrayList<>();
                                    deletedImageIds = new ArrayList<>();
                                    Intent listIntent;
                                    if (currentScreen.equals(Constants.ADD_EDIT_PROPERTY))
                                        listIntent = new Intent(ADD_EDIT_PROPERTY);
                                    else listIntent = new Intent(Constants.GET_APPLIANCE_LIST);
                                    getActivity().sendBroadcast(listIntent);
                                    ((HolderActivity) getActivity()).oneStepBack();
                                    break;
                                case Constants.STATUS_201:

                                    break;
                                case Constants.STATUS_203:
                                    getActivity().showSnackBar(getView().getRootView(), addEditModel.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(addEditModel.getMessage());
                                    break;
                            }
                        }
                        break;
                    case GET_APPLIANCE_DETAIL:
                        if (response.isSuccessful()) {
                            ApplianceDetailModel detailModel = (ApplianceDetailModel) response.body();
                            switch (detailModel.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(detailModel);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(propertyObj.getMessage());
                                    break;
                            }
                            getBrands();
                            //get Property listing for spinner
                            getPropertiesAndApplianceType();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(ResourceBean resourceObj, ApiName check) {
        switch (check) {
            case GET_RESOURCES_API:
                if (applianceTypeAdapter != null) {
                    listApplianceType = resourceObj.getData().get(0).getInfo();
                    applianceTypeAdapter.updateListApplianceType(getActivity(), resourceObj.getData().get(0).getInfo());
                    for (int i = 0; i < resourceObj.getData().get(0).getInfo().size(); i++) {
                        if (mApplianceType.equals(resourceObj.getData().get(0).getInfo().get(i).getValue())) {
                            getView().getSpinnerApplianceTypeView().setSelection(i + 1);
                            return;
                        }
                    }
                }
                break;
            case GET_RESOURCES_BRANDS:
                if (brandAdapter != null) {
                    listBrands = resourceObj.getData().get(0).getInfo();
                    brandAdapter.updateListWithOption(getActivity(), resourceObj.getData().get(0).getInfo());
                    for (int i = 0; i < resourceObj.getData().get(0).getInfo().size(); i++) {
                        if (mApplianceBrandId.equals(String.valueOf(resourceObj.getData().get(0).getInfo().get(i).getID()))) {
                            getView().getSpinnerBrandNameView().setSelection(i + 1);
                            return;
                        }
                    }
                }
                break;
        }


    }

    private void onSuccess(GetPropertyResponse propertyObj) {
        if (propertyAdapter != null) {
            propertyAdapter.updateList(getActivity(), propertyObj.getData());
            int position = 0;
            for (int i = 0; i < propertyObj.getData().size(); i++) {
                if (propertyId.equals(String.valueOf(propertyObj.getData().get(i).getValue())))
                    position = i;
            }
            getView().getPropertySpinner().setSelection(position);
        }
    }


    private void onSuccess(ApplianceDetailModel detailModel) {
        try {
            if (getActivity() != null && isViewAttached()) {
                applianceObj = detailModel;
                getView().getApplianceName().setText(detailModel.getData().getApplianceName());
                getView().getApplianceNameDetail().setText(detailModel.getData().getApplianceName());

                getView().getModelName().setText(detailModel.getData().getApplianceModel());
                getView().getModelNameDetail().setText(detailModel.getData().getApplianceModel());

                getView().getSerialNumber().setText(detailModel.getData().getApplianceSerialNo());
                getView().getSerialNumberDetail().setText(detailModel.getData().getApplianceSerialNo());

                mApplianceBrandId = detailModel.getData().getApplianceBrand();
                getView().getExtendedWarrantyExpDate().setText(detailModel.getData().getExtendedWarrantyDate());
                if (detailModel.getData().getPurchaseDate().isEmpty())
                    getView().getPurchaseDate().setText("");
                else
                    getView().getPurchaseDate().setText(Utils.getInstance().getYMDFormattedTime(detailModel.getData().getPurchaseDate()));

                if (detailModel.getData().getWarrantyExpirationDate().isEmpty())
                    getView().getWarrantyExpDate().setText("");
                else
                    getView().getWarrantyExpDate().setText(Utils.getInstance().getYMDFormattedTime(detailModel.getData().getWarrantyExpirationDate()));

                mApplianceType = detailModel.getData().getApplianceType();

                //set property image
                if (detailModel.getData().getProperty() != null && detailModel.getData().getProperty().getPropertyAvatar() != null &&
                        !TextUtils.isEmpty(detailModel.getData().getProperty().getPropertyAvatar())) {
                    Picasso.with(getActivity()).load(detailModel.getData().getProperty().getPropertyAvatar())
                            .resize(500, 500).centerCrop().into(getView().getPropertyImage(), new Callback() {
                        @Override
                        public void onSuccess() {
                            getView().getImageProgress().setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            getView().getPropertyImage().setImageResource(R.drawable.default_property);
                            getView().getImageProgress().setVisibility(View.GONE);
                        }
                    });
                } else {
                    getView().getPropertyImage().setImageResource(R.drawable.default_property);
                    getView().getImageProgress().setVisibility(View.GONE);
                }

                //set view pager for appliance images and appliance documents

                if (!detailModel.getData().getPropertyApplianceImages().isEmpty()) {
                    if (uploadImageList == null) uploadImageList = new ArrayList<>();
                    if (uploadImageList.size() > 0) {
                        uploadImageList.remove(uploadImageList.size() - 1);
                        thumbImageAdapter.notifyItemRemoved(uploadImageList.size());
                    }

                    ThumbnailBean obj = null;
                    for (int i = 0; i < detailModel.getData().getPropertyApplianceImages().size(); i++) {
                        obj = new ThumbnailBean();
                        obj.setExtension("jpg");
                        obj.setImagePath(detailModel.getData().getPropertyApplianceImages().get(i).getApplianceAvatar());
                        obj.setName("server side images");
                        obj.setFile(false);
                        obj.setAddView(false);
                        obj.setCheck(ScreenNavigation.OLD_ITEM);
                        obj.setImageID(String.valueOf(detailModel.getData().getPropertyApplianceImages().get(i).getApplianceImageId()));
                        uploadImageList.add(obj);
                    }

                    imgAdapter = new ViewPagerSubCatAdapter(getActivity(),
                            uploadImageList, this);

                    // Binds the Adapter to the ViewPager
                    getView().getImageViewPager().setAdapter(imgAdapter);

                    imgAdapter.notifyDataSetChanged();

                    getView().getImgIndicator().setViewPager(getView().getImageViewPager());

                }


                if (detailModel.getData().getPropertyApplianceDoc() != null) {
                    if (uploadFileList == null) uploadFileList = new ArrayList<>();

                    if (uploadFileList.size() > 0) {
                        uploadFileList.remove(uploadFileList.size() - 1);
                        thumbFileAdapter.notifyItemRemoved(uploadFileList.size());
                    }

                    ThumbnailBean obj = null;
                    for (int i = 0; i < detailModel.getData().getPropertyApplianceDoc().size(); i++) {
                        obj = new ThumbnailBean();
                        obj.setExtension("jpg");
                        obj.setImagePath(detailModel.getData().getPropertyApplianceDoc().get(i).getApplianceDocAvatar());
                        obj.setName("server side images");
                        obj.setFile(false);
                        obj.setAddView(false);
                        obj.setCheck(ScreenNavigation.OLD_ITEM);
                        obj.setImageID(String.valueOf(detailModel.getData().getPropertyApplianceDoc().get(i).getApplianceDocId()));
                        uploadFileList.add(obj);
                    }

                    docsAdapter = new ViewPagerSubCatAdapter(getActivity(),
                            uploadFileList, this);
                    // Binds the Adapter to the ViewPagerBaseAc
                    getView().getDocsViewPager().setAdapter(docsAdapter);

                    docsAdapter.notifyDataSetChanged();

                    getView().getDocsIndicator().setViewPager(getView().getDocsViewPager());
                }

                if (currentScreen.equals(Constants.CLICK_EDIT_APPLIANCE)) {
                    enableDisableUI(true);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.et_purchase_date:
                    showPurchaseDateDialog();
                    break;
                case R.id.et_warranty_expire_date:
                    showWarrantyDateDialog();
                    break;
                case R.id.avatar:
                    if (applianceObj != null && !applianceObj.getData().getProperty().getPropertyAvatar().isEmpty()) {
                        Intent profilePicIntent = new Intent(getActivity(), HolderActivity.class);
                        profilePicIntent.putExtra(Constants.DESTINATION, BROWSE_PIC);
                        profilePicIntent.putExtra(Constants.PIC_TITLE, Constants.TITLE_PROPERTY_IMAGE);
                        profilePicIntent.putExtra(BROWSE_PIC, applianceObj.getData().getProperty().getPropertyAvatar());
                        if (Utils.getInstance().isEqualLollipop()) {
                            Pair<View, String> p1 = Pair.create((View) getView().getPropertyImage(), Constants.TRANS_PROPERTY_IMAGE);
                            ActivityOptions options =
                                    ActivityOptions.makeSceneTransitionAnimation(getActivity(), p1);
                            getActivity().startActivity(profilePicIntent, options.toBundle());
                        } else {
                            getActivity().startActivity(profilePicIntent);
                        }
                    }
                    break;
            }
        }
    }

    private void showPurchaseDateDialog() {
        if (purchaseDate == 0) {
            Calendar c = Calendar.getInstance();
            purchaseYear = c.get(Calendar.YEAR);
            purchaseMonth = c.get(Calendar.MONTH);
            purchaseDate = c.get(Calendar.DAY_OF_MONTH);
        }

        DatePickerDialog pickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, final int year, int monthOfYear, int dayOfMonth) {
                if (view.isShown()) {
                    Calendar userAge = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                    Calendar minAdultAge = new GregorianCalendar();
                    minAdultAge.add(Calendar.YEAR, -18);
                    SimpleDateFormat month_date = new SimpleDateFormat("MM");
//                  SimpleDateFormat year_date = new SimpleDateFormat("yy");
                    String month = month_date.format(userAge.getTime());
//                  String yea = year_date.format(userAge.getTime());
                    DecimalFormat mFormat = new DecimalFormat("00");
                    purchaseDate = dayOfMonth;
                    purchaseMonth = monthOfYear;
                    purchaseYear = year;

                    //check validation if extend warranty is not less than purchase date
                    getView().getPurchaseDate().setText(new StringBuilder().append(year).append("-").append(month).append("-").append(mFormat.format(Double.valueOf(dayOfMonth))));
                }

            }
        }, purchaseYear, purchaseMonth, purchaseDate);
        pickerDialog.getDatePicker().setMaxDate(GregorianCalendar.getInstance().getTimeInMillis());
        if (!Utils.getInstance().isEqualLollipop() && pickerDialog.getWindow() != null) {
            pickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        pickerDialog.show();
    }

    private void showWarrantyDateDialog() {
        if (warrantyDate == 0) {
            Calendar c = Calendar.getInstance();
            warrantyYear = c.get(Calendar.YEAR);
            warrantyMonth = c.get(Calendar.MONTH);
            warrantyDate = c.get(Calendar.DAY_OF_MONTH);
        }
        DatePickerDialog pickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, final int year, int monthOfYear, int dayOfMonth) {
                if (view.isShown()) {
                    Calendar userAge = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                    Calendar minAdultAge = new GregorianCalendar();
                    minAdultAge.add(Calendar.YEAR, -18);
                    SimpleDateFormat month_date = new SimpleDateFormat("MM");
//                  SimpleDateFormat year_date = new SimpleDateFormat("yy");
                    String month = month_date.format(userAge.getTime());
//                  String yea = year_date.format(userAge.getTime());
                    DecimalFormat mFormat = new DecimalFormat("00");
                    warrantyDate = dayOfMonth;
                    warrantyMonth = monthOfYear;
                    warrantyYear = year;

                    getView().getWarrantyExpDate().setText(new StringBuilder().append(year).append("-").append(month).append("-").append(mFormat.format(Double.valueOf(dayOfMonth))));
                }

            }
        }, warrantyYear, warrantyMonth, warrantyDate);
//        pickerDialog.getDatePicker().setMaxDate(GregorianCalendar.getInstance().getTimeInMillis());
        if (!Utils.getInstance().isEqualLollipop() && pickerDialog.getWindow() != null) {
            pickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        pickerDialog.show();
    }


    @Override
    public void onFilesChosen(List<ChosenFile> list) {
        ChosenFile file = list.get(0);
        String extension = "";
        int i = list.get(0).getDisplayName().lastIndexOf('.');
        if (i > 0) {
            extension = list.get(0).getDisplayName().substring(i + 1);
        }
        if (extension.equals("pdf") || extension.equals("doc") || extension.equals("docx")) {
            long fileSize = file.getSize() / 1024; // convert bytes into Kb
            if (fileSize >= 5120) {
                Utils.getInstance().showToast("File size must be less than 5 MB");
                return;
            }
            manageFileList(file, null, extension);
        } else {
            getActivity().showSnakBar(getView().getRootView(), "you can choose only pdf or doc files.");
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        ChosenImage image = list.get(0);
        String extension = "";
        int i = list.get(0).getDisplayName().lastIndexOf('.');
        if (i > 0) {
            extension = list.get(0).getDisplayName().substring(i + 1);
        }

        if (!extension.equals("png") && !extension.equals("jpg") && !extension.equals("jpeg")) {
            getActivity().showSnakBar(getView().getRootView(), "you can choose only images.");
            return;
        }
        if (!forDocumentSection) {
            //Add Images info to list
            if (uploadImageList == null) uploadImageList = new ArrayList<>();
            if (uploadImageList.size() > 0) {
                uploadImageList.remove(uploadImageList.size() - 1);
                thumbImageAdapter.notifyItemRemoved(uploadImageList.size());
            }
            ThumbnailBean obj = new ThumbnailBean();
            obj.setExtension(extension);
            obj.setImagePath(FileUtils.getPath(getActivity(), Uri.parse(image.getQueryUri())));
            obj.setName(image.getDisplayName());
            obj.setFile(true);
            obj.setCheck(ScreenNavigation.ADD_ITEM);
            obj.setImageID("");
            obj.setAddView(false);
            uploadImageList.add(obj);

            if (thumbImageAdapter != null) {
                ThumbnailBean obj1 = new ThumbnailBean();
                obj1.setAddView(true);
                obj1.setFile(false);
                obj1.setImagePath("");
                obj1.setFile(false);
                uploadImageList.add(obj1);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        thumbImageAdapter.notifyDataSetChanged();
                    }
                });
                getView().getApplianceImageRecycler().smoothScrollToPosition(uploadImageList.size() - 1);
            }
        } else {
            //image selected for document section.
            manageFileList(null, image, extension);
        }

    }

    @Override
    public void onError(String s) {

    }

    private void manageFileList(ChosenFile file, ChosenImage image, String extension) {
        //Add Images info to list
        if (uploadFileList == null) uploadFileList = new ArrayList<>();
        if (uploadFileList.size() > 0) {
            uploadFileList.remove(uploadFileList.size() - 1);
            thumbFileAdapter.notifyItemRemoved(uploadFileList.size());
        }
        ThumbnailBean obj = new ThumbnailBean();
        obj.setExtension(extension);
        if (file != null) {
            obj.setImagePath(file.getOriginalPath());
            obj.setName(file.getDisplayName());
        } else if (image != null) {
            obj.setImagePath(image.getThumbnailPath());
            obj.setName(image.getDisplayName());
        }
        obj.setCheck(ScreenNavigation.ADD_ITEM);
        obj.setImageID("");
        obj.setFile(true);
        obj.setAddView(false);
        uploadFileList.add(obj);

        if (thumbFileAdapter != null) {
            ThumbnailBean obj1 = new ThumbnailBean();
            obj1.setAddView(true);
            obj1.setFile(false);
            obj1.setImagePath("");
            obj1.setImageID("");
            uploadFileList.add(obj1);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    thumbFileAdapter.notifyDataSetChanged();
                }
            });
            getView().getApplianceDocsRecycler().smoothScrollToPosition(uploadFileList.size() - 1);
        }
    }


    public void onAddOptionClick() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose Option");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        //choose camera
                        try {
                            isCameraSelected = true;
                            isGallerySelected = false;
                            forDocumentSection = false;
                            checkExternalStoragePermission();
                        } catch (ActivityNotFoundException e) {
                            e.printStackTrace();
                            String errorMessage = "Your device doesn't support capturing images";
                            Utils.getInstance().showToast(errorMessage);
                        }
                        break;
                    case 1:
                        //choose gallery
                        try {
                            isCameraSelected = false;
                            isGallerySelected = true;
                            forDocumentSection = false;
                            checkExternalStoragePermission();
                        } catch (Exception e) {
                            String errorMessage = "There are no images!";
                            Utils.getInstance().showToast(errorMessage);
                        }
                        break;

                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        builder.show();
    }

    public void onAddFileClick() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Choose from file", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose option");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        //choose camera
                        try {
                            isCameraSelected = true;
                            isGallerySelected = false;
                            forDocumentSection = true;
                            checkExternalStoragePermission();
                        } catch (ActivityNotFoundException e) {
                            e.printStackTrace();
                            String errorMessage = "Your device doesn't support capturing images";
                            Utils.getInstance().showToast(errorMessage);
                        }
                        break;
                    case 1:
                        //choose gallery
                        try {
                            isCameraSelected = false;
                            isGallerySelected = true;
                            forDocumentSection = true;
                            checkExternalStoragePermission();
                        } catch (Exception e) {
                            String errorMessage = "There are no images!";
                            Utils.getInstance().showToast(errorMessage);
                        }
                        break;
                    case 2:
                        //chose file
                        try {
                            isCameraSelected = false;
                            isGallerySelected = false;
                            forDocumentSection = true;
                            checkExternalStoragePermission();
                        } catch (Exception e) {
                            String errorMessage = "There are no files!";
                            Utils.getInstance().showToast(errorMessage);
                        }
                        break;
                    case 3:
                        dialog.dismiss();
                        break;
                }
            }
        });
        builder.show();
    }

    private void checkExternalStoragePermission() {
        boolean isExternalStorage = PermissionsAndroid.getInstance().checkCameraPermission(getActivity());
        if (!isExternalStorage) {
            PermissionsAndroid.getInstance().requestForCameraPermission(((AddEditApplianceFragment) getView()));
        } else if (isCameraSelected) {
            takePicture();
        } else if (isGallerySelected) {
            pickImageSingle();
        } else if (forDocumentSection) {
            pickFilesSingle();
        }
    }

    private void takePicture() {
        cameraPicker = new CameraImagePicker(getActivity());
//        cameraPicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        cameraPicker.setImagePickerCallback(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        pickerPath = cameraPicker.pickImage();
    }

    private void pickImageSingle() {
        imagePicker = new ImagePicker(getActivity());
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();
    }

    /*
    Image handling for this class
     */
    private void pickFilesSingle() {
        filePicker = new FilePicker(getActivity());
        filePicker.setFilePickerCallback(this);
        filePicker.setFolderName(getActivity().getString(R.string.app_name));
        filePicker.pickFile();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(getActivity());
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(getActivity());
                    cameraPicker.setImagePickerCallback(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            } else if (requestCode == Picker.PICK_FILE) {
                filePicker.submit(data);
            }
        } else if (resultCode == RESULT_CANCELED) {
//            Utils.getInstance().showToast("Request Cancelled");
        }
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 102: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Show option to update profile
                    if (isCameraSelected) {
                        takePicture();
                    } else if (isGallerySelected) {
                        pickImageSingle();
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Utils.getInstance().showToast("Camera/Storage permission Denied");
                }
                return;
            }

        }
    }

    public void deleteImage(final int position) {
        Utils.getInstance().confirmDialog(getActivity(), "Are you sure, you want to delete this image?", "delete", new Utils.ConfirmDialogCallbackInterface() {
            @Override
            public void onYesClick(String tag) {
                deletedImageIds.add(uploadImageList.get(position).getImageID());
                uploadImageList.remove(position);

                getView().getApplianceImageRecycler().setAdapter(thumbImageAdapter);

                imgAdapter = new ViewPagerSubCatAdapter(getActivity(),
                        uploadImageList, AddEditAppliancePresenterImpl.this);

                // Binds the Adapter to the ViewPager
                getView().getImageViewPager().setAdapter(imgAdapter);
                imgAdapter.notifyDataSetChanged();
                getView().getImgIndicator().setViewPager(getView().getImageViewPager());


            }

            @Override
            public void onNoClick(String tag) {

            }
        });

    }

    public void deleteDocImage(final int position) {
        Utils.getInstance().confirmDialog(getActivity(), "Are you sure, you want to delete this file?", "delete", new Utils.ConfirmDialogCallbackInterface() {
            @Override
            public void onYesClick(String tag) {
                deletedFileIds.add(uploadFileList.get(position).getImageID());
                uploadFileList.remove(position);
                getView().getApplianceDocsRecycler().setAdapter(thumbFileAdapter);

                docsAdapter = new ViewPagerSubCatAdapter(getActivity(),
                        uploadFileList, AddEditAppliancePresenterImpl.this);
                getView().getDocsViewPager().setAdapter(docsAdapter);
                docsAdapter.notifyDataSetChanged();
                getView().getDocsIndicator().setViewPager(getView().getDocsViewPager());
            }

            @Override
            public void onNoClick(String tag) {

            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spinner_property_name:
                propertyId = String.valueOf(propertyObj.getData().get(position).getValue());
                break;
            case R.id.spinner_brand_name:
                if (position != 0)
                    mApplianceBrandId = String.valueOf(listBrands.get(position - 1).getID());
                else
                    mApplianceBrandId = "";
                break;
            case R.id.spinner_appliance_type:
                if (position != 0)
                    mApplianceType = listApplianceType.get(position - 1).getValue();
                else
                    mApplianceType = "";
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case 200:
                final String message = resultData.getString("message");
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        Utils.getInstance().showToast(message);
                    }
                }, 1000);
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (getActivity() != null && isViewAttached())
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());


        return false;
    }
}
