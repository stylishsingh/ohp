package com.ohp.ownerviews.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.ApplianceTypeAndBrandAdapter;
import com.ohp.commonviews.adapter.JobTypeExpertiseSpinnerAdapter;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.beans.ResourceBean;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.beans.AllContactListResponse;
import com.ohp.ownerviews.fragment.AddEditContact;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;
import com.ohp.utils.Validations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static com.ohp.enums.ApiName.GET_RESOURCES_API;

/**
 * Created by vishal.sharma on 8/31/2017.
 */

public class AddEditContactPresenterImpl extends FragmentPresenter<AddEditContact, APIHandler> implements APIResponseInterface.OnCallableResponse {
    int CategoryValue;
    private JobTypeExpertiseSpinnerAdapter jobTypeExpertiseAdapter;
    private List<ResourceBean.DataBean.InfoBean> contactCategoryList = new ArrayList<>();
    private ApplianceTypeAndBrandAdapter categoryAdapter;
    private String Name = "", Email = "", Designation = "", ContactNumber = "", Category = "", currentScreen, contactId = "";

    public AddEditContactPresenterImpl(AddEditContact addEditContact, Context context) {
        super(addEditContact, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");

            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    ((HolderActivity) getActivity()).oneStepBack();
                }
            });

            categoryAdapter = new ApplianceTypeAndBrandAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, null);
            categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getCategory().setAdapter(categoryAdapter);
        }

        getView().getCategory().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position == 0) {
//                    Category = "";
//                } else {
                Category = String.valueOf(contactCategoryList.get(position).getID());
//                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        getResource();
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case ADD_EDIT_CONTACT:
                        if (response.isSuccessful()) {
                            GenericBean genericBean = (GenericBean) response.body();
                            switch (genericBean.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(genericBean);
                                    break;
                                case Constants.STATUS_201:
                                    Utils.getInstance().showToast(genericBean.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(genericBean.getMessage());
                                    break;
                                case Constants.STATUS_203:
                                    Utils.getInstance().showToast(genericBean.getMessage());
                            }
                        }
                        break;
                    case GET_RESOURCES_API:
                        if (response.isSuccessful()) {
                            getActivity().hideProgressDialog();
                            ResourceBean resourcesObj = (ResourceBean) response.body();
                            switch (resourcesObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(resourcesObj);
                                    break;
                                case Constants.STATUS_201:
                                    if (categoryAdapter != null) {
                                        categoryAdapter.updateList(getActivity(), contactCategoryList);
                                    }
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(resourcesObj.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(ResourceBean resourceObj) {
        try {
            if (getActivity() != null && isViewAttached()) {

                contactCategoryList = new ArrayList<>();
                if (!contactCategoryList.isEmpty())
                    contactCategoryList.clear();

                contactCategoryList.addAll(resourceObj.getData().get(0).getInfo());
                categoryAdapter.updateListCategoryType(getActivity(), contactCategoryList);

                if (currentScreen.equals(Constants.CLICK_VIEW_CONTACT) || currentScreen.equals(Constants.CLICK_EDIT_CONTACT)) {
                    for (int i = 0; i < contactCategoryList.size(); i++) {
                        if (CategoryValue == contactCategoryList.get(i).getID()) {
                            getView().getCategory().setSelection(i);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(GenericBean genericBean) {
        if (getActivity() != null && isViewAttached()) {
            Intent listIntent = new Intent(Constants.CONTACT_LIST_UPDATE);
            getActivity().sendBroadcast(listIntent);
            ((HolderActivity) getActivity()).oneStepBack();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void AddEditContact() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {

                Name = getView().getName().getText().toString().trim();
                Email = getView().getEmail().getText().toString().trim();
                Designation = getView().getDesignnatiom().getText().toString().trim();
                ContactNumber = getView().getContactNumber().getText().toString();
                //  Category=contactCategoryList.size()==0 ? "" : String.valueOf(contactCategoryList.get(getView().getCategory().getSelectedItemPosition()-1).getID());

                String response = Validations.getInstance().validateContactFields(Name, Email, ContactNumber);
                if (!response.trim().isEmpty()) {
                    getActivity().showSnakBar(getView().getRootView(), response);
                } else {
                    Map<String, String> params = new HashMap<>();
                    params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    if (currentScreen.equals(Constants.CLICK_VIEW_CONTACT) || currentScreen.equals(Constants.CLICK_EDIT_CONTACT))
                        params.put(Constants.CONTACT_ID, contactId);

                    params.put(Constants.FULL_NAME, Name);
                    params.put(Constants.PHONE, ContactNumber);
                    params.put(Constants.EMAIL, Email);
                    params.put(Constants.DESIGNATION, Designation);
                    params.put(Constants.CATEGORY, Category);

                    getInterActor().AddEditContact(params, ApiName.ADD_EDIT_CONTACT);
                }

            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    public void getResource() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                getActivity().showProgressDialog();

                param.put(Constants.TYPE, "user_category");
                getInterActor().getResources(param, GET_RESOURCES_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    public void saveBundleInfo(Bundle arguments) {
        if (arguments != null) {
            currentScreen = arguments.getString(Constants.DESTINATION, "");
            AllContactListResponse.DataBean allContactListResponse = arguments.getParcelable(Constants.CONTACT_LIST_OBJECT);
            if (allContactListResponse != null) {
                contactId = String.valueOf(allContactListResponse.getContactId());
                getView().getName().setText(allContactListResponse.getFullName());
                getView().getEmail().setText(allContactListResponse.getEmail());
                getView().getDesignnatiom().setText(allContactListResponse.getDesignation());
                getView().getContactNumber().setText(allContactListResponse.getPhone() + "");
                CategoryValue = allContactListResponse.getCategory();


            }
        }
    }

    public void hideViewForViewContact() {
        getView().getName().setEnabled(false);
        getView().getName().setFocusable(false);
        getView().getNameTitle().setHint("Full Name");
        getView().getEmail().setEnabled(false);
        getView().getEmail().setFocusable(false);
        getView().getEmailTitle().setHint("Email");
        getView().getDesignnatiom().setEnabled(false);
        getView().getDesignnatiom().setFocusable(false);
        getView().getDesignationTitle().setHint("Designation");
        getView().getContactNumber().setEnabled(false);
        getView().getContactNumber().setFocusable(false);
        getView().getContactNumberTitle().setHint("Contact Number");
        getView().getCategory().setEnabled(false);
        getView().getCategoryTitle().setText("Category");
        getView().getCategoryTitle().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
    }

    public void hideViewForEditContactDetail() {

        getView().getEmail().setEnabled(true);
        getView().getEmail().setFocusable(true);
        getView().getEmail().setFocusableInTouchMode(true);
        getView().getEmail().requestFocus();
        getView().getEmailTitle().setHint("Enter Email");

        getView().getDesignnatiom().setEnabled(true);
        getView().getDesignnatiom().setFocusable(true);
        getView().getDesignnatiom().requestFocus();
        getView().getDesignnatiom().setFocusableInTouchMode(true);
        getView().getDesignationTitle().setHint("Enter Designation");

        getView().getContactNumber().setEnabled(true);
        getView().getContactNumber().setFocusable(true);
        getView().getContactNumber().requestFocus();
        getView().getContactNumber().setFocusableInTouchMode(true);
        getView().getContactNumberTitle().setHint("Enter Contact Number");

        getView().getName().setEnabled(true);
        getView().getName().setFocusable(true);
        getView().getName().setFocusableInTouchMode(true);
        getView().getName().requestFocus();
        getView().getNameTitle().setHint("Enter Full Name");

        getView().getCategory().setEnabled(true);
        getView().getCategory().requestFocus();
        getView().getCategoryTitle().setHint("Enter Category");
        getView().getCategoryTitle().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
    }
}
