package com.ohp.ownerviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.ApplianceSpinnerAdapter;
import com.ohp.commonviews.adapter.ProjectSpinnerAdapter;
import com.ohp.commonviews.adapter.PropertySpinnerAdapter;
import com.ohp.commonviews.adapter.ProviderSpinnerAdapter;
import com.ohp.commonviews.adapter.ReminderAdapter;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.beans.GetPropertyResponse;
import com.ohp.commonviews.beans.ResourceBean;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.beans.EventDetailModel;
import com.ohp.ownerviews.view.OwnerAddEditEventView;
import com.ohp.utils.CalendarPicker;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;
import com.ohp.utils.Validations;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static com.ohp.enums.ApiName.ADD_EDIT_EVENT;
import static com.ohp.enums.ApiName.GET_EVENT_REMINDER_API;
import static com.ohp.utils.Constants.ADD_EDIT_PROPERTY;
import static com.ohp.utils.Constants.CLICK_OWNER_ADD_EVENT;
import static com.ohp.utils.Constants.CLICK_OWNER_EDIT_EVENT;
import static com.ohp.utils.Constants.CLICK_OWNER_VIEW_EVENT;
import static com.ohp.utils.Constants.NAVIGATE_FROM;
import static com.ohp.utils.Constants.STATUS_200;
import static com.ohp.utils.Constants.STATUS_201;
import static com.ohp.utils.Constants.STATUS_202;
import static com.ohp.utils.Constants.STATUS_203;

/**
 * @author Amanpal Singh.
 */

public class OwnerAddEditEventFragmentPresenterImpl extends FragmentPresenter<OwnerAddEditEventView, APIHandler> implements APIResponseInterface.OnCallableResponse, AdapterView.OnItemSelectedListener, View.OnClickListener, View.OnTouchListener {

    private final BroadcastReceiver eventDetailReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                Intent listIntent = new Intent(Constants.GET_EVENTS);
                if (intent.getExtras().containsKey(Constants.NAVIGATE_FROM)
                        && Constants.NOTIFICATIONS.equalsIgnoreCase(intent.getExtras().getString(NAVIGATE_FROM))) {
                    listIntent.putExtra("year", intent.getExtras().getInt("year"));
                    listIntent.putExtra("month", intent.getExtras().getInt("month"));
                    listIntent.putExtra("day", intent.getExtras().getInt("day"));
                    if (eventID.equalsIgnoreCase(intent.getExtras().getString(Constants.EVENT_ID)))
                        getEventDetails(eventID);
                } else
                    getEventDetails(eventID);


                context.sendBroadcast(listIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };
    public boolean isOccurrence, isEventRepeat;
    private PropertySpinnerAdapter propertyAdapter;
    private boolean bgApp = false;
    private ApplianceSpinnerAdapter applianceAdapter;
    private ProjectSpinnerAdapter projectAdapter;
    private ProviderSpinnerAdapter providerAdapter;
    private ReminderAdapter reminderEventAdapter;
    private List<GetPropertyResponse.DataBean> listProperties = new ArrayList<>(), listAppliances = new ArrayList<>(),
            listProjects = new ArrayList<>(), listProviders = new ArrayList<>();
    private List<ResourceBean.DataBean.InfoBean> listEventReminder = new ArrayList<>();
    private String propertyId = "", applianceId = "", projectId = "", providerId = "", eventRepeatId = "", eventTitle = "", addNotes = "", eventDateTime = "", reminderDateTime = "", eventID = "", eventTimeTo = "";
    private String currentScreen = "";

    public OwnerAddEditEventFragmentPresenterImpl(OwnerAddEditEventView addEditEventView, Context context) {
        super(addEditEventView, context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null && isViewAttached()) {
            getActivity().unregisterReceiver(eventDetailReceiver);
        }
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(eventDetailReceiver, new IntentFilter(Constants.GET_EVENT_DETAILS));
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            getView().getToolbar().setVisibility(View.VISIBLE);
            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    if (bgApp)
                        ((HolderActivity) getActivity()).moveToParentActivity();
                    else
                        ((HolderActivity) getActivity()).oneStepBack();
                }
            });

            //initialize spinners
            propertyAdapter = new PropertySpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listProperties);
            propertyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerPropertyView().setAdapter(propertyAdapter);

            applianceAdapter = new ApplianceSpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listAppliances);
            applianceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerApplianceView().setAdapter(applianceAdapter);

            projectAdapter = new ProjectSpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listProjects);
            projectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerProjectView().setAdapter(projectAdapter);

            providerAdapter = new ProviderSpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listProviders);
            providerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerProviderView().setAdapter(providerAdapter);


            reminderEventAdapter = new ReminderAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listEventReminder);
            reminderEventAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerRepeatEventView().setAdapter(reminderEventAdapter);

            propertyAdapter = new PropertySpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listProperties);
            propertyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerPropertyView().setAdapter(propertyAdapter);

            getPropertiesList();

            //set scroll listener on address field
            getView().getEtAddNotesView().setOnTouchListener(new View.OnTouchListener() {  //Register a callback to be invoked when a touch event is sent to this view.
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    //Called when a touch event is dispatched to a view. This allows listeners to get a chance to respond before the target view.
                    getView().getNestedScrollView().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });

        }

    }

    public void saveBundleInfo(Bundle arguments) {
        if (getActivity() != null && isViewAttached()) {
            eventID = arguments.getString(Constants.EVENT_ID);
            bgApp = arguments.getBoolean(Constants.BG_APP);
            propertyId = arguments.getString(Constants.PROPERTY_ID, "");
            currentScreen = arguments.getString(Constants.DESTINATION, "");
            isOccurrence = arguments.getBoolean(Constants.OCCURRENCE, false);
            isEventRepeat = arguments.getBoolean(Constants.EVENT_REPEAT_EMPTY, false);
            switch (currentScreen) {
                case CLICK_OWNER_EDIT_EVENT:
                    getEventDetails(eventID);
                    getView().getToolbarTitleView().setText(R.string.title_edit_event);
                    enableDisableUI(true);
                    break;
                case CLICK_OWNER_ADD_EVENT:
                    getView().getToolbarTitleView().setText(R.string.title_add_event);
                    getView().getTILEventTitleView().setVisibility(View.VISIBLE);
                    getView().getTILEventTitleDetailView().setVisibility(View.GONE);
                    getView().getTILNotesView().setVisibility(View.VISIBLE);
                    getView().getTILNotesDetailView().setVisibility(View.GONE);
                    getEventReminder();
                    break;
                case ADD_EDIT_PROPERTY:
                    getView().getToolbarTitleView().setText(R.string.title_add_event);
                    getEventReminder();
                    getView().getTILEventTitleView().setVisibility(View.VISIBLE);
                    getView().getTILEventTitleDetailView().setVisibility(View.GONE);
                    getView().getTILNotesView().setVisibility(View.VISIBLE);
                    getView().getTILNotesDetailView().setVisibility(View.GONE);
                    break;
                case CLICK_OWNER_VIEW_EVENT:
                    getEventDetails(eventID);
                    getView().getToolbarTitleView().setText(R.string.title_event_details);
                    enableDisableUI(false);
                    break;
            }
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_PROPERTY:
                        if (response.isSuccessful()) {
                            GetPropertyResponse responseObj = (GetPropertyResponse) response.body();
                            switch (responseObj.getStatus()) {
                                case STATUS_200:
                                    onSuccess(responseObj, ApiName.GET_PROPERTY);
                                    break;
                                case STATUS_201:
                                    break;
                                case STATUS_202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());
                                    break;
                            }
                        }
                        break;

                    case GET_APPLIANCE:
                        if (response.isSuccessful()) {
                            GetPropertyResponse responseObj = (GetPropertyResponse) response.body();
                            switch (responseObj.getStatus()) {
                                case STATUS_200:
                                    onSuccess(responseObj, ApiName.GET_APPLIANCE);
                                    break;
                                case STATUS_201:
                                    break;
                                case STATUS_202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());
                                    break;
                            }
                        }
                        break;

                    case GET_PROJECT_LIST:
                        if (response.isSuccessful()) {
                            GetPropertyResponse responseObj = (GetPropertyResponse) response.body();
                            switch (responseObj.getStatus()) {
                                case STATUS_200:
                                    onSuccess(responseObj, ApiName.GET_PROJECT_LIST);
                                    break;
                                case STATUS_201:
                                    break;
                                case STATUS_202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());

                                    break;
                            }
                        }
                        break;

                    case GET_EVENT_REMINDER_API:
                        if (response.isSuccessful()) {
                            ResourceBean responseObj = (ResourceBean) response.body();
                            switch (responseObj.getStatus()) {
                                case STATUS_200:
                                    onSuccess(responseObj, GET_EVENT_REMINDER_API);
                                    break;
                                case STATUS_201:
                                    break;
                                case STATUS_202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());

                                    break;
                            }
                        }
                        break;

                    case GET_EVENT_DETAIL_API:
                        if (response.isSuccessful()) {
                            EventDetailModel responseObj = (EventDetailModel) response.body();
                            switch (responseObj.getStatus()) {
                                case STATUS_200:
                                    onSuccess(responseObj);
                                    break;
                                case STATUS_201:
                                    break;
                                case STATUS_202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());
                                    break;
                            }
                            getEventReminder();
                        }
                        break;
                    case GET_PROJECT_BIDS_PROVIDER:
                        if (response.isSuccessful()) {
                            GetPropertyResponse responseObj = (GetPropertyResponse) response.body();
                            switch (responseObj.getStatus()) {
                                case STATUS_200:
                                    onSuccess(responseObj, ApiName.GET_PROJECT_BIDS_PROVIDER);
                                    break;
                                case STATUS_201:
                                    break;
                                case STATUS_202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());
                                    break;
                            }
                        }
                        break;
                    case ADD_EDIT_EVENT:
                        if (response.isSuccessful()) {
                            GenericBean responseObj = (GenericBean) response.body();
                            switch (responseObj.getStatus()) {
                                case STATUS_200:
                                    Intent listIntent;

                                    if (currentScreen.equals(ADD_EDIT_PROPERTY))
                                        listIntent = new Intent(Constants.ADD_EDIT_PROPERTY);
                                    else {
                                        listIntent = new Intent(Constants.GET_EVENTS);
                                        final String[] parseDateTime, parseDate;
                                        parseDateTime = eventDateTime.split(" ");
                                        parseDate = parseDateTime[0].split("-");

                                        listIntent.putExtra("year", Integer.parseInt(parseDate[0]));
                                        listIntent.putExtra("month", Integer.parseInt(parseDate[1]));
                                        listIntent.putExtra("day", Integer.parseInt(parseDate[2]));
                                    }
                                    getActivity().sendBroadcast(listIntent);
                                    ((HolderActivity) getActivity()).oneStepBack();
                                    break;
                                case STATUS_201:
                                    break;
                                case STATUS_202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());
                                    break;
                                case STATUS_203:
                                    getActivity().showSnackBar(getView().getRootView(), responseObj.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void onSuccess(EventDetailModel responseObj) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getView().getEtEventTitleView().setText(responseObj.getData().getEventName());
                getView().getEtEventTitleDetailView().setText(responseObj.getData().getEventName());
                getView().getEtAddNotesView().setText(responseObj.getData().getEventNotes());
                getView().getEtNotesDetailView().setText(responseObj.getData().getEventNotes());
                propertyId = String.valueOf(responseObj.getData().getPropertyId());
                applianceId = String.valueOf(responseObj.getData().getApplianceId());
                providerId = String.valueOf(responseObj.getData().getProviderId());
                projectId = String.valueOf(responseObj.getData().getProjectId());
                eventRepeatId = String.valueOf(responseObj.getData().getEventRepeat());
                getView().getTVEventFromTimeView().setText(Utils.getInstance().getYMDEventFormattedTime(responseObj.getData().getEventTimeFrom().replace("+00:00", "")));
                if (!responseObj.getData().getEventTimeTo().isEmpty())
                    getView().getTVEventToTimeView().setText(Utils.getInstance().getYMDEventFormattedTime(responseObj.getData().getEventTimeTo().replace("+00:00", "")));
                if (!responseObj.getData().getEventReminder().isEmpty())
                    getView().getRepeatReminderView().setText(Utils.getInstance().getYMDEventFormattedTime(responseObj.getData().getEventReminder()));
                for (int i = 0; i < listProperties.size(); i++) {
                    if (propertyId.equals(String.valueOf(listProperties.get(i).getValue()))) {
                        getView().getSpinnerPropertyView().setSelection(i + 1);
                        return;
                    }
                }

                for (int i = 0; i < listProviders.size(); i++) {
                    if (providerId.equals(String.valueOf(listProviders.get(i).getValue()))) {
                        getView().getSpinnerProviderView().setSelection(i + 1);
                        return;
                    }
                }

                for (int i = 0; i < listEventReminder.size(); i++) {
                    if (eventRepeatId.equals(String.valueOf(listEventReminder.get(i).getValue()))) {
                        getView().getSpinnerRepeatEventView().setSelection(i + 1);
                        return;
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void onSuccess(GetPropertyResponse dataObj, ApiName check) {
        try {
            if (getActivity() != null && isViewAttached()) {
                switch (check) {
                    case GET_PROPERTY:
                        if (dataObj.getData() != null && dataObj.getData().size() > 0) {
                            /************update property spinner**********/
                            if (getActivity() != null && isViewAttached()) {
                                if (propertyAdapter != null) {
                                    listProperties.addAll(dataObj.getData());
                                    propertyAdapter.updateListWithOption(getActivity(), dataObj.getData());
                                }
                                for (int i = 0; i < dataObj.getData().size(); i++) {
                                    if (propertyId.equals(String.valueOf(dataObj.getData().get(i).getValue()))) {
                                        getView().getSpinnerPropertyView().setSelection(i + 1);
                                        return;
                                    }
                                }
                            }
                        }
                        break;

                    case GET_PROJECT_BIDS_PROVIDER:
                        /************update provider spinner**********/
                        if (getActivity() != null && isViewAttached()) {
                            if (getView().getEventType() != 2) {
                                if (providerAdapter != null) {
                                    listProviders = dataObj.getData();
                                    providerAdapter.updateList(getActivity(), dataObj.getData());
                                }
                                for (int i = 0; i < dataObj.getData().size(); i++) {
                                    if (providerId.equals(String.valueOf(dataObj.getData().get(i).getValue()))) {
                                        getView().getSpinnerProviderView().setSelection(i + 1);
                                        return;
                                    }
                                }
                            } else {
                                providerAdapter.updateOwner(getActivity(), Utils.getInstance().getUserName(getActivity()));
                            }
                        }
                        break;
                    case GET_APPLIANCE:
                        if (applianceAdapter != null) {
                            getView().getSpinnerApplianceView().setVisibility(View.VISIBLE);
                            listAppliances = dataObj.getData();
                            applianceAdapter.updateList(getActivity(), dataObj.getData());
                            for (int i = 0; i < dataObj.getData().size(); i++) {
                                if (applianceId.equals(String.valueOf(dataObj.getData().get(i).getValue()))) {
                                    getView().getSpinnerApplianceView().setSelection(i + 1);
                                    return;
                                }
                            }
                        }
                        break;
                    case GET_PROJECT_LIST:
                        if (projectAdapter != null) {
                            getView().getSpinnerProjectView().setVisibility(View.VISIBLE);
                            listProjects = dataObj.getData();
                            projectAdapter.updateList(getActivity(), dataObj.getData());
                            for (int i = 0; i < dataObj.getData().size(); i++) {
                                if (projectId.equals(String.valueOf(dataObj.getData().get(i).getValue()))) {
                                    getView().getSpinnerProjectView().setSelection(i + 1);
                                    return;
                                }
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void onSuccess(ResourceBean dataObj, ApiName check) {
        try {
            if (getActivity() != null && isViewAttached()) {
                switch (check) {
                    case GET_EVENT_REMINDER_API:
                        if (reminderEventAdapter != null) {
                            getView().getSpinnerProjectView().setVisibility(View.VISIBLE);
                            if (!listEventReminder.isEmpty())
                                listEventReminder.clear();
                            listEventReminder.addAll(dataObj.getData().get(0).getInfo());
                            reminderEventAdapter.updateListEvent(getActivity(), dataObj.getData().get(0).getInfo());
                            for (int i = 0; i < listEventReminder.size(); i++) {
                                if (eventRepeatId.toLowerCase().equals(listEventReminder.get(i).getValue().toLowerCase())) {
                                    getView().getSpinnerRepeatEventView().setSelection(i + 1);
                                    return;
                                }
                            }

                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {

            case R.id.spinner_project_name:
                if (position != 0) {
                    projectId = String.valueOf(listProjects.get(position - 1).getValue());
                    getProvidersList(projectId);
                } else {
                    projectId = "";
                    providerId = "";
                    listProviders.clear();
                    providerAdapter.updateList(getActivity(), listProviders);
                }
                break;
            case R.id.spinner_property_name:
                if (position != 0) {
                    propertyId = String.valueOf(listProperties.get(position - 1).getValue());
                    getAppliancesList(propertyId);
                    getProjectsList(propertyId);
                } else {
                    propertyId = "";
                    listAppliances.clear();
                    applianceAdapter.updateList(getActivity(), listAppliances);
                    listProjects.clear();
                    projectAdapter.updateList(getActivity(), listProjects);
                }
                break;
            case R.id.spinner_provider_name:
                if (position != 0) {
                    providerId = String.valueOf(listProviders.get(position - 1).getValue());
                } else {
                    providerId = "";
                }
                break;
            case R.id.spinner_repeat_event:
                if (position != 0) {
                    eventRepeatId = String.valueOf(listEventReminder.get(position - 1).getID());
                } else {
                    eventRepeatId = "";
                }
                break;
            case R.id.spinner_appliance_name:
                if (position != 0) {
                    applianceId = String.valueOf(listAppliances.get(position - 1).getValue());
                } else {
                    applianceId = "";
                }
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void getEventDetails(String eventID) {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                //hit API to get Property ID
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.EVENT_ID, eventID);
                getInterActor().getEventDetails(param, ApiName.GET_EVENT_DETAIL_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }


    private void getPropertiesList() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                //hit API to get Property ID
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                getInterActor().getProperty(param, ApiName.GET_PROPERTY);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    private void getProvidersList(String projectId) {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                //hit API to get Property ID
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.PROJECT_ID, projectId);
                getInterActor().getProjectBidsProviders(param, ApiName.GET_PROJECT_BIDS_PROVIDER);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    private void getAppliancesList(String propertyId) {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.PROPERTY_ID, propertyId);
                getInterActor().getAppliance(param, ApiName.GET_APPLIANCE);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    private void getProjectsList(String propertyId) {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.PROPERTY_ID, propertyId);
                getInterActor().getProjectListing(param, ApiName.GET_PROJECT_LIST);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    /*private void getReminder() {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                Map<String, String> param = new HashMap<>();
                getActivity().showProgressDialog();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.TYPE, Constants.EVENT_REMINDER_REPEAT);
                getInterActor().getResources(param, ApiName.GET_REMINDER_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }*/

    private void getEventReminder() {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                Map<String, String> param = new HashMap<>();
                getActivity().showProgressDialog();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.TYPE, Constants.EVENT_REPEAT);
                getInterActor().getResources(param, GET_EVENT_REMINDER_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    public boolean addEditEvent() {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    eventTitle = getView().getEtEventTitleView().getText().toString().trim();
                    eventDateTime = getView().getTVEventFromTimeView().getText().toString().trim();
                    reminderDateTime = getView().getRepeatReminderView().getText().toString().trim();
                    addNotes = getView().getEtAddNotesView().getText().toString().trim();
                    eventTimeTo = getView().getTVEventToTimeView().getText().toString();
                    String response = Validations.getInstance().validateEventFields(eventTitle, eventDateTime, eventTimeTo, reminderDateTime);

                    if (!response.isEmpty()) {
                        getActivity().showSnackBar(getView().getRootView(), response);
                    } else {
                        Map<String, String> param = new HashMap<>();
                        getActivity().showProgressDialog();

                        if (getView().getCurrentScreen().equals(Constants.CLICK_OWNER_EDIT_EVENT) || getView().getCurrentScreen().equals(Constants.CLICK_OWNER_VIEW_EVENT))
                            param.put(Constants.EVENT_ID, eventID);
                        param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                        param.put(Constants.PROPERTY_ID, propertyId);
                        param.put(Constants.APPLIANCE_ID, applianceId);
                        param.put(Constants.PROJECT_ID, projectId);
                        param.put(Constants.PROVIDER_ID, providerId);
                        param.put(Constants.EVENT_NAME, Utils.getInstance().capitalize(eventTitle));
                        param.put(Constants.EVENT_NOTES, Utils.getInstance().capitalize(addNotes));
                        param.put(Constants.EVENT_DATE, eventDateTime);
                        param.put(Constants.EVENT_TIME_FROM, eventDateTime);
                        param.put(Constants.EVENT_TIME_TO, eventTimeTo);
                        param.put(Constants.EVENT_REMINDER, reminderDateTime);
                        param.put(Constants.EVENT_REPEAT, eventRepeatId);
                        if (isOccurrence) {
                            if (currentScreen.equals(CLICK_OWNER_ADD_EVENT)) {
                                if (!eventRepeatId.isEmpty())
                                    param.put(Constants.CHANGE_IN_SERIES, "1");
                                else param.put(Constants.CHANGE_IN_SERIES, "0");
                            } else
                                param.put(Constants.CHANGE_IN_SERIES, "0");
                        } else {
                            if (!eventRepeatId.isEmpty())
                                param.put(Constants.CHANGE_IN_SERIES, "1");
                            else
                                param.put(Constants.CHANGE_IN_SERIES, "0");
                        }
                        //save user current timeZone value will be like Asia/Kolkata
                        param.put(Constants.APP_TIME_ZONE, Utils.getInstance().getAppTimeZone());

                        getInterActor().addEditEvents(param, ADD_EDIT_EVENT);
                        return true;
                    }
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            switch (v.getId()) {
                case R.id.tv_date_time_from:
                    CalendarPicker.getInstance().showCalendar(getActivity(), this, v.getId(), getView().getTVEventFromTimeView().getText().toString().trim());
                    break;
                case R.id.tv_date_time_to:
                    CalendarPicker.getInstance().showCalendar(getActivity(), this, v.getId(), getView().getEtDateTimeToView().getText().toString().trim());
                    break;
                case R.id.tv_repeat_reminder_time:
                    System.out.println("getView().getRepeatReminderView().getText().toString() = " + getView().getRepeatReminderView().getText().toString());
                    CalendarPicker.getInstance().showCalendar(getActivity(), this, v.getId(), getView().getRepeatReminderView().getText().toString().trim());
                    break;
            }
        }
    }

    public void setEventReminderDateTime(String dateTime, int viewId) {
        System.out.println("dateTime = " + dateTime);
        if (getActivity() != null && isViewAttached()) {
            switch (viewId) {

                case R.id.tv_date_time_from:
//                    if (!getView().getTVEventToTimeView().getText().toString().isEmpty()
//                            && !isValidDate(getView().getTVEventToTimeView().getText().toString(), dateTime, "from")) {
//                        getActivity().showToast("Event from date should be equal to event to date and Event from time should be less than event to time.");
//                    } else {
                    getView().getTVEventFromTimeView().setText(dateTime);
//                    }
                    break;
                case R.id.tv_date_time_to:
//                    if (!getView().getTVEventFromTimeView().getText().toString().isEmpty()
//                            && !isValidDate(getView().getTVEventFromTimeView().getText().toString(), dateTime, "to")) {
//                        getActivity().showToast("Event from date should be equal to event to date and Event to time should be greater than event from time.");
//                    } else {
                    getView().getTVEventToTimeView().setText(dateTime);
//                    }
                    break;
                case R.id.tv_repeat_reminder_time:

//                    System.out.println("Event from--->" + getView().getTVEventToTimeView().getText().toString());
//                    System.out.println("Event to--->" + getView().getTVEventToTimeView().getText().toString());
//
//                    if (getView().getTVEventFromTimeView().getText().toString().isEmpty() ||
//                            getView().getTVEventToTimeView().getText().toString().isEmpty()) {
//                        getActivity().showToast("Please select Event from and Event to first to set reminder.");
//                    } else if (!isValidDate(getView().getTVEventToTimeView().getText().toString(), dateTime, "reminder")) {
//                        getActivity().showToast("Event reminder date & time should be less than event to date & time.");
//                    } else if (!isValidDate(getView().getTVEventFromTimeView().getText().toString(), dateTime, "reminder")) {
//                        getActivity().showToast("Event reminder date & time should be less than event from date & time.");
//                    } else {
                    getView().getRepeatReminderView().setText(dateTime);
//                    }
                    break;
            }
        }
    }

    private boolean isValidDate(String date1, String date2, String check) {
        Calendar timeFrom, timeTo;
        try {
            String[] splitDate1, splitDate2;
            String[] arrayDate1, arrayDate2, arrayTime1, arrayTime2;
            timeFrom = Calendar.getInstance();
            timeTo = Calendar.getInstance();


            splitDate1 = date1.split(" ");
            splitDate2 = date2.split(" ");

            System.out.println("splitDate1 = " + splitDate1[0]);
            System.out.println("splitDate2 = " + splitDate2[0]);


            arrayDate1 = splitDate1[0].split("-");
            arrayDate2 = splitDate2[0].split("-");

            arrayTime1 = splitDate1[1].split(":");
            arrayTime2 = splitDate2[1].split(":");


            switch (check) {
                case "from":
                    timeFrom.set(Integer.parseInt(arrayDate1[0]), Integer.parseInt(arrayDate1[1]), Integer.parseInt(arrayDate1[2]),
                            Integer.parseInt(arrayTime1[0]), Integer.parseInt(arrayTime1[1]), Integer.parseInt(arrayTime1[2]));

                    timeTo.set(Integer.parseInt(arrayDate2[0]), Integer.parseInt(arrayDate2[1]), Integer.parseInt(arrayDate2[2]),
                            Integer.parseInt(arrayTime2[0]), Integer.parseInt(arrayTime2[1]), Integer.parseInt(arrayTime2[2]));
                    return !timeTo.getTime().equals(timeFrom.getTime()) && timeTo.getTime().before(timeFrom.getTime());
                case "to":
                    timeFrom.set(Integer.parseInt(arrayDate1[0]), Integer.parseInt(arrayDate1[1]), Integer.parseInt(arrayDate1[2]));
                    timeTo.set(Integer.parseInt(arrayDate2[0]), Integer.parseInt(arrayDate2[1]), Integer.parseInt(arrayDate2[2]));

                    return timeTo.getTime().equals(timeFrom.getTime());
                case "reminder":
                    timeFrom.set(Integer.parseInt(arrayDate1[0]), Integer.parseInt(arrayDate1[1]), Integer.parseInt(arrayDate1[2]),
                            Integer.parseInt(arrayTime1[0]), Integer.parseInt(arrayTime1[1]), Integer.parseInt(arrayTime1[2]));

                    timeTo.set(Integer.parseInt(arrayDate2[0]), Integer.parseInt(arrayDate2[1]), Integer.parseInt(arrayDate2[2]),
                            Integer.parseInt(arrayTime2[0]), Integer.parseInt(arrayTime2[1]), Integer.parseInt(arrayTime2[2]));
                    return !timeTo.getTime().equals(timeFrom.getTime()) && timeTo.getTime().before(timeFrom.getTime()) && !timeTo.getTime().after(timeFrom.getTime());
            }


        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return false;

    }

    public void enableDisableUI(boolean isEdit) {

        if (getActivity() != null && isViewAttached()) {
            if (isEdit) {

                getView().getEtReminderView().setEnabled(true);
                getView().getEtReminderView().setFocusable(true);
                getView().getEtReminderView().setFocusableInTouchMode(true);
                getView().getEtReminderView().requestFocus();

                getView().getEtDateTimeToView().setEnabled(true);
                getView().getRepeatReminderView().setEnabled(true);

                getView().getEtDateTimeFromView().setEnabled(true);

                getView().getTILEventTitleView().setVisibility(View.VISIBLE);
                getView().getTILEventTitleDetailView().setVisibility(View.GONE);

                getView().getTILNotesView().setVisibility(View.VISIBLE);
                getView().getTILNotesDetailView().setVisibility(View.GONE);

                getView().getEtAddNotesView().setEnabled(true);
                getView().getEtAddNotesView().setFocusable(true);
                getView().getEtAddNotesView().setFocusableInTouchMode(true);
                getView().getEtAddNotesView().requestFocus();

                getView().getEtEventTitleView().setEnabled(true);
                getView().getEtEventTitleView().setFocusable(true);
                getView().getEtEventTitleView().setFocusableInTouchMode(true);
                getView().getEtEventTitleView().requestFocus();

                getView().getSpinnerProviderView().setEnabled(true);
                getView().getSpinnerPropertyView().setEnabled(true);
                getView().getSpinnerProjectView().setEnabled(true);
                getView().getSpinnerApplianceView().setEnabled(true);
                if (!isEventRepeat) {
                    if (!isOccurrence) {
                        getView().getSpinnerRepeatEventView().setEnabled(true);
                        getView().getHeaderEventRepeatNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                    } else {
                        getView().getSpinnerRepeatEventView().setEnabled(false);
                        getView().getHeaderEventRepeatNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                    }
                } else {
                    getView().getSpinnerRepeatEventView().setEnabled(true);
                    getView().getHeaderEventRepeatNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                }

                getView().getHeaderPropertyNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getHeaderPropertyNameView().setText(R.string.label_select_property);

                getView().getHeaderProjectNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getHeaderProjectNameView().setText(R.string.label_select_project);

                getView().getHeaderApplianceNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getHeaderApplianceNameView().setText(R.string.label_select_appliance);

                getView().getHeaderProviderNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getHeaderProviderNameView().setText(R.string.label_select_provider_name);

                getView().getHeaderRepeatReminderName().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getHeaderRepeatReminderName().setText(R.string.set_reminder);

                getView().getTVHeaderEventFromTimeView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getTVHeaderEventToView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));

                getView().getProviderNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getPropertyNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getApplianceNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getProjectNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getEventFromLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getEventToLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getReminderTimeLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getRepeatEventLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));


            } else {
                getView().getEtEventTitleView().setEnabled(false);
                getView().getEtEventTitleView().setFocusable(false);

                getView().getEtEventTitleDetailView().setEnabled(false);
                getView().getEtEventTitleDetailView().setFocusable(false);

                getView().getEtAddNotesView().setEnabled(false);
                getView().getEtAddNotesView().setFocusable(false);

                getView().getEtNotesDetailView().setEnabled(false);
                getView().getEtNotesDetailView().setFocusable(false);

                getView().getEtDateTimeFromView().setEnabled(false);
                getView().getEtDateTimeFromView().setFocusable(false);

                getView().getEtDateTimeToView().setEnabled(false);
                getView().getEtDateTimeToView().setFocusable(false);

                getView().getEtReminderView().setEnabled(false);
                getView().getRepeatReminderView().setEnabled(false);
                getView().getEtReminderView().setFocusable(false);

                getView().getSpinnerProviderView().setEnabled(false);
                getView().getSpinnerPropertyView().setEnabled(false);
                getView().getSpinnerProjectView().setEnabled(false);
                getView().getSpinnerApplianceView().setEnabled(false);
                getView().getSpinnerRepeatEventView().setEnabled(false);

                getView().getTILEventTitleDetailView().setVisibility(View.VISIBLE);
                getView().getTILEventTitleView().setVisibility(View.GONE);

                getView().getTILNotesView().setVisibility(View.GONE);
                getView().getTILNotesDetailView().setVisibility(View.VISIBLE);

                getView().getHeaderPropertyNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getHeaderPropertyNameView().setText(R.string.label_select_property_detail);
                getView().getHeaderProjectNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getHeaderProjectNameView().setText(R.string.label_select_project_detail);

                getView().getHeaderApplianceNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getHeaderApplianceNameView().setText(R.string.label_select_appliance_detail);

                getView().getHeaderProviderNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getHeaderProviderNameView().setText(R.string.label_select_provider_name_detail);

                getView().getHeaderEventRepeatNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getHeaderRepeatReminderName().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getHeaderRepeatReminderName().setText(R.string.set_reminder_detail);

                getView().getTVHeaderEventFromTimeView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getTVHeaderEventToView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));

                getView().getProviderNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
                getView().getPropertyNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
                getView().getApplianceNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
                getView().getProjectNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
                getView().getEventFromLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
                getView().getEventToLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
                getView().getReminderTimeLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
                getView().getRepeatEventLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));

            }
        }

    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
        }
        return false;
    }
}
