package com.ohp.ownerviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.interfaces.AdapterPresenter;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.adapter.DocumentListAdapter;
import com.ohp.ownerviews.beans.DocumentListModel;
import com.ohp.ownerviews.view.DocumentView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static com.ohp.utils.Constants.ADD_EDIT_PROPERTY;

/**
 * @author Amanpal Singh.
 */

public class DocumentListFragmentPresenterImpl extends FragmentPresenter<DocumentView, APIHandler>
        implements APIResponseInterface.OnCallableResponse, AdapterPresenter<DocumentListAdapter, DocumentListModel>, View.OnClickListener {

    private DocumentListAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private List<DocumentListModel.DataBean> documentList = new ArrayList<>();
    private int deletedAppliancePosition;
    private String propertyId = "", applianceId = "", searchQuery = "";
    private int page = 1;
    private int pastVisibleItems, visibleItemCount, totalItemCount;
    private boolean loading = true, isSearched;
    private DocumentListModel listModel;
    private BroadcastReceiver documentsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            page = 1;
            documentList = new ArrayList<>();
            getDocumentList();
            Intent listIntent = new Intent(Constants.ADD_EDIT_PROPERTY);
            context.sendBroadcast(listIntent);
        }
    };

    public DocumentListFragmentPresenterImpl(DocumentView documentView, Context context) {
        super(documentView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(documentsReceiver,
                    new IntentFilter(Constants.GET_DOCUMENTS_LIST));

            page = 1;
            if (getView().getNavigateFrom().equals(Constants.ADD_EDIT_PROPERTY)) {
                propertyId = getView().getPropertyID();
                getActivity().setSupportActionBar(getView().getToolbarView());
                if (getActivity().getSupportActionBar() != null)
                    getActivity().getSupportActionBar().setTitle("");
                getView().getToolbarView().setTitle("");
                getView().getToolbarTitle().setText(R.string.title_document_listing);
                getView().getToolbarView().setNavigationIcon(R.drawable.ic_navigation_back);
                getView().getToolbarView().setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        ((HolderActivity) getActivity()).oneStepBack();
                    }
                });
            }


            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });
        }

        getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0) {
                    getView().getCloseBtn().setVisibility(View.VISIBLE);
                    searchQuery = s.toString();
                    page = 1;
                    getDocumentList();
                    isSearched = true;

                } else {
                    page = 1;
                    searchQuery = "";
                    isSearched = true;
                    getView().getCloseBtn().setVisibility(View.GONE);
                    documentList.clear();
                    adapter.update(documentList);
                    getDocumentList();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        getDocumentList();
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_DOCUMENT_LIST:
                        if (response.isSuccessful()) {
                            listModel = (DocumentListModel) response.body();
                            switch (listModel.getStatus()) {
                                case Constants.STATUS_200:
                                    loading = true;
                                    onSuccess(listModel);
                                    break;
                                case Constants.STATUS_201:
                                    if (documentList == null)
                                        documentList = new ArrayList<>();
                                    if (!documentList.isEmpty())
                                        documentList.clear();
                                    adapter.update(documentList);
//                                    getActivity().showSnakBar(getView().getRootView(), listModel.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    if (getActivity() instanceof OwnerDashboardActivity)
                                        ((OwnerDashboardActivity) getActivity()).logout(listModel.getMessage());
                                    else if (getActivity() instanceof HolderActivity)
                                        ((HolderActivity) getActivity()).logout(listModel.getMessage());
                                    break;
                            }
                        }
                        break;
                    case DELETE_DOCUMENT:
                        if (response.isSuccessful()) {
                            GenericBean genericBeanModel = (GenericBean) response.body();
                            loading = false;
                            switch (listModel.getStatus()) {
                                case Constants.STATUS_200:
                                    if (adapter != null) {
                                        documentList.remove(deletedAppliancePosition);
                                        adapter.notifyDataSetChanged();
//                                        adapter.deleteItem(deletedAppliancePosition);
                                    }
                                    if (documentList != null && documentList.size() > 0) {
                                        getView().getEmptyView().setVisibility(View.GONE);
                                    } else {
                                        getView().getEmptyView().setVisibility(View.VISIBLE);
                                    }
                                    if (getView().getNavigateFrom().equals(ADD_EDIT_PROPERTY)) {
                                        Intent listIntent = new Intent(Constants.ADD_EDIT_PROPERTY);
                                        getActivity().sendBroadcast(listIntent);
                                    }
                                    break;
                                case Constants.STATUS_201:
//                                    getActivity().showSnakBar(getView().getRootView(), listModel.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    if (getActivity() instanceof OwnerDashboardActivity)
                                        ((OwnerDashboardActivity) getActivity()).logout(listModel.getMessage());
                                    else if (getActivity() instanceof HolderActivity)
                                        ((HolderActivity) getActivity()).logout(listModel.getMessage());
                                    break;
                            }
                        }
                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(DocumentListModel listModel) {
        if (getActivity() != null && isViewAttached()) {
            if (listModel.getData() != null && listModel.getData().size() > 0) {
                getView().getEmptyView().setVisibility(View.GONE);
                if (documentList == null)
                    documentList = new ArrayList<>();
                if (isSearched && searchQuery.length() != 0) {
                    documentList = new ArrayList<>();
                    documentList.addAll(listModel.getData());
                } else if (listModel.getTotalRecord() > documentList.size()) {
                    documentList.addAll(listModel.getData());
                }
                adapter.update(documentList);

            } else {
                if (documentList.isEmpty()) {
                    adapter.clearAll();
                    getView().getEmptyView().setVisibility(View.VISIBLE);
                } else {
                    if (isSearched || searchQuery.length() != 0) {
                        adapter.clearAll();
                        getView().getEmptyView().setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        getDocumentList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null && isViewAttached())
            getActivity().unregisterReceiver(documentsReceiver);
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onItemClicked(final int layoutPosition, ScreenNavigation click, ImageView imageView) {
        Intent intent = new Intent(getActivity(), HolderActivity.class);
        switch (click) {
            case DETAIL_ITEM:
                intent.putExtra(Constants.PROPERTY_ID, String.valueOf(documentList.get(layoutPosition).getPropertyId()));
                intent.putExtra(Constants.DOCUMENT_ID, String.valueOf(documentList.get(layoutPosition).getId()));
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_DOCUMENT);
//                if (Utils.getInstance().isEqualLollipop()) {
//                    Pair<View, String> pair = Pair.create((View) imageView, Constants.TRANS_PROPERTY_IMAGE);
//                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pair);
//                    getActivity().startActivity(intent, options.toBundle());
//                } else {
                getActivity().startActivity(intent);
//                }
                break;
            case EDIT_ITEM:
                intent.putExtra(Constants.PROPERTY_ID, String.valueOf(documentList.get(layoutPosition).getPropertyId()));
                intent.putExtra(Constants.DOCUMENT_ID, String.valueOf(documentList.get(layoutPosition).getId()));
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_EDIT_DOCUMENT);
                getActivity().startActivity(intent);
                break;
            case DELETE_ITEM:
                Utils.getInstance().confirmDialog(getActivity(), "Are you sure, you want to delete this document?", "delete", new Utils.ConfirmDialogCallbackInterface() {
                    @Override
                    public void onYesClick(String tag) {
                        deletedAppliancePosition = layoutPosition;
                        deleteDocument(String.valueOf(documentList.get(layoutPosition).getId()));
                    }

                    @Override
                    public void onNoClick(String tag) {

                    }
                });

                break;
        }
    }

    private void getDocumentList() {
        try {
            if (getActivity() != null)
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    if (TextUtils.isEmpty(searchQuery))
                        getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    if (!TextUtils.isEmpty(searchQuery))
                        param.put(Constants.SEARCH_PARAM, searchQuery);
                    param.put(Constants.PAGE_PARAM, String.valueOf(page));
                    param.put(Constants.PROPERTY_ID, propertyId);
                    getInterActor().getDocumentList(param, ApiName.GET_DOCUMENT_LIST);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteDocument(String documentId) {
        try {
            if (getActivity() != null && Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.DOCUMENT_ID, documentId);
                getInterActor().deleteDocument(param, ApiName.DELETE_DOCUMENT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public DocumentListAdapter initAdapter() {
        if (getActivity() != null && isViewAttached()) {
            adapter = new DocumentListAdapter(documentList, getActivity(), this);
            getView().getRecyclerView().setNestedScrollingEnabled(false);
            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

                            if (listModel != null && listModel.getTotalRecord() > documentList.size()) {
                                page += 1;
                                getDocumentList();
                            }

                            System.out.println("scrollY = " + scrollY);
                            System.out.println("(v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) = " + (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight()));

                            System.out.println("oldScrollY = " + oldScrollY);
                            System.out.println((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                    scrollY > oldScrollY);

                        }
                    }
                }
            });
        }
        return adapter;
    }

    @Override
    public void updateList(List<DocumentListModel> filterList) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_add_documents:
                adapter.mItemManger.closeAllItems();
                Intent intent = new Intent(getActivity(), HolderActivity.class);
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_ADD_DOCUMENT);
                intent.putExtra(Constants.PROPERTY_ID, propertyId);
                getActivity().startActivity(intent);
                break;
            case R.id.btn_clear:
                searchQuery = "";
                page = 1;
                getView().getSearchEditText().setText("");
                break;
        }
    }
}
