package com.ohp.ownerviews.presenter;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.ServiceProvidersModel;
import com.ohp.dataHolder.DataHolder;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.interfaces.AdapterPresenter;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.adapter.ServiceProviderListAdapter;
import com.ohp.ownerviews.fragment.ServiceProviderList;
import com.ohp.ownerviews.view.ServiceProviderListView;
import com.ohp.serviceproviderviews.fragment.ProviderFilterDialogFragment;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * @author Amanpal Singh.
 */

public class ServiceProviderListPresenterImpl extends FragmentPresenter<ServiceProviderListView, APIHandler>
        implements APIResponseInterface.OnCallableResponse, View.OnClickListener, AdapterPresenter<ServiceProviderListAdapter, ServiceProvidersModel.DataBean>,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {

    private ServiceProvidersModel listModel;
    private LocationRequest mLocationRequest;
    private int permsRequestCode = 200;
    private List<ServiceProvidersModel.DataBean> listServiceProviders = new ArrayList<>();
    private ServiceProviderListAdapter adapter;
    private boolean isSearched = false, isFilterChanged = false;
    private String searchQuery = "";
    private int page = 1;
    private LinearLayoutManager linearLayoutManager;
    private double longitude = 0, latitude = 0;
    private String stringLongitude = "", stringLatitude = "", distance = "", rating = "", expertise = "", licensed = "";
    private final BroadcastReceiver filterReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            page = 1;
            distance = intent.getStringExtra(Constants.FILTER_DISTANCE);
            expertise = intent.getStringExtra(Constants.FILTER_EXPERTISE);
            licensed = intent.getStringExtra(Constants.FILTER_LICENSED);
            rating = intent.getStringExtra(Constants.FILTER_RATING);
            listServiceProviders.clear();
            adapter.update(listServiceProviders);
            getServiceProvidersList();

        }
    };
    // Handler to get the current location
    private Handler handler = new Handler();
    private GoogleApiClient mGoogleApiClient;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            getLastLocation();
        }
    };
    private final BroadcastReceiver mGpsDetectionReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            page = 1;
            listServiceProviders.clear();
            adapter.update(listServiceProviders);
            if (intent.getExtras().getBoolean("isGpsEnabled", false)) {
                getLastLocation();
            } else {
                stringLatitude = "";
                stringLongitude = "";
                getServiceProvidersList();
            }
        }
    };

    public ServiceProviderListPresenterImpl(ServiceProviderListView serviceProviderListView, Context context) {
        super(serviceProviderListView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(mGpsDetectionReceiver, new IntentFilter("gpsDetectionIntent"));
            getActivity().registerReceiver(filterReceiver, new IntentFilter(Constants.BROWSE_SERVICE_PROVIDER));
            linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getRecyclerView().setLayoutManager(linearLayoutManager);
            getView().getRecyclerView().setAdapter(initAdapter());
            getView().getRecyclerView().setNestedScrollingEnabled(false);

            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

                            if (listModel != null && listModel.getTotalRecord() > listServiceProviders.size()) {
                                page += 1;
                                getServiceProvidersList();
                            }
                        }
                    }
                }
            });

            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });

            getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(final CharSequence s, int start, int before, int count) {
                    if (s != null && s.length() > 0) {
                        getView().getCloseBtn().setVisibility(View.VISIBLE);

                        searchQuery = s.toString();
                        page = 1;
                        isSearched = true;
                        getServiceProvidersList();
                    } else {
                        searchQuery = "";
                        page = 1;
                        isSearched = true;
                        listServiceProviders.clear();
                        adapter.update(listServiceProviders);
                        getServiceProvidersList();
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        getView().getCloseBtn().setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        mLocationRequest = generateLocationRequest();
        buildGoogleApiClient();
        connectToGoogleApiClient();
        getLastLocation();
    }


    public void getServiceProvidersList() {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    if (TextUtils.isEmpty(searchQuery))
                        getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    if (!TextUtils.isEmpty(searchQuery))
                        param.put(Constants.SEARCH_PARAM, searchQuery);
                    param.put(Constants.PAGE_PARAM, String.valueOf(page));
                    if (!TextUtils.isEmpty(distance))
                        param.put(Constants.DISTANCE, distance);
                    if (!TextUtils.isEmpty(rating))
                        param.put(Constants.RATING, rating);
                    if (!TextUtils.isEmpty(expertise))
                        param.put(Constants.EXPERTISE, expertise);
                    if (!TextUtils.isEmpty(licensed))
                        param.put(Constants.LICENSE, licensed);
                    param.put(Constants.LATITUDE, stringLatitude);
                    param.put(Constants.LONGITUDE, stringLongitude);

                    getInterActor().browseServiceProvider(param, ApiName.BROWSE_SERVICE_PROVIDER);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case BROWSE_SERVICE_PROVIDER:
                        if (response.isSuccessful()) {
                            listModel = (ServiceProvidersModel) response.body();
                            switch (listModel.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(listModel);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((OwnerDashboardActivity) getActivity()).logout(listModel.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(ServiceProvidersModel listModel) {
        if (getActivity() != null && isViewAttached()) {
            if (listModel.getData() != null && listModel.getData().size() > 0) {
                getView().getEmptyView().setVisibility(View.GONE);
                if (listServiceProviders == null) listServiceProviders = new ArrayList<>();

                if (isSearched || searchQuery.length() != 0) {
                    listServiceProviders = new ArrayList<>();
                    listServiceProviders.addAll(listModel.getData());
                    isSearched = false;
                } else if (listModel.getTotalRecord() > listServiceProviders.size()) {
                    listServiceProviders.addAll(listModel.getData());
                }
                if (isFilterChanged) {
                    listServiceProviders = listModel.getData();
                    isFilterChanged = false;
                }
                adapter.update(listServiceProviders);
            } else {
                if (listServiceProviders.isEmpty()) {
                    adapter.clearAll();
                    getView().getEmptyView().setVisibility(View.VISIBLE);
                } else {
                    if (isSearched || searchQuery.length() != 0) {
                        adapter.clearAll();
                        getView().getEmptyView().setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.btn_clear:
                    searchQuery = "";
                    page = 1;
                    getView().getSearchEditText().setText("");
                    break;
            }
        }
    }

    @Override
    public ServiceProviderListAdapter initAdapter() {
        if (getActivity() != null && isViewAttached()) {
            adapter = new ServiceProviderListAdapter(listServiceProviders, getActivity(), this);
            getView().getRecyclerView().setAdapter(adapter);
        }
        return adapter;
    }

    @Override
    public void updateList(List<ServiceProvidersModel.DataBean> filterList) {

    }

    public void onItemClicked(final int layoutPosition, ScreenNavigation click, ImageView imageView) {
        if (getActivity() != null && isViewAttached()) {
            Intent intent = new Intent(getActivity(), HolderActivity.class);
            switch (click) {
                case DETAIL_ITEM:
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_PROVIDER);
                    intent.putExtra(Constants.NAVIGATE_FROM, Constants.BROWSE_SERVICE_PROVIDER);
                    intent.putParcelableArrayListExtra(Constants.PROVIDER_LIST, (ArrayList<? extends Parcelable>) listServiceProviders);
                    intent.putExtra(Constants.POSITION, layoutPosition);
                    if (Utils.getInstance().isEqualLollipop()) {
                        Pair<View, String> pair = Pair.create((View) imageView, Constants.TRANS_PROPERTY_IMAGE);
                        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pair);
                        getActivity().startActivity(intent, options.toBundle());
                    } else {
                        getActivity().startActivity(intent);
                    }
                    break;
            }
        }
    }


    /*********************************************************************************************/

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().unregisterReceiver(mGpsDetectionReceiver);
                getActivity().unregisterReceiver(filterReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * function to initialize the Google API Client
     */
    private synchronized void buildGoogleApiClient() {
        if (getActivity() != null && isViewAttached()) {
            if (mGoogleApiClient == null)
                mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .build();
        }
    }

    /**
     * function to get the last known location of device
     */
    private void getLastLocation() {

        if (getActivity() != null && isViewAttached()) {
            if (!getLastKnownLocationIfAllowed())
                checkLocationPermission();
            else {
                Location lastKnowLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                if (lastKnowLocation != null) { // last location received
                    if (handler != null && runnable != null)
                        handler.removeCallbacks(runnable);
                    getLocationData(lastKnowLocation);

                } else { // last location not received
                    if (handler != null && runnable != null)
                        handler.postDelayed(runnable, 3000);

                }
            }
        }

    }

    private void getLocationData(Location lastKnownLocation) {
        try {
            longitude = lastKnownLocation.getLongitude();
            latitude = lastKnownLocation.getLatitude();

            stringLongitude = String.valueOf(longitude);
            stringLatitude = String.valueOf(latitude);
            getServiceProvidersList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        try {
            if (getActivity() != null && isViewAttached()) {
                switch (requestCode) {
                    case 200:
                        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                            // Or use LocationManager.GPS_PROVIDER
                            System.out.println("permission location granted--->" + getLastKnownLocationIfAllowed());
                            if (getLastKnownLocationIfAllowed())
                                checkLocationPermission();

                        } else {
                            page = 1;
                            listServiceProviders.clear();
                            adapter.update(listServiceProviders);
                            stringLatitude = "";
                            stringLongitude = "";
                            getServiceProvidersList();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    public void checkLocationPermission() {
        try {
            if (getActivity() != null && isViewAttached()) {
                int hasPermission;
                hasPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
                if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                    ((ServiceProviderList) getView()).requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            permsRequestCode);

                    return;
                }

                getLastLocation();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean getLastKnownLocationIfAllowed() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null && isViewAttached()) {
            disconnectFromGoogleApiClient();
            if (handler != null) {
                handler.removeCallbacks(runnable);
            }
            DataHolder.getInstance().setRating("");
            DataHolder.getInstance().setDistance("");
            DataHolder.getInstance().setExpertise("");
            DataHolder.getInstance().setLicensed("");
            DataHolder.clearInstance();
            ProviderFilterDialogFragment.clearInstance();
        }
    }

    /**
     * function to connect to Google API Client
     */
    private void connectToGoogleApiClient() {
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    /**
     * function to disconnect from Google API Client
     */
    private void disconnectFromGoogleApiClient() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
// register for location updates
        buildLocationSettingsRequest(mLocationRequest, mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * function to build the request dialog for checking the Location
     *
     * @param mLocationRequest location request
     * @param mGoogleApiClient google api client
     * @param resultCallback   callback
     */
    private void buildLocationSettingsRequest(LocationRequest mLocationRequest, GoogleApiClient mGoogleApiClient, ResultCallback<LocationSettingsResult> resultCallback) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        LocationSettingsRequest mLocationSettingsRequest = builder.build();

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(resultCallback);
    }


    // function to create mLocation Request
    private LocationRequest generateLocationRequest() {
        // Create the LocationRequest object
        LocationRequest mLocationRequest = LocationRequest.create();
        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(5000);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(1000);
        return mLocationRequest;
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                getLastLocation();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                try {
                    status.startResolutionForResult(getActivity(), 1);
                } catch (IntentSender.SendIntentException e) {
//                    Toast.makeText(getActivity(), "User choose not to make required location settings changes.", Toast.LENGTH_SHORT).show();
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                Toast.makeText(getActivity(), "Location settings are inadequate, and cannot be fixed here. Dialog " +
//                        "not created.", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case 1:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getLastLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        page = 1;
                        listServiceProviders.clear();
                        adapter.update(listServiceProviders);
                        stringLatitude = "";
                        stringLongitude = "";
                        getServiceProvidersList();
                        break;
                }
                break;
        }
    }


}
