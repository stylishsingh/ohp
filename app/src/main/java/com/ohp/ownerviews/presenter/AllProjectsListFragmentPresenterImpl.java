package com.ohp.ownerviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.enums.ApiName;
import com.ohp.interfaces.AdapterPresenter;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.adapter.OwnerAllProjectsAdapter;
import com.ohp.ownerviews.beans.OwnerAllProjectsModel;
import com.ohp.ownerviews.view.AllProjectsDetailView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * @author Vishal Sharma
 */

public class AllProjectsListFragmentPresenterImpl extends FragmentPresenter<AllProjectsDetailView, APIHandler>
        implements APIHandler.OnCallableResponse, View.OnClickListener, AdapterPresenter<OwnerAllProjectsAdapter, OwnerAllProjectsModel.DataBean>, PopupMenu.OnMenuItemClickListener {
    public int page = 1;   //for pagination
    public String projectType = "";
    private OwnerAllProjectsAdapter allProjectsAdapter;
    private String searchQuery = "";
    private boolean isSearched = false, isFilterChanged = false;
    private List<OwnerAllProjectsModel.DataBean> listAllProjects = new ArrayList<>();
    private BroadcastReceiver projectsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("DashboardActivityPresenterImpl.onReceive");
            if (getActivity() != null && isViewAttached()) {
                page = 1;
                listAllProjects = new ArrayList<>();
                getAllProjects();
            }

        }
    };
    private OwnerAllProjectsModel listModel;
    private int adapterPosition = 0;

    public AllProjectsListFragmentPresenterImpl(AllProjectsDetailView allProjectsDetailView, Context context) {
        super(allProjectsDetailView, context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(projectsReceiver,
                    new IntentFilter(Constants.GET_PROJECTS));
            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getRecyclerView().setLayoutManager(lm);

            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });

            getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(final CharSequence s, int start, int before, int count) {
                    if (s != null && s.length() > 0) {
                        getView().getCloseBtn().setVisibility(View.VISIBLE);
                        searchQuery = s.toString();
                        page = 1;
                        isSearched = true;
                        getAllProjects();


                    } else {
                        searchQuery = "";
                        page = 1;
                        isSearched = true;
                        listAllProjects.clear();
                        updateList(listAllProjects);
                        getAllProjects();
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        getView().getCloseBtn().setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            getAllProjects();

        }
    }

    public void getAllProjects() {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    if (TextUtils.isEmpty(searchQuery))
                        getActivity().showProgressDialog();
                    Map<String, String> params = new HashMap<>();
                    params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    params.put(Constants.PAGE_PARAM, String.valueOf(page));
                    params.put(Constants.SEARCH_PARAM, searchQuery);
                    params.put(Constants.PROPERTY_ID, getView().propertyId());
                    params.put(Constants.PROJECT_TYPE, projectType);
                    getInterActor().getAllProjects(params, ApiName.GET_PROJECTS);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        if (getActivity() != null && isViewAttached())
            getActivity().unregisterReceiver(projectsReceiver);
        super.onDestroy();

    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    getActivity().hideProgressDialog();
                    switch (api) {
                        case GET_PROJECTS:
                            if (response.isSuccessful()) {
                                OwnerAllProjectsModel responseObj = (OwnerAllProjectsModel) response.body();
                                switch (responseObj.getStatus()) {
                                    case Constants.STATUS_200:
                                        onSuccess(responseObj);
                                        break;
                                    case Constants.STATUS_201:
                                        break;
                                    case Constants.STATUS_202:
                                        ((OwnerDashboardActivity) getActivity()).logout(responseObj.getMessage());
                                        break;
                                }
                            }
                            break;
                        case DELETE_PROJECT:
                            getActivity().hideProgressDialog();
                            if (response.isSuccessful()) {
                                GenericBean genericBean = (GenericBean) response.body();
                                switch (genericBean.getStatus()) {
                                    case Constants.STATUS_200:
                                        onSuccess(genericBean);
                                        break;
                                    case Constants.STATUS_201:
                                        break;
                                    case Constants.STATUS_202:
                                        ((OwnerDashboardActivity) getActivity()).logout(genericBean.getMessage());
                                        break;
                                }
                            }
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onSuccess(GenericBean genericBean) {
        if (getActivity() != null && isViewAttached()) {
            listAllProjects.remove(adapterPosition);
            allProjectsAdapter.notifyDataSetChanged();
            Intent listIntent = new Intent(Constants.GET_PROJECTS);
            getActivity().sendBroadcast(listIntent);

            Intent intent = new Intent(Constants.ADD_EDIT_PROPERTY);
            getActivity().sendBroadcast(intent);
        }
        if (listAllProjects != null && listAllProjects.size() > 0) {
            getView().getEmptyView().setVisibility(View.GONE);
        } else {
            Intent listIntent = new Intent(Constants.GET_PROJECTS);
            getActivity().sendBroadcast(listIntent);
            Intent intent = new Intent(Constants.ADD_EDIT_PROPERTY);
            getActivity().sendBroadcast(intent);
            getView().getEmptyView().setVisibility(View.VISIBLE);
        }
    }

    private void onSuccess(OwnerAllProjectsModel dataObj) {
        listModel = dataObj;
        if (listModel.getData() != null && listModel.getData().size() > 0) {
            getView().getEmptyView().setVisibility(View.GONE);
            if (listAllProjects == null) listAllProjects = new ArrayList<>();

            if (isSearched || searchQuery.length() != 0) {
                listAllProjects = new ArrayList<>();
                listAllProjects.addAll(listModel.getData());
                isSearched = false;
            } else if (listModel.getTotalRecord() > listAllProjects.size()) {
                listAllProjects.addAll(listModel.getData());
            }
            if (isFilterChanged) {
                listAllProjects = listModel.getData();
                isFilterChanged = false;
            }
            allProjectsAdapter.update(listAllProjects);
        } else {
            if (listAllProjects.isEmpty()) {
                allProjectsAdapter.clearAll();
                getView().getEmptyView().setVisibility(View.VISIBLE);
            } else {
                if (isSearched || searchQuery.length() != 0) {
                    allProjectsAdapter.clearAll();
                    getView().getEmptyView().setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void deleteProject(int project_id, int position) {
        if (getActivity() != null && isViewAttached()) {
            adapterPosition = position;
            try {
                getActivity().showProgressDialog();
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Map<String, String> params = new HashMap<>();
                    params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    params.put(Constants.PROJECT_ID, String.valueOf(project_id));
                    getInterActor().deleteProject(params, ApiName.DELETE_PROJECT);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            Intent intent = new Intent(getActivity(), HolderActivity.class);
            switch (v.getId()) {
                case R.id.fab_add:
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_ADD_PROJECT);
                    intent.putExtra(Constants.PROJECT_STATUS, "");
                    intent.putExtra(Constants.PROPERTY_ID, getView().propertyId());
                    getActivity().startActivity(intent);
                    break;
                case R.id.btn_clear:
                    searchQuery = "";
                    page = 1;
                    getView().getSearchEditText().setText("");
                    break;
            }


        }
    }

    @Override
    public OwnerAllProjectsAdapter initAdapter() {
        if (getActivity() != null && isViewAttached()) {
            allProjectsAdapter = new OwnerAllProjectsAdapter(getActivity(), listAllProjects, this);
            getView().getRecyclerView().setAdapter(allProjectsAdapter);
            getView().getRecyclerView().setNestedScrollingEnabled(false);
            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

                            if (listModel != null && listModel.getTotalRecord() > listAllProjects.size()) {
                                page += 1;
                                getAllProjects();
                            }

                            System.out.println("scrollY = " + scrollY);
                            System.out.println("(v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) = " + (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight()));

                            System.out.println("oldScrollY = " + oldScrollY);
                            System.out.println((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                    scrollY > oldScrollY);

                        }
                    }
                }
            });
        }
        return allProjectsAdapter;
    }

    @Override
    public void updateList(List<OwnerAllProjectsModel.DataBean> filterList) {

    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        isFilterChanged = true;
        page = 1;
        switch (menuItem.getItemId()) {
            case R.id.action_current:
                projectType = "current";
                listAllProjects = new ArrayList<>();
                getAllProjects();
                break;
            case R.id.action_completed:
                projectType = "completed";
                listAllProjects = new ArrayList<>();
                getAllProjects();
                break;
            case R.id.action_all:
                projectType = "";
                listAllProjects = new ArrayList<>();
                getAllProjects();
                break;
        }
        return true;
    }

    public void setToolbar() {
        getActivity().setSupportActionBar(getView().getToolbar());
        if (getActivity().getSupportActionBar() != null)
            getActivity().getSupportActionBar().setTitle("");
        getView().getToolbar().setTitle("");
        getView().getToolbarTitle().setText(R.string.title_all_projects);
        getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
        getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                if (getActivity() instanceof OwnerDashboardActivity)
                    ((OwnerDashboardActivity) getActivity()).oneStepBack();
                else if (getActivity() instanceof HolderActivity)
                    ((HolderActivity) getActivity()).oneStepBack();
            }
        });
    }
}
