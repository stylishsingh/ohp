package com.ohp.ownerviews.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.library.mvp.BaseView;
import com.ohp.R;
import com.ohp.commonviews.activities.BaseActivity;
import com.ohp.ownerviews.presenter.OwnerProfileActivityPresenterImpl;
import com.ohp.ownerviews.view.OwnerProfileActivityView;

/**
 * Created by Anuj Sharma on 5/25/2017.
 */

public class OwnerProfileActivity extends BaseActivity<OwnerProfileActivityPresenterImpl> implements OwnerProfileActivityView{
    private Toolbar toolbar;
    private TextView toolbarTitle;
    @Override
    protected OwnerProfileActivityPresenterImpl createPresenter() {
        return new OwnerProfileActivityPresenterImpl(this,this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_profile);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initUI() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        (getSupportFragmentManager().findFragmentById(R.id.profile_container)).
                onActivityResult(requestCode,resultCode,data);
    }

    @Override
    public Toolbar getToolbarView() {
        return toolbar;
    }

    @Override
    public TextView getToolbarTitleView() {
        return toolbarTitle;
    }
}
