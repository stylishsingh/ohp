package com.ohp.interfaces;

import com.ohp.enums.ScreenNavigation;

/**
 * @author Amanpal Singh.
 */

public interface OnItemClickListener {


    void onItemClick(int layoutPosition, ScreenNavigation click);
}

