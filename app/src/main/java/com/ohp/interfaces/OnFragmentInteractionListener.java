package com.ohp.interfaces;

import android.os.Bundle;

import com.ohp.enums.CurrentScreen;

/**
 * @author Amanpal Singh
 */

public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    void navigateTo(CurrentScreen tag, Bundle bundle);


//    void setOrderPagerAdapter();
}