package com.ohp.interfaces;

import java.util.List;

/**
 * @author Amanpal Singh.
 */

public interface AdapterPresenter<T, M> {

    T initAdapter();

    void updateList(List<M> filterList);

}
