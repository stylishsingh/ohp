package com.ohp.serviceproviderviews.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.serviceproviderviews.adapter.AllWonProjectViewAdapter;
import com.ohp.serviceproviderviews.beans.GetAllProjectsServiceProviderResponse;
import com.ohp.serviceproviderviews.view.AllWonViewProjectView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * Created by vishal.sharma on 7/28/2017.
 */

public class AllWonViewProjectPresenterImpl extends FragmentPresenter<AllWonViewProjectView, APIHandler> implements APIResponseInterface.OnCallableResponse,View.OnClickListener {
    public boolean isFilterChanged = false, isSearched = false;

    private List<GetAllProjectsServiceProviderResponse.DataBean.WonProjectsBean> listallWonProjectsResponses=new ArrayList<>();
    private GetAllProjectsServiceProviderResponse allWonProjectsResponse;
    private AllWonProjectViewAdapter allWonProjectViewAdapter;
    private String searchQuery = "";  //for search query
    public int page = 1;
    public AllWonViewProjectPresenterImpl(AllWonViewProjectView allWonViewProjectView, Context context) {
        super(allWonViewProjectView, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {

            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            getView().getToolbarTitle().setText("Won Projects");
            getView().getToolbar().setVisibility(View.VISIBLE);
            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    ((HolderActivity) getActivity()).oneStepBack();
                }
            });



            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getRecyclerWonProjectList().setLayoutManager(lm);
            getView().getRecyclerWonProjectList().setNestedScrollingEnabled(false);
            allWonProjectViewAdapter=new AllWonProjectViewAdapter(getActivity(),null,this);
            getView().getRecyclerWonProjectList().setAdapter(allWonProjectViewAdapter);
            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

                            if (allWonProjectsResponse != null && allWonProjectsResponse.getTotalRecord() > listallWonProjectsResponses.size()) {

                                System.out.println("total Records" + allWonProjectsResponse.getTotalRecord());
                                page += 1;
                                getAllWonProjects();
                            }

                        }
                    }
                }
            });
            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });
            getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s != null && s.length() > 0) {
                        getView().getCloseBtn().setVisibility(View.VISIBLE);
                        searchQuery = s.toString();
                        page = 1;
                        isSearched = true;
                        getAllWonProjects();

                    } else {
                        searchQuery = "";
                        page = 1;
                        isSearched = true;
                        getAllWonProjects();
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        getView().getCloseBtn().setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            getAllWonProjects();

        }

    }

    private void getAllWonProjects() {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        if (Utils.getInstance().isInternetAvailable(getActivity())) {
                            if (TextUtils.isEmpty(searchQuery))
                            getActivity().showProgressDialog();
                            Map<String, String> params = new HashMap<>();
                            params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                           /* if (!TextUtils.isEmpty(OwnerDashboardActivity.sort_order))
                                params.put(Constants.SORT_ORDER, "DESC");*/
                            if (!TextUtils.isEmpty(searchQuery))
                                params.put(Constants.SEARCH_PARAM, searchQuery);
                            params.put(Constants.PAGE_PARAM, String.valueOf(page));
                            params.put(Constants.PROJECT_TYPE, Constants.PROJECT_TYPE_WON);
                            getInterActor().getAllProjectsServiceProvider(params, ApiName.GET_PROJECTS);

                        } else {
                            getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    getActivity().hideProgressDialog();
                }
                switch (api) {
                    case GET_PROJECTS:
                        getActivity().hideProgressDialog();
                        if (response.isSuccessful()) {
                            allWonProjectsResponse = (GetAllProjectsServiceProviderResponse) response.body();
                            switch (allWonProjectsResponse.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(allWonProjectsResponse);
                                    break;
                                case Constants.STATUS_201:
                                    getActivity().showSnakBar(getView().getRootView(), allWonProjectsResponse.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((OwnerDashboardActivity) getActivity()).logout(allWonProjectsResponse.getMessage());
                                    break;
                            }
                        }
                        break;
                    case DELETE_PROJECT:

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void onSuccess(GetAllProjectsServiceProviderResponse allWonProjectsResponse) {
        if(getActivity()!=null && isViewAttached()){
            if(allWonProjectsResponse.getData().getWonProjects().size()>0){
                getView().getEmptyLayout().setVisibility(View.GONE);
                if (isSearched) {
                    listallWonProjectsResponses = new ArrayList<>();
                    listallWonProjectsResponses.addAll(allWonProjectsResponse.getData().getWonProjects());
                    isSearched = false;
                } else if (allWonProjectsResponse.getTotalRecord() > listallWonProjectsResponses.size()) {
                    listallWonProjectsResponses.addAll(allWonProjectsResponse.getData().getWonProjects());
                }
                allWonProjectViewAdapter.updateList(listallWonProjectsResponses);
            }else{
                if (listallWonProjectsResponses.isEmpty()) {
                    allWonProjectViewAdapter.clearAll();
                    getView().getEmptyLayout().setVisibility(View.VISIBLE);
                } else {
                    if (isSearched || searchQuery.length() != 0) {
                        allWonProjectViewAdapter.clearAll();
                        getView().getEmptyLayout().setVisibility(View.VISIBLE);
                    }
                }
            }
        }


    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.btn_clear:
                    searchQuery = "";
                    page = 1;
                    getView().getSearchEditText().setText("");
//                    propertiesList.clearAll();
//                    adapter.update(propertiesList);
//                    getPropertiesList();
                    break;
            }
        }
    }
}
