package com.ohp.serviceproviderviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.ThumbImageAdapter;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.beans.ProjectDetailResponse;
import com.ohp.commonviews.beans.ThumbnailBean;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.serviceproviderviews.adapter.ProjectTaskAdapter;
import com.ohp.serviceproviderviews.adapter.ResourceServiceAdapter;
import com.ohp.serviceproviderviews.beans.AddEditBidResponse;
import com.ohp.serviceproviderviews.view.ProjectDetailServiceView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static com.ohp.utils.Constants.BROWSE_PROJECTS;
import static com.ohp.utils.Constants.CLICK_EDIT_BID;
import static com.ohp.utils.Constants.CLICK_VIEW_BID;
import static com.ohp.utils.Constants.NAVIGATE_FROM;
import static com.ohp.utils.Constants.PROJECT_DETAIL_SERVICE_PROVIDER;
import static com.ohp.utils.Constants.PROJECT_DETAIL_WON_PROJECTS;

/**
 * @author Vishal Sharma.
 */

public class ProjectDetailServiceFragmentPresenterImpl extends FragmentPresenter<ProjectDetailServiceView, APIHandler> implements APIHandler.OnCallableResponse, View.OnClickListener {

    private String currentScreen = "", bidID = "", Notes = "";
    private boolean bgApp = false;
    private ResourceServiceAdapter resourceAdapter;
    private ThumbImageAdapter detailImageAdapter, applianceImageAdapter, applianceDocAdapter;
    private ProjectTaskAdapter taskCompletedAdapter, taskOnGoingAdapter;
    private List<ProjectDetailResponse.DataBean.ProjectTasksBean.TaskBean> listCompletedTask = new ArrayList<>(), listOnGoingTask = new ArrayList<>();
    private String projectID;
    private final BroadcastReceiver projectDetailReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getExtras().containsKey(Constants.NAVIGATE_FROM)
                        && Constants.NOTIFICATIONS.equalsIgnoreCase(intent.getExtras().getString(NAVIGATE_FROM))) {
                    if (projectID.equalsIgnoreCase(intent.getExtras().getString(Constants.PROJECT_ID)))
                        getProjectDetailServiceProvider(projectID);
                } else
                    getProjectDetailServiceProvider(projectID);


                Intent listIntent = new Intent(Constants.GET_PROJECTS);
                context.sendBroadcast(listIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };
    private String addNotes = "", bidAmount = "";

    public ProjectDetailServiceFragmentPresenterImpl(ProjectDetailServiceView projectDetailServiceView, Context context) {
        super(projectDetailServiceView, context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null && isViewAttached()) {
            getActivity().unregisterReceiver(projectDetailReceiver);
        }
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {

            getActivity().registerReceiver(projectDetailReceiver, new IntentFilter(Constants.PROJECT_DETAIL));

            //show Toolbar
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            getView().getToolbarTitle().setText(R.string.title_project_details);
            getView().getToolbar().setVisibility(View.VISIBLE);
            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    if (bgApp)
                        ((HolderActivity) getActivity()).moveToParentActivity();
                    else
                        ((HolderActivity) getActivity()).oneStepBack();
                }
            });


            //Add Dummy Data to resource recyclerView
            LinearLayoutManager lm = new LinearLayoutManager(getActivity());
            lm.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getResourceRecycler().setLayoutManager(lm);
            resourceAdapter = new ResourceServiceAdapter(getActivity(), null, this);
            getView().getResourceRecycler().setAdapter(resourceAdapter);

            // project detail images

            LinearLayoutManager lm1 = new LinearLayoutManager(getActivity());
            lm1.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getProjectDetailRecycler().setLayoutManager(lm1);

            LinearLayoutManager llApplianceImages = new LinearLayoutManager(getActivity());
            llApplianceImages.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getRVApplianceImages().setLayoutManager(llApplianceImages);

            LinearLayoutManager llApplianceDocs = new LinearLayoutManager(getActivity());
            llApplianceDocs.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getRVApplianceDocs().setLayoutManager(llApplianceDocs);


            detailImageAdapter = new ThumbImageAdapter(getActivity(), null, this, false, false);
            getView().getProjectDetailRecycler().setAdapter(detailImageAdapter);

            /*applianceImageAdapter = new ThumbImageAdapter(getActivity(), null, this, false, false);
            getView().getRVApplianceImages().setAdapter(applianceImageAdapter);

            applianceDocAdapter = new ThumbImageAdapter(getActivity(), null, this, false, false);
            getView().getRVApplianceDocs().setAdapter(applianceDocAdapter);*/

            taskOnGoingAdapter = new ProjectTaskAdapter(getActivity(), listOnGoingTask);
            getView().getRVApplianceImages().setAdapter(taskOnGoingAdapter);

            taskCompletedAdapter = new ProjectTaskAdapter(getActivity(), listCompletedTask);
            getView().getRVApplianceDocs().setAdapter(taskCompletedAdapter);


        }

    }

    public void saveBundleInfo(Bundle bundle) {
        try {
            if (getActivity() != null && isViewAttached() && bundle != null) {
                currentScreen = bundle.getString(Constants.DESTINATION, "");
                projectID = bundle.getString(Constants.PROJECT_ID, "");
                bgApp = bundle.getBoolean(Constants.BG_APP);
                bidID = bundle.getString(Constants.BID_ID, "");
                switch (currentScreen) {
                    case CLICK_VIEW_BID:
                        getView().getToolbarTitle().setText(R.string.Project_Detail);
                        getView().getImageEdit().setVisibility(View.VISIBLE);
                        getView().hitBidAmount().setVisibility(View.GONE);
                        break;
                    case CLICK_EDIT_BID:
                        /*getView().setBidAmount().requestFocus();
                        getView().setBidAmount().setFocusableInTouchMode(true);*/
                        getView().getImageEdit().setVisibility(View.VISIBLE);
                        getView().hitBidAmount().setVisibility(View.GONE);
                        getView().getToolbarTitle().setText(R.string.Project_Detail);
                        break;
                    case BROWSE_PROJECTS:
                       /* getView().setBidAmount().requestFocus();
                        getView().setBidAmount().setFocusableInTouchMode(true);*/
                        getView().hitBidAmount().setVisibility(View.GONE);
                        getView().getImageEdit().setVisibility(View.VISIBLE);
                        break;
                    case PROJECT_DETAIL_SERVICE_PROVIDER:
                        // getView().setBidAmount().requestFocus();
                        getView().setBidAmount().setFocusableInTouchMode(false);
                        getView().hitBidAmount().setVisibility(View.GONE);
                        getView().getImageEdit().setVisibility(View.GONE);
                        getView().setBidText().setText("Bid (USD)");
                        break;
                    case PROJECT_DETAIL_WON_PROJECTS:
                       /* getView().setBidAmount().requestFocus();
                        getView().setBidAmount().setFocusableInTouchMode(true);*/
                        getView().hitBidAmount().setVisibility(View.GONE);
                        getView().setBidText().setText("Bid (USD)");
                        break;


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getProjectDetailServiceProvider(String projectID) {
        try {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> params = new HashMap<>();
                params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                params.put(Constants.PROJECT_ID, projectID);
                getInterActor().getProjectDetailServiceProvider(params, ApiName.GET_PROJECT_DETAIL);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addNotes() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_notes, null);
        final TextInputEditText notes = (TextInputEditText) view.findViewById(R.id.et_note);

        final TextInputEditText bidAmount = (TextInputEditText) view.findViewById(R.id.et_bid_amount);

        if (!this.bidAmount.equalsIgnoreCase("0"))
            bidAmount.setText(this.bidAmount);

        notes.setText(addNotes);
        bidAmount.requestFocus();

        TextView txt_save = (TextView) view.findViewById(R.id.txt_save);
        TextView txt_cancel = (TextView) view.findViewById(R.id.txt_cancel);
        builder.setCustomTitle(view);
        final AlertDialog alert = builder.create();
        txt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProjectDetailServiceFragmentPresenterImpl.this.bidAmount = bidAmount.getText().toString();
                if (ProjectDetailServiceFragmentPresenterImpl.this.bidAmount.length() == 0) {
                    getActivity().showSnackBar(getView().getRootView(), "Please enter your bid.");
                } else if (Integer.parseInt(ProjectDetailServiceFragmentPresenterImpl.this.bidAmount) == 0) {
                    getActivity().showSnackBar(getView().getRootView(), "Bid amount should be more than 0.");
                } else {
                    addNotes = notes.getText().toString();
                    ProjectDetailServiceFragmentPresenterImpl.this.bidAmount = bidAmount.getText().toString();
                    Utils.getInstance().hideSoftKeyboard(getActivity(), notes);
                    addEditBid();
                    alert.dismiss();
                }
            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), notes);
                alert.dismiss();
            }
        });
        alert.setCancelable(false);
        alert.show();
        if (alert.getWindow() != null) {
            alert.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    getActivity().hideProgressDialog();
                }
                switch (api) {
                    case GET_PROJECT_DETAIL:
                        if (response.isSuccessful()) {
                            ProjectDetailResponse dataResponse = (ProjectDetailResponse) response.body();
                            switch (dataResponse.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(dataResponse);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(dataResponse.getMessage());
                                    break;
                            }
                        }
                        break;
                    case ADD_EDIT_BID:
                        if (response.isSuccessful()) {
                            AddEditBidResponse addEditBidResponse = (AddEditBidResponse) response.body();
                            switch (addEditBidResponse.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(addEditBidResponse);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(addEditBidResponse.getMessage());
                                    break;
                            }
                        }
                        break;

                    case MARK_COMPLETED_PROJECT:
                        if (response.isSuccessful()) {
                            GenericBean responseObj = (GenericBean) response.body();
                            switch (responseObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(responseObj);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onSuccess(GenericBean responseObj) {
        if (getActivity() != null && isViewAttached()) {
            getView().getTVMarkCompletedProject().setVisibility(View.GONE);
        }
    }

    private void onSuccess(AddEditBidResponse addEditBidResponse) {
        if (getActivity() != null && isViewAttached()) {
            Intent intent = new Intent(Constants.BROADCAST_FOR_BIDS);
            getActivity().sendBroadcast(intent);

            Intent intent1 = new Intent(Constants.BROADCAST_FOR_NEAR_PROJECTS);
            getActivity().sendBroadcast(intent1);

            getProjectIdDetail(getView().getProjectId());
        }

//        ((HolderActivity) getActivity()).oneStepBack();
    }

    private void onSuccess(ProjectDetailResponse dataObj) {
        try {
            if (getActivity() != null && isViewAttached() && dataObj.getData() != null) {
                // for property detail...
                switch (dataObj.getData().getMyBid().getBidStatus()) {
                    case 0:
                        getView().getImageEdit().setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        getView().getImageEdit().setVisibility(View.GONE);
                        break;
                    case 2:
                        getView().getImageEdit().setVisibility(View.GONE);
                        break;
                }
                switch (dataObj.getData().getProjectStatus()) {
                    case 1:
                        getView().getImageEdit().setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        if (!currentScreen.equals(CLICK_VIEW_BID))
                            getView().getTVMarkCompletedProject().setVisibility(View.VISIBLE);
                        getView().getImageEdit().setVisibility(View.GONE);
                        break;
                    case 3:
                        getView().getImageEdit().setVisibility(View.GONE);
                        break;
                }

                bidID = String.valueOf(dataObj.getData().getMyBid().getBidId());
                // For Bid detail...///
                getView().setBidAmount().setText(String.valueOf(dataObj.getData().getMyBid().getBidAmount()));
                bidAmount = String.valueOf(dataObj.getData().getMyBid().getBidAmount());
                addNotes = dataObj.getData().getMyBid().getBidNote();

                if (currentScreen.equals(CLICK_VIEW_BID) &&
                        dataObj.getData().getMyBid().getBidStatus() == 2) {
                    getView().getImageEdit().setVisibility(View.GONE);
                } else {
                    if (dataObj.getData().getProjectStatus() == 1 && dataObj.getData().getMyBid().getBidStatus() == 2) {
                        getView().getImageEdit().setVisibility(View.VISIBLE);
                        getView().setBidAmount().setText("0");
                        bidAmount = "";
                        addNotes = "";
                        bidID = "";
                    }
                }


                getView().setPropertyOwnerName().setText(dataObj.getData().getProjectOwner().getFullName());
                getView().setPropertyOwnerAddress().setText(dataObj.getData().getProjectOwner().getAddress());
                getView().setPropertyName().setText(dataObj.getData().getProperty().getPropertyName());
                getView().setPropertyAddress().setText(dataObj.getData().getProperty().getPropertyAddress());
                getView().setPropertyDescription().setText(dataObj.getData().getProperty().getPropertyDescription());
                getView().setPropertySqFtArea().setText(dataObj.getData().getProperty().getPropertyArea());
                getView().setPropertyConstYear().setText(dataObj.getData().getProperty().getConstructionYear());

                if (dataObj.getData().getProjectOwner().getGender().equalsIgnoreCase("male")) {
                    getView().setPropertyOwnerName().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_male_small, 0, 0, 0);
                } else {
                    getView().setPropertyOwnerName().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_female_small, 0, 0, 0);
                }

                if (!TextUtils.isEmpty(dataObj.getData().getProperty().getPropertyImage()))
                    Picasso.with(getContext()).load(dataObj.getData().getProperty().getPropertyImage()).resize(400, 400).centerCrop().into(getView().setPropertyImage());

                if (dataObj.getData().getProperty().getResource() != null && dataObj.getData().getProperty().getResource().size() > 0) {
                    resourceAdapter.updateResource(dataObj.getData().getProperty().getResource());
                }
                getView().getResourceRecycler().setAdapter(resourceAdapter);

                //  for project detail...
                getView().setProjectName().setText(dataObj.getData().getProjectName());
                getView().setProjectJobType().setText(dataObj.getData().getProjectJobtype().getJobtypeTitle().equalsIgnoreCase("") ? " " : dataObj.getData().getProjectJobtype().getJobtypeTitle());
                getView().setProjectCreatedOn().setText(Utils.getInstance().getYMDProjectFormattedDateTime(dataObj.getData().getCreated().replace("+00:00", "")));

                // for project expertise..
                StringBuilder expertise = new StringBuilder();
                if (dataObj.getData().getProjectExpertise().size() > 0) {
                    for (int i = 0; i < dataObj.getData().getProjectExpertise().size(); i++) {


                        if (i < (dataObj.getData().getProjectExpertise().size() - 1)) {
                            expertise.append(dataObj.getData().getProjectExpertise().get(i).getExpertiseTitle()).append(", ");
                        } else {
                            expertise.append(dataObj.getData().getProjectExpertise().get(i).getExpertiseTitle());
                        }
                    }
                }
                getView().setProjectExpertise().setText(expertise);

                getView().setProjectDescription().setText(TextUtils.isEmpty(dataObj.getData().getProjectDescription())
                        ? " " : dataObj.getData().getProjectDescription());

                // for project appliance ..
                getView().setApplianceName().setText(TextUtils.isEmpty(dataObj.getData().getProjectAppliance().getApplianceName())
                        ? " " : dataObj.getData().getProjectAppliance().getApplianceName());

                getView().setApplianceBrand().setText(TextUtils.isEmpty(dataObj.getData().getProjectAppliance().getApplianceBrand())
                        ? " " : dataObj.getData().getProjectAppliance().getApplianceBrand());


                getView().setApplianceWarranty().setText(TextUtils.isEmpty(dataObj.getData().getProjectAppliance().getExtendedWarrantyDate())
                        ? " " : dataObj.getData().getProjectAppliance().getExtendedWarrantyDate() + " yrs");


                getView().setAppliancePurchaseDate().setText(TextUtils.isEmpty(dataObj.getData().getProjectAppliance().getPurchaseDate())
                        ? " " : Utils.getInstance().getYMDProjectFormattedDateTime(dataObj.getData().getProjectAppliance().getPurchaseDate().replace("+00:00", "")));


                getView().setApplianceExpirationDate().setText(TextUtils.isEmpty(dataObj.getData().getProjectAppliance().getWarrantyExpirationDate())
                        ? " " : Utils.getInstance().getYMDProjectFormattedDateTime(dataObj.getData().getProjectAppliance().getWarrantyExpirationDate().replace("+00:00", "")));

                List<ThumbnailBean> detailImageList = new ArrayList<>();
                if (dataObj.getData().getProjectImages().size() > 0) {
                    for (int i = 0; i < dataObj.getData().getProjectImages().size(); i++) {
                        ThumbnailBean obj = new ThumbnailBean();
                        obj.setAddView(false);
                        obj.setImageID(String.valueOf(dataObj.getData().getProjectImages().get(i).getProjectImageId()));
                        obj.setFile(false);
                        obj.setCheck(ScreenNavigation.OLD_ITEM);
                        obj.setImagePath(dataObj.getData().getProjectImages().get(i).getProjectImageAvatar());
                        detailImageList.add(obj);
                    }
                    detailImageAdapter.updateList(detailImageList);
                    getView().NoProjectImages().setVisibility(View.GONE);
                } else {
                    detailImageAdapter.updateList(detailImageList);
                    getView().NoProjectImages().setVisibility(View.VISIBLE);
                }

            }

            switch (currentScreen) {
                case CLICK_EDIT_BID:
                case BROWSE_PROJECTS:
                case CLICK_VIEW_BID:
                    getView().getCVApplianceDocView().setVisibility(View.GONE);
                    getView().getCVApplianceImageView().setVisibility(View.GONE);
                    break;
                    /* List<ThumbnailBean> uploadImageList = new ArrayList<>();
                    if (!dataObj.getData().getProjectAppliance().getPropertyApplianceImages().isEmpty()) {
                        ThumbnailBean obj;
                        for (int i = 0; i < dataObj.getData().getProjectAppliance().getPropertyApplianceImages().size(); i++) {
                            obj = new ThumbnailBean();
                            obj.setExtension("jpg");
                            obj.setImagePath(dataObj.getData().getProjectAppliance().getPropertyApplianceImages().get(i).getApplianceAvatar());
                            obj.setName("server side images");
                            obj.setFile(false);
                            obj.setAddView(false);
                            obj.setCheck(ScreenNavigation.OLD_ITEM);
                            obj.setImageID(String.valueOf(dataObj.getData().getProjectAppliance().getPropertyApplianceImages().get(i).getApplianceImageId()));
                            uploadImageList.add(obj);
                        }

                        applianceImageAdapter.updateList(uploadImageList);
                        getView().getTVNoApplianceImages().setVisibility(View.GONE);
                        *//*ViewPagerSubCatAdapter imgAdapter = new ViewPagerSubCatAdapter(getActivity(), uploadImageList);

                        // Binds the Adapter to the ViewPager
                        getView().getImageViewPager().setAdapter(imgAdapter);

                        imgAdapter.notifyDataSetChanged();

                        getView().getImgIndicator().setViewPager(getView().getImageViewPager());*//*

                    } else {
                        applianceImageAdapter.updateList(uploadImageList);
                        getView().getTVNoApplianceImages().setVisibility(View.VISIBLE);
                    }

                    List<ThumbnailBean> uploadFileList = new ArrayList<>();
                    if (!dataObj.getData().getProjectAppliance().getPropertyApplianceDoc().isEmpty()) {
                        ThumbnailBean obj;
                        for (int i = 0; i < dataObj.getData().getProjectAppliance().getPropertyApplianceDoc().size(); i++) {
                            obj = new ThumbnailBean();
                            obj.setExtension("jpg");
                            obj.setImagePath(dataObj.getData().getProjectAppliance().getPropertyApplianceDoc().get(i).getApplianceDocAvatar());
                            obj.setName("server side images");
                            obj.setFile(false);
                            obj.setAddView(false);
                            obj.setCheck(ScreenNavigation.OLD_ITEM);
                            obj.setImageID(String.valueOf(dataObj.getData().getProjectAppliance().getPropertyApplianceDoc().get(i).getApplianceDocId()));
                            uploadFileList.add(obj);
                        }

                        applianceDocAdapter.updateList(uploadFileList);
                        getView().getTVNoApplianceDocs().setVisibility(View.GONE);

               *//* ViewPagerSubCatAdapter docsAdapter = new ViewPagerSubCatAdapter(getActivity(), uploadFileList);
                // Binds the Adapter to the ViewPager
                getView().getDocsViewPager().setAdapter(docsAdapter);

                docsAdapter.notifyDataSetChanged();

                getView().getDocsIndicator().setViewPager(getView().getDocsViewPager());*//*
                    } else {
                        applianceDocAdapter.updateList(uploadFileList);
                        getView().getTVNoApplianceDocs().setVisibility(View.VISIBLE);
                    }

                    break;*/

                case PROJECT_DETAIL_SERVICE_PROVIDER:
                case PROJECT_DETAIL_WON_PROJECTS:
                    if (dataObj.getData().getProjectTasks() != null) {
                        if (!dataObj.getData().getProjectTasks().getOngoingTasks().isEmpty()) {
                            listOnGoingTask = dataObj.getData().getProjectTasks().getOngoingTasks();
                            taskOnGoingAdapter.update(listOnGoingTask);
                        } else {
                            taskOnGoingAdapter.update(listOnGoingTask);
                            getView().getTVNoApplianceImages().setVisibility(View.VISIBLE);
                        }

                        if (!dataObj.getData().getProjectTasks().getCompletedTasks().isEmpty()) {
                            listCompletedTask = dataObj.getData().getProjectTasks().getCompletedTasks();
                            taskCompletedAdapter.update(listCompletedTask);
                        } else {
                            taskCompletedAdapter.update(listCompletedTask);
                            getView().getTVNoApplianceDocs().setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    public void GoneUiForEditBid() {
        getView().hitBidAmount().setVisibility(View.GONE);
        getView().setLinearAddNotes().setVisibility(View.GONE);
        getView().setBidText().setText("Amount");
    }

    public void GoneUiForSaveBid() {
        getView().hitBidAmount().setVisibility(View.VISIBLE);
        //  getView().setBidText().setFocusable(true);
        getView().setLinearAddNotes().setVisibility(View.VISIBLE);
    }

    public void getProjectIdDetail(String project_id) {
        projectID = project_id;
        getProjectDetailServiceProvider(project_id);
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.img_edit:
                    addNotes();
                    getView().getImageEdit().setVisibility(View.VISIBLE);
                    break;
                case R.id.tv_mark_completed:
                    markCompletedProject();
                    break;
            }
        }
    }

    public void addEditBid() {
        try {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> params = new HashMap<>();
                params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                if (!bidID.equalsIgnoreCase("0"))
                    params.put(Constants.BID_ID, bidID);
                params.put(Constants.PROJECT_ID, projectID);
                params.put(Constants.BID_Amount, bidAmount);
                params.put(Constants.BID_NOTE, addNotes);
                getInterActor().getAddEditBidServiceProvider(params, ApiName.ADD_EDIT_BID);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void markCompletedProject() {
        try {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> params = new HashMap<>();
                params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                params.put(Constants.PROJECT_ID, projectID);
                getInterActor().markCompletedProject(params, ApiName.MARK_COMPLETED_PROJECT);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
