package com.ohp.serviceproviderviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.interfaces.AdapterPresenter;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.serviceproviderviews.activities.SPDashboardActivity;
import com.ohp.serviceproviderviews.adapter.TeamMemberListAdapter;
import com.ohp.serviceproviderviews.beans.TeamMembersModel;
import com.ohp.serviceproviderviews.view.TeamMembersView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * @author Amanpal Singh.
 */

public class TeamMembersFragmentPresenterImpl extends FragmentPresenter<TeamMembersView, APIHandler>
        implements APIResponseInterface.OnCallableResponse, View.OnClickListener, AdapterPresenter<TeamMemberListAdapter, TeamMembersModel> {

    private String searchQuery = "";
    private int page = 1;
    private final BroadcastReceiver teamMemberReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            page = 1;
            listTeamMembers.clear();

            adapter.update(listTeamMembers);
            getTeamMembers();
        }
    };
    private TeamMemberListAdapter adapter;
    private boolean isSearched = false, isFilterChanged = false;
    private LinearLayoutManager linearLayoutManager;
    private List<TeamMembersModel.DataBean> listTeamMembers = new ArrayList<>();
    private TeamMembersModel listModel;
    private int deletedMemberPosition = 0;

    public TeamMembersFragmentPresenterImpl(TeamMembersView teamMembersView, Context context) {
        super(teamMembersView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(teamMemberReceiver, new IntentFilter(Constants.GET_TEAM_MEMBERS));


            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });

            getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(final CharSequence s, int start, int before, int count) {
                    if (s != null && s.length() > 0) {
                        getView().getCloseBtn().setVisibility(View.VISIBLE);

                        searchQuery = s.toString();
                        page = 1;
                        isSearched = true;
                        getTeamMembers();


                    } else {
                        searchQuery = "";
                        page = 1;
                        isSearched = true;
                        listTeamMembers.clear();
                        adapter.update(listTeamMembers);
                        getTeamMembers();
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        getView().getCloseBtn().setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            getTeamMembers();
        }
    }

    @Override
    public void onDestroy() {
        if (getActivity() != null && isViewAttached())
            getActivity().unregisterReceiver(teamMemberReceiver);
        super.onDestroy();

    }

    private void getTeamMembers() {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    if (TextUtils.isEmpty(searchQuery))
                        getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    if (!TextUtils.isEmpty(searchQuery))
                        param.put(Constants.SEARCH_PARAM, searchQuery);
                    param.put(Constants.PAGE_PARAM, String.valueOf(page));
                    getInterActor().getTeamMembers(param, ApiName.TEAM_MEMBERS_API);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case TEAM_MEMBERS_API:
                        if (response.isSuccessful()) {
                            listModel = (TeamMembersModel) response.body();
                            switch (listModel.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(listModel);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((SPDashboardActivity) getActivity()).logout(listModel.getMessage());
                                    break;
                            }
                        }
                        break;
                    case DELETE_TEAM_LIST:
                        if (response.isSuccessful()) {
                            GenericBean genericBean = (GenericBean) response.body();
                            switch (genericBean.getStatus()) {
                                case Constants.STATUS_200:
                                    if (adapter != null) {
                                        listTeamMembers.remove(deletedMemberPosition);
                                        adapter.deleteItem(deletedMemberPosition);
                                    }
                                    if (listTeamMembers != null && listTeamMembers.size() > 0) {
                                        getView().getEmptyView().setVisibility(View.GONE);
                                    } else {
                                        getView().getEmptyView().setVisibility(View.VISIBLE);
                                    }
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((OwnerDashboardActivity) getActivity()).logout(genericBean.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(TeamMembersModel listModel) {
        if (getActivity() != null && isViewAttached()) {
            if (listModel.getData() != null && listModel.getData().size() > 0) {
                getView().getEmptyView().setVisibility(View.GONE);
                if (listTeamMembers == null) listTeamMembers = new ArrayList<>();
                if (isSearched || searchQuery.length() != 0) {
                    listTeamMembers = new ArrayList<>();
                    listTeamMembers.addAll(listModel.getData());
                    isSearched = false;
                } else if (listModel.getTotalRecord() > listTeamMembers.size()) {
                    listTeamMembers.addAll(listModel.getData());
                }
                if (isFilterChanged) {
                    listTeamMembers = listModel.getData();
                    isFilterChanged = false;
                }
                adapter.update(listTeamMembers);
            } else {
                if (listTeamMembers.isEmpty()) {
                    adapter.clearAll();
                    getView().getEmptyView().setVisibility(View.VISIBLE);
                } else {
                    if (isSearched || searchQuery.length() != 0) {
                        adapter.clearAll();
                        getView().getEmptyView().setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.btn_clear:
                    searchQuery = "";
                    page = 1;
                    getView().getSearchEditText().setText("");
                    break;
                case R.id.fab_add:
                    Intent intent = new Intent(getActivity(), HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_ADD_TEAM);
                    getActivity().startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public TeamMemberListAdapter initAdapter() {
        if (getActivity() != null && isViewAttached()) {
            adapter = new TeamMemberListAdapter(getActivity(), listTeamMembers, this);
            getView().getRecyclerView().setAdapter(adapter);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getRecyclerView().setLayoutManager(linearLayoutManager);
            getView().getRecyclerView().setNestedScrollingEnabled(false);

            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

                            if (listTeamMembers != null && listModel.getTotalRecord() > listTeamMembers.size()) {
                                page += 1;
                                getTeamMembers();
                            }
                        }
                    }
                }
            });
        }
        return adapter;
    }

    @Override
    public void updateList(List<TeamMembersModel> filterList) {

    }

    public void onItemClicked(final int layoutPosition, ScreenNavigation click) {
        Intent intent = new Intent(getActivity(), HolderActivity.class);
        switch (click) {
            /*case DETAIL_ITEM:
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_TEAM);
                intent.putExtra(Constants.MEMBER_ID, String.valueOf(listTeamMembers.get(layoutPosition).getUserId()));
                getActivity().startActivity(intent);
                break;*/
            case EDIT_ITEM:
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_EDIT_TEAM);
                intent.putExtra(Constants.MEMBER_ID, String.valueOf(listTeamMembers.get(layoutPosition).getUserId()));
                intent.putExtra(Constants.TEAM_MEMBER_OBJ, listTeamMembers.get(layoutPosition));
                getActivity().startActivity(intent);
                break;
            case DELETE_ITEM:
                Utils.getInstance().confirmDialog(getActivity(), "Are you sure, you want to delete this team member?", "delete", new Utils.ConfirmDialogCallbackInterface() {
                    @Override
                    public void onYesClick(String tag) {
                        deletedMemberPosition = layoutPosition;
                        deleteTeamMemberID(String.valueOf(listTeamMembers.get(layoutPosition).getUserId()));
                    }

                    @Override
                    public void onNoClick(String tag) {

                    }
                });

                break;
        }
    }

    public void onItemClicked(int layoutPosition, ScreenNavigation click, ImageView ivAvatar) {
        Intent intent = new Intent(getActivity(), HolderActivity.class);
        switch (click) {
            case DETAIL_ITEM:
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_TEAM);
                intent.putExtra(Constants.MEMBER_ID, String.valueOf(listTeamMembers.get(layoutPosition).getUserId()));
                intent.putExtra(Constants.TEAM_MEMBER_OBJ, listTeamMembers.get(layoutPosition));
                getActivity().startActivity(intent);
                break;
        }
    }

    private void deleteTeamMemberID(String teamID) {
        try {
            if (getActivity() != null) {
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.MEMBER_ID, teamID);
                getInterActor().deleteTeamMember(param, ApiName.DELETE_TEAM_LIST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
