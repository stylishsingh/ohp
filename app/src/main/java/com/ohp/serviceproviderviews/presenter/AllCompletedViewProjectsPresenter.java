package com.ohp.serviceproviderviews.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.serviceproviderviews.adapter.AllCompletedViewAdapter;
import com.ohp.serviceproviderviews.beans.GetAllProjectsServiceProviderResponse;
import com.ohp.serviceproviderviews.view.AllCompletedViewProjectsView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * Created by vishal.sharma on 7/31/2017.
 */

public class AllCompletedViewProjectsPresenter extends FragmentPresenter<AllCompletedViewProjectsView, APIHandler> implements APIResponseInterface.OnCallableResponse, View.OnClickListener {
    public boolean isFilterChanged = false, isSearched = false;
    private int page = 1;
    private String searchQuery = "";  //for search query
    private List<GetAllProjectsServiceProviderResponse.DataBean.CompletedProjectsBean> listallWonProjectsResponses = new ArrayList<>();
    private GetAllProjectsServiceProviderResponse allWonProjectsResponse;
    private AllCompletedViewAdapter allCompletedViewAdapter;

    public AllCompletedViewProjectsPresenter(AllCompletedViewProjectsView allCompletedViewProjectsView, Context context) {
        super(allCompletedViewProjectsView, context);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {

            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            getView().getToolbarTitle().setText("Completed Projects");
            getView().getToolbar().setVisibility(View.VISIBLE);
            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    ((HolderActivity) getActivity()).oneStepBack();
                }
            });


            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getRecyclerCompletedProjectList().setLayoutManager(lm);
            getView().getRecyclerCompletedProjectList().setNestedScrollingEnabled(false);

            allCompletedViewAdapter = new AllCompletedViewAdapter(getActivity(), null, this);
            getView().getRecyclerCompletedProjectList().setAdapter(allCompletedViewAdapter);
        }
        getAllCompletedProjects();
    }

    private void getAllCompletedProjects() {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        if (Utils.getInstance().isInternetAvailable(getActivity())) {

                            getActivity().showProgressDialog();
                            Map<String, String> params = new HashMap<>();
                            params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                           /* if (!TextUtils.isEmpty(OwnerDashboardActivity.sort_order))
                                params.put(Constants.SORT_ORDER, "DESC");*/
                            if (!TextUtils.isEmpty(searchQuery))
                                params.put(Constants.SEARCH_PARAM, searchQuery);
                            params.put(Constants.PAGE_PARAM, String.valueOf(page));
                            params.put(Constants.PROJECT_TYPE, Constants.PROJECT_TYPE_COMPLETED);
                            getInterActor().getAllProjectsServiceProvider(params, ApiName.GET_PROJECTS);

                        } else {
                            getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    getActivity().hideProgressDialog();
                }
                switch (api) {
                    case GET_PROJECTS:
                        getActivity().hideProgressDialog();
                        if (response.isSuccessful()) {
                            allWonProjectsResponse = (GetAllProjectsServiceProviderResponse) response.body();
                            switch (allWonProjectsResponse.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(allWonProjectsResponse);
                                    break;
                                case Constants.STATUS_201:
                                    getActivity().showSnakBar(getView().getRootView(), allWonProjectsResponse.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((OwnerDashboardActivity) getActivity()).logout(allWonProjectsResponse.getMessage());
                                    break;
                            }
                        }
                        break;
                    case DELETE_PROJECT:

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onSuccess(GetAllProjectsServiceProviderResponse allCompletedProjectsResponse) {
        if (getActivity() != null && isViewAttached()) {
            if (allWonProjectsResponse.getData().getCompletedProjects().size() > 0) {
                getView().getEmptyLayout().setVisibility(View.GONE);
                if (isSearched) {
                    listallWonProjectsResponses = new ArrayList<>();
                    listallWonProjectsResponses.addAll(allCompletedProjectsResponse.getData().getCompletedProjects());
                    isSearched = false;
                } else if (allWonProjectsResponse.getTotalRecord() > listallWonProjectsResponses.size()) {
                    listallWonProjectsResponses.addAll(allWonProjectsResponse.getData().getCompletedProjects());
                } else {
                    listallWonProjectsResponses.addAll(allCompletedProjectsResponse.getData().getCompletedProjects());
                }

                if (isFilterChanged) {
                    listallWonProjectsResponses = allWonProjectsResponse.getData().getCompletedProjects();
                    isFilterChanged = false;
                }

                allCompletedViewAdapter.updateList(listallWonProjectsResponses);
            } else {
                allCompletedViewAdapter.cancelAll();
                getView().getEmptyLayout().setVisibility(View.VISIBLE);
            }
        }


    }

    @Override
    public void onFailure(Throwable t, ApiName api) {

    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.btn_clear:
                    searchQuery = "";
                    page = 1;
                    getView().getSearchEditText().setText("");
//                    propertiesList.clearAll();
//                    adapter.update(propertiesList);
//                    getPropertiesList();
                    break;
            }
        }
    }
}
