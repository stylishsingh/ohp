package com.ohp.serviceproviderviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.ProjectSpinnerAdapter;
import com.ohp.commonviews.adapter.ReminderAdapter;
import com.ohp.commonviews.adapter.TeamMemberSpinnerAdapter;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.beans.GetPropertyResponse;
import com.ohp.commonviews.beans.ResourceBean;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.beans.EventDetailModel;
import com.ohp.serviceproviderviews.beans.GetTeamMemberResponse;
import com.ohp.serviceproviderviews.view.SpAddEditEventView;
import com.ohp.utils.CalendarPicker;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;
import com.ohp.utils.Validations;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static com.ohp.enums.ApiName.ADD_EDIT_EVENT;
import static com.ohp.enums.ApiName.GET_EVENT_REMINDER_API;
import static com.ohp.utils.Constants.ADD_EDIT_PROPERTY;
import static com.ohp.utils.Constants.CLICK_SP_ADD_EVENT;
import static com.ohp.utils.Constants.CLICK_SP_EDIT_EVENT;
import static com.ohp.utils.Constants.CLICK_SP_VIEW_EVENT;
import static com.ohp.utils.Constants.NAVIGATE_FROM;

/**
 * @author Amanpal Singh.
 */

public class SpAddEditEventFragmentPresenterImpl extends FragmentPresenter<SpAddEditEventView, APIHandler> implements APIResponseInterface.OnCallableResponse, AdapterView.OnItemSelectedListener, View.OnClickListener, View.OnTouchListener {

    private final BroadcastReceiver eventDetailReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getExtras().containsKey(Constants.NAVIGATE_FROM)
                        && Constants.NOTIFICATIONS.equalsIgnoreCase(intent.getExtras().getString(NAVIGATE_FROM))) {
                    if (eventID.equalsIgnoreCase(intent.getExtras().getString(Constants.EVENT_ID)))
                        getEventDetails(eventID);
                } else
                    getEventDetails(eventID);
                Intent listIntent = new Intent(Constants.GET_EVENTS);
                context.sendBroadcast(listIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };
    public boolean isOccurrence, isEventRepeat;
    private ProjectSpinnerAdapter projectAdapter;
    private boolean bgApp = false;
    private TeamMemberSpinnerAdapter teamMemberAdapter;
    private ReminderAdapter reminderEventAdapter;
    private List<GetPropertyResponse.DataBean> listProjects = new ArrayList<>();
    private List<ResourceBean.DataBean.InfoBean> listEventReminder = new ArrayList<>();
    private List<GetTeamMemberResponse.DataBean> listTeamMember = new ArrayList<>();
    private String propertyId = "", applianceId = "", projectId = "", teamMemberId = "", eventRepeatId = "", applianceName = "", propertyName = "",
            eventTitle = "", addNotes = "", eventDateTime = "", reminderDateTime = "", eventID = "", eventTimeTo = "";
    private String currentScreen = "";

    public SpAddEditEventFragmentPresenterImpl(SpAddEditEventView addEditEventView, Context context) {
        super(addEditEventView, context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null && isViewAttached()) {
            getActivity().unregisterReceiver(eventDetailReceiver);
        }
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {

            getActivity().registerReceiver(eventDetailReceiver, new IntentFilter(Constants.GET_EVENT_DETAILS));

            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            getView().getToolbar().setVisibility(View.VISIBLE);
            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    if (bgApp)
                        ((HolderActivity) getActivity()).moveToParentActivity();
                    else
                        ((HolderActivity) getActivity()).oneStepBack();
                }
            });

            projectAdapter = new ProjectSpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listProjects);
            projectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerProjectView().setAdapter(projectAdapter);

            teamMemberAdapter = new TeamMemberSpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listTeamMember);
            teamMemberAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerTeamMemberView().setAdapter(teamMemberAdapter);


            reminderEventAdapter = new ReminderAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listEventReminder);
            reminderEventAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerRepeatEventView().setAdapter(reminderEventAdapter);

            //set scroll listener on address field
            getView().getEtAddNotesView().setOnTouchListener(new View.OnTouchListener() {  //Register a callback to be invoked when a touch event is sent to this view.
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    //Called when a touch event is dispatched to a view. This allows listeners to get a chance to respond before the target view.
                    getView().getNestedScrollView().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });

        }

    }

    public void saveBundleInfo(Bundle arguments) {
        if (getActivity() != null && isViewAttached()) {
            eventID = arguments.getString(Constants.EVENT_ID);
            bgApp = arguments.getBoolean(Constants.BG_APP);
            propertyId = arguments.getString(Constants.PROPERTY_ID, "");
            currentScreen = arguments.getString(Constants.DESTINATION, "");
            isOccurrence = arguments.getBoolean(Constants.OCCURRENCE, false);
            isEventRepeat = arguments.getBoolean(Constants.EVENT_REPEAT_EMPTY, false);
            switch (currentScreen) {
                case CLICK_SP_EDIT_EVENT:
                    getEventDetails(eventID);
                    getView().getToolbarTitleView().setText(R.string.title_edit_event);
                    enableDisableUI(true);
                    break;
                case CLICK_SP_ADD_EVENT:
                    getView().getToolbarTitleView().setText(R.string.title_add_event);
                    getView().getTILEventTitleView().setVisibility(View.VISIBLE);
                    getView().getTILEventTitleDetailView().setVisibility(View.GONE);
                    getView().getTILNotesView().setVisibility(View.VISIBLE);
                    getView().getTILNotesDetailView().setVisibility(View.GONE);
                    getView().getTILPropertyView().setVisibility(View.VISIBLE);
                    getView().getTILPropertyDetailView().setVisibility(View.GONE);
                    getView().getTILApplianceView().setVisibility(View.VISIBLE);
                    getView().getTILApplianceDetailView().setVisibility(View.GONE);
                    getEventReminder();
                    getTeamMembers();
                    getProjectsList();
                    break;
                case ADD_EDIT_PROPERTY:
                    getView().getToolbarTitleView().setText(R.string.title_add_event);
                    getEventReminder();
                    getView().getTILEventTitleView().setVisibility(View.VISIBLE);
                    getView().getTILEventTitleDetailView().setVisibility(View.GONE);
                    getView().getTILNotesView().setVisibility(View.VISIBLE);
                    getView().getTILNotesDetailView().setVisibility(View.GONE);
                    break;
                case CLICK_SP_VIEW_EVENT:
                    getEventDetails(eventID);
                    getView().getToolbarTitleView().setText(R.string.title_event_details);
                    enableDisableUI(false);
                    break;
            }
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_PROJECT_LIST:
                        if (response.isSuccessful()) {
                            GetPropertyResponse responseObj = (GetPropertyResponse) response.body();
                            switch (responseObj.getStatus()) {
                                case 200:
                                    onSuccess(responseObj, ApiName.GET_PROJECT_LIST);
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());

                                    break;
                            }
                        }
                        break;

                    case GET_TEAM_MEMBER:
                        if (response.isSuccessful()) {
                            GetTeamMemberResponse responseObj = (GetTeamMemberResponse) response.body();
                            switch (responseObj.getStatus()) {
                                case 200:
                                    onSuccess(responseObj);
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());

                                    break;
                            }
                        }
                        break;

                    case GET_EVENT_REMINDER_API:
                        if (response.isSuccessful()) {
                            ResourceBean responseObj = (ResourceBean) response.body();
                            switch (responseObj.getStatus()) {
                                case 200:
                                    onSuccess(responseObj, GET_EVENT_REMINDER_API);
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());

                                    break;
                            }
                        }
                        break;
                    case GET_EVENT_DETAIL_API:
                        if (response.isSuccessful()) {
                            EventDetailModel responseObj = (EventDetailModel) response.body();
                            switch (responseObj.getStatus()) {
                                case 200:
                                    onSuccess(responseObj);
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());
                                    break;
                            }
                            getEventReminder();
                            getTeamMembers();
                            getProjectsList();
                        }
                        break;

                    case ADD_EDIT_EVENT:
                        if (response.isSuccessful()) {
                            GenericBean responseObj = (GenericBean) response.body();
                            switch (responseObj.getStatus()) {
                                case 200:
                                    Intent listIntent;

                                    if (currentScreen.equals(ADD_EDIT_PROPERTY))
                                        listIntent = new Intent(Constants.ADD_EDIT_PROPERTY);
                                    else {
                                        listIntent = new Intent(Constants.GET_EVENTS);
                                        final String[] parseDateTime, parseDate;
                                        parseDateTime = eventDateTime.split(" ");
                                        parseDate = parseDateTime[0].split("-");

                                        listIntent.putExtra("year", Integer.parseInt(parseDate[0]));
                                        listIntent.putExtra("month", Integer.parseInt(parseDate[1]));
                                        listIntent.putExtra("day", Integer.parseInt(parseDate[2]));
                                    }
                                    getActivity().sendBroadcast(listIntent);
                                    ((HolderActivity) getActivity()).oneStepBack();
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(GetTeamMemberResponse dataObj) {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (teamMemberAdapter != null) {
                    if (getView().getEventType() != 2) {
                        getView().getSpinnerTeamMemberView().setVisibility(View.VISIBLE);
                        listTeamMember = dataObj.getData();
                        teamMemberAdapter.updateList(getActivity(), dataObj.getData());
                        for (int i = 0; i < dataObj.getData().size(); i++) {
                            if (teamMemberId.equals(String.valueOf(dataObj.getData().get(i).getValue()))) {
                                getView().getSpinnerTeamMemberView().setSelection(i + 1);
                                return;
                            }
                        }
                    } else {
                        getView().getHeaderTeamMemberView().setText(R.string.label_select_provider_name_detail);
                        teamMemberAdapter.updateProvider(getActivity(), Utils.getInstance().getUserName(getActivity()));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void onSuccess(EventDetailModel responseObj) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getView().getEtEventTitleView().setText(responseObj.getData().getEventName());
                getView().getEtEventTitleDetailView().setText(responseObj.getData().getEventName());
                getView().getEtAddNotesView().setText(responseObj.getData().getEventNotes());
                getView().getEtNotesDetailView().setText(responseObj.getData().getEventNotes());
                propertyId = String.valueOf(responseObj.getData().getPropertyId());
                applianceId = String.valueOf(responseObj.getData().getApplianceId());
                teamMemberId = String.valueOf(responseObj.getData().getProviderId());
                projectId = String.valueOf(responseObj.getData().getProjectId());
                eventRepeatId = String.valueOf(responseObj.getData().getEventRepeat());
                getView().getEtPropertyView().setText(responseObj.getData().getPropertyName());
                getView().getEtPropertyDetailView().setText(responseObj.getData().getPropertyName());

                getView().getEtApplianceView().setText(responseObj.getData().getApplianceName());
                getView().getEtApplianceDetailView().setText(responseObj.getData().getApplianceName());
                getView().getTVEventFromTimeView().setText(Utils.getInstance().getYMDEventFormattedTime(responseObj.getData().getEventTimeFrom().replace("+00:00", "")));

                if (!responseObj.getData().getEventTimeTo().isEmpty())
                    getView().getTVEventToTimeView().setText(Utils.getInstance().getYMDEventFormattedTime(responseObj.getData().getEventTimeTo().replace("+00:00", "")));
                if (!responseObj.getData().getEventReminder().isEmpty())
                    getView().getRepeatReminderView().setText(Utils.getInstance().getYMDEventFormattedTime(responseObj.getData().getEventReminder()));
                for (int i = 0; i < listEventReminder.size(); i++) {
                    if (eventRepeatId.equals(String.valueOf(listEventReminder.get(i).getValue()))) {
                        getView().getSpinnerRepeatEventView().setSelection(i + 1);
                        return;
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void onSuccess(GetPropertyResponse dataObj, ApiName check) {
        try {
            if (getActivity() != null && isViewAttached()) {
                switch (check) {
                    case GET_PROJECT_LIST:
                        if (projectAdapter != null) {
                            getView().getSpinnerProjectView().setVisibility(View.VISIBLE);
                            if (!listProjects.isEmpty())
                                listProjects.clear();
                            listProjects.addAll(dataObj.getData());
                            projectAdapter.updateList(getActivity(), dataObj.getData());
                            for (int i = 0; i < dataObj.getData().size(); i++) {
                                if (projectId.equals(String.valueOf(dataObj.getData().get(i).getValue()))) {
                                    getView().getSpinnerProjectView().setSelection(i + 1);
                                    return;
                                }
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void onSuccess(ResourceBean dataObj, ApiName check) {
        try {
            if (getActivity() != null && isViewAttached()) {
                switch (check) {
                    case GET_EVENT_REMINDER_API:
                        if (reminderEventAdapter != null) {
                            getView().getSpinnerProjectView().setVisibility(View.VISIBLE);
                            if (!listEventReminder.isEmpty())
                                listEventReminder.clear();
                            listEventReminder.addAll(dataObj.getData().get(0).getInfo());
                            reminderEventAdapter.updateListEvent(getActivity(), dataObj.getData().get(0).getInfo());
                            for (int i = 0; i < listEventReminder.size(); i++) {
                                if (eventRepeatId.toLowerCase().equals(listEventReminder.get(i).getValue().toLowerCase())) {
                                    getView().getSpinnerRepeatEventView().setSelection(i + 1);
                                    return;
                                }
                            }

                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (getActivity() != null && isViewAttached()) {
            switch (parent.getId()) {
                case R.id.spinner_project_name:
                    if (position != 0) {
                        projectId = String.valueOf(listProjects.get(position - 1).getValue());


                        System.out.println("listProjects.get(position - 1).getApplianceId() = " + listProjects.get(position - 1).getApplianceId());
                        System.out.println("listProjects.get(position - 1).getPropertyId() = " + listProjects.get(position - 1).getPropertyId());

                        if (listProjects.get(position - 1).getPropertyId() == 0 && TextUtils.isEmpty(listProjects.get(position - 1).getPropertyName())) {
                            getView().getEtPropertyView().setEnabled(true);
                            getView().getEtPropertyView().setFocusable(true);
                            getView().getEtPropertyView().setFocusableInTouchMode(true);
                            propertyId = "";
                        } else if (!TextUtils.isEmpty(listProjects.get(position - 1).getPropertyName())) {
                            getView().getEtPropertyView().setEnabled(false);
                            propertyId = TextUtils.isEmpty(String.valueOf(listProjects.get(position - 1).getPropertyId())) ? "" : String.valueOf(listProjects.get(position - 1).getPropertyId());
                        }/* else {
                            propertyName = "";
                            propertyId = String.valueOf(listProjects.get(position - 1).getPropertyId());
                            getView().getEtPropertyView().setEnabled(false);
                        }*/
                        if (listProjects.get(position - 1).getApplianceId() == 0 && TextUtils.isEmpty(listProjects.get(position - 1).getApplianceName())) {
                            getView().getEtApplianceView().setEnabled(true);
                            getView().getEtApplianceView().setFocusable(true);
                            getView().getEtApplianceView().setFocusableInTouchMode(true);
                            applianceId = "";
                        } else if (!TextUtils.isEmpty(listProjects.get(position - 1).getApplianceName())) {
                            getView().getEtApplianceView().setEnabled(false);
                            applianceId = TextUtils.isEmpty(String.valueOf(listProjects.get(position - 1).getApplianceId())) ? "" : String.valueOf(listProjects.get(position - 1).getApplianceId());
                        } else {
                            applianceName = "";
                            applianceId = String.valueOf(listProjects.get(position - 1).getApplianceId());
                            getView().getEtApplianceView().setEnabled(false);
                        }
                        getView().getEtPropertyView().setText(listProjects.get(position - 1).getPropertyName());
                        getView().getEtApplianceView().setText(listProjects.get(position - 1).getApplianceName());

                    } else {
                        projectId = "";
                        propertyId = "";
                        applianceId = "";
                        applianceName = "";
                        propertyName = "";
                        getView().getEtPropertyView().setEnabled(true);
                        getView().getEtPropertyView().setFocusable(true);
                        getView().getEtPropertyView().setFocusableInTouchMode(true);
                        getView().getEtApplianceView().setEnabled(true);
                        getView().getEtApplianceView().setFocusable(true);
                        getView().getEtApplianceView().setFocusableInTouchMode(true);
                        getView().getEtPropertyView().setText("");
                        getView().getEtApplianceView().setText("");
                    }
                    break;
                case R.id.spinner_member_name:
                    if (position != 0) {
                        teamMemberId = String.valueOf(listTeamMember.get(position - 1).getValue());
                    } else {
                        teamMemberId = "";
                    }
                    break;
                case R.id.spinner_repeat_event:
                    if (position != 0) {
                        eventRepeatId = String.valueOf(listEventReminder.get(position - 1).getID());
                    } else {
                        eventRepeatId = "";
                    }
                    break;

            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void getEventDetails(String eventID) {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.EVENT_ID, eventID);
                getInterActor().getEventDetails(param, ApiName.GET_EVENT_DETAIL_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }


    private void getProjectsList() {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                getInterActor().getProjectListing(param, ApiName.GET_PROJECT_LIST);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    private void getTeamMembers() {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                getInterActor().getTeamMember(param, ApiName.GET_TEAM_MEMBER);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    private void getEventReminder() {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                Map<String, String> param = new HashMap<>();
                getActivity().showProgressDialog();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.TYPE, Constants.EVENT_REPEAT);
                getInterActor().getResources(param, GET_EVENT_REMINDER_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    public boolean addEditEvent() {
        try {
            if (getActivity() != null && isViewAttached()) {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    eventTitle = getView().getEtEventTitleView().getText().toString().trim();
                    eventDateTime = getView().getTVEventFromTimeView().getText().toString().trim();
                    reminderDateTime = getView().getRepeatReminderView().getText().toString().trim();
                    addNotes = getView().getEtAddNotesView().getText().toString().trim();
                    eventTimeTo = getView().getTVEventToTimeView().getText().toString().trim();
                    applianceName = getView().getEtApplianceView().getText().toString().trim();
                    propertyName = getView().getEtPropertyView().getText().toString().trim();
                    String response = Validations.getInstance().validateEventFields(eventTitle, eventDateTime, eventTimeTo, reminderDateTime);

                    if (!response.isEmpty()) {
                        getActivity().showSnackBar(getView().getRootView(), response);
                    } else {
                        Map<String, String> param = new HashMap<>();
                        getActivity().showProgressDialog();

                        if (getView().getCurrentScreen().equals(Constants.CLICK_SP_EDIT_EVENT) || getView().getCurrentScreen().equals(Constants.CLICK_SP_VIEW_EVENT))
                            param.put(Constants.EVENT_ID, eventID);
                        param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                        if (!TextUtils.isEmpty(propertyId))
                            param.put(Constants.PROPERTY_ID, propertyId);
                        if (!TextUtils.isEmpty(propertyName))
                            param.put(Constants.PROPERTY_NAME, propertyName);
                        if (!TextUtils.isEmpty(applianceId))
                            param.put(Constants.APPLIANCE_ID, applianceId);
                        if (!TextUtils.isEmpty(applianceName))
                            param.put(Constants.APPLIANCE_NAME, applianceName);
                        param.put(Constants.PROJECT_ID, projectId);
                        param.put(Constants.PROVIDER_ID, teamMemberId);
                        param.put(Constants.EVENT_NAME, Utils.getInstance().capitalize(eventTitle));
                        param.put(Constants.EVENT_NOTES, Utils.getInstance().capitalize(addNotes));
                        param.put(Constants.EVENT_DATE, eventDateTime);
                        param.put(Constants.EVENT_TIME_FROM, eventDateTime);
                        param.put(Constants.EVENT_TIME_TO, eventTimeTo);
                        param.put(Constants.EVENT_REMINDER, reminderDateTime);
                        param.put(Constants.EVENT_REPEAT, eventRepeatId);
                        if (isOccurrence) {
                            if (currentScreen.equals(CLICK_SP_ADD_EVENT)) {
                                if (!eventRepeatId.isEmpty())
                                    param.put(Constants.CHANGE_IN_SERIES, "1");
                                else param.put(Constants.CHANGE_IN_SERIES, "0");
                            } else
                                param.put(Constants.CHANGE_IN_SERIES, "0");

                        } else {
                            if (!eventRepeatId.isEmpty())
                                param.put(Constants.CHANGE_IN_SERIES, "1");
                            else
                                param.put(Constants.CHANGE_IN_SERIES, "0");
                        }
                        //save user current timeZone, value will be like Asia/Kolkata
                        param.put(Constants.APP_TIME_ZONE, Utils.getInstance().getAppTimeZone());

                        getInterActor().addEditEvents(param, ADD_EDIT_EVENT);
                        return true;
                    }
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            switch (v.getId()) {
                case R.id.tv_date_time_from:
                    CalendarPicker.getInstance().showCalendar(getActivity(), this, v.getId(), getView().getTVEventFromTimeView().getText().toString().trim());
                    break;
                case R.id.tv_date_time_to:
                    CalendarPicker.getInstance().showCalendar(getActivity(), this, v.getId(), getView().getEtDateTimeToView().getText().toString().trim());
                    break;
                case R.id.tv_repeat_reminder_time:
                    System.out.println("getView().getRepeatReminderView().getText().toString() = " + getView().getRepeatReminderView().getText().toString());
                    CalendarPicker.getInstance().showCalendar(getActivity(), this, v.getId(), getView().getRepeatReminderView().getText().toString().trim());
                    break;
            }
        }
    }

    public void setEventReminderDateTime(String dateTime, int viewId) {
        System.out.println("dateTime = " + dateTime);
        if (getActivity() != null && isViewAttached()) {
            switch (viewId) {

                case R.id.tv_date_time_from:
                    getView().getTVEventFromTimeView().setText(dateTime);
                    break;
                case R.id.tv_date_time_to:
                    getView().getTVEventToTimeView().setText(dateTime);
                    break;
                case R.id.tv_repeat_reminder_time:
                    getView().getRepeatReminderView().setText(dateTime);
                    break;
            }
        }
    }

    private boolean isValidDate(String date1, String date2, String check) {
        Calendar timeFrom, timeTo;
        try {
            String[] splitDate1, splitDate2;
            String[] arrayDate1, arrayDate2, arrayTime1, arrayTime2;
            timeFrom = Calendar.getInstance();
            timeTo = Calendar.getInstance();


            splitDate1 = date1.split(" ");
            splitDate2 = date2.split(" ");

            System.out.println("splitDate1 = " + splitDate1[0]);
            System.out.println("splitDate2 = " + splitDate2[0]);


            arrayDate1 = splitDate1[0].split("-");
            arrayDate2 = splitDate2[0].split("-");

            arrayTime1 = splitDate1[1].split(":");
            arrayTime2 = splitDate2[1].split(":");


            switch (check) {
                case "from":
                    timeFrom.set(Integer.parseInt(arrayDate1[0]), Integer.parseInt(arrayDate1[1]), Integer.parseInt(arrayDate1[2]),
                            Integer.parseInt(arrayTime1[0]), Integer.parseInt(arrayTime1[1]), Integer.parseInt(arrayTime1[2]));

                    timeTo.set(Integer.parseInt(arrayDate2[0]), Integer.parseInt(arrayDate2[1]), Integer.parseInt(arrayDate2[2]),
                            Integer.parseInt(arrayTime2[0]), Integer.parseInt(arrayTime2[1]), Integer.parseInt(arrayTime2[2]));
                    return !timeTo.getTime().equals(timeFrom.getTime()) && timeTo.getTime().before(timeFrom.getTime());
                case "to":
                    timeFrom.set(Integer.parseInt(arrayDate1[0]), Integer.parseInt(arrayDate1[1]), Integer.parseInt(arrayDate1[2]));
                    timeTo.set(Integer.parseInt(arrayDate2[0]), Integer.parseInt(arrayDate2[1]), Integer.parseInt(arrayDate2[2]));

                    return timeTo.getTime().equals(timeFrom.getTime());
                case "reminder":
                    timeFrom.set(Integer.parseInt(arrayDate1[0]), Integer.parseInt(arrayDate1[1]), Integer.parseInt(arrayDate1[2]),
                            Integer.parseInt(arrayTime1[0]), Integer.parseInt(arrayTime1[1]), Integer.parseInt(arrayTime1[2]));

                    timeTo.set(Integer.parseInt(arrayDate2[0]), Integer.parseInt(arrayDate2[1]), Integer.parseInt(arrayDate2[2]),
                            Integer.parseInt(arrayTime2[0]), Integer.parseInt(arrayTime2[1]), Integer.parseInt(arrayTime2[2]));
                    return !timeTo.getTime().equals(timeFrom.getTime()) && timeTo.getTime().before(timeFrom.getTime()) && !timeTo.getTime().after(timeFrom.getTime());
            }


        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return false;

    }

    public void enableDisableUI(boolean isEdit) {

        if (getActivity() != null && isViewAttached()) {
            if (isEdit) {

                getView().getEtReminderView().setEnabled(true);
                getView().getEtReminderView().setFocusable(true);
                getView().getEtReminderView().setFocusableInTouchMode(true);
                getView().getEtReminderView().requestFocus();

                getView().getEtDateTimeToView().setEnabled(true);
                getView().getRepeatReminderView().setEnabled(true);

                getView().getEtDateTimeFromView().setEnabled(true);

                getView().getTILEventTitleView().setVisibility(View.VISIBLE);
                getView().getTILEventTitleDetailView().setVisibility(View.GONE);

                getView().getTILNotesView().setVisibility(View.VISIBLE);
                getView().getTILNotesDetailView().setVisibility(View.GONE);

                getView().getTILPropertyView().setVisibility(View.VISIBLE);
                getView().getTILPropertyDetailView().setVisibility(View.GONE);

                getView().getTILApplianceView().setVisibility(View.VISIBLE);
                getView().getTILApplianceDetailView().setVisibility(View.GONE);

                getView().getEtApplianceView().setEnabled(true);
                getView().getEtApplianceView().setFocusable(true);
                getView().getEtApplianceView().setFocusableInTouchMode(true);
                getView().getEtApplianceView().requestFocus();

                getView().getEtPropertyView().setEnabled(true);
                getView().getEtPropertyView().setFocusable(true);
                getView().getEtPropertyView().setFocusableInTouchMode(true);
                getView().getEtPropertyView().requestFocus();

                getView().getEtAddNotesView().setEnabled(true);
                getView().getEtAddNotesView().setFocusable(true);
                getView().getEtAddNotesView().setFocusableInTouchMode(true);
                getView().getEtAddNotesView().requestFocus();

                getView().getEtEventTitleView().setEnabled(true);
                getView().getEtEventTitleView().setFocusable(true);
                getView().getEtEventTitleView().setFocusableInTouchMode(true);
                getView().getEtEventTitleView().requestFocus();

                getView().getSpinnerTeamMemberView().setEnabled(true);
                getView().getSpinnerProjectView().setEnabled(true);
                if (!isEventRepeat) {
                    if (!isOccurrence) {
                        getView().getSpinnerRepeatEventView().setEnabled(true);
                        getView().getHeaderEventRepeatNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                    } else {
                        getView().getSpinnerRepeatEventView().setEnabled(false);
                        getView().getHeaderEventRepeatNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                    }
                } else {
                    getView().getSpinnerRepeatEventView().setEnabled(true);
                    getView().getHeaderEventRepeatNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                }


                getView().getHeaderProjectNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getHeaderProjectNameView().setText(R.string.label_select_project);

                getView().getHeaderTeamMemberView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getHeaderTeamMemberView().setText(R.string.label_select_member_name);

                getView().getHeaderRepeatReminderName().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getHeaderRepeatReminderName().setText(R.string.set_reminder);

                getView().getTVHeaderEventFromTimeView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getTVHeaderEventToView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));

                getView().getTeamMemberLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getProjectNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getEventFromLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getEventToLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getReminderTimeLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getRepeatEventLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));


            } else {
                getView().getEtEventTitleView().setEnabled(false);
                getView().getEtEventTitleView().setFocusable(false);

                getView().getEtEventTitleDetailView().setEnabled(false);
                getView().getEtEventTitleDetailView().setFocusable(false);

                getView().getEtAddNotesView().setEnabled(false);
                getView().getEtAddNotesView().setFocusable(false);

                getView().getEtNotesDetailView().setEnabled(false);
                getView().getEtNotesDetailView().setFocusable(false);

                getView().getEtPropertyView().setEnabled(false);
                getView().getEtPropertyView().setFocusable(false);

                getView().getEtPropertyDetailView().setEnabled(false);
                getView().getEtPropertyDetailView().setFocusable(false);

                getView().getEtApplianceView().setEnabled(false);
                getView().getEtApplianceView().setFocusable(false);

                getView().getEtApplianceDetailView().setEnabled(false);
                getView().getEtApplianceDetailView().setFocusable(false);

                getView().getEtDateTimeFromView().setEnabled(false);
                getView().getEtDateTimeFromView().setFocusable(false);

                getView().getEtDateTimeToView().setEnabled(false);
                getView().getEtDateTimeToView().setFocusable(false);

                getView().getEtReminderView().setEnabled(false);
                getView().getRepeatReminderView().setEnabled(false);
                getView().getEtReminderView().setFocusable(false);

                getView().getSpinnerTeamMemberView().setEnabled(false);
                getView().getSpinnerProjectView().setEnabled(false);
                getView().getSpinnerRepeatEventView().setEnabled(false);

                getView().getTILEventTitleDetailView().setVisibility(View.VISIBLE);
                getView().getTILEventTitleView().setVisibility(View.GONE);

                getView().getTILNotesView().setVisibility(View.GONE);
                getView().getTILNotesDetailView().setVisibility(View.VISIBLE);

                getView().getTILPropertyView().setVisibility(View.GONE);
                getView().getTILPropertyDetailView().setVisibility(View.VISIBLE);

                getView().getTILApplianceView().setVisibility(View.GONE);
                getView().getTILApplianceDetailView().setVisibility(View.VISIBLE);

                getView().getHeaderProjectNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getHeaderProjectNameView().setText(R.string.label_select_project_detail);


                getView().getHeaderTeamMemberView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getHeaderTeamMemberView().setText(R.string.label_select_member_name_detail);

                getView().getHeaderEventRepeatNameView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getHeaderRepeatReminderName().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getHeaderRepeatReminderName().setText(R.string.set_reminder_detail);

                getView().getTVHeaderEventFromTimeView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getTVHeaderEventToView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));

                getView().getTeamMemberLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
                getView().getProjectNameLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
                getView().getEventFromLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
                getView().getEventToLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
                getView().getReminderTimeLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
                getView().getRepeatEventLineView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));

            }
        }

    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
        }
        return false;
    }
}
