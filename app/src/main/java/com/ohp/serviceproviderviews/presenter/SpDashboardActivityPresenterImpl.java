package com.ohp.serviceproviderviews.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.view.MenuItem;
import android.view.View;

import com.library.mvp.ActivityPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.enums.ApiName;
import com.ohp.enums.CurrentScreen;
import com.ohp.serviceproviderviews.activities.SPDashboardActivity;
import com.ohp.serviceproviderviews.view.SpDashboardView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import retrofit2.Response;

/**
 * @author Amanpal Singh.
 */

public class SpDashboardActivityPresenterImpl extends ActivityPresenter<SpDashboardView, APIHandler> implements APIResponseInterface.OnCallableResponse, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private CurrentScreen currentScreen;

    public SpDashboardActivityPresenterImpl(SpDashboardView spDashboardView, Context context) {
        super(spDashboardView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Bundle bundle;
        switch (item.getItemId()) {
            case R.id.nav_dashboard:
                if (getActivity() != null && currentScreen != CurrentScreen.SP_DASHBOARD_SCREEN) {
                    currentScreen = CurrentScreen.SP_DASHBOARD_SCREEN;
                    ((SPDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.title_dashboard);
                    ((SPDashboardActivity) getActivity()).changeScreen(R.id.sp_dashboard_container, CurrentScreen.SP_DASHBOARD_SCREEN, false, false, null);
                }
                break;

            case R.id.nav_browse_projects:
                if (getActivity() != null && currentScreen != CurrentScreen.SERVICE_NEAREST_PROJECT) {
                    currentScreen = CurrentScreen.SERVICE_NEAREST_PROJECT;
                    ((SPDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.Browse_projects);
                    bundle = new Bundle();
                    bundle.putString("getnearestProject", getActivity().getIntent().getStringExtra("getnearestProject"));
                    ((SPDashboardActivity) getActivity()).changeScreen(R.id.sp_dashboard_container, CurrentScreen.SERVICE_NEAREST_PROJECT, false, false, bundle);
                }
                break;
            case R.id.nav_my_bids:
                if (getActivity() != null && currentScreen != CurrentScreen.GET_MY_BIDS) {
                    currentScreen = CurrentScreen.GET_MY_BIDS;
                    ((SPDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.My_bids);
                    bundle = new Bundle();
                    bundle.putString("getmyBids", getActivity().getIntent().getStringExtra("getmyBids"));
                    ((SPDashboardActivity) getActivity()).changeScreen(R.id.sp_dashboard_container, CurrentScreen.GET_MY_BIDS, false, false, bundle);
                }

                break;
            case R.id.nav_messages:
                if (getActivity() != null && currentScreen != CurrentScreen.MESSAGE_LIST_SCREEN) {
                    currentScreen = CurrentScreen.MESSAGE_LIST_SCREEN;
                    ((SPDashboardActivity) getActivity()).getToolbarTitleView().setText("Message Listing");
                    bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    ((SPDashboardActivity) getActivity()).changeScreen(R.id.sp_dashboard_container, CurrentScreen.MESSAGE_LIST_SCREEN, false, false, bundle);
                }
                break;
            case R.id.nav_change_password:
                if (getActivity() != null && currentScreen != CurrentScreen.CHANGE_PASSWORD) {
                    currentScreen = CurrentScreen.CHANGE_PASSWORD;
                    ((SPDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.title_change_password);
                    bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    ((SPDashboardActivity) getActivity()).changeScreen(R.id.sp_dashboard_container, CurrentScreen.CHANGE_PASSWORD, false, false, bundle);
                }
                break;

            case R.id.nav_about:
                if (getActivity() != null && currentScreen != CurrentScreen.ABOUT_SCREEN) {
                    currentScreen = CurrentScreen.ABOUT_SCREEN;
                    ((SPDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.about_ohp);
                    bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    bundle.putString(Constants.URL, Constants.URL_ABOUT_US);
                    ((SPDashboardActivity) getActivity()).changeScreen(R.id.sp_dashboard_container, CurrentScreen.SETTING, false, false, bundle);
                }
                break;
            case R.id.nav_privacy_policies:
                if (getActivity() != null && currentScreen != CurrentScreen.PRIVACY_POLICY_SCREEN) {
                    currentScreen = CurrentScreen.PRIVACY_POLICY_SCREEN;
                    ((SPDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.privacy_policies);
                    bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    bundle.putString(Constants.URL, Constants.URL_PRIVACY_POLICY);
                    ((SPDashboardActivity) getActivity()).changeScreen(R.id.sp_dashboard_container, CurrentScreen.SETTING, false, false, bundle);
                }
                break;
            case R.id.nav_terms_and_conditions:
                if (getActivity() != null && currentScreen != CurrentScreen.TERMS_AND_CONDITIONS_SCREEN) {
                    currentScreen = CurrentScreen.TERMS_AND_CONDITIONS_SCREEN;
                    ((SPDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.terms_and_conditions);
                    bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    bundle.putString(Constants.URL, Constants.URL_TERMS_AND_CONDITIONS);
                    ((SPDashboardActivity) getActivity()).changeScreen(R.id.sp_dashboard_container, CurrentScreen.SETTING, false, false, bundle);
                }
                break;

            case R.id.nav_logout:
                Utils.getInstance().confirmDialog(getActivity(), "Are you sure you want to Sign out?", "Sign Out", new Utils.ConfirmDialogCallbackInterface() {
                    @Override
                    public void onYesClick(String tag) {
                        if (isViewAttached()) {
                            ((SPDashboardActivity) getView()).logout("invalid");
                        }
                    }

                    @Override
                    public void onNoClick(String tag) {

                    }
                });
                break;
        }
        if (isViewAttached()) {
            getView().getDrawerView().closeDrawer(GravityCompat.START);
        }
        return true;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentScreen = CurrentScreen.SP_DASHBOARD_SCREEN;
        ((SPDashboardActivity) getActivity()).changeScreen(R.id.sp_dashboard_container, CurrentScreen.SP_DASHBOARD_SCREEN, false, false, null);
    }


    public void onBackPressed() {
        if (isViewAttached() && getView().getDrawerView().isDrawerOpen(GravityCompat.START)) {
            getView().getDrawerView().closeDrawer(GravityCompat.START);
        } else {
            getView().backPress();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.header_layout:
                if (getActivity() != null && isViewAttached()) {
                    if (currentScreen != CurrentScreen.SP_PROFILE_SCREEN) {
                        currentScreen = CurrentScreen.SP_PROFILE_SCREEN;
                        ((SPDashboardActivity) getActivity()).changeScreen(R.id.sp_dashboard_container, CurrentScreen.SP_PROFILE_SCREEN, false, false, null);
                    }
                    getView().getDrawerView().closeDrawer(GravityCompat.START);
                }
                break;
            case R.id.tv_name:
                if (getActivity() != null && isViewAttached()) {
                    if (currentScreen != CurrentScreen.SP_PROFILE_SCREEN) {
                        currentScreen = CurrentScreen.SP_PROFILE_SCREEN;
                        ((SPDashboardActivity) getActivity()).changeScreen(R.id.sp_dashboard_container, CurrentScreen.SP_PROFILE_SCREEN, false, false, null);
                    }
                    getView().getDrawerView().closeDrawer(GravityCompat.START);
                }
                break;
        }
    }


    @Override
    public void onSuccess(Response response, ApiName api) {

    }

    @Override
    public void onFailure(Throwable t, ApiName api) {

    }
}
