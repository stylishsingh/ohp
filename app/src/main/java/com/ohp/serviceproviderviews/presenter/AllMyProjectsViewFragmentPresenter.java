package com.ohp.serviceproviderviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.serviceproviderviews.adapter.MyProjectsViewAdapter;
import com.ohp.serviceproviderviews.beans.GetAllProjectsServiceProviderResponse;
import com.ohp.serviceproviderviews.view.AllMyProjectsViewFragmentView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * Created by vishal.sharma on 7/31/2017.
 */

public class AllMyProjectsViewFragmentPresenter extends FragmentPresenter<AllMyProjectsViewFragmentView, APIHandler> implements APIResponseInterface.OnCallableResponse, View.OnClickListener {
    public int page = 1;
    private MyProjectsViewAdapter adapter;
    private List<GetAllProjectsServiceProviderResponse.DataBean.MyProjectsBean> listMyProjects = new ArrayList<>();
    private boolean isSearched = false;
    private String searchQuery = "";  //for search query
    private BroadcastReceiver projectsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("DashboardActivityPresenterImpl.onReceive");
            if (getActivity() != null && isViewAttached()) {
                page = 1;
                listMyProjects = new ArrayList<>();
                getAllMyProjects();
            }

        }
    };
    private GetAllProjectsServiceProviderResponse allMyProjectsResponse;
    private int adapterPosition = 0;

    public AllMyProjectsViewFragmentPresenter(AllMyProjectsViewFragmentView allMyProjectsViewFragmentView, Context context) {
        super(allMyProjectsViewFragmentView, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(projectsReceiver,
                    new IntentFilter(Constants.GET_PROJECTS_SP_MYPROJECTS));
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            getView().getToolbarTitle().setText("My Projects");
            getView().getToolbar().setVisibility(View.VISIBLE);
            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    ((HolderActivity) getActivity()).oneStepBack();
                }
            });

            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getRecyclerMyProjectList().setLayoutManager(lm);
            getView().getRecyclerMyProjectList().setNestedScrollingEnabled(false);
            adapter = new MyProjectsViewAdapter(getActivity(), null, this);
            getView().getRecyclerMyProjectList().setAdapter(adapter);

//            getView().getNSView().setNestedScrollingEnabled(false);
            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

                            if (allMyProjectsResponse != null && allMyProjectsResponse.getTotalRecord() > listMyProjects.size()) {

                                System.out.println("total Records" + allMyProjectsResponse.getTotalRecord());
                                page += 1;
                                getAllMyProjects();
                            }
                        }
                    }
                }
            });
            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });
            getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s != null && s.length() > 0) {
                        getView().getCloseBtn().setVisibility(View.VISIBLE);
                        searchQuery = s.toString();
                        page = 1;
                        isSearched = true;
                        getAllMyProjects();

                    } else {
                        searchQuery = "";
                        page = 1;
                        isSearched = true;
                        getAllMyProjects();
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        getView().getCloseBtn().setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });


        }

        getAllMyProjects();
    }

    private void getAllMyProjects() {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        if (Utils.getInstance().isInternetAvailable(getActivity())) {
                            if (TextUtils.isEmpty(searchQuery))
                                getActivity().showProgressDialog();
                            Map<String, String> params = new HashMap<>();
                            params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                            if (!TextUtils.isEmpty(searchQuery))
                                params.put(Constants.SEARCH_PARAM, searchQuery);
                            params.put(Constants.PAGE_PARAM, String.valueOf(page));
                            params.put(Constants.PROJECT_TYPE, Constants.PROJECT_TYPE_MY_PROJECT);
                            getInterActor().getAllProjectsServiceProvider(params, ApiName.GET_PROJECTS);

                        } else {
                            getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    getActivity().hideProgressDialog();
                }
                switch (api) {
                    case GET_PROJECTS:
                        getActivity().hideProgressDialog();
                        if (response.isSuccessful()) {
                            allMyProjectsResponse = (GetAllProjectsServiceProviderResponse) response.body();
                            switch (allMyProjectsResponse.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(allMyProjectsResponse);
                                    break;
                                case Constants.STATUS_201:
                                    getActivity().showSnakBar(getView().getRootView(), allMyProjectsResponse.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((OwnerDashboardActivity) getActivity()).logout(allMyProjectsResponse.getMessage());
                                    break;
                            }
                        }
                        break;
                    case DELETE_PROJECT:
                        getActivity().hideProgressDialog();
                        if (response.isSuccessful()) {
                            GenericBean genericBean = (GenericBean) response.body();
                            switch (genericBean.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(genericBean);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((OwnerDashboardActivity) getActivity()).logout(genericBean.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onSuccess(GenericBean genericBean) {
        listMyProjects.remove(adapterPosition);
        adapter.notifyDataSetChanged();

        Intent listIntent = new Intent(Constants.GET_PROJECTS_SP);
        getActivity().sendBroadcast(listIntent);
    }

    private void onSuccess(GetAllProjectsServiceProviderResponse dataObj) {
        if (getActivity() != null && isViewAttached()) {
            if (dataObj.getData() != null) {
                if (dataObj.getData().getMyProjects().size() > 0) {
                    getView().getEmptyLayout().setVisibility(View.GONE);
                    if (isSearched || searchQuery.length() != 0) {
                        listMyProjects = new ArrayList<>();
                        listMyProjects.addAll(dataObj.getData().getMyProjects());
                        isSearched = false;
                    } else if (dataObj.getTotalRecord() > listMyProjects.size()) {
                        listMyProjects.addAll(dataObj.getData().getMyProjects());
                    }
                    adapter.updateList(listMyProjects);
                } else {
                    if (listMyProjects.isEmpty()) {
                        adapter.clearAll();
                        getView().getEmptyLayout().setVisibility(View.VISIBLE);
                    } else {
                        if (isSearched || searchQuery.length() != 0) {
                            adapter.clearAll();
                            getView().getEmptyLayout().setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            switch (v.getId()) {
                case R.id.btn_clear:
                    searchQuery = "";
                    page = 1;
                    getView().getSearchEditText().setText("");
                    break;
            }
        }
    }

    public void deleteProject(int projectId, int position) {
        try {
            if (getActivity() != null && isViewAttached()) {
                adapterPosition = position;
                getActivity().showProgressDialog();
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Map<String, String> params = new HashMap<>();
                    params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    params.put(Constants.PROJECT_ID, String.valueOf(projectId));
                    getInterActor().deleteProject(params, ApiName.DELETE_PROJECT);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
