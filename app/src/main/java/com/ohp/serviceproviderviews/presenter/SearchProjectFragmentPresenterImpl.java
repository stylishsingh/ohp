package com.ohp.serviceproviderviews.presenter;

import android.content.Context;

import com.library.mvp.FragmentPresenter;
import com.ohp.api.APIHandler;
import com.ohp.enums.ApiName;
import com.ohp.serviceproviderviews.view.SearchProjectView;

import retrofit2.Response;

/**
 * Created by vishal.sharma on 7/28/2017.
 */

public class SearchProjectFragmentPresenterImpl extends FragmentPresenter<SearchProjectView, APIHandler> implements APIHandler.OnCallableResponse {
    public SearchProjectFragmentPresenterImpl(SearchProjectView searchProjectView, Context context) {
        super(searchProjectView, context);
    }





    @Override
    public void onSuccess(Response response, ApiName api) {

    }

    @Override
    public void onFailure(Throwable t, ApiName api) {

    }

    @Override
    protected APIHandler createInterActor() {
        return null;
    }
}
