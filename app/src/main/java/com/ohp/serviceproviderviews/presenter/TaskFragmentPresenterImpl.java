package com.ohp.serviceproviderviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.serviceproviderviews.activities.SPDashboardActivity;
import com.ohp.serviceproviderviews.adapter.TaskListingAdapter;
import com.ohp.serviceproviderviews.beans.GetTaskListingResponse;
import com.ohp.serviceproviderviews.view.TaskFragmentView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static com.ohp.enums.ApiName.GET_TASK_LISTING;

/**
 * @author Vishal Sharma.
 */

public class TaskFragmentPresenterImpl extends FragmentPresenter<TaskFragmentView, APIHandler> implements APIResponseInterface.OnCallableResponse,
        View.OnClickListener, PopupMenu.OnMenuItemClickListener {
    public String searchQuery = "", taskType = "";
    public int page = 1;
    private GetTaskListingResponse getTaskListingResponse;
    private boolean isSearched = false, isFilterChanged = false;
    private TaskListingAdapter taskListingAdapter;
    private List<GetTaskListingResponse.DataBean> taskList = new ArrayList<>();
    private BroadcastReceiver taskListReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("DashboardActivityPresenterImpl.onReceive");
            if (getActivity() != null && isViewAttached()) {
                page = 1;
                taskType = "";
                taskList = new ArrayList<>();
                getTaskList();
            }

        }
    };
    private int adapterPosition = 0;
    private String changeInSeries = "0";

    public TaskFragmentPresenterImpl(TaskFragmentView taskFragmentView, Context context) {
        super(taskFragmentView, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(taskListReciever,
                    new IntentFilter(Constants.TASK_LIST_UPDATE));
            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getRecyclerTaskList().setLayoutManager(lm);
            getView().getRecyclerTaskList().setNestedScrollingEnabled(false);
            taskListingAdapter = new TaskListingAdapter(getActivity(), null, this, Utils.getInstance().getRole(getActivity()));
            getView().getRecyclerTaskList().setAdapter(taskListingAdapter);

            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

                            if (taskList != null && getTaskListingResponse.getTotalRecord() > taskList.size()) {

                                System.out.println("total Records" + getTaskListingResponse.getTotalRecord());
                                page += 1;
                                getTaskList();
                            }
                        }
                    }
                }
            });
            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });
            getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s != null && s.length() > 0) {
                        getView().getCloseBtn().setVisibility(View.VISIBLE);
                        searchQuery = s.toString();
                        page = 1;
                        isSearched = true;
                        getTaskList();

                    } else {
                        searchQuery = "";
                        page = 1;
                        isSearched = true;
                        getTaskList();
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        getView().getCloseBtn().setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });


            getTaskList();
        }
    }

    @Override
    public void onDestroy() {
        if (getActivity() != null && isViewAttached())
            getActivity().unregisterReceiver(taskListReciever);
        super.onDestroy();

    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_TASK_LISTING:
                        if (response.isSuccessful()) {
                            getTaskListingResponse = (GetTaskListingResponse) response.body();
                            switch (getTaskListingResponse.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(getTaskListingResponse);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    if (getActivity() instanceof OwnerDashboardActivity)
                                        ((OwnerDashboardActivity) getActivity()).logout(getTaskListingResponse.getMessage());
                                    else if (getActivity() instanceof SPDashboardActivity)
                                        ((SPDashboardActivity) getActivity()).logout(getTaskListingResponse.getMessage());
                                    break;
                            }
                        }
                        break;
                    case DELETE_TASK:
                        getActivity().hideProgressDialog();
                        if (response.isSuccessful()) {
                            GenericBean genericBean = (GenericBean) response.body();
                            switch (genericBean.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(genericBean);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    if (getActivity() instanceof OwnerDashboardActivity)
                                        ((OwnerDashboardActivity) getActivity()).logout(genericBean.getMessage());
                                    else if (getActivity() instanceof SPDashboardActivity)
                                        ((SPDashboardActivity) getActivity()).logout(genericBean.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(GenericBean genericBean) {
        if (getActivity() != null && isViewAttached()) {

            taskList.remove(adapterPosition);
            taskListingAdapter.notifyDataSetChanged();

            Intent listIntent = new Intent(Constants.TASK_LIST_UPDATE);
            getActivity().sendBroadcast(listIntent);
        }
    }

    private void onSuccess(GetTaskListingResponse getTaskListingResponse) {
        if (getTaskListingResponse.getData() != null && getTaskListingResponse.getData().size() > 0) {
            getView().getEmptyLayout().setVisibility(View.GONE);
            if (isSearched || searchQuery.length() != 0) {
                taskList = new ArrayList<>();
                taskList.addAll(getTaskListingResponse.getData());
                isSearched = false;
            } else if (getTaskListingResponse.getTotalRecord() > taskList.size()) {
                taskList.addAll(getTaskListingResponse.getData());
            }
            if (isFilterChanged) {
                taskList = getTaskListingResponse.getData();
                isFilterChanged = false;
            }
            taskListingAdapter.updateList(taskList);

        } else {
            if (taskList.isEmpty()) {
                taskListingAdapter.clearAll();
                getView().getEmptyLayout().setVisibility(View.VISIBLE);
            } else {
                if (isSearched || searchQuery.length() != 0) {
                    taskListingAdapter.clearAll();
                    getView().getEmptyLayout().setVisibility(View.VISIBLE);
                }
            }
        }


    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            switch (v.getId()) {
                case R.id.fab_add:
                    taskListingAdapter.mItemManger.closeAllItems();
                    Intent intent = new Intent(getActivity(), HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_ADD_TASK);
                    intent.putExtra(Constants.TASK_ID, "");
                    intent.putExtra(Constants.TASK_REPEAT_EMPTY, true);
                    intent.putExtra(Constants.OCCURRENCE, true);
                    intent.putExtra(Constants.EVENT_TYPE, 0);
                    getActivity().startActivity(intent);
                    break;
                case R.id.btn_clear:
                    searchQuery = "";
                    page = 1;
                    getView().getSearchEditText().setText("");
                    break;

            }

        }
    }

    private void getTaskList() {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        if (Utils.getInstance().isInternetAvailable(getActivity())) {
                            if (TextUtils.isEmpty(searchQuery))
                                getActivity().showProgressDialog();
                            Map<String, String> params = new HashMap<>();
                            params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                           /* if (!TextUtils.isEmpty(OwnerDashboardActivity.sort_order))
                                params.put(Constants.SORT_ORDER, "DESC");*/
                            if (!TextUtils.isEmpty(searchQuery))
                                params.put(Constants.SEARCH_PARAM, searchQuery);
                            params.put(Constants.PAGE_PARAM, String.valueOf(page));
                            // params.put(Constants.PROJECT_TYPE, Constants.PROJECT_TYPE_WON);
                            params.put(Constants.TASK_TYPE, taskType);
                            getInterActor().getTaskListing(params, GET_TASK_LISTING);

                        } else {
                            getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void deleteProject(String taskId) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().showProgressDialog();
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Map<String, String> params = new HashMap<>();
                    params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    params.put(Constants.TASK_ID, taskId);
                    params.put(Constants.CHANGE_IN_SERIES, changeInSeries);
                    getInterActor().deleteTask(params, ApiName.DELETE_TASK);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        isFilterChanged = true;
        page = 1;
        taskList = new ArrayList<>();
        taskListingAdapter.clearAll();
        switch (menuItem.getItemId()) {
            case R.id.action_current_task:
                taskType = "ongoing";
                getTaskList();
                break;
            case R.id.action_completed_task:
                taskType = "completed";
                getTaskList();
                break;
            case R.id.action_all_task:
                taskType = "";
                getTaskList();
                break;
        }
        return true;
    }

    public void onItemClicked(final int layoutPosition, ScreenNavigation click) {
        final Intent intent = new Intent(getActivity(), HolderActivity.class);
        switch (click) {
            case DETAIL_ITEM:
                intent.putExtra(Constants.OCCURRENCE, false);
                if (taskList.get(layoutPosition).getTaskRepeat() == 0)
                    intent.putExtra(Constants.TASK_REPEAT_EMPTY, true);
                else
                    intent.putExtra(Constants.TASK_REPEAT_EMPTY, false);
                intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_TASK);
                intent.putExtra(Constants.TASK_ID, String.valueOf(taskList.get(layoutPosition).getTaskId()));
                intent.putExtra(Constants.TASK_TYPE, taskList.get(layoutPosition).getTaskType());
                getActivity().startActivity(intent);
                break;
            case EDIT_ITEM:
                if (taskList.get(layoutPosition).getTaskRepeat() != 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String[] options = {"Edit Following Occurrences", "Edit Current Occurrence", "Cancel"};
                    builder.setTitle("Choose option")
                            .setItems(options, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    switch (which) {
                                        case 0:
                                            intent.putExtra(Constants.TASK_REPEAT_EMPTY, false);
                                            intent.putExtra(Constants.OCCURRENCE, false);
                                            intent.putExtra(Constants.TASK_ID, String.valueOf(taskList.get(layoutPosition).getTaskId()));
                                            intent.putExtra(Constants.DESTINATION, Constants.CLICK_EDIT_TASK);
                                            intent.putExtra(Constants.TASK_TYPE, taskList.get(layoutPosition).getTaskType());
                                            getActivity().startActivity(intent);
                                            break;
                                        case 1:
                                            intent.putExtra(Constants.TASK_REPEAT_EMPTY, false);
                                            intent.putExtra(Constants.OCCURRENCE, true);
                                            intent.putExtra(Constants.TASK_ID, String.valueOf(taskList.get(layoutPosition).getTaskId()));
                                            intent.putExtra(Constants.DESTINATION, Constants.CLICK_EDIT_TASK);
                                            intent.putExtra(Constants.TASK_TYPE, taskList.get(layoutPosition).getTaskType());
                                            getActivity().startActivity(intent);
                                            break;
                                        case 2:
                                            dialog.dismiss();
                                            break;

                                    }
                                }
                            });
                    builder.setCancelable(false);
                    builder.show();
                } else {
                    intent.putExtra(Constants.TASK_REPEAT_EMPTY, true);
                    intent.putExtra(Constants.OCCURRENCE, false);
                    intent.putExtra(Constants.TASK_ID, String.valueOf(taskList.get(layoutPosition).getTaskId()));
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_EDIT_TASK);
                    intent.putExtra(Constants.EVENT_TYPE, taskList.get(layoutPosition).getTaskStatus());
                    getActivity().startActivity(intent);
                }

                break;
            case DELETE_ITEM:
                if (String.valueOf(taskList.get(layoutPosition).getTaskRepeat()).isEmpty()) {
                    Utils.getInstance().confirmDialog(getActivity(), "Are you sure, you want to delete this task?", "delete", new Utils.ConfirmDialogCallbackInterface() {
                        @Override
                        public void onYesClick(String tag) {
                            adapterPosition = layoutPosition;
                            deleteProject(String.valueOf(taskList.get(layoutPosition).getTaskId()));
                        }

                        @Override
                        public void onNoClick(String tag) {

                        }
                    });
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String[] options = {"Delete Following Occurrences", "Delete Current Occurrence", "No"};
                    builder.setTitle("Are you sure, you want to delete this task?")
                            .setItems(options, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    switch (which) {
                                        case 0:
                                            changeInSeries = "1";
                                            adapterPosition = layoutPosition;
                                            deleteProject(String.valueOf(taskList.get(layoutPosition).getTaskId()));
                                            break;
                                        case 1:
                                            changeInSeries = "0";
                                            adapterPosition = layoutPosition;
                                            deleteProject(String.valueOf(taskList.get(layoutPosition).getTaskId()));
                                            break;
                                        case 2:
                                            dialog.dismiss();
                                            break;

                                    }
                                }
                            });
                    builder.setCancelable(false);
                    builder.show();
                }
                break;
        }
    }

}
