package com.ohp.serviceproviderviews.presenter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.JobTypeAndExpertise;
import com.ohp.enums.ApiName;
import com.ohp.serviceproviderviews.activities.SPDashboardActivity;
import com.ohp.serviceproviderviews.activities.SPProfileActivity;
import com.ohp.serviceproviderviews.beans.SpProfileModel;
import com.ohp.serviceproviderviews.fragment.SPProfileFragment;
import com.ohp.serviceproviderviews.view.SPProfileFragmentView;
import com.ohp.utils.Constants;
import com.ohp.utils.FileUtils;
import com.ohp.utils.PermissionsAndroid;
import com.ohp.utils.SharedPreferencesHandler;
import com.ohp.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.ohp.enums.ApiName.GET_RESOURCES_API;

/**
 * @author Amanpal Singh.
 */

public class SPProfileFragmentPresenterImpl extends FragmentPresenter<SPProfileFragmentView, APIHandler> implements APIHandler.OnCallableResponse,
        View.OnClickListener, ImagePickerCallback {

    private boolean isFirstTimeUser = false, isImageSelected = false, cameraSelected = false, gallerySelected = false;
    private String profileImagePath;
    private Place place;
    private String gender = "male", licensed = "0", insured = "0", bonded = "0", expertiseName = "",
            mSelectedExpertise = "", selectedExpertiseName = "", latitude = "", longitude = "";
    private String[] detailExpertise;
    private int PLACE_PICKER_REQUEST = 1;
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private String pickerPath;
    private ArrayAdapter expertiseAdapter;
    private boolean[] arraySelectionExpertise;
    private SpProfileModel profileObj;

    private List<JobTypeAndExpertise.DataBean.InfoBean> listExpertise = new ArrayList<>();

    public MultiSelectSpinner.MultiSpinnerListener expertiseSpinnerListener = new MultiSelectSpinner.MultiSpinnerListener() {

        public void onItemsSelected(boolean[] selected) {
            mSelectedExpertise = "";
            expertiseName = "";
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    if (mSelectedExpertise.trim().isEmpty()) {
                        mSelectedExpertise = String.valueOf(listExpertise.get(i).getId());
                        expertiseName = listExpertise.get(i).getValue();
                    } else {
                        mSelectedExpertise = mSelectedExpertise + "," + String.valueOf(listExpertise.get(i).getId());
                        expertiseName = expertiseName + "," + listExpertise.get(i).getValue();
                    }
                }
            }

            if (getActivity() != null && isViewAttached()) {
                if (mSelectedExpertise.isEmpty()) {
                    mSelectedExpertise = String.valueOf(listExpertise.get(0).getId());
                    expertiseName = listExpertise.get(0).getValue();
                    arraySelectionExpertise[0] = true;
                    getView().getExpertiseSpinnerView().setAllCheckedText(expertiseName);
                    getView().getExpertiseSpinnerView().selectItem(0, arraySelectionExpertise[0]);
                } else {
                    if (getView().getExpertiseSpinnerView().isSelectAll())
                        getView().getExpertiseSpinnerView().setAllCheckedText(expertiseName);
                }
            }
        }
    };


    public SPProfileFragmentPresenterImpl(SPProfileFragmentView spProfileFragmentView, Context context) {
        super(spProfileFragmentView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("picker_path")) {
                pickerPath = savedInstanceState.getString("picker_path");
            }
        }

        if (getActivity() != null && getView() != null) {
            if (getActivity() instanceof SPDashboardActivity) {
                ((SPDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.title_profile);
            } else if (getActivity() instanceof SPProfileActivity) {
                ((SPProfileActivity) getActivity()).getToolbarTitleView().setText(R.string.title_profile);
            }

            getView().getEmail().setEnabled(false);
            getView().getEmail().setVisibility(View.VISIBLE);
//            }

            if (isFirstTimeUser) {
                enableDisableUI(true);
            } else {
                enableDisableUI(true);
            }

            //set radio group click listener
            getView().getGenderRadioGroup().setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                    switch (checkedId) {
                        case R.id.radio_male:
                            getView().getMaleRadio().setChecked(true);
                            getView().getFemaleRadio().setChecked(false);
                            gender = "male";
                            break;
                        case R.id.radio_female:
                            getView().getMaleRadio().setChecked(false);
                            getView().getFemaleRadio().setChecked(true);
                            gender = "female";
                            break;
                    }
                }
            });

            //set license radio group click listener
            getView().getLicenseRadioGroup().setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                    switch (checkedId) {
                        case R.id.radio_license_yes:
                            getView().getLicenseYesRadio().setChecked(true);
                            getView().getLicenseNoRadio().setChecked(false);
                            licensed = "1";
                            getView().getLicenseNumberLayoutView().setVisibility(View.VISIBLE);
                            break;
                        case R.id.radio_license_no:
                            getView().getLicenseYesRadio().setChecked(false);
                            getView().getLicenseNoRadio().setChecked(true);
                            licensed = "0";
                            getView().getLicenseNumberLayoutView().setVisibility(View.GONE);
                            break;
                    }
                }
            });

            //set license radio group click listener
            getView().getBondedRadioGroup().setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                    switch (checkedId) {
                        case R.id.radio_bonded_yes:
                            getView().getBondedYesRadio().setChecked(true);
                            getView().getBondedNoRadio().setChecked(false);
                            bonded = "1";
                            break;
                        case R.id.radio_bonded_no:
                            getView().getBondedYesRadio().setChecked(false);
                            getView().getBondedNoRadio().setChecked(true);
                            bonded = "0";
                            break;
                    }
                }
            });

            //set license radio group click listener
            getView().getInsuredRadioGroup().setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                    switch (checkedId) {
                        case R.id.radio_insured_yes:
                            getView().getInsuredYesRadio().setChecked(true);
                            getView().getInsuredNoRadio().setChecked(false);
                            insured = "1";
                            break;
                        case R.id.radio_insured_no:
                            getView().getInsuredYesRadio().setChecked(false);
                            getView().getInsuredNoRadio().setChecked(true);
                            insured = "0";
                            break;
                    }
                }
            });

           /*
            Hit API to get Profile Detail
             */
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                getActivity().showProgressDialog();
                getInterActor().getSpProfile(param, ApiName.GET_SP_PROFILE_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    /*
    Save BUndle Info
     */
    public void saveBundleInfo(Bundle bundle) {
        isFirstTimeUser = bundle.getBoolean("is_edit");
        if (isFirstTimeUser) {
            //din't show back button
        }
    }

    public boolean saveProfileInfo() {
        if (getActivity() != null && isViewAttached()) {
            Map<String, String> param = new HashMap<>();
            String website = getView().getWebsite().getText().toString().trim();
            param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
            if (getView().getName().getText().toString().trim().length() == 0) {
                Utils.getInstance().showToast("Please enter full name");
            } else if (getView().getAddress().getText().toString().isEmpty()) {
                Utils.getInstance().showToast("Please select address");
            } else if (getView().getCompany().getText().toString().trim().length() == 0) {
                Utils.getInstance().showToast("Please enter Business name");
            } else if (licensed.equals("1") && getView().getLicenseNumber().getText().toString().trim().isEmpty()) {
                Utils.getInstance().showToast("Please enter License number");
            } else if (!website.isEmpty() && !URLUtil.isValidUrl(website)) {
                Utils.getInstance().showToast("Please enter valid website");
            } else {
                String fullName = getView().getName().getText().toString().replaceAll("\\s+", " ");
                String companyName = getView().getCompany().getText().toString().replaceAll("\\s+", " ");
                param.put(Constants.FULL_NAME, fullName);
                if (latitude.isEmpty() && longitude.isEmpty()) {
                    if (place != null && place.getLatLng() != null) {
                        param.put(Constants.LATITUDE, String.valueOf(place.getLatLng().latitude));
                        param.put(Constants.LONGITUDE, String.valueOf(place.getLatLng().longitude));
                    } else {
                        Utils.getInstance().showToast("Please select address");
                    }
                } else {
                    param.put(Constants.LATITUDE, latitude);
                    param.put(Constants.LONGITUDE, longitude);
                }

                param.put(Constants.ADDRESS, getView().getAddress().getText().toString());

                param.put(Constants.GENDER, gender);

                param.put(Constants.LICENSED, licensed);

                param.put(Constants.INSURED, insured);

                param.put(Constants.BONDED, bonded);

                param.put(Constants.EXPERTISE, mSelectedExpertise);

                param.put(Constants.BUSINESS_NAME, companyName);
                param.put(Constants.LICENSE_NO, getView().getLicenseNumber().getText().toString().trim());
                param.put(Constants.WEBSITE, website);
                // Validation successful, now check for profile image
                if (isImageSelected && !TextUtils.isEmpty(profileImagePath)) {
                    getActivity().showProgressDialog();
                    getInterActor().saveSpProfileWithPic(param, profileImagePath, ApiName.SAVE_SP_PROFILE_API);
                } else {
                    getActivity().showProgressDialog();
                    getInterActor().saveSpProfile(param, ApiName.SAVE_SP_PROFILE_API);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && getView() != null) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.edit_profile:
                    // show picker for image capture
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String[] options = {"Camera", "Gallery", "Cancel"};
                    final Bundle bundle = new Bundle();
                    builder.setTitle("Select Option")
                            .setItems(options, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    switch (which) {
                                        case 0:
                                            cameraSelected = true;
                                            gallerySelected = false;
                                            checkCameraPermission();
                                            break;
                                        case 1:
                                            cameraSelected = false;
                                            gallerySelected = true;
                                            checkCameraPermission();
                                            break;
                                        case 2:
                                            dialog.dismiss();
                                            break;
                                    }
                                }
                            });
                    builder.setCancelable(false);
                    builder.show();
                    break;
                case R.id.profile_img:

                    break;
                case R.id.et_address:
                    //Move to Search Address Fragment
                    getActivity().showProgressDialog();
                    try {
                        PlacePicker.IntentBuilder placeBuilder = new PlacePicker.IntentBuilder();
                        getActivity().startActivityForResult(placeBuilder.build(getActivity()), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                        getActivity().hideProgressDialog();
                    }
                    break;
            }
        }
    }


    //  @TargetApi(Build.VERSION_CODES.M)
    private void checkCameraPermission() {
        boolean isExternalStorage = PermissionsAndroid.getInstance().checkCameraPermission(getActivity());
        if (!isExternalStorage) {
            PermissionsAndroid.getInstance().requestForCameraPermission(((SPProfileFragment) getView()));
        } else if (cameraSelected) {
            takePicture();
        } else if (gallerySelected) {
            pickImageSingle();
        }
    }


    private void takePicture() {
        cameraPicker = new CameraImagePicker(getActivity());
//        cameraPicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        cameraPicker.setImagePickerCallback(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        pickerPath = cameraPicker.pickImage();
    }

    private void pickImageSingle() {
        imagePicker = new ImagePicker(getActivity());
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (resultCode == RESULT_OK) {
                    if (requestCode == PLACE_PICKER_REQUEST) {
                        place = PlacePicker.getPlace(data, getActivity());
                        if (place != null) {
                            getView().getAddress().setText(place.getAddress());
                            if (place != null && place.getLatLng() != null) {
                                latitude = String.valueOf(place.getLatLng().latitude);
                                longitude = String.valueOf(place.getLatLng().longitude);
                            }
                        }
                    } else if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                        if (imagePicker == null) {
                            imagePicker = new ImagePicker(getActivity());
                        }
                        imagePicker.submit(data);
                    } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                        if (cameraPicker == null) {
                            cameraPicker = new CameraImagePicker(getActivity());
                            cameraPicker.setImagePickerCallback(this);
                            cameraPicker.reinitialize(pickerPath);
                        }
                        cameraPicker.submit(data);
                    }
                } else if (resultCode == 999) {
                    if (data.getData() != null) {
                        profileImagePath = data.getData().toString();
                        isImageSelected = true;
                        Picasso.with(getActivity()).load(new File(data.getData().toString())).into(getView().getProfileImg());
                    }
                    Utils.getInstance().showToast("cropped successful " + data.getData());
                } else {
                    Utils.getInstance().showToast("Request cancelled");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_SP_PROFILE_API:
                        if (response.isSuccessful()) {
                            SpProfileModel profileObj = (SpProfileModel) response.body();
                            this.profileObj = profileObj;
                            switch (profileObj.getStatus()) {
                                case Constants.STATUS_200:
                                    updateView(profileObj);
                                    break;
                                case Constants.STATUS_201:
                                    Utils.getInstance().showToast(profileObj.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    if (isViewAttached()) {
                                        if (getActivity() instanceof SPProfileActivity)
                                            ((SPProfileActivity) getActivity()).logout(profileObj.getMessage());
                                        else if (getActivity() instanceof SPDashboardActivity)
                                            ((SPDashboardActivity) getActivity()).logout(profileObj.getMessage());
                                    }
                                    break;
                            }
                        }
                        getExpertise();
                        break;
                    case SAVE_SP_PROFILE_API:
                        if (response.isSuccessful()) {
                            SpProfileModel saveObj = (SpProfileModel) response.body();
                            switch (saveObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(saveObj);
                                    break;
                                case Constants.STATUS_201:
                                    ((SPProfileFragment) getView()).editMenu.setVisible(false);
                                    ((SPProfileFragment) getView()).saveMenu.setVisible(true);
                                    break;
                                case Constants.STATUS_202:
                                    if (isViewAttached()) {
                                        if (getActivity() instanceof SPProfileActivity)
                                            ((SPProfileActivity) getActivity()).logout(saveObj.getMessage());
                                        else if (getActivity() instanceof SPDashboardActivity)
                                            ((SPDashboardActivity) getActivity()).logout(saveObj.getMessage());
                                    }
                                    break;
                            }
                        }
                        break;
                    case GET_RESOURCES_API:
                        if (response.isSuccessful()) {
                            getActivity().hideProgressDialog();
                            JobTypeAndExpertise resourcesObj = (JobTypeAndExpertise) response.body();
                            switch (resourcesObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(resourcesObj);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(resourcesObj.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getExpertise() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));

                param.put(Constants.TYPE, "jobtype_expertise");
                getInterActor().getResourcesJobAndExpertise(param, GET_RESOURCES_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    private void onSuccess(JobTypeAndExpertise resourcesObj) {
        try {
            if (getActivity() != null && isViewAttached()) {

                listExpertise = new ArrayList<>();
                if (!listExpertise.isEmpty())
                    listExpertise.clear();

                List<String> listExpert = new ArrayList<>();

                listExpertise.addAll(resourcesObj.getData().get(1).get(0).getInfo());
                for (int i = 0; i < listExpertise.size(); i++) {
                    listExpert.add(listExpertise.get(i).getValue());
                }

                expertiseAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_multiple_choice, listExpert);
                getView().getExpertiseSpinnerView().setListAdapter(expertiseAdapter).setAllCheckedText(selectedExpertiseName);

                if (!listExpertise.isEmpty()) {
                    arraySelectionExpertise = new boolean[listExpertise.size()];
                    for (int j = 0; j < listExpertise.size(); j++) {
                        if (detailExpertise != null) {

                            for (int i = 0; i < detailExpertise.length; i++) {
                                if (detailExpertise[i].equals(String.valueOf(listExpertise.get(j).getId()))) {
                                    arraySelectionExpertise[j] = true;
                                    if (expertiseName.trim().isEmpty())
                                        expertiseName = String.valueOf(listExpertise.get(j).getValue());
                                    else
                                        expertiseName = expertiseName + "," + String.valueOf(listExpertise.get(j).getValue());
                                    if (mSelectedExpertise.trim().isEmpty())
                                        mSelectedExpertise = String.valueOf(listExpertise.get(j).getId());
                                    else
                                        mSelectedExpertise = mSelectedExpertise + "," + String.valueOf(listExpertise.get(j).getId());
                                }
                            }
                        }

                    }

                    switch (expertiseName) {
                        case "":
                            mSelectedExpertise = String.valueOf(listExpertise.get(0).getId());
                            expertiseName = listExpertise.get(0).getValue();
                            arraySelectionExpertise[0] = true;
                            break;
                    }


                    getView().getExpertiseSpinnerView().setAllCheckedText(expertiseName);
                    for (int k = 0; k < arraySelectionExpertise.length; k++) {
                        System.out.println(k + "K----boolean" + arraySelectionExpertise[k]);
                        getView().getExpertiseSpinnerView().selectItem(k, arraySelectionExpertise[k]);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(SpProfileModel saveObj) {
        SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_fullname), saveObj.getData().getFullName());
        SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_email), saveObj.getData().getEmail());
        SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_address), saveObj.getData().getAddress());
        SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_profile_pic), saveObj.getData().getProfileImage200X200());
        //Change Menu option and make all fields non-editable
        Utils.getInstance().showToast(saveObj.getMessage());
        getView().getName().setText(saveObj.getData().getFullName());
        if (isFirstTimeUser) {
            //Navigate to service provider Dashboard
            Intent intent = new Intent(getActivity(), SPDashboardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            getActivity().startActivity(intent);
        }
        if (getActivity() instanceof SPDashboardActivity) {
            ((SPDashboardActivity) getActivity()).showUserInfo();
            enableDisableUI(false);
            ((SPProfileFragment) getView()).editMenu.setVisible(true);
            ((SPProfileFragment) getView()).saveMenu.setVisible(false);
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t != null && t.getMessage() != null)
                    Utils.getInstance().showToast(t.getMessage());
                else
                    Utils.getInstance().showToast(getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param isEditable if true make all fields editable else non-editable
     */
    public void enableDisableUI(boolean isEditable) {
        if (getActivity() != null && isViewAttached()) {
            //apply animation
            if (Utils.getInstance().isEqualLollipop()) {
                TransitionManager.beginDelayedTransition((ViewGroup) getView().getRootView(), new AutoTransition());
            }
            if (isEditable) {
                getView().getName().setEnabled(true);
                getView().getAddress().setEnabled(true);
                getView().getProfileImg().setEnabled(true);
                getView().getEditProfile().show();

                getView().getGenderRadioGroup().setEnabled(true);
                getView().getMaleRadio().setEnabled(true);
                getView().getFemaleRadio().setEnabled(true);

                getView().getLicenseRadioGroup().setEnabled(true);
                getView().getLicenseYesRadio().setEnabled(true);
                getView().getLicenseNoRadio().setEnabled(true);

                getView().getBondedRadioGroup().setEnabled(true);
                getView().getBondedYesRadio().setEnabled(true);
                getView().getBondedNoRadio().setEnabled(true);

                getView().getInsuredRadioGroup().setEnabled(true);
                getView().getInsuredYesRadio().setEnabled(true);
                getView().getInsuredNoRadio().setEnabled(true);

                getView().getWebsite().setEnabled(true);
                getView().getWebsite().setFocusable(true);
                getView().getWebsite().setFocusableInTouchMode(true);
                getView().getWebsite().requestFocus();

                getView().getLicenseNumber().setEnabled(true);
                getView().getLicenseNumber().setFocusable(true);
                getView().getLicenseNumber().setFocusableInTouchMode(true);
                getView().getLicenseNumber().requestFocus();

                getView().getCompany().setEnabled(true);
                getView().getCompany().setFocusable(true);
                getView().getCompany().setFocusableInTouchMode(true);
                getView().getCompany().requestFocus();

                getView().getName().setEnabled(true);
                getView().getName().setFocusable(true);
                getView().getName().setFocusableInTouchMode(true);
                getView().getName().requestFocus();

                getView().getLicenseNumber().setEnabled(true);

                getView().getWebsite().setEnabled(true);

                getView().getExpertiseSpinnerView().setEnabled(true);

                getView().getFullNameLayoutView().setEnabled(true);


                /*labels*/
                getView().getTVBondedView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getTVInsuredView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getTVGenderView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getTVLicensedView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getTVExpertiseView().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                getView().getTVExpertiseView().setText(R.string.title_select_expertise);

                getView().getLineExpertiseView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));


            } else {
                getView().getName().setEnabled(false);
                getView().getAddress().setEnabled(false);
                getView().getProfileImg().setEnabled(false);
                getView().getEditProfile().hide();

                getView().getGenderRadioGroup().setEnabled(false);
                getView().getMaleRadio().setEnabled(false);
                getView().getFemaleRadio().setEnabled(false);

                getView().getLicenseRadioGroup().setEnabled(false);
                getView().getLicenseYesRadio().setEnabled(false);
                getView().getLicenseNoRadio().setEnabled(false);

                getView().getBondedRadioGroup().setEnabled(false);
                getView().getBondedYesRadio().setEnabled(false);
                getView().getBondedNoRadio().setEnabled(false);

                getView().getInsuredRadioGroup().setEnabled(false);
                getView().getInsuredYesRadio().setEnabled(false);
                getView().getInsuredNoRadio().setEnabled(false);

                getView().getCompany().setEnabled(false);
                getView().getCompany().setFocusable(false);

                getView().getLicenseNumber().setEnabled(false);
                getView().getLicenseNumber().setFocusable(false);

                getView().getWebsite().setEnabled(false);
                getView().getWebsite().setFocusable(false);

                getView().getExpertiseSpinnerView().setEnabled(false);

                getView().getFullNameLayoutView().setEnabled(false);


                /*labels*/
                getView().getTVBondedView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getTVInsuredView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getTVGenderView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getTVLicensedView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getTVExpertiseView().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                getView().getTVExpertiseView().setText(R.string.title_expertise);
                getView().getLineExpertiseView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.view_color_detail));
            }
        }
    }

    private void updateView(SpProfileModel profileObj) {
        if (getActivity() != null && isViewAttached()) {
            if (!TextUtils.isEmpty(profileObj.getData().getProfileImage200X200())) {
                // set image
                Picasso.with(getActivity()).load(profileObj.getData().getProfileImage200X200()).into(getView().getProfileImg(), new Callback() {
                    @Override
                    public void onSuccess() {
                        if (getActivity() != null && isViewAttached())
                            getView().getProfileProgress().setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        if (getActivity() != null && isViewAttached()) {
                            getView().getProfileImg().setImageResource(R.drawable.profile_img);
                            getView().getProfileProgress().setVisibility(View.GONE);
                        }
                    }
                });
            } else {
                //show default image
                getView().getProfileImg().setImageResource(R.drawable.profile_img);
            }
            getView().getName().setText(profileObj.getData().getFullName());
            getView().getEmail().setText(profileObj.getData().getEmail());

            detailExpertise = new String[profileObj.getData().getExpertise().size()];
            for (int i = 0; i < profileObj.getData().getExpertise().size(); i++) {
                detailExpertise[i] = profileObj.getData().getExpertise().get(i).getExpertiseId();
            }

            //set gender selectable
            if (!TextUtils.isEmpty(profileObj.getData().getGender())) {
                if (profileObj.getData().getGender().equals("male")) {
                    getView().getMaleRadio().setChecked(true);
                    getView().getFemaleRadio().setChecked(false);
                    gender = "male";
                } else if (profileObj.getData().getGender().equals("female")) {
                    getView().getMaleRadio().setChecked(false);
                    getView().getFemaleRadio().setChecked(true);
                    gender = "female";
                }
            }

            licensed = String.valueOf(profileObj.getData().getIsLicensed());
            if (licensed.equals("1")) {
                getView().getLicenseYesRadio().setChecked(true);
                getView().getLicenseNoRadio().setChecked(false);
                getView().getLicenseNumber().setText(profileObj.getData().getLicenseNo());
                getView().getLicenseNumberLayoutView().setVisibility(View.VISIBLE);
            } else if (licensed.equals("0")) {
                getView().getLicenseYesRadio().setChecked(false);
                getView().getLicenseNoRadio().setChecked(true);
                getView().getLicenseNumber().setText(profileObj.getData().getLicenseNo());
                getView().getLicenseNumberLayoutView().setVisibility(View.GONE);
            }

            insured = String.valueOf(profileObj.getData().getInsured());
            if (insured.equals("1")) {
                getView().getInsuredYesRadio().setChecked(true);
                getView().getInsuredNoRadio().setChecked(false);
            } else if (insured.equals("0")) {
                getView().getInsuredYesRadio().setChecked(false);
                getView().getInsuredNoRadio().setChecked(true);
            }

            bonded = String.valueOf(profileObj.getData().getBonded());
            if (bonded.equals("1")) {
                getView().getBondedYesRadio().setChecked(true);
                getView().getBondedNoRadio().setChecked(false);

            } else if (bonded.equals("0")) {
                getView().getBondedYesRadio().setChecked(false);
                getView().getBondedNoRadio().setChecked(true);
            }

            getView().getWebsite().setText(profileObj.getData().getWebsite());
            getView().getCompany().setText(profileObj.getData().getBusinessName());

            if (!TextUtils.isEmpty(profileObj.getData().getAddress()))
                getView().getAddress().setText(profileObj.getData().getAddress());

            latitude = profileObj.getData().getLatitude();
            longitude = profileObj.getData().getLongitude();

        }
    }


    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        String extension = "";
        int i = list.get(0).getDisplayName().lastIndexOf('.');
        if (i > 0) {
            extension = list.get(0).getDisplayName().substring(i + 1);
        }
        if (!extension.equals("png") && !extension.equals("jpg") && !extension.equals("jpeg")) {
            getActivity().showSnakBar(getView().getRootView(), "you can choose only images.");
            return;
        }
        ChosenImage image = list.get(0);
        isImageSelected = true;
        profileImagePath = FileUtils.getPath(getActivity(), Uri.parse(image.getQueryUri()));
        getView().getProfileProgress().setVisibility(View.VISIBLE);


        Picasso.with(getActivity()).load(new File(profileImagePath)).into(getView().getProfileImg(), new Callback() {
            @Override
            public void onSuccess() {
                getView().getProfileProgress().setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                getView().getProfileProgress().setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onError(String s) {
        Utils.getInstance().showToast(s);
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 102: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Show option to update profile
                    if (cameraSelected) {
                        takePicture();
                    } else if (gallerySelected) {
                        pickImageSingle();
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Utils.getInstance().showToast("Camera/Storage permission Denied");
                }
                return;
            }

        }
    }

}
