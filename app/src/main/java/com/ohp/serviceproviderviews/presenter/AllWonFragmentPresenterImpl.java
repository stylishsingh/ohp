package com.ohp.serviceproviderviews.presenter;

import android.content.Context;

import com.library.mvp.FragmentPresenter;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.enums.ApiName;
import com.ohp.serviceproviderviews.view.AllWonProjectsView;

import retrofit2.Response;

/**
 * Created by vishal.sharma on 7/28/2017.
 */

public class AllWonFragmentPresenterImpl extends FragmentPresenter<AllWonProjectsView, APIHandler> implements APIResponseInterface.OnCallableResponse {
    public AllWonFragmentPresenterImpl(AllWonProjectsView allWonProjectsView, Context context) {
        super(allWonProjectsView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {

    }

    @Override
    public void onFailure(Throwable t, ApiName api) {

    }
}
