package com.ohp.serviceproviderviews.presenter;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.serviceproviderviews.adapter.GetNearestProjectAdapter;
import com.ohp.serviceproviderviews.beans.GetNearestProjectsResponse;
import com.ohp.serviceproviderviews.fragment.GetNearestProjectsFragment;
import com.ohp.serviceproviderviews.view.GetNearestProjectView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * @author Amanpal Singh.
 */

public class GetNearestProjectsFragmentPresenterImpl extends FragmentPresenter<GetNearestProjectView, APIHandler> implements APIResponseInterface.OnCallableResponse, LocationListener, View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {

    public boolean isSearched = false;
    public int page = 1;
    // GPSTracker gps;
    private GetNearestProjectsResponse getNearestProjectsResponse;
    private int permsRequestCode = 200;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private String searchQuery = "";
    private String stringLongitude = "", stringLatitude = "";
    private GetNearestProjectAdapter getNearestProjectAdapter;
    private List<GetNearestProjectsResponse.DataBean> listNearestProjectsResponses = new ArrayList<>();
    private double longitude, latitude;
    private BroadcastReceiver nearProjectsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction().equals(Constants.BROADCAST_FOR_NEAR_PROJECTS)) {
                page = 1;
                System.out.println("DashboardActivityPresenterImpl.onReceive");
                listNearestProjectsResponses.clear();
                getNearestProjectAdapter.updateList(listNearestProjectsResponses);
                if (isViewAttached()) {
                    browseAllProjects();
                }
            }
        }
    };

    // Handler to get the current location
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            getLastLocation();
        }
    };
    private final BroadcastReceiver mGpsDetectionReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            page = 1;
            listNearestProjectsResponses.clear();
            getNearestProjectAdapter.updateList(listNearestProjectsResponses);
            if (intent.getExtras().getBoolean("isGpsEnabled", false)) {
                getLastLocation();
            } else {
                stringLatitude = "";
                stringLongitude = "";
                browseAllProjects();
            }
        }
    };

    public GetNearestProjectsFragmentPresenterImpl(GetNearestProjectView getNearestProjectView, Context context) {
        super(getNearestProjectView, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(nearProjectsReceiver,
                    new IntentFilter(Constants.BROADCAST_FOR_NEAR_PROJECTS));
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            getView().viewProjectsList().setLayoutManager(linearLayoutManager);
            getView().viewProjectsList().setNestedScrollingEnabled(false);

            getNearestProjectAdapter = new GetNearestProjectAdapter(getActivity(), listNearestProjectsResponses);
            getView().viewProjectsList().setAdapter(getNearestProjectAdapter);

            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

                            if (getNearestProjectsResponse != null && getNearestProjectsResponse.getTotalRecord() > listNearestProjectsResponses.size()) {

                                System.out.println("total Records" + getNearestProjectsResponse.getTotalRecord());
                                page += 1;
                                browseAllProjects();
                            }
                        }
                    }
                }
            });
            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });
            getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s != null && s.length() > 0) {
                        getView().getCloseBtn().setVisibility(View.VISIBLE);
                        searchQuery = s.toString();
                        page = 1;
                        isSearched = true;
                        browseAllProjects();

                    } else {
                        searchQuery = "";
                        page = 1;
                        isSearched = true;
                        browseAllProjects();
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        getView().getCloseBtn().setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            mLocationRequest = generateLocationRequest();
            buildGoogleApiClient();
            connectToGoogleApiClient();
            getLastLocation();
        }

    }

    private void browseAllProjects() {
        try {
            if (getActivity() != null && isViewAttached() && Utils.getInstance().isInternetAvailable(getActivity())) {
                if (TextUtils.isEmpty(searchQuery))
                    getActivity().showProgressDialog();
                Map<String, String> params = new HashMap<>();
                params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                if (!TextUtils.isEmpty(searchQuery))
                    params.put(Constants.SEARCH_PARAM, searchQuery);
                params.put(Constants.PAGE_PARAM, String.valueOf(page));
                params.put(Constants.LATITUDE, stringLatitude);
                params.put(Constants.LONGITUDE, stringLongitude);
                getInterActor().getAllNearestProjects(params, ApiName.GET_NEAREST_PROJECTS);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }


    @Override
    public void onSuccess(Response response, ApiName api) {

        try {
            if (getActivity() != null && isViewAttached()) {
                if (getActivity().isShowing())
                    getActivity().hideProgressDialog();
                switch (api) {
                    case GET_NEAREST_PROJECTS:
                        getNearestProjectsResponse = (GetNearestProjectsResponse) response.body();
                        switch (getNearestProjectsResponse.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(getNearestProjectsResponse);
                                break;
                            case Constants.STATUS_201:
                                break;
                            case Constants.STATUS_202:
                                ((OwnerDashboardActivity) getActivity()).logout(getNearestProjectsResponse.getMessage());
                                break;

                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void onSuccess(GetNearestProjectsResponse getNearestProjectsResponse) {

        try {
            if (getActivity() != null && isViewAttached()) {
                if (getNearestProjectsResponse.getData() != null) {
                    if (getNearestProjectsResponse.getData().size() > 0) {
                        getView().setEmptyLayout().setVisibility(View.GONE);
                        if (isSearched) {
                            listNearestProjectsResponses = new ArrayList<>();
                            listNearestProjectsResponses.addAll(getNearestProjectsResponse.getData());
                            isSearched = false;
                        } else if (getNearestProjectsResponse.getTotalRecord() > listNearestProjectsResponses.size()) {
                            listNearestProjectsResponses.addAll(getNearestProjectsResponse.getData());
                        }
                        getNearestProjectAdapter.updateList(listNearestProjectsResponses);
                    } else {
                        if (listNearestProjectsResponses.isEmpty()) {
                            getNearestProjectAdapter.clearAll();
                            getView().setEmptyLayout().setVisibility(View.VISIBLE);
                        } else {
                            if (isSearched || searchQuery.length() != 0) {
                                getNearestProjectAdapter.clearAll();
                                getView().setEmptyLayout().setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.btn_clear:
                    searchQuery = "";
                    page = 1;
                    getView().getSearchEditText().setText("");
                    break;
            }
        }

    }

    /*********************************************************************************************/

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null && isViewAttached()) {
            getActivity().unregisterReceiver(mGpsDetectionReceiver);
        }
    }

    /**
     * function to initialize the Google API Client
     */
    private synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null)
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
    }

    /**
     * function to get the last known location of device
     */
    private void getLastLocation() {

        if (getActivity() != null && isViewAttached()) {
            if (!getLastKnownLocationIfAllowed())
                checkLocationPermission();
            else {
                Location lastKnowLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (lastKnowLocation != null) { // last location received
                    if (handler != null && runnable != null)
                        handler.removeCallbacks(runnable);
                    getLocationData(lastKnowLocation);

                } else { // last location not received
                    if (handler != null && runnable != null)
                        handler.postDelayed(runnable, 3000);
                }
            }
        }
    }

    private void getLocationData(Location lastKnownLocation) {
        try {
            longitude = lastKnownLocation.getLongitude();
            latitude = lastKnownLocation.getLatitude();

            stringLongitude = String.valueOf(longitude);
            stringLatitude = String.valueOf(latitude);
            browseAllProjects();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        try {
            if (getActivity() != null && isViewAttached()) {
                switch (requestCode) {
                    case 200:
                        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                            // Or use LocationManager.GPS_PROVIDER
                            System.out.println("permission location granted--->" + getLastKnownLocationIfAllowed());
                            if (getLastKnownLocationIfAllowed())
                                checkLocationPermission();

                        } else {
                            page = 1;
                            listNearestProjectsResponses.clear();
                            getNearestProjectAdapter.updateList(listNearestProjectsResponses);
                            stringLatitude = "";
                            stringLongitude = "";
                            browseAllProjects();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @TargetApi(Build.VERSION_CODES.M)
    public void checkLocationPermission() {
        try {
            if (getActivity() != null && isViewAttached()) {
                int hasPermission;
                hasPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
                if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                    ((GetNearestProjectsFragment) getView()).requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            permsRequestCode);
                    return;
                }
                getLastLocation();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean getLastKnownLocationIfAllowed() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disconnectFromGoogleApiClient();
        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
    }

    /**
     * function to connect to Google API Client
     */
    private void connectToGoogleApiClient() {
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    /**
     * function to disconnect from Google API Client
     */
    private void disconnectFromGoogleApiClient() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
// register for location updates
        buildLocationSettingsRequest(mLocationRequest, mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * function to build the request dialog for checking the Location
     *
     * @param mLocationRequest location request
     * @param mGoogleApiClient google api client
     * @param resultCallback   callback
     */
    private void buildLocationSettingsRequest(LocationRequest mLocationRequest, GoogleApiClient mGoogleApiClient, ResultCallback<LocationSettingsResult> resultCallback) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        LocationSettingsRequest mLocationSettingsRequest = builder.build();

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(resultCallback);
    }


    // function to create mLocation Request
    private LocationRequest generateLocationRequest() {
        // Create the LocationRequest object
        LocationRequest mLocationRequest = LocationRequest.create();
        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(5000);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(1000);
        return mLocationRequest;
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                getLastLocation();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                try {
                    status.startResolutionForResult(getActivity(), 1);
                } catch (IntentSender.SendIntentException e) {
//                    Toast.makeText(getActivity(), "User choose not to make required location settings changes.", Toast.LENGTH_SHORT).show();
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                /*Toast.makeText(getActivity(), "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.", Toast.LENGTH_SHORT).show();*/
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case 1:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getLastLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        page = 1;
                        listNearestProjectsResponses.clear();
                        getNearestProjectAdapter.updateList(listNearestProjectsResponses);
                        stringLatitude = "";
                        stringLongitude = "";
                        browseAllProjects();
                        break;
                }
                break;
        }
    }


}
