package com.ohp.serviceproviderviews.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.JobTypeExpertiseSpinnerAdapter;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.beans.JobTypeAndExpertise;
import com.ohp.enums.ApiName;
import com.ohp.serviceproviderviews.adapter.ProviderListAdapter;
import com.ohp.serviceproviderviews.beans.ProvidersListResponse;
import com.ohp.serviceproviderviews.beans.TeamMembersModel;
import com.ohp.serviceproviderviews.fragment.AddEditTeamMemberFragment;
import com.ohp.serviceproviderviews.view.AddEditTeamMemberView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;
import com.ohp.utils.Validations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.ohp.enums.ApiName.ADD_EDIT_TEAM_MEMBER;
import static com.ohp.enums.ApiName.ADD_TEAM_MEMBER;
import static com.ohp.enums.ApiName.GET_PROVIDER_LIST;
import static com.ohp.enums.ApiName.GET_RESOURCES_API;
import static com.ohp.utils.Constants.CLICK_ADD_TEAM;
import static com.ohp.utils.Constants.CLICK_EDIT_TEAM;
import static com.ohp.utils.Constants.CLICK_VIEW_TEAM;

/**
 * @author vishal.sharma .
 */

public class AddEditTeamMemberPresenterImpl extends FragmentPresenter<AddEditTeamMemberView, APIHandler> implements APIResponseInterface.OnCallableResponse, View.OnClickListener {
    private boolean isSearched = false, isTeamMember = false;
    private JobTypeExpertiseSpinnerAdapter jobTypeExpertiseAdapter;
    private List<JobTypeAndExpertise.DataBean.InfoBean> listExpertise = new ArrayList<>();
    public MultiSelectSpinner.MultiSpinnerListener expertiseSpinnerListener = new MultiSelectSpinner.MultiSpinnerListener() {

        public void onItemsSelected(boolean[] selected) {
            mSelectedExpertise = "";
            expertiseName = "";
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    if (mSelectedExpertise.trim().isEmpty()) {
                        mSelectedExpertise = String.valueOf(listExpertise.get(i).getId());
                        expertiseName = listExpertise.get(i).getValue();
                    } else {
                        mSelectedExpertise = mSelectedExpertise + "," + String.valueOf(listExpertise.get(i).getId());
                        expertiseName = expertiseName + "," + listExpertise.get(i).getValue();
                    }
                }
            }
            // arraySelectionExpertise=new boolean[listExpertise.size()];
            if (getActivity() != null && isViewAttached()) {

                //  if(arraySelectionExpertise==null)arraySelectionExpertise = new boolean[listExpertise.size()];

                if (mSelectedExpertise.isEmpty()) {
                    /*mSelectedExpertise = String.valueOf(listExpertise.get(0).getId());
                    expertiseName = listExpertise.get(0).getValue();
                    arraySelectionExpertise[0] = true;
                    getView().getExpertiseSpinnerView().setAllCheckedText(expertiseName);
                    getView().getExpertiseSpinnerView().selectItem(0, arraySelectionExpertise[0]);*/
                } else {
                    if (getView().getExpertiseSpinnerView().isSelectAll())
                        getView().getExpertiseSpinnerView().setAllCheckedText(expertiseName);
                }
            }
        }
    };
    private ArrayAdapter expertiseAdapter;
    private int PLACE_PICKER_REQUEST = 1;
    private String selectedExpertiseName = "", expertiseName = "", mSelectedExpertise = "", memberType = "2", Provider_id = "", Full_Name = "", Email = "", Address = "";
    private boolean[] arraySelectionExpertise;
    private String[] detailExpertise;
    private Place place;
    private String searchQuery = "";
    private String longitude = "", latitude = "";
    private int page = 1;
    private TeamMembersModel.DataBean dataObj;
    private ProviderListAdapter providerListAdapter;
    private List<ProvidersListResponse.DataBean> providersList = new ArrayList<>();
    private ProvidersListResponse providersListResponse;
    private boolean isitemClick = false;
    private String currentScreen = "";

    public AddEditTeamMemberPresenterImpl(AddEditTeamMemberView addEditTeamMemberView, Context context) {
        super(addEditTeamMemberView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && isViewAttached()) {
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");

            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    ((HolderActivity) getActivity()).oneStepBack();
                }
            });
        }

        //set radio group click listener
        if (getActivity() != null && isViewAttached()) {
            getView().getRadioGroup().setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                    switch (checkedId) {
                        case R.id.radio_teamMember:
                            getView().getTeamMemberRadio().setChecked(true);
                            getView().getContractorRadio().setChecked(false);
                            memberType = "2";
                            break;
                        case R.id.radio_contractor:
                            getView().getTeamMemberRadio().setChecked(false);
                            getView().getContractorRadio().setChecked(true);
                            memberType = "1 ";
                            break;
                    }
                }
            });

            getResource();

            getView().getMemberName().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s != null && s.length() > 0) {
                        searchQuery = s.toString();
                        page = 1;
                        //   isTeamMember = false;
                        if (currentScreen.equals(Constants.CLICK_ADD_TEAM)) {
                            if (!isSearched) {
                                getProvidersList();
                            }
                            isSearched = false;
                        }

                    } else {

                        isProvidersVisible(false);
                        if (currentScreen.equals(Constants.CLICK_ADD_TEAM)) {
                            searchQuery = "";
                            page = 1;
                            isSearched = false;
                            getProvidersList();
                        }

                    }
                }


                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getProviderList().setLayoutManager(lm);

            providerListAdapter = new ProviderListAdapter(getActivity(), null, this);
            getView().getProviderList().setAdapter(providerListAdapter);
        }


    }

    private void getProvidersList() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.PAGE_PARAM, String.valueOf(page));
                param.put(Constants.SEARCH_PARAM, searchQuery);
                getInterActor().getProviderList(param, GET_PROVIDER_LIST);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }


    }

    private void getResource() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                getActivity().showProgressDialog();

                param.put(Constants.TYPE, "jobtype_expertise");
                getInterActor().getResourcesJobAndExpertise(param, GET_RESOURCES_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    public void AddTeamMember() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {


              /*  if (isTeamMember) {
                    if (TextUtils.isEmpty(Provider_id)) {
                        getActivity().showSnakBar(getView().getRootView(), "Please enter Provider Id");
                    } else {
                        mSelectedExpertise = "";
                        getActivity().showProgressDialog();
                        Map<String, String> param = new HashMap<>();
                        param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                        param.put(Constants.FULL_NAME, Full_Name);
                        param.put(Constants.EMAIL, Email);
                        param.put(Constants.EXPERTISE, mSelectedExpertise);
                        param.put(Constants.ADDRESS, Address);
                        param.put(Constants.ROLE, String.valueOf(2));
                        param.put(Constants.PROVIDER_ID, Provider_id);
                        param.put(Constants.TYPE_STATUS, memberType);
                        param.put(Constants.LATITUDE, latitude);
                        param.put(Constants.LONGITUDE, longitude);
                        getInterActor().addTeamMember(param, ADD_TEAM_MEMBER);
                    }
                } else {*/
                Full_Name = getView().getMemberName().getText().toString();
                Email = getView().getMemberEmail().getText().toString();
                Address = getView().getMemberAddress().getText().toString();
                String response = Validations.getInstance().validateAddTeamMember(Full_Name, Email, mSelectedExpertise, Address);
                if (!response.trim().isEmpty()) {
                    getActivity().showSnakBar(getView().getRootView(), response);
                } else {
                    getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    param.put(Constants.FULL_NAME, Full_Name);
                    param.put(Constants.EMAIL, Email);
                    param.put(Constants.EXPERTISE, mSelectedExpertise);
                    param.put(Constants.ADDRESS, Address);
                    param.put(Constants.ROLE, String.valueOf(2));
                    param.put(Constants.PROVIDER_ID, Provider_id);
                    param.put(Constants.TYPE_STATUS, memberType);
                    param.put(Constants.LATITUDE, latitude);
                    param.put(Constants.LONGITUDE, latitude);
                    getInterActor().addTeamMember(param, ADD_TEAM_MEMBER);

                }


            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));

            }
        }
    }

    public void EditTeamMember() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Full_Name = getView().getMemberName().getText().toString();
                Email = getView().getMemberEmail().getText().toString();
                Address = getView().getMemberAddress().getText().toString();

                String response = Validations.getInstance().validateAddTeamMember(Full_Name, Email, mSelectedExpertise, Address);
                if (!response.trim().isEmpty()) {
                    getActivity().showSnakBar(getView().getRootView(), response);
                } else {
                    getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    param.put(Constants.FULL_NAME, Full_Name);
                    param.put(Constants.EMAIL, Email);
                    param.put(Constants.EXPERTISE, mSelectedExpertise);
                    param.put(Constants.ADDRESS, Address);
                    param.put(Constants.ROLE, String.valueOf(2));
                    param.put(Constants.PROVIDER_ID, Provider_id);
                    param.put(Constants.TYPE_STATUS, memberType);
                    param.put(Constants.LATITUDE, latitude);
                    param.put(Constants.LONGITUDE, longitude);
                    getInterActor().addEditTeamMember(param, ADD_EDIT_TEAM_MEMBER);
                }
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }

        }

    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            getActivity().hideProgressDialog();
            switch (api) {
                case GET_RESOURCES_API:
                    if (response.isSuccessful()) {
                        getActivity().hideProgressDialog();
                        JobTypeAndExpertise resourcesObj = (JobTypeAndExpertise) response.body();
                        switch (resourcesObj.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(resourcesObj);
                                break;
                            case Constants.STATUS_201:
                                if (jobTypeExpertiseAdapter != null) {
                                    jobTypeExpertiseAdapter.updateList(getActivity(), listExpertise);
                                }
                                break;
                            case Constants.STATUS_202:
                                ((HolderActivity) getActivity()).logout(resourcesObj.getMessage());
                                break;
                        }
                    }
                    break;
                case GET_PROVIDER_LIST:
                    if (response.isSuccessful()) {
                        providersListResponse = (ProvidersListResponse) response.body();
                        switch (providersListResponse.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(providersListResponse);
                                break;
                            case Constants.STATUS_201:
                                //hide recyclerview and show scrollview
                                isProvidersVisible(false);
                                break;
                            case Constants.STATUS_202:
                                //hide recyclerview and show scrollview
                                isProvidersVisible(false);
                                break;
                        }
                    }
                    break;
                case ADD_TEAM_MEMBER:
                    if (response.isSuccessful()) {
                        GenericBean genericBeanObj = (GenericBean) response.body();
                        switch (genericBeanObj.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(genericBeanObj);
                                break;
                            case Constants.STATUS_201:
                                getActivity().showSnakBar(getView().getRootView(), genericBeanObj.getMessage());
                                break;
                            case Constants.STATUS_202:
                                ((HolderActivity) getActivity()).logout(genericBeanObj.getMessage());
                                break;
                        }
                    }
                    break;
                case ADD_EDIT_TEAM_MEMBER:
                    if (response.isSuccessful()) {
                        GenericBean genericBeanObj = (GenericBean) response.body();
                        switch (genericBeanObj.getStatus()) {
                            case Constants.STATUS_200:
                                onSuccess(genericBeanObj);
                                break;
                            case Constants.STATUS_201:
                                getActivity().showSnakBar(getView().getRootView(), genericBeanObj.getMessage());
                                break;
                            case Constants.STATUS_202:
                                ((HolderActivity) getActivity()).logout(genericBeanObj.getMessage());
                                break;
                        }
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(GenericBean genericBeanObj) {
        try {
            if (getActivity() != null && isViewAttached()) {
                Intent listIntent = new Intent(Constants.GET_TEAM_MEMBERS);
                getActivity().sendBroadcast(listIntent);

                ((HolderActivity) getActivity()).oneStepBack();
            }
        } catch (Exception e) {

        }

    }

    private void onSuccess(ProvidersListResponse providersListResponse) {
        if (providersListResponse.getData() != null) {
            if (providersListResponse.getData().size() > 0) {
                getView().getExpertiseSpinnerView().setEnabled(false);
                getView().getMemberEmail().setEnabled(false);
                //  if(providersList==null)providersList=new ArrayList<>();
                // getView().getMemberName().setText(providersListResponse.getData().get(0));

                providersList = providersListResponse.getData();
                providerListAdapter.updateList(providersList);
                getView().getProviderList().bringToFront();

                //hide recyclerview and show scrollview
                isProvidersVisible(true);
            } else {
                getView().getMemberEmail().setText("");
                getView().getMemberAddress().setText("");
                getView().getExpertiseSpinnerView().setEnabled(true);
                getView().getMemberEmail().setEnabled(true);
                getView().getMemberEmail().setFocusable(true);
                getView().getMemberEmail().setFocusableInTouchMode(true);
                //hide recyclerview and show scrollview
                detailExpertise = null;
                setExpertise();
                isProvidersVisible(false);
            }
        }

    }

    private void onSuccess(JobTypeAndExpertise resourcesObj) {
        try {
            listExpertise = new ArrayList<>();
            if (!listExpertise.isEmpty())
                listExpertise.clear();

            List<String> listExpert = new ArrayList<>();

            listExpertise.addAll(resourcesObj.getData().get(1).get(0).getInfo());
            for (int i = 0; i < listExpertise.size(); i++) {
                listExpert.add(listExpertise.get(i).getValue());
            }

            expertiseAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_multiple_choice, listExpert);
            getView().getExpertiseSpinnerView().setListAdapter(expertiseAdapter).setAllCheckedText(selectedExpertiseName);

            if (currentScreen.equals(CLICK_VIEW_TEAM) || currentScreen.equals(CLICK_EDIT_TEAM))
                setExpertise();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setExpertise() {
        if (!listExpertise.isEmpty()) {
            arraySelectionExpertise = new boolean[listExpertise.size()];
            for (int j = 0; j < listExpertise.size(); j++) {
                if (detailExpertise != null) {
                    for (int i = 0; i < detailExpertise.length; i++) {
                        if (detailExpertise[i].equals(String.valueOf(listExpertise.get(j).getId()))) {
                            arraySelectionExpertise[j] = true;
                            if (expertiseName.trim().isEmpty())
                                expertiseName = String.valueOf(listExpertise.get(j).getValue());
                            else
                                expertiseName = expertiseName + "," + String.valueOf(listExpertise.get(j).getValue());

                            if (mSelectedExpertise.trim().isEmpty())
                                mSelectedExpertise = String.valueOf(listExpertise.get(j).getId());
                            else
                                mSelectedExpertise = mSelectedExpertise + "," + String.valueOf(listExpertise.get(j).getId());
                        }
                    }
                } else {
                    arraySelectionExpertise[j] = false;
                    expertiseName = "Select Expertise";
                    mSelectedExpertise = "";
                }
            }

            getView().getExpertiseSpinnerView().setAllCheckedText(expertiseName);
            for (int k = 0; k < arraySelectionExpertise.length; k++) {
                System.out.println(k + "K----boolean" + arraySelectionExpertise[k]);
                getView().getExpertiseSpinnerView().selectItem(k, arraySelectionExpertise[k]);
            }
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached())
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
        getActivity().showProgressDialog();
        try {
            PlacePicker.IntentBuilder placeBuilder = new PlacePicker.IntentBuilder();
            ((AddEditTeamMemberFragment) getView()).startActivityForResult(placeBuilder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
            getActivity().hideProgressDialog();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (getActivity() != null && isViewAttached()) {
            getActivity().hideProgressDialog();
            if (resultCode == RESULT_OK) {
                if (requestCode == PLACE_PICKER_REQUEST) {
                    place = PlacePicker.getPlace(data, getActivity());
                    if (place != null) {
                        getView().getMemberAddress().setText(place.getAddress());
                        longitude = String.valueOf(place.getLatLng().longitude);
                        latitude = String.valueOf(place.getLatLng().latitude);
                    }
                }
            }
        }
    }

    public void onItemClick(int Position) {
        if (getActivity() != null && isViewAttached() && !TextUtils.isEmpty(String.valueOf(Position))) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            ProvidersListResponse.DataBean dataObj = providersListResponse.getData().get(Position);
            isSearched = true;
            // isTeamMember = true;
            getView().getMemberName().setText(dataObj.getFullName());
            getView().getMemberEmail().setText(dataObj.getEmail());
            getView().getMemberAddress().setText(dataObj.getAddress());
            latitude = dataObj.getLatitude();
            longitude = dataObj.getLongitude();

            detailExpertise = new String[dataObj.getExpertise().size()];
            for (int i = 0; i < dataObj.getExpertise().size(); i++) {
                detailExpertise[i] = String.valueOf(dataObj.getExpertise().get(i).getExpertiseId());
            }
            Provider_id = String.valueOf(dataObj.getUserId());
            providerListAdapter.clearAll();
            setExpertise();
            //   getView().getExpertiseSpinnerView().setAllCheckedText(providersList.get(layoutPosition).getExpertise().get(layoutPosition).getExpertiseTitle());
            //hide recyclerview and show scrollview
            isProvidersVisible(false);
        }
    }

    private void isProvidersVisible(boolean check) {
        if (getActivity() != null && isViewAttached()) {
            if (!check) {
                getView().getProviderList().setVisibility(View.GONE);
                getView().getNestedScrollView().setVisibility(View.VISIBLE);
            } else {
                getView().getProviderList().setVisibility(View.VISIBLE);
                getView().getNestedScrollView().setVisibility(View.GONE);
            }
        }
    }

    public void saveBundleInfo(Bundle bundle) {
        try {
            if (getActivity() != null && isViewAttached() && bundle != null) {
                currentScreen = bundle.getString(Constants.DESTINATION, "");
                //   Provider_id = bundle.getString(Constants.MEMBER_ID);
                if (bundle.getString(Constants.MEMBER_ID) != null) {
                    dataObj = bundle.getParcelable(Constants.TEAM_MEMBER_OBJ);
                    if (dataObj != null) {
                        Provider_id = String.valueOf(dataObj.getUserId());
                        longitude = dataObj.getLongitude();
                        latitude = dataObj.getLatitude();
                        isSearched = true;
                    }
                }


                switch (currentScreen) {
                    case CLICK_VIEW_TEAM:
                        getView().getMemberEmail().setFocusable(false);
                        getView().getExpertiseSpinnerView().setEnabled(false);
                        getView().getMemberName().setFocusable(false);
                        getView().getMemberAddress().setFocusable(false);
                        getView().getMemberAddress().setOnClickListener(null);
                        getView().getContractorRadio().setEnabled(false);
                        getView().getTeamMemberRadio().setEnabled(false);
                        getView().getRadioGroup().setOnCheckedChangeListener(null);
                        getView().getToolbarTitle().setText("Team Member Details");
                        getView().getMemberHint().setHint("Member Name");
                        getView().getMemberEmailHint().setHint("Email");
                        getView().getExpertiseHint().setText(R.string.title_expertise);
                        getView().getExpertiseHint().setTextColor(ContextCompat.getColor(getActivity(), R.color.disabled_color));
                        getView().getMemberAddressHint().setHint("Address");
                        setDetailData();
                        break;
                    case CLICK_EDIT_TEAM:
                        setViewVisibleForEdit();
                        isSearched = true;
                        getView().getMemberHint().setHint("Enter Member Name");
                        getView().getMemberEmailHint().setHint("Invite By Email");
                        getView().getExpertiseHint().setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                        getView().getExpertiseHint().setText(R.string.title_select_expertise);
                        getView().getToolbarTitle().setText("Edit Team Member");
                        getView().getMemberAddressHint().setHint("Enter Address");
                        setDetailData();
                        break;
                    case CLICK_ADD_TEAM:
                        getView().getToolbarTitle().setText("Add Team Member");

                        break;

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setDetailData() {
        if (getActivity() != null && isViewAttached()) {
            isSearched = true;
            getView().getMemberName().setText(dataObj.getName());
            getView().getMemberEmail().setText(dataObj.getEmail());

            detailExpertise = new String[dataObj.getExpertise().size()];
            for (int i = 0; i < dataObj.getExpertise().size(); i++) {
                detailExpertise[i] = String.valueOf(dataObj.getExpertise().get(i).getExpertiseId());
            }
            getView().getMemberAddress().setText(dataObj.getAddress());
            if (dataObj.getTypeStatus() == 1) {
                getView().getContractorRadio().setChecked(true);
                getView().getTeamMemberRadio().setChecked(false);
            } else {
                getView().getTeamMemberRadio().setChecked(true);
                getView().getContractorRadio().setChecked(false);
            }
        }

    }

    public void setViewVisibleForEdit() {
        getView().getMemberName().setFocusable(true);
        getView().getMemberName().requestFocus();
        getView().getMemberName().setFocusableInTouchMode(true);
        getView().getMemberAddress().setFocusable(false);
        // getView().getMemberAddress().requestFocus();
        getView().getMemberAddress().setOnClickListener(this);
        // getView().getMemberAddress().setFocusableInTouchMode(true);
        getView().getMemberEmail().setFocusable(false);
        getView().getExpertiseSpinnerView().setEnabled(false);
        getView().getExpertiseSpinnerView().setListener(null);
        getView().getContractorRadio().setEnabled(true);
        getView().getTeamMemberRadio().setEnabled(true);
    }

    public void setData(TeamMembersModel.DataBean dataObj) {

        getView().getMemberName().setText(dataObj.getName());
        getView().getMemberEmail().setText(dataObj.getEmail());

        detailExpertise = new String[dataObj.getExpertise().size()];
        for (int i = 0; i < dataObj.getExpertise().size(); i++) {
            detailExpertise[i] = String.valueOf(dataObj.getExpertise().get(i).getExpertiseId());
        }
        setExpertise();
        getView().getMemberAddress().setText(dataObj.getAddress());
        if (dataObj.getTypeStatus() == 1) {
            getView().getContractorRadio().setChecked(true);
            getView().getTeamMemberRadio().setChecked(false);
        } else {
            getView().getTeamMemberRadio().setChecked(true);
            getView().getContractorRadio().setChecked(false);
        }
    }
}
