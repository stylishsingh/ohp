package com.ohp.serviceproviderviews.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.commonviews.adapter.DashboardPagerAdapter;
import com.ohp.serviceproviderviews.activities.SPDashboardActivity;
import com.ohp.serviceproviderviews.fragment.DashboardAllProjectsFragment;
import com.ohp.serviceproviderviews.fragment.SpEventListFragment;
import com.ohp.serviceproviderviews.fragment.TaskFragment;
import com.ohp.serviceproviderviews.fragment.TeamMembersFragment;
import com.ohp.serviceproviderviews.view.SpDashboardFragmentView;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Amanpal Singh.
 */

public class SpDashboardFragmentPresenterImpl extends FragmentPresenter<SpDashboardFragmentView, APIHandler> implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener {

    public DashboardPagerAdapter pagerAdapter;

    public SpDashboardFragmentPresenterImpl(SpDashboardFragmentView spDashboardFragmentView, Context context) {
        super(spDashboardFragmentView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return null;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            ((SPDashboardActivity) getActivity()).getToolbarTitleView().setText(R.string.title_dashboard);
        }
        setDashboardPagerAdapter();
    }

    private void setDashboardPagerAdapter() {
        //Creating our pager adapter
        if (isViewAttached()) {
            String titles[] = new String[]{"Tasks", "Projects", "Events", "Teams"};
            List<Fragment> fragmentList = new ArrayList<>();
            fragmentList.add(new TaskFragment());
            fragmentList.add(new DashboardAllProjectsFragment());
            fragmentList.add(new SpEventListFragment());
            fragmentList.add(new TeamMembersFragment());
            DashboardPagerAdapter pagerAdapter = new DashboardPagerAdapter(getActivity().getSupportFragmentManager(), fragmentList, titles);
            getView().getViewPager().setAdapter(pagerAdapter);
            this.pagerAdapter = pagerAdapter;
        }

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
