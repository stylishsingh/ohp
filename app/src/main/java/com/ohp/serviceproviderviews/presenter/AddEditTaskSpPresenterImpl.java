package com.ohp.serviceproviderviews.presenter;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.FilePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.ProjectSpinnerAdapter;
import com.ohp.commonviews.adapter.ReminderAdapter;
import com.ohp.commonviews.adapter.ThumbImageAdapter;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.beans.GetPropertyResponse;
import com.ohp.commonviews.beans.ResourceBean;
import com.ohp.commonviews.beans.ThumbnailBean;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.service.ImagesResultReceiver;
import com.ohp.service.ImagesService;
import com.ohp.serviceproviderviews.adapter.TeamMemberSpinnerAdapter;
import com.ohp.serviceproviderviews.beans.AddEditTaskResponse;
import com.ohp.serviceproviderviews.beans.GetTaskDetailResponse;
import com.ohp.serviceproviderviews.beans.GetTeamMemberResponse;
import com.ohp.serviceproviderviews.fragment.AddEditTaskFragmentSp;
import com.ohp.serviceproviderviews.view.AddEditTaskViewSp;
import com.ohp.utils.CalendarPicker;
import com.ohp.utils.Constants;
import com.ohp.utils.FileUtils;
import com.ohp.utils.PermissionsAndroid;
import com.ohp.utils.Utils;
import com.ohp.utils.Validations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.ohp.enums.ApiName.ADD_EDIT_TASK;
import static com.ohp.enums.ApiName.GET_EVENT_REMINDER_API;
import static com.ohp.enums.ApiName.GET_TEAM_MEMBER;
import static com.ohp.utils.Constants.CLICK_ADD_TASK;
import static com.ohp.utils.Constants.CLICK_EDIT_TASK;
import static com.ohp.utils.Constants.CLICK_VIEW_TASK;
import static com.ohp.utils.Constants.NAVIGATE_FROM;

/**
 * @author Vishal Sharma.
 */

public class AddEditTaskSpPresenterImpl extends FragmentPresenter<AddEditTaskViewSp, APIHandler> implements APIResponseInterface.OnCallableResponse
        , View.OnClickListener, ImagePickerCallback, ImagesResultReceiver.Receiver {
    private final BroadcastReceiver taskDetailReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null && intent.getExtras().containsKey(Constants.NAVIGATE_FROM)
                    && Constants.NOTIFICATIONS.equalsIgnoreCase(intent.getExtras().getString(NAVIGATE_FROM))) {
                if (taskId.equalsIgnoreCase(intent.getExtras().getString(Constants.TASK_ID)))
                    getTaskDetail(taskId);
            } else
                getTaskDetail(taskId);
            Intent listIntent = new Intent(Constants.TASK_LIST_UPDATE);
            context.sendBroadcast(listIntent);
        }
    };
    public boolean isOccurrence, isTaskRepeat;
    private boolean bgApp = false;
    private ProjectSpinnerAdapter projectAdapter;
    private ReminderAdapter reminderEventAdapter;
    private TeamMemberSpinnerAdapter teamMemberSpinnerAdapter;
    private List<ResourceBean.DataBean.InfoBean> listEventReminder = new ArrayList<>();
    private List<GetTeamMemberResponse.DataBean> listTeamMember = new ArrayList<>();
    private List<GetPropertyResponse.DataBean> listProperties = new ArrayList<>(), listProjects = new ArrayList<>(), listMembers = new ArrayList<>();
    private String pickerPath, currentScreen, taskName = "", propertyName = "", project_id = "", property_id = "", taskNotes = "", task_time_from = "", task_time_to = "", task_reminder = "", task_repeat = "", member_id = "", taskId = "", isDeleted;
    private List<ThumbnailBean> uploadImageList = new ArrayList<>();
    private List<ThumbnailBean> detailImageList = new ArrayList<>();
    private List<String> deletedImageIds = new ArrayList<>();
    private ThumbImageAdapter detailImageAdapter, editImageAdapter;
    private List<ThumbnailBean> tempUploadImageList = new ArrayList<>();
    private boolean isImageSelected = false;
    private boolean isCameraSelected = false;
    private boolean isGallerySelected = false;
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private FilePicker filePicker;

    public AddEditTaskSpPresenterImpl(AddEditTaskViewSp addEditTaskViewSp, Context context) {
        super(addEditTaskViewSp, context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null && isViewAttached()) {
            getActivity().unregisterReceiver(taskDetailReceiver);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            if (savedInstanceState != null) {
                if (savedInstanceState.containsKey("picker_path")) {
                    pickerPath = savedInstanceState.getString("picker_path");
                }
            }
            getActivity().registerReceiver(taskDetailReceiver, new IntentFilter(Constants.GET_TASK_DETAIL));

            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            getView().getToolbar().setVisibility(View.VISIBLE);
            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    if (bgApp)
                        ((HolderActivity) getActivity()).moveToParentActivity();
                    else
                        ((HolderActivity) getActivity()).oneStepBack();
                }
            });


            // initialize spinner
            projectAdapter = new ProjectSpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listProjects);
            projectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerProjectView().setAdapter(projectAdapter);


            reminderEventAdapter = new ReminderAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listEventReminder);
            reminderEventAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerRepeatEventView().setAdapter(reminderEventAdapter);

            teamMemberSpinnerAdapter = new TeamMemberSpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listTeamMember);
            teamMemberSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            getView().getSpinnerTeamMemberView().setAdapter(teamMemberSpinnerAdapter);


            if (uploadImageList == null) uploadImageList = new ArrayList<>();
            LinearLayoutManager lm1 = new LinearLayoutManager(getContext());
            lm1.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getViewEditProjectRecycler().setLayoutManager(lm1);

            ThumbnailBean obj = new ThumbnailBean();
            obj.setAddView(true);
            obj.setFile(false);
            obj.setImagePath("");
            uploadImageList.add(obj);
            editImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, true);
            getView().getViewEditProjectRecycler().setAdapter(editImageAdapter);


            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getViewProjectRecycler().setLayoutManager(lm);

            if (detailImageList == null) detailImageList = new ArrayList<>();
            ThumbnailBean obj1 = new ThumbnailBean();
            obj1.setAddView(true);
            obj1.setFile(false);
            obj1.setImagePath("");
            detailImageList.add(obj1);
            detailImageAdapter = new ThumbImageAdapter(getActivity(), null, this, false, false);
            getView().getViewProjectRecycler().setAdapter(detailImageAdapter);
        }

        getView().getSpinnerProjectView().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Utils.getInstance().showToast(listProjects.get(position).getPropertyName());
                if (listProjects.size() > 0 && listProjects != null) {
                    if (position == 0) {
                        getView().getEdt_PropertyView().setText("");
                        propertyName = "";
                        project_id = "";
                        getView().getEdt_PropertyView().setText("");
                    } else {
                        if (TextUtils.isEmpty(listProjects.get(position - 1).getPropertyName())) {
                            property_id = "";
                            getView().getEdt_PropertyView().setText("");
                            getView().getEdt_PropertyView().setEnabled(true);
                            getView().getEdt_PropertyView().setFocusableInTouchMode(true);
                        } else {
                            propertyName = listProjects.get(position - 1).getPropertyName();
                            getView().getEdt_PropertyView().setText(propertyName);
                            getView().getEdt_PropertyView().setEnabled(false);
                            property_id = TextUtils.isEmpty(String.valueOf(listProjects.get(position - 1).getPropertyId())) ? "" : String.valueOf(listProjects.get(position - 1).getPropertyId());
                        }

                        project_id = String.valueOf(listProjects.get(position - 1).getValue());

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_PROJECT_LIST:
                        if (response.isSuccessful()) {
                            GetPropertyResponse responseObj = (GetPropertyResponse) response.body();
                            switch (responseObj.getStatus()) {
                                case 200:
                                    onSuccess(responseObj, ApiName.GET_PROJECT_LIST);
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());

                                    break;
                            }
                        }
                        break;
                    case GET_EVENT_REMINDER_API:
                        if (response.isSuccessful()) {
                            ResourceBean responseObj = (ResourceBean) response.body();
                            switch (responseObj.getStatus()) {
                                case 200:
                                    onSuccess(responseObj, GET_EVENT_REMINDER_API);
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());
                                    break;
                            }
                        }
                        break;

                    case GET_TEAM_MEMBER:
                        if (response.isSuccessful()) {
                            GetTeamMemberResponse responseObj = (GetTeamMemberResponse) response.body();
                            switch (responseObj.getStatus()) {
                                case 200:
                                    onSuccess(responseObj);
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());

                                    break;
                            }
                        }
                        break;
                    case ADD_EDIT_TASK:
                        if (response.isSuccessful()) {
                            AddEditTaskResponse genericBean = (AddEditTaskResponse) response.body();
                            switch (genericBean.getStatus()) {
                                case 200:
                                    onSuccess(genericBean);
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((HolderActivity) getActivity()).logout(genericBean.getMessage());
                                    break;
                                case 203:
                                    getActivity().showSnackBar(getView().getRootView(), genericBean.getMessage());
                                    break;
                            }
                        }
                        break;
                    case GET_TASK_DETAIL:
                        if (response.isSuccessful()) {
                            GetTaskDetailResponse getTaskDetailResponse = (GetTaskDetailResponse) response.body();
                            switch (getTaskDetailResponse.getStatus()) {
                                case 200:
                                    onSuccess(getTaskDetailResponse);
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((HolderActivity) getActivity()).logout(getTaskDetailResponse.getMessage());
                                    break;
                            }
                            getProjectsList();
                            getTaskReminder();
                            getTeamMember();
                        }
                        break;
                    case MARK_COMPLETED_TASK:
                        if (response.isSuccessful()) {
                            GenericBean responseObj = (GenericBean) response.body();
                            switch (responseObj.getStatus()) {
                                case 200:
                                    onSuccess(responseObj);
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());
                                    break;
                            }
                        }

                        break;
                    case REOPEN_TASK:
                        if (response.isSuccessful()) {
                            GenericBean responseObj = (GenericBean) response.body();
                            switch (responseObj.getStatus()) {
                                case 200:
                                    onSuccess(responseObj);
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());
                                    break;
                            }
                        }
                        break;
                    case COMPLETE_TASK:
                        if (response.isSuccessful()) {
                            GenericBean responseObj = (GenericBean) response.body();
                            switch (responseObj.getStatus()) {
                                case 200:
                                    onSuccess(responseObj);
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((HolderActivity) getActivity()).logout(responseObj.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(GenericBean responseObj) {
        if (getActivity() != null && isViewAttached()) {
            Intent listIntent = new Intent(Constants.TASK_LIST_UPDATE);
            getActivity().sendBroadcast(listIntent);
            getTaskDetail(taskId);
        }
    }


    private void onSuccess(GetTaskDetailResponse dataObj) {
        if (getActivity() != null && isViewAttached() && dataObj.getData() != null) {

            getView().getTaskTittleView().setText(dataObj.getData().getTaskName());
            getView().getTaskTittle().setText(dataObj.getData().getTaskName());
            getView().getTaskNotesView().setText(dataObj.getData().getTaskNotes());
            getView().getTaskNotes().setText(dataObj.getData().getTaskNotes());
            getView().getTVTaskFromTime().setText(Utils.getInstance().getYMDEventFormattedTime(dataObj.getData().getTaskTimeFrom().replace("+00:00", "")));
            getView().getEtDateTimeFromView().setText(Utils.getInstance().getYMDEventFormattedTime(dataObj.getData().getTaskTimeFrom().replace("+00:00", "")));
            if (!dataObj.getData().getTaskTimeTo().isEmpty()) {
                getView().gettvTaskToTime().setText(Utils.getInstance().getYMDEventFormattedTime(dataObj.getData().getTaskTimeTo().replace("+00:00", "")));
                getView().getEtDateTimeToView().setText(Utils.getInstance().getYMDEventFormattedTime(dataObj.getData().getTaskTimeTo().replace("+00:00", "")));
            }
            getView().getProjectView().setText(dataObj.getData().getProject().getProjectName());
            getView().getPropertyView().setText(dataObj.getData().getProperty().getPropertyName());
            getView().getTeamMemberView().setText(dataObj.getData().getMember().getMemberName());
            if (!TextUtils.isEmpty(dataObj.getData().getTaskReminder())) {
                getView().getRepeatReminder().setText(Utils.getInstance().getYMDEventFormattedTime(dataObj.getData().getTaskReminder().replace("+00:00", "")));
                getView().getRepeatReminderView().setText(Utils.getInstance().getYMDEventFormattedTime(dataObj.getData().getTaskReminder().replace("+00:00", "")));
            }

            project_id = String.valueOf(dataObj.getData().getProject().getProjectId());
            member_id = String.valueOf(dataObj.getData().getMember().getMemberId());
            task_repeat = String.valueOf(dataObj.getData().getTaskRepeat());


            if (dataObj.getData().getTaskImages() != null && dataObj.getData().getTaskImages().size() > 0) {
                if (detailImageList == null) detailImageList = new ArrayList<>();
                if (detailImageList.size() > 0) detailImageList.clear();
                if (!uploadImageList.isEmpty()) {
                    uploadImageList.clear();
                }

                for (int i = 0; i < dataObj.getData().getTaskImages().size(); i++) {
                    ThumbnailBean obj = new ThumbnailBean();
                    obj.setAddView(false);
                    obj.setImageID(String.valueOf(dataObj.getData().getTaskImages().get(i).getImageId()));
                    obj.setFile(false);
                    obj.setCheck(ScreenNavigation.OLD_ITEM);
                    obj.setImagePath(dataObj.getData().getTaskImages().get(i).getImage());
                    detailImageList.add(obj);
                    uploadImageList.add(obj);
                }

                // Add View after All Info in uploadImageList for adding data in edit view
                ThumbnailBean obj = new ThumbnailBean();
                obj.setAddView(true);
                obj.setFile(false);
                obj.setImagePath("");
                obj.setCheck(ScreenNavigation.DUMMY_ITEM);
                uploadImageList.add(obj);

                System.out.println("Images List" + detailImageList.size());
                System.out.println("Upload Images List" + uploadImageList.size());
                detailImageAdapter = new ThumbImageAdapter(getActivity(), detailImageList, this, false, false);
                getView().getViewProjectRecycler().setAdapter(detailImageAdapter);
                //detailImageAdapter.updateList(detailImageList);
                editImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, true);
                getView().getViewEditProjectRecycler().setAdapter(editImageAdapter);

            } else {
                getView().getNoImages().setVisibility(View.VISIBLE);
            }

            switch (Utils.getInstance().getRole(getActivity())) {
                case 1:
                    if (dataObj.getData().getTaskStatus() == 4) {
                        getView().getTVReopenView().setVisibility(View.VISIBLE);
                        getView().getReopenDoneView().setVisibility(View.VISIBLE);
                        getView().getTVDoneView().setVisibility(View.VISIBLE);
                        getView().getNSView().scrollTo(0, 0);
                    } else
                        getView().getReopenDoneView().setVisibility(View.GONE);
                    break;
                case 2:
                    if (dataObj.getData().getTaskType() == 2) {
                        if (dataObj.getData().getTaskStatus() == 1) {
                            getView().getTVMArkCompletedView().setVisibility(View.VISIBLE);
                            getView().getNSView().scrollTo(0, 0);
                        } else
                            getView().getTVMArkCompletedView().setVisibility(View.GONE);
                    } else {
                        if (dataObj.getData().getTaskStatus() == 1) {
                            getView().getTVReopenView().setVisibility(View.GONE);
                            getView().getReopenDoneView().setVisibility(View.VISIBLE);
                            getView().getTVDoneView().setVisibility(View.VISIBLE);
                            getView().getNSView().scrollTo(0, 0);
                        } else if (dataObj.getData().getTaskStatus() == 5) {
                            getView().getTVReopenView().setVisibility(View.VISIBLE);
                            getView().getReopenDoneView().setVisibility(View.VISIBLE);
                            getView().getTVDoneView().setVisibility(View.VISIBLE);
                            getView().getNSView().scrollTo(0, 0);
                        } else {
                            getView().getReopenDoneView().setVisibility(View.GONE);
                        }

                    }

                    break;
            }


        }
    }

    private void onSuccess(AddEditTaskResponse genericBean) {
        //  Utils.getInstance().showToast(genericBean.getMessage());

        Intent listIntent = new Intent(Constants.TASK_LIST_UPDATE);
        getActivity().sendBroadcast(listIntent);
        if (!TextUtils.isEmpty(String.valueOf(genericBean.getData().getTaskId())))
            uploadTaskImages(String.valueOf(genericBean.getData().getTaskId()));

        ((HolderActivity) getActivity()).oneStepBack();
    }

    private void uploadTaskImages(String taskId) {
        try {
            System.out.println("upload Images" + uploadImageList.size());
            uploadImageList.remove(uploadImageList.size() - 1);
            getActivity().startService(createCallingIntent(taskId));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Intent createCallingIntent(String taskId) {
        Intent i = new Intent(getActivity(), ImagesService.class);
        ImagesResultReceiver receiver = new ImagesResultReceiver(new Handler());
        receiver.setReceiver(this);
        i.putExtra(Constants.RECEIVER, receiver);
        i.putExtra(Constants.SCREEN, Constants.CLICK_ADD_TASK);
        i.putParcelableArrayListExtra(Constants.IMAGE_LIST, (ArrayList<? extends Parcelable>) tempUploadImageList);
        i.putExtra(Constants.TASK_ID, taskId);
        return i;
    }

    private void onSuccess(GetTeamMemberResponse responseObj) {
        try {
            if (responseObj.getData().size() > 0) {
                listTeamMember = responseObj.getData();
                teamMemberSpinnerAdapter.updateList(getActivity(), listTeamMember);
                for (int i = 0; i < listTeamMember.size(); i++) {
                    if (member_id.equals(String.valueOf(listTeamMember.get(i).getValue()))) {
                        getView().getSpinnerTeamMemberView().setSelection(i + 1);
                        return;
                    }
                }
            } else {
                teamMemberSpinnerAdapter.updateListNoMember(getActivity());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(ResourceBean dataObj, ApiName check) {
        try {
            if (getActivity() != null && isViewAttached()) {
                switch (check) {
                    case GET_EVENT_REMINDER_API:
                        if (reminderEventAdapter != null) {
                            getView().getSpinnerProjectView().setVisibility(View.VISIBLE);
                            listEventReminder = dataObj.getData().get(0).getInfo();
                            reminderEventAdapter.updateListEventTask(getActivity(), listEventReminder);
                            for (int i = 0; i < listEventReminder.size(); i++) {
                                if (task_repeat.equalsIgnoreCase(String.valueOf(listEventReminder.get(i).getID()))) {
                                    getView().getSpinnerRepeatEventView().setSelection(i + 1);
                                    getView().getRepeatTaskView().setText(listEventReminder.get(i).getValue());
                                    return;
                                }
                            }

                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(GetPropertyResponse dataObj, ApiName check) {
        try {
            if (getActivity() != null && isViewAttached()) {
                switch (check) {
                    case GET_PROJECT_LIST:
                        if (projectAdapter != null) {
                            getView().getSpinnerProjectView().setVisibility(View.VISIBLE);
                            listProjects = dataObj.getData();
                            projectAdapter.updateList(getActivity(), listProjects);
                            for (int i = 0; i < dataObj.getData().size(); i++) {
                                if (project_id.equals(String.valueOf(dataObj.getData().get(i).getValue()))) {
                                    getView().getSpinnerProjectView().setSelection(i + 1);
                                    return;
                                }
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveBundleInfo(Bundle arguments) {
        if (getActivity() != null && isViewAttached() && arguments != null) {
            taskId = arguments.getString(Constants.TASK_ID);
            bgApp = arguments.getBoolean(Constants.BG_APP);
            currentScreen = arguments.getString(Constants.DESTINATION, "");
            isOccurrence = arguments.getBoolean(Constants.OCCURRENCE, false);
            isTaskRepeat = arguments.getBoolean(Constants.TASK_REPEAT_EMPTY, false);
            switch (currentScreen) {
                case CLICK_ADD_TASK:
                    getView().getToolbarTitle().setText("Add Task");
                    getProjectsList();
                    getTaskReminder();
                    getTeamMember();
                    break;
                case CLICK_EDIT_TASK:
                    getView().getToolbarTitle().setText("Edit Task");
                    hideViewForEditTaskDetail();
                    break;
                case CLICK_VIEW_TASK:
                    getView().getToolbarTitle().setText("Task Details");
                    hideViewForViewTaskDetail();
                    break;
            }
            if (!TextUtils.isEmpty(taskId))
                getTaskDetail(taskId);
        }
    }

    private void getTaskDetail(String task_id) {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.TASK_ID, task_id);
                getInterActor().getTaskDetail(param, ApiName.GET_TASK_DETAIL);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.tv_date_time_from:
                    CalendarPicker.getInstance().showCalendar(getActivity(), this, v.getId(), getView().getTVTaskFromTime().getText().toString().trim());
                    break;
                case R.id.tv_date_time_to:
                    CalendarPicker.getInstance().showCalendar(getActivity(), this, v.getId(), getView().getEtDateTimeToView().getText().toString().trim());
                    break;
                case R.id.tv_repeat_reminder_time:
                    System.out.println("getView().getRepeatReminderView().getText().toString() = " + getView().getRepeatReminderView().getText().toString());
                    CalendarPicker.getInstance().showCalendar(getActivity(), this, v.getId(), getView().getRepeatReminder().getText().toString().trim());
                    break;
                case R.id.tv_mark_completed:
                    markCompletedTask(taskId);
                    break;
                case R.id.tv_reopen:
                    reopenTask(taskId);
                    break;
                case R.id.tv_done:
                    completeTask(taskId);
                    break;
            }
        }
    }

    private void markCompletedTask(String taskID) {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.TASK_ID, taskID);
                getInterActor().markCompletedTask(param, ApiName.MARK_COMPLETED_TASK);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    private void completeTask(String taskID) {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.TASK_ID, taskID);
                getInterActor().completeTask(param, ApiName.COMPLETE_TASK);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    private void reopenTask(String taskID) {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.TASK_ID, taskID);
                getInterActor().reopenTask(param, ApiName.REOPEN_TASK);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    public void setEventReminderDateTime(String dateTime, int viewId) {
        System.out.println("dateTime = " + dateTime);
        if (getActivity() != null && isViewAttached()) {
            switch (viewId) {
                case R.id.tv_date_time_from:
                    getView().getTVTaskFromTime().setText(dateTime);
                    break;
                case R.id.tv_date_time_to:
                    getView().gettvTaskToTime().setText(dateTime);
                    break;
                case R.id.tv_repeat_reminder_time:
                    getView().getRepeatReminder().setText(dateTime);
                    break;
            }
        }

    }

    private void getProjectsList() {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.PROPERTY_ID, "");
                getInterActor().getProjectListing(param, ApiName.GET_PROJECT_LIST);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    private void getTaskReminder() {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                Map<String, String> param = new HashMap<>();
                getActivity().showProgressDialog();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.TYPE, Constants.TASK_REPEAT);
                getInterActor().getResources(param, GET_EVENT_REMINDER_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    private void getTeamMember() {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                Map<String, String> param = new HashMap<>();
                getActivity().showProgressDialog();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                getInterActor().getTeamMember(param, GET_TEAM_MEMBER);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    public void AddEditTask() {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());

                // project_id = listProjects.size() == 0 ? "" : String.valueOf(listProjects.get(getView().getSpinnerProjectView().getSelectedItemPosition()).getValue() - 1);
                // property_id = "";

                taskNotes = getView().getTaskNotes().getText().toString().trim();
                taskName = getView().getTaskTittle().getText().toString().trim();
                task_time_from = getView().getTVTaskFromTime().getText().toString();
                task_time_to = getView().gettvTaskToTime().getText().toString();
                task_reminder = getView().getRepeatReminder().getText().toString();
                propertyName = getView().getEdt_PropertyView().getText().toString().trim();
                if (getView().getSpinnerRepeatEventView().getSelectedItemPosition() != 0)
                    task_repeat = listEventReminder.size() == 0 ? "" : String.valueOf(listEventReminder.get(getView().getSpinnerRepeatEventView().getSelectedItemPosition() - 1).getID());
                else
                    task_repeat = "";
                if (getView().getSpinnerTeamMemberView().getSelectedItemPosition() != 0)
                    member_id = listTeamMember.size() == 0 ? "" : String.valueOf(listTeamMember.get(getView().getSpinnerTeamMemberView().getSelectedItemPosition() - 1).getValue());
                else
                    member_id = "";
                String response = Validations.getInstance().validateTaskFields(taskName, task_time_from, task_time_to, task_reminder);
                if (!response.isEmpty()) {
                    getActivity().showSnackBar(getView().getRootView(), response);
                } else {
                    getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));


                    if (currentScreen.equals(Constants.CLICK_EDIT_TASK) || currentScreen.equals(Constants.CLICK_VIEW_TASK)) {
                        param.put(Constants.TASK_ID, taskId);
                    }


                    String imageIds = "";
                    for (int i = 0; i < deletedImageIds.size(); i++) {
                        if (!deletedImageIds.get(i).trim().isEmpty())
                            if (i == 0) {
                                imageIds = deletedImageIds.get(i);
                            } else {
                                imageIds = imageIds + "," + deletedImageIds.get(i);
                            }
                    }


                    if (!tempUploadImageList.isEmpty()) {
                        tempUploadImageList.clear();
                    }
                    if (currentScreen.equals(Constants.CLICK_EDIT_TASK) || currentScreen.equals(Constants.CLICK_VIEW_TASK)) {
                        for (int i = 0; i < uploadImageList.size(); i++) {
                            if (uploadImageList.get(i).getCheck().equals(ScreenNavigation.ADD_ITEM)) {
                                ThumbnailBean beanObj = new ThumbnailBean();
                                beanObj.setAddView(uploadImageList.get(i).isAddView());
                                beanObj.setCheck(uploadImageList.get(i).getCheck());
                                beanObj.setExtension(uploadImageList.get(i).getExtension());
                                beanObj.setFile(uploadImageList.get(i).isFile());
                                beanObj.setName(uploadImageList.get(i).getName());
                                beanObj.setImagePath(uploadImageList.get(i).getImagePath());
                                tempUploadImageList.add(beanObj);
                            }
                        }
                    } else {
                        if (uploadImageList.size() > 0) {
                            tempUploadImageList.addAll(uploadImageList);
                            tempUploadImageList.remove(tempUploadImageList.size() - 1);
                        }
                    }


                    param.put(Constants.PROJECT_ID, project_id);
                    param.put(Constants.PROPERTY_ID, property_id);
                    param.put(Constants.PROPERTY_NAME, propertyName);
                    param.put(Constants.TASK_NOTES, taskNotes);
                    param.put(Constants.TASK_NAME, taskName);
                    param.put(Constants.TASK_TIME_FROM, task_time_from);
                    param.put(Constants.TASK_TIME_TO, task_time_to);
                    param.put(Constants.TASK_REMINDER, task_reminder);
                    param.put(Constants.TASK_REPEAT, task_repeat);

                    param.put(Constants.IS_DELETED, imageIds);
                    param.put(Constants.MEMBER_ID, member_id);
                    if (isOccurrence) {
                        if (currentScreen.equals(CLICK_ADD_TASK)) {
                            if (!task_repeat.isEmpty())
                                param.put(Constants.CHANGE_IN_SERIES, "1");
                            else param.put(Constants.CHANGE_IN_SERIES, "0");
                        } else
                            param.put(Constants.CHANGE_IN_SERIES, "0");

                    } else {
                        if (!task_repeat.isEmpty())
                            param.put(Constants.CHANGE_IN_SERIES, "1");
                        else
                            param.put(Constants.CHANGE_IN_SERIES, "0");
                    }
                    //save user current timeZone value will be like Asia/Kolkata
                    param.put(Constants.APP_TIME_ZONE, Utils.getInstance().getAppTimeZone());

                    getInterActor().addEditTask(param, ADD_EDIT_TASK);
                }
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(getActivity());
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(getActivity());
                    cameraPicker.setImagePickerCallback(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            } else if (requestCode == Picker.PICK_FILE) {
                filePicker.submit(data);
            }
        } else if (resultCode == RESULT_CANCELED) {
            Utils.getInstance().showToast("Request Cancelled");
        }

    }

    public void onRequestPermissionResult(int requestCode, int[] grantResults) {
        switch (requestCode) {
            case 102: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Show option to update profile
                    if (isCameraSelected) {
                        takePicture();
                    } else if (isGallerySelected) {
                        pickImageSingle();
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Utils.getInstance().showToast("Camera/Storage permission Denied");
                }
                return;
            }

        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        ChosenImage image = list.get(0);
        isImageSelected = true;
        String extension = "";
        int i = list.get(0).getDisplayName().lastIndexOf('.');
        if (i > 0) {
            extension = list.get(0).getDisplayName().substring(i + 1);
        }
        //Add Images info to list
        if (!extension.equals("png") && !extension.equals("jpg") && !extension.equals("jpeg")) {
            getActivity().showSnakBar(getView().getRootView(), "you can choose only images.");
            return;
        }
        if (uploadImageList.size() > 0) {
            uploadImageList.remove(uploadImageList.size() - 1);
            editImageAdapter.notifyItemRemoved(uploadImageList.size());
        }
        ThumbnailBean obj = new ThumbnailBean();
        obj.setExtension(extension);
        obj.setImagePath(FileUtils.getPath(getActivity(), Uri.parse(image.getQueryUri())));
        obj.setImageID("");
        obj.setName(image.getDisplayName());
        obj.setFile(true);
        obj.setCheck(ScreenNavigation.ADD_ITEM);
        obj.setAddView(false);
        uploadImageList.add(obj);

        if (editImageAdapter != null) {
            ThumbnailBean obj1 = new ThumbnailBean();
            obj1.setAddView(true);
            obj1.setFile(false);
            obj1.setImagePath("");
            obj1.setFile(false);
            uploadImageList.add(obj1);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    editImageAdapter.notifyDataSetChanged();
                }
            });
            getView().getViewEditProjectRecycler().smoothScrollToPosition(uploadImageList.size() - 1);
        }
    }

    @Override
    public void onError(String s) {
        Utils.getInstance().showToast(s);
    }

    public void onAddOptionClick() {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Choose Option");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            //choose camera
                            try {
                                isCameraSelected = true;
                                isGallerySelected = false;
                                checkExternalStoragePermission();
                            } catch (ActivityNotFoundException e) {
                                e.printStackTrace();
                                String errorMessage = "Your device doesn't support capturing images";
                                Utils.getInstance().showToast(errorMessage);
                            }
                            break;
                        case 1:
                            //choose gallery
                            try {
                                isCameraSelected = false;
                                isGallerySelected = true;
                                checkExternalStoragePermission();
                            } catch (Exception e) {
                                String errorMessage = "There are no images!";
                                Utils.getInstance().showToast(errorMessage);
                            }
                            break;
                        case 2:
                            dialog.dismiss();
                            break;
                    }
                }
            });
            builder.show();
        }
    }

    private void checkExternalStoragePermission() {
        boolean isExternalStorage = PermissionsAndroid.getInstance().checkCameraPermission(getActivity());
        if (!isExternalStorage) {
            PermissionsAndroid.getInstance().requestForCameraPermission(((AddEditTaskFragmentSp) getView()));
        } else if (isCameraSelected) {
            takePicture();
        } else if (isGallerySelected) {
            pickImageSingle();
        }
    }

    private void takePicture() {
        cameraPicker = new CameraImagePicker(((AddEditTaskFragmentSp) getView()));
//        cameraPicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        cameraPicker.setImagePickerCallback(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        pickerPath = cameraPicker.pickImage();
    }

    private void pickImageSingle() {
        imagePicker = new ImagePicker(((AddEditTaskFragmentSp) getView()));
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();
    }


    public void deleteImage(final int position) {
        if (getActivity() != null && isViewAttached()) {

            Utils.getInstance().confirmDialog(getActivity(), "Are you sure, you want to delete this image?", "delete", new Utils.ConfirmDialogCallbackInterface() {
                @Override
                public void onYesClick(String tag) {
                    if (deletedImageIds == null) deletedImageIds = new ArrayList<>();
                    deletedImageIds.add(uploadImageList.get(position).getImageID());
                    uploadImageList.remove(position);
                    getView().getViewEditProjectRecycler().setAdapter(editImageAdapter);
                }

                @Override
                public void onNoClick(String tag) {

                }
            });
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

    }

    public void hideViewForEditTaskDetail() {
        getView().getEditTask().setVisibility(View.VISIBLE);
        getView().getViewTask().setVisibility(View.GONE);
    }

    public void hideViewForViewTaskDetail() {
        getView().getEditTask().setVisibility(View.GONE);
        getView().getViewTask().setVisibility(View.VISIBLE);
    }
}
