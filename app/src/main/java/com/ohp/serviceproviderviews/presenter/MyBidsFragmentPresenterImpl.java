package com.ohp.serviceproviderviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.serviceproviderviews.adapter.MyBidsAdapter;
import com.ohp.serviceproviderviews.beans.MyBidsResponse;
import com.ohp.serviceproviderviews.view.MyBidsView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * @author Amanpal Singh.
 */

public class MyBidsFragmentPresenterImpl extends FragmentPresenter<MyBidsView, APIHandler> implements APIResponseInterface.OnCallableResponse, View.OnClickListener {
    public int page = 0;
    public boolean isFilterChanged = false, isSearched = false;
    private List<MyBidsResponse.DataBean> listResponse = new ArrayList<>();
    private String searchQuery = "";  //for search query
    private BroadcastReceiver bidsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction().equals(Constants.BROADCAST_FOR_BIDS)) {
                System.out.println("DashboardActivityPresenterImpl.onReceive");
                //  Toast.makeText(context, " for bids broadcast", Toast.LENGTH_SHORT).show();
                if (isViewAttached()) {
                    getMyBids();
                }
            }
        }
    };
    private MyBidsAdapter myBidsAdapter;
    private int adapterPosition;
    private MyBidsResponse myBidsResponse;

    public MyBidsFragmentPresenterImpl(MyBidsView myBidsView, Context context) {
        super(myBidsView, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().registerReceiver(bidsReceiver,
                new IntentFilter(Constants.BROADCAST_FOR_BIDS));
        if (getActivity() != null && isViewAttached()) {
            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.VERTICAL);
            getView().bidListView().setLayoutManager(lm);
            getView().bidListView().setNestedScrollingEnabled(false);
            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

                            if (myBidsResponse != null && myBidsResponse.getTotalRecord() > listResponse.size()) {

                                System.out.println("total Records" + myBidsResponse.getTotalRecord());
                                page += 1;
                                getMyBids();
                            }
                        }
                    }
                }
            });
            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });
            getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s != null && s.length() > 0) {
                        getView().getCloseBtn().setVisibility(View.VISIBLE);
                        searchQuery = s.toString();
                        page = 1;
                        isSearched = true;
                        getMyBids();

                    } else {
                        searchQuery = "";
                        page = 1;
                        isSearched = true;
                        getMyBids();
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        getView().getCloseBtn().setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });


            myBidsAdapter = new MyBidsAdapter(getActivity(), null, MyBidsFragmentPresenterImpl.this);
            getView().bidListView().setAdapter(myBidsAdapter);
        }
        getMyBids();
    }

    private void getMyBids() {
        try {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                if (TextUtils.isEmpty(searchQuery))
                    getActivity().showProgressDialog();
                Map<String, String> params = new HashMap<>();
                params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                params.put(Constants.SEARCH_PARAM, searchQuery);
                params.put(Constants.PAGE_PARAM, String.valueOf(page));
                getInterActor().serviceProviderBids(params, ApiName.GET_MY_BIDS);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        if (getActivity() != null && isViewAttached()) {
            try {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_MY_BIDS:
                        if (response.isSuccessful()) {
                            myBidsResponse = (MyBidsResponse) response.body();
                            switch ((myBidsResponse.getStatus())) {
                                case Constants.STATUS_200:
                                    onSuccess(myBidsResponse);
                                    break;
                                case Constants.STATUS_201:
                                    getActivity().showSnakBar(getView().getRootView(), myBidsResponse.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((OwnerDashboardActivity) getActivity()).logout(myBidsResponse.getMessage());
                                    break;
                            }

                        }
                        break;
                    case DELETE_BID:
                        getActivity().hideProgressDialog();
                        if (response.isSuccessful()) {
                            GenericBean genericBean = (GenericBean) response.body();
                            switch (genericBean.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(genericBean);
                                    break;
                                case Constants.STATUS_201:
                                    getActivity().showSnakBar(getView().getRootView(), genericBean.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((OwnerDashboardActivity) getActivity()).logout(genericBean.getMessage());
                                    break;

                            }
                        }
                        break;
                }
            } catch (Exception e) {

            }
        }

    }

    private void onSuccess(GenericBean genericBean) {
        if (getActivity() != null && isViewAttached()) {
            listResponse.remove(adapterPosition);
            myBidsAdapter.notifyDataSetChanged();
        }
    }

    private void onSuccess(MyBidsResponse myBidsResponse) {
        listResponse = new ArrayList<>();
        if (myBidsResponse.getData().size() > 0 && myBidsResponse.getData() != null) {
            getView().getEmptyLayout().setVisibility(View.GONE);
            if (isSearched || searchQuery.length() != 0) {
                listResponse = new ArrayList<>();
                listResponse.addAll(myBidsResponse.getData());
                isSearched = false;
            } else if (myBidsResponse.getTotalRecord() > listResponse.size()) {
                listResponse.addAll(myBidsResponse.getData());
            }

            myBidsAdapter.updateList(listResponse);


        } else {
            if (listResponse.isEmpty()) {
                myBidsAdapter.clearAll();
                getView().getEmptyLayout().setVisibility(View.VISIBLE);
            } else {
                if (isSearched || searchQuery.length() != 0) {
                    myBidsAdapter.clearAll();
                    getView().getEmptyLayout().setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    public void deleteProject(int bid_id, int position) {

        if (getActivity() != null && isViewAttached()) {

            adapterPosition = position;
            try {
                getActivity().showProgressDialog();
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    Map<String, String> params = new HashMap<>();
                    params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    params.put(Constants.BID_ID, String.valueOf(bid_id));
                    getInterActor().deleteMyBid(params, ApiName.DELETE_BID);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.btn_clear:
                    searchQuery = "";
                    page = 1;
                    getView().getSearchEditText().setText("");
                    break;
            }
        }
    }
}
