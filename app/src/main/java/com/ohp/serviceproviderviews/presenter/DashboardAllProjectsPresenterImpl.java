package com.ohp.serviceproviderviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.enums.ApiName;
import com.ohp.serviceproviderviews.activities.SPDashboardActivity;
import com.ohp.serviceproviderviews.adapter.AllProjectsOngoingAdapter;
import com.ohp.serviceproviderviews.adapter.DashboardCompletedProjectAdapter;
import com.ohp.serviceproviderviews.adapter.DashboardMyProjectsServiceProviderAdapter;
import com.ohp.serviceproviderviews.adapter.DashboardWonProjectAdapter;
import com.ohp.serviceproviderviews.beans.GetAllProjectsServiceProviderResponse;
import com.ohp.serviceproviderviews.view.DashboardAllProjectView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * Created by vishal.sharma on 7/28/2017.
 */

public class DashboardAllProjectsPresenterImpl extends FragmentPresenter<DashboardAllProjectView, APIHandler> implements APIResponseInterface.OnCallableResponse, View.OnClickListener {

    private int page = 1;   //for pagination
    private String propertyId = "";
    private AllProjectsOngoingAdapter allProjectsAdapter;
    private DashboardWonProjectAdapter all_projects_won_adapter;
    private DashboardCompletedProjectAdapter all_projects_completed_adapter;
    private DashboardMyProjectsServiceProviderAdapter all_myprojects_adapter;
    private List<GetAllProjectsServiceProviderResponse.DataBean.MyProjectsBean> myProjectsBeen = new ArrayList<>();
    private List<GetAllProjectsServiceProviderResponse.DataBean.WonProjectsBean> wonProjectBeen = new ArrayList<>();
    private List<GetAllProjectsServiceProviderResponse.DataBean.CompletedProjectsBean> completedProjectBeen = new ArrayList<>();


    public DashboardAllProjectsPresenterImpl(DashboardAllProjectView dashboardAllProjectView, Context context) {
        super(dashboardAllProjectView, context);
    }

    private BroadcastReceiver projectsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("DashboardActivityPresenterImpl.onReceive");
            if (getActivity() != null && isViewAttached()) {
                page = 1;
                getAllProjects();
            }

        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(projectsReceiver,
                    new IntentFilter(Constants.GET_PROJECTS_SP));
            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getRVWonProjects().setLayoutManager(lm);

            LinearLayoutManager lm1 = new LinearLayoutManager(getActivity());
            lm1.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getRVMyProjects().setLayoutManager(lm1);

            LinearLayoutManager lm2 = new LinearLayoutManager(getContext());
            lm2.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getRVCompletedProjects().setLayoutManager(lm2);
            getAllProjects();
        }
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    private void getAllProjects() {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (Utils.getInstance().isInternetAvailable(getActivity())) {
                    getActivity().showProgressDialog();
                    Map<String, String> params = new HashMap<>();
                    params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    params.put(Constants.PAGE_PARAM, String.valueOf(page));
                    // params.put(Constants.PROPERTY_ID, propertyId);
                    getInterActor().getAllProjectsServiceProvider(params, ApiName.GET_PROJECTS);
                } else {
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        if (getActivity() != null && isViewAttached())
            getActivity().unregisterReceiver(projectsReceiver);
        super.onDestroy();

    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    getActivity().hideProgressDialog();
                }
                switch (api) {
                    case GET_PROJECTS:
                        if (response.isSuccessful()) {
                            GetAllProjectsServiceProviderResponse getAllProjectsResponse = (GetAllProjectsServiceProviderResponse) response.body();
                            switch (getAllProjectsResponse.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(getAllProjectsResponse);
                                    break;
                                case Constants.STATUS_201:
                                    getActivity().showSnakBar(getView().getRootView(), getAllProjectsResponse.getMessage());
                                    break;
                                case Constants.STATUS_202:
                                    ((SPDashboardActivity) getActivity()).logout(getAllProjectsResponse.getMessage());
                                    break;
                            }
                        }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onSuccess(GetAllProjectsServiceProviderResponse getAllProjectsResponse) {
        if (getActivity() != null && isViewAttached()) {
            if (getAllProjectsResponse.getData() != null) {


                // adapter for my projects service provider

                if (!myProjectsBeen.isEmpty())
                    myProjectsBeen.clear();
                all_myprojects_adapter = new DashboardMyProjectsServiceProviderAdapter(getActivity(), myProjectsBeen);
                getView().getRVMyProjects().setAdapter(all_myprojects_adapter);
                if (getAllProjectsResponse.getData().getMyProjects().size() > 0) {
                    myProjectsBeen.addAll(getAllProjectsResponse.getData().getMyProjects());
                    all_myprojects_adapter.update(myProjectsBeen);
                    getView().getTVNoMyProjects().setVisibility(View.GONE);
                    getView().getTVMyProjectsViewAll().setVisibility(View.VISIBLE);
                } else {
                    all_myprojects_adapter.update(myProjectsBeen);
                    getView().getTVNoMyProjects().setVisibility(View.VISIBLE);
                    getView().getTVMyProjectsViewAll().setVisibility(View.GONE);
                    System.out.println("upcomingProjectlist<oPart");
                }


                // adapter for all won project....
                if (!wonProjectBeen.isEmpty())
                    wonProjectBeen.clear();
                all_projects_won_adapter = new DashboardWonProjectAdapter(getActivity(), wonProjectBeen);
                getView().getRVWonProjects().setAdapter(all_projects_won_adapter);
                if (getAllProjectsResponse.getData().getWonProjects().size() > 0) {
                    wonProjectBeen.addAll(getAllProjectsResponse.getData().getWonProjects());
                    all_projects_won_adapter.update(wonProjectBeen);
                    getView().getTVNoWonProjects().setVisibility(View.GONE);
                    getView().getTVWonViewAll().setVisibility(View.VISIBLE);
                } else {
                    all_projects_won_adapter.update(wonProjectBeen);
                    getView().getTVNoWonProjects().setVisibility(View.VISIBLE);
                    getView().getTVWonViewAll().setVisibility(View.GONE);
                    System.out.println("upcomingProjectlist<oPart");
                }


                //adapter for all completed...

                if (!completedProjectBeen.isEmpty())
                    completedProjectBeen.clear();
                all_projects_completed_adapter = new DashboardCompletedProjectAdapter(getActivity(), completedProjectBeen);
                getView().getRVCompletedProjects().setAdapter(all_projects_completed_adapter);
                if (getAllProjectsResponse.getData().getCompletedProjects().size() > 0) {

                    completedProjectBeen.addAll(getAllProjectsResponse.getData().getCompletedProjects());
                    all_projects_completed_adapter.update(completedProjectBeen);
                    System.out.println("getCompleted_projectProjectlist<oPartADAPTER");
                    getView().getTVNoCompletedProjects().setVisibility(View.GONE);
                    getView().getCompletedProjects().setVisibility(View.VISIBLE);
                } else {
                    all_projects_completed_adapter.update(completedProjectBeen);
                    getView().getTVNoCompletedProjects().setVisibility(View.VISIBLE);
                    getView().getCompletedProjects().setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Intent intent = new Intent(getActivity(), HolderActivity.class);
            switch (v.getId()) {
                case R.id.tv_won_view_all:
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_ALL_WON_PROJECTS_SERVICEPROVIDER);
                    getActivity().startActivity(intent);
                    break;
                case R.id.tv_my_project_view_all:
                    if (myProjectsBeen != null && myProjectsBeen.size() > 0) {
                        intent.putExtra(Constants.DESTINATION, Constants.CLICK_ALL_MY_PROJECTS_SERVICEPROVIDER);
                        getActivity().startActivity(intent);
                    }
                    break;
                case R.id.tv_completed_view_all:
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_ALL_COMPLETED_PROJECTS_SERVICEPROVIDER);
                    getActivity().startActivity(intent);
                    break;
                case R.id.fab_add_projects:
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_ADD_EDIT_PROJECT_SERVICE_PROVIDER);
                    getActivity().startActivity(intent);
                    break;
            }
        }
    }
}
