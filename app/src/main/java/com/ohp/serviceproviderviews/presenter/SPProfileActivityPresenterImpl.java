package com.ohp.serviceproviderviews.presenter;

import android.content.Context;
import android.os.Bundle;

import com.library.mvp.ActivityPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.enums.CurrentScreen;
import com.ohp.serviceproviderviews.activities.SPProfileActivity;
import com.ohp.serviceproviderviews.view.SPProfileActivityView;

/**
 * Created by Anuj Sharma on 5/25/2017.
 */

public class SPProfileActivityPresenterImpl extends ActivityPresenter<SPProfileActivityView, APIHandler> {

    public SPProfileActivityPresenterImpl(SPProfileActivityView view, Context context) {
        super(view, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Receive Intent and proceed
        /*if(getActivity().getIntent()!=null){

        }else{

        }*/
        Bundle bundle = new Bundle();
        if (getActivity().getIntent() != null) {
            bundle.putBoolean("is_edit", getActivity().getIntent().getBooleanExtra("is_edit", false));
        }
        if (bundle == null)
            ((SPProfileActivity) getActivity()).changeScreen(R.id.profile_container, CurrentScreen.SP_PROFILE_SCREEN, false, false, null);
        else
            ((SPProfileActivity) getActivity()).changeScreen(R.id.profile_container, CurrentScreen.SP_PROFILE_SCREEN, false, false, bundle);
    }
}
