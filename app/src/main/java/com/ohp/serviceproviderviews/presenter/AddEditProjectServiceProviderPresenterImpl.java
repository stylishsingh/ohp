package com.ohp.serviceproviderviews.presenter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.FilePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.FilePickerCallback;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenFile;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.JobTypeExpertiseSpinnerAdapter;
import com.ohp.commonviews.adapter.ThumbImageAdapter;
import com.ohp.commonviews.beans.JobTypeAndExpertise;
import com.ohp.commonviews.beans.ProjectDetailResponse;
import com.ohp.commonviews.beans.ThumbnailBean;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.service.ImagesResultReceiver;
import com.ohp.service.ImagesService;
import com.ohp.serviceproviderviews.adapter.ProjectTaskAdapter;
import com.ohp.serviceproviderviews.beans.AddEditProjectSpResponse;
import com.ohp.serviceproviderviews.fragment.AddEditProjectServiceProvider;
import com.ohp.serviceproviderviews.view.AddEditProjectServiceProviderView;
import com.ohp.utils.Constants;
import com.ohp.utils.FileUtils;
import com.ohp.utils.PermissionsAndroid;
import com.ohp.utils.Utils;
import com.ohp.utils.Validations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.ohp.enums.ApiName.ADD_EDIT_PROJECT_SP;
import static com.ohp.enums.ApiName.GET_RESOURCES_API;
import static com.ohp.utils.Constants.CLICK_ADD_EDIT_PROJECT_SERVICE_PROVIDER;
import static com.ohp.utils.Constants.MY_PROJECTS_SERVICE_PROVIDER_EDIT;
import static com.ohp.utils.Constants.MY_PROJECTS_SERVICE_PROVIDER_View;
import static com.ohp.utils.Constants.STATUS_200;
import static com.ohp.utils.Constants.STATUS_201;
import static com.ohp.utils.Constants.STATUS_202;
import static com.ohp.utils.Constants.STATUS_203;

/**
 * @author Vishal Sharma.
 */

public class AddEditProjectServiceProviderPresenterImpl extends FragmentPresenter<AddEditProjectServiceProviderView, APIHandler>
        implements APIResponseInterface.OnCallableResponse, ImagePickerCallback, ImagesResultReceiver.Receiver, View.OnClickListener, FilePickerCallback {

    private int selectedJobType = 0;
    private ThumbImageAdapter detailImageAdapter, editImageAdapter;
    private List<ThumbnailBean> detailImageList = new ArrayList<>();
    private List<ThumbnailBean> uploadImageList = new ArrayList<>();
    private List<ThumbnailBean> tempUploadImageList = new ArrayList<>();
    private ProjectTaskAdapter taskCompletedAdapter, taskOnGoingAdapter;
    private List<ProjectDetailResponse.DataBean.ProjectTasksBean.TaskBean> listCompletedTask = new ArrayList<>(), listOnGoingTask = new ArrayList<>();
    private List<String> deletedImageIds = new ArrayList<>();
    private boolean isImageSelected = false;
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private boolean isCameraSelected = false, isGallerySelected = false, forDocumentSection = false;
    private String pickerPath;
    private int PLACE_PICKER_REQUEST = 1;
    private Place place;
    private List<JobTypeAndExpertise.DataBean.InfoBean> listExpertise = new ArrayList<>(), listJobType = new ArrayList<>();
    private JobTypeExpertiseSpinnerAdapter jobTypeExpertiseAdapter;
    private ArrayAdapter expertiseAdapter;
    private String selectedExpertiseName = "";
    private String currentScreen = "", project_id = "";
    private boolean[] arraySelectionExpertise;
    private String mSelectedExpertise = "";
    private String[] detailExpertise;
    private String expertiseName = "";
    public MultiSelectSpinner.MultiSpinnerListener expertiseSpinnerListener = new MultiSelectSpinner.MultiSpinnerListener() {

        public void onItemsSelected(boolean[] selected) {
            mSelectedExpertise = "";
            expertiseName = "";
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    if (mSelectedExpertise.trim().isEmpty()) {
                        mSelectedExpertise = String.valueOf(listExpertise.get(i).getId());
                        expertiseName = listExpertise.get(i).getValue();
                    } else {
                        mSelectedExpertise = mSelectedExpertise + "," + String.valueOf(listExpertise.get(i).getId());
                        expertiseName = expertiseName + "," + listExpertise.get(i).getValue();
                    }
                }
            }

            if (getActivity() != null && isViewAttached()) {
                if (mSelectedExpertise.isEmpty()) {
                    mSelectedExpertise = String.valueOf(listExpertise.get(0).getId());
                    expertiseName = listExpertise.get(0).getValue();
                    arraySelectionExpertise[0] = true;
                    getView().getExpertiseSpinnerView().setAllCheckedText(expertiseName);
                    getView().getExpertiseSpinnerView().selectItem(0, arraySelectionExpertise[0]);
                } else {
                    if (getView().getExpertiseSpinnerView().isSelectAll()) {
                        getView().getExpertiseSpinnerView().setAllCheckedText(expertiseName);
                    }
                }
            }
        }
    };
    private FilePicker filePicker;
    private String mProjectName = "", mProjectAppliance = "", mProjectPropertyName = "", mProjectJobType = "", mProjectDescription = "", mProjectPropertyAddress;

    public AddEditProjectServiceProviderPresenterImpl(AddEditProjectServiceProviderView addEditProjectServiceProviderView, Context context) {
        super(addEditProjectServiceProviderView, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");

            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    ((HolderActivity) getActivity()).oneStepBack();
                }
            });
        }

        //Initialize RecyclerView for Detail View
        if (getActivity() != null && isViewAttached()) {
            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getProjectDetailRecycler().setLayoutManager(lm);
            detailImageAdapter = new ThumbImageAdapter(getActivity(), null, this, true, false);
            getView().getProjectDetailRecycler().setAdapter(detailImageAdapter);

            if (uploadImageList == null) uploadImageList = new ArrayList<>();
            LinearLayoutManager lm1 = new LinearLayoutManager(getContext());
            lm1.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getEditProjectDetailRecycler().setLayoutManager(lm1);


            LinearLayoutManager ongoingLayoutManager = new LinearLayoutManager(getContext());
            ongoingLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getRVOnGoingTasks().setLayoutManager(ongoingLayoutManager);
            taskOnGoingAdapter = new ProjectTaskAdapter(getActivity(), listOnGoingTask);
            getView().getRVOnGoingTasks().setAdapter(taskOnGoingAdapter);


            LinearLayoutManager completedLayoutManager = new LinearLayoutManager(getContext());
            completedLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            getView().getRVCompletedTasks().setLayoutManager(completedLayoutManager);
            taskCompletedAdapter = new ProjectTaskAdapter(getActivity(), listCompletedTask);
            getView().getRVCompletedTasks().setAdapter(taskCompletedAdapter);

            ThumbnailBean obj = new ThumbnailBean();
            obj.setAddView(true);
            obj.setFile(false);
            obj.setImagePath("");
            uploadImageList.add(obj);
            editImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, true);
            getView().getEditProjectDetailRecycler().setAdapter(editImageAdapter);


            if (detailImageList == null) detailImageList = new ArrayList<>();
            ThumbnailBean obj1 = new ThumbnailBean();
            obj1.setAddView(true);
            obj1.setFile(false);
            obj1.setImagePath("");
            detailImageList.add(obj1);
            detailImageAdapter = new ThumbImageAdapter(getActivity(), detailImageList, this, false, false);
            getView().getProjectDetailRecycler().setAdapter(detailImageAdapter);

        }
        getView().getJobType().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                return false;
            }
        });

        getView().getExpertiseSpinnerView().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                return false;
            }
        });

        getResource();
    }

    public void saveBundleInfo(Bundle bundle) {
        try {
            if (getActivity() != null && isViewAttached() && bundle != null) {
                currentScreen = bundle.getString(Constants.DESTINATION, "");
                if (bundle.getString(Constants.PROJECT_ID) != null) {
                    project_id = bundle.getString(Constants.PROJECT_ID);
                    //   getPresenter().getOnGoingObject(Project_ID);
                    // getProjectDetail(project_id);
                }

                switch (currentScreen) {
                    case MY_PROJECTS_SERVICE_PROVIDER_View:

                        getView().getToolbarTitle().setText(R.string.Project_Detail);
                        visibilityGoneUiForDetailProject();
                        break;
                    case MY_PROJECTS_SERVICE_PROVIDER_EDIT:
                        getView().getToolbarTitle().setText(R.string.edit_project);
                        visibilityGoneUiForSaveProject();
                        break;
                    case CLICK_ADD_EDIT_PROJECT_SERVICE_PROVIDER:
                        getView().getToolbarTitle().setText(R.string.add_project);
                        visibilityGoneUiForSaveProject();

                        break;

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void visibilityGoneUiForSaveProject() {
        getView().getDetailProject().setVisibility(View.GONE);
        getView().getEditProject().setVisibility(View.VISIBLE);
    }

    public void visibilityGoneUiForDetailProject() {
        getView().getDetailProject().setVisibility(View.VISIBLE);
        getView().getEditProject().setVisibility(View.GONE);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    private void getResource() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.TYPE, "jobtype_expertise");
                getInterActor().getResourcesJobAndExpertise(param, GET_RESOURCES_API);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            getActivity().hideProgressDialog();
            switch (api) {
                case GET_RESOURCES_API:
                    if (response.isSuccessful()) {
                        getActivity().hideProgressDialog();
                        JobTypeAndExpertise resourcesObj = (JobTypeAndExpertise) response.body();
                        switch (resourcesObj.getStatus()) {
                            case STATUS_200:
                                onSuccess(resourcesObj);
                                break;
                            case STATUS_201:
                                if (jobTypeExpertiseAdapter != null) {
                                    jobTypeExpertiseAdapter.updateList(getActivity(), listJobType);
                                }
                                break;
                            case STATUS_202:
                                ((HolderActivity) getActivity()).logout(resourcesObj.getMessage());
                                break;
                        }
                    }
                    break;
                case ADD_EDIT_PROJECT_SP:
                    if (response.isSuccessful()) {
                        AddEditProjectSpResponse addEditProjectSpResponse = (AddEditProjectSpResponse) response.body();
                        switch (addEditProjectSpResponse.getStatus()) {
                            case STATUS_200:
                                onSuccess(addEditProjectSpResponse);
                                break;
                            case STATUS_201:
                                break;
                            case STATUS_202:
                                ((HolderActivity) getActivity()).logout(addEditProjectSpResponse.getMessage());
                                break;
                            case STATUS_203:
                                getActivity().showSnackBar(getView().getRootView(), addEditProjectSpResponse.getMessage());
                                break;

                        }
                    }
                    break;
                case GET_PROJECT_DETAIL:
                    if (response.isSuccessful()) {
                        ProjectDetailResponse projectDetailResponse = (ProjectDetailResponse) response.body();
                        switch (projectDetailResponse.getStatus()) {
                            case STATUS_200:
                                onSuccess(projectDetailResponse);
                                break;
                            case STATUS_201:
                                break;
                            case STATUS_202:
                                ((HolderActivity) getActivity()).logout(projectDetailResponse.getMessage());
                                break;
                        }
                        getResource();

                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void onSuccess(ProjectDetailResponse dataObj) {

        try {
            if (getActivity() != null && isViewAttached()) {
                if (listJobType != null && listJobType.size() > 0) {
                    for (int i = 0; i < listJobType.size(); i++) {
                        if (listJobType.get(i).getId() == selectedJobType) {
                            getView().getJobType().setSelection(i);
                        }
                    }
                }
                getView().getPropertyName().setText(dataObj.getData().getProperty().getPropertyName());
                getView().getProperty().setText(dataObj.getData().getProperty().getPropertyName());
                getView().getApplianceView().setText(dataObj.getData().getProjectAppliance().getApplianceName());
                getView().getApplianceName().setText(dataObj.getData().getProjectAppliance().getApplianceName());
                getView().getPropertyAddress().setText(dataObj.getData().getProperty().getPropertyAddress());
                getView().setPropertyAddress().setText(dataObj.getData().getProperty().getPropertyAddress());
                getView().getProjectNameEdit().setText(dataObj.getData().getProjectName());
                getView().getProjectName().setText(dataObj.getData().getProjectName());
                getView().getProjectDescriptionEdit().setText(dataObj.getData().getProjectDescription());
                getView().getProjectDescription().setText(dataObj.getData().getProjectDescription());
                getView().getProjectCreatedOn().setText(Utils.getInstance().getYMDProjectFormattedDateTime(dataObj.getData().getCreated().replace("+00:00", "")));
                getView().getProjectJopType().setText(dataObj.getData().getProjectJobtype().getJobtypeTitle());
                selectedJobType = dataObj.getData().getProjectJobtype().getJobtypeId();

                detailExpertise = new String[dataObj.getData().getProjectExpertise().size()];
                for (int i = 0; i < dataObj.getData().getProjectExpertise().size(); i++) {
                    detailExpertise[i] = String.valueOf(dataObj.getData().getProjectExpertise().get(i).getExpertiseId());
                }

                if (dataObj.getData().getProjectImages() != null && dataObj.getData().getProjectImages().size() > 0) {
                    if (detailImageList == null) detailImageList = new ArrayList<>();
                    if (detailImageList.size() > 0) detailImageList.clear();
                    if (!uploadImageList.isEmpty()) {
                        uploadImageList.clear();
                    }

                    for (int i = 0; i < dataObj.getData().getProjectImages().size(); i++) {
                        ThumbnailBean obj = new ThumbnailBean();
                        obj.setAddView(false);
                        obj.setImageID(String.valueOf(dataObj.getData().getProjectImages().get(i).getProjectImageId()));
                        obj.setFile(false);
                        obj.setCheck(ScreenNavigation.OLD_ITEM);
                        obj.setImagePath(dataObj.getData().getProjectImages().get(i).getProjectImageAvatar());
                        detailImageList.add(obj);
                        uploadImageList.add(obj);
                    }

                    // Add View after All Info in uploadImageList for adding data in edit view
                    ThumbnailBean obj = new ThumbnailBean();
                    obj.setAddView(true);
                    obj.setFile(false);
                    obj.setImagePath("");
                    obj.setCheck(ScreenNavigation.DUMMY_ITEM);
                    uploadImageList.add(obj);

                    System.out.println("Images List" + detailImageList.size());
                    System.out.println("Upload Images List" + uploadImageList.size());
                    detailImageAdapter = new ThumbImageAdapter(getActivity(), detailImageList, this, false, false);
                    getView().getProjectDetailRecycler().setAdapter(detailImageAdapter);
                    //detailImageAdapter.updateList(detailImageList);
                    editImageAdapter = new ThumbImageAdapter(getActivity(), uploadImageList, this, false, true);
                    getView().getEditProjectDetailRecycler().setAdapter(editImageAdapter);
                } else {
                    getView().NoImages().setVisibility(View.VISIBLE);
                }


                if (dataObj.getData().getProjectTasks() != null) {
                    if (!dataObj.getData().getProjectTasks().getOngoingTasks().isEmpty()) {
                        listOnGoingTask = dataObj.getData().getProjectTasks().getOngoingTasks();
                        taskOnGoingAdapter.update(listOnGoingTask);
                    } else {
                        taskOnGoingAdapter.update(listOnGoingTask);
                        getView().getTVNoOnGoingTasks().setVisibility(View.VISIBLE);
                    }

                    if (!dataObj.getData().getProjectTasks().getCompletedTasks().isEmpty()) {
                        listCompletedTask = dataObj.getData().getProjectTasks().getCompletedTasks();
                        taskCompletedAdapter.update(listCompletedTask);
                    } else {
                        taskCompletedAdapter.update(listCompletedTask);
                        getView().getTVNoCompletedTasks().setVisibility(View.VISIBLE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(AddEditProjectSpResponse addEditProjectSpResponse) {
        try {
            Intent listIntent = new Intent(Constants.GET_PROJECTS_SP);
            getActivity().sendBroadcast(listIntent);

            Intent listIntentProjects = new Intent(Constants.GET_PROJECTS_SP_MYPROJECTS);
            getActivity().sendBroadcast(listIntentProjects);

            uploadApplianceImages(String.valueOf(addEditProjectSpResponse.getData().getProjectId()));
            ((HolderActivity) getActivity()).oneStepBack();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void uploadApplianceImages(String projectId) {
        try {
            System.out.println("upload Images" + uploadImageList.size());
            uploadImageList.remove(uploadImageList.size() - 1);
            getActivity().startService(createCallingIntent(projectId));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Intent createCallingIntent(String projectId) {
        Intent i = new Intent(getActivity(), ImagesService.class);
        ImagesResultReceiver receiver = new ImagesResultReceiver(new Handler());
        receiver.setReceiver(this);
        i.putExtra(Constants.RECEIVER, receiver);
        i.putExtra(Constants.SCREEN, Constants.ADD_EDIT_PROJECT_SP);
        i.putParcelableArrayListExtra(Constants.IMAGE_LIST, (ArrayList<? extends Parcelable>) tempUploadImageList);
        i.putExtra(Constants.PROJECT_ID, projectId);
        return i;
    }

    private void onSuccess(JobTypeAndExpertise resourcesObj) {
        try {
            if (getActivity() != null && isViewAttached()) {

                listJobType = new ArrayList<>();
                if (!listJobType.isEmpty())
                    listJobType.clear();

                listJobType.addAll(resourcesObj.getData().get(0).get(0).getInfo());

                jobTypeExpertiseAdapter = new JobTypeExpertiseSpinnerAdapter(getActivity(), R.layout.spinner_item, R.id.tv_title, listJobType);
                jobTypeExpertiseAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                getView().getJobType().setAdapter(jobTypeExpertiseAdapter);

                if (listJobType != null && listJobType.size() > 0) {
                    for (int i = 0; i < listJobType.size(); i++) {
                        if (listJobType.get(i).getId() == selectedJobType) {
                            getView().getJobType().setSelection(i);
                        }
                    }
                }

                listExpertise = new ArrayList<>();
                if (!listExpertise.isEmpty())
                    listExpertise.clear();

                List<String> listExpert = new ArrayList<>();

                listExpertise.addAll(resourcesObj.getData().get(1).get(0).getInfo());
                for (int i = 0; i < listExpertise.size(); i++) {
                    listExpert.add(listExpertise.get(i).getValue());
                }

                expertiseAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_multiple_choice, listExpert);
                getView().getExpertiseSpinnerView().setListAdapter(expertiseAdapter).setAllCheckedText(selectedExpertiseName);

                if (!listExpertise.isEmpty()) {
                    arraySelectionExpertise = new boolean[listExpertise.size()];
                    for (int j = 0; j < listExpertise.size(); j++) {
                        if (detailExpertise != null) {

                            for (int i = 0; i < detailExpertise.length; i++) {
                                if (detailExpertise[i].equals(String.valueOf(listExpertise.get(j).getId()))) {
                                    arraySelectionExpertise[j] = true;
                                    if (expertiseName.trim().isEmpty())
                                        expertiseName = String.valueOf(listExpertise.get(j).getValue());
                                    else
                                        expertiseName = expertiseName + "," + String.valueOf(listExpertise.get(j).getValue());

                                    if (mSelectedExpertise.trim().isEmpty())
                                        mSelectedExpertise = String.valueOf(listExpertise.get(j).getId());
                                    else
                                        mSelectedExpertise = mSelectedExpertise + "," + String.valueOf(listExpertise.get(j).getId());

                                }
                            }
                            getView().getProjectExpertise().setText(expertiseName);
                        }

                    }

                   /* switch (currentScreen) {
                        case CLICK_ADD_PROJECT:

                            break;
                    }*/
                    switch (currentScreen) {
                        case CLICK_ADD_EDIT_PROJECT_SERVICE_PROVIDER:
                            mSelectedExpertise = String.valueOf(listExpertise.get(0).getId());
                            expertiseName = listExpertise.get(0).getValue();
                            arraySelectionExpertise[0] = true;
                            break;
                    }


                    getView().getExpertiseSpinnerView().setAllCheckedText(expertiseName);
                    for (int k = 0; k < arraySelectionExpertise.length; k++) {
                        System.out.println(k + "K----boolean" + arraySelectionExpertise[k]);
                        getView().getExpertiseSpinnerView().selectItem(k, arraySelectionExpertise[k]);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        ChosenImage image = list.get(0);
        isImageSelected = true;
        String extension = "";
        int i = list.get(0).getDisplayName().lastIndexOf('.');
        if (i > 0) {
            extension = list.get(0).getDisplayName().substring(i + 1);
        }
        //Add Images info to list
        if (!extension.equals("png") && !extension.equals("jpg") && !extension.equals("jpeg")) {
            getActivity().showSnakBar(getView().getRootView(), "you can choose only images.");
            return;
        }
        if (uploadImageList.size() > 0) {
            uploadImageList.remove(uploadImageList.size() - 1);
            editImageAdapter.notifyItemRemoved(uploadImageList.size());
        }
        ThumbnailBean obj = new ThumbnailBean();
        obj.setExtension(extension);
        obj.setImagePath(FileUtils.getPath(getActivity(), Uri.parse(image.getQueryUri())));
        obj.setImageID("");
        obj.setName(image.getDisplayName());
        obj.setFile(true);
        obj.setCheck(ScreenNavigation.ADD_ITEM);
        obj.setAddView(false);
        uploadImageList.add(obj);

        if (editImageAdapter != null) {
            ThumbnailBean obj1 = new ThumbnailBean();
            obj1.setAddView(true);
            obj1.setFile(false);
            obj1.setImagePath("");
            obj1.setFile(false);
            uploadImageList.add(obj1);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    editImageAdapter.notifyDataSetChanged();
//                    editImageAdapter.updateList(uploadImageList);
                }
            });
            getView().getEditProjectDetailRecycler().smoothScrollToPosition(uploadImageList.size() - 1);
        }
    }

    @Override
    public void onError(String message) {
        Utils.getInstance().showToast(message);
    }

    private void checkExternalStoragePermission() {
        boolean isExternalStorage = PermissionsAndroid.getInstance().checkCameraPermission(getActivity());
        if (!isExternalStorage) {
            PermissionsAndroid.getInstance().requestForCameraPermission(((AddEditProjectServiceProvider) getView()));
        } else if (isCameraSelected) {
            takePicture();
        } else if (isGallerySelected) {
            pickImageSingle();
        } else if (forDocumentSection) {
            pickFilesSingle();
        }
    }

    /*
   Image handling for this class
    */
    private void pickFilesSingle() {
        filePicker = new FilePicker(getActivity());
        filePicker.setFilePickerCallback(this);
        filePicker.setFolderName(getActivity().getString(R.string.app_name));
        filePicker.pickFile();
    }

    public void onAddOptionClick() {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Choose from file", "Cancel"};
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Choose Option");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            //choose camera
                            try {
                                isCameraSelected = true;
                                isGallerySelected = false;
                                forDocumentSection = false;
                                checkExternalStoragePermission();
                            } catch (ActivityNotFoundException e) {
                                e.printStackTrace();
                                String errorMessage = "Your device doesn't support capturing images";
                                Utils.getInstance().showToast(errorMessage);
                            }
                            break;
                        case 1:
                            //choose gallery
                            try {
                                isCameraSelected = false;
                                isGallerySelected = true;
                                forDocumentSection = false;
                                checkExternalStoragePermission();
                            } catch (Exception e) {
                                String errorMessage = "There are no images!";
                                Utils.getInstance().showToast(errorMessage);
                            }
                            break;

                        case 2:
                            //choose gallery
                            try {
                                isCameraSelected = false;
                                isGallerySelected = false;
                                forDocumentSection = true;
                                checkExternalStoragePermission();
                            } catch (Exception e) {
                                String errorMessage = "There are no images!";
                                Utils.getInstance().showToast(errorMessage);
                            }
                            break;
                        case 3:
                            dialog.dismiss();
                            break;
                    }
                }
            });
            builder.show();
        }
    }

    public void deleteImage(final int position) {
        if (getActivity() != null && isViewAttached()) {

            Utils.getInstance().confirmDialog(getActivity(), "Are you sure, you want to delete this image?", "delete", new Utils.ConfirmDialogCallbackInterface() {
                @Override
                public void onYesClick(String tag) {
                    if (deletedImageIds == null) deletedImageIds = new ArrayList<>();
                    deletedImageIds.add(uploadImageList.get(position).getImageID());
                    uploadImageList.remove(position);
                    getView().getEditProjectDetailRecycler().setAdapter(editImageAdapter);
                }

                @Override
                public void onNoClick(String tag) {

                }
            });
        }
    }

    private void takePicture() {
        cameraPicker = new CameraImagePicker(getActivity());
//        cameraPicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        cameraPicker.setImagePickerCallback(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        pickerPath = cameraPicker.pickImage();
    }

    private void pickImageSingle() {
        imagePicker = new ImagePicker(getActivity());
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();
    }

    public void onRequestPermissionResult(int requestCode, int[] grantResults) {
        switch (requestCode) {
            case 102: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Show option to update profile
                    if (isCameraSelected) {
                        takePicture();
                    } else if (isGallerySelected) {
                        pickImageSingle();
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Utils.getInstance().showToast("Camera/Storage permission Denied");
                }
                return;
            }

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (getActivity() != null && isViewAttached()) {
            if (resultCode == RESULT_OK) {
                getActivity().hideProgressDialog();
                if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                    if (imagePicker == null) {
                        imagePicker = new ImagePicker(getActivity());
                    }
                    imagePicker.submit(data);
                } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                    if (cameraPicker == null) {
                        cameraPicker = new CameraImagePicker(getActivity());
                        cameraPicker.setImagePickerCallback(this);
                        cameraPicker.reinitialize(pickerPath);
                    }
                    cameraPicker.submit(data);
                } else if (requestCode == Picker.PICK_FILE) {
                    filePicker.submit(data);
                } else if (requestCode == PLACE_PICKER_REQUEST) {
                    place = PlacePicker.getPlace(data, getActivity());
                    if (place != null) {
                        getView().setPropertyAddress().setText(place.getAddress());
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                Utils.getInstance().showToast("Request Cancelled");
            }
        }
    }

    public void AddEditProject() {
        if (getActivity() != null && isViewAttached()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {

                mProjectName = getView().getProjectName().getText().toString().trim();
                mProjectDescription = getView().getProjectDescription().getText().toString();
                mProjectAppliance = getView().getProjectName().getText().toString().trim();
                mProjectPropertyName = getView().getProperty().getText().toString().trim();
                mProjectAppliance = getView().getApplianceName().getText().toString().trim();
                mProjectJobType = listJobType.size() == 0 ? "" : String.valueOf(listJobType.get(getView().getJobType().getSelectedItemPosition()).getId());
                mProjectPropertyAddress = getView().setPropertyAddress().getText().toString().trim();

                String response = Validations.getInstance().validateAddProjectFieldsForSp(mProjectName, mSelectedExpertise, mProjectJobType);
                if (!response.trim().isEmpty()) {
                    getActivity().showSnakBar(getView().getRootView(), response);
                } else {
                    getActivity().showProgressDialog();
                    Map<String, String> params = new HashMap<>();
                    params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    getActivity().showProgressDialog();

                    if (currentScreen.equals(MY_PROJECTS_SERVICE_PROVIDER_EDIT) || currentScreen.equals(MY_PROJECTS_SERVICE_PROVIDER_View))

                        params.put(Constants.PROJECT_ID, project_id);


                    String imageIds = "";
                    for (int i = 0; i < deletedImageIds.size(); i++) {
                        if (!deletedImageIds.get(i).trim().isEmpty())
                            if (i == 0) {
                                imageIds = deletedImageIds.get(i);
                            } else {
                                imageIds = imageIds + "," + deletedImageIds.get(i);
                            }
                    }

                    if (!tempUploadImageList.isEmpty()) {
                        tempUploadImageList.clear();
                    }
                    if (currentScreen.equals(Constants.MY_PROJECTS_SERVICE_PROVIDER_EDIT) || currentScreen.equals(Constants.MY_PROJECTS_SERVICE_PROVIDER_View)) {
                        for (int i = 0; i < uploadImageList.size(); i++) {
                            if (uploadImageList.get(i).getCheck().equals(ScreenNavigation.ADD_ITEM)) {
                                ThumbnailBean beanObj = new ThumbnailBean();
                                beanObj.setAddView(uploadImageList.get(i).isAddView());
                                beanObj.setCheck(uploadImageList.get(i).getCheck());
                                beanObj.setExtension(uploadImageList.get(i).getExtension());
                                beanObj.setFile(uploadImageList.get(i).isFile());
                                beanObj.setName(uploadImageList.get(i).getName());
                                beanObj.setImagePath(uploadImageList.get(i).getImagePath());
                                tempUploadImageList.add(beanObj);
                            }
                        }
                    } else {
                        if (uploadImageList.size() > 0) {
                            tempUploadImageList.addAll(uploadImageList);
                            tempUploadImageList.remove(tempUploadImageList.size() - 1);
                        }
                    }


                    params.put(Constants.PROJECT_NAME, Utils.getInstance().capitalize(mProjectName));
                    params.put(Constants.PROJECT_DESCRIPTION, Utils.getInstance().capitalize(mProjectDescription));
                    params.put(Constants.APPLIANCE_NAME, mProjectAppliance);
                    params.put(Constants.PROPERTY_ADDRESS, mProjectPropertyAddress);
                    params.put(Constants.PROPERTY_Name, mProjectPropertyName);
                    params.put(Constants.EXPERTISE_ID, mSelectedExpertise);
                    params.put(Constants.JOB_TYPE, mProjectJobType);
                    params.put(Constants.IS_DELETED, imageIds);
                    getInterActor().addEdit_Project_Service_Provider(params, ADD_EDIT_PROJECT_SP);
                }
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached())
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
        getActivity().showProgressDialog();
        try {
            PlacePicker.IntentBuilder placeBuilder = new PlacePicker.IntentBuilder();
            ((AddEditProjectServiceProvider) getView()).startActivityForResult(placeBuilder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
            getActivity().hideProgressDialog();
        }
    }

    public void getOnGoingObject(String project_id) {
        if (!TextUtils.isEmpty(project_id)) {
            this.project_id = project_id;
            getProjectDetail(project_id);
        }
    }


    private void getProjectDetail(String projectID) {
        try {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                getActivity().showProgressDialog();
                Map<String, String> params = new HashMap<>();
                params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                params.put(Constants.PROJECT_ID, projectID);
                getInterActor().getProjectDetail(params, ApiName.GET_PROJECT_DETAIL);
            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFilesChosen(List<ChosenFile> list) {
        ChosenFile file = list.get(0);
        String extension = "";
        int i = list.get(0).getDisplayName().lastIndexOf('.');
        if (i > 0) {
            extension = list.get(0).getDisplayName().substring(i + 1);
        }
        if (extension.equals("pdf") || extension.equals("doc") || extension.equals("docx")) {
            long fileSize = file.getSize() / 1024; // convert bytes into Kb
            if (fileSize >= 5120) {
                Utils.getInstance().showToast("File size must be less than 5 MB");
                return;
            }
            manageFileList(file, null, extension);
        } else {
            getActivity().showSnakBar(getView().getRootView(), "you can choose only pdf or doc files.");
        }
    }

    private void manageFileList(ChosenFile file, ChosenImage image, String extension) {
        //Add Images info to list
        if (uploadImageList == null) uploadImageList = new ArrayList<>();
        if (uploadImageList.size() > 0) {
            uploadImageList.remove(uploadImageList.size() - 1);
            editImageAdapter.notifyItemRemoved(uploadImageList.size());
        }
        ThumbnailBean obj = new ThumbnailBean();
        obj.setExtension(extension);
        if (file != null) {
            obj.setImagePath(file.getOriginalPath());
            obj.setName(file.getDisplayName());
        } else if (image != null) {
            obj.setImagePath(image.getThumbnailPath());
            obj.setName(image.getDisplayName());
        }
        obj.setCheck(ScreenNavigation.ADD_ITEM);
        obj.setImageID("");
        obj.setFile(true);
        obj.setAddView(false);
        uploadImageList.add(obj);

        if (editImageAdapter != null) {
            ThumbnailBean obj1 = new ThumbnailBean();
            obj1.setAddView(true);
            obj1.setFile(false);
            obj1.setImagePath("");
            obj1.setImageID("");
            uploadImageList.add(obj1);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    editImageAdapter.notifyDataSetChanged();
                }
            });
            getView().getEditProjectDetailRecycler().smoothScrollToPosition(uploadImageList.size() - 1);
        }
    }
}
