package com.ohp.serviceproviderviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * Created by vishal.sharma on 8/2/2017.
 */

public class GetNearestProjectsResponse extends GenericBean {


    /**
     * totalRecord : 62
     * data : [{"project_id":240,"project_name":"Project3","property_owner":"","project_status":0,"property_name":"01","bids_count":0,"created":"31-07-2017","provider_name":"","project_jobtype":{"jobtype_id":1,"jobtype_title":"Repair"},"project_expertise":[{"expertise_id":1,"expertise_title":"Cleaning Services"}],"away_in_kms":"0.037038129929371784"},{"project_id":239,"project_name":"Project 2","property_owner":"","project_status":0,"property_name":"01","bids_count":0,"created":"31-07-2017","provider_name":"","project_jobtype":{"jobtype_id":3,"jobtype_title":"Garden/Landscaping"},"project_expertise":[{"expertise_id":1,"expertise_title":"Cleaning Services"},{"expertise_id":2,"expertise_title":"Flooring & Carpets"}],"away_in_kms":"0.037038129929371784"},{"project_id":238,"project_name":"Project 1","property_owner":"","project_status":0,"property_name":"01","bids_count":0,"created":"31-07-2017","provider_name":"","project_jobtype":{"jobtype_id":1,"jobtype_title":"Repair"},"project_expertise":[{"expertise_id":1,"expertise_title":"Cleaning Services"}],"away_in_kms":"0.037038129929371784"},{"project_id":235,"project_name":"Znjzjzj","property_owner":"","project_status":1,"property_name":"Bznssn","bids_count":0,"created":"31-07-2017","provider_name":"","project_jobtype":{"jobtype_id":1,"jobtype_title":"Repair"},"project_expertise":[{"expertise_id":1,"expertise_title":"Cleaning Services"},{"expertise_id":2,"expertise_title":"Flooring & Carpets"},{"expertise_id":3,"expertise_title":"Garden/Landscaping"}],"away_in_kms":"0.0530363508759852"},{"project_id":228,"project_name":"Test","property_owner":"","project_status":1,"property_name":"prop 2","bids_count":0,"created":"31-07-2017","provider_name":"","project_jobtype":{"jobtype_id":3,"jobtype_title":"Garden/Landscaping"},"project_expertise":[{"expertise_id":1,"expertise_title":"Cleaning Services"},{"expertise_id":2,"expertise_title":"Flooring & Carpets"},{"expertise_id":3,"expertise_title":"Garden/Landscaping"}],"away_in_kms":"0.016844121082415765"}]
     */

    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * project_id : 240
         * project_name : Project3
         * property_owner :
         * project_status : 0
         * property_name : 01
         * bids_count : 0
         * created : 31-07-2017
         * provider_name :
         * project_jobtype : {"jobtype_id":1,"jobtype_title":"Repair"}
         * project_expertise : [{"expertise_id":1,"expertise_title":"Cleaning Services"}]
         * away_in_kms : 0.037038129929371784
         */

        @SerializedName("project_id")
        private int projectId;
        @SerializedName("project_name")
        private String projectName;
        @SerializedName("property_owner")
        private String propertyOwner;
        @SerializedName("project_status")
        private int projectStatus;
        @SerializedName("property_name")
        private String propertyName;
        @SerializedName("bids_count")
        private int bidsCount;
        @SerializedName("created")
        private String created;
        @SerializedName("provider_name")
        private String providerName;
        @SerializedName("project_jobtype")
        private ProjectJobtypeBean projectJobtype;
        @SerializedName("away_in_kms")
        private String awayInKms;
        @SerializedName("project_expertise")
        private List<ProjectExpertiseBean> projectExpertise;

        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getPropertyOwner() {
            return propertyOwner;
        }

        public void setPropertyOwner(String propertyOwner) {
            this.propertyOwner = propertyOwner;
        }

        public int getProjectStatus() {
            return projectStatus;
        }

        public void setProjectStatus(int projectStatus) {
            this.projectStatus = projectStatus;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public int getBidsCount() {
            return bidsCount;
        }

        public void setBidsCount(int bidsCount) {
            this.bidsCount = bidsCount;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getProviderName() {
            return providerName;
        }

        public void setProviderName(String providerName) {
            this.providerName = providerName;
        }

        public ProjectJobtypeBean getProjectJobtype() {
            return projectJobtype;
        }

        public void setProjectJobtype(ProjectJobtypeBean projectJobtype) {
            this.projectJobtype = projectJobtype;
        }

        public String getAwayInKms() {
            return awayInKms;
        }

        public void setAwayInKms(String awayInKms) {
            this.awayInKms = awayInKms;
        }

        public List<ProjectExpertiseBean> getProjectExpertise() {
            return projectExpertise;
        }

        public void setProjectExpertise(List<ProjectExpertiseBean> projectExpertise) {
            this.projectExpertise = projectExpertise;
        }

        public static class ProjectJobtypeBean {
            /**
             * jobtype_id : 1
             * jobtype_title : Repair
             */

            @SerializedName("jobtype_id")
            private int jobtypeId;
            @SerializedName("jobtype_title")
            private String jobtypeTitle;

            public int getJobtypeId() {
                return jobtypeId;
            }

            public void setJobtypeId(int jobtypeId) {
                this.jobtypeId = jobtypeId;
            }

            public String getJobtypeTitle() {
                return jobtypeTitle;
            }

            public void setJobtypeTitle(String jobtypeTitle) {
                this.jobtypeTitle = jobtypeTitle;
            }
        }

        public static class ProjectExpertiseBean {
            /**
             * expertise_id : 1
             * expertise_title : Cleaning Services
             */

            @SerializedName("expertise_id")
            private int expertiseId;
            @SerializedName("expertise_title")
            private String expertiseTitle;

            public int getExpertiseId() {
                return expertiseId;
            }

            public void setExpertiseId(int expertiseId) {
                this.expertiseId = expertiseId;
            }

            public String getExpertiseTitle() {
                return expertiseTitle;
            }

            public void setExpertiseTitle(String expertiseTitle) {
                this.expertiseTitle = expertiseTitle;
            }
        }
    }
}
