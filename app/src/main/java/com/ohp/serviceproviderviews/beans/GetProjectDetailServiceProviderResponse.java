package com.ohp.serviceproviderviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * Created by vishal.sharma on 8/1/2017.
 */

public class GetProjectDetailServiceProviderResponse extends GenericBean {

    /**
     * data : {"project_id":187,"project_name":"no use","project_description":"gzgbs","project_created":"2017-07-27T11:56:57+00:00","project_owner":{"user_id":223,"full_name":"Mr Sukhdev","gender":"male","address":"E-40 Eltop Area"},"property":{"property_id":179,"property_name":"Sukhdev home 12 ggvfhhvdjb good to me that the first to","property_address":"E-40 Eltop Areaa","property_area":"450","construction_year":"2014","property_description":"This is test description12","property_image":"http://onehomeportal.mobilytedev.com/img/property_picture/original_image/D2fyJz2ABIdMjM0.jpg","resource":[{"title":"beds","value":"4"},{"title":"baths","value":"4"},{"title":"garage","value":"0"},{"title":"type","value":"0"}]},"project_provider":{"provider_id":0,"provider_name":""},"project_images":[{"project_image_id":86,"project_image_avatar":"http://onehomeportal.mobilytedev.com/img/project_picture/original_image/Of0neleCLpsWH6p.jpg"}],"project_appliance":{"appliance_id":94,"appliance_name":"PankhaTest","appliance_brand":"PhilipsTest","appliance_avatar":"","purchase_date":"2017-04-15T00:00:00+00:00","warranty_expiration_date":"2017-04-15T00:00:00+00:00","extended_warranty_date":"2017-04-15T00:00:00+00:00"},"project_jobtype":{"jobtype_id":"1","jobtype_title":"Repair"},"project_expertise":[{"expertise_id":"1","expertise_title":"Cleaning Services"},{"expertise_id":"2","expertise_title":"Flooring & Carpets"},{"expertise_id":"3","expertise_title":"Garden/Landscaping"}],"my_bid":{"bid_id":0,"bid_amount":0},"bids_count":0,"bids":[{"bid_id":1,"bid_amount":21002,"bid_note":"12","bidder_id":6,"bidder_name":"Mr Sukhdevssss","bidder_expertise":[],"bidder_license_no":"123123123123","bidder_avatar":""}]}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * project_id : 187
         * project_name : no use
         * project_description : gzgbs
         * project_created : 2017-07-27T11:56:57+00:00
         * project_owner : {"user_id":223,"full_name":"Mr Sukhdev","gender":"male","address":"E-40 Eltop Area"}
         * property : {"property_id":179,"property_name":"Sukhdev home 12 ggvfhhvdjb good to me that the first to","property_address":"E-40 Eltop Areaa","property_area":"450","construction_year":"2014","property_description":"This is test description12","property_image":"http://onehomeportal.mobilytedev.com/img/property_picture/original_image/D2fyJz2ABIdMjM0.jpg","resource":[{"title":"beds","value":"4"},{"title":"baths","value":"4"},{"title":"garage","value":"0"},{"title":"type","value":"0"}]}
         * project_provider : {"provider_id":0,"provider_name":""}
         * project_images : [{"project_image_id":86,"project_image_avatar":"http://onehomeportal.mobilytedev.com/img/project_picture/original_image/Of0neleCLpsWH6p.jpg"}]
         * project_appliance : {"appliance_id":94,"appliance_name":"PankhaTest","appliance_brand":"PhilipsTest","appliance_avatar":"","purchase_date":"2017-04-15T00:00:00+00:00","warranty_expiration_date":"2017-04-15T00:00:00+00:00","extended_warranty_date":"2017-04-15T00:00:00+00:00"}
         * project_jobtype : {"jobtype_id":"1","jobtype_title":"Repair"}
         * project_expertise : [{"expertise_id":"1","expertise_title":"Cleaning Services"},{"expertise_id":"2","expertise_title":"Flooring & Carpets"},{"expertise_id":"3","expertise_title":"Garden/Landscaping"}]
         * my_bid : {"bid_id":0,"bid_amount":0}
         * bids_count : 0
         * bids : [{"bid_id":1,"bid_amount":21002,"bid_note":"12","bidder_id":6,"bidder_name":"Mr Sukhdevssss","bidder_expertise":[],"bidder_license_no":"123123123123","bidder_avatar":""}]
         */

        @SerializedName("project_id")
        private int projectId;
        @SerializedName("project_name")
        private String projectName;
        @SerializedName("project_description")
        private String projectDescription;
        @SerializedName("created")
        private String projectCreated;
        @SerializedName("project_owner")
        private ProjectOwnerBean projectOwner;
        @SerializedName("property")
        private PropertyBean property;
        @SerializedName("project_provider")
        private ProjectProviderBean projectProvider;
        @SerializedName("project_appliance")
        private ProjectApplianceBean projectAppliance;
        @SerializedName("project_jobtype")
        private ProjectJobtypeBean projectJobtype;
        @SerializedName("my_bid")
        private MyBidBean myBid;
        @SerializedName("bids_count")
        private int bidsCount;
        @SerializedName("project_images")
        private List<ProjectImagesBean> projectImages;
        @SerializedName("project_expertise")
        private List<ProjectExpertiseBean> projectExpertise;
        @SerializedName("bids")
        private List<?> bids;

        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getProjectDescription() {
            return projectDescription;
        }

        public void setProjectDescription(String projectDescription) {
            this.projectDescription = projectDescription;
        }

        public String getProjectCreated() {
            return projectCreated;
        }

        public void setProjectCreated(String projectCreated) {
            this.projectCreated = projectCreated;
        }

        public ProjectOwnerBean getProjectOwner() {
            return projectOwner;
        }

        public void setProjectOwner(ProjectOwnerBean projectOwner) {
            this.projectOwner = projectOwner;
        }

        public PropertyBean getProperty() {
            return property;
        }

        public void setProperty(PropertyBean property) {
            this.property = property;
        }

        public ProjectProviderBean getProjectProvider() {
            return projectProvider;
        }

        public void setProjectProvider(ProjectProviderBean projectProvider) {
            this.projectProvider = projectProvider;
        }

        public ProjectApplianceBean getProjectAppliance() {
            return projectAppliance;
        }

        public void setProjectAppliance(ProjectApplianceBean projectAppliance) {
            this.projectAppliance = projectAppliance;
        }

        public ProjectJobtypeBean getProjectJobtype() {
            return projectJobtype;
        }

        public void setProjectJobtype(ProjectJobtypeBean projectJobtype) {
            this.projectJobtype = projectJobtype;
        }

        public MyBidBean getMyBid() {
            return myBid;
        }

        public void setMyBid(MyBidBean myBid) {
            this.myBid = myBid;
        }

        public int getBidsCount() {
            return bidsCount;
        }

        public void setBidsCount(int bidsCount) {
            this.bidsCount = bidsCount;
        }

        public List<ProjectImagesBean> getProjectImages() {
            return projectImages;
        }

        public void setProjectImages(List<ProjectImagesBean> projectImages) {
            this.projectImages = projectImages;
        }

        public List<ProjectExpertiseBean> getProjectExpertise() {
            return projectExpertise;
        }

        public void setProjectExpertise(List<ProjectExpertiseBean> projectExpertise) {
            this.projectExpertise = projectExpertise;
        }

        public List<?> getBids() {
            return bids;
        }

        public void setBids(List<?> bids) {
            this.bids = bids;
        }

        public static class ProjectOwnerBean {
            /**
             * user_id : 223
             * full_name : Mr Sukhdev
             * gender : male
             * address : E-40 Eltop Area
             */

            @SerializedName("user_id")
            private int userId;
            @SerializedName("full_name")
            private String fullName;
            @SerializedName("gender")
            private String gender;
            @SerializedName("address")
            private String address;

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getFullName() {
                return fullName;
            }

            public void setFullName(String fullName) {
                this.fullName = fullName;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }
        }

        public static class PropertyBean {
            /**
             * property_id : 179
             * property_name : Sukhdev home 12 ggvfhhvdjb good to me that the first to
             * property_address : E-40 Eltop Areaa
             * property_area : 450
             * construction_year : 2014
             * property_description : This is test description12
             * property_image : http://onehomeportal.mobilytedev.com/img/property_picture/original_image/D2fyJz2ABIdMjM0.jpg
             * resource : [{"title":"beds","value":"4"},{"title":"baths","value":"4"},{"title":"garage","value":"0"},{"title":"type","value":"0"}]
             */

            @SerializedName("property_id")
            private int propertyId;
            @SerializedName("property_name")
            private String propertyName;
            @SerializedName("property_address")
            private String propertyAddress;
            @SerializedName("property_area")
            private String propertyArea;
            @SerializedName("construction_year")
            private String constructionYear;
            @SerializedName("property_description")
            private String propertyDescription;
            @SerializedName("property_image")
            private String propertyImage;
            @SerializedName("resource")
            private List<ResourceBean> resource;

            public int getPropertyId() {
                return propertyId;
            }

            public void setPropertyId(int propertyId) {
                this.propertyId = propertyId;
            }

            public String getPropertyName() {
                return propertyName;
            }

            public void setPropertyName(String propertyName) {
                this.propertyName = propertyName;
            }

            public String getPropertyAddress() {
                return propertyAddress;
            }

            public void setPropertyAddress(String propertyAddress) {
                this.propertyAddress = propertyAddress;
            }

            public String getPropertyArea() {
                return propertyArea;
            }

            public void setPropertyArea(String propertyArea) {
                this.propertyArea = propertyArea;
            }

            public String getConstructionYear() {
                return constructionYear;
            }

            public void setConstructionYear(String constructionYear) {
                this.constructionYear = constructionYear;
            }

            public String getPropertyDescription() {
                return propertyDescription;
            }

            public void setPropertyDescription(String propertyDescription) {
                this.propertyDescription = propertyDescription;
            }

            public String getPropertyImage() {
                return propertyImage;
            }

            public void setPropertyImage(String propertyImage) {
                this.propertyImage = propertyImage;
            }

            public List<ResourceBean> getResource() {
                return resource;
            }

            public void setResource(List<ResourceBean> resource) {
                this.resource = resource;
            }

            public static class ResourceBean {
                /**
                 * title : beds
                 * value : 4
                 */

                @SerializedName("title")
                private String title;
                @SerializedName("value")
                private String value;

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }
            }
        }

        public static class ProjectProviderBean {

            /**
             * bid_id : 11
             * bid_amount : 1213
             * bid_note :
             * user_id : 2
             * full_name : service
             * gender : male
             * address : E 38, Phase 8, Industrial Area, Phase 8, Industrial Area, Sahibzada Ajit Singh Nagar, Punjab 140308, India
             * profile_image_100X100 :
             * profile_image_200X200 :
             * is_licensed : 1
             * license_no : 256398877
             * business_name : service enterprises
             * insured : 1
             * website :
             * bonded : 1
             * latitude : 30.708285999999994
             * longitude : 76.7021599
             * expertise : [{"expertise_id":1,"expertise_title":"Cleaning Services"},{"expertise_id":3,"expertise_title":"Garden/Landscaping"}]
             * away_in_kms : 0
             */

            @SerializedName("bid_id")
            private int bidId;
            @SerializedName("bid_amount")
            private int bidAmount;
            @SerializedName("bid_note")
            private String bidNote;
            @SerializedName("user_id")
            private int userId;
            @SerializedName("full_name")
            private String fullName;
            @SerializedName("gender")
            private String gender;
            @SerializedName("address")
            private String address;
            @SerializedName("profile_image_100X100")
            private String profileImage100X100;
            @SerializedName("profile_image_200X200")
            private String profileImage200X200;
            @SerializedName("is_licensed")
            private int isLicensed;
            @SerializedName("license_no")
            private String licenseNo;
            @SerializedName("business_name")
            private String businessName;
            @SerializedName("insured")
            private int insured;
            @SerializedName("website")
            private String website;
            @SerializedName("bonded")
            private int bonded;
            @SerializedName("latitude")
            private String latitude;
            @SerializedName("longitude")
            private String longitude;
            @SerializedName("away_in_kms")
            private int awayInKms;
            @SerializedName("expertise")
            private List<ExpertiseBean> expertise;

            public int getBidId() {
                return bidId;
            }

            public void setBidId(int bidId) {
                this.bidId = bidId;
            }

            public int getBidAmount() {
                return bidAmount;
            }

            public void setBidAmount(int bidAmount) {
                this.bidAmount = bidAmount;
            }

            public String getBidNote() {
                return bidNote;
            }

            public void setBidNote(String bidNote) {
                this.bidNote = bidNote;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getFullName() {
                return fullName;
            }

            public void setFullName(String fullName) {
                this.fullName = fullName;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getProfileImage100X100() {
                return profileImage100X100;
            }

            public void setProfileImage100X100(String profileImage100X100) {
                this.profileImage100X100 = profileImage100X100;
            }

            public String getProfileImage200X200() {
                return profileImage200X200;
            }

            public void setProfileImage200X200(String profileImage200X200) {
                this.profileImage200X200 = profileImage200X200;
            }

            public int getIsLicensed() {
                return isLicensed;
            }

            public void setIsLicensed(int isLicensed) {
                this.isLicensed = isLicensed;
            }

            public String getLicenseNo() {
                return licenseNo;
            }

            public void setLicenseNo(String licenseNo) {
                this.licenseNo = licenseNo;
            }

            public String getBusinessName() {
                return businessName;
            }

            public void setBusinessName(String businessName) {
                this.businessName = businessName;
            }

            public int getInsured() {
                return insured;
            }

            public void setInsured(int insured) {
                this.insured = insured;
            }

            public String getWebsite() {
                return website;
            }

            public void setWebsite(String website) {
                this.website = website;
            }

            public int getBonded() {
                return bonded;
            }

            public void setBonded(int bonded) {
                this.bonded = bonded;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public int getAwayInKms() {
                return awayInKms;
            }

            public void setAwayInKms(int awayInKms) {
                this.awayInKms = awayInKms;
            }

            public List<ExpertiseBean> getExpertise() {
                return expertise;
            }

            public void setExpertise(List<ExpertiseBean> expertise) {
                this.expertise = expertise;
            }

            public static class ExpertiseBean {
                /**
                 * expertise_id : 1
                 * expertise_title : Cleaning Services
                 */

                @SerializedName("expertise_id")
                private int expertiseId;
                @SerializedName("expertise_title")
                private String expertiseTitle;

                public int getExpertiseId() {
                    return expertiseId;
                }

                public void setExpertiseId(int expertiseId) {
                    this.expertiseId = expertiseId;
                }

                public String getExpertiseTitle() {
                    return expertiseTitle;
                }

                public void setExpertiseTitle(String expertiseTitle) {
                    this.expertiseTitle = expertiseTitle;
                }
            }
        }

        public static class ProjectApplianceBean {

            /**
             * appliance_id : 187
             * appliance_name : App 2
             * appliance_brand : Asko
             * appliance_avatar :
             * purchase_date : 2017-08-01T00:00:00+00:00
             * warranty_expiration_date : 2017-12-08T00:00:00+00:00
             * extended_warranty_date :
             * property_appliance_images : [{"appliance_image_id":221,"appliance_avatar":"http://ohpstaging.mobilytedev.com/img/appliance_picture/original_image/yACJpYUkTmE3fcb.png"},{"appliance_image_id":222,"appliance_avatar":"http://ohpstaging.mobilytedev.com/img/appliance_picture/original_image/fI4FypcY5koneRu.jpg"}]
             * property_appliance_doc : [{"appliance_doc_id":264,"appliance_doc_avatar":"http://ohpstaging.mobilytedev.com/img/document_picture/original_image/irVhyPN6mxDK5sm.png"},{"appliance_doc_id":265,"appliance_doc_avatar":"http://ohpstaging.mobilytedev.com/img/document_picture/original_image/KfNqH3JyiJj0gzl.png"}]
             */

            @SerializedName("appliance_id")
            private int applianceId;
            @SerializedName("appliance_name")
            private String applianceName;
            @SerializedName("appliance_brand")
            private String applianceBrand;
            @SerializedName("appliance_avatar")
            private String applianceAvatar;
            @SerializedName("purchase_date")
            private String purchaseDate;
            @SerializedName("warranty_expiration_date")
            private String warrantyExpirationDate;
            @SerializedName("extended_warranty_date")
            private String extendedWarrantyDate;
            @SerializedName("property_appliance_images")
            private List<PropertyApplianceImagesBean> propertyApplianceImages;
            @SerializedName("property_appliance_doc")
            private List<PropertyApplianceDocBean> propertyApplianceDoc;

            public int getApplianceId() {
                return applianceId;
            }

            public void setApplianceId(int applianceId) {
                this.applianceId = applianceId;
            }

            public String getApplianceName() {
                return applianceName;
            }

            public void setApplianceName(String applianceName) {
                this.applianceName = applianceName;
            }

            public String getApplianceBrand() {
                return applianceBrand;
            }

            public void setApplianceBrand(String applianceBrand) {
                this.applianceBrand = applianceBrand;
            }

            public String getApplianceAvatar() {
                return applianceAvatar;
            }

            public void setApplianceAvatar(String applianceAvatar) {
                this.applianceAvatar = applianceAvatar;
            }

            public String getPurchaseDate() {
                return purchaseDate;
            }

            public void setPurchaseDate(String purchaseDate) {
                this.purchaseDate = purchaseDate;
            }

            public String getWarrantyExpirationDate() {
                return warrantyExpirationDate;
            }

            public void setWarrantyExpirationDate(String warrantyExpirationDate) {
                this.warrantyExpirationDate = warrantyExpirationDate;
            }

            public String getExtendedWarrantyDate() {
                return extendedWarrantyDate;
            }

            public void setExtendedWarrantyDate(String extendedWarrantyDate) {
                this.extendedWarrantyDate = extendedWarrantyDate;
            }

            public List<PropertyApplianceImagesBean> getPropertyApplianceImages() {
                return propertyApplianceImages;
            }

            public void setPropertyApplianceImages(List<PropertyApplianceImagesBean> propertyApplianceImages) {
                this.propertyApplianceImages = propertyApplianceImages;
            }

            public List<PropertyApplianceDocBean> getPropertyApplianceDoc() {
                return propertyApplianceDoc;
            }

            public void setPropertyApplianceDoc(List<PropertyApplianceDocBean> propertyApplianceDoc) {
                this.propertyApplianceDoc = propertyApplianceDoc;
            }

            public static class PropertyApplianceImagesBean {
                /**
                 * appliance_image_id : 221
                 * appliance_avatar : http://ohpstaging.mobilytedev.com/img/appliance_picture/original_image/yACJpYUkTmE3fcb.png
                 */

                @SerializedName("appliance_image_id")
                private int applianceImageId;
                @SerializedName("appliance_avatar")
                private String applianceAvatar;

                public int getApplianceImageId() {
                    return applianceImageId;
                }

                public void setApplianceImageId(int applianceImageId) {
                    this.applianceImageId = applianceImageId;
                }

                public String getApplianceAvatar() {
                    return applianceAvatar;
                }

                public void setApplianceAvatar(String applianceAvatar) {
                    this.applianceAvatar = applianceAvatar;
                }
            }

            public static class PropertyApplianceDocBean {
                /**
                 * appliance_doc_id : 264
                 * appliance_doc_avatar : http://ohpstaging.mobilytedev.com/img/document_picture/original_image/irVhyPN6mxDK5sm.png
                 */

                @SerializedName("appliance_doc_id")
                private int applianceDocId;
                @SerializedName("appliance_doc_avatar")
                private String applianceDocAvatar;

                public int getApplianceDocId() {
                    return applianceDocId;
                }

                public void setApplianceDocId(int applianceDocId) {
                    this.applianceDocId = applianceDocId;
                }

                public String getApplianceDocAvatar() {
                    return applianceDocAvatar;
                }

                public void setApplianceDocAvatar(String applianceDocAvatar) {
                    this.applianceDocAvatar = applianceDocAvatar;
                }
            }
        }

        public static class ProjectJobtypeBean {
            /**
             * jobtype_id : 1
             * jobtype_title : Repair
             */

            @SerializedName("jobtype_id")
            private int jobtypeId;
            @SerializedName("jobtype_title")
            private String jobtypeTitle;

            public int getJobtypeId() {
                return jobtypeId;
            }

            public void setJobtypeId(int jobtypeId) {
                this.jobtypeId = jobtypeId;
            }

            public String getJobtypeTitle() {
                return jobtypeTitle;
            }

            public void setJobtypeTitle(String jobtypeTitle) {
                this.jobtypeTitle = jobtypeTitle;
            }
        }

        public static class MyBidBean {
            /**
             * bid_id : 0
             * bid_amount : 0
             */

            @SerializedName("bid_id")
            private int bidId;
            @SerializedName("bid_amount")
            private int bidAmount;
            /**
             * bid_note :
             */

            @SerializedName("bid_note")
            private String bidNote;

            public int getBidId() {
                return bidId;
            }

            public void setBidId(int bidId) {
                this.bidId = bidId;
            }

            public int getBidAmount() {
                return bidAmount;
            }

            public void setBidAmount(int bidAmount) {
                this.bidAmount = bidAmount;
            }

            public String getBidNote() {
                return bidNote;
            }

            public void setBidNote(String bidNote) {
                this.bidNote = bidNote;
            }
        }

        public static class ProjectImagesBean {
            /**
             * project_image_id : 86
             * project_image_avatar : http://onehomeportal.mobilytedev.com/img/project_picture/original_image/Of0neleCLpsWH6p.jpg
             */

            @SerializedName("project_image_id")
            private int projectImageId;
            @SerializedName("project_image_avatar")
            private String projectImageAvatar;

            public int getProjectImageId() {
                return projectImageId;
            }

            public void setProjectImageId(int projectImageId) {
                this.projectImageId = projectImageId;
            }

            public String getProjectImageAvatar() {
                return projectImageAvatar;
            }

            public void setProjectImageAvatar(String projectImageAvatar) {
                this.projectImageAvatar = projectImageAvatar;
            }
        }

        public static class ProjectExpertiseBean {
            /**
             * expertise_id : 1
             * expertise_title : Cleaning Services
             */

            @SerializedName("expertise_id")
            private int expertiseId;
            @SerializedName("expertise_title")
            private String expertiseTitle;

            public int getExpertiseId() {
                return expertiseId;
            }

            public void setExpertiseId(int expertiseId) {
                this.expertiseId = expertiseId;
            }

            public String getExpertiseTitle() {
                return expertiseTitle;
            }

            public void setExpertiseTitle(String expertiseTitle) {
                this.expertiseTitle = expertiseTitle;
            }
        }


    }
}
