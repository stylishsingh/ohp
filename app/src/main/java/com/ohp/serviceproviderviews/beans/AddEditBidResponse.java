package com.ohp.serviceproviderviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

/**
 * Created by vishal.sharma on 8/2/2017.
 */

public class AddEditBidResponse extends GenericBean {


    /**
     * status : 200
     * data : {"bid_id":13}
     * message : Bid added successfully.
     */


    @SerializedName("data")
    private DataBean data;




    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }



    public static class DataBean {
        /**
         * bid_id : 13
         */

        @SerializedName("bid_id")
        private int bidId;

        public int getBidId() {
            return bidId;
        }

        public void setBidId(int bidId) {
            this.bidId = bidId;
        }
    }
}
