package com.ohp.serviceproviderviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

/**
 * Created by vishal.sharma on 8/16/2017.
 */

public class AddEditProjectSpResponse extends GenericBean {

    /**
     * status : 200
     * data : {"project_id":3}
     * message : Project added successfully.
     */


    @SerializedName("data")
    private DataBean data;


    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }


    public static class DataBean {
        /**
         * project_id : 3
         */

        @SerializedName("project_id")
        private int projectId;

        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }
    }
}
