package com.ohp.serviceproviderviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * Created by vishal.sharma on 8/28/2017.
 */

public class GetTaskDetailResponse extends GenericBean {


    /**
     * status : 200
     * data : {"task_id":16,"task_name":"Gcc","task_notes":"Fdch","task_time_from":"2017-08-30T03:10:00+00:00","task_time_to":"2017-08-30T04:00:00+00:00","task_reminder":"2017-08-29T08:51:00+00:00","task_repeat":"Weekly","created":"2017-08-28T06:25:25+00:00","property":{"proeprty_id":0,"property_name":"Default Property","property_owner_name":"service"},"task_images":[{"image_id":3,"image":"http://onehomeportal.mobilytedev.com/img/document_picture/original_image/nijwHzL3VfTucK5.jpg"}],"member":{"member_id":29,"member_name":"Rohit1233"},"project":{"project_id":83,"project_name":"Mandeepss"}}
     */


    @SerializedName("data")
    private DataBean data;


    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * task_id : 16
         * task_name : Gcc
         * task_notes : Fdch
         * task_time_from : 2017-08-30T03:10:00+00:00
         * task_time_to : 2017-08-30T04:00:00+00:00
         * task_reminder : 2017-08-29T08:51:00+00:00
         * task_repeat : Weekly
         * created : 2017-08-28T06:25:25+00:00
         * property : {"proeprty_id":0,"property_name":"Default Property","property_owner_name":"service"}
         * task_images : [{"image_id":3,"image":"http://onehomeportal.mobilytedev.com/img/document_picture/original_image/nijwHzL3VfTucK5.jpg"}]
         * member : {"member_id":29,"member_name":"Rohit1233"}
         * project : {"project_id":83,"project_name":"Mandeepss"}
         */

        @SerializedName("task_id")
        private int taskId;
        @SerializedName("task_status")
        private int taskStatus;
        @SerializedName("task_type")
        private int taskType;
        @SerializedName("task_name")
        private String taskName;
        @SerializedName("task_notes")
        private String taskNotes;
        @SerializedName("task_time_from")
        private String taskTimeFrom;
        @SerializedName("task_time_to")
        private String taskTimeTo;
        @SerializedName("task_reminder")
        private String taskReminder;
        @SerializedName("task_repeat")
        private String taskRepeat;
        @SerializedName("created")
        private String created;
        @SerializedName("property")
        private PropertyBean property;
        @SerializedName("member")
        private MemberBean member;
        @SerializedName("project")
        private ProjectBean project;
        @SerializedName("task_images")
        private List<TaskImagesBean> taskImages;

        public int getTaskType() {
            return taskType;
        }

        public void setTaskType(int taskType) {
            this.taskType = taskType;
        }

        public int getTaskStatus() {
            return taskStatus;
        }

        public void setTaskStatus(int taskStatus) {
            this.taskStatus = taskStatus;
        }

        public int getTaskId() {
            return taskId;
        }

        public void setTaskId(int taskId) {
            this.taskId = taskId;
        }

        public String getTaskName() {
            return taskName;
        }

        public void setTaskName(String taskName) {
            this.taskName = taskName;
        }

        public String getTaskNotes() {
            return taskNotes;
        }

        public void setTaskNotes(String taskNotes) {
            this.taskNotes = taskNotes;
        }

        public String getTaskTimeFrom() {
            return taskTimeFrom;
        }

        public void setTaskTimeFrom(String taskTimeFrom) {
            this.taskTimeFrom = taskTimeFrom;
        }

        public String getTaskTimeTo() {
            return taskTimeTo;
        }

        public void setTaskTimeTo(String taskTimeTo) {
            this.taskTimeTo = taskTimeTo;
        }

        public String getTaskReminder() {
            return taskReminder;
        }

        public void setTaskReminder(String taskReminder) {
            this.taskReminder = taskReminder;
        }

        public String getTaskRepeat() {
            return taskRepeat;
        }

        public void setTaskRepeat(String taskRepeat) {
            this.taskRepeat = taskRepeat;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public PropertyBean getProperty() {
            return property;
        }

        public void setProperty(PropertyBean property) {
            this.property = property;
        }

        public MemberBean getMember() {
            return member;
        }

        public void setMember(MemberBean member) {
            this.member = member;
        }

        public ProjectBean getProject() {
            return project;
        }

        public void setProject(ProjectBean project) {
            this.project = project;
        }

        public List<TaskImagesBean> getTaskImages() {
            return taskImages;
        }

        public void setTaskImages(List<TaskImagesBean> taskImages) {
            this.taskImages = taskImages;
        }

        public static class PropertyBean {
            /**
             * proeprty_id : 0
             * property_name : Default Property
             * property_owner_name : service
             */

            @SerializedName("proeprty_id")
            private int proeprtyId;
            @SerializedName("property_name")
            private String propertyName;
            @SerializedName("property_owner_name")
            private String propertyOwnerName;

            public int getProeprtyId() {
                return proeprtyId;
            }

            public void setProeprtyId(int proeprtyId) {
                this.proeprtyId = proeprtyId;
            }

            public String getPropertyName() {
                return propertyName;
            }

            public void setPropertyName(String propertyName) {
                this.propertyName = propertyName;
            }

            public String getPropertyOwnerName() {
                return propertyOwnerName;
            }

            public void setPropertyOwnerName(String propertyOwnerName) {
                this.propertyOwnerName = propertyOwnerName;
            }
        }

        public static class MemberBean {
            /**
             * member_id : 29
             * member_name : Rohit1233
             */

            @SerializedName("member_id")
            private int memberId;
            @SerializedName("member_name")
            private String memberName;

            public int getMemberId() {
                return memberId;
            }

            public void setMemberId(int memberId) {
                this.memberId = memberId;
            }

            public String getMemberName() {
                return memberName;
            }

            public void setMemberName(String memberName) {
                this.memberName = memberName;
            }
        }

        public static class ProjectBean {
            /**
             * project_id : 83
             * project_name : Mandeepss
             */

            @SerializedName("project_id")
            private int projectId;
            @SerializedName("project_name")
            private String projectName;

            public int getProjectId() {
                return projectId;
            }

            public void setProjectId(int projectId) {
                this.projectId = projectId;
            }

            public String getProjectName() {
                return projectName;
            }

            public void setProjectName(String projectName) {
                this.projectName = projectName;
            }
        }

        public static class TaskImagesBean {
            /**
             * image_id : 3
             * image : http://onehomeportal.mobilytedev.com/img/document_picture/original_image/nijwHzL3VfTucK5.jpg
             */

            @SerializedName("image_id")
            private int imageId;
            @SerializedName("image")
            private String image;

            public int getImageId() {
                return imageId;
            }

            public void setImageId(int imageId) {
                this.imageId = imageId;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }
    }
}
