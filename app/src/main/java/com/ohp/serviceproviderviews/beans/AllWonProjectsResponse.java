package com.ohp.serviceproviderviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * Created by vishal.sharma on 7/31/2017.
 */

public class AllWonProjectsResponse extends GenericBean {


    /**
     * totalRecord : 4
     * data : {"completed_projects":[{"project_id":231,"project_name":"Qwew","property_owner":"Mandeepft","project_status":3,"property_name":"test","bids_count":0,"created":"31-07-2017","provider_name":"","project_jobtype":{"jobtype_id":1,"jobtype_title":"Repair"},"project_expertise":[{"expertise_id":1,"expertise_title":"Cleaning Services"}],"away_in_kms":""}],"my_projects":[{"project_id":236,"project_name":"Test new project2","property_owner":"Mr Sukhdev","project_status":4,"property_name":"Sukhdev home 12 ggvfhhvdjb good to me that the first to","bids_count":0,"created":"31-07-2017","provider_name":"","project_jobtype":{"jobtype_id":2,"jobtype_title":"Removal/Recycle"},"project_expertise":[{"expertise_id":1,"expertise_title":"Cleaning Services"},{"expertise_id":2,"expertise_title":"Flooring & Carpets"}],"away_in_kms":""}],"won_projects":[{"project_id":233,"project_name":"1111","property_owner":"Mandeepft","project_status":2,"property_name":"test","bids_count":0,"created":"31-07-2017","provider_name":"","project_jobtype":{"jobtype_id":1,"jobtype_title":"Repair"},"project_expertise":[{"expertise_id":1,"expertise_title":"Cleaning Services"}],"away_in_kms":""},{"project_id":232,"project_name":"Asdsadasda","property_owner":"Mandeepft","project_status":2,"property_name":"qwswasa","bids_count":0,"created":"31-07-2017","provider_name":"","project_jobtype":{"jobtype_id":1,"jobtype_title":"Repair"},"project_expertise":[{"expertise_id":1,"expertise_title":"Cleaning Services"}],"away_in_kms":""}]}
     */

    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private DataBean data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        @SerializedName("completed_projects")
        private List<CompletedProjectsBean> completedProjects;
        @SerializedName("my_projects")
        private List<MyProjectsBean> myProjects;
        @SerializedName("won_projects")
        private List<WonProjectsBean> wonProjects;

        public List<CompletedProjectsBean> getCompletedProjects() {
            return completedProjects;
        }

        public void setCompletedProjects(List<CompletedProjectsBean> completedProjects) {
            this.completedProjects = completedProjects;
        }

        public List<MyProjectsBean> getMyProjects() {
            return myProjects;
        }

        public void setMyProjects(List<MyProjectsBean> myProjects) {
            this.myProjects = myProjects;
        }

        public List<WonProjectsBean> getWonProjects() {
            return wonProjects;
        }

        public void setWonProjects(List<WonProjectsBean> wonProjects) {
            this.wonProjects = wonProjects;
        }

        public static class CompletedProjectsBean {
            /**
             * project_id : 231
             * project_name : Qwew
             * property_owner : Mandeepft
             * project_status : 3
             * property_name : test
             * bids_count : 0
             * created : 31-07-2017
             * provider_name :
             * project_jobtype : {"jobtype_id":1,"jobtype_title":"Repair"}
             * project_expertise : [{"expertise_id":1,"expertise_title":"Cleaning Services"}]
             * away_in_kms :
             */

            @SerializedName("project_id")
            private int projectId;
            @SerializedName("project_name")
            private String projectName;
            @SerializedName("property_owner")
            private String propertyOwner;
            @SerializedName("project_status")
            private int projectStatus;
            @SerializedName("property_name")
            private String propertyName;
            @SerializedName("bids_count")
            private int bidsCount;
            @SerializedName("created")
            private String created;
            @SerializedName("provider_name")
            private String providerName;
            @SerializedName("project_jobtype")
            private ProjectJobtypeBean projectJobtype;
            @SerializedName("away_in_kms")
            private String awayInKms;
            @SerializedName("project_expertise")
            private List<ProjectExpertiseBean> projectExpertise;

            public int getProjectId() {
                return projectId;
            }

            public void setProjectId(int projectId) {
                this.projectId = projectId;
            }

            public String getProjectName() {
                return projectName;
            }

            public void setProjectName(String projectName) {
                this.projectName = projectName;
            }

            public String getPropertyOwner() {
                return propertyOwner;
            }

            public void setPropertyOwner(String propertyOwner) {
                this.propertyOwner = propertyOwner;
            }

            public int getProjectStatus() {
                return projectStatus;
            }

            public void setProjectStatus(int projectStatus) {
                this.projectStatus = projectStatus;
            }

            public String getPropertyName() {
                return propertyName;
            }

            public void setPropertyName(String propertyName) {
                this.propertyName = propertyName;
            }

            public int getBidsCount() {
                return bidsCount;
            }

            public void setBidsCount(int bidsCount) {
                this.bidsCount = bidsCount;
            }

            public String getCreated() {
                return created;
            }

            public void setCreated(String created) {
                this.created = created;
            }

            public String getProviderName() {
                return providerName;
            }

            public void setProviderName(String providerName) {
                this.providerName = providerName;
            }

            public ProjectJobtypeBean getProjectJobtype() {
                return projectJobtype;
            }

            public void setProjectJobtype(ProjectJobtypeBean projectJobtype) {
                this.projectJobtype = projectJobtype;
            }

            public String getAwayInKms() {
                return awayInKms;
            }

            public void setAwayInKms(String awayInKms) {
                this.awayInKms = awayInKms;
            }

            public List<ProjectExpertiseBean> getProjectExpertise() {
                return projectExpertise;
            }

            public void setProjectExpertise(List<ProjectExpertiseBean> projectExpertise) {
                this.projectExpertise = projectExpertise;
            }

            public static class ProjectJobtypeBean {
                /**
                 * jobtype_id : 1
                 * jobtype_title : Repair
                 */

                @SerializedName("jobtype_id")
                private int jobtypeId;
                @SerializedName("jobtype_title")
                private String jobtypeTitle;

                public int getJobtypeId() {
                    return jobtypeId;
                }

                public void setJobtypeId(int jobtypeId) {
                    this.jobtypeId = jobtypeId;
                }

                public String getJobtypeTitle() {
                    return jobtypeTitle;
                }

                public void setJobtypeTitle(String jobtypeTitle) {
                    this.jobtypeTitle = jobtypeTitle;
                }
            }

            public static class ProjectExpertiseBean {
                /**
                 * expertise_id : 1
                 * expertise_title : Cleaning Services
                 */

                @SerializedName("expertise_id")
                private int expertiseId;
                @SerializedName("expertise_title")
                private String expertiseTitle;

                public int getExpertiseId() {
                    return expertiseId;
                }

                public void setExpertiseId(int expertiseId) {
                    this.expertiseId = expertiseId;
                }

                public String getExpertiseTitle() {
                    return expertiseTitle;
                }

                public void setExpertiseTitle(String expertiseTitle) {
                    this.expertiseTitle = expertiseTitle;
                }
            }
        }

        public static class MyProjectsBean {
            /**
             * project_id : 236
             * project_name : Test new project2
             * property_owner : Mr Sukhdev
             * project_status : 4
             * property_name : Sukhdev home 12 ggvfhhvdjb good to me that the first to
             * bids_count : 0
             * created : 31-07-2017
             * provider_name :
             * project_jobtype : {"jobtype_id":2,"jobtype_title":"Removal/Recycle"}
             * project_expertise : [{"expertise_id":1,"expertise_title":"Cleaning Services"},{"expertise_id":2,"expertise_title":"Flooring & Carpets"}]
             * away_in_kms :
             */

            @SerializedName("project_id")
            private int projectId;
            @SerializedName("project_name")
            private String projectName;
            @SerializedName("property_owner")
            private String propertyOwner;
            @SerializedName("project_status")
            private int projectStatus;
            @SerializedName("property_name")
            private String propertyName;
            @SerializedName("bids_count")
            private int bidsCount;
            @SerializedName("created")
            private String created;
            @SerializedName("provider_name")
            private String providerName;
            @SerializedName("project_jobtype")
            private ProjectJobtypeBeanX projectJobtype;
            @SerializedName("away_in_kms")
            private String awayInKms;
            @SerializedName("project_expertise")
            private List<ProjectExpertiseBeanX> projectExpertise;

            public int getProjectId() {
                return projectId;
            }

            public void setProjectId(int projectId) {
                this.projectId = projectId;
            }

            public String getProjectName() {
                return projectName;
            }

            public void setProjectName(String projectName) {
                this.projectName = projectName;
            }

            public String getPropertyOwner() {
                return propertyOwner;
            }

            public void setPropertyOwner(String propertyOwner) {
                this.propertyOwner = propertyOwner;
            }

            public int getProjectStatus() {
                return projectStatus;
            }

            public void setProjectStatus(int projectStatus) {
                this.projectStatus = projectStatus;
            }

            public String getPropertyName() {
                return propertyName;
            }

            public void setPropertyName(String propertyName) {
                this.propertyName = propertyName;
            }

            public int getBidsCount() {
                return bidsCount;
            }

            public void setBidsCount(int bidsCount) {
                this.bidsCount = bidsCount;
            }

            public String getCreated() {
                return created;
            }

            public void setCreated(String created) {
                this.created = created;
            }

            public String getProviderName() {
                return providerName;
            }

            public void setProviderName(String providerName) {
                this.providerName = providerName;
            }

            public ProjectJobtypeBeanX getProjectJobtype() {
                return projectJobtype;
            }

            public void setProjectJobtype(ProjectJobtypeBeanX projectJobtype) {
                this.projectJobtype = projectJobtype;
            }

            public String getAwayInKms() {
                return awayInKms;
            }

            public void setAwayInKms(String awayInKms) {
                this.awayInKms = awayInKms;
            }

            public List<ProjectExpertiseBeanX> getProjectExpertise() {
                return projectExpertise;
            }

            public void setProjectExpertise(List<ProjectExpertiseBeanX> projectExpertise) {
                this.projectExpertise = projectExpertise;
            }

            public static class ProjectJobtypeBeanX {
                /**
                 * jobtype_id : 2
                 * jobtype_title : Removal/Recycle
                 */

                @SerializedName("jobtype_id")
                private int jobtypeId;
                @SerializedName("jobtype_title")
                private String jobtypeTitle;

                public int getJobtypeId() {
                    return jobtypeId;
                }

                public void setJobtypeId(int jobtypeId) {
                    this.jobtypeId = jobtypeId;
                }

                public String getJobtypeTitle() {
                    return jobtypeTitle;
                }

                public void setJobtypeTitle(String jobtypeTitle) {
                    this.jobtypeTitle = jobtypeTitle;
                }
            }

            public static class ProjectExpertiseBeanX {
                /**
                 * expertise_id : 1
                 * expertise_title : Cleaning Services
                 */

                @SerializedName("expertise_id")
                private int expertiseId;
                @SerializedName("expertise_title")
                private String expertiseTitle;

                public int getExpertiseId() {
                    return expertiseId;
                }

                public void setExpertiseId(int expertiseId) {
                    this.expertiseId = expertiseId;
                }

                public String getExpertiseTitle() {
                    return expertiseTitle;
                }

                public void setExpertiseTitle(String expertiseTitle) {
                    this.expertiseTitle = expertiseTitle;
                }
            }
        }

        public static class WonProjectsBean {
            /**
             * project_id : 233
             * project_name : 1111
             * property_owner : Mandeepft
             * project_status : 2
             * property_name : test
             * bids_count : 0
             * created : 31-07-2017
             * provider_name :
             * project_jobtype : {"jobtype_id":1,"jobtype_title":"Repair"}
             * project_expertise : [{"expertise_id":1,"expertise_title":"Cleaning Services"}]
             * away_in_kms :
             */

            @SerializedName("project_id")
            private int projectId;
            @SerializedName("project_name")
            private String projectName;
            @SerializedName("property_owner")
            private String propertyOwner;
            @SerializedName("project_status")
            private int projectStatus;
            @SerializedName("property_name")
            private String propertyName;
            @SerializedName("bids_count")
            private int bidsCount;
            @SerializedName("created")
            private String created;
            @SerializedName("provider_name")
            private String providerName;
            @SerializedName("project_jobtype")
            private ProjectJobtypeBeanXX projectJobtype;
            @SerializedName("away_in_kms")
            private String awayInKms;
            @SerializedName("project_expertise")
            private List<ProjectExpertiseBeanXX> projectExpertise;

            public int getProjectId() {
                return projectId;
            }

            public void setProjectId(int projectId) {
                this.projectId = projectId;
            }

            public String getProjectName() {
                return projectName;
            }

            public void setProjectName(String projectName) {
                this.projectName = projectName;
            }

            public String getPropertyOwner() {
                return propertyOwner;
            }

            public void setPropertyOwner(String propertyOwner) {
                this.propertyOwner = propertyOwner;
            }

            public int getProjectStatus() {
                return projectStatus;
            }

            public void setProjectStatus(int projectStatus) {
                this.projectStatus = projectStatus;
            }

            public String getPropertyName() {
                return propertyName;
            }

            public void setPropertyName(String propertyName) {
                this.propertyName = propertyName;
            }

            public int getBidsCount() {
                return bidsCount;
            }

            public void setBidsCount(int bidsCount) {
                this.bidsCount = bidsCount;
            }

            public String getCreated() {
                return created;
            }

            public void setCreated(String created) {
                this.created = created;
            }

            public String getProviderName() {
                return providerName;
            }

            public void setProviderName(String providerName) {
                this.providerName = providerName;
            }

            public ProjectJobtypeBeanXX getProjectJobtype() {
                return projectJobtype;
            }

            public void setProjectJobtype(ProjectJobtypeBeanXX projectJobtype) {
                this.projectJobtype = projectJobtype;
            }

            public String getAwayInKms() {
                return awayInKms;
            }

            public void setAwayInKms(String awayInKms) {
                this.awayInKms = awayInKms;
            }

            public List<ProjectExpertiseBeanXX> getProjectExpertise() {
                return projectExpertise;
            }

            public void setProjectExpertise(List<ProjectExpertiseBeanXX> projectExpertise) {
                this.projectExpertise = projectExpertise;
            }

            public static class ProjectJobtypeBeanXX {
                /**
                 * jobtype_id : 1
                 * jobtype_title : Repair
                 */

                @SerializedName("jobtype_id")
                private int jobtypeId;
                @SerializedName("jobtype_title")
                private String jobtypeTitle;

                public int getJobtypeId() {
                    return jobtypeId;
                }

                public void setJobtypeId(int jobtypeId) {
                    this.jobtypeId = jobtypeId;
                }

                public String getJobtypeTitle() {
                    return jobtypeTitle;
                }

                public void setJobtypeTitle(String jobtypeTitle) {
                    this.jobtypeTitle = jobtypeTitle;
                }
            }

            public static class ProjectExpertiseBeanXX {
                /**
                 * expertise_id : 1
                 * expertise_title : Cleaning Services
                 */

                @SerializedName("expertise_id")
                private int expertiseId;
                @SerializedName("expertise_title")
                private String expertiseTitle;

                public int getExpertiseId() {
                    return expertiseId;
                }

                public void setExpertiseId(int expertiseId) {
                    this.expertiseId = expertiseId;
                }

                public String getExpertiseTitle() {
                    return expertiseTitle;
                }

                public void setExpertiseTitle(String expertiseTitle) {
                    this.expertiseTitle = expertiseTitle;
                }
            }
        }
    }
}
