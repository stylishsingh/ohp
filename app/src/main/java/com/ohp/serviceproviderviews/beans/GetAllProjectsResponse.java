package com.ohp.serviceproviderviews.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vishal.sharma on 7/18/2017.
 */

public class GetAllProjectsResponse extends GenericBean {


    /**
     * totalRecord : 3
     * data : {"completed_project":[{"project_id":66,"project_name":"Test new projectw","property_name":"testing propert","bids_count":0,"created":"17-07-2017","provider_name":"","project_jobtype":{"jobtype_id":2,"jobtype_title":"Removal/Recycle"},"project_expertise":[{"expertise_id":1,"expertise_title":"Cleaning Services"},{"expertise_id":2,"expertise_title":"Flooring & Carpets"}]}],"upcomming_project":[{"project_id":64,"project_name":"Test new project","property_name":"testing propert","bids_count":0,"created":"17-07-2017","provider_name":"","project_jobtype":{"jobtype_id":2,"jobtype_title":"Removal/Recycle"},"project_expertise":[{"expertise_id":1,"expertise_title":"Cleaning Services"},{"expertise_id":2,"expertise_title":"Flooring & Carpets"}]}],"ongoing_project":[{"project_id":65,"project_name":"Test new projectw","property_name":"testing propert","bids_count":0,"created":"17-07-2017","provider_name":"","project_jobtype":{"jobtype_id":2,"jobtype_title":"Removal/Recycle"},"project_expertise":[{"expertise_id":1,"expertise_title":"Cleaning Services"},{"expertise_id":2,"expertise_title":"Flooring & Carpets"}]}]}
     */
    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private DataBean data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
        private List<CompletedProjectBean> completed_project;
        private List<UpcommingProjectBean> upcomming_project;
        private List<OngoingProjectBean> ongoing_project;

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.completed_project = new ArrayList<CompletedProjectBean>();
            in.readList(this.completed_project, CompletedProjectBean.class.getClassLoader());
            this.upcomming_project = new ArrayList<UpcommingProjectBean>();
            in.readList(this.upcomming_project, UpcommingProjectBean.class.getClassLoader());
            this.ongoing_project = in.createTypedArrayList(OngoingProjectBean.CREATOR);
        }

        public List<CompletedProjectBean> getCompleted_project() {
            return completed_project;
        }

        public void setCompleted_project(List<CompletedProjectBean> completed_project) {
            this.completed_project = completed_project;
        }

        public List<UpcommingProjectBean> getUpcomming_project() {
            return upcomming_project;
        }

        public void setUpcomming_project(List<UpcommingProjectBean> upcomming_project) {
            this.upcomming_project = upcomming_project;
        }

        public List<OngoingProjectBean> getOngoing_project() {
            return ongoing_project;
        }

        public void setOngoing_project(List<OngoingProjectBean> ongoing_project) {
            this.ongoing_project = ongoing_project;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeList(this.completed_project);
            dest.writeList(this.upcomming_project);
            dest.writeTypedList(this.ongoing_project);
        }

        public static class CompletedProjectBean implements Parcelable {
            public static final Creator<CompletedProjectBean> CREATOR = new Creator<CompletedProjectBean>() {
                @Override
                public CompletedProjectBean createFromParcel(Parcel source) {
                    return new CompletedProjectBean(source);
                }

                @Override
                public CompletedProjectBean[] newArray(int size) {
                    return new CompletedProjectBean[size];
                }
            };
            /**
             * project_id : 66
             * project_name : Test new projectw
             * property_name : testing propert
             * bids_count : 0
             * created : 17-07-2017
             * provider_name :
             * project_jobtype : {"jobtype_id":2,"jobtype_title":"Removal/Recycle"}
             * project_expertise : [{"expertise_id":1,"expertise_title":"Cleaning Services"},{"expertise_id":2,"expertise_title":"Flooring & Carpets"}]
             */

            private int project_id;
            private String project_name;
            private String property_name;
            private int bids_count;
            private String created;
            private String provider_name;
            private ProjectJobtypeBean project_jobtype;
            private List<ProjectExpertiseBean> project_expertise;

            public CompletedProjectBean() {
            }

            protected CompletedProjectBean(Parcel in) {
                this.project_id = in.readInt();
                this.project_name = in.readString();
                this.property_name = in.readString();
                this.bids_count = in.readInt();
                this.created = in.readString();
                this.provider_name = in.readString();
                this.project_jobtype = in.readParcelable(ProjectJobtypeBean.class.getClassLoader());
                this.project_expertise = new ArrayList<ProjectExpertiseBean>();
                in.readList(this.project_expertise, ProjectExpertiseBean.class.getClassLoader());
            }

            public int getProject_id() {
                return project_id;
            }

            public void setProject_id(int project_id) {
                this.project_id = project_id;
            }

            public String getProject_name() {
                return project_name;
            }

            public void setProject_name(String project_name) {
                this.project_name = project_name;
            }

            public String getProperty_name() {
                return property_name;
            }

            public void setProperty_name(String property_name) {
                this.property_name = property_name;
            }

            public int getBids_count() {
                return bids_count;
            }

            public void setBids_count(int bids_count) {
                this.bids_count = bids_count;
            }

            public String getCreated() {
                return created;
            }

            public void setCreated(String created) {
                this.created = created;
            }

            public String getProvider_name() {
                return provider_name;
            }

            public void setProvider_name(String provider_name) {
                this.provider_name = provider_name;
            }

            public ProjectJobtypeBean getProject_jobtype() {
                return project_jobtype;
            }

            public void setProject_jobtype(ProjectJobtypeBean project_jobtype) {
                this.project_jobtype = project_jobtype;
            }

            public List<ProjectExpertiseBean> getProject_expertise() {
                return project_expertise;
            }

            public void setProject_expertise(List<ProjectExpertiseBean> project_expertise) {
                this.project_expertise = project_expertise;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.project_id);
                dest.writeString(this.project_name);
                dest.writeString(this.property_name);
                dest.writeInt(this.bids_count);
                dest.writeString(this.created);
                dest.writeString(this.provider_name);
                dest.writeParcelable(this.project_jobtype, flags);
                dest.writeList(this.project_expertise);
            }

            public static class ProjectJobtypeBean implements Parcelable {
                public static final Creator<ProjectJobtypeBean> CREATOR = new Creator<ProjectJobtypeBean>() {
                    @Override
                    public ProjectJobtypeBean createFromParcel(Parcel source) {
                        return new ProjectJobtypeBean(source);
                    }

                    @Override
                    public ProjectJobtypeBean[] newArray(int size) {
                        return new ProjectJobtypeBean[size];
                    }
                };
                /**
                 * jobtype_id : 2
                 * jobtype_title : Removal/Recycle
                 */

                private int jobtype_id;
                private String jobtype_title;

                public ProjectJobtypeBean() {
                }

                protected ProjectJobtypeBean(Parcel in) {
                    this.jobtype_id = in.readInt();
                    this.jobtype_title = in.readString();
                }

                public int getJobtype_id() {
                    return jobtype_id;
                }

                public void setJobtype_id(int jobtype_id) {
                    this.jobtype_id = jobtype_id;
                }

                public String getJobtype_title() {
                    return jobtype_title;
                }

                public void setJobtype_title(String jobtype_title) {
                    this.jobtype_title = jobtype_title;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeInt(this.jobtype_id);
                    dest.writeString(this.jobtype_title);
                }
            }

            public static class ProjectExpertiseBean {
                /**
                 * expertise_id : 1
                 * expertise_title : Cleaning Services
                 */

                private int expertise_id;
                private String expertise_title;

                public int getExpertise_id() {
                    return expertise_id;
                }

                public void setExpertise_id(int expertise_id) {
                    this.expertise_id = expertise_id;
                }

                public String getExpertise_title() {
                    return expertise_title;
                }

                public void setExpertise_title(String expertise_title) {
                    this.expertise_title = expertise_title;
                }
            }
        }

        public static class UpcommingProjectBean implements Parcelable {
            public static final Creator<UpcommingProjectBean> CREATOR = new Creator<UpcommingProjectBean>() {
                @Override
                public UpcommingProjectBean createFromParcel(Parcel source) {
                    return new UpcommingProjectBean(source);
                }

                @Override
                public UpcommingProjectBean[] newArray(int size) {
                    return new UpcommingProjectBean[size];
                }
            };
            /**
             * project_id : 64
             * project_name : Test new project
             * property_name : testing propert
             * bids_count : 0
             * created : 17-07-2017
             * provider_name :
             * project_jobtype : {"jobtype_id":2,"jobtype_title":"Removal/Recycle"}
             * project_expertise : [{"expertise_id":1,"expertise_title":"Cleaning Services"},{"expertise_id":2,"expertise_title":"Flooring & Carpets"}]
             */

            private int project_id;
            private String project_name;
            private String property_name;
            private int bids_count;
            private String created;
            private String provider_name;
            private ProjectJobtypeBeanX project_jobtype;
            private List<ProjectExpertiseBeanX> project_expertise;

            public UpcommingProjectBean() {
            }

            protected UpcommingProjectBean(Parcel in) {
                this.project_id = in.readInt();
                this.project_name = in.readString();
                this.property_name = in.readString();
                this.bids_count = in.readInt();
                this.created = in.readString();
                this.provider_name = in.readString();
                this.project_jobtype = in.readParcelable(ProjectJobtypeBeanX.class.getClassLoader());
                this.project_expertise = new ArrayList<ProjectExpertiseBeanX>();
                in.readList(this.project_expertise, ProjectExpertiseBeanX.class.getClassLoader());
            }

            public int getProject_id() {
                return project_id;
            }

            public void setProject_id(int project_id) {
                this.project_id = project_id;
            }

            public String getProject_name() {
                return project_name;
            }

            public void setProject_name(String project_name) {
                this.project_name = project_name;
            }

            public String getProperty_name() {
                return property_name;
            }

            public void setProperty_name(String property_name) {
                this.property_name = property_name;
            }

            public int getBids_count() {
                return bids_count;
            }

            public void setBids_count(int bids_count) {
                this.bids_count = bids_count;
            }

            public String getCreated() {
                return created;
            }

            public void setCreated(String created) {
                this.created = created;
            }

            public String getProvider_name() {
                return provider_name;
            }

            public void setProvider_name(String provider_name) {
                this.provider_name = provider_name;
            }

            public ProjectJobtypeBeanX getProject_jobtype() {
                return project_jobtype;
            }

            public void setProject_jobtype(ProjectJobtypeBeanX project_jobtype) {
                this.project_jobtype = project_jobtype;
            }

            public List<ProjectExpertiseBeanX> getProject_expertise() {
                return project_expertise;
            }

            public void setProject_expertise(List<ProjectExpertiseBeanX> project_expertise) {
                this.project_expertise = project_expertise;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.project_id);
                dest.writeString(this.project_name);
                dest.writeString(this.property_name);
                dest.writeInt(this.bids_count);
                dest.writeString(this.created);
                dest.writeString(this.provider_name);
                dest.writeParcelable(this.project_jobtype, flags);
                dest.writeList(this.project_expertise);
            }

            public static class ProjectJobtypeBeanX implements Parcelable {
                public static final Creator<ProjectJobtypeBeanX> CREATOR = new Creator<ProjectJobtypeBeanX>() {
                    @Override
                    public ProjectJobtypeBeanX createFromParcel(Parcel source) {
                        return new ProjectJobtypeBeanX(source);
                    }

                    @Override
                    public ProjectJobtypeBeanX[] newArray(int size) {
                        return new ProjectJobtypeBeanX[size];
                    }
                };
                /**
                 * jobtype_id : 2
                 * jobtype_title : Removal/Recycle
                 */

                private int jobtype_id;
                private String jobtype_title;

                public ProjectJobtypeBeanX() {
                }

                protected ProjectJobtypeBeanX(Parcel in) {
                    this.jobtype_id = in.readInt();
                    this.jobtype_title = in.readString();
                }

                public int getJobtype_id() {
                    return jobtype_id;
                }

                public void setJobtype_id(int jobtype_id) {
                    this.jobtype_id = jobtype_id;
                }

                public String getJobtype_title() {
                    return jobtype_title;
                }

                public void setJobtype_title(String jobtype_title) {
                    this.jobtype_title = jobtype_title;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeInt(this.jobtype_id);
                    dest.writeString(this.jobtype_title);
                }
            }

            public static class ProjectExpertiseBeanX implements Parcelable {
                public static final Creator<ProjectExpertiseBeanX> CREATOR = new Creator<ProjectExpertiseBeanX>() {
                    @Override
                    public ProjectExpertiseBeanX createFromParcel(Parcel source) {
                        return new ProjectExpertiseBeanX(source);
                    }

                    @Override
                    public ProjectExpertiseBeanX[] newArray(int size) {
                        return new ProjectExpertiseBeanX[size];
                    }
                };
                /**
                 * expertise_id : 1
                 * expertise_title : Cleaning Services
                 */

                private int expertise_id;
                private String expertise_title;

                public ProjectExpertiseBeanX() {
                }

                protected ProjectExpertiseBeanX(Parcel in) {
                    this.expertise_id = in.readInt();
                    this.expertise_title = in.readString();
                }

                public int getExpertise_id() {
                    return expertise_id;
                }

                public void setExpertise_id(int expertise_id) {
                    this.expertise_id = expertise_id;
                }

                public String getExpertise_title() {
                    return expertise_title;
                }

                public void setExpertise_title(String expertise_title) {
                    this.expertise_title = expertise_title;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeInt(this.expertise_id);
                    dest.writeString(this.expertise_title);
                }
            }
        }

        public static class OngoingProjectBean implements Parcelable {
            public static final Creator<OngoingProjectBean> CREATOR = new Creator<OngoingProjectBean>() {
                @Override
                public OngoingProjectBean createFromParcel(Parcel source) {
                    return new OngoingProjectBean(source);
                }

                @Override
                public OngoingProjectBean[] newArray(int size) {
                    return new OngoingProjectBean[size];
                }
            };
            /**
             * project_id : 65
             * project_name : Test new projectw
             * property_name : testing propert
             * bids_count : 0
             * created : 17-07-2017
             * provider_name :
             * project_jobtype : {"jobtype_id":2,"jobtype_title":"Removal/Recycle"}
             * project_expertise : [{"expertise_id":1,"expertise_title":"Cleaning Services"},{"expertise_id":2,"expertise_title":"Flooring & Carpets"}]
             */

            private int project_id;
            private String project_name;
            private String property_name;
            private int bids_count;
            private String created;
            private String provider_name;
            private ProjectJobtypeBeanXX project_jobtype;
            private List<ProjectExpertiseBeanXX> project_expertise;

            public OngoingProjectBean() {
            }

            protected OngoingProjectBean(Parcel in) {
                this.project_id = in.readInt();
                this.project_name = in.readString();
                this.property_name = in.readString();
                this.bids_count = in.readInt();
                this.created = in.readString();
                this.provider_name = in.readString();
                this.project_jobtype = in.readParcelable(ProjectJobtypeBeanXX.class.getClassLoader());
                this.project_expertise = new ArrayList<ProjectExpertiseBeanXX>();
                in.readList(this.project_expertise, ProjectExpertiseBeanXX.class.getClassLoader());
            }

            public int getProject_id() {
                return project_id;
            }

            public void setProject_id(int project_id) {
                this.project_id = project_id;
            }

            public String getProject_name() {
                return project_name;
            }

            public void setProject_name(String project_name) {
                this.project_name = project_name;
            }

            public String getProperty_name() {
                return property_name;
            }

            public void setProperty_name(String property_name) {
                this.property_name = property_name;
            }

            public int getBids_count() {
                return bids_count;
            }

            public void setBids_count(int bids_count) {
                this.bids_count = bids_count;
            }

            public String getCreated() {
                return created;
            }

            public void setCreated(String created) {
                this.created = created;
            }

            public String getProvider_name() {
                return provider_name;
            }

            public void setProvider_name(String provider_name) {
                this.provider_name = provider_name;
            }

            public ProjectJobtypeBeanXX getProject_jobtype() {
                return project_jobtype;
            }

            public void setProject_jobtype(ProjectJobtypeBeanXX project_jobtype) {
                this.project_jobtype = project_jobtype;
            }

            public List<ProjectExpertiseBeanXX> getProject_expertise() {
                return project_expertise;
            }

            public void setProject_expertise(List<ProjectExpertiseBeanXX> project_expertise) {
                this.project_expertise = project_expertise;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.project_id);
                dest.writeString(this.project_name);
                dest.writeString(this.property_name);
                dest.writeInt(this.bids_count);
                dest.writeString(this.created);
                dest.writeString(this.provider_name);
                dest.writeParcelable(this.project_jobtype, flags);
                dest.writeList(this.project_expertise);
            }

            public static class ProjectJobtypeBeanXX implements Parcelable {
                public static final Creator<ProjectJobtypeBeanXX> CREATOR = new Creator<ProjectJobtypeBeanXX>() {
                    @Override
                    public ProjectJobtypeBeanXX createFromParcel(Parcel source) {
                        return new ProjectJobtypeBeanXX(source);
                    }

                    @Override
                    public ProjectJobtypeBeanXX[] newArray(int size) {
                        return new ProjectJobtypeBeanXX[size];
                    }
                };
                /**
                 * jobtype_id : 2
                 * jobtype_title : Removal/Recycle
                 */

                private int jobtype_id;
                private String jobtype_title;

                public ProjectJobtypeBeanXX() {
                }

                protected ProjectJobtypeBeanXX(Parcel in) {
                    this.jobtype_id = in.readInt();
                    this.jobtype_title = in.readString();
                }

                public int getJobtype_id() {
                    return jobtype_id;
                }

                public void setJobtype_id(int jobtype_id) {
                    this.jobtype_id = jobtype_id;
                }

                public String getJobtype_title() {
                    return jobtype_title;
                }

                public void setJobtype_title(String jobtype_title) {
                    this.jobtype_title = jobtype_title;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeInt(this.jobtype_id);
                    dest.writeString(this.jobtype_title);
                }
            }

            public static class ProjectExpertiseBeanXX implements Parcelable {
                public static final Creator<ProjectExpertiseBeanXX> CREATOR = new Creator<ProjectExpertiseBeanXX>() {
                    @Override
                    public ProjectExpertiseBeanXX createFromParcel(Parcel source) {
                        return new ProjectExpertiseBeanXX(source);
                    }

                    @Override
                    public ProjectExpertiseBeanXX[] newArray(int size) {
                        return new ProjectExpertiseBeanXX[size];
                    }
                };
                /**
                 * expertise_id : 1
                 * expertise_title : Cleaning Services
                 */

                private int expertise_id;
                private String expertise_title;

                public ProjectExpertiseBeanXX() {
                }

                protected ProjectExpertiseBeanXX(Parcel in) {
                    this.expertise_id = in.readInt();
                    this.expertise_title = in.readString();
                }

                public int getExpertise_id() {
                    return expertise_id;
                }

                public void setExpertise_id(int expertise_id) {
                    this.expertise_id = expertise_id;
                }

                public String getExpertise_title() {
                    return expertise_title;
                }

                public void setExpertise_title(String expertise_title) {
                    this.expertise_title = expertise_title;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeInt(this.expertise_id);
                    dest.writeString(this.expertise_title);
                }
            }
        }
    }
}
