package com.ohp.serviceproviderviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * Created by vishal.sharma on 8/23/2017.
 */

public class GetTeamMemberResponse extends GenericBean {


    /**
     * totalRecord : 16
     * data : [{"value":38,"title":""},{"value":36,"title":""},{"value":39,"title":"aaa b"},{"value":22,"title":"Ankush"},{"value":33,"title":"Bakers"},{"value":23,"title":"Developer Singh"},{"value":46,"title":"Rohit"},{"value":27,"title":"Rohit123"},{"value":29,"title":"Rohit1233"},{"value":28,"title":"Rohit1233"},{"value":30,"title":"Rohit420"},{"value":41,"title":"Test teat"},{"value":25,"title":"weffff"},{"value":47,"title":"Ysghs"},{"value":48,"title":"Yuui"},{"value":49,"title":"Yuui"}]
     */

    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * value : 38
         * title :
         */

        @SerializedName("value")
        private int value;
        @SerializedName("title")
        private String title;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
