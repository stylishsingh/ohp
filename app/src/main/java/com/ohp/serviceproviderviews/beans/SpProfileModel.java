package com.ohp.serviceproviderviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class SpProfileModel extends GenericBean {


    /**
     * data : {"access_token":"OHP_1501763632JixDYFcsUzhQCmAK","full_name":"Visghj","email":"ah@yopmail.com","gender":"male","address":"65, Mohali Pind Rd, Sector 55, Phase 1, Sector 55, Sahibzada Ajit Singh Nagar, Punjab 160055, India","city":"","state":"","is_verified":1,"is_social":2,"role":2,"status":1,"profile_image_100X100":"","profile_image_200X200":"","is_licensed":1,"license_no":"123456789","business_name":" icy cugigig","insured":0,"address_official":"","website":"http://www.yoyo.com","bonded":0,"latitude":"30.726227500000018","longitude":"76.71423046874995","expertise":[{"expertise_id":"1","expertise_title":"Cleaning Services"}]}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * access_token : OHP_1501763632JixDYFcsUzhQCmAK
         * full_name : Visghj
         * email : ah@yopmail.com
         * gender : male
         * address : 65, Mohali Pind Rd, Sector 55, Phase 1, Sector 55, Sahibzada Ajit Singh Nagar, Punjab 160055, India
         * city :
         * state :
         * is_verified : 1
         * is_social : 2
         * role : 2
         * status : 1
         * profile_image_100X100 :
         * profile_image_200X200 :
         * is_licensed : 1
         * license_no : 123456789
         * business_name :  icy cugigig
         * insured : 0
         * address_official :
         * website : http://www.yoyo.com
         * bonded : 0
         * latitude : 30.726227500000018
         * longitude : 76.71423046874995
         * expertise : [{"expertise_id":"1","expertise_title":"Cleaning Services"}]
         */

        @SerializedName("access_token")
        private String accessToken;
        @SerializedName("full_name")
        private String fullName;
        @SerializedName("email")
        private String email;
        @SerializedName("gender")
        private String gender;
        @SerializedName("address")
        private String address;
        @SerializedName("city")
        private String city;
        @SerializedName("state")
        private String state;
        @SerializedName("is_verified")
        private int isVerified;
        @SerializedName("is_social")
        private int isSocial;
        @SerializedName("role")
        private int role;
        @SerializedName("status")
        private int statusX;
        @SerializedName("profile_image_100X100")
        private String profileImage100X100;
        @SerializedName("profile_image_200X200")
        private String profileImage200X200;
        @SerializedName("is_licensed")
        private int isLicensed;
        @SerializedName("license_no")
        private String licenseNo;
        @SerializedName("business_name")
        private String businessName;
        @SerializedName("insured")
        private int insured;
        @SerializedName("address_official")
        private String addressOfficial;
        @SerializedName("website")
        private String website;
        @SerializedName("bonded")
        private int bonded;
        @SerializedName("latitude")
        private String latitude;
        @SerializedName("longitude")
        private String longitude;
        @SerializedName("expertise")
        private List<ExpertiseBean> expertise;

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public int getIsVerified() {
            return isVerified;
        }

        public void setIsVerified(int isVerified) {
            this.isVerified = isVerified;
        }

        public int getIsSocial() {
            return isSocial;
        }

        public void setIsSocial(int isSocial) {
            this.isSocial = isSocial;
        }

        public int getRole() {
            return role;
        }

        public void setRole(int role) {
            this.role = role;
        }

        public int getStatusX() {
            return statusX;
        }

        public void setStatusX(int statusX) {
            this.statusX = statusX;
        }

        public String getProfileImage100X100() {
            return profileImage100X100;
        }

        public void setProfileImage100X100(String profileImage100X100) {
            this.profileImage100X100 = profileImage100X100;
        }

        public String getProfileImage200X200() {
            return profileImage200X200;
        }

        public void setProfileImage200X200(String profileImage200X200) {
            this.profileImage200X200 = profileImage200X200;
        }

        public int getIsLicensed() {
            return isLicensed;
        }

        public void setIsLicensed(int isLicensed) {
            this.isLicensed = isLicensed;
        }

        public String getLicenseNo() {
            return licenseNo;
        }

        public void setLicenseNo(String licenseNo) {
            this.licenseNo = licenseNo;
        }

        public String getBusinessName() {
            return businessName;
        }

        public void setBusinessName(String businessName) {
            this.businessName = businessName;
        }

        public int getInsured() {
            return insured;
        }

        public void setInsured(int insured) {
            this.insured = insured;
        }

        public String getAddressOfficial() {
            return addressOfficial;
        }

        public void setAddressOfficial(String addressOfficial) {
            this.addressOfficial = addressOfficial;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public int getBonded() {
            return bonded;
        }

        public void setBonded(int bonded) {
            this.bonded = bonded;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public List<ExpertiseBean> getExpertise() {
            return expertise;
        }

        public void setExpertise(List<ExpertiseBean> expertise) {
            this.expertise = expertise;
        }

        public static class ExpertiseBean {
            /**
             * expertise_id : 1
             * expertise_title : Cleaning Services
             */

            @SerializedName("expertise_id")
            private String expertiseId;
            @SerializedName("expertise_title")
            private String expertiseTitle;

            public String getExpertiseId() {
                return expertiseId;
            }

            public void setExpertiseId(String expertiseId) {
                this.expertiseId = expertiseId;
            }

            public String getExpertiseTitle() {
                return expertiseTitle;
            }

            public void setExpertiseTitle(String expertiseTitle) {
                this.expertiseTitle = expertiseTitle;
            }
        }
    }
}
