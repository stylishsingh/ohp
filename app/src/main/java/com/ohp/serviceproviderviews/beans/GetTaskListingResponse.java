package com.ohp.serviceproviderviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * @author Vishal Sharma.
 */

public class GetTaskListingResponse extends GenericBean {


    /**
     * totalRecord : 22
     * data : [{"task_id":35,"task_name":"Xd d","task_status":1,"task_notes":"","created":"2017-08-29T12:36:18+00:00","property":{"proeprty_id":0,"property_name":"Default Property","property_owner_name":"service"},"task_images":[],"member":{"member_id":23,"member_name":"Developer Singh"},"project":{"project_id":69,"project_name":"Mandeep"}},{"task_id":32,"task_name":"Mandeep4","task_status":1,"task_notes":"NotessaS","created":"2017-08-29T10:34:57+00:00","property":{"proeprty_id":0,"property_name":"Sukhdev prop","property_owner_name":"service"},"task_images":[],"member":{"member_id":33,"member_name":"Bakers"},"project":{"project_id":91,"project_name":"Sukhdev proj"}},{"task_id":31,"task_name":"Mandeep 3","task_status":1,"task_notes":"","created":"2017-08-29T10:28:38+00:00","property":{"proeprty_id":0,"property_name":"","property_owner_name":"service"},"task_images":[],"member":{"member_id":46,"member_name":"Rohit"},"project":{"project_id":91,"project_name":"Sukhdev proj"}},{"task_id":29,"task_name":"Mandeep 2","task_status":1,"task_notes":"","created":"2017-08-29T10:26:43+00:00","property":{"proeprty_id":0,"property_name":"","property_owner_name":"service"},"task_images":[],"member":{"member_id":33,"member_name":"Bakers"},"project":{"project_id":91,"project_name":"Sukhdev proj"}},{"task_id":28,"task_name":"Mandeep with proj","task_status":1,"task_notes":"","created":"2017-08-29T10:14:16+00:00","property":{"proeprty_id":0,"property_name":"","property_owner_name":"service"},"task_images":[],"member":{"member_id":23,"member_name":"Developer Singh"},"project":{"project_id":91,"project_name":"Sukhdev proj"}},{"task_id":27,"task_name":"Testing task","task_status":1,"task_notes":"Testing notes 2","created":"2017-08-29T07:59:25+00:00","property":{"proeprty_id":0,"property_name":"new 456","property_owner_name":"service"},"task_images":[{"image_id":5,"image":"http://onehomeportal.mobilytedev.com/img/document_picture/original_image/EYaxHR2nd82yTNa.jpg"}],"member":{"member_id":23,"member_name":"Developer Singh"},"project":{"project_id":93,"project_name":"Nulla"}},{"task_id":26,"task_name":"Cycy","task_status":1,"task_notes":"Ufft","created":"2017-08-29T07:02:54+00:00","property":{"proeprty_id":0,"property_name":"Default Property","property_owner_name":"service"},"task_images":[{"image_id":4,"image":"http://onehomeportal.mobilytedev.com/img/document_picture/original_image/FFvAt4H98xfbRuH.jpg"}],"member":{"member_id":23,"member_name":"Developer Singh"},"project":{"project_id":79,"project_name":"Mandeepss"}},{"task_id":25,"task_name":"Gcyc","task_status":1,"task_notes":"Tdxt","created":"2017-08-29T06:58:54+00:00","property":{"proeprty_id":0,"property_name":"Default Property","property_owner_name":"service"},"task_images":[],"member":{"member_id":29,"member_name":"Rohit1233"},"project":{"project_id":76,"project_name":"Mandeepss"}},{"task_id":24,"task_name":"Test appliance NAME2","task_status":1,"task_notes":"Test property address12","created":"2017-08-29T06:47:49+00:00","property":{"proeprty_id":0,"property_name":"Test property name","property_owner_name":"service"},"task_images":[],"member":{"member_id":10,"member_name":"lambu kumar"},"project":{"project_id":81,"project_name":"Mandeepss"}},{"task_id":23,"task_name":"Vjch","task_status":1,"task_notes":"Chch ","created":"2017-08-29T06:46:57+00:00","property":{"proeprty_id":0,"property_name":"Default Property Ann","property_owner_name":""},"task_images":[],"member":{"member_id":39,"member_name":"Newspapers"},"project":{"project_id":0,"project_name":""}}]
     */

    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * task_id : 35
         * task_name : Xd d
         * task_status : 1
         * task_notes :
         * created : 2017-08-29T12:36:18+00:00
         * property : {"proeprty_id":0,"property_name":"Default Property","property_owner_name":"service"}
         * task_images : []
         * member : {"member_id":23,"member_name":"Developer Singh"}
         * project : {"project_id":69,"project_name":"Mandeep"}
         */

        @SerializedName("task_id")
        private int taskId;
        @SerializedName("task_name")
        private String taskName;
        @SerializedName("task_status")
        private int taskStatus;
        @SerializedName("task_notes")
        private String taskNotes;
        @SerializedName("created")
        private String created;
        @SerializedName("task_repeat")
        private int taskRepeat;
        @SerializedName("task_type")
        private int taskType;
        @SerializedName("property")
        private PropertyBean property;
        @SerializedName("member")
        private MemberBean member;
        @SerializedName("project")
        private ProjectBean project;
        @SerializedName("task_images")
        private List<?> taskImages;

        public int getTaskType() {
            return taskType;
        }

        public void setTaskType(int taskType) {
            this.taskType = taskType;
        }

        public int getTaskRepeat() {
            return taskRepeat;
        }

        public void setTaskRepeat(int taskRepeat) {
            this.taskRepeat = taskRepeat;
        }

        public int getTaskId() {
            return taskId;
        }

        public void setTaskId(int taskId) {
            this.taskId = taskId;
        }

        public String getTaskName() {
            return taskName;
        }

        public void setTaskName(String taskName) {
            this.taskName = taskName;
        }

        public int getTaskStatus() {
            return taskStatus;
        }

        public void setTaskStatus(int taskStatus) {
            this.taskStatus = taskStatus;
        }

        public String getTaskNotes() {
            return taskNotes;
        }

        public void setTaskNotes(String taskNotes) {
            this.taskNotes = taskNotes;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public PropertyBean getProperty() {
            return property;
        }

        public void setProperty(PropertyBean property) {
            this.property = property;
        }

        public MemberBean getMember() {
            return member;
        }

        public void setMember(MemberBean member) {
            this.member = member;
        }

        public ProjectBean getProject() {
            return project;
        }

        public void setProject(ProjectBean project) {
            this.project = project;
        }

        public List<?> getTaskImages() {
            return taskImages;
        }

        public void setTaskImages(List<?> taskImages) {
            this.taskImages = taskImages;
        }

        public static class PropertyBean {
            /**
             * proeprty_id : 0
             * property_name : Default Property
             * property_owner_name : service
             */

            @SerializedName("proeprty_id")
            private int proeprtyId;
            @SerializedName("property_name")
            private String propertyName;
            @SerializedName("property_owner_name")
            private String propertyOwnerName;

            public int getProeprtyId() {
                return proeprtyId;
            }

            public void setProeprtyId(int proeprtyId) {
                this.proeprtyId = proeprtyId;
            }

            public String getPropertyName() {
                return propertyName;
            }

            public void setPropertyName(String propertyName) {
                this.propertyName = propertyName;
            }

            public String getPropertyOwnerName() {
                return propertyOwnerName;
            }

            public void setPropertyOwnerName(String propertyOwnerName) {
                this.propertyOwnerName = propertyOwnerName;
            }
        }

        public static class MemberBean {
            /**
             * member_id : 23
             * member_name : Developer Singh
             */

            @SerializedName("member_id")
            private int memberId;
            @SerializedName("member_name")
            private String memberName;

            public int getMemberId() {
                return memberId;
            }

            public void setMemberId(int memberId) {
                this.memberId = memberId;
            }

            public String getMemberName() {
                return memberName;
            }

            public void setMemberName(String memberName) {
                this.memberName = memberName;
            }
        }

        public static class ProjectBean {
            /**
             * project_id : 69
             * project_name : Mandeep
             */

            @SerializedName("project_id")
            private int projectId;
            @SerializedName("project_name")
            private String projectName;

            public int getProjectId() {
                return projectId;
            }

            public void setProjectId(int projectId) {
                this.projectId = projectId;
            }

            public String getProjectName() {
                return projectName;
            }

            public void setProjectName(String projectName) {
                this.projectName = projectName;
            }
        }
    }
}
