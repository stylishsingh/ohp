package com.ohp.serviceproviderviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * Created by vishal.sharma on 8/2/2017.
 */

public class MyBidsResponse extends GenericBean{

    /**
     * totalRecord : 2
     * data : [{"bid_id":29,"bid_amount":2000,"bid_note":"","bid_created":"2017-08-04T20:07:25+00:00","project_id":244,"project_name":"no least ","project_owner":"Sukhdev","project_expertise":[{"expertise_id":"1","expertise_title":"Cleaning Services"},{"expertise_id":"2","expertise_title":"Flooring & Carpets"}],"project_jobtype":{"jobtype_id":"2","jobtype_title":"Removal/Recycle"},"property_id":214,"property_name":"arjun di khotty"},{"bid_id":28,"bid_amount":2000,"bid_note":"ccdzhjdashdasgjkd","bid_created":"2017-08-04T20:02:26+00:00","project_id":233,"project_name":"1111","project_owner":"Mandeepft","project_expertise":[{"expertise_id":"1","expertise_title":"Cleaning Services"}],"project_jobtype":{"jobtype_id":"1","jobtype_title":"Repair"},"property_id":175,"property_name":"test"}]
     */

    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * bid_id : 29
         * bid_amount : 2000
         * bid_note :
         * bid_created : 2017-08-04T20:07:25+00:00
         * project_id : 244
         * project_name : no least
         * project_owner : Sukhdev
         * project_expertise : [{"expertise_id":"1","expertise_title":"Cleaning Services"},{"expertise_id":"2","expertise_title":"Flooring & Carpets"}]
         * project_jobtype : {"jobtype_id":"2","jobtype_title":"Removal/Recycle"}
         * property_id : 214
         * property_name : arjun di khotty
         */

        @SerializedName("bid_id")
        private int bidId;
        @SerializedName("bid_status")
        private int bidStatus;
        @SerializedName("bid_amount")
        private int bidAmount;
        @SerializedName("bid_note")
        private String bidNote;
        @SerializedName("bid_created")
        private String bidCreated;
        @SerializedName("project_id")
        private int projectId;
        @SerializedName("project_name")
        private String projectName;
        @SerializedName("project_owner")
        private String projectOwner;
        @SerializedName("project_jobtype")
        private ProjectJobtypeBean projectJobtype;
        @SerializedName("property_id")
        private int propertyId;
        @SerializedName("property_name")
        private String propertyName;
        @SerializedName("project_expertise")
        private List<ProjectExpertiseBean> projectExpertise;

        public int getBidId() {
            return bidId;
        }

        public void setBidId(int bidId) {
            this.bidId = bidId;
        }

        public int getBidStatus() {
            return bidStatus;
        }

        public void setBidStatus(int bidStatus) {
            this.bidStatus = bidStatus;
        }

        public int getBidAmount() {
            return bidAmount;
        }

        public void setBidAmount(int bidAmount) {
            this.bidAmount = bidAmount;
        }

        public String getBidNote() {
            return bidNote;
        }

        public void setBidNote(String bidNote) {
            this.bidNote = bidNote;
        }

        public String getBidCreated() {
            return bidCreated;
        }

        public void setBidCreated(String bidCreated) {
            this.bidCreated = bidCreated;
        }

        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getProjectOwner() {
            return projectOwner;
        }

        public void setProjectOwner(String projectOwner) {
            this.projectOwner = projectOwner;
        }

        public ProjectJobtypeBean getProjectJobtype() {
            return projectJobtype;
        }

        public void setProjectJobtype(ProjectJobtypeBean projectJobtype) {
            this.projectJobtype = projectJobtype;
        }

        public int getPropertyId() {
            return propertyId;
        }

        public void setPropertyId(int propertyId) {
            this.propertyId = propertyId;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public List<ProjectExpertiseBean> getProjectExpertise() {
            return projectExpertise;
        }

        public void setProjectExpertise(List<ProjectExpertiseBean> projectExpertise) {
            this.projectExpertise = projectExpertise;
        }

        public static class ProjectJobtypeBean {
            /**
             * jobtype_id : 2
             * jobtype_title : Removal/Recycle
             */

            @SerializedName("jobtype_id")
            private String jobtypeId;
            @SerializedName("jobtype_title")
            private String jobtypeTitle;

            public String getJobtypeId() {
                return jobtypeId;
            }

            public void setJobtypeId(String jobtypeId) {
                this.jobtypeId = jobtypeId;
            }

            public String getJobtypeTitle() {
                return jobtypeTitle;
            }

            public void setJobtypeTitle(String jobtypeTitle) {
                this.jobtypeTitle = jobtypeTitle;
            }
        }

        public static class ProjectExpertiseBean {
            /**
             * expertise_id : 1
             * expertise_title : Cleaning Services
             */

            @SerializedName("expertise_id")
            private String expertiseId;
            @SerializedName("expertise_title")
            private String expertiseTitle;

            public String getExpertiseId() {
                return expertiseId;
            }

            public void setExpertiseId(String expertiseId) {
                this.expertiseId = expertiseId;
            }

            public String getExpertiseTitle() {
                return expertiseTitle;
            }

            public void setExpertiseTitle(String expertiseTitle) {
                this.expertiseTitle = expertiseTitle;
            }
        }
    }
}
