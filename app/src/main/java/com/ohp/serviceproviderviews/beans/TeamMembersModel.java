package com.ohp.serviceproviderviews.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class TeamMembersModel extends GenericBean {

    @SerializedName("totalRecord")
    private int totalRecord;

    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * user_id : 16
         * name : Rohit
         * type_status : 0
         * email : rohit.kumar2@mobilyte.com
         * address : mohali e-40
         * role : 2
         * profile_image_100X100 :
         * profile_image_200X200 :
         */

        @SerializedName("user_id")
        private int userId;
        @SerializedName("name")
        private String name;
        @SerializedName("type_status")
        private int typeStatus;
        @SerializedName("email")
        private String email;
        @SerializedName("latitude")
        private String latitude;
        @SerializedName("longitude")
        private String longitude;
        @SerializedName("address")
        private String address;
        @SerializedName("role")
        private int role;
        @SerializedName("profile_image_100X100")
        private String profileImage100X100;
        @SerializedName("profile_image_200X200")
        private String profileImage200X200;
        @SerializedName("expertise")
        private List<ExpertiseBean> expertise;

        public DataBean() {
        }

        public List<ExpertiseBean> getExpertise() {
            return expertise;
        }

        public void setExpertise(List<ExpertiseBean> expertise) {
            this.expertise = expertise;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getTypeStatus() {
            return typeStatus;
        }

        public void setTypeStatus(int typeStatus) {
            this.typeStatus = typeStatus;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public int getRole() {
            return role;
        }

        public void setRole(int role) {
            this.role = role;
        }

        public String getProfileImage100X100() {
            return profileImage100X100;
        }

        public void setProfileImage100X100(String profileImage100X100) {
            this.profileImage100X100 = profileImage100X100;
        }

        public String getProfileImage200X200() {
            return profileImage200X200;
        }

        public void setProfileImage200X200(String profileImage200X200) {
            this.profileImage200X200 = profileImage200X200;
        }

        public static class ExpertiseBean implements Parcelable {
            public static final Creator<ExpertiseBean> CREATOR = new Creator<ExpertiseBean>() {
                @Override
                public ExpertiseBean createFromParcel(Parcel source) {
                    return new ExpertiseBean(source);
                }

                @Override
                public ExpertiseBean[] newArray(int size) {
                    return new ExpertiseBean[size];
                }
            };
            /**
             * expertise_id : 1
             * expertise_title : Cleaning Services
             */

            @SerializedName("expertise_id")
            private int expertiseId;
            @SerializedName("expertise_title")
            private String expertiseTitle;

            public ExpertiseBean() {
            }

            protected ExpertiseBean(Parcel in) {
                this.expertiseId = in.readInt();
                this.expertiseTitle = in.readString();
            }

            public int getExpertiseId() {
                return expertiseId;
            }

            public void setExpertiseId(int expertiseId) {
                this.expertiseId = expertiseId;
            }

            public String getExpertiseTitle() {
                return expertiseTitle;
            }

            public void setExpertiseTitle(String expertiseTitle) {
                this.expertiseTitle = expertiseTitle;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.expertiseId);
                dest.writeString(this.expertiseTitle);
            }
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.userId);
            dest.writeString(this.name);
            dest.writeInt(this.typeStatus);
            dest.writeString(this.email);
            dest.writeString(this.latitude);
            dest.writeString(this.longitude);
            dest.writeString(this.address);
            dest.writeInt(this.role);
            dest.writeString(this.profileImage100X100);
            dest.writeString(this.profileImage200X200);
            dest.writeTypedList(this.expertise);
        }

        protected DataBean(Parcel in) {
            this.userId = in.readInt();
            this.name = in.readString();
            this.typeStatus = in.readInt();
            this.email = in.readString();
            this.latitude = in.readString();
            this.longitude = in.readString();
            this.address = in.readString();
            this.role = in.readInt();
            this.profileImage100X100 = in.readString();
            this.profileImage200X200 = in.readString();
            this.expertise = in.createTypedArrayList(ExpertiseBean.CREATOR);
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }
}
