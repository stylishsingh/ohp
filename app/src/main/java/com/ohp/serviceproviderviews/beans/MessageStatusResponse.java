package com.ohp.serviceproviderviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

/**
 * Created by vishal.sharma on 9/8/2017.
 */

public class MessageStatusResponse extends GenericBean {


    /**
     * data : {"chat_id":49,"send_by":7,"send_by_name":"Op","send_to":6,"send_to_name":"Poo","message":"sam okoko","created":"2017-09-09T06:59:33+00:00"}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * chat_id : 49
         * send_by : 7
         * send_by_name : Op
         * send_to : 6
         * send_to_name : Poo
         * message : sam okoko
         * created : 2017-09-09T06:59:33+00:00
         */

        @SerializedName("chat_id")
        private int chatId;
        @SerializedName("send_by")
        private int sendBy;
        @SerializedName("send_by_name")
        private String sendByName;
        @SerializedName("send_to")
        private int sendTo;
        @SerializedName("send_to_name")
        private String sendToName;
        @SerializedName("message")
        private String messageX;
        @SerializedName("created")
        private String created;

        public int getChatId() {
            return chatId;
        }

        public void setChatId(int chatId) {
            this.chatId = chatId;
        }

        public int getSendBy() {
            return sendBy;
        }

        public void setSendBy(int sendBy) {
            this.sendBy = sendBy;
        }

        public String getSendByName() {
            return sendByName;
        }

        public void setSendByName(String sendByName) {
            this.sendByName = sendByName;
        }

        public int getSendTo() {
            return sendTo;
        }

        public void setSendTo(int sendTo) {
            this.sendTo = sendTo;
        }

        public String getSendToName() {
            return sendToName;
        }

        public void setSendToName(String sendToName) {
            this.sendToName = sendToName;
        }

        public String getMessageX() {
            return messageX;
        }

        public void setMessageX(String messageX) {
            this.messageX = messageX;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }
    }
}
