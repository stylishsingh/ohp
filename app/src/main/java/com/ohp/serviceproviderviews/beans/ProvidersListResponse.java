package com.ohp.serviceproviderviews.beans;

import com.google.gson.annotations.SerializedName;
import com.ohp.commonviews.beans.GenericBean;

import java.util.List;

/**
 * Created by vishal.sharma on 8/16/2017.
 */

public class ProvidersListResponse extends GenericBean {


    /**
     * totalRecord : 3
     * data : [{"user_id":14,"full_name":"Vishal Kumar","address":"mohali e-40","email":"vishal.kumar11@mobilyte.com","expertise":[{"expertise_id":2,"expertise_title":"Flooring & Carpets"},{"expertise_id":3,"expertise_title":"Garden/Landscaping"}]},{"user_id":13,"full_name":"Vishal Kumar","address":"mohali e-40","email":"vishal.kumar1@mobilyte.com","expertise":[{"expertise_id":2,"expertise_title":"Flooring & Carpets"},{"expertise_id":3,"expertise_title":"Garden/Landscaping"}]},{"user_id":11,"full_name":"Vishal Kumar","address":"mohali e-40","email":"vishal.kumar@mobilyte.com","expertise":[{"expertise_id":2,"expertise_title":"Flooring & Carpets"},{"expertise_id":3,"expertise_title":"Garden/Landscaping"}]}]
     */

    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * user_id : 14
         * full_name : Vishal Kumar
         * address : mohali e-40
         * email : vishal.kumar11@mobilyte.com
         * expertise : [{"expertise_id":2,"expertise_title":"Flooring & Carpets"},{"expertise_id":3,"expertise_title":"Garden/Landscaping"}]
         */

        @SerializedName("user_id")
        private int userId;
        @SerializedName("full_name")
        private String fullName;
        @SerializedName("address")
        private String address;
        @SerializedName("email")
        private String email;
        @SerializedName("latitude")
        private String latitude;
        @SerializedName("longitude")
        private String longitude;
        @SerializedName("expertise")
        private List<ExpertiseBean> expertise;

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }


        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public List<ExpertiseBean> getExpertise() {
            return expertise;
        }

        public void setExpertise(List<ExpertiseBean> expertise) {
            this.expertise = expertise;
        }

        public static class ExpertiseBean {
            /**
             * expertise_id : 2
             * expertise_title : Flooring & Carpets
             */

            @SerializedName("expertise_id")
            private int expertiseId;
            @SerializedName("expertise_title")
            private String expertiseTitle;

            public int getExpertiseId() {
                return expertiseId;
            }

            public void setExpertiseId(int expertiseId) {
                this.expertiseId = expertiseId;
            }

            public String getExpertiseTitle() {
                return expertiseTitle;
            }

            public void setExpertiseTitle(String expertiseTitle) {
                this.expertiseTitle = expertiseTitle;
            }
        }
    }
}
