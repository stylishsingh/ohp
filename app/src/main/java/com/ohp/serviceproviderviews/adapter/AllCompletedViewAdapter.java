package com.ohp.serviceproviderviews.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.serviceproviderviews.beans.GetAllProjectsServiceProviderResponse;
import com.ohp.serviceproviderviews.presenter.AllCompletedViewProjectsPresenter;
import com.ohp.utils.Constants;

import java.util.List;

/**
 * Created by vishal.sharma on 8/3/2017.
 */

public class AllCompletedViewAdapter extends RecyclerView.Adapter<AllCompletedViewAdapter.Viewholder> implements SwipeAdapterInterface {
    public SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);
    private View view;
    private SwipeLayout item;
    private List<GetAllProjectsServiceProviderResponse.DataBean.CompletedProjectsBean> allWonProjectsResponses;
    private AllCompletedViewProjectsPresenter allWonViewProjectPresenter;
    private Context context;


    public AllCompletedViewAdapter(Context context, List<GetAllProjectsServiceProviderResponse.DataBean.CompletedProjectsBean> allWonProjectsResponses, AllCompletedViewProjectsPresenter allWonViewProjectPresenter) {
        this.context = context;
        this.allWonProjectsResponses = allWonProjectsResponses;
        this.allWonViewProjectPresenter = allWonViewProjectPresenter;

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_service_project_detail_screen, viewGroup, false);
        item = (SwipeLayout) view.findViewById(R.id.swipe_item);
        item.setShowMode(SwipeLayout.ShowMode.PullOut);
        //  item.addDrag(SwipeLayout.DragEdge.Left, item.findViewById(R.id.bottom_wrapper));
        item.addDrag(SwipeLayout.DragEdge.Right, item.findViewById(R.id.bottom_wrapper_2));
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(Viewholder viewHolder, int position) {
        viewHolder.txt_Project_name.setText(allWonProjectsResponses.get(position).getProjectName());
        viewHolder.txt_Property_name.setText(allWonProjectsResponses.get(position).getPropertyName());
        viewHolder.txt_propertyOwnerName.setText(allWonProjectsResponses.get(position).getPropertyOwner()); // not  given in API property owner name
        if (allWonProjectsResponses.get(position).getProjectExpertise().size() > 0) {
            StringBuilder expertise = new StringBuilder();
            for (int i = 0; i < allWonProjectsResponses.get(position).getProjectExpertise().size(); i++) {

                if (i < (allWonProjectsResponses.get(position).getProjectExpertise().size() - 1)) {
                    expertise.append(allWonProjectsResponses.get(position).getProjectExpertise().get(i).getExpertiseTitle() + ", ");
                } else {
                    expertise.append(allWonProjectsResponses.get(position).getProjectExpertise().get(i).getExpertiseTitle());
                }
            }
            viewHolder.txt_expertise.setText(expertise);
        }
        viewHolder.txt_JobType_name.setText(allWonProjectsResponses.get(position).getProjectJobtype().getJobtypeTitle());
        viewHolder.txt_created_time.setText(allWonProjectsResponses.get(position).getCreated());
        mItemManger.bind(viewHolder.itemView, position);
    }

    public void updateList(List<GetAllProjectsServiceProviderResponse.DataBean.CompletedProjectsBean> listallWonProjectsResponses){
        this.allWonProjectsResponses=listallWonProjectsResponses;
        notifyDataSetChanged();
    }

    public void cancelAll(){
        this.allWonProjectsResponses=null;
        notifyDataSetChanged();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_item;
    }

    @Override
    public void notifyDTChanged() {

    }
    @Override
    public int getItemCount() {
        return allWonProjectsResponses==null ? 0 :allWonProjectsResponses.size();
    }
    public class Viewholder extends RecyclerView.ViewHolder{

        private TextView txt_Project_name, txt_Property_name, txt_JobType_name, txt_created_time, txt_expertise, txt_propertyOwnerName;
        private ImageView ivEdit, ivDelete;
        private CardView cardView;


        public Viewholder(View itemView) {
            super(itemView);
            ivEdit = (ImageView) view.findViewById(R.id.iv_edit);
            ivDelete = (ImageView) view.findViewById(R.id.iv_delete);
            txt_Project_name = (TextView) itemView.findViewById(R.id.txt_project_name);
            txt_Property_name = (TextView) itemView.findViewById(R.id.txt_property_detail);
            txt_created_time = (TextView) itemView.findViewById(R.id.txt_created_date_detail);
            txt_JobType_name = (TextView) itemView.findViewById(R.id.txt_Job_Type_detail);
            txt_expertise = (TextView) itemView.findViewById(R.id.txt_expertise_detail);
            txt_propertyOwnerName = (TextView) itemView.findViewById(R.id.txt_property_detail);
            cardView = (CardView) itemView.findViewById(R.id.parent_cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.PROJECT_DETAIL_WON_PROJECTS);
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(allWonProjectsResponses.get(getAdapterPosition()).getProjectId()));
                    // intent.putExtra(Constants.PROJECT_ID, "187");
                    context.startActivity(intent);
                }
            });

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemManger.closeAllItems();
                }
            });

            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemManger.closeAllItems();
                }
            });

        }
    }
}
