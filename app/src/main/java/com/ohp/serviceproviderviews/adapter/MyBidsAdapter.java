package com.ohp.serviceproviderviews.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.serviceproviderviews.beans.MyBidsResponse;
import com.ohp.serviceproviderviews.presenter.MyBidsFragmentPresenterImpl;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.List;

/**
 * @author Vishal Sharma.
 */

public class MyBidsAdapter extends RecyclerView.Adapter<MyBidsAdapter.ViewHolder> implements SwipeAdapterInterface {
    public SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);
    private Context context;
    private List<MyBidsResponse.DataBean> listItems;
    private MyBidsFragmentPresenterImpl myBidsFragmentPresenterImpl;

    public MyBidsAdapter(Context activity, List<MyBidsResponse.DataBean> listItems, MyBidsFragmentPresenterImpl myBidsFragmentPresenterImpl) {
        this.context = activity;
        this.listItems = listItems;
        this.myBidsFragmentPresenterImpl = myBidsFragmentPresenterImpl;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_mybid_detail, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        try {
            viewHolder.txt_project_name.setText(listItems.get(position).getProjectName());
            viewHolder.txt_Project_owner_name.setText(listItems.get(position).getProjectOwner());
            if (listItems.get(position).getProjectExpertise().size() > 0) {
                StringBuilder expertise = new StringBuilder();
                for (int i = 0; i < listItems.get(position).getProjectExpertise().size(); i++) {

                    if (i < (listItems.get(position).getProjectExpertise().size() - 1)) {
                        expertise.append(listItems.get(position).getProjectExpertise().get(i).getExpertiseTitle()).append(", ");
                    } else {
                        expertise.append(listItems.get(position).getProjectExpertise().get(i).getExpertiseTitle());
                    }
                }
                viewHolder.txt_expertise.setText(expertise);
            }
            viewHolder.txt_JobType_name.setText(listItems.get(position).getProjectJobtype().getJobtypeTitle());
            //viewHolder.txt_created_time.setText(listItems.get(position).getBidCreated());
            String date = Utils.getInstance().getYMDProjectFormattedDateTime(listItems.get(position).getBidCreated()).replace("+00:00", "");
            viewHolder.txt_created_time.setText(date);
            viewHolder.txt_bid_amount.setText(String.valueOf(listItems.get(position).getBidAmount()) + " (USD)");

            switch (listItems.get(position).getBidStatus()) {
                case 0:// new bid
                    viewHolder.item.setLeftSwipeEnabled(true);
                    viewHolder.item.setRightSwipeEnabled(true);
                    viewHolder.statusView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                    break;
                case 1:// accept bid
                    viewHolder.item.setLeftSwipeEnabled(false);
                    viewHolder.item.setRightSwipeEnabled(false);
                    viewHolder.statusView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.holo_green_dark));
                    break;
                case 2:// reject bid
                    viewHolder.item.setLeftSwipeEnabled(false);
                    viewHolder.item.setRightSwipeEnabled(false);
                    viewHolder.statusView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
                    break;
            }


            mItemManger.bind(viewHolder.itemView, position);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateList(List<MyBidsResponse.DataBean> listResponse) {
        this.listItems = listResponse;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listItems == null ? 0 : listItems.size();
    }

    public void clearAll() {
        listItems = null;
        notifyDataSetChanged();

    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_item;
    }

    @Override
    public void notifyDTChanged() {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_Project_owner_name, txt_JobType_name, txt_created_time, txt_expertise, txt_bid_amount, txt_project_name;
        private CardView cardView;
        private ImageView iv_edit, iv_delete;
        private View statusView;
        private SwipeLayout item;

        public ViewHolder(View itemView) {
            super(itemView);
            item = (SwipeLayout) itemView.findViewById(R.id.swipe_item);
            item.setShowMode(SwipeLayout.ShowMode.PullOut);
            item.addDrag(SwipeLayout.DragEdge.Left, item.findViewById(R.id.bottom_wrapper));
            item.addDrag(SwipeLayout.DragEdge.Right, item.findViewById(R.id.bottom_wrapper_2));
            txt_project_name = (TextView) itemView.findViewById(R.id.txt_project_name);
            txt_Project_owner_name = (TextView) itemView.findViewById(R.id.txt_owner_name);
            txt_expertise = (TextView) itemView.findViewById(R.id.txt_expertise_detail);
            txt_JobType_name = (TextView) itemView.findViewById(R.id.txt_Job_Type_detail);
            txt_bid_amount = (TextView) itemView.findViewById(R.id.txt_bid_amount);
            txt_created_time = (TextView) itemView.findViewById(R.id.txt_created_date_detail);
            cardView = (CardView) itemView.findViewById(R.id.parent_cardView);
            iv_edit = (ImageView) itemView.findViewById(R.id.iv_edit);
            iv_delete = (ImageView) itemView.findViewById(R.id.iv_delete);
            statusView = itemView.findViewById(R.id.view_status);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_BID);
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(listItems.get(getAdapterPosition()).getProjectId()));
                    intent.putExtra(Constants.BID_ID, String.valueOf(listItems.get(getAdapterPosition()).getBidId()));
                    context.startActivity(intent);
                }
            });
            iv_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_EDIT_BID);
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(listItems.get(getAdapterPosition()).getProjectId()));
                    intent.putExtra(Constants.BID_ID, String.valueOf(listItems.get(getAdapterPosition()).getBidId()));
                    context.startActivity(intent);
                    mItemManger.closeAllItems();
                }
            });
            iv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Utils.getInstance().confirmDialog(context, "Are you sure, you want to delete this Bid ?", "delete", new Utils.ConfirmDialogCallbackInterface() {
                        @Override
                        public void onYesClick(String tag) {
                            mItemManger.closeAllItems();
                            myBidsFragmentPresenterImpl.deleteProject(listItems.get(getAdapterPosition()).getBidId(), getAdapterPosition());
                        }

                        @Override
                        public void onNoClick(String tag) {
                            mItemManger.closeAllItems();
                        }
                    });

                    mItemManger.closeAllItems();
                }
            });
        }
    }
}
