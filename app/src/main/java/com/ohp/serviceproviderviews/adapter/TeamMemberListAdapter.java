package com.ohp.serviceproviderviews.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.ohp.R;
import com.ohp.enums.ScreenNavigation;
import com.ohp.serviceproviderviews.beans.TeamMembersModel;
import com.ohp.serviceproviderviews.presenter.TeamMembersFragmentPresenterImpl;

import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class TeamMemberListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements SwipeAdapterInterface {

    private final Context context;
    public SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);
    private List<TeamMembersModel.DataBean> listItems;
    private TeamMembersFragmentPresenterImpl presenter;
    private SwipeLayout item;

    public TeamMemberListAdapter(Context context, List<TeamMembersModel.DataBean> items, TeamMembersFragmentPresenterImpl presenter) {
        listItems = items;
        this.context = context;
        this.presenter = presenter;
    }

    public void update(List<TeamMembersModel.DataBean> items) {
        listItems = items;
        notifyDataSetChanged();
    }

    public void clearAll() {
        listItems = null;
        notifyDataSetChanged();
    }

    public void deleteItem(int deletedItemPos) {
        notifyItemRemoved(deletedItemPos);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate your layout and pass it to view holder
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_team_member, parent, false);
        item = (SwipeLayout) view.findViewById(R.id.swipe_item);
        item.setShowMode(SwipeLayout.ShowMode.PullOut);
        item.addDrag(SwipeLayout.DragEdge.Left, item.findViewById(R.id.bottom_wrapper));
        item.addDrag(SwipeLayout.DragEdge.Right, item.findViewById(R.id.bottom_wrapper_2));
        return new ViewHolder(view);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        System.out.println("ATTACHED");
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        System.out.println("View Attached");
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        try {
            if (holder instanceof ViewHolder) {
                ViewHolder dataHolder = (ViewHolder) holder;
                TeamMembersModel.DataBean dataObj = listItems.get(position);

                dataHolder.tvName.setText(dataObj.getName());
                dataHolder.tvEmail.setText(dataObj.getEmail());
                dataHolder.tvAddress.setText(dataObj.getAddress());

                String expertise = "";
                for (int i = 0; i < dataObj.getExpertise().size(); i++) {
                    if (i == 0) {
                        expertise = dataObj.getExpertise().get(i).getExpertiseTitle();
                    } else {
                        expertise = expertise + "," + dataObj.getExpertise().get(i).getExpertiseTitle();
                    }
                }

                switch (dataObj.getTypeStatus()) {
                    case 1:
                        dataHolder.tvName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_contractor, 0, 0, 0);
                        break;
                    case 2:
                        dataHolder.tvName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_team_member, 0, 0, 0);
                        break;
                }

                Spannable spannableExpertise = new SpannableString("Expertise in: " + expertise);
                spannableExpertise.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.light_gray)), 0, 14, 0);

                dataHolder.tvExpertise.setText(spannableExpertise);

                mItemManger.bind(holder.itemView, position);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return listItems == null ? 0 : listItems.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_item;
    }

    @Override
    public void notifyDTChanged() {

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivEdit, ivDelete, ivAvatar;
        private CardView cvItem;
        private TextView tvName, tvAddress, tvEmail, tvExpertise;
        private ProgressBar progressBar;

        public ViewHolder(View view) {
            super(view);

            ivEdit = (ImageView) view.findViewById(R.id.iv_edit);
            ivDelete = (ImageView) view.findViewById(R.id.iv_delete);
            ivAvatar = (ImageView) view.findViewById(R.id.avatar);
            cvItem = (CardView) view.findViewById(R.id.cv_item);
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvAddress = (TextView) view.findViewById(R.id.tv_address);
            tvEmail = (TextView) view.findViewById(R.id.tv_email);
            tvExpertise = (TextView) view.findViewById(R.id.tv_expertise);
            progressBar = (ProgressBar) view.findViewById(R.id.progress);

            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.DELETE_ITEM);
                    mItemManger.closeAllItems();
                }
            });

            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.EDIT_ITEM);
                    mItemManger.closeAllItems();
                }
            });

            cvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.DETAIL_ITEM, ivAvatar);
                }
            });
        }

    }


}
