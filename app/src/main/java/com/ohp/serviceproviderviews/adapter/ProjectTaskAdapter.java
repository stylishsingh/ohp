package com.ohp.serviceproviderviews.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.library.basecontroller.AppBaseCompatActivity;
import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.ProjectDetailResponse;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * @author Amanpal Singh.
 */

public class ProjectTaskAdapter extends RecyclerView.Adapter<ProjectTaskAdapter.ViewHolder> {
    private List<ProjectDetailResponse.DataBean.ProjectTasksBean.TaskBean> listItems;
    private Context context;
    private View view;

    public ProjectTaskAdapter(AppBaseCompatActivity activity, List<ProjectDetailResponse.DataBean.ProjectTasksBean.TaskBean> listItems) {
        context = activity;
        this.listItems = listItems;
    }


    public void update(List<ProjectDetailResponse.DataBean.ProjectTasksBean.TaskBean> items) {
        listItems = items;
        notifyDataSetChanged();
    }

    public void clearAll() {
        listItems = null;
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_project_task, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            holder.img_android.setImageResource(R.drawable.ic_my_projects);
            holder.tvTaskName.setText(listItems.get(position).getTaskName());
            holder.tvPropertyName.setText(listItems.get(position).getProperty().getPropertyName());


            holder.tvProjectName.setText(listItems.get(position).getProject().getProjectName());

            holder.tvCreatedDate.setText(Utils.getInstance().getYMDProjectFormattedDateTime(listItems.get(position).getCreated().replace("+00:00", "")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTaskName, tvPropertyName, tvProjectName, tvCreatedDate;
        private CircleImageView img_android;
        private CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTaskName = (TextView) itemView.findViewById(R.id.tv_name);
            tvPropertyName = (TextView) itemView.findViewById(R.id.tv_property_name);
            tvCreatedDate = (TextView) itemView.findViewById(R.id.tv_created_date);
            tvProjectName = (TextView) itemView.findViewById(R.id.tv_project_name);
            img_android = (CircleImageView) itemView.findViewById(R.id.dummyImage);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_TASK);
                    intent.putExtra(Constants.TASK_ID, String.valueOf(listItems.get(getAdapterPosition()).getTaskId()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
