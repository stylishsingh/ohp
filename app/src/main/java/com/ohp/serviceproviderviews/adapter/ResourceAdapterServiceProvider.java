package com.ohp.serviceproviderviews.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.beans.ResourcesValueModel;
import com.ohp.serviceproviderviews.beans.GetProjectDetailServiceProviderResponse;
import com.ohp.serviceproviderviews.presenter.ProjectDetailServiceFragmentPresenterImpl;

import java.util.List;

/**
 * Created by vishal.sharma on 7/28/2017.
 */


public class ResourceAdapterServiceProvider extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<GetProjectDetailServiceProviderResponse.DataBean.PropertyBean.ResourceBean> resourceList;
    private ProjectDetailServiceFragmentPresenterImpl presenter;
    private List<ResourcesValueModel> listResourcesValue;
    private boolean isEdit;

    public ResourceAdapterServiceProvider(Context ctx, List<GetProjectDetailServiceProviderResponse.DataBean.PropertyBean.ResourceBean> list, ProjectDetailServiceFragmentPresenterImpl presenter) {
        this.context = ctx;
        this.resourceList = list;
        this.presenter = presenter;
        this.listResourcesValue = listResourcesValue;
        this.isEdit = isEdit;
    }


  /*  public void updateResourceValues(List<ResourcesValueModel> listResourcesValue) {
        this.listResourcesValue = listResourcesValue;
        notifyItemRangeChanged(0, listResourcesValue.size());
    }*/

    public void updateResource(List<GetProjectDetailServiceProviderResponse.DataBean.PropertyBean.ResourceBean> list) {
        this.resourceList = list;
        notifyItemRangeChanged(0, list.size());
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_resource_serviceprovider, parent, false);
        return new ResourcesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ResourcesViewHolder) {
            //set data to spinner
            ResourcesViewHolder vh = (ResourcesViewHolder) holder;

            /*String title = resourceList.get(position).getTitle().replace("_", " ");
            System.out.println("ResourcesAdapter.onBindViewHolder--->" + resourceList.get(position).getTitle() + " " + listResourcesValue.get(position).getCount());*/

            vh.resourceDetailTitle.setText(resourceList.get(position).getTitle());
            vh.resourceCount.setText(resourceList.get(position).getValue());
        }



    }

    @Override
    public int getItemCount() {
        return (resourceList == null) ? 0 : resourceList.size();
    }

    private class ResourcesViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout resourceEditLayout, resourceDetailLayout;
        private TextView resourceTitle, resourceDetailTitle, resourceCount;

        private ResourcesViewHolder(View itemView) {
            super(itemView);
           // resourceEditLayout = (LinearLayout) itemView.findViewById(R.id.resource_edit_container);
            resourceDetailLayout = (LinearLayout) itemView.findViewById(R.id.resource_detail_container);
          //  resourceTitle = (TextView) itemView.findViewById(R.id.resource_title);
            resourceDetailTitle = (TextView) itemView.findViewById(R.id.resource_detail_title);
            resourceCount = (TextView) itemView.findViewById(R.id.resource_count);


            resourceDetailLayout.setVisibility(View.VISIBLE);


        }
    }


}
