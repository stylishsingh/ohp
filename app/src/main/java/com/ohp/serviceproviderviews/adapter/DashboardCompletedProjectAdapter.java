package com.ohp.serviceproviderviews.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.library.basecontroller.AppBaseCompatActivity;
import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.serviceproviderviews.beans.GetAllProjectsServiceProviderResponse;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vishal.sharma on 8/1/2017.
 */


public class DashboardCompletedProjectAdapter extends RecyclerView.Adapter<DashboardCompletedProjectAdapter.Viewholder> {
    private List<GetAllProjectsServiceProviderResponse.DataBean.CompletedProjectsBean> getAllProjectsResponsesList;
    private Context context;
    private View view;

    public DashboardCompletedProjectAdapter(AppBaseCompatActivity activity, List<GetAllProjectsServiceProviderResponse.DataBean.CompletedProjectsBean> getAllProjectsResponsesList) {
        context = activity;
        this.getAllProjectsResponsesList = getAllProjectsResponsesList;
    }


    public void update(List<GetAllProjectsServiceProviderResponse.DataBean.CompletedProjectsBean> items) {
        getAllProjectsResponsesList = items;
        notifyDataSetChanged();
    }

    public void clearAll() {
        getAllProjectsResponsesList = null;
        notifyDataSetChanged();
    }


    @Override
    public Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.all_projects_list, viewGroup, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, int position) {
        holder.img_android.setImageResource(R.drawable.ic_completed_porject);
        holder.txt_Project_name.setText(getAllProjectsResponsesList.get(position).getProjectName());
        holder.txt_Property_name.setText(getAllProjectsResponsesList.get(position).getPropertyName());


        holder.txt_JobType_name.setText(getAllProjectsResponsesList.get(position).getProjectJobtype().getJobtypeTitle());

        holder.txt_created_time.setText(Utils.getInstance().getYMDProjectFormattedDateTime(getAllProjectsResponsesList.get(position).getCreated().replace("+00:00", "")));
    }


    @Override
    public int getItemCount() {
        return getAllProjectsResponsesList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private TextView txt_Project_name, txt_Property_name, txt_JobType_name, txt_created_time;
        private CircleImageView img_android;
        private CardView cardView;

        public Viewholder(View itemView) {
            super(itemView);
            txt_Project_name = (TextView) itemView.findViewById(R.id.txt_project_name);
            txt_Property_name = (TextView) itemView.findViewById(R.id.txt_PropertyName);
            txt_created_time = (TextView) itemView.findViewById(R.id.tv_created_date);
            txt_JobType_name = (TextView) itemView.findViewById(R.id.txt_job_type);
            img_android = (CircleImageView) itemView.findViewById(R.id.dummyImage);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.PROJECT_DETAIL_SERVICE_PROVIDER);
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(getAllProjectsResponsesList.get(getAdapterPosition()).getProjectId()));
                    //intent.putExtra(Constants.PROJECT_ID, "187");
                    context.startActivity(intent);
                }
            });
        }
    }
}