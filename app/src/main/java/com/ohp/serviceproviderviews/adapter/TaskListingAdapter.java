package com.ohp.serviceproviderviews.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.ohp.R;
import com.ohp.serviceproviderviews.beans.GetTaskListingResponse;
import com.ohp.serviceproviderviews.presenter.TaskFragmentPresenterImpl;
import com.ohp.utils.Utils;

import java.util.List;

import static com.ohp.enums.ScreenNavigation.DELETE_ITEM;
import static com.ohp.enums.ScreenNavigation.DETAIL_ITEM;
import static com.ohp.enums.ScreenNavigation.EDIT_ITEM;

/**
 * Created by vishal.sharma on 8/28/2017.
 */


public class TaskListingAdapter extends RecyclerView.Adapter<TaskListingAdapter.ViewHolder> implements SwipeAdapterInterface {
    public SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);
    private View view;
    private List<GetTaskListingResponse.DataBean> listItems;
    private TaskFragmentPresenterImpl presenter;
    private Context context;
    private int role;

    public TaskListingAdapter(Context context, List<GetTaskListingResponse.DataBean> listItems, TaskFragmentPresenterImpl presenter, int role) {
        this.context = context;
        this.listItems = listItems;
        this.presenter = presenter;
        this.role = role;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_task, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        switch (listItems.get(position).getTaskStatus()) {
            case 1:
                viewHolder.view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                break;
            case 3:
                viewHolder.view.setBackgroundColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                break;
            case 4:
                viewHolder.view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                break;
            case 5:
                viewHolder.view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                break;
        }

        viewHolder.txt_task_name.setText(listItems.get(position).getTaskName());
        viewHolder.txt_TeamMemberName.setText(listItems.get(position).getMember().getMemberName());
        viewHolder.txt_PropertyName.setText(listItems.get(position).getProperty().getPropertyName());

        viewHolder.txt_Project_name.setText(listItems.get(position).getProject().getProjectName());
        if (!listItems.get(position).getCreated().isEmpty())
            viewHolder.txt_created_time.setText(Utils.getInstance().getYMDFormattedTime(listItems.get(position).getCreated()));

        switch (listItems.get(position).getTaskType()) {
            case 1:
                viewHolder.item.setRightSwipeEnabled(true);
                viewHolder.item.setLeftSwipeEnabled(true);
                break;
            case 2:
                viewHolder.item.setRightSwipeEnabled(false);
                viewHolder.item.setLeftSwipeEnabled(false);
                break;
        }
        mItemManger.bind(viewHolder.itemView, position);

    }

    public void updateList(List<GetTaskListingResponse.DataBean> listItems) {
        this.listItems = listItems;
        notifyDataSetChanged();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_item;
    }

    @Override
    public void notifyDTChanged() {

    }


    @Override
    public int getItemCount() {
        return listItems == null ? 0 : listItems.size();
    }

    public void clearAll() {
        listItems = null;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_task_name, txt_Project_name, txt_created_time, txt_PropertyName, txt_TeamMemberName;
        private ImageView ivEdit, ivDelete;
        private CardView cardView;
        private View view;
        private SwipeLayout item;

        public ViewHolder(View itemView) {
            super(itemView);
            item = (SwipeLayout) itemView.findViewById(R.id.swipe_item);
            item.setShowMode(SwipeLayout.ShowMode.PullOut);
            item.addDrag(SwipeLayout.DragEdge.Left, item.findViewById(R.id.bottom_wrapper));
            item.addDrag(SwipeLayout.DragEdge.Right, item.findViewById(R.id.bottom_wrapper_2));
            ivEdit = (ImageView) itemView.findViewById(R.id.iv_edit);
            ivDelete = (ImageView) itemView.findViewById(R.id.iv_delete);
            view = itemView.findViewById(R.id.view_status);
            txt_task_name = (TextView) itemView.findViewById(R.id.txt_task);

            txt_created_time = (TextView) itemView.findViewById(R.id.txt_created_date_detail);
            txt_Project_name = (TextView) itemView.findViewById(R.id.txt_Job_Type_detail);
            txt_PropertyName = (TextView) itemView.findViewById(R.id.txt_expertise_detail);
            txt_TeamMemberName = (TextView) itemView.findViewById(R.id.txt_owner_name);
            cardView = (CardView) itemView.findViewById(R.id.parent_cardView);

            if (role == 1) {
                item.removeAllSwipeDeniers();
            }

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), DETAIL_ITEM);

                }
            });

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemManger.closeAllItems();
                    presenter.onItemClicked(getAdapterPosition(), DELETE_ITEM);
                }
            });

            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemManger.closeAllItems();
                    presenter.onItemClicked(getAdapterPosition(), EDIT_ITEM);
                }
            });

        }
    }
}
