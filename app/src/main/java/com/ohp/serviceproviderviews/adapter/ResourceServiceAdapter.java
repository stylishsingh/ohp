package com.ohp.serviceproviderviews.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.beans.ProjectDetailResponse;
import com.ohp.serviceproviderviews.presenter.ProjectDetailServiceFragmentPresenterImpl;

import java.util.List;

/**
 * Created by vishal.sharma on 8/1/2017.
 */

public class ResourceServiceAdapter extends RecyclerView.Adapter<ResourceServiceAdapter.ViewHolder> {
    private Context context;
    private List<ProjectDetailResponse.DataBean.PropertyBean.ResourceBean> resourceList;
    private ProjectDetailServiceFragmentPresenterImpl presenter;

    public ResourceServiceAdapter(Context ctx, List<ProjectDetailResponse.DataBean.PropertyBean.ResourceBean> list, ProjectDetailServiceFragmentPresenterImpl presenter) {
        this.context = ctx;
        this.resourceList = list;
        this.presenter = presenter;
    }


    public void updateResource(List<ProjectDetailResponse.DataBean.PropertyBean.ResourceBean> list) {
        this.resourceList = list;
        notifyItemRangeChanged(0, list.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_resource_serviceprovider, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
     viewHolder.resourceDetailTitle.setText(resourceList.get(i).getTitle());
        viewHolder.resourceCount.setText(resourceList.get(i).getValue());
    }

    @Override
    public int getItemCount() {
        return (resourceList == null) ? 0 : resourceList.size();
    }

    public class  ViewHolder extends RecyclerView.ViewHolder{
        private TextView  resourceDetailTitle, resourceCount;
        public ViewHolder(View itemView) {
            super(itemView);
            resourceDetailTitle = (TextView) itemView.findViewById(R.id.resource_detail_title);
            resourceCount = (TextView) itemView.findViewById(R.id.resource_count);


        }
    }
}
