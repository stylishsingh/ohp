package com.ohp.serviceproviderviews.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.library.basecontroller.AppBaseCompatActivity;
import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.serviceproviderviews.beans.GetAllProjectsServiceProviderResponse;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vishal.sharma on 8/14/2017.
 */

public class DashboardMyProjectsServiceProviderAdapter extends RecyclerView.Adapter<DashboardMyProjectsServiceProviderAdapter.ViewHolder> {
    private List<GetAllProjectsServiceProviderResponse.DataBean.MyProjectsBean> getAllProjectsResponsesList;
    private Context context;
    private View view;

    public DashboardMyProjectsServiceProviderAdapter(AppBaseCompatActivity activity, List<GetAllProjectsServiceProviderResponse.DataBean.MyProjectsBean> getAllProjectsResponsesList) {
        context = activity;
        this.getAllProjectsResponsesList = getAllProjectsResponsesList;
    }


    public void update(List<GetAllProjectsServiceProviderResponse.DataBean.MyProjectsBean> items) {
        getAllProjectsResponsesList = items;
        notifyDataSetChanged();
    }

    public void clearAll() {
        getAllProjectsResponsesList = null;
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.all_projects_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            holder.img_android.setImageResource(R.drawable.ic_my_projects);
            holder.txt_Project_name.setText(getAllProjectsResponsesList.get(position).getProjectName());
            holder.txt_Property_name.setText(getAllProjectsResponsesList.get(position).getPropertyName());


            holder.txt_JobType_name.setText(getAllProjectsResponsesList.get(position).getProjectJobtype().getJobtypeTitle());

            holder.txt_created_time.setText(Utils.getInstance().getYMDProjectFormattedDateTime(getAllProjectsResponsesList.get(position).getCreated().replace("+00:00", "")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return getAllProjectsResponsesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_Project_name, txt_Property_name, txt_JobType_name, txt_created_time;
        private CircleImageView img_android;
        private CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_Project_name = (TextView) itemView.findViewById(R.id.txt_project_name);
            txt_Property_name = (TextView) itemView.findViewById(R.id.txt_PropertyName);
            txt_created_time = (TextView) itemView.findViewById(R.id.tv_created_date);
            txt_JobType_name = (TextView) itemView.findViewById(R.id.txt_job_type);
            img_android = (CircleImageView) itemView.findViewById(R.id.dummyImage);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.MY_PROJECTS_SERVICE_PROVIDER_View);
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(getAllProjectsResponsesList.get(getAdapterPosition()).getProjectId()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
