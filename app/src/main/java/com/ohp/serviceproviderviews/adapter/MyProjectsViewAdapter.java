package com.ohp.serviceproviderviews.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.serviceproviderviews.beans.GetAllProjectsServiceProviderResponse;
import com.ohp.serviceproviderviews.presenter.AllMyProjectsViewFragmentPresenter;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.List;

/**
 * Created by vishal.sharma on 8/16/2017.
 */


public class MyProjectsViewAdapter extends RecyclerView.Adapter<MyProjectsViewAdapter.ViewHolder> implements SwipeAdapterInterface {
    public SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);
    private View view;
    private SwipeLayout item;
    private List<GetAllProjectsServiceProviderResponse.DataBean.MyProjectsBean> listItems;
    private AllMyProjectsViewFragmentPresenter allWonViewProjectPresenter;
    private Context context;

    public MyProjectsViewAdapter(Context context, List<GetAllProjectsServiceProviderResponse.DataBean.MyProjectsBean> listItems, AllMyProjectsViewFragmentPresenter allWonViewProjectPresenter) {
        this.context = context;
        this.listItems = listItems;
        this.allWonViewProjectPresenter = allWonViewProjectPresenter;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_service_project_detail_screen, viewGroup, false);
        item = (SwipeLayout) view.findViewById(R.id.swipe_item);
        item.setShowMode(SwipeLayout.ShowMode.PullOut);
        item.addDrag(SwipeLayout.DragEdge.Left, item.findViewById(R.id.bottom_wrapper));
        item.addDrag(SwipeLayout.DragEdge.Right, item.findViewById(R.id.bottom_wrapper_2));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.txt_Project_name.setText(listItems.get(position).getProjectName());
        viewHolder.txt_Property_name.setText(listItems.get(position).getPropertyName());
        viewHolder.txt_propertyOwnerName.setText(listItems.get(position).getPropertyOwner()); // not  given in API property owner name
        if (listItems.get(position).getProjectExpertise().size() > 0) {
            StringBuilder expertise = new StringBuilder();
            for (int i = 0; i < listItems.get(position).getProjectExpertise().size(); i++) {

                if (i < (listItems.get(position).getProjectExpertise().size() - 1)) {
                    expertise.append(listItems.get(position).getProjectExpertise().get(i).getExpertiseTitle() + ", ");
                } else {
                    expertise.append(listItems.get(position).getProjectExpertise().get(i).getExpertiseTitle());
                }
            }
            viewHolder.txt_expertise.setText(expertise);
        }
        viewHolder.txt_JobType_name.setText(listItems.get(position).getProjectJobtype().getJobtypeTitle());
        if (!listItems.get(position).getCreated().isEmpty())
            viewHolder.txt_created_time.setText(Utils.getInstance().getYMDFormattedTime(listItems.get(position).getCreated()));
        mItemManger.bind(viewHolder.itemView, position);
    }

    public void updateList(List<GetAllProjectsServiceProviderResponse.DataBean.MyProjectsBean> listItems) {
        this.listItems = listItems;
        notifyDataSetChanged();
    }

    public void clearAll() {
        this.listItems = null;
        notifyDataSetChanged();
    }


    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_item;
    }

    @Override
    public void notifyDTChanged() {

    }


    @Override
    public int getItemCount() {
        return listItems == null ? 0 : listItems.size();
    }

    public void cancelAll() {
        listItems = null;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_Project_name, txt_Property_name, txt_JobType_name, txt_created_time, txt_expertise, txt_propertyOwnerName;
        private ImageView ivEdit, ivDelete;
        private CardView cardView;


        public ViewHolder(View itemView) {
            super(itemView);
            ivEdit = (ImageView) view.findViewById(R.id.iv_edit);
            ivDelete = (ImageView) view.findViewById(R.id.iv_delete);
            txt_Project_name = (TextView) itemView.findViewById(R.id.txt_project_name);
            txt_Property_name = (TextView) itemView.findViewById(R.id.txt_property_detail);
            txt_created_time = (TextView) itemView.findViewById(R.id.txt_created_date_detail);
            txt_JobType_name = (TextView) itemView.findViewById(R.id.txt_Job_Type_detail);
            txt_expertise = (TextView) itemView.findViewById(R.id.txt_expertise_detail);
            txt_propertyOwnerName = (TextView) itemView.findViewById(R.id.txt_owner_name);
            cardView = (CardView) itemView.findViewById(R.id.parent_cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    // intent.putExtra(Constants.DESTINATION, Constants.PROJECT_DETAIL_SERVICE_PROVIDER);
                    intent.putExtra(Constants.DESTINATION, Constants.MY_PROJECTS_SERVICE_PROVIDER_View);
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(listItems.get(getAdapterPosition()).getProjectId()));
                    //   intent.putExtra(Constants.PROJECT_ID, "187");
                    context.startActivity(intent);
                }
            });

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().confirmDialog(context, "Are you sure, you want to delete this project?", "delete", new Utils.ConfirmDialogCallbackInterface() {
                        @Override
                        public void onYesClick(String tag) {
                            mItemManger.closeAllItems();
                            allWonViewProjectPresenter.deleteProject(listItems.get(getAdapterPosition()).getProjectId(), getAdapterPosition());
                        }

                        @Override
                        public void onNoClick(String tag) {
                            mItemManger.closeAllItems();
                        }
                    });
                }
            });

            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    // intent.putExtra(Constants.DESTINATION, Constants.PROJECT_DETAIL_SERVICE_PROVIDER);
                    intent.putExtra(Constants.DESTINATION, Constants.MY_PROJECTS_SERVICE_PROVIDER_EDIT);
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(listItems.get(getAdapterPosition()).getProjectId()));
                    //   intent.putExtra(Constants.PROJECT_ID, "187");
                    context.startActivity(intent);
                    mItemManger.closeAllItems();
                }
            });

        }
    }
}