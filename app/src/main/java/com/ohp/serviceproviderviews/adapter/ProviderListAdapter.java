package com.ohp.serviceproviderviews.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.serviceproviderviews.beans.ProvidersListResponse;
import com.ohp.serviceproviderviews.presenter.AddEditTeamMemberPresenterImpl;

import java.util.List;

/**
 * Created by vishal.sharma on 8/16/2017.
 */

public class ProviderListAdapter extends RecyclerView.Adapter<ProviderListAdapter.Viewholder> {
    private View view;
    private Context context;
    private List<ProvidersListResponse.DataBean> providersListResponses;
    private AddEditTeamMemberPresenterImpl addEditTeamMemberPresenter;

    public ProviderListAdapter(Context context, List<ProvidersListResponse.DataBean> providersListResponses, AddEditTeamMemberPresenterImpl addEditTeamMemberPresenter) {
        this.context = context;
        this.providersListResponses = providersListResponses;
        this.addEditTeamMemberPresenter = addEditTeamMemberPresenter;
    }


    @Override
    public Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.providerlist_layout, viewGroup, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(Viewholder viewholder, int i) {
        viewholder.text_memberName.setText(providersListResponses.get(i).getFullName());
    }

    public void updateList(List<ProvidersListResponse.DataBean> providersListResponses) {
        this.providersListResponses = providersListResponses;
        notifyDataSetChanged();
    }

    public void clearAll() {
        this.providersListResponses = null;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return providersListResponses == null ? 0 : providersListResponses.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        TextView text_memberName;
        RelativeLayout relativeLayout;

        public Viewholder(View itemView) {
            super(itemView);
            text_memberName = (TextView) itemView.findViewById(R.id.et_memberName);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.parent);
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addEditTeamMemberPresenter.onItemClick(getAdapterPosition());

                }
            });

        }
    }
}
