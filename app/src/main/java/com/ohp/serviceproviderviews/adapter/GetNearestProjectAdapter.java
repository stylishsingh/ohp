package com.ohp.serviceproviderviews.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.serviceproviderviews.beans.GetNearestProjectsResponse;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.List;

/**
 * Created by vishal.sharma on 8/2/2017.
 */

public class GetNearestProjectAdapter extends RecyclerView.Adapter<GetNearestProjectAdapter.ViewHolder> {
    private Context context;
    private View view;
    private List<GetNearestProjectsResponse.DataBean> listItems;

    public GetNearestProjectAdapter(Context context, List<GetNearestProjectsResponse.DataBean> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    public void updateList(List<GetNearestProjectsResponse.DataBean> getNearestProjectsResponse) {
        this.listItems = getNearestProjectsResponse;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_nearest_project_layout_adapter, viewGroup, false);
        return new ViewHolder(view);
    }

    public void clearAll() {
        listItems = null;
        notifyDataSetChanged();
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.txt_Project_name.setText(listItems.get(position).getProjectName());
        viewHolder.txt_Property_name.setText(listItems.get(position).getPropertyName());
        viewHolder.txt_propertyOwnerName.setText(listItems.get(position).getPropertyOwner()); // not  given in API property owner name
        if (listItems.get(position).getProjectExpertise().size() > 0) {
            StringBuilder expertise = new StringBuilder();
            for (int i = 0; i < listItems.get(position).getProjectExpertise().size(); i++) {

                if (i < (listItems.get(position).getProjectExpertise().size() - 1)) {
                    expertise.append(listItems.get(position).getProjectExpertise().get(i).getExpertiseTitle()).append(", ");
                } else {
                    expertise.append(listItems.get(position).getProjectExpertise().get(i).getExpertiseTitle());
                }
            }
            viewHolder.txt_expertise.setText(expertise);
        }
        viewHolder.txt_JobType_name.setText(TextUtils.isEmpty(listItems.get(position).getProjectJobtype().getJobtypeTitle()) ? "N/A" : listItems.get(position).getProjectJobtype().getJobtypeTitle());
        viewHolder.txt_created_time.setText(TextUtils.isEmpty(listItems.get(position).getCreated()) ? "N/A" : Utils.getInstance().getYMDFormattedTime(listItems.get(position).getCreated().replace("+00:00", "")));
    }

    @Override
    public int getItemCount() {
        return listItems == null ? 0 : listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        private TextView txt_Project_name, txt_Property_name, txt_JobType_name, txt_created_time, txt_expertise, txt_propertyOwnerName;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_Project_name = (TextView) itemView.findViewById(R.id.txt_project_name);
            txt_Property_name = (TextView) itemView.findViewById(R.id.txt_property_detail);
            txt_created_time = (TextView) itemView.findViewById(R.id.txt_created_date_detail);
            txt_JobType_name = (TextView) itemView.findViewById(R.id.txt_Job_Type_detail);
            txt_expertise = (TextView) itemView.findViewById(R.id.txt_expertise_detail);
            txt_propertyOwnerName = (TextView) itemView.findViewById(R.id.txt_owner_name);
            cardView = (CardView) itemView.findViewById(R.id.parent_cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.BROWSE_PROJECTS);
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(listItems.get(getAdapterPosition()).getProjectId()));

                    //  intent.putExtra(Constants.PROJECT_ID, "187");
                    context.startActivity(intent);
                }
            });
        }
    }
}
