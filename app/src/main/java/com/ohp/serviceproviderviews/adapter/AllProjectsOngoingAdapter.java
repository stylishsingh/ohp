package com.ohp.serviceproviderviews.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.library.basecontroller.AppBaseCompatActivity;
import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.serviceproviderviews.beans.GetAllProjectsResponse;
import com.ohp.utils.Constants;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vishal.sharma on 7/18/2017.
 */

public class AllProjectsOngoingAdapter extends RecyclerView.Adapter<AllProjectsOngoingAdapter.Viewholder> {
    private List<GetAllProjectsResponse.DataBean.OngoingProjectBean> getAllProjectsResponsesList;
    private Context context;
    private View view;

    public AllProjectsOngoingAdapter(AppBaseCompatActivity activity, List<GetAllProjectsResponse.DataBean.OngoingProjectBean> getAllProjectsResponsesList) {
        context = activity;
        this.getAllProjectsResponsesList = getAllProjectsResponsesList;
    }


    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_projects_list, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, int position) {
        holder.img_android.setImageResource(R.drawable.ic_ongoing_project);
        holder.txt_Project_name.setText(getAllProjectsResponsesList.get(position).getProject_name());
        holder.txt_Property_name.setText(getAllProjectsResponsesList.get(position).getProperty_name());


        holder.txt_JobType_name.setText(getAllProjectsResponsesList.get(position).getProject_jobtype().getJobtype_title());

        holder.txt_created_time.setText(getAllProjectsResponsesList.get(position).getCreated());
    }

    public void update(List<GetAllProjectsResponse.DataBean.OngoingProjectBean> items) {
        getAllProjectsResponsesList = items;
        notifyDataSetChanged();
    }

    public void clearAll() {
        getAllProjectsResponsesList = null;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return getAllProjectsResponsesList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private TextView txt_Project_name, txt_Property_name, txt_JobType_name, txt_created_time;
        private CircleImageView img_android;
        private CardView cardView;

        public Viewholder(View itemView) {
            super(itemView);
            txt_Project_name = (TextView) itemView.findViewById(R.id.txt_project_name);
            txt_Property_name = (TextView) itemView.findViewById(R.id.txt_PropertyName);
            txt_created_time = (TextView) itemView.findViewById(R.id.tv_created_date);
            txt_JobType_name = (TextView) itemView.findViewById(R.id.txt_job_type);
            img_android = (CircleImageView) itemView.findViewById(R.id.dummyImage);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_VIEW_PROJECT);
                    intent.putExtra(Constants.PROJECT_ID, String.valueOf(getAllProjectsResponsesList.get(getAdapterPosition()).getProject_id()));
                    context.startActivity(intent);
                }
            });
        }
    }

}
