package com.ohp.serviceproviderviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.presenter.TaskFragmentPresenterImpl;
import com.ohp.serviceproviderviews.view.TaskFragmentView;
import com.ohp.utils.Utils;

/**
 * Created by vishal.sharma on 8/8/2017.
 */

public class TaskFragment extends BaseFragment<TaskFragmentPresenterImpl> implements TaskFragmentView {
    private View rootView;
    private RecyclerView recyclerView;
    private RelativeLayout relativeLayout;
    /*  private Toolbar mToolbar;
      private TextView toolbarTitle;*/
    private EditText etSearch;
    private LinearLayout btnClear;
    private boolean isMenu = false;
    private NestedScrollView nsRecyclerView;
    private FloatingActionButton floatingActionButton;

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
    }

    @Override
    protected TaskFragmentPresenterImpl onAttachPresenter() {
        return new TaskFragmentPresenterImpl(this, getActivity());
    }

    @Override
    protected void initUI(View view) {
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fab_add);
        floatingActionButton.setOnClickListener(getPresenter());
     /*   mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);*/
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_task);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.empty_list_layout);
        nsRecyclerView = (NestedScrollView) view.findViewById(R.id.ns_recycler_view);
        etSearch = (EditText) view.findViewById(R.id.et_search);
        btnClear = (LinearLayout) view.findViewById(R.id.btn_clear);
        btnClear.setOnClickListener(getPresenter());

        if (Utils.getInstance().getRole(getActivity()) == 2) {
            floatingActionButton.setVisibility(View.VISIBLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_task, container, false);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (Utils.getInstance().getRole(getActivity()) == 1) {
            menu.clear();
            inflater.inflate(R.menu.menu_filter_tasks, menu);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_filter_tasks:
                showPopup();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showPopup() {
        try {
            PopupMenu popup;
            if (Utils.getInstance().getRole(getActivity()) == 1)
                popup = new PopupMenu(getActivity(), getActivity().findViewById(R.id.item_filter_tasks));
            else
                popup = new PopupMenu(getActivity(), getActivity().findViewById(R.id.item_filter));
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.menu_task_list, popup.getMenu());
            popup.getMenu().findItem(R.id.action_current_task).setChecked(false);
            popup.getMenu().findItem(R.id.action_completed_task).setChecked(false);
            popup.getMenu().findItem(R.id.action_all_task).setChecked(false);
            switch (getPresenter().taskType) {
                case "ongoing":
                    popup.getMenu().findItem(R.id.action_current_task).setChecked(true);
                    break;
                case "completed":
                    popup.getMenu().findItem(R.id.action_completed_task).setChecked(true);
                    break;
                case "":
                    popup.getMenu().findItem(R.id.action_all_task).setChecked(true);
                    break;
            }
            popup.show();
            popup.setOnMenuItemClickListener(getPresenter());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public RecyclerView getRecyclerTaskList() {
        return recyclerView;
    }

    @Override
    public RelativeLayout getEmptyLayout() {
        return relativeLayout;
    }

    @Override
    public NestedScrollView getNSRecyclerView() {
        return nsRecyclerView;
    }

    @Override
    public EditText getSearchEditText() {
        return etSearch;
    }

    @Override
    public LinearLayout getCloseBtn() {
        return btnClear;
    }
}
