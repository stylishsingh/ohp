package com.ohp.serviceproviderviews.fragment;

import android.view.View;

import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.presenter.SearchProjectFragmentPresenterImpl;
import com.ohp.serviceproviderviews.view.SearchProjectView;

/**
 * Created by vishal.sharma on 7/28/2017.
 */

public class SearchProjectsFragments extends BaseFragment<SearchProjectFragmentPresenterImpl> implements SearchProjectView {
    @Override
    protected SearchProjectFragmentPresenterImpl onAttachPresenter() {
        return new SearchProjectFragmentPresenterImpl(this, getActivity());
    }

    @Override
    protected void initUI(View view) {

    }
}
