package com.ohp.serviceproviderviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.presenter.MyBidsFragmentPresenterImpl;
import com.ohp.serviceproviderviews.view.MyBidsView;

/**
 * Created by vishal.sharma on 8/2/2017.
 */

public class MyBidsFragment extends BaseFragment<MyBidsFragmentPresenterImpl> implements MyBidsView {
    private View view;
    private RecyclerView recyclerView;
    private RelativeLayout emptyLayout;
    private EditText etSearch;
    private LinearLayout btnClear;
    private NestedScrollView nsRecyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }

    @Override
    protected MyBidsFragmentPresenterImpl onAttachPresenter() {
        return new MyBidsFragmentPresenterImpl(this, getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_mybids_layout, container, false);
        return view;
    }

    @Override
    protected void initUI(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_bids);
        emptyLayout = (RelativeLayout) view.findViewById(R.id.empty_list_layout);
        nsRecyclerView = (NestedScrollView) view.findViewById(R.id.ns_recycler_view);
        etSearch = (EditText) view.findViewById(R.id.et_search);
        btnClear = (LinearLayout) view.findViewById(R.id.btn_clear);
        btnClear.setOnClickListener(getPresenter());
    }

    @Override
    public RelativeLayout getEmptyLayout() {
        return emptyLayout;
    }

    @Override
    public RecyclerView bidListView() {
        return recyclerView;
    }

    @Override
    public View getRootView() {
        return view;
    }

    @Override
    public NestedScrollView getNSRecyclerView() {
        return nsRecyclerView;
    }

    @Override
    public EditText getSearchEditText() {
        return etSearch;
    }

    @Override
    public LinearLayout getCloseBtn() {
        return btnClear;
    }
}
