package com.ohp.serviceproviderviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.presenter.DashboardAllProjectsPresenterImpl;
import com.ohp.serviceproviderviews.view.DashboardAllProjectView;

/**
 * @author Vishal Sharma.
 */

public class DashboardAllProjectsFragment extends BaseFragment<DashboardAllProjectsPresenterImpl> implements DashboardAllProjectView {
    private View view;
    private RecyclerView rvWonProjects, rvMyProjects, rvCompletedProjects;
    private FloatingActionButton fabAddProperties;
    private TextView tvMyProjectsViewAll, tvWonViewAll, tvCompletedViewAll, tvNoWonProjects, tvNoMyProjects, tvNoCompletedProjects, tvMyProjectsTitle, tvWonTitle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.project_detail_property_fragment, container, false);
        return view;
    }

    @Override
    protected DashboardAllProjectsPresenterImpl onAttachPresenter() {
        return new DashboardAllProjectsPresenterImpl(this, getActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    protected void initUI(View view) {
        tvMyProjectsViewAll = (TextView) view.findViewById(R.id.tv_my_project_view_all);
        tvMyProjectsViewAll.setOnClickListener(getPresenter());
        tvWonViewAll = (TextView) view.findViewById(R.id.tv_won_view_all);
        tvWonViewAll.setOnClickListener(getPresenter());
        tvCompletedViewAll = (TextView) view.findViewById(R.id.tv_completed_view_all);
        tvCompletedViewAll.setOnClickListener(getPresenter());
        tvNoCompletedProjects = (TextView) view.findViewById(R.id.tv_no_completed_projects);
        tvNoMyProjects = (TextView) view.findViewById(R.id.tv_no_my_projects);
        tvNoWonProjects = (TextView) view.findViewById(R.id.tv_no_won_projects);
        tvMyProjectsTitle = (TextView) view.findViewById(R.id.tv_my_projects_title);
        tvWonTitle = (TextView) view.findViewById(R.id.tv_won_title);
        rvWonProjects = (RecyclerView) view.findViewById(R.id.rv_won_projects);
        rvMyProjects = (RecyclerView) view.findViewById(R.id.rv_my_projects);
        rvCompletedProjects = (RecyclerView) view.findViewById(R.id.recycler_view_completed);
        fabAddProperties = (FloatingActionButton) view.findViewById(R.id.fab_add_projects);
        fabAddProperties.setOnClickListener(getPresenter());
        //  fabAddProperties.setVisibility(View.INVISIBLE);
    }

    @Override
    public View getRootView() {
        return view;
    }

    @Override
    public TextView getTVWonViewAll() {
        return tvWonViewAll;
    }

    @Override
    public TextView getTVMyProjectsViewAll() {
        return tvMyProjectsViewAll;
    }

    @Override
    public TextView getCompletedProjects() {
        return tvCompletedViewAll;
    }

    @Override
    public TextView getTVNoMyProjects() {
        return tvNoMyProjects;
    }

    @Override
    public TextView getTVNoWonProjects() {
        return tvNoWonProjects;
    }

    @Override
    public TextView getTVNoCompletedProjects() {
        return tvNoCompletedProjects;
    }

    @Override
    public RecyclerView getRVMyProjects() {
        return rvMyProjects;
    }

    @Override
    public RecyclerView getRVWonProjects() {
        return rvWonProjects;
    }

    @Override
    public RecyclerView getRVCompletedProjects() {
        return rvCompletedProjects;
    }
}
