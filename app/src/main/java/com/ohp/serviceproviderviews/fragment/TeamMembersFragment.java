package com.ohp.serviceproviderviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.presenter.TeamMembersFragmentPresenterImpl;
import com.ohp.serviceproviderviews.view.TeamMembersView;

/**
 * @author Amanpal Singh.
 */

public class TeamMembersFragment extends BaseFragment<TeamMembersFragmentPresenterImpl> implements TeamMembersView {

    private RecyclerView recyclerView;
    private RelativeLayout emptyView;
    private EditText etSearch;
    private LinearLayout btnClear;
    private NestedScrollView nsRecyclerView;
    private View rootView;
    private FloatingActionButton floatingActionButton;

    @Override
    protected TeamMembersFragmentPresenterImpl onAttachPresenter() {
        return new TeamMembersFragmentPresenterImpl(this, getActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_team_members, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    protected void initUI(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        nsRecyclerView = (NestedScrollView) view.findViewById(R.id.ns_recycler_view);
        emptyView = (RelativeLayout) view.findViewById(R.id.empty_list_layout);
        etSearch = (EditText) view.findViewById(R.id.et_search);
        btnClear = (LinearLayout) view.findViewById(R.id.btn_clear);

        recyclerView.setAdapter(getPresenter().initAdapter());
        btnClear.setOnClickListener(getPresenter());
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fab_add);
        floatingActionButton.setOnClickListener(getPresenter());
    }

    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public NestedScrollView getNSRecyclerView() {
        return nsRecyclerView;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public RelativeLayout getEmptyView() {
        return emptyView;
    }

    @Override
    public EditText getSearchEditText() {
        return etSearch;
    }

    @Override
    public LinearLayout getCloseBtn() {
        return btnClear;
    }

}
