package com.ohp.serviceproviderviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.presenter.AllCompletedViewProjectsPresenter;
import com.ohp.serviceproviderviews.view.AllCompletedViewProjectsView;

/**
 * Created by vishal.sharma on 7/31/2017.
 */

public class AllCompletedViewProjectsServiceFragment extends BaseFragment<AllCompletedViewProjectsPresenter> implements AllCompletedViewProjectsView {

    private View view;
    private RecyclerView recyclerView;
    private RelativeLayout relativeLayout;
    private Toolbar mToolbar;
    private TextView toolbarTitle;
    private EditText etSearch;
    private LinearLayout btnClear;
    private NestedScrollView nsRecyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_won_projects, container, false);
        return view;
    }
    @Override
    protected AllCompletedViewProjectsPresenter onAttachPresenter() {
        return new AllCompletedViewProjectsPresenter(this,getActivity());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_dashboard, menu);
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_won);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.empty_list_layout);
        nsRecyclerView = (NestedScrollView) view.findViewById(R.id.ns_recycler_view);
        etSearch = (EditText) view.findViewById(R.id.et_search);
        btnClear = (LinearLayout) view.findViewById(R.id.btn_clear);
        btnClear.setOnClickListener(getPresenter());
    }

    @Override
    public View getRootView() {
        return view;
    }

    @Override
    public RecyclerView getRecyclerCompletedProjectList() {
        return recyclerView;
    }

    @Override
    public RelativeLayout getEmptyLayout() {
        return relativeLayout;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public NestedScrollView getNSRecyclerView() {
        return nsRecyclerView;
    }

    @Override
    public EditText getSearchEditText() {
        return etSearch;
    }

    @Override
    public LinearLayout getCloseBtn() {
        return btnClear;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }


}
