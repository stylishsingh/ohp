package com.ohp.serviceproviderviews.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.presenter.AddEditTaskSpPresenterImpl;
import com.ohp.serviceproviderviews.view.AddEditTaskViewSp;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

/**
 * @author Vishal Sharma.
 */

public class AddEditTaskFragmentSp extends BaseFragment<AddEditTaskSpPresenterImpl> implements AddEditTaskViewSp {
    private View rootView, EditTask, ViewTask, viewReopenDone;
    private String currentScreen = "";
    private NestedScrollView nestedScrollView;
    private MenuItem editMenu, saveMenu;
    private RecyclerView recyclerViewEditProject, recyclerView_ViewProject;
    private Toolbar mToolbar;
    private AppCompatTextView tvMarkCompleted, tvReopen, tvDone;
    private TextView toolbarTitle, tvHeaderPropertyName, tvHeaderProjectName,
            tvHeaderEventRepeatName, tvHeaderRepeatReminderName, tvRepeatReminder, tvHeaderTaskFromTime,
            tvTaskFromTime, tvHeaderTaskToTime, tvTaskToTime, tvHeaderTaskName, tvNoImages;
    private AppCompatSpinner spinnerProject, spinnerAppliance, spinnerRepeatEvent, spinnerTeamMember;
    private TextInputEditText etTaskTitle, etAddNotes, etDateTimeFrom, etDateTimeTo, etReminder, edit_Property;
    private TextInputEditText etTaskTilleView, etAddNotesView, etReminderDateView, etProjectView, etPropertyView, etTeamMemberView, etRepeatReminderTaskView;
    private TextView etDateTimeFromView, etDateTimeToView;
    private int taskType = 0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_edit_task_main_sp, container, false);
        return rootView;
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        //  tvHeaderPropertyName = (TextView) view.findViewById(R.id.tvHeaderPropertyName);
        tvHeaderProjectName = (TextView) view.findViewById(R.id.tvHeaderProjectName);
        tvHeaderTaskName = (TextView) view.findViewById(R.id.tvHeaderTaskName);
        recyclerViewEditProject = (RecyclerView) view.findViewById(R.id.recycler_project_images_edit);

        tvHeaderTaskFromTime = (TextView) view.findViewById(R.id.label_date_time_from);
        tvHeaderTaskToTime = (TextView) view.findViewById(R.id.label_date_time_to);
        tvTaskFromTime = (TextView) view.findViewById(R.id.tv_date_time_from);
        tvTaskFromTime.setOnClickListener(getPresenter());
        tvTaskToTime = (TextView) view.findViewById(R.id.tv_date_time_to);
        tvTaskToTime.setOnClickListener(getPresenter());
        tvHeaderEventRepeatName = (TextView) view.findViewById(R.id.label_repeat_event);
        tvHeaderRepeatReminderName = (TextView) view.findViewById(R.id.label_repeat_reminder_time);
        tvRepeatReminder = (TextView) view.findViewById(R.id.tv_repeat_reminder_time);
        tvRepeatReminder.setOnClickListener(getPresenter());
        nestedScrollView = (NestedScrollView) view.findViewById(R.id.nested_scroll_view);
        spinnerProject = (AppCompatSpinner) view.findViewById(R.id.spinner_project_name);
        edit_Property = (TextInputEditText) view.findViewById(R.id.spinner_property_name);
        spinnerRepeatEvent = (AppCompatSpinner) view.findViewById(R.id.spinner_repeat_event);
        spinnerTeamMember = (AppCompatSpinner) view.findViewById(R.id.spinner_team);
        etAddNotes = (TextInputEditText) view.findViewById(R.id.et_add_notes);
        etTaskTitle = (TextInputEditText) view.findViewById(R.id.et_task_title);
        ViewTask = view.findViewById(R.id.fragment_Save_view_project);
        EditTask = view.findViewById(R.id.fragment_save_edit_project);

        viewReopenDone = view.findViewById(R.id.rl_reopen_complete);
        tvMarkCompleted = (AppCompatTextView) view.findViewById(R.id.tv_mark_completed);
        tvReopen = (AppCompatTextView) view.findViewById(R.id.tv_reopen);
        tvDone = (AppCompatTextView) view.findViewById(R.id.tv_done);

        tvMarkCompleted.setOnClickListener(getPresenter());
        tvDone.setOnClickListener(getPresenter());
        tvReopen.setOnClickListener(getPresenter());

        // initialize views to view Task detail
        recyclerView_ViewProject = (RecyclerView) view.findViewById(R.id.recycler_project_images_view);
        etTaskTilleView = (TextInputEditText) view.findViewById(R.id.et_task_title_view);
        etAddNotesView = (TextInputEditText) view.findViewById(R.id.et_add_notes_view);
        etDateTimeFromView = (TextView) view.findViewById(R.id.tv_date_time_from_view);
        etDateTimeToView = (TextView) view.findViewById(R.id.tv_date_time_to_view);
        etTeamMemberView = (TextInputEditText) view.findViewById(R.id.et_teamMember_view);
        etPropertyView = (TextInputEditText) view.findViewById(R.id.et_property_view);
        etProjectView = (TextInputEditText) view.findViewById(R.id.et_project_view);
        etRepeatReminderTaskView = (TextInputEditText) view.findViewById(R.id.et_repeatTask_view);
        etReminderDateView = (TextInputEditText) view.findViewById(R.id.et_setReminderDate_view);
        tvNoImages = (TextView) view.findViewById(R.id.txt_Noimages);


        if (getArguments() != null) {
            getPresenter().saveBundleInfo(getArguments());
            currentScreen = getArguments().getString(Constants.DESTINATION, "");
            taskType = getArguments().getInt(Constants.TASK_TYPE);
        }
    }

    @Override
    protected AddEditTaskSpPresenterImpl onAttachPresenter() {
        return new AddEditTaskSpPresenterImpl(this, getActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_event, menu);
        editMenu = menu.findItem(R.id.event_edit);
        saveMenu = menu.findItem(R.id.event_save);
        if (taskType != 2)
            switch (currentScreen) {
                case Constants.CLICK_VIEW_TASK:
                    if (Utils.getInstance().getRole(getActivity()) == 1) {
                        editMenu.setVisible(false);
                        saveMenu.setVisible(false);
                    } else {
                        editMenu.setVisible(true);
                        saveMenu.setVisible(false);
                    }
                    break;
                case Constants.CLICK_EDIT_TASK:
                    editMenu.setVisible(false);
                    saveMenu.setVisible(true);
                    break;
                case Constants.CLICK_ADD_TASK:
                    editMenu.setVisible(false);
                    saveMenu.setVisible(true);

                    break;
            }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.event_edit:
                if (!getPresenter().isTaskRepeat) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String[] options = {"Edit Following Occurrences", "Edit Current Occurrence", "Cancel"};
                    builder.setTitle("Choose Option")
                            .setItems(options, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    switch (which) {
                                        case 0:
                                            editMenu.setVisible(false);
                                            saveMenu.setVisible(true);
                                            toolbarTitle.setText(getString(R.string.title_edit_task));
                                            getPresenter().isOccurrence = false;
                                            getPresenter().hideViewForEditTaskDetail();
                                            break;
                                        case 1:
                                            editMenu.setVisible(false);
                                            saveMenu.setVisible(true);
                                            toolbarTitle.setText(getString(R.string.title_edit_task));
                                            getPresenter().isOccurrence = true;
                                            getPresenter().hideViewForEditTaskDetail();
                                            break;
                                        case 2:
                                            dialog.dismiss();
                                            break;

                                    }
                                }
                            });
                    builder.setCancelable(false);
                    builder.show();
                } else {
                    editMenu.setVisible(false);
                    saveMenu.setVisible(true);
                    toolbarTitle.setText(getString(R.string.title_edit_event));
                    getPresenter().isOccurrence = true;
                    getPresenter().hideViewForEditTaskDetail();
                }
                break;
            case R.id.event_save:
                getPresenter().AddEditTask();

                break;
        }
        return true;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public NestedScrollView getNSView() {
        return nestedScrollView;
    }

    @Override
    public TextInputEditText getTaskTittle() {
        return etTaskTitle;
    }

    @Override
    public TextInputEditText getTaskNotes() {
        return etAddNotes;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public TextView getTVTaskFromTime() {
        return tvTaskFromTime;
    }

    @Override
    public TextView getRepeatReminder() {
        return tvRepeatReminder;
    }


    @Override
    public TextView getRepeatReminderView() {
        return etReminderDateView;
    }

    @Override
    public TextView getEtDateTimeToView() {
        return etDateTimeToView;
    }

    @Override
    public TextView gettvTaskToTime() {
        return tvTaskToTime;
    }

    @Override
    public TextView getEtDateTimeFromView() {
        return etDateTimeFromView;
    }

    @Override
    public TextInputEditText getEdt_PropertyView() {
        return edit_Property;
    }

    @Override
    public AppCompatSpinner getSpinnerTeamMemberView() {
        return spinnerTeamMember;
    }

    @Override
    public AppCompatSpinner getSpinnerProjectView() {
        return spinnerProject;
    }

    @Override
    public AppCompatSpinner getSpinnerRepeatEventView() {
        return spinnerRepeatEvent;
    }

    @Override
    public TextInputEditText getTaskTittleView() {
        return etTaskTilleView;
    }

    @Override
    public TextInputEditText getTaskNotesView() {
        return etAddNotesView;
    }

    @Override
    public TextInputEditText getPropertyView() {
        return etPropertyView;
    }

    @Override
    public TextInputEditText getTeamMemberView() {
        return etTeamMemberView;
    }

    @Override
    public TextInputEditText getProjectView() {
        return etProjectView;
    }

    @Override
    public TextInputEditText getRepeatTaskView() {
        return etRepeatReminderTaskView;
    }

    @Override
    public RecyclerView getViewProjectRecycler() {
        return recyclerView_ViewProject;
    }

    @Override
    public RecyclerView getViewEditProjectRecycler() {
        return recyclerViewEditProject;
    }


    @Override
    public View getViewTask() {
        return ViewTask;
    }

    @Override
    public View getEditTask() {
        return EditTask;
    }

    @Override
    public TextView getNoImages() {
        return tvNoImages;
    }

    @Override
    public AppCompatTextView getTVDoneView() {
        return tvDone;
    }

    @Override
    public AppCompatTextView getTVReopenView() {
        return tvReopen;
    }

    @Override
    public AppCompatTextView getTVMArkCompletedView() {
        return tvMarkCompleted;
    }

    @Override
    public View getReopenDoneView() {
        return viewReopenDone;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getPresenter().onRequestPermissionResult(requestCode, grantResults);
    }


}
