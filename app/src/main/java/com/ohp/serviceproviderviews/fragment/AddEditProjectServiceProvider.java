package com.ohp.serviceproviderviews.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.presenter.AddEditProjectServiceProviderPresenterImpl;
import com.ohp.serviceproviderviews.view.AddEditProjectServiceProviderView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

/**
 * Created by vishal.sharma on 8/14/2017.
 */

public class AddEditProjectServiceProvider extends BaseFragment<AddEditProjectServiceProviderPresenterImpl> implements AddEditProjectServiceProviderView {
    private View rootView;
    private Toolbar mToolbar;
    private TextView toolbarTitle, text_expertise, txt_noProjectImages, tvNoOnGoingTasks, tvNoCompletedTasks;
    private TextInputEditText etPropertyName, etPropertyAddress, etProjectName, etProjectCreatedOn, etProjectJobType, etProjectDescription,
            et_projectName_edit, et_project_expertise, etProjectDescription_edit, et_appliance_name, et_propertyName_edit, et_applianceName_edit, et_mPropertyAddress;
    private AppCompatSpinner mSpinnerJobType;
    private RecyclerView mRecyclerProjectImages, recyclerEditProject, rvOnGoingTasks, rvCompletedTasks;
    private MenuItem editMenu, saveMenu;
    private NestedScrollView nestedScrollView;
    private String currentScreen = "", propertyID = "";
    private ImageView Img_propertyImage;
    private ProgressBar progressBar;
    private MultiSelectSpinner expertiseSpinner;
    private LinearLayout parentLinearLayout;
    private View detailProject;
    private View EditProject;
    private String CurrentScreen;

    @Override
    protected AddEditProjectServiceProviderPresenterImpl onAttachPresenter() {
        return new AddEditProjectServiceProviderPresenterImpl(this, getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_project_sp_main, container, false);
        return rootView;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_project, menu);
        editMenu = menu.findItem(R.id.project_edit);
        saveMenu = menu.findItem(R.id.project_save);
        saveMenu.setVisible(true);
        editMenu.setVisible(false);

        // change this view
        if (editMenu != null && saveMenu != null) {
            switch (currentScreen) {
                case Constants.CLICK_ADD_PROJECT:
                    saveMenu.setVisible(true);
                    editMenu.setVisible(false);
                    break;
                case Constants.CLICK_EDIT_PROJECT:
                    saveMenu.setVisible(true);
                    editMenu.setVisible(false);
                    break;
                case Constants.MY_PROJECTS_SERVICE_PROVIDER_View:
                    saveMenu.setVisible(false);
                    editMenu.setVisible(true);
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.project_save:
                getPresenter().AddEditProject();
                Utils.getInstance().hideSoftKeyboard(getActivity(), rootView);
                break;
            case R.id.project_edit:
                toolbarTitle.setText(R.string.edit_project);
                saveMenu.setVisible(true);
                editMenu.setVisible(false);
                getPresenter().visibilityGoneUiForSaveProject();
                break;
        }
        return true;
    }


    @Override
    protected void initUI(View view) {
        nestedScrollView = (NestedScrollView) view.findViewById(R.id.nested_scroll_view);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        txt_noProjectImages = (TextView) view.findViewById(R.id.txt_Noimages);
        etProjectName = (TextInputEditText) view.findViewById(R.id.et_project_name);
        et_projectName_edit = (TextInputEditText) view.findViewById(R.id.et_project_name_edit);
        et_appliance_name = (TextInputEditText) view.findViewById(R.id.et_appliance_name_view);
        etPropertyName = (TextInputEditText) view.findViewById(R.id.et_property_name);
        etPropertyAddress = (TextInputEditText) view.findViewById(R.id.et_property_address);
        etProjectCreatedOn = (TextInputEditText) view.findViewById(R.id.et_project_created_on);
        etProjectJobType = (TextInputEditText) view.findViewById(R.id.et_project_job_type);
        etProjectDescription = (TextInputEditText) view.findViewById(R.id.et_project_description);
        etProjectDescription_edit = (TextInputEditText) view.findViewById(R.id.etProjectDescription_edit);
        et_project_expertise = (TextInputEditText) view.findViewById(R.id.et_project_expertise);
        Img_propertyImage = (ImageView) view.findViewById(R.id.avatar);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        mSpinnerJobType = (AppCompatSpinner) view.findViewById(R.id.spinner_job_type);
        et_applianceName_edit = (TextInputEditText) view.findViewById(R.id.et_appliance_name_edit);
        expertiseSpinner = (MultiSelectSpinner) view.findViewById(R.id.spinner_expertise);
        et_propertyName_edit = (TextInputEditText) view.findViewById(R.id.et_property_name_edit);
        mRecyclerProjectImages = (RecyclerView) view.findViewById(R.id.recycler_project_images);
        recyclerEditProject = (RecyclerView) view.findViewById(R.id.recycler_project_images_save);
        parentLinearLayout = (LinearLayout) view.findViewById(R.id.detail_top_container);
        detailProject = view.findViewById(R.id.fragment_project_detail);
        EditProject = view.findViewById(R.id.fragment_save_edit_project);
        et_mPropertyAddress = (TextInputEditText) view.findViewById(R.id.et_propertyAddress);
        et_mPropertyAddress.setOnClickListener(getPresenter());

        /*Task list view reference*/
        rvOnGoingTasks = (RecyclerView) view.findViewById(R.id.rv_ongoing_tasks);
        rvCompletedTasks = (RecyclerView) view.findViewById(R.id.rv_completed_tasks);

        tvNoOnGoingTasks = (TextView) view.findViewById(R.id.tv_no_ongoing_tasks);
        tvNoCompletedTasks = (TextView) view.findViewById(R.id.tv_no_completed_tasks);


        if (getArguments() != null) {
            getPresenter().saveBundleInfo(getArguments());
            currentScreen = getArguments().getString(Constants.DESTINATION, "");
            if (getArguments().getString(Constants.PROJECT_ID) != null) {
                String Project_ID = getArguments().getString(Constants.PROJECT_ID);
                getPresenter().getOnGoingObject(Project_ID);
            }
        }

        expertiseSpinner.setListener(getPresenter().expertiseSpinnerListener);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public NestedScrollView getNestedScrollView() {
        return nestedScrollView;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public String getPropertyID() {
        return propertyID;
    }

    @Override
    public ProgressBar getProgressBar() {
        return progressBar;
    }

    @Override
    public ImageView getPropertyImage() {
        return Img_propertyImage;
    }

    @Override
    public MultiSelectSpinner getExpertiseSpinnerView() {
        return expertiseSpinner;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public TextInputEditText getPropertyName() {
        return etPropertyName;
    }

    @Override
    public TextInputEditText getPropertyAddress() {
        return etPropertyAddress;
    }

    @Override
    public TextInputEditText setPropertyAddress() {
        return et_mPropertyAddress;
    }

    @Override
    public TextInputEditText getProjectName() {
        return etProjectName;
    }

    @Override
    public TextInputEditText getProjectCreatedOn() {
        return etProjectCreatedOn;
    }

    @Override
    public TextInputEditText getProjectJopType() {
        return etProjectJobType;
    }

    @Override
    public TextInputEditText getProjectDescriptionEdit() {
        return etProjectDescription_edit;
    }

    @Override
    public TextInputEditText getProjectExpertise() {
        return et_project_expertise;
    }

    @Override
    public TextInputEditText getProjectDescription() {
        return etProjectDescription;
    }

    @Override
    public TextInputEditText getApplianceView() {
        return et_appliance_name;
    }

    @Override
    public TextInputEditText getProjectNameEdit() {
        return et_projectName_edit;
    }


    @Override
    public AppCompatSpinner getJobType() {
        return mSpinnerJobType;
    }

    @Override
    public TextView getExpertise() {
        return text_expertise;
    }

    @Override
    public TextInputEditText getProperty() {
        return et_propertyName_edit;
    }

    @Override
    public TextInputEditText getApplianceName() {
        return et_applianceName_edit;
    }

    @Override
    public View getDetailProject() {
        return detailProject;
    }

    @Override
    public View getEditProject() {
        return EditProject;
    }

    @Override
    public LinearLayout getLayout() {
        return parentLinearLayout;
    }


    @Override
    public RecyclerView getProjectDetailRecycler() {
        return mRecyclerProjectImages;
    }

    @Override
    public RecyclerView getEditProjectDetailRecycler() {
        return recyclerEditProject;
    }


    @Override
    public TextView NoImages() {
        return txt_noProjectImages;
    }

    @Override
    public RecyclerView getRVOnGoingTasks() {
        return rvOnGoingTasks;
    }

    @Override
    public RecyclerView getRVCompletedTasks() {
        return rvCompletedTasks;
    }

    @Override
    public TextView getTVNoOnGoingTasks() {
        return tvNoOnGoingTasks;
    }

    @Override
    public TextView getTVNoCompletedTasks() {
        return tvNoCompletedTasks;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getPresenter().onRequestPermissionResult(requestCode, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }
}
