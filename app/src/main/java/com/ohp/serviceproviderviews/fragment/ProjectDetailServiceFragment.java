package com.ohp.serviceproviderviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.presenter.ProjectDetailServiceFragmentPresenterImpl;
import com.ohp.serviceproviderviews.view.ProjectDetailServiceView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * @author Vishal Sharma.
 */

public class ProjectDetailServiceFragment extends BaseFragment<ProjectDetailServiceFragmentPresenterImpl> implements ProjectDetailServiceView {
    private View view;
    private Toolbar mToolbar;
    private TextView toolbarTitle, txtBid,/* txt_add_notes,*/
            txt_noProjectImages, tvNoApplianceImages, tvNoApplianceDocs, tvMarkCompleted;
    private TextInputEditText etProjectName, etProjectDescription, etProjectCreatedOn, etProjectJobType, etProjectExpertise, etPropertyName, etPropertyDescription,
            etPropertyAddress, etPropertySqFtArea, etPropertyConstYear,
            etApplianceName, etApplianceBrand, etAppliancePurchaseDate, etApplianceExpirationDate, etApplianceWarrantyYrs;
    private RecyclerView recyclerView, mRecyclerProjectImages, rvApplianceImages, rvApplianceDocs;
    private ImageView propertyImage, img_bid, img_edit;
    private TextView propertyOwnerName, propertyOwnerAddress;
    private RelativeLayout linear_addNotes;
    private EditText editTextBidAmount;
    private String projectID = "";
    private ViewPager vpApplianceImg, vpApplianceDocs;
    private CirclePageIndicator indicatorImg, indicatorDocs;
    private MenuItem editMenu, saveMenu;
    private CardView cvApplianceImage, cvApplianceDoc;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_project_detail_service, container, false);
        return view;
    }

    @Override
    protected ProjectDetailServiceFragmentPresenterImpl onAttachPresenter() {
        return new ProjectDetailServiceFragmentPresenterImpl(this, getActivity());
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        txt_noProjectImages = (TextView) view.findViewById(R.id.tv_NoProjectImages);
        tvNoApplianceImages = (TextView) view.findViewById(R.id.tv_no_appliance_images);
        tvNoApplianceDocs = (TextView) view.findViewById(R.id.tv_no_appliance_docs);
        tvMarkCompleted = (TextView) view.findViewById(R.id.tv_mark_completed);
        etPropertyName = (TextInputEditText) view.findViewById(R.id.et_property_name);
        etPropertyAddress = (TextInputEditText) view.findViewById(R.id.et_property_address);
        etPropertyDescription = (TextInputEditText) view.findViewById(R.id.etPropertyDescription_edit);
        etPropertySqFtArea = (TextInputEditText) view.findViewById(R.id.et_property_area);
        etPropertyConstYear = (TextInputEditText) view.findViewById(R.id.et_construction_year);
        etProjectName = (TextInputEditText) view.findViewById(R.id.et_project_name_edit);
        etProjectCreatedOn = (TextInputEditText) view.findViewById(R.id.et_project_created_on);
        etProjectJobType = (TextInputEditText) view.findViewById(R.id.et_project_job_type);
        etProjectExpertise = (TextInputEditText) view.findViewById(R.id.et_project_expertise);
        etProjectDescription = (TextInputEditText) view.findViewById(R.id.et_project_description);
        etApplianceName = (TextInputEditText) view.findViewById(R.id.et_appliance_name);
        etApplianceBrand = (TextInputEditText) view.findViewById(R.id.et_appliance_brand);
        etAppliancePurchaseDate = (TextInputEditText) view.findViewById(R.id.et_purchase_date);
        etApplianceExpirationDate = (TextInputEditText) view.findViewById(R.id.et_warranty_expire_date);
        etApplianceWarrantyYrs = (TextInputEditText) view.findViewById(R.id.et_warranty_years);
        recyclerView = (RecyclerView) view.findViewById(R.id.resource_recycler);
        mRecyclerProjectImages = (RecyclerView) view.findViewById(R.id.recycler_project_images);
        rvApplianceImages = (RecyclerView) view.findViewById(R.id.rv_appliance_images);
        rvApplianceDocs = (RecyclerView) view.findViewById(R.id.rv_appliance_docs);
        propertyImage = (ImageView) view.findViewById(R.id.property_image);
        propertyOwnerName = (TextView) view.findViewById(R.id.txt_property_owner_Name);
        txtBid = (TextView) view.findViewById(R.id.txt_bid);
        img_bid = (ImageView) view.findViewById(R.id.img_bid);
//        txt_add_notes = (TextView) view.findViewById(R.id.txt_add_notes);
//        txt_add_notes.setOnClickListener(getPresenter());
        //   linear_addNotes = (RelativeLayout) view.findViewById(R.id.addNotes);
        editTextBidAmount = (EditText) view.findViewById(R.id.edt_bidAmount);
        img_bid.setOnClickListener(getPresenter());
        img_edit = (ImageView) view.findViewById(R.id.img_edit);
        img_edit.setOnClickListener(getPresenter());
        propertyOwnerAddress = (TextView) view.findViewById(R.id.txt_property_owner_Address);

        vpApplianceImg = (ViewPager) view.findViewById(R.id.viewpager_image);
        vpApplianceDocs = (ViewPager) view.findViewById(R.id.viewpager_docs);
        indicatorImg = (CirclePageIndicator) view.findViewById(R.id.indicator_img);
        indicatorDocs = (CirclePageIndicator) view.findViewById(R.id.indicator_docs);

        cvApplianceDoc = (CardView) view.findViewById(R.id.cv_appliance_doc);
        cvApplianceImage = (CardView) view.findViewById(R.id.cv_appliance_image);

        tvMarkCompleted.setOnClickListener(getPresenter());

        if (getArguments() != null) {
            getPresenter().saveBundleInfo(getArguments());
        }
        if (getArguments().getString(Constants.PROJECT_ID) != null) {
            projectID = getArguments().getString(Constants.PROJECT_ID);
            getPresenter().getProjectIdDetail(projectID);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.profile_save:
                getPresenter().GoneUiForEditBid();
                getPresenter().addEditBid();
                Utils.getInstance().hideSoftKeyboard(getActivity(), view);
                break;
            case R.id.profile_edit:
                toolbarTitle.setText("EDIT BIDS");
                saveMenu.setVisible(true);
                editMenu.setVisible(false);
                getPresenter().GoneUiForSaveBid();
                // getPresenter().visibilityGoneUiForSaveProject();
                break;
        }
        return true;
    }

    @Override
    public RecyclerView getProjectDetailRecycler() {
        return mRecyclerProjectImages;
    }

    @Override
    public RecyclerView getRVApplianceImages() {
        return rvApplianceImages;
    }

    @Override
    public RecyclerView getRVApplianceDocs() {
        return rvApplianceDocs;
    }


    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }


    @Override
    public View getRootView() {
        return view;
    }

    @Override
    public String getProjectId() {
        return projectID;
    }

    @Override
    public TextInputEditText setProjectName() {
        return etProjectName;
    }

    @Override
    public TextInputEditText setProjectDescription() {
        return etProjectDescription;
    }

    @Override
    public TextInputEditText setProjectExpertise() {
        return etProjectExpertise;
    }

    @Override
    public TextInputEditText setProjectCreatedOn() {
        return etProjectCreatedOn;
    }

    @Override
    public TextInputEditText setProjectJobType() {
        return etProjectJobType;
    }

    @Override
    public TextInputEditText setPropertyName() {
        return etPropertyName;
    }

    @Override
    public TextInputEditText setPropertyDescription() {
        return etPropertyDescription;
    }

    @Override
    public TextInputEditText setPropertyAddress() {
        return etPropertyAddress;
    }

    @Override
    public TextInputEditText setPropertySqFtArea() {
        return etPropertySqFtArea;
    }

    @Override
    public TextInputEditText setPropertyConstYear() {
        return etPropertyConstYear;
    }

    @Override
    public TextInputEditText setApplianceName() {
        return etApplianceName;
    }

    @Override
    public TextInputEditText setApplianceBrand() {
        return etApplianceBrand;
    }

    @Override
    public TextInputEditText setAppliancePurchaseDate() {
        return etAppliancePurchaseDate;
    }

    @Override
    public TextInputEditText setApplianceExpirationDate() {
        return etApplianceExpirationDate;
    }

    @Override
    public TextInputEditText setApplianceWarranty() {
        return etApplianceWarrantyYrs;
    }

    @Override
    public RecyclerView getResourceRecycler() {
        return recyclerView;
    }

    @Override
    public ImageView setPropertyImage() {
        return propertyImage;
    }

    @Override
    public TextView setPropertyOwnerName() {
        return propertyOwnerName;
    }

    /*@Override
    public TextView setAddNotes() {
        return txt_add_notes;
    }*/

    @Override
    public TextView NoProjectImages() {
        return txt_noProjectImages;
    }

    @Override
    public TextView setPropertyOwnerAddress() {
        return propertyOwnerAddress;
    }

    @Override
    public TextView setBidText() {
        return txtBid;
    }

    @Override
    public ImageView hitBidAmount() {
        return img_bid;
    }

    @Override
    public RelativeLayout setLinearAddNotes() {
        return linear_addNotes;
    }

    @Override
    public EditText setBidAmount() {
        return editTextBidAmount;
    }

    @Override
    public ImageView getImageEdit() {
        return img_edit;
    }

    @Override
    public ViewPager getImageViewPager() {
        return vpApplianceImg;
    }

    @Override
    public ViewPager getDocsViewPager() {
        return vpApplianceDocs;
    }

    @Override
    public CirclePageIndicator getImgIndicator() {
        return indicatorImg;
    }

    @Override
    public CirclePageIndicator getDocsIndicator() {
        return indicatorDocs;
    }

    @Override
    public CardView getCVApplianceDocView() {
        return cvApplianceDoc;
    }

    @Override
    public CardView getCVApplianceImageView() {
        return cvApplianceImage;
    }

    @Override
    public TextView getTVNoApplianceDocs() {
        return tvNoApplianceDocs;
    }

    @Override
    public TextView getTVNoApplianceImages() {
        return tvNoApplianceImages;
    }

    @Override
    public TextView getTVMarkCompletedProject() {
        return tvMarkCompleted;
    }
}
