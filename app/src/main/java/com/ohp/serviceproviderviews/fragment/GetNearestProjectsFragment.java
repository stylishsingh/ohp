package com.ohp.serviceproviderviews.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.presenter.GetNearestProjectsFragmentPresenterImpl;
import com.ohp.serviceproviderviews.view.GetNearestProjectView;

/**
 * @author Vishal Sharma.
 */

public class GetNearestProjectsFragment extends BaseFragment<GetNearestProjectsFragmentPresenterImpl> implements GetNearestProjectView {
    private View view;
    private RecyclerView recyclerView;
    private RelativeLayout relativeLayout;
    private EditText etSearch;
    private LinearLayout btnClear;
    private NestedScrollView nsRecyclerView;

    @Override
    protected GetNearestProjectsFragmentPresenterImpl onAttachPresenter() {
        return new GetNearestProjectsFragmentPresenterImpl(this, getActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_nearest_projects, container, false);
        return view;
    }

    @Override
    protected void initUI(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_list_projects);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.empty_list_layout);
        nsRecyclerView = (NestedScrollView) view.findViewById(R.id.ns_recycler_view);
        etSearch = (EditText) view.findViewById(R.id.et_search);
        btnClear = (LinearLayout) view.findViewById(R.id.btn_clear);
        btnClear.setOnClickListener(getPresenter());
    }

    @Override
    public View getRootView() {
        return view;
    }


    @Override
    public RecyclerView viewProjectsList() {
        return recyclerView;
    }

    @Override
    public RelativeLayout setEmptyLayout() {
        return relativeLayout;
    }


    @Override
    public NestedScrollView getNSRecyclerView() {
        return nsRecyclerView;
    }

    @Override
    public EditText getSearchEditText() {
        return etSearch;
    }

    @Override
    public LinearLayout getCloseBtn() {
        return btnClear;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        getPresenter().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }
}
