package com.ohp.serviceproviderviews.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.beans.TeamMembersModel;
import com.ohp.serviceproviderviews.presenter.AddEditTeamMemberPresenterImpl;
import com.ohp.serviceproviderviews.view.AddEditTeamMemberView;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

import static com.ohp.utils.Constants.CLICK_ADD_TEAM;
import static com.ohp.utils.Constants.CLICK_EDIT_TEAM;
import static com.ohp.utils.Constants.CLICK_VIEW_TEAM;

/**
 * @author vishal.sharma
 */

public class AddEditTeamMemberFragment extends BaseFragment<AddEditTeamMemberPresenterImpl> implements AddEditTeamMemberView {
    public MenuItem editMenu, saveMenu;
    private View view;
    private Toolbar mToolbar;
    private TextView toolbarTitle;
    private NestedScrollView nestedScrollView;
    private TextInputEditText txt_mMemberName, txt_mMemberEmail, txt_mMemberAddress;
    private TextInputLayout txt_memberNamehint, txt_memberEmailHint, txt_memberAddressHint;
    private MultiSelectSpinner mMemberExpertise;
    private RadioGroup radioGroup;
    private RadioButton teamMemberRadio, contractorRadio;
    private RecyclerView providerRecyclerView;
    private String currentScreen = "", provider_id = "";
    private TeamMembersModel.DataBean dataObj;
    private TextView txt_expertisehint;

    @Override
    protected AddEditTeamMemberPresenterImpl onAttachPresenter() {
        return new AddEditTeamMemberPresenterImpl(this, getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_profile, menu);
        editMenu = menu.findItem(R.id.profile_edit);
        saveMenu = menu.findItem(R.id.profile_save);
        switch (currentScreen) {
            case CLICK_VIEW_TEAM:
                saveMenu.setVisible(false);
                editMenu.setVisible(true);
                break;
            case CLICK_EDIT_TEAM:
                saveMenu.setVisible(true);
                editMenu.setVisible(false);
                break;
            case CLICK_ADD_TEAM:
                saveMenu.setVisible(true);
                editMenu.setVisible(false);
                break;

        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        editMenu = menu.findItem(R.id.profile_edit);
        saveMenu = menu.findItem(R.id.profile_save);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Utils.getInstance().hideSoftKeyboard(getActivity(), view);
        switch (item.getItemId()) {
            case R.id.profile_edit:
                toolbarTitle.setText("Edit Team Member");
                txt_memberNamehint.setHint("Enter Member Name");
                txt_memberEmailHint.setHint("Invite By Email");
                txt_memberAddressHint.setHint("Enter Address");
                txt_expertisehint.setText(R.string.title_select_expertise);
                txt_memberNamehint.setFocusable(true);
                txt_memberNamehint.setFocusableInTouchMode(true);
                txt_memberNamehint.requestFocus();
                txt_expertisehint.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                saveMenu.setVisible(true);
                editMenu.setVisible(false);
                getPresenter().setViewVisibleForEdit();
                break;
            case R.id.profile_save:
                if (currentScreen.equals(Constants.CLICK_EDIT_TEAM) || currentScreen.equals(Constants.CLICK_VIEW_TEAM)) {
                    getPresenter().EditTeamMember();
                } else {
                    getPresenter().AddTeamMember();
                }
                Utils.getInstance().hideSoftKeyboard(getActivity(), view);
                break;
        }
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_edit_team_member, container, false);
        return view;
    }

    @Override
    protected void initUI(View view) {
        txt_expertisehint = (TextView) view.findViewById(R.id.txt_expertise);
        nestedScrollView = (NestedScrollView) view.findViewById(R.id.nested_scroll_view);
        txt_mMemberName = (TextInputEditText) view.findViewById(R.id.et_memberName);
        txt_memberNamehint = (TextInputLayout) view.findViewById(R.id.til_memberName);
        txt_memberEmailHint = (TextInputLayout) view.findViewById(R.id.til_member_email);
        txt_memberAddressHint = (TextInputLayout) view.findViewById(R.id.til_member_address);
        txt_mMemberEmail = (TextInputEditText) view.findViewById(R.id.et_memberEmail);
        txt_mMemberAddress = (TextInputEditText) view.findViewById(R.id.et_memberAddress);
        txt_mMemberAddress.setOnClickListener(getPresenter());
        mMemberExpertise = (MultiSelectSpinner) view.findViewById(R.id.spinner_expertise);
        mMemberExpertise.setListener(getPresenter().expertiseSpinnerListener);
        radioGroup = (RadioGroup) view.findViewById(R.id.radio_group);
        teamMemberRadio = (RadioButton) view.findViewById(R.id.radio_teamMember);
        contractorRadio = (RadioButton) view.findViewById(R.id.radio_contractor);
        providerRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        if (getArguments() != null) {
            getPresenter().saveBundleInfo(getArguments());
            currentScreen = getArguments().getString(Constants.DESTINATION, "");
            if (getArguments().getString(Constants.MEMBER_ID) != null) {
                provider_id = getArguments().getString(Constants.MEMBER_ID);
                dataObj = getArguments().getParcelable(Constants.TEAM_MEMBER_OBJ);
                getPresenter().setData(dataObj);

                //   getPresenter().getOnGoingObject(Project_ID);
                // getProjectDetail(project_id);
            }

        }
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }


    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public TextView getExpertiseHint() {
        return txt_expertisehint;
    }

    @Override
    public View getRootView() {
        return view;
    }

    @Override
    public NestedScrollView getNestedScrollView() {
        return nestedScrollView;
    }

    @Override
    public TextInputEditText getMemberName() {
        return txt_mMemberName;
    }

    @Override
    public TextInputEditText getMemberAddress() {
        return txt_mMemberAddress;
    }

    @Override
    public TextInputEditText getMemberEmail() {
        return txt_mMemberEmail;
    }

    @Override
    public TextInputLayout getMemberHint() {
        return txt_memberNamehint;
    }

    @Override
    public TextInputLayout getMemberEmailHint() {
        return txt_memberEmailHint;
    }

    @Override
    public TextInputLayout getMemberAddressHint() {
        return txt_memberAddressHint;
    }

    @Override
    public MultiSelectSpinner getExpertiseSpinnerView() {
        return mMemberExpertise;
    }

    @Override
    public RadioGroup getRadioGroup() {
        return radioGroup;
    }

    @Override
    public RadioButton getTeamMemberRadio() {
        return teamMemberRadio;
    }

    @Override
    public RadioButton getContractorRadio() {
        return contractorRadio;
    }

    @Override
    public RecyclerView getProviderList() {
        return providerRecyclerView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }


}
