package com.ohp.serviceproviderviews.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.JobTypeAndExpertise;
import com.ohp.dataHolder.DataHolder;
import com.ohp.enums.ApiName;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Response;

import static com.ohp.enums.ApiName.GET_RESOURCES_API;

/**
 * @author Amanpal Singh.
 */

public class ProviderFilterDialogFragment extends DialogFragment implements View.OnClickListener, APIResponseInterface.OnCallableResponse, RadioGroup.OnCheckedChangeListener {
    private static ProviderFilterDialogFragment instance;
    private View rootView;
    private MultiSelectSpinner spinnerExpertise;
    private RatingBar ratingProvider;
    private List<JobTypeAndExpertise.DataBean.InfoBean> listExpertise = new ArrayList<>();
    private ArrayAdapter expertiseAdapter;
    private boolean[] arraySelectionExpertise;
    private String[] detailExpertise;
    private String mSelectedExpertise = "", selectedExpertiseName = "", expertiseName = "",
            distance = "", rating = "", licensed = "0";

    public MultiSelectSpinner.MultiSpinnerListener expertiseSpinnerListener = new MultiSelectSpinner.MultiSpinnerListener() {

        public void onItemsSelected(boolean[] selected) {
            mSelectedExpertise = "";
            expertiseName = "";
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    if (mSelectedExpertise.trim().isEmpty()) {
                        mSelectedExpertise = String.valueOf(listExpertise.get(i).getId());
                        expertiseName = listExpertise.get(i).getValue();
                    } else {
                        mSelectedExpertise = mSelectedExpertise + "," + String.valueOf(listExpertise.get(i).getId());
                        expertiseName = expertiseName + "," + listExpertise.get(i).getValue();
                    }
                }
            }

            if (getActivity() != null) {
                if (!mSelectedExpertise.isEmpty()) {
                    if (spinnerExpertise.isSelectAll())
                        spinnerExpertise.setAllCheckedText(expertiseName);
                } else {
                    spinnerExpertise.setAllCheckedText("Select Expertise");
                }
            }
        }
    };
    private TextInputEditText etDistance;
    private TextView btnCancel, btnOK, btnResetFilter;
    private RadioGroup licenseRadioGroup;
    //    private GetProjectsRequestBean requestObj;
    private RadioButton licenseYesRadio, licenseNoRadio;

    public static ProviderFilterDialogFragment getInstance() {
        return instance == null ? new ProviderFilterDialogFragment() : instance;
    }

    public synchronized static void clearInstance() {
        instance = null;
    }

    public static DisplayMetrics getDeviceMetrics(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        display.getMetrics(metrics);
        return metrics;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_filter_provider, container, false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getDialog().getWindow() != null)
            getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setValues();
        getExpertise();
    }

    private void setValues() {
        if (getActivity() != null && isAdded()) {
            rating = DataHolder.getInstance().getRating();
            licensed = DataHolder.getInstance().getLicensed();
            mSelectedExpertise = DataHolder.getInstance().getExpertise();
            distance = DataHolder.getInstance().getDistance();

            if (!distance.isEmpty())
                etDistance.setText(distance);
            if (!rating.isEmpty())
                ratingProvider.setRating(Float.parseFloat(rating));

            if (!mSelectedExpertise.isEmpty()) {
                detailExpertise = mSelectedExpertise.split(",");
            }

            if (!licensed.isEmpty()) {
                if (licensed.equalsIgnoreCase("1")) {
                    licenseYesRadio.setChecked(true);
                    licenseNoRadio.setChecked(false);
                } else {
                    licenseYesRadio.setChecked(false);
                    licenseNoRadio.setChecked(true);
                }
            }
        }
    }

    private APIHandler getInterActor() {
        return new APIHandler(this);
    }

    private void initViews() {
        btnResetFilter = (TextView) rootView.findViewById(R.id.btn_reset_filter);
        btnCancel = (TextView) rootView.findViewById(R.id.btn_cancel);
        btnOK = (TextView) rootView.findViewById(R.id.btn_ok);
        etDistance = (TextInputEditText) rootView.findViewById(R.id.et_distance);
        spinnerExpertise = (MultiSelectSpinner) rootView.findViewById(R.id.spinner_expertise);

        ratingProvider = (RatingBar) rootView.findViewById(R.id.rating);

        licenseRadioGroup = (RadioGroup) rootView.findViewById(R.id.radio_group_license);

        licenseYesRadio = (RadioButton) rootView.findViewById(R.id.radio_license_yes);
        licenseNoRadio = (RadioButton) rootView.findViewById(R.id.radio_license_no);

        spinnerExpertise.setListener(expertiseSpinnerListener);
        ratingProvider.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ProviderFilterDialogFragment.this.rating = String.valueOf(rating);
            }
        });
        licenseRadioGroup.setOnCheckedChangeListener(this);
        btnOK.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                /*DataHolder.getInstance().setRating("");
                DataHolder.getInstance().setDistance("");
                DataHolder.getInstance().setExpertise("");
                DataHolder.getInstance().setLicensed("");*/
                Utils.getInstance().hideSoftKeyboard(getActivity(), rootView);
                dismiss();
                break;
            case R.id.btn_ok:
                if (DataHolder.getInstance() != null && isAdded()) {
                    distance = etDistance.getText().toString();
                    DataHolder.getInstance().setRating(rating);
                    DataHolder.getInstance().setDistance(distance);
                    DataHolder.getInstance().setExpertise(mSelectedExpertise);
                    DataHolder.getInstance().setLicensed(licensed);
                    Intent listIntent = new Intent(Constants.BROWSE_SERVICE_PROVIDER);
                    listIntent.putExtra(Constants.FILTER_DISTANCE, distance);
                    listIntent.putExtra(Constants.FILTER_RATING, rating);
                    listIntent.putExtra(Constants.FILTER_LICENSED, licensed);
                    listIntent.putExtra(Constants.FILTER_EXPERTISE, mSelectedExpertise);
                    getActivity().sendBroadcast(listIntent);
                    Utils.getInstance().hideSoftKeyboard(getActivity(), rootView);
                }
                dismiss();
                break;

        }
    }

    private void getExpertise() {
        if (getActivity() != null && isAdded()) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
//                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.TYPE, "jobtype_expertise");
                getInterActor().getResourcesJobAndExpertise(param, GET_RESOURCES_API);
            } else {
                Snackbar.make(rootView, getActivity().getString(R.string.error_internet), Snackbar.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isAdded()) {
//                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_RESOURCES_API:
                        if (response.isSuccessful()) {
                            JobTypeAndExpertise resourcesObj = (JobTypeAndExpertise) response.body();
                            switch (resourcesObj.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(resourcesObj);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(resourcesObj.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null) {
                if (t != null && t.getMessage() != null)
                    Utils.getInstance().showToast(t.getMessage());
                else
                    Utils.getInstance().showToast(getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(JobTypeAndExpertise resourcesObj) {
        try {
            if (getActivity() != null && isAdded()) {

                listExpertise = new ArrayList<>();
                if (!listExpertise.isEmpty())
                    listExpertise.clear();

                List<String> listExpert = new ArrayList<>();

                listExpertise.addAll(resourcesObj.getData().get(1).get(0).getInfo());
                for (int i = 0; i < listExpertise.size(); i++) {
                    listExpert.add(listExpertise.get(i).getValue());
                }

                expertiseAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_multiple_choice, listExpert);
                spinnerExpertise.setListAdapter(expertiseAdapter).setAllCheckedText(selectedExpertiseName);

                if (!listExpertise.isEmpty()) {
                    arraySelectionExpertise = new boolean[listExpertise.size()];
                    for (int j = 0; j < listExpertise.size(); j++) {
                        if (detailExpertise != null) {
                            for (int i = 0; i < detailExpertise.length; i++) {
                                if (detailExpertise[i].equals(String.valueOf(listExpertise.get(j).getId()))) {
                                    arraySelectionExpertise[j] = true;
                                    if (expertiseName.trim().isEmpty())
                                        expertiseName = String.valueOf(listExpertise.get(j).getValue());
                                    else
                                        expertiseName = expertiseName + "," + String.valueOf(listExpertise.get(j).getValue());
                                    /*if (mSelectedExpertise.trim().isEmpty())
                                        mSelectedExpertise = String.valueOf(listExpertise.get(j).getId());
                                    else
                                        mSelectedExpertise = mSelectedExpertise + "," + String.valueOf(listExpertise.get(j).getId());*/
                                }
                            }
                        }

                    }
                    spinnerExpertise.setAllCheckedText(expertiseName);
                    for (int k = 0; k < arraySelectionExpertise.length; k++) {
                        System.out.println(k + "K----boolean" + arraySelectionExpertise[k]);
                        spinnerExpertise.selectItem(k, arraySelectionExpertise[k]);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.radio_license_yes:
                licenseYesRadio.setChecked(true);
                licenseNoRadio.setChecked(false);
                licensed = "1";
                break;
            case R.id.radio_license_no:
                licenseYesRadio.setChecked(false);
                licenseNoRadio.setChecked(true);
                licensed = "0";
                break;
        }
    }
}