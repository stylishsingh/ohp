package com.ohp.serviceproviderviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.presenter.SpEventListFragmentPresenterImpl;
import com.ohp.serviceproviderviews.view.SpEventListView;
import com.ohp.utils.Constants;
import com.roomorama.caldroid.CaldroidFragment;

/**
 * @author Amanpal Singh.
 */

public class SpEventListFragment extends BaseFragment<SpEventListFragmentPresenterImpl> implements SpEventListView {
    public LinearLayout calendar1;
    private View rootView;
    private CaldroidFragment caldroidFragment;
    private FloatingActionButton addEvent;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private View rlEmptyLayout;
    private TextView tvToday, tvTimeFrom, tvTimeFromTo, tvRepeat, tvEventTitle, tvNotes, tvPropertyName, tvProjectName;
    private CardView cvCurrentDate;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private String navigateFrom = "", propertyId = "";


    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
    }

    @Override
    protected SpEventListFragmentPresenterImpl onAttachPresenter() {
        return new SpEventListFragmentPresenterImpl(this, getActivity());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_filter_projects:
                showPopup();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showPopup() {
        try {
            PopupMenu popup;

            popup = new PopupMenu(getActivity(), getActivity().findViewById(R.id.item_filter));
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.menu_event_list, popup.getMenu());
            popup.getMenu().findItem(R.id.action_my_event).setChecked(false);
            popup.getMenu().findItem(R.id.action_assigned_event).setChecked(false);
            popup.getMenu().findItem(R.id.action_all_event).setChecked(false);
            switch (getPresenter().eventType) {
                case "my":
                    popup.getMenu().findItem(R.id.action_my_event).setChecked(true);
                    break;
                case "assigned":
                    popup.getMenu().findItem(R.id.action_assigned_event).setChecked(true);
                    break;
                case "":
                    popup.getMenu().findItem(R.id.action_all_event).setChecked(true);
                    break;
            }
            popup.show();
            popup.setOnMenuItemClickListener(getPresenter());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void initUI(View view) {
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        calendar1 = (LinearLayout) view.findViewById(R.id.calendar1);
        cvCurrentDate = (CardView) view.findViewById(R.id.cv_current_date);
        addEvent = (FloatingActionButton) view.findViewById(R.id.fab_add);
        tvToday = (TextView) view.findViewById(R.id.tv_today);
        tvTimeFrom = (TextView) view.findViewById(R.id.tv_time_from);
        tvPropertyName = (TextView) view.findViewById(R.id.tv_property_name);
        tvProjectName = (TextView) view.findViewById(R.id.tv_project_name);
        tvTimeFromTo = (TextView) view.findViewById(R.id.tv_time_from_to);
        tvEventTitle = (TextView) view.findViewById(R.id.tv_event_title);
        tvRepeat = (TextView) view.findViewById(R.id.tv_repeat);
        tvNotes = (TextView) view.findViewById(R.id.tv_notes);
        tvToday = (TextView) view.findViewById(R.id.tv_today);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        rlEmptyLayout = view.findViewById(R.id.empty_list_layout);
        addEvent.setOnClickListener(getPresenter());
        caldroidFragment = new CaldroidFragment();
        caldroidFragment.setCaldroidListener(getPresenter().listener);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        getRecyclerView().setAdapter(getPresenter().initAdapter());
        getCVCurrentDateView().setOnClickListener(getPresenter());
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            navigateFrom = bundle.getString(Constants.DESTINATION, "");
            if (navigateFrom.equalsIgnoreCase(Constants.ADD_EDIT_PROPERTY)) {
                toolbar.setVisibility(View.VISIBLE);
                propertyId = bundle.getString(Constants.PROPERTY_ID, "");
                getPresenter().setToolbar();
            } else
                toolbar.setVisibility(View.GONE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_owner_events, container, false);
        return rootView;
    }


    @Override
    public void setCaldroid(CaldroidFragment fragment) {
        caldroidFragment = fragment;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public CaldroidFragment getCaldroidFragmentView() {
        return caldroidFragment;
    }

    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public LinearLayoutManager getLinearLayoutManagerView() {
        return linearLayoutManager;
    }

    @Override
    public View getEmptyView() {
        return rlEmptyLayout;
    }

    @Override
    public TextView getTVTodayView() {
        return tvToday;
    }

    @Override
    public TextView getTVTimeFromView() {
        return tvTimeFrom;
    }

    @Override
    public TextView getTVTimeFromToView() {
        return tvTimeFromTo;
    }

    @Override
    public TextView getTVEventTitleView() {
        return tvEventTitle;
    }

    @Override
    public TextView getTVNotesView() {
        return tvNotes;
    }

    @Override
    public TextView getTVRepeatView() {
        return tvRepeat;
    }

    @Override
    public TextView getTVPropertyNameView() {
        return tvPropertyName;
    }

    @Override
    public TextView getTVProjectNameView() {
        return tvProjectName;
    }

    @Override
    public CardView getCVCurrentDateView() {
        return cvCurrentDate;
    }

    @Override
    public String getNavigateFrom() {
        return navigateFrom;
    }

    @Override
    public String getPropertyID() {
        return propertyId;
    }

    @Override
    public Toolbar getToolbarView() {
        return toolbar;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public LinearLayout getLLCalendarView() {
        return calendar1;
    }
}
