package com.ohp.serviceproviderviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.presenter.SpDashboardFragmentPresenterImpl;
import com.ohp.serviceproviderviews.view.SpDashboardFragmentView;
import com.ohp.utils.CustomViewPager;

/**
 * @author Amanpal Singh.
 */

public class SpDashboardFragment extends BaseFragment<SpDashboardFragmentPresenterImpl> implements SpDashboardFragmentView {

    private View rootView;
    private TabLayout tabLayout;
    private CustomViewPager viewPager;
    private MenuItem itemFilter;

    @Override
    protected SpDashboardFragmentPresenterImpl onAttachPresenter() {
        return new SpDashboardFragmentPresenterImpl(this, getActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sp_dashboard, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (itemFilter != null) {
                    Fragment fragment = getPresenter().pagerAdapter.getItem(viewPager.getCurrentItem());

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void initUI(View view) {

        //Initializing the tabLayout
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        //Initializing getViewPager
        viewPager = (CustomViewPager) view.findViewById(R.id.pager);

        //Adding onTabSelectedListener to swipe views
//        tabLayout.addOnTabSelectedListener(getPresenter());
//        viewPager.addOnPageChangeListener(getPresenter());
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(4);
//        viewPager.setPagingEnabled(false);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_dashboard, menu);
        itemFilter = menu.findItem(R.id.item_filter);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        System.out.println("Click>>" + viewPager.getCurrentItem());
        Fragment fragment = getPresenter().pagerAdapter.getItem(viewPager.getCurrentItem());
        if (fragment instanceof TaskFragment) {
            ((TaskFragment) fragment).showPopup();
        } else if (fragment instanceof SpEventListFragment) {
            ((SpEventListFragment) fragment).showPopup();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public CustomViewPager getViewPager() {
        return viewPager;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

}
