package com.ohp.serviceproviderviews.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.fragment.BaseFragment;
import com.ohp.serviceproviderviews.presenter.SPProfileFragmentPresenterImpl;
import com.ohp.serviceproviderviews.view.SPProfileFragmentView;
import com.ohp.utils.Utils;

import de.hdodenhof.circleimageview.CircleImageView;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

/**
 * @author Amanpal Singh.
 */

public class SPProfileFragment extends BaseFragment<SPProfileFragmentPresenterImpl> implements SPProfileFragmentView {
    public MenuItem editMenu, saveMenu;
    private View rootView;
    private NestedScrollView profileNestedScroll;
    private FloatingActionButton editProfile;
    private CircleImageView profileImg;
    private ProgressBar progressBar;
    private TextInputEditText etName, etEmail, etCompany, etLicenseNumber, etWebsite;
    private TextInputEditText etAddress;
    private RadioGroup genderRadioGroup, licenseRadioGroup, bondedRadioGroup, insuredRadioGroup;
    private RadioButton maleRadio, femaleRadio, licenseYesRadio, licenseNoRadio, bondedYesRadio, bondedNoRadio, insuredYesRadio, insuredNoRadio;
    private TextInputLayout tilFullName, tilLicensedNumber;
    private TextView tvGender, tvLicensed, tvInsured, tvBonded, tvExpertise;
    private View viewLineExpertise;
    private boolean isFirstTime;
    private MultiSelectSpinner spinnerExpertise;

    @Override
    protected SPProfileFragmentPresenterImpl onAttachPresenter() {
        return new SPProfileFragmentPresenterImpl(this, getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_profile, menu);
        editMenu = menu.findItem(R.id.profile_edit);
        saveMenu = menu.findItem(R.id.profile_save);
//        if (isFirstTime) {
        editMenu.setVisible(false);
        saveMenu.setVisible(true);
//        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        editMenu = menu.findItem(R.id.profile_edit);
        saveMenu = menu.findItem(R.id.profile_save);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Utils.getInstance().hideSoftKeyboard(getActivity(), rootView);
        switch (item.getItemId()) {
            case R.id.profile_edit:
                saveMenu.setVisible(true);
                editMenu.setVisible(false);
                getPresenter().enableDisableUI(true);
                break;
            case R.id.profile_save:
                if (getPresenter().saveProfileInfo()) {
                    getPresenter().enableDisableUI(false);
                    saveMenu.setVisible(false);
                    editMenu.setVisible(true);
                }
                break;
        }
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sp_profile, container, false);
        return rootView;
    }

    @Override
    protected void initUI(View view) {
        profileNestedScroll = (NestedScrollView) view.findViewById(R.id.profile_nested_scroll);
        profileImg = (CircleImageView) view.findViewById(R.id.profile_img);
        editProfile = (FloatingActionButton) view.findViewById(R.id.edit_profile);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        tilFullName = (TextInputLayout) view.findViewById(R.id.til_name);
        etName = (TextInputEditText) view.findViewById(R.id.et_name);
        etEmail = (TextInputEditText) view.findViewById(R.id.et_email);
        etAddress = (TextInputEditText) view.findViewById(R.id.et_address);
        genderRadioGroup = (RadioGroup) view.findViewById(R.id.radio_group);
        maleRadio = (RadioButton) view.findViewById(R.id.radio_male);
        femaleRadio = (RadioButton) view.findViewById(R.id.radio_female);

        licenseRadioGroup = (RadioGroup) view.findViewById(R.id.radio_group_license);
        licenseYesRadio = (RadioButton) view.findViewById(R.id.radio_license_yes);
        licenseNoRadio = (RadioButton) view.findViewById(R.id.radio_license_no);

        bondedRadioGroup = (RadioGroup) view.findViewById(R.id.radio_group_bonded);
        bondedYesRadio = (RadioButton) view.findViewById(R.id.radio_bonded_yes);
        bondedNoRadio = (RadioButton) view.findViewById(R.id.radio_bonded_no);

        insuredRadioGroup = (RadioGroup) view.findViewById(R.id.radio_group_insured);
        insuredYesRadio = (RadioButton) view.findViewById(R.id.radio_insured_yes);
        insuredNoRadio = (RadioButton) view.findViewById(R.id.radio_insured_no);

        etCompany = (TextInputEditText) view.findViewById(R.id.et_company);
        etLicenseNumber = (TextInputEditText) view.findViewById(R.id.et_license_number);
        etWebsite = (TextInputEditText) view.findViewById(R.id.et_website);

        tvGender = (TextView) view.findViewById(R.id.tv_gender);
        tvBonded = (TextView) view.findViewById(R.id.tv_bonded);
        tvExpertise = (TextView) view.findViewById(R.id.tv_expertise_name);
        tvInsured = (TextView) view.findViewById(R.id.tv_insured);
        tvLicensed = (TextView) view.findViewById(R.id.tv_licensed);

        tilLicensedNumber = (TextInputLayout) view.findViewById(R.id.til_license_number);

        viewLineExpertise = view.findViewById(R.id.view_line_expertise);
        spinnerExpertise = (MultiSelectSpinner) view.findViewById(R.id.spinner_expertise);
        spinnerExpertise.setListener(getPresenter().expertiseSpinnerListener);

        editProfile.setOnClickListener(getPresenter());
        etAddress.setOnClickListener(getPresenter());
        profileImg.setOnClickListener(getPresenter());

        if (getArguments() != null) {
            Bundle bundle = getArguments();
            getPresenter().saveBundleInfo(bundle);
            isFirstTime = true;
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        getPresenter().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public NestedScrollView getScrollView() {
        return profileNestedScroll;
    }

    @Override
    public CircleImageView getProfileImg() {
        return profileImg;
    }

    @Override
    public FloatingActionButton getEditProfile() {
        return editProfile;
    }

    @Override
    public ProgressBar getProfileProgress() {
        return progressBar;
    }

    @Override
    public TextInputEditText getName() {
        return etName;
    }

    @Override
    public TextInputEditText getEmail() {
        return etEmail;
    }

    @Override
    public TextInputEditText getAddress() {
        return etAddress;
    }

    @Override
    public RadioGroup getGenderRadioGroup() {
        return genderRadioGroup;
    }

    @Override
    public RadioButton getMaleRadio() {
        return maleRadio;
    }

    @Override
    public RadioButton getFemaleRadio() {
        return femaleRadio;
    }

    @Override
    public TextInputEditText getCompany() {
        return etCompany;
    }

    @Override
    public RadioGroup getLicenseRadioGroup() {
        return licenseRadioGroup;
    }

    @Override
    public RadioButton getLicenseYesRadio() {
        return licenseYesRadio;
    }

    @Override
    public RadioButton getLicenseNoRadio() {
        return licenseNoRadio;
    }

    @Override
    public RadioGroup getBondedRadioGroup() {
        return bondedRadioGroup;
    }

    @Override
    public RadioButton getBondedYesRadio() {
        return bondedYesRadio;
    }

    @Override
    public RadioButton getBondedNoRadio() {
        return bondedNoRadio;
    }

    @Override
    public RadioGroup getInsuredRadioGroup() {
        return insuredRadioGroup;
    }

    @Override
    public RadioButton getInsuredYesRadio() {
        return insuredYesRadio;
    }

    @Override
    public RadioButton getInsuredNoRadio() {
        return insuredNoRadio;
    }

    @Override
    public TextInputEditText getLicenseNumber() {
        return etLicenseNumber;
    }

    @Override
    public TextInputEditText getWebsite() {
        return etWebsite;
    }

    @Override
    public MenuItem getEditMenu() {
        return editMenu;
    }

    @Override
    public MenuItem getSaveMenu() {
        return saveMenu;
    }

    @Override
    public MultiSelectSpinner getExpertiseSpinnerView() {
        return spinnerExpertise;
    }

    @Override
    public TextInputLayout getFullNameLayoutView() {
        return tilFullName;
    }

    @Override
    public TextInputLayout getLicenseNumberLayoutView() {
        return tilLicensedNumber;
    }

    @Override
    public TextView getTVGenderView() {
        return tvGender;
    }

    @Override
    public TextView getTVLicensedView() {
        return tvLicensed;
    }

    @Override
    public TextView getTVInsuredView() {
        return tvInsured;
    }

    @Override
    public TextView getTVBondedView() {
        return tvBonded;
    }

    @Override
    public TextView getTVExpertiseView() {
        return tvExpertise;
    }

    @Override
    public View getLineExpertiseView() {
        return viewLineExpertise;
    }

}
