package com.ohp.serviceproviderviews.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;
import com.ohp.R;
import com.ohp.commonviews.activities.BaseActivity;
import com.ohp.serviceproviderviews.presenter.SpDashboardActivityPresenterImpl;
import com.ohp.serviceproviderviews.view.SpDashboardView;
import com.ohp.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * @author Amanpal Singh.
 */

public class SPDashboardActivity extends BaseActivity<SpDashboardActivityPresenterImpl> implements SpDashboardView {

    public static String sort_order = "DESC";
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;
    private CircleImageView profileImg;
    private TextView toolbarTitle, tvName;
    private LinearLayout profileLayout;

    @Override
    protected SpDashboardActivityPresenterImpl createPresenter() {
        return new SpDashboardActivityPresenterImpl(this, this);
    }

    @Override
    protected void initUI() {
        /*Subscribe topic to Firebase.*/
        if (!TextUtils.isEmpty(Utils.getInstance().getTopic(this)))
            FirebaseMessaging.getInstance().subscribeToTopic(Utils.getInstance().getTopic(this));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("");
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = LayoutInflater.from(this).inflate(R.layout.nav_header_dashboard, null);
        navigationView.addHeaderView(header);
        profileLayout = (LinearLayout) header.findViewById(R.id.header_layout);
        tvName = (TextView) header.findViewById(R.id.tv_name);
        profileImg = (CircleImageView) header.findViewById(R.id.profile_img);

        navigationView.setItemIconTintList(null);
        drawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                hideKeyboard();
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerToggle.setHomeAsUpIndicator(R.drawable.ic_hamburger);
        drawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getDrawerView().isDrawerOpen(GravityCompat.START)) {
                    getDrawerView().closeDrawer(GravityCompat.START);
                } else {
                    hideKeyboard();
                    getDrawerView().openDrawer(GravityCompat.START);
                }
            }
        });
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        navigationView.setNavigationItemSelectedListener(getPresenter());

        tvName.setOnClickListener(getPresenter());
        profileLayout.setOnClickListener(getPresenter());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_sp_dashboard);
        super.onCreate(savedInstanceState);

    }

    public void showUserInfo() {
        /*
        Set User Name and Profile Pic on Drawer
         */
        if (TextUtils.isEmpty(Utils.getInstance().getUserProfilePic(this))) {
            //show default profile pic
        } else {
            Picasso.with(this).load(Utils.getInstance().getUserProfilePic(this)).resize(200, 200).centerCrop().into(profileImg, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    profileImg.setImageResource(R.drawable.profile_img);
                }
            });
        }
        if (!TextUtils.isEmpty(Utils.getInstance().getUserName(this))) {
        }
        tvName.setText(Utils.getInstance().getUserName(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        showUserInfo();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        (getSupportFragmentManager().findFragmentById(R.id.sp_dashboard_container)).
                onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onBackPressed() {
        getPresenter().onBackPressed();
    }

    @Override
    public Toolbar getToolbarView() {
        return toolbar;
    }

    @Override
    public DrawerLayout getDrawerView() {
        return drawerLayout;
    }

    @Override
    public void backPress() {
        super.onBackPressed();
    }

    @Override
    public ActionBarDrawerToggle getDrawerToggleView() {
        return drawerToggle;
    }

    @Override
    public NavigationView getNavigationView() {
        return navigationView;
    }

    @Override
    public TextView getToolbarTitleView() {
        return toolbarTitle;
    }

}
