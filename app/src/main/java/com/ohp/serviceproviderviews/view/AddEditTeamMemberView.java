package com.ohp.serviceproviderviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.library.mvp.BaseView;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

/**
 * Created by vishal.sharma on 8/10/2017.
 */

public interface AddEditTeamMemberView extends BaseView {
    View getRootView();

    NestedScrollView getNestedScrollView();

    TextInputEditText getMemberName();

    TextInputEditText getMemberAddress();

    TextInputEditText getMemberEmail();

    TextInputLayout getMemberHint();

    TextInputLayout getMemberEmailHint();

    TextInputLayout getMemberAddressHint();

    MultiSelectSpinner getExpertiseSpinnerView();

    RadioGroup getRadioGroup();


    RadioButton getTeamMemberRadio();


    RadioButton getContractorRadio();

    RecyclerView getProviderList();


    Toolbar getToolbar();


    TextView getToolbarTitle();

    TextView getExpertiseHint();

}
