package com.ohp.serviceproviderviews.view;

import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by Anuj Sharma on 5/30/2017.
 */

public interface SPProfileActivityView extends BaseView {
    Toolbar getToolbarView();

    TextView getToolbarTitleView();
}
