package com.ohp.serviceproviderviews.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by vishal.sharma on 7/28/2017.
 */

public interface DashboardAllProjectView extends BaseView{
    View getRootView();

    TextView getTVWonViewAll();

    TextView getTVMyProjectsViewAll();
    TextView getCompletedProjects();

    TextView getTVNoMyProjects();

    TextView getTVNoWonProjects();

    TextView getTVNoCompletedProjects();

    RecyclerView getRVMyProjects();

    RecyclerView getRVWonProjects();

    RecyclerView getRVCompletedProjects();

}
