package com.ohp.serviceproviderviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * @author Amanpal Singh.
 */

public interface SpAddEditEventView extends BaseView {

    NestedScrollView getNestedScrollView();

    TextInputEditText getEtEventTitleView();

    TextInputEditText getEtEventTitleDetailView();

    TextInputEditText getEtAddNotesView();

    TextInputEditText getEtNotesDetailView();

    TextInputEditText getEtPropertyView();

    TextInputEditText getEtPropertyDetailView();

    TextInputEditText getEtApplianceView();

    TextInputEditText getEtApplianceDetailView();

    TextInputEditText getEtDateTimeFromView();

    TextInputEditText getEtDateTimeToView();

    TextInputEditText getEtReminderView();

    AppCompatSpinner getSpinnerTeamMemberView();

    AppCompatSpinner getSpinnerProjectView();

    AppCompatSpinner getSpinnerRepeatEventView();

    View getRootView();

    String getCurrentScreen();

    int getEventType();

    Toolbar getToolbar();

    TextView getToolbarTitleView();

    TextView getHeaderTeamMemberView();

    TextView getHeaderProjectNameView();

    TextView getHeaderEventRepeatNameView();

    TextView getHeaderRepeatReminderName();

    TextView getRepeatReminderView();

    TextView getTVHeaderEventFromTimeView();

    TextView getTVEventFromTimeView();

    TextView getTVHeaderEventToView();

    TextView getTVEventToTimeView();

    TextInputLayout getTILEventTitleView();

    TextInputLayout getTILEventTitleDetailView();

    TextInputLayout getTILNotesView();

    TextInputLayout getTILNotesDetailView();

    TextInputLayout getTILPropertyView();

    TextInputLayout getTILPropertyDetailView();

    TextInputLayout getTILApplianceView();

    TextInputLayout getTILApplianceDetailView();

    View getTeamMemberLineView();

    View getProjectNameLineView();

    View getRepeatEventLineView();

    View getReminderTimeLineView();

    View getEventFromLineView();

    View getEventToLineView();

}
