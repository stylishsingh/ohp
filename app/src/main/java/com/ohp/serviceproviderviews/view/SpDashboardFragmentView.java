package com.ohp.serviceproviderviews.view;

import android.support.design.widget.TabLayout;
import android.view.View;

import com.library.mvp.BaseView;
import com.ohp.utils.CustomViewPager;

/**
 * @author Amanpal Singh.
 */

public interface SpDashboardFragmentView extends BaseView {

    TabLayout getTabLayout();

    CustomViewPager getViewPager();

    View getRootView();

}
