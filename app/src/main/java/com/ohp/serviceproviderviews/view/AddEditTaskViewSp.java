package com.ohp.serviceproviderviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by vishal.sharma on 8/22/2017.
 */

public interface AddEditTaskViewSp extends BaseView {
    View getRootView();

    Toolbar getToolbar();

    NestedScrollView getNSView();

    TextInputEditText getTaskTittle();

    TextInputEditText getTaskNotes();

    TextView getToolbarTitle();

    TextView getTVTaskFromTime();

    TextView getRepeatReminderView();

    TextView getEtDateTimeToView();

    TextView getEtDateTimeFromView();

    TextInputEditText getEdt_PropertyView();


    AppCompatSpinner getSpinnerTeamMemberView();

    AppCompatSpinner getSpinnerProjectView();

    RecyclerView getViewEditProjectRecycler();

    AppCompatSpinner getSpinnerRepeatEventView();


    TextInputEditText getTaskTittleView();

    TextInputEditText getTaskNotesView();

    TextInputEditText getPropertyView();


    TextInputEditText getTeamMemberView();

    TextInputEditText getProjectView();

    TextInputEditText getRepeatTaskView();

    RecyclerView getViewProjectRecycler();

    TextView gettvTaskToTime();


    TextView getRepeatReminder();

    View getViewTask();


    View getEditTask();

    TextView getNoImages();

    AppCompatTextView getTVDoneView();

    AppCompatTextView getTVReopenView();

    AppCompatTextView getTVMArkCompletedView();

    View getReopenDoneView();


}
