package com.ohp.serviceproviderviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.library.mvp.BaseView;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * Created by vishal.sharma on 7/28/2017.
 */

public interface ProjectDetailServiceView extends BaseView {
    View getRootView();

    String getProjectId();

    Toolbar getToolbar();

    TextView getToolbarTitle();

    TextInputEditText setProjectName();

    TextInputEditText setProjectDescription();

    TextInputEditText setProjectExpertise();

    TextInputEditText setProjectCreatedOn();

    TextInputEditText setProjectJobType();

    TextInputEditText setPropertyName();

    TextInputEditText setPropertyDescription();

    TextInputEditText setPropertyAddress();

    TextInputEditText setPropertySqFtArea();

    TextInputEditText setPropertyConstYear();

    TextInputEditText setApplianceName();

    TextInputEditText setApplianceBrand();

    TextInputEditText setAppliancePurchaseDate();

    TextInputEditText setApplianceExpirationDate();

    TextInputEditText setApplianceWarranty();

    RecyclerView getResourceRecycler();

    RecyclerView getProjectDetailRecycler();

    RecyclerView getRVApplianceImages();

    RecyclerView getRVApplianceDocs();

    ImageView setPropertyImage();

    TextView setPropertyOwnerName();

//    TextView setAddNotes();

    TextView NoProjectImages();

    TextView getTVNoApplianceDocs();

    TextView getTVNoApplianceImages();

    TextView getTVMarkCompletedProject();

    TextView setPropertyOwnerAddress();

    TextView setBidText();

    ImageView hitBidAmount();

    RelativeLayout setLinearAddNotes();

    EditText setBidAmount();

    ImageView getImageEdit();

    ViewPager getImageViewPager();

    ViewPager getDocsViewPager();

    CirclePageIndicator getImgIndicator();

    CirclePageIndicator getDocsIndicator();

    CardView getCVApplianceDocView();

    CardView getCVApplianceImageView();
}
