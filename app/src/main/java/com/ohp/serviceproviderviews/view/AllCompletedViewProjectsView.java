package com.ohp.serviceproviderviews.view;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by vishal.sharma on 7/31/2017.
 */

public interface AllCompletedViewProjectsView extends BaseView {
    View getRootView();
    RecyclerView getRecyclerCompletedProjectList();
    RelativeLayout getEmptyLayout();
    TextView getToolbarTitle();

    NestedScrollView getNSRecyclerView();

    EditText getSearchEditText();
    LinearLayout getCloseBtn();
    Toolbar getToolbar();
}
