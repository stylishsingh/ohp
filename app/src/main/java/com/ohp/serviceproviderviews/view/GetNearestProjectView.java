package com.ohp.serviceproviderviews.view;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.library.mvp.BaseView;

/**
 * Created by vishal.sharma on 8/2/2017.
 */

public interface GetNearestProjectView extends BaseView {
    View getRootView();


    RecyclerView viewProjectsList();

    RelativeLayout setEmptyLayout();

    NestedScrollView getNSRecyclerView();

    EditText getSearchEditText();

    LinearLayout getCloseBtn();

}
