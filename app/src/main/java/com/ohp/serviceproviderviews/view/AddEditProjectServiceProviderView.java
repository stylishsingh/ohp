package com.ohp.serviceproviderviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.library.mvp.BaseView;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

/**
 * Created by vishal.sharma on 8/14/2017.
 */

public interface AddEditProjectServiceProviderView extends BaseView {
    View getRootView();

    NestedScrollView getNestedScrollView();

    Toolbar getToolbar();

    TextView getToolbarTitle();

    String getPropertyID();

    ProgressBar getProgressBar();

    ImageView getPropertyImage();

    MultiSelectSpinner getExpertiseSpinnerView();


    TextInputEditText getPropertyName();

    TextInputEditText getPropertyAddress();

    TextInputEditText setPropertyAddress();

    TextInputEditText getProjectName();

    TextInputEditText getProjectCreatedOn();

    TextInputEditText getProjectJopType();

    TextInputEditText getProjectDescriptionEdit();

    TextInputEditText getProjectExpertise();

    TextInputEditText getProjectDescription();

    TextInputEditText getApplianceView();

    TextInputEditText getProjectNameEdit();

    AppCompatSpinner getJobType();

    TextView getExpertise();

    TextInputEditText getProperty();

    TextInputEditText getApplianceName();

    View getDetailProject();

    View getEditProject();

    LinearLayout getLayout();

    RecyclerView getProjectDetailRecycler();

    RecyclerView getEditProjectDetailRecycler();

    TextView NoImages();

    RecyclerView getRVOnGoingTasks();

    RecyclerView getRVCompletedTasks();

    TextView getTVNoOnGoingTasks();

    TextView getTVNoCompletedTasks();


}
