package com.ohp.serviceproviderviews.view;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.library.mvp.BaseView;

import de.hdodenhof.circleimageview.CircleImageView;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

/**
 * Created by Anuj Sharma on 6/8/2017.
 */

public interface SPProfileFragmentView extends BaseView {
    View getRootView();

    NestedScrollView getScrollView();

    CircleImageView getProfileImg();

    FloatingActionButton getEditProfile();

    ProgressBar getProfileProgress();

    TextInputEditText getName();

    TextInputEditText getEmail();

    TextInputEditText getAddress();

    RadioGroup getGenderRadioGroup();

    RadioButton getMaleRadio();

    RadioButton getFemaleRadio();

    TextInputEditText getCompany();

    RadioGroup getLicenseRadioGroup();

    RadioButton getLicenseYesRadio();

    RadioButton getLicenseNoRadio();

    RadioGroup getBondedRadioGroup();

    RadioButton getBondedYesRadio();

    RadioButton getBondedNoRadio();

    RadioGroup getInsuredRadioGroup();

    RadioButton getInsuredYesRadio();

    RadioButton getInsuredNoRadio();

    TextInputEditText getLicenseNumber();

    TextInputEditText getWebsite();

    MenuItem getEditMenu();

    MenuItem getSaveMenu();

    MultiSelectSpinner getExpertiseSpinnerView();

    TextInputLayout getFullNameLayoutView();

    TextInputLayout getLicenseNumberLayoutView();

    TextView getTVGenderView();

    TextView getTVLicensedView();

    TextView getTVInsuredView();

    TextView getTVBondedView();

    TextView getTVExpertiseView();

    View getLineExpertiseView();
}
