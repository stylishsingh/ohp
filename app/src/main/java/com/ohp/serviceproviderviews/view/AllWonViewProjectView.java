package com.ohp.serviceproviderviews.view;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by vishal.sharma on 7/28/2017.
 */

public interface AllWonViewProjectView extends BaseView {
    View getRootView();
    RecyclerView getRecyclerWonProjectList();
    RelativeLayout getEmptyLayout();
    TextView getToolbarTitle();

    NestedScrollView getNSRecyclerView();

    EditText getSearchEditText();
    LinearLayout getCloseBtn();
    Toolbar getToolbar();
}
