package com.ohp.commonviews.beans;

/**
 * @author Amanpal Singh..
 */

public class ResourcesValueModel {

    String title, count;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
