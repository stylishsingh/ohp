package com.ohp.commonviews.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vishal.sharma on 9/7/2017.
 */

public class GetMessagesResponse extends GenericBean {

    /**
     * totalRecord : 3
     * data : [{"chat_id":14,"send_by":8,"send_by_name":"Ps","send_to":6,"send_to_name":"Poo","message":"I am fine","created":"2017-09-07 10:27:36"},{"chat_id":13,"send_by":6,"send_by_name":"Poo","send_to":8,"send_to_name":"Ps","message":"Hello, how are u?","created":"2017-09-07 10:26:12"},{"chat_id":12,"send_by":6,"send_by_name":"Poo","send_to":8,"send_to_name":"Ps","message":"Hello","created":"2017-09-07 10:25:57"}]
     */

    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * chat_id : 14
         * send_by : 8
         * send_by_name : Ps
         * send_to : 6
         * send_to_name : Poo
         * message : I am fine
         * created : 2017-09-07 10:27:36
         */

        @SerializedName("chat_id")
        private int chatId;
        @SerializedName("send_by")
        private int sendBy;
        @SerializedName("send_by_name")
        private String sendByName;
        @SerializedName("send_to")
        private int sendTo;
        @SerializedName("send_to_name")
        private String sendToName;
        @SerializedName("message")
        private String messageX;
        @SerializedName("created")
        private String created;

        public int getChatId() {
            return chatId;
        }

        public void setChatId(int chatId) {
            this.chatId = chatId;
        }

        public int getSendBy() {
            return sendBy;
        }

        public void setSendBy(int sendBy) {
            this.sendBy = sendBy;
        }

        public String getSendByName() {
            return sendByName;
        }

        public void setSendByName(String sendByName) {
            this.sendByName = sendByName;
        }

        public int getSendTo() {
            return sendTo;
        }

        public void setSendTo(int sendTo) {
            this.sendTo = sendTo;
        }

        public String getSendToName() {
            return sendToName;
        }

        public void setSendToName(String sendToName) {
            this.sendToName = sendToName;
        }

        public String getMessageX() {
            return messageX;
        }

        public void setMessageX(String messageX) {
            this.messageX = messageX;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }
    }
}
