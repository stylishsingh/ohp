package com.ohp.commonviews.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.ohp.enums.ScreenNavigation;

/**
 * Created by Anuj Sharma on 5/22/2017.
 */

public class ThumbnailBean implements Parcelable {
    public static final Creator<ThumbnailBean> CREATOR = new Creator<ThumbnailBean>() {
        @Override
        public ThumbnailBean createFromParcel(Parcel source) {
            return new ThumbnailBean(source);
        }

        @Override
        public ThumbnailBean[] newArray(int size) {
            return new ThumbnailBean[size];
        }
    };
    private String imagePath;
    private boolean isFile = false;
    private boolean isAddView = false;
    private String extension;
    private String name;
    private ScreenNavigation check;
    private String imageID;

    public ThumbnailBean() {
    }

    protected ThumbnailBean(Parcel in) {
        this.imagePath = in.readString();
        this.isFile = in.readByte() != 0;
        this.isAddView = in.readByte() != 0;
        this.extension = in.readString();
        this.name = in.readString();
        int tmpCheck = in.readInt();
        this.check = tmpCheck == -1 ? null : ScreenNavigation.values()[tmpCheck];
        this.imageID = in.readString();
    }

    public String getImageID() {
        return imageID == null ? "" : imageID;
    }

    public void setImageID(String imageID) {
        this.imageID = imageID;
    }

    public ScreenNavigation getCheck() {
        return check == null ? ScreenNavigation.DUMMY_ITEM : check;
    }

    public void setCheck(ScreenNavigation check) {
        this.check = check;
    }

    public String getExtension() {
        return extension == null ? "" : extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public boolean isFile() {
        return isFile;
    }

    public void setFile(boolean file) {
        isFile = file;
    }

    public boolean isAddView() {
        return isAddView;
    }

    public void setAddView(boolean addView) {
        isAddView = addView;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imagePath);
        dest.writeByte(this.isFile ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isAddView ? (byte) 1 : (byte) 0);
        dest.writeString(this.extension);
        dest.writeString(this.name);
        dest.writeInt(this.check == null ? -1 : this.check.ordinal());
        dest.writeString(this.imageID);
    }
}
