package com.ohp.commonviews.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Anuj Sharma on 5/19/2017.
 */

public  class GenericBean {

    /**
     * status : 200
     * message : User has been registed successfully.
     */

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
