package com.ohp.commonviews.beans;

import com.google.gson.annotations.SerializedName;

/**
 * @author Anuj Sharma.
 */

public class LoginResponse extends GenericBean {

    /**
     * data : {"full_name":"","email":"rahul.jain@mobilyte.com","role":1,"access_token":"access_token_JiwzKFQsMyhVCmAK","profile_image_100X100":"","profile_image_200X200":""}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * full_name :
         * email : rahul.jain@mobilyte.com
         * role : 1
         * access_token : access_token_JiwzKFQsMyhVCmAK
         * profile_image_100X100 :
         * profile_image_200X200 :
         */

        @SerializedName("full_name")
        private String fullName;
        @SerializedName("email")
        private String email;
        @SerializedName("role")
        private int role;

        @SerializedName("is_verified")
        private int isVerified;

        @SerializedName("is_social")
        private int isSocial;

        @SerializedName("access_token")
        private String accessToken;
        @SerializedName("profile_image_100X100")
        private String profileImage100X100;
        @SerializedName("profile_image_200X200")
        private String profileImage200X200;
        @SerializedName("user_id")
        private int user_id;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        /**
         * gender : male
         * address :
         * city :
         * state :
         * status : 1
         */

        @SerializedName("gender")
        private String gender;
        @SerializedName("address")
        private String address;
        @SerializedName("city")
        private String city;
        @SerializedName("state")
        private String state;
        @SerializedName("status")
        private int status;

        public int getIsVerified() {
            return isVerified;
        }

        public void setIsVerified(int isVerified) {
            this.isVerified = isVerified;
        }

        public int getIsSocial() {
            return isSocial;
        }

        public void setIsSocial(int isSocial) {
            this.isSocial = isSocial;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public int getRole() {
            return role;
        }

        public void setRole(int role) {
            this.role = role;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getProfileImage100X100() {
            return profileImage100X100;
        }

        public void setProfileImage100X100(String profileImage100X100) {
            this.profileImage100X100 = profileImage100X100;
        }

        public String getProfileImage200X200() {
            return profileImage200X200;
        }

        public void setProfileImage200X200(String profileImage200X200) {
            this.profileImage200X200 = profileImage200X200;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
