package com.ohp.commonviews.beans;

/**
 * @author Amanpal Singh.
 */

public class AddEditModel extends GenericBean {

    /**
     * data : {"id":60}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 60
         */

        private int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
