package com.ohp.commonviews.beans;

/**
 * Created by vishal.sharma on 7/14/2017.
 */

public class AddProjectModelResponse extends GenericBean {


    /**
     * status : 200
     * data : {"project_id":17}
     * message : Project added successfully.
     */


    private DataBean data;



    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }


    public static class DataBean {
        /**
         * project_id : 17
         */

        private int project_id;

        public int getProject_id() {
            return project_id;
        }

        public void setProject_id(int project_id) {
            this.project_id = project_id;
        }
    }
}
