package com.ohp.commonviews.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Anuj Sharma on 5/26/2017.
 */

public class ResourceBean extends GenericBean{

    @SerializedName("data")
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : bedroom
         * type : int
         * info : [{"value":"1"},{"value":"2"},{"value":"3"},{"value":"4"},{"value":"5"},{"value":"6"}]
         */

        @SerializedName("title")
        private String title;
        @SerializedName("type")
        private String type;
        @SerializedName("info")
        private List<InfoBean> info;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<InfoBean> getInfo() {
            return info;
        }

        public void setInfo(List<InfoBean> info) {
            this.info = info;
        }

        public static class InfoBean {
            /**
             * value : 1
             */

            @SerializedName("value")
            private String value;
            @SerializedName("id")
            private int ID;

            public int getID() {
                return ID;
            }

            public void setID(int ID) {
                this.ID = ID;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }
}
