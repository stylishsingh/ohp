package com.ohp.commonviews.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vishal.sharma on 9/8/2017.
 */

public class GetChatListResponse extends GenericBean {

    /**
     * totalRecord : 2
     * data : [{"user_id":57,"full_name":"Jatin harish","profile_image_100X100":"","profile_image_200X200":"","message":"Hello","created":"2017-09-08T04:50:55+00:00"},{"user_id":8,"full_name":"Ps","profile_image_100X100":"","profile_image_200X200":"","message":"Hello","created":"2017-09-08T04:50:55+00:00"}]
     */

    @SerializedName("totalRecord")
    private int totalRecord;
    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * user_id : 57
         * full_name : Jatin harish
         * profile_image_100X100 :
         * profile_image_200X200 :
         * message : Hello
         * created : 2017-09-08T04:50:55+00:00
         */

        @SerializedName("user_id")
        private int userId;
        @SerializedName("full_name")
        private String fullName;
        @SerializedName("profile_image_100X100")
        private String profileImage100X100;
        @SerializedName("profile_image_200X200")
        private String profileImage200X200;
        @SerializedName("message")
        private String messageX;
        @SerializedName("created")
        private String created;

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getProfileImage100X100() {
            return profileImage100X100;
        }

        public void setProfileImage100X100(String profileImage100X100) {
            this.profileImage100X100 = profileImage100X100;
        }

        public String getProfileImage200X200() {
            return profileImage200X200;
        }

        public void setProfileImage200X200(String profileImage200X200) {
            this.profileImage200X200 = profileImage200X200;
        }

        public String getMessageX() {
            return messageX;
        }

        public void setMessageX(String messageX) {
            this.messageX = messageX;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.userId);
            dest.writeString(this.fullName);
            dest.writeString(this.profileImage100X100);
            dest.writeString(this.profileImage200X200);
            dest.writeString(this.messageX);
            dest.writeString(this.created);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.userId = in.readInt();
            this.fullName = in.readString();
            this.profileImage100X100 = in.readString();
            this.profileImage200X200 = in.readString();
            this.messageX = in.readString();
            this.created = in.readString();
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }
}
