package com.ohp.commonviews.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vishal.sharma on 7/13/2017.
 */

public class JobTypeAndExpertise extends GenericBean {

    /**
     * status : 200
     * message : Success
     * data : [[{"title":"job_type","type":"string","info":[{"value":"Repair","id":1},{"value":"Removal/Recycle","id":2},{"value":"Garden/Landscaping","id":3}]}],[{"title":"expertise","type":"string","info":[{"value":"Cleaning Services","id":1},{"value":"Flooring & Carpets","id":2},{"value":"Garden/Landscaping","id":3}]}]]
     */

    @SerializedName("data")
    private List<List<DataBean>> data;



    public List<List<DataBean>> getData() {
        return data;
    }

    public void setData(List<List<DataBean>> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : job_type
         * type : string
         * info : [{"value":"Repair","id":1},{"value":"Removal/Recycle","id":2},{"value":"Garden/Landscaping","id":3}]
         */
        @SerializedName("title")
        private String title;
        @SerializedName("type")
        private String type;
        @SerializedName("info")
        private List<InfoBean> info;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<InfoBean> getInfo() {
            return info;
        }

        public void setInfo(List<InfoBean> info) {
            this.info = info;
        }

        public static class InfoBean {
            /**
             * value : Repair
             * id : 1
             */
            @SerializedName("value")
            private String value;
            @SerializedName("id")
            private int id;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }
        }
    }
}
