package com.ohp.commonviews.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Anuj Sharma on 5/26/2017.
 */

public class RegisterResponse extends GenericBean {

    /**
     * data : {"access_token":"access_token_JiwzYFUsI0RZCmAK","email":"anuj@mobilyte.com"}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * access_token : access_token_JiwzYFUsI0RZCmAK
         * email : anuj@mobilyte.com
         */

        @SerializedName("access_token")
        private String accessToken;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        @SerializedName("email")

        private String email;
        @SerializedName("user_id")
        private String user_id;

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}
