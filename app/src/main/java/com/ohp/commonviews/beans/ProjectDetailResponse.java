package com.ohp.commonviews.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vishal.sharma on 7/18/2017.
 */

public class ProjectDetailResponse extends GenericBean {


    /**
     * data : {"project_id":330,"project_name":"Nsnsjsj","project_description":"Nsjsjsjs","project_owner":{"user_id":233,"full_name":"Arsh Deep","gender":"female","address":"Phase 8, Industrial Area, Sector 73, Sahibzada Ajit Singh Nagar, Chandigarh 160071, India"},"property":{"property_id":208,"property_name":"pro1","property_address":"Shop No. E- 36, Ground Floor, Phase 8, Industrial Area, Sahibzada Ajit Singh Nagar, Punjab 160072, India","property_image":"http://onehomeportalqa.mobilytedev.com/img/property_picture/original_image/2nEFIgQ47z4HQVn.jpg","beds":"","baths":2,"garage":"","type":"Co-op"},"created":"01-08-2017","project_provider":{"provider_id":0,"provider_name":""},"project_images":[{"project_image_id":160,"project_image_avatar":"http://onehomeportalqa.mobilytedev.com/img/project_picture/original_image/Oba4cQn0YOntGxW.png"}],"project_appliance":{"appliance_id":124,"appliance_name":"Hdbdbdb","appliance_brand":"","appliance_avatar":"","warranty_expiration_date":"","extended_warranty_date":""},"project_jobtype":{"jobtype_id":"1","jobtype_title":"Repair"},"project_expertise":[{"expertise_id":"1","expertise_title":"Cleaning Services"}],"bids_count":0,"bids":[]}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * project_id : 330
         * project_name : Nsnsjsj
         * project_description : Nsjsjsjs
         * project_owner : {"user_id":233,"full_name":"Arsh Deep","gender":"female","address":"Phase 8, Industrial Area, Sector 73, Sahibzada Ajit Singh Nagar, Chandigarh 160071, India"}
         * property : {"property_id":208,"property_name":"pro1","property_address":"Shop No. E- 36, Ground Floor, Phase 8, Industrial Area, Sahibzada Ajit Singh Nagar, Punjab 160072, India","property_image":"http://onehomeportalqa.mobilytedev.com/img/property_picture/original_image/2nEFIgQ47z4HQVn.jpg","beds":"","baths":2,"garage":"","type":"Co-op"}
         * created : 01-08-2017
         * project_tasks : {"completed_tasks":[],"ongoing_tasks":[{"task_id":11,"task_name":"Test appliance NAME2","task_notes":"Test property address12","task_time_from":"","task_time_to":"1970-01-01T00:00:02+00:00","task_reminder":"","task_repeat":"","created":"2017-08-24T05:05:25+00:00","property":{"proeprty_id":0,"property_name":"Test property name","property_owner_name":"service"},"task_images":[],"member":{"member_id":10,"member_name":"lambu kumar"},"project":{"project_id":81,"project_name":"Mandeepss"}},{"task_id":12,"task_name":"Test appliance NAME2","task_notes":"Test property address12","task_time_from":"","task_time_to":"1970-01-01T00:00:02+00:00","task_reminder":"","task_repeat":"","created":"2017-08-24T06:34:06+00:00","property":{"proeprty_id":0,"property_name":"Test property name","property_owner_name":"service"},"task_images":[],"member":{"member_id":10,"member_name":"lambu kumar"},"project":{"project_id":81,"project_name":"Mandeepss"}},{"task_id":13,"task_name":"Test appliance NAME2","task_notes":"Test property address12","task_time_from":"","task_time_to":"1970-01-01T00:00:02+00:00","task_reminder":"","task_repeat":"","created":"2017-08-24T06:34:13+00:00","property":{"proeprty_id":0,"property_name":"Test property name","property_owner_name":"service"},"task_images":[],"member":{"member_id":10,"member_name":"lambu kumar"},"project":{"project_id":81,"project_name":"Mandeepss"}},{"task_id":14,"task_name":"Test appliance NAME2","task_notes":"Test property address12","task_time_from":"","task_time_to":"1970-01-01T00:00:02+00:00","task_reminder":"","task_repeat":"","created":"2017-08-24T06:34:40+00:00","property":{"proeprty_id":0,"property_name":"Test property name","property_owner_name":"service"},"task_images":[],"member":{"member_id":10,"member_name":"lambu kumar"},"project":{"project_id":81,"project_name":"Mandeepss"}}]}
         * project_provider : {"provider_id":0,"provider_name":""}
         * project_images : [{"project_image_id":160,"project_image_avatar":"http://onehomeportalqa.mobilytedev.com/img/project_picture/original_image/Oba4cQn0YOntGxW.png"}]
         * project_appliance : {"appliance_id":124,"appliance_name":"Hdbdbdb","appliance_brand":"","appliance_avatar":"","warranty_expiration_date":"","extended_warranty_date":""}
         * project_jobtype : {"jobtype_id":"1","jobtype_title":"Repair"}
         * project_expertise : [{"expertise_id":"1","expertise_title":"Cleaning Services"}]
         * bids_count : 0
         * bids : []
         */

        @SerializedName("project_id")
        private int projectId;
        @SerializedName("project_status")
        private int projectStatus;
        @SerializedName("project_name")
        private String projectName;
        @SerializedName("project_description")
        private String projectDescription;
        @SerializedName("project_owner")
        private ProjectOwnerBean projectOwner;
        @SerializedName("property")
        private PropertyBean property;
        @SerializedName("created")
        private String created;
        @SerializedName("project_provider")
        private ProjectProviderBean projectProvider;
        @SerializedName("project_appliance")
        private ProjectApplianceBean projectAppliance;
        @SerializedName("project_jobtype")
        private ProjectJobtypeBean projectJobtype;
        @SerializedName("my_bid")
        private MyBidBean myBid;
        @SerializedName("bids_count")
        private int bidsCount;
        @SerializedName("project_images")
        private List<ProjectImagesBean> projectImages;
        @SerializedName("project_expertise")
        private List<ProjectExpertiseBean> projectExpertise;
        @SerializedName("bids")
        private List<BidsBean> bids;
        @SerializedName("project_tasks")
        private ProjectTasksBean projectTasks;


        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }

        public int getProjectStatus() {
            return projectStatus;
        }

        public void setProjectStatus(int projectStatus) {
            this.projectStatus = projectStatus;
        }

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getProjectDescription() {
            return projectDescription;
        }

        public void setProjectDescription(String projectDescription) {
            this.projectDescription = projectDescription;
        }

        public ProjectOwnerBean getProjectOwner() {
            return projectOwner;
        }

        public void setProjectOwner(ProjectOwnerBean projectOwner) {
            this.projectOwner = projectOwner;
        }

        public PropertyBean getProperty() {
            return property;
        }

        public void setProperty(PropertyBean property) {
            this.property = property;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public ProjectProviderBean getProjectProvider() {
            return projectProvider;
        }

        public void setProjectProvider(ProjectProviderBean projectProvider) {
            this.projectProvider = projectProvider;
        }

        public ProjectApplianceBean getProjectAppliance() {
            return projectAppliance;
        }

        public void setProjectAppliance(ProjectApplianceBean projectAppliance) {
            this.projectAppliance = projectAppliance;
        }

        public ProjectJobtypeBean getProjectJobtype() {
            return projectJobtype;
        }

        public void setProjectJobtype(ProjectJobtypeBean projectJobtype) {
            this.projectJobtype = projectJobtype;
        }

        public MyBidBean getMyBid() {
            return myBid;
        }

        public void setMyBid(MyBidBean myBid) {
            this.myBid = myBid;
        }

        public int getBidsCount() {
            return bidsCount;
        }

        public void setBidsCount(int bidsCount) {
            this.bidsCount = bidsCount;
        }

        public List<ProjectImagesBean> getProjectImages() {
            return projectImages;
        }

        public void setProjectImages(List<ProjectImagesBean> projectImages) {
            this.projectImages = projectImages;
        }

        public List<ProjectExpertiseBean> getProjectExpertise() {
            return projectExpertise;
        }

        public void setProjectExpertise(List<ProjectExpertiseBean> projectExpertise) {
            this.projectExpertise = projectExpertise;
        }

        public List<BidsBean> getBids() {
            return bids;
        }

        public void setBids(List<BidsBean> bids) {
            this.bids = bids;
        }

        public ProjectTasksBean getProjectTasks() {
            return projectTasks;
        }

        public void setProjectTasks(ProjectTasksBean projectTasks) {
            this.projectTasks = projectTasks;
        }

        public static class BidsBean implements Parcelable {


            /**
             * bid_id : 9
             * bid_amount : 123456
             * bid_note : The best way to thing if is was just like a not just for a to read or and a not just to for years of my time life is so has very far very good app very very
             * user_id : 6
             * full_name : Max
             * gender : male
             * address : Mohali Stadium Rd, Sector 55, Mohali Village, Sahibzada Ajit Singh Nagar, Chandigarh 160055, India
             * profile_image_100X100 : http://ohpstaging.mobilytedev.com/img/profile_picture/100X100/EQfLDoNr0UbKGn2.png
             * profile_image_200X200 : http://ohpstaging.mobilytedev.com/img/profile_picture/original_image/EQfLDoNr0UbKGn2.png
             * is_licensed : 1
             * license_no : 123456789
             * business_name : Mobilyte
             * insured : 1
             * website : www.google.com
             * bonded : 1
             * latitude : 30.70829
             * longitude : 76.7023768
             * expertise : [{"expertise_id":1,"expertise_title":"Cleaning Services"}]
             * away_in_kms : 0
             */

            @SerializedName("bid_id")
            private int bidId;
            @SerializedName("bid_amount")
            private int bidAmount;
            @SerializedName("avg_rating")
            private int avgRating;
            @SerializedName("bid_note")
            private String bidNote;
            @SerializedName("user_id")
            private int userId;
            @SerializedName("full_name")
            private String fullName;
            @SerializedName("gender")
            private String gender;
            @SerializedName("address")
            private String address;
            @SerializedName("profile_image_100X100")
            private String profileImage100X100;
            @SerializedName("profile_image_200X200")
            private String profileImage200X200;
            @SerializedName("is_licensed")
            private int isLicensed;
            @SerializedName("license_no")
            private String licenseNo;
            @SerializedName("business_name")
            private String businessName;
            @SerializedName("insured")
            private int insured;
            @SerializedName("website")
            private String website;
            @SerializedName("bonded")
            private int bonded;
            @SerializedName("latitude")
            private String latitude;
            @SerializedName("longitude")
            private String longitude;
            @SerializedName("away_in_kms")
            private int awayInKms;
            @SerializedName("expertise")
            private List<ExpertiseBean> expertise;

            public BidsBean() {
            }

            public int getBidId() {
                return bidId;
            }

            public void setBidId(int bidId) {
                this.bidId = bidId;
            }

            public int getBidAmount() {
                return bidAmount;
            }

            public void setBidAmount(int bidAmount) {
                this.bidAmount = bidAmount;
            }

            public int getAvgRating() {
                return avgRating;
            }

            public void setAvgRating(int avgRating) {
                this.avgRating = avgRating;
            }

            public String getBidNote() {
                return bidNote;
            }

            public void setBidNote(String bidNote) {
                this.bidNote = bidNote;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getFullName() {
                return fullName;
            }

            public void setFullName(String fullName) {
                this.fullName = fullName;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getProfileImage100X100() {
                return profileImage100X100;
            }

            public void setProfileImage100X100(String profileImage100X100) {
                this.profileImage100X100 = profileImage100X100;
            }

            public String getProfileImage200X200() {
                return profileImage200X200;
            }

            public void setProfileImage200X200(String profileImage200X200) {
                this.profileImage200X200 = profileImage200X200;
            }

            public int getIsLicensed() {
                return isLicensed;
            }

            public void setIsLicensed(int isLicensed) {
                this.isLicensed = isLicensed;
            }

            public String getLicenseNo() {
                return licenseNo;
            }

            public void setLicenseNo(String licenseNo) {
                this.licenseNo = licenseNo;
            }

            public String getBusinessName() {
                return businessName;
            }

            public void setBusinessName(String businessName) {
                this.businessName = businessName;
            }

            public int getInsured() {
                return insured;
            }

            public void setInsured(int insured) {
                this.insured = insured;
            }

            public String getWebsite() {
                return website;
            }

            public void setWebsite(String website) {
                this.website = website;
            }

            public int getBonded() {
                return bonded;
            }

            public void setBonded(int bonded) {
                this.bonded = bonded;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public int getAwayInKms() {
                return awayInKms;
            }

            public void setAwayInKms(int awayInKms) {
                this.awayInKms = awayInKms;
            }

            public List<ExpertiseBean> getExpertise() {
                return expertise;
            }

            public void setExpertise(List<ExpertiseBean> expertise) {
                this.expertise = expertise;
            }

            public static class ExpertiseBean implements Parcelable {
                public static final Creator<ExpertiseBean> CREATOR = new Creator<ExpertiseBean>() {
                    @Override
                    public ExpertiseBean createFromParcel(Parcel source) {
                        return new ExpertiseBean(source);
                    }

                    @Override
                    public ExpertiseBean[] newArray(int size) {
                        return new ExpertiseBean[size];
                    }
                };
                /**
                 * expertise_id : 1
                 * expertise_title : Cleaning Services
                 */

                @SerializedName("expertise_id")
                private int expertiseId;
                @SerializedName("expertise_title")
                private String expertiseTitle;

                public ExpertiseBean() {
                }

                protected ExpertiseBean(Parcel in) {
                    this.expertiseId = in.readInt();
                    this.expertiseTitle = in.readString();
                }

                public int getExpertiseId() {
                    return expertiseId;
                }

                public void setExpertiseId(int expertiseId) {
                    this.expertiseId = expertiseId;
                }

                public String getExpertiseTitle() {
                    return expertiseTitle;
                }

                public void setExpertiseTitle(String expertiseTitle) {
                    this.expertiseTitle = expertiseTitle;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeInt(this.expertiseId);
                    dest.writeString(this.expertiseTitle);
                }
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.bidId);
                dest.writeInt(this.bidAmount);
                dest.writeInt(this.avgRating);
                dest.writeString(this.bidNote);
                dest.writeInt(this.userId);
                dest.writeString(this.fullName);
                dest.writeString(this.gender);
                dest.writeString(this.address);
                dest.writeString(this.profileImage100X100);
                dest.writeString(this.profileImage200X200);
                dest.writeInt(this.isLicensed);
                dest.writeString(this.licenseNo);
                dest.writeString(this.businessName);
                dest.writeInt(this.insured);
                dest.writeString(this.website);
                dest.writeInt(this.bonded);
                dest.writeString(this.latitude);
                dest.writeString(this.longitude);
                dest.writeInt(this.awayInKms);
                dest.writeTypedList(this.expertise);
            }

            protected BidsBean(Parcel in) {
                this.bidId = in.readInt();
                this.bidAmount = in.readInt();
                this.avgRating = in.readInt();
                this.bidNote = in.readString();
                this.userId = in.readInt();
                this.fullName = in.readString();
                this.gender = in.readString();
                this.address = in.readString();
                this.profileImage100X100 = in.readString();
                this.profileImage200X200 = in.readString();
                this.isLicensed = in.readInt();
                this.licenseNo = in.readString();
                this.businessName = in.readString();
                this.insured = in.readInt();
                this.website = in.readString();
                this.bonded = in.readInt();
                this.latitude = in.readString();
                this.longitude = in.readString();
                this.awayInKms = in.readInt();
                this.expertise = in.createTypedArrayList(ExpertiseBean.CREATOR);
            }

            public static final Creator<BidsBean> CREATOR = new Creator<BidsBean>() {
                @Override
                public BidsBean createFromParcel(Parcel source) {
                    return new BidsBean(source);
                }

                @Override
                public BidsBean[] newArray(int size) {
                    return new BidsBean[size];
                }
            };
        }

        public static class ProjectOwnerBean {
            /**
             * user_id : 233
             * full_name : Arsh Deep
             * gender : female
             * address : Phase 8, Industrial Area, Sector 73, Sahibzada Ajit Singh Nagar, Chandigarh 160071, India
             */

            @SerializedName("user_id")
            private int userId;
            @SerializedName("full_name")
            private String fullName;
            @SerializedName("gender")
            private String gender;
            @SerializedName("address")
            private String address;

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getFullName() {
                return fullName;
            }

            public void setFullName(String fullName) {
                this.fullName = fullName;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }
        }

        public static class PropertyBean {

            /**
             * property_id :
             * property_name : Test property
             * property_address : E-44, Sector 72, Industrial Area,, Phase 8, Industrial Area, Sahibzada Ajit Singh Nagar, Punjab 1600
             * property_area :
             * construction_year :
             * property_description :
             * property_image :
             * resource : [{"title":"beds","value":"0"},{"title":"baths","value":"0"},{"title":"garage","value":"0"},{"title":"type","value":"0"}]
             */

            @SerializedName("property_id")
            private int propertyId;
            @SerializedName("property_name")
            private String propertyName;
            @SerializedName("property_address")
            private String propertyAddress;
            @SerializedName("property_area")
            private String propertyArea;
            @SerializedName("construction_year")
            private String constructionYear;
            @SerializedName("property_description")
            private String propertyDescription;
            @SerializedName("property_image")
            private String propertyImage;
            @SerializedName("resource")
            private List<ResourceBean> resource;

            public int getPropertyId() {
                return propertyId;
            }

            public void setPropertyId(int propertyId) {
                this.propertyId = propertyId;
            }

            public String getPropertyName() {
                return propertyName;
            }

            public void setPropertyName(String propertyName) {
                this.propertyName = propertyName;
            }

            public String getPropertyAddress() {
                return propertyAddress;
            }

            public void setPropertyAddress(String propertyAddress) {
                this.propertyAddress = propertyAddress;
            }

            public String getPropertyArea() {
                return propertyArea;
            }

            public void setPropertyArea(String propertyArea) {
                this.propertyArea = propertyArea;
            }

            public String getConstructionYear() {
                return constructionYear;
            }

            public void setConstructionYear(String constructionYear) {
                this.constructionYear = constructionYear;
            }

            public String getPropertyDescription() {
                return propertyDescription;
            }

            public void setPropertyDescription(String propertyDescription) {
                this.propertyDescription = propertyDescription;
            }

            public String getPropertyImage() {
                return propertyImage;
            }

            public void setPropertyImage(String propertyImage) {
                this.propertyImage = propertyImage;
            }

            public List<ResourceBean> getResource() {
                return resource;
            }

            public void setResource(List<ResourceBean> resource) {
                this.resource = resource;
            }

            public static class ResourceBean {
                /**
                 * title : beds
                 * value : 0
                 */

                @SerializedName("title")
                private String title;
                @SerializedName("value")
                private String value;

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }
            }
        }

        public static class ProjectProviderBean implements Parcelable {


            /**
             * bid_amount : 1213
             * bid_note :
             * bid_id : 11
             * user_id : 2
             * full_name : service
             * gender : male
             * address : E 38, Phase 8, Industrial Area, Phase 8, Industrial Area, Sahibzada Ajit Singh Nagar, Punjab 140308, India
             * profile_image_100X100 :
             * profile_image_200X200 :
             * is_licensed : 1
             * license_no : 256398877
             * business_name : service enterprises
             * insured : 1
             * website :
             * bonded : 1
             * latitude : 30.708285999999994
             * longitude : 76.7021599
             * expertise : [{"expertise_id":1,"expertise_title":"Cleaning Services"},{"expertise_id":3,"expertise_title":"Garden/Landscaping"}]
             * away_in_kms : 0
             */

            @SerializedName("bid_amount")
            private int bidAmount;
            @SerializedName("bid_note")
            private String bidNote;
            @SerializedName("bid_id")
            private int bidId;
            @SerializedName("avg_rating")
            private int avgRating;
            @SerializedName("user_id")
            private int userId;
            @SerializedName("full_name")
            private String fullName;
            @SerializedName("gender")
            private String gender;
            @SerializedName("address")
            private String address;
            @SerializedName("profile_image_100X100")
            private String profileImage100X100;
            @SerializedName("profile_image_200X200")
            private String profileImage200X200;
            @SerializedName("is_licensed")
            private int isLicensed;
            @SerializedName("license_no")
            private String licenseNo;
            @SerializedName("business_name")
            private String businessName;
            @SerializedName("insured")
            private int insured;
            @SerializedName("website")
            private String website;
            @SerializedName("bonded")
            private int bonded;
            @SerializedName("latitude")
            private String latitude;
            @SerializedName("longitude")
            private String longitude;
            @SerializedName("away_in_kms")
            private int awayInKms;
            @SerializedName("expertise")
            private List<ExpertiseBean> expertise;

            public ProjectProviderBean() {
            }

            public int getBidAmount() {
                return bidAmount;
            }

            public void setBidAmount(int bidAmount) {
                this.bidAmount = bidAmount;
            }

            public int getAvgRating() {
                return avgRating;
            }

            public void setAvgRating(int avgRating) {
                this.avgRating = avgRating;
            }

            public String getBidNote() {
                return bidNote;
            }

            public void setBidNote(String bidNote) {
                this.bidNote = bidNote;
            }

            public int getBidId() {
                return bidId;
            }

            public void setBidId(int bidId) {
                this.bidId = bidId;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getFullName() {
                return fullName;
            }

            public void setFullName(String fullName) {
                this.fullName = fullName;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getProfileImage100X100() {
                return profileImage100X100;
            }

            public void setProfileImage100X100(String profileImage100X100) {
                this.profileImage100X100 = profileImage100X100;
            }

            public String getProfileImage200X200() {
                return profileImage200X200;
            }

            public void setProfileImage200X200(String profileImage200X200) {
                this.profileImage200X200 = profileImage200X200;
            }

            public int getIsLicensed() {
                return isLicensed;
            }

            public void setIsLicensed(int isLicensed) {
                this.isLicensed = isLicensed;
            }

            public String getLicenseNo() {
                return licenseNo;
            }

            public void setLicenseNo(String licenseNo) {
                this.licenseNo = licenseNo;
            }

            public String getBusinessName() {
                return businessName;
            }

            public void setBusinessName(String businessName) {
                this.businessName = businessName;
            }

            public int getInsured() {
                return insured;
            }

            public void setInsured(int insured) {
                this.insured = insured;
            }

            public String getWebsite() {
                return website;
            }

            public void setWebsite(String website) {
                this.website = website;
            }

            public int getBonded() {
                return bonded;
            }

            public void setBonded(int bonded) {
                this.bonded = bonded;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public int getAwayInKms() {
                return awayInKms;
            }

            public void setAwayInKms(int awayInKms) {
                this.awayInKms = awayInKms;
            }

            public List<ExpertiseBean> getExpertise() {
                return expertise;
            }

            public void setExpertise(List<ExpertiseBean> expertise) {
                this.expertise = expertise;
            }

            public static class ExpertiseBean implements Parcelable {
                public static final Creator<ExpertiseBean> CREATOR = new Creator<ExpertiseBean>() {
                    @Override
                    public ExpertiseBean createFromParcel(Parcel source) {
                        return new ExpertiseBean(source);
                    }

                    @Override
                    public ExpertiseBean[] newArray(int size) {
                        return new ExpertiseBean[size];
                    }
                };
                /**
                 * expertise_id : 1
                 * expertise_title : Cleaning Services
                 */

                @SerializedName("expertise_id")
                private int expertiseId;
                @SerializedName("expertise_title")
                private String expertiseTitle;

                public ExpertiseBean() {
                }

                protected ExpertiseBean(Parcel in) {
                    this.expertiseId = in.readInt();
                    this.expertiseTitle = in.readString();
                }

                public int getExpertiseId() {
                    return expertiseId;
                }

                public void setExpertiseId(int expertiseId) {
                    this.expertiseId = expertiseId;
                }

                public String getExpertiseTitle() {
                    return expertiseTitle;
                }

                public void setExpertiseTitle(String expertiseTitle) {
                    this.expertiseTitle = expertiseTitle;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeInt(this.expertiseId);
                    dest.writeString(this.expertiseTitle);
                }
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.bidAmount);
                dest.writeString(this.bidNote);
                dest.writeInt(this.bidId);
                dest.writeInt(this.avgRating);
                dest.writeInt(this.userId);
                dest.writeString(this.fullName);
                dest.writeString(this.gender);
                dest.writeString(this.address);
                dest.writeString(this.profileImage100X100);
                dest.writeString(this.profileImage200X200);
                dest.writeInt(this.isLicensed);
                dest.writeString(this.licenseNo);
                dest.writeString(this.businessName);
                dest.writeInt(this.insured);
                dest.writeString(this.website);
                dest.writeInt(this.bonded);
                dest.writeString(this.latitude);
                dest.writeString(this.longitude);
                dest.writeInt(this.awayInKms);
                dest.writeTypedList(this.expertise);
            }

            protected ProjectProviderBean(Parcel in) {
                this.bidAmount = in.readInt();
                this.bidNote = in.readString();
                this.bidId = in.readInt();
                this.avgRating = in.readInt();
                this.userId = in.readInt();
                this.fullName = in.readString();
                this.gender = in.readString();
                this.address = in.readString();
                this.profileImage100X100 = in.readString();
                this.profileImage200X200 = in.readString();
                this.isLicensed = in.readInt();
                this.licenseNo = in.readString();
                this.businessName = in.readString();
                this.insured = in.readInt();
                this.website = in.readString();
                this.bonded = in.readInt();
                this.latitude = in.readString();
                this.longitude = in.readString();
                this.awayInKms = in.readInt();
                this.expertise = in.createTypedArrayList(ExpertiseBean.CREATOR);
            }

            public static final Creator<ProjectProviderBean> CREATOR = new Creator<ProjectProviderBean>() {
                @Override
                public ProjectProviderBean createFromParcel(Parcel source) {
                    return new ProjectProviderBean(source);
                }

                @Override
                public ProjectProviderBean[] newArray(int size) {
                    return new ProjectProviderBean[size];
                }
            };
        }

        public static class ProjectApplianceBean {

            /**
             * appliance_id : 187
             * appliance_name : App 2
             * appliance_brand : Asko
             * appliance_avatar :
             * purchase_date : 2017-08-01T00:00:00+00:00
             * warranty_expiration_date : 2017-12-08T00:00:00+00:00
             * extended_warranty_date :
             * property_appliance_images : [{"appliance_image_id":221,"appliance_avatar":"http://ohpstaging.mobilytedev.com/img/appliance_picture/original_image/yACJpYUkTmE3fcb.png"},{"appliance_image_id":222,"appliance_avatar":"http://ohpstaging.mobilytedev.com/img/appliance_picture/original_image/fI4FypcY5koneRu.jpg"}]
             * property_appliance_doc : [{"appliance_doc_id":264,"appliance_doc_avatar":"http://ohpstaging.mobilytedev.com/img/document_picture/original_image/irVhyPN6mxDK5sm.png"},{"appliance_doc_id":265,"appliance_doc_avatar":"http://ohpstaging.mobilytedev.com/img/document_picture/original_image/KfNqH3JyiJj0gzl.png"}]
             */

            @SerializedName("appliance_id")
            private int applianceId;
            @SerializedName("appliance_name")
            private String applianceName;
            @SerializedName("appliance_brand")
            private String applianceBrand;
            @SerializedName("appliance_avatar")
            private String applianceAvatar;
            @SerializedName("purchase_date")
            private String purchaseDate;
            @SerializedName("warranty_expiration_date")
            private String warrantyExpirationDate;
            @SerializedName("extended_warranty_date")
            private String extendedWarrantyDate;
            @SerializedName("property_appliance_images")
            private List<PropertyApplianceImagesBean> propertyApplianceImages;
            @SerializedName("property_appliance_doc")
            private List<PropertyApplianceDocBean> propertyApplianceDoc;

            public int getApplianceId() {
                return applianceId;
            }

            public void setApplianceId(int applianceId) {
                this.applianceId = applianceId;
            }

            public String getApplianceName() {
                return applianceName;
            }

            public void setApplianceName(String applianceName) {
                this.applianceName = applianceName;
            }

            public String getApplianceBrand() {
                return applianceBrand;
            }

            public void setApplianceBrand(String applianceBrand) {
                this.applianceBrand = applianceBrand;
            }

            public String getApplianceAvatar() {
                return applianceAvatar;
            }

            public void setApplianceAvatar(String applianceAvatar) {
                this.applianceAvatar = applianceAvatar;
            }

            public String getPurchaseDate() {
                return purchaseDate;
            }

            public void setPurchaseDate(String purchaseDate) {
                this.purchaseDate = purchaseDate;
            }

            public String getWarrantyExpirationDate() {
                return warrantyExpirationDate;
            }

            public void setWarrantyExpirationDate(String warrantyExpirationDate) {
                this.warrantyExpirationDate = warrantyExpirationDate;
            }

            public String getExtendedWarrantyDate() {
                return extendedWarrantyDate;
            }

            public void setExtendedWarrantyDate(String extendedWarrantyDate) {
                this.extendedWarrantyDate = extendedWarrantyDate;
            }

            public List<PropertyApplianceImagesBean> getPropertyApplianceImages() {
                return propertyApplianceImages;
            }

            public void setPropertyApplianceImages(List<PropertyApplianceImagesBean> propertyApplianceImages) {
                this.propertyApplianceImages = propertyApplianceImages;
            }

            public List<PropertyApplianceDocBean> getPropertyApplianceDoc() {
                return propertyApplianceDoc;
            }

            public void setPropertyApplianceDoc(List<PropertyApplianceDocBean> propertyApplianceDoc) {
                this.propertyApplianceDoc = propertyApplianceDoc;
            }

            public static class PropertyApplianceImagesBean {
                /**
                 * appliance_image_id : 221
                 * appliance_avatar : http://ohpstaging.mobilytedev.com/img/appliance_picture/original_image/yACJpYUkTmE3fcb.png
                 */

                @SerializedName("appliance_image_id")
                private int applianceImageId;
                @SerializedName("appliance_avatar")
                private String applianceAvatar;

                public int getApplianceImageId() {
                    return applianceImageId;
                }

                public void setApplianceImageId(int applianceImageId) {
                    this.applianceImageId = applianceImageId;
                }

                public String getApplianceAvatar() {
                    return applianceAvatar;
                }

                public void setApplianceAvatar(String applianceAvatar) {
                    this.applianceAvatar = applianceAvatar;
                }
            }

            public static class PropertyApplianceDocBean {
                /**
                 * appliance_doc_id : 264
                 * appliance_doc_avatar : http://ohpstaging.mobilytedev.com/img/document_picture/original_image/irVhyPN6mxDK5sm.png
                 */

                @SerializedName("appliance_doc_id")
                private int applianceDocId;
                @SerializedName("appliance_doc_avatar")
                private String applianceDocAvatar;

                public int getApplianceDocId() {
                    return applianceDocId;
                }

                public void setApplianceDocId(int applianceDocId) {
                    this.applianceDocId = applianceDocId;
                }

                public String getApplianceDocAvatar() {
                    return applianceDocAvatar;
                }

                public void setApplianceDocAvatar(String applianceDocAvatar) {
                    this.applianceDocAvatar = applianceDocAvatar;
                }
            }
        }

        public static class ProjectJobtypeBean {
            /**
             * jobtype_id : 1
             * jobtype_title : Repair
             */

            @SerializedName("jobtype_id")
            private int jobtypeId;
            @SerializedName("jobtype_title")
            private String jobtypeTitle;

            public int getJobtypeId() {
                return jobtypeId;
            }

            public void setJobtypeId(int jobtypeId) {
                this.jobtypeId = jobtypeId;
            }

            public String getJobtypeTitle() {
                return jobtypeTitle;
            }

            public void setJobtypeTitle(String jobtypeTitle) {
                this.jobtypeTitle = jobtypeTitle;
            }
        }

        public static class MyBidBean {

            /**
             * bid_id : 0
             * bid_amount : 0
             * bid_note :
             */

            @SerializedName("bid_id")
            private int bidId;
            @SerializedName("bid_amount")
            private int bidAmount;
            @SerializedName("bid_note")
            private String bidNote;
            @SerializedName("bid_status")
            private int bidStatus;

            public int getBidStatus() {
                return bidStatus;
            }

            public void setBidStatus(int bidStatus) {
                this.bidStatus = bidStatus;
            }

            public int getBidId() {
                return bidId;
            }

            public void setBidId(int bidId) {
                this.bidId = bidId;
            }

            public int getBidAmount() {
                return bidAmount;
            }

            public void setBidAmount(int bidAmount) {
                this.bidAmount = bidAmount;
            }

            public String getBidNote() {
                return bidNote;
            }

            public void setBidNote(String bidNote) {
                this.bidNote = bidNote;
            }
        }


        public static class ProjectImagesBean {
            /**
             * project_image_id : 160
             * project_image_avatar : http://onehomeportalqa.mobilytedev.com/img/project_picture/original_image/Oba4cQn0YOntGxW.png
             */

            @SerializedName("project_image_id")
            private int projectImageId;
            @SerializedName("project_image_avatar")
            private String projectImageAvatar;

            public int getProjectImageId() {
                return projectImageId;
            }

            public void setProjectImageId(int projectImageId) {
                this.projectImageId = projectImageId;
            }

            public String getProjectImageAvatar() {
                return projectImageAvatar;
            }

            public void setProjectImageAvatar(String projectImageAvatar) {
                this.projectImageAvatar = projectImageAvatar;
            }
        }

        public static class ProjectExpertiseBean {
            /**
             * expertise_id : 1
             * expertise_title : Cleaning Services
             */

            @SerializedName("expertise_id")
            private int expertiseId;
            @SerializedName("expertise_title")
            private String expertiseTitle;

            public int getExpertiseId() {
                return expertiseId;
            }

            public void setExpertiseId(int expertiseId) {
                this.expertiseId = expertiseId;
            }

            public String getExpertiseTitle() {
                return expertiseTitle;
            }

            public void setExpertiseTitle(String expertiseTitle) {
                this.expertiseTitle = expertiseTitle;
            }
        }

        public static class ProjectTasksBean {
            @SerializedName("completed_tasks")
            private List<TaskBean> completedTasks;
            @SerializedName("ongoing_tasks")
            private List<TaskBean> ongoingTasks;

            public List<TaskBean> getCompletedTasks() {
                return completedTasks;
            }

            public void setCompletedTasks(List<TaskBean> completedTasks) {
                this.completedTasks = completedTasks;
            }

            public List<TaskBean> getOngoingTasks() {
                return ongoingTasks;
            }

            public void setOngoingTasks(List<TaskBean> ongoingTasks) {
                this.ongoingTasks = ongoingTasks;
            }

            public static class TaskBean {

                /**
                 * task_id : 16
                 * task_name : Gcc
                 * task_notes : Fdch
                 * task_time_from : 2017-08-30T03:10:00+00:00
                 * task_time_to : 2017-08-30T04:00:00+00:00
                 * task_reminder : 2017-08-29T08:51:00+00:00
                 * task_repeat : 97
                 * created : 2017-08-28T06:25:25+00:00
                 * property : {"proeprty_id":0,"property_name":"Default Property","property_owner_name":"service"}
                 * task_images : [{"image_id":3,"image":"http://onehomeportal.mobilytedev.com/img/document_picture/original_image/nijwHzL3VfTucK5.jpg"}]
                 * member : {"member_id":29,"member_name":"Rohit1233"}
                 * project : {"project_id":83,"project_name":"Mandeepss"}
                 */

                @SerializedName("task_id")
                private int taskId;
                @SerializedName("task_name")
                private String taskName;
                @SerializedName("task_notes")
                private String taskNotes;
                @SerializedName("task_time_from")
                private String taskTimeFrom;
                @SerializedName("task_time_to")
                private String taskTimeTo;
                @SerializedName("task_reminder")
                private String taskReminder;
                @SerializedName("task_repeat")
                private int taskRepeat;
                @SerializedName("created")
                private String created;
                @SerializedName("property")
                private PropertyBean property;
                @SerializedName("member")
                private MemberBean member;
                @SerializedName("project")
                private ProjectBean project;
                @SerializedName("task_images")
                private List<TaskImagesBean> taskImages;

                public int getTaskId() {
                    return taskId;
                }

                public void setTaskId(int taskId) {
                    this.taskId = taskId;
                }

                public String getTaskName() {
                    return taskName;
                }

                public void setTaskName(String taskName) {
                    this.taskName = taskName;
                }

                public String getTaskNotes() {
                    return taskNotes;
                }

                public void setTaskNotes(String taskNotes) {
                    this.taskNotes = taskNotes;
                }

                public String getTaskTimeFrom() {
                    return taskTimeFrom;
                }

                public void setTaskTimeFrom(String taskTimeFrom) {
                    this.taskTimeFrom = taskTimeFrom;
                }

                public String getTaskTimeTo() {
                    return taskTimeTo;
                }

                public void setTaskTimeTo(String taskTimeTo) {
                    this.taskTimeTo = taskTimeTo;
                }

                public String getTaskReminder() {
                    return taskReminder;
                }

                public void setTaskReminder(String taskReminder) {
                    this.taskReminder = taskReminder;
                }

                public int getTaskRepeat() {
                    return taskRepeat;
                }

                public void setTaskRepeat(int taskRepeat) {
                    this.taskRepeat = taskRepeat;
                }

                public String getCreated() {
                    return created;
                }

                public void setCreated(String created) {
                    this.created = created;
                }

                public PropertyBean getProperty() {
                    return property;
                }

                public void setProperty(PropertyBean property) {
                    this.property = property;
                }

                public MemberBean getMember() {
                    return member;
                }

                public void setMember(MemberBean member) {
                    this.member = member;
                }

                public ProjectBean getProject() {
                    return project;
                }

                public void setProject(ProjectBean project) {
                    this.project = project;
                }

                public List<TaskImagesBean> getTaskImages() {
                    return taskImages;
                }

                public void setTaskImages(List<TaskImagesBean> taskImages) {
                    this.taskImages = taskImages;
                }

                public static class PropertyBean {
                    /**
                     * proeprty_id : 0
                     * property_name : Default Property
                     * property_owner_name : service
                     */

                    @SerializedName("proeprty_id")
                    private int proeprtyId;
                    @SerializedName("property_name")
                    private String propertyName;
                    @SerializedName("property_owner_name")
                    private String propertyOwnerName;

                    public int getProeprtyId() {
                        return proeprtyId;
                    }

                    public void setProeprtyId(int proeprtyId) {
                        this.proeprtyId = proeprtyId;
                    }

                    public String getPropertyName() {
                        return propertyName;
                    }

                    public void setPropertyName(String propertyName) {
                        this.propertyName = propertyName;
                    }

                    public String getPropertyOwnerName() {
                        return propertyOwnerName;
                    }

                    public void setPropertyOwnerName(String propertyOwnerName) {
                        this.propertyOwnerName = propertyOwnerName;
                    }
                }

                public static class MemberBean {
                    /**
                     * member_id : 29
                     * member_name : Rohit1233
                     */

                    @SerializedName("member_id")
                    private int memberId;
                    @SerializedName("member_name")
                    private String memberName;

                    public int getMemberId() {
                        return memberId;
                    }

                    public void setMemberId(int memberId) {
                        this.memberId = memberId;
                    }

                    public String getMemberName() {
                        return memberName;
                    }

                    public void setMemberName(String memberName) {
                        this.memberName = memberName;
                    }
                }

                public static class ProjectBean {
                    /**
                     * project_id : 83
                     * project_name : Mandeepss
                     */

                    @SerializedName("project_id")
                    private int projectId;
                    @SerializedName("project_name")
                    private String projectName;

                    public int getProjectId() {
                        return projectId;
                    }

                    public void setProjectId(int projectId) {
                        this.projectId = projectId;
                    }

                    public String getProjectName() {
                        return projectName;
                    }

                    public void setProjectName(String projectName) {
                        this.projectName = projectName;
                    }
                }

                public static class TaskImagesBean {
                    /**
                     * image_id : 3
                     * image : http://onehomeportal.mobilytedev.com/img/document_picture/original_image/nijwHzL3VfTucK5.jpg
                     */

                    @SerializedName("image_id")
                    private int imageId;
                    @SerializedName("image")
                    private String image;

                    public int getImageId() {
                        return imageId;
                    }

                    public void setImageId(int imageId) {
                        this.imageId = imageId;
                    }

                    public String getImage() {
                        return image;
                    }

                    public void setImage(String image) {
                        this.image = image;
                    }
                }
            }
        }
    }
}
