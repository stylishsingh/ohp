package com.ohp.commonviews.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class ServiceProvidersModel extends GenericBean implements Parcelable {


    public static final Creator<ServiceProvidersModel> CREATOR = new Creator<ServiceProvidersModel>() {
        @Override
        public ServiceProvidersModel createFromParcel(Parcel source) {
            return new ServiceProvidersModel(source);
        }

        @Override
        public ServiceProvidersModel[] newArray(int size) {
            return new ServiceProvidersModel[size];
        }
    };
    @SerializedName("data")
    private List<DataBean> data;
    /**
     * totalRecord : 4
     */

    @SerializedName("totalRecord")
    private int totalRecord;

    public ServiceProvidersModel() {
    }

    protected ServiceProvidersModel(Parcel in) {
        this.data = new ArrayList<>();
        in.readList(this.data, DataBean.class.getClassLoader());
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.data);
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public static class DataBean implements Parcelable {

        /**
         * id : 285
         * role : 2
         * full_name : Aditi Gupta
         * username :
         * gender : female
         * email : aditi@yopmail.com
         * address : Chicago, IL, USA
         * is_licensed : 1
         * license_no : 97979797
         * business_name : new comp
         * insured : 1
         * website :
         * bonded : 0
         * latitude :
         * longitude :
         * profile_image_100X100 :
         * profile_image_200X200 :
         * away_in_kms :
         * expertise : [{"expertise_id":"1","expertise_title":"Cleaning Services"},{"expertise_id":"2","expertise_title":"Flooring & Carpets"},{"expertise_id":"3","expertise_title":"Garden/Landscaping"}]
         */

        @SerializedName("user_id")
        private int user_id;
        @SerializedName("role")
        private int role;
        @SerializedName("avg_rating")
        private int avgRating;
        @SerializedName("full_name")
        private String fullName;
        @SerializedName("username")
        private String username;
        @SerializedName("gender")
        private String gender;
        @SerializedName("email")
        private String email;
        @SerializedName("address")
        private String address;
        @SerializedName("is_licensed")
        private int isLicensed;
        @SerializedName("license_no")
        private String licenseNo;
        @SerializedName("business_name")
        private String businessName;
        @SerializedName("insured")
        private int insured;
        @SerializedName("website")
        private String website;
        @SerializedName("bonded")
        private int bonded;
        @SerializedName("latitude")
        private String latitude;
        @SerializedName("longitude")
        private String longitude;
        @SerializedName("profile_image_100X100")
        private String profileImage100X100;
        @SerializedName("profile_image_200X200")
        private String profileImage200X200;
        @SerializedName("away_in_kms")
        private String awayInKms;
        @SerializedName("expertise")
        private List<ExpertiseBean> expertise;

        public DataBean() {
        }

        public int getAvgRating() {
            return avgRating;
        }

        public void setAvgRating(int avgRating) {
            this.avgRating = avgRating;
        }

        public int getId() {
            return user_id;
        }

        public void setId(int id) {
            this.user_id = id;
        }

        public int getRole() {
            return role;
        }

        public void setRole(int role) {
            this.role = role;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getIsLicensed() {
            return isLicensed;
        }

        public void setIsLicensed(int isLicensed) {
            this.isLicensed = isLicensed;
        }

        public String getLicenseNo() {
            return licenseNo;
        }

        public void setLicenseNo(String licenseNo) {
            this.licenseNo = licenseNo;
        }

        public String getBusinessName() {
            return businessName;
        }

        public void setBusinessName(String businessName) {
            this.businessName = businessName;
        }

        public int getInsured() {
            return insured;
        }

        public void setInsured(int insured) {
            this.insured = insured;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public int getBonded() {
            return bonded;
        }

        public void setBonded(int bonded) {
            this.bonded = bonded;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getProfileImage100X100() {
            return profileImage100X100;
        }

        public void setProfileImage100X100(String profileImage100X100) {
            this.profileImage100X100 = profileImage100X100;
        }

        public String getProfileImage200X200() {
            return profileImage200X200;
        }

        public void setProfileImage200X200(String profileImage200X200) {
            this.profileImage200X200 = profileImage200X200;
        }

        public String getAwayInKms() {
            return awayInKms;
        }

        public void setAwayInKms(String awayInKms) {
            this.awayInKms = awayInKms;
        }

        public List<ExpertiseBean> getExpertise() {
            return expertise;
        }

        public void setExpertise(List<ExpertiseBean> expertise) {
            this.expertise = expertise;
        }

        public static class ExpertiseBean implements Parcelable {
            public static final Creator<ExpertiseBean> CREATOR = new Creator<ExpertiseBean>() {
                @Override
                public ExpertiseBean createFromParcel(Parcel source) {
                    return new ExpertiseBean(source);
                }

                @Override
                public ExpertiseBean[] newArray(int size) {
                    return new ExpertiseBean[size];
                }
            };
            /**
             * expertise_id : 1
             * expertise_title : Cleaning Services
             */

            @SerializedName("expertise_id")
            private String expertiseId;
            @SerializedName("expertise_title")
            private String expertiseTitle;

            public ExpertiseBean() {
            }

            protected ExpertiseBean(Parcel in) {
                this.expertiseId = in.readString();
                this.expertiseTitle = in.readString();
            }

            public String getExpertiseId() {
                return expertiseId;
            }

            public void setExpertiseId(String expertiseId) {
                this.expertiseId = expertiseId;
            }

            public String getExpertiseTitle() {
                return expertiseTitle;
            }

            public void setExpertiseTitle(String expertiseTitle) {
                this.expertiseTitle = expertiseTitle;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.expertiseId);
                dest.writeString(this.expertiseTitle);
            }
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.user_id);
            dest.writeInt(this.role);
            dest.writeInt(this.avgRating);
            dest.writeString(this.fullName);
            dest.writeString(this.username);
            dest.writeString(this.gender);
            dest.writeString(this.email);
            dest.writeString(this.address);
            dest.writeInt(this.isLicensed);
            dest.writeString(this.licenseNo);
            dest.writeString(this.businessName);
            dest.writeInt(this.insured);
            dest.writeString(this.website);
            dest.writeInt(this.bonded);
            dest.writeString(this.latitude);
            dest.writeString(this.longitude);
            dest.writeString(this.profileImage100X100);
            dest.writeString(this.profileImage200X200);
            dest.writeString(this.awayInKms);
            dest.writeTypedList(this.expertise);
        }

        protected DataBean(Parcel in) {
            this.user_id = in.readInt();
            this.role = in.readInt();
            this.avgRating = in.readInt();
            this.fullName = in.readString();
            this.username = in.readString();
            this.gender = in.readString();
            this.email = in.readString();
            this.address = in.readString();
            this.isLicensed = in.readInt();
            this.licenseNo = in.readString();
            this.businessName = in.readString();
            this.insured = in.readInt();
            this.website = in.readString();
            this.bonded = in.readInt();
            this.latitude = in.readString();
            this.longitude = in.readString();
            this.profileImage100X100 = in.readString();
            this.profileImage200X200 = in.readString();
            this.awayInKms = in.readString();
            this.expertise = in.createTypedArrayList(ExpertiseBean.CREATOR);
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }
}
