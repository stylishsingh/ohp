package com.ohp.commonviews.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class ReviewModel extends GenericBean {

    @SerializedName("totalRecord")
    private int totalRecord;

    @SerializedName("data")
    private List<DataBean> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * rating_id : 2
         * rating : 5
         * feedback : Test feedback
         * rating_by : Amanpal Singh
         * profile_image_100X100 : https://lh6.googleusercontent.com/-OBDdmnMwOOM/AAAAAAAAAAI/AAAAAAAAAB4/pYjLxaXAFDI/photo.jpg
         * profile_image_200X200 : https://lh6.googleusercontent.com/-OBDdmnMwOOM/AAAAAAAAAAI/AAAAAAAAAB4/pYjLxaXAFDI/photo.jpg
         */

        @SerializedName("rating_id")
        private int ratingId;
        @SerializedName("rating")
        private int rating;
        @SerializedName("feedback")
        private String feedback;
        @SerializedName("rating_by")
        private String ratingBy;
        @SerializedName("profile_image_100X100")
        private String profileImage100X100;
        @SerializedName("profile_image_200X200")
        private String profileImage200X200;

        public int getRatingId() {
            return ratingId;
        }

        public void setRatingId(int ratingId) {
            this.ratingId = ratingId;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public String getFeedback() {
            return feedback;
        }

        public void setFeedback(String feedback) {
            this.feedback = feedback;
        }

        public String getRatingBy() {
            return ratingBy;
        }

        public void setRatingBy(String ratingBy) {
            this.ratingBy = ratingBy;
        }

        public String getProfileImage100X100() {
            return profileImage100X100;
        }

        public void setProfileImage100X100(String profileImage100X100) {
            this.profileImage100X100 = profileImage100X100;
        }

        public String getProfileImage200X200() {
            return profileImage200X200;
        }

        public void setProfileImage200X200(String profileImage200X200) {
            this.profileImage200X200 = profileImage200X200;
        }
    }
}
