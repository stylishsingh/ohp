package com.ohp.commonviews.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Anuj Sharma on 6/2/2017.
 */

public class GetPropertyResponse extends GenericBean{


    @SerializedName("data")
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : Mandeepss
         * value : 85
         * property_id : 0
         * property_name : Default Property
         * appliance_id : 0
         * appliance_name : GHh
         */

        @SerializedName("title")
        private String title;
        @SerializedName("value")
        private int value;
        @SerializedName("property_id")
        private int propertyId;
        @SerializedName("property_name")
        private String propertyName;
        @SerializedName("appliance_id")
        private int applianceId;
        @SerializedName("appliance_name")
        private String applianceName;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public int getPropertyId() {
            return propertyId;
        }

        public void setPropertyId(int propertyId) {
            this.propertyId = propertyId;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public int getApplianceId() {
            return applianceId;
        }

        public void setApplianceId(int applianceId) {
            this.applianceId = applianceId;
        }

        public String getApplianceName() {
            return applianceName;
        }

        public void setApplianceName(String applianceName) {
            this.applianceName = applianceName;
        }
    }
}
