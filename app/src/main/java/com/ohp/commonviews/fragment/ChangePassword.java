package com.ohp.commonviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.presenter.ChangePasswordPresenterImpl;
import com.ohp.commonviews.view.ChangePasswordView;

/**
 * @author Vishal Sharma.
 */

public class ChangePassword extends BaseFragment<ChangePasswordPresenterImpl> implements ChangePasswordView {
    private View rootView;
    private Toolbar mToolbar;
    private TextView toolbarTitle;
    private TextInputEditText etNewPwd, etConfirmPwd, etOldPwd;
    private AppCompatButton btnConfirm;
    private TextInputLayout et_textInputLayout;

    @Override
    protected ChangePasswordPresenterImpl onAttachPresenter() {
        return new ChangePasswordPresenterImpl(this, getActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_reset_pwd, container, false);
        return rootView;
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setVisibility(View.GONE);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        etNewPwd = (TextInputEditText) view.findViewById(R.id.et_new_password);
        etConfirmPwd = (TextInputEditText) view.findViewById(R.id.et_confirm_password);
        etOldPwd = (TextInputEditText) view.findViewById(R.id.et_old_password);
        et_textInputLayout = (TextInputLayout) view.findViewById(R.id.till_password);
        et_textInputLayout.setVisibility(View.VISIBLE);
        btnConfirm = (AppCompatButton) view.findViewById(R.id.btn_confirm);
        btnConfirm.setOnClickListener(getPresenter());
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public TextInputEditText getOldPassword() {
        return etOldPwd;
    }

    @Override
    public TextInputEditText getNewPassword() {
        return etNewPwd;
    }

    @Override
    public TextInputEditText getConfirmPassword() {
        return etConfirmPwd;
    }

    @Override
    public AppCompatButton getConfirmBtn() {
        return btnConfirm;
    }
}
