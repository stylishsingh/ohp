package com.ohp.commonviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.presenter.VerifyOTPFragPresenterImpl;
import com.ohp.commonviews.view.VerifyOTPView;

/**
 * @author anuj.sharma
 */

public class VerifyOTPFragment extends BaseFragment<VerifyOTPFragPresenterImpl> implements VerifyOTPView {
    private View rootView;
    private Toolbar mToolbar;
    private TextView toolbarTitle;
    private TextInputEditText etVerificationCode;
    private AppCompatButton btnConfirm;
    private TextView tvResend;

    @Override
    protected VerifyOTPFragPresenterImpl onAttachPresenter() {
        return new VerifyOTPFragPresenterImpl(this,getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_verify_otp, container, false);
        return rootView;
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        etVerificationCode = (TextInputEditText) view.findViewById(R.id.et_verification_code);
        btnConfirm = (AppCompatButton) view.findViewById(R.id.btn_confirm);
        tvResend = (TextView) view.findViewById(R.id.tv_resend);
        btnConfirm.setOnClickListener(getPresenter());
        tvResend.setOnClickListener(getPresenter());

        if(getArguments()!=null && getArguments().getString("access_token")!=null){
            Bundle bundle = getArguments();
            getPresenter().saveBundleInfo(bundle);
        }
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public TextInputEditText getVerificationCode() {
        return etVerificationCode;
    }

    @Override
    public AppCompatButton getConfirmBtn() {
        return btnConfirm;
    }

    @Override
    public TextView getResendBtn() {
        return tvResend;
    }
}
