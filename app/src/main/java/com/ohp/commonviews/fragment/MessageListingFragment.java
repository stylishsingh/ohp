package com.ohp.commonviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ohp.R;
import com.ohp.commonviews.presenter.MessageListingFragmentPresenter;
import com.ohp.commonviews.view.MessageListingFragmentView;

/**
 * Created by vishal.sharma on 9/7/2017.
 */

public class MessageListingFragment extends BaseFragment<MessageListingFragmentPresenter> implements MessageListingFragmentView {
    private View rooView;
    private RecyclerView recyclerView;
    private RelativeLayout relativeLayout;
    private EditText etSearch;
    private LinearLayout btnClear;
    private boolean isMenu = false;
    private NestedScrollView nsRecyclerView;
    @Override
    protected MessageListingFragmentPresenter onAttachPresenter() {
        return new MessageListingFragmentPresenter(this, getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rooView = inflater.inflate(R.layout.fragment_message_layout_list, container, false);
        return rooView;
    }

    @Override
    protected void initUI(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_task);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.empty_list_layout);
        nsRecyclerView = (NestedScrollView) view.findViewById(R.id.ns_recycler_view);
        etSearch = (EditText) view.findViewById(R.id.et_search);
        btnClear = (LinearLayout) view.findViewById(R.id.btn_clear);
        btnClear.setOnClickListener(getPresenter());
    }

    @Override
    public View getRootView() {
        return rooView;
    }

    @Override
    public RecyclerView getRecyclerTaskList() {
        return recyclerView;
    }

    @Override
    public RelativeLayout getEmptyLayout() {
        return relativeLayout;
    }

    @Override
    public NestedScrollView getNSRecyclerView() {
        return nsRecyclerView;
    }

    @Override
    public EditText getSearchEditText() {
        return etSearch;
    }

    @Override
    public LinearLayout getCloseBtn() {
        return btnClear;
    }

}
