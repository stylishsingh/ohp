package com.ohp.commonviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.presenter.ForgotPwdFragmentImpl;
import com.ohp.commonviews.view.ForgotPwdView;

/**
 * Created by Anuj Sharma on 5/19/2017.
 */

public class ForgotPwdFragment extends BaseFragment<ForgotPwdFragmentImpl> implements ForgotPwdView {
    private View rootView;
    private Toolbar mToolbar;
    private TextView toolbarTitle;
    private LinearLayout verificationLayout;
    private TextInputEditText etEmail, etVerificationCode;
    private AppCompatButton btnSendCode, btnConfirm;

    @Override
    protected ForgotPwdFragmentImpl onAttachPresenter() {
        return new ForgotPwdFragmentImpl(this, getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_forgot_pwd, container, false);
        return rootView;
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        verificationLayout = (LinearLayout) view.findViewById(R.id.layout_verification_code);
        etEmail = (TextInputEditText) view.findViewById(R.id.et_email);
        etVerificationCode = (TextInputEditText) view.findViewById(R.id.et_verification_code);
        btnSendCode = (AppCompatButton) view.findViewById(R.id.btn_send_verification);
        btnConfirm = (AppCompatButton) view.findViewById(R.id.btn_confirm);

        btnSendCode.setOnClickListener(getPresenter());
        btnConfirm.setOnClickListener(getPresenter());
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public LinearLayout getVerificationLayout() {
        return verificationLayout;
    }

    @Override
    public TextInputEditText getEmail() {
        return etEmail;
    }

    @Override
    public TextInputEditText getVerificationCode() {
        return etVerificationCode;
    }

    @Override
    public AppCompatButton getSendVerificationBtn() {
        return btnSendCode;
    }

    @Override
    public AppCompatButton getConfirmBtn() {
        return btnConfirm;
    }


}
