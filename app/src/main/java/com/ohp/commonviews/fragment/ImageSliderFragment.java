package com.ohp.commonviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.ImageSliderAdapter;
import com.ohp.commonviews.beans.ThumbnailBean;
import com.ohp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import static com.ohp.utils.Constants.BROWSE_PIC;
import static com.ohp.utils.Constants.IS_SLIDER;

/**
 * @author Amanpal Singh.
 */

public class ImageSliderFragment extends Fragment {


    private View rootView;
    private ViewPager viewPager;
    private Toolbar mToolbar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_image_slider, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Initializing getViewPager
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        if (getActivity() instanceof HolderActivity) {
            ((HolderActivity) getActivity()).setSupportActionBar(mToolbar);
            mToolbar.setNavigationIcon(R.drawable.ic_navigation_back);
            if (((HolderActivity) getActivity()).getSupportActionBar() != null)
                mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((HolderActivity) getActivity()).oneStepBack();
                    }
                });
        }
        int position;
        List<Fragment> fragments;
        ImageSliderAdapter mAdapter;
        List<ThumbnailBean> imagesList;
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        try {
            if (getArguments() != null) {
                imagesList = getArguments().getParcelableArrayList(Constants.IMAGE_LIST);
                position = getArguments().getInt(Constants.POSITION);
                fragments = buildFragment(imagesList, getArguments());
                mAdapter = new ImageSliderAdapter(getActivity().getSupportFragmentManager(), fragments);
                viewPager.setAdapter(mAdapter);
                if (imagesList != null) {
                    //                viewPager.setOffscreenPageLimit(imagesList.size());
                    viewPager.setCurrentItem(position);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private List<Fragment> buildFragment(List<ThumbnailBean> imagesList, Bundle bundle1) {


        List<Fragment> fragment = new ArrayList<>();

        for (int i = 0; i < imagesList.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.PIC_TITLE, imagesList.get(i).getName());
            bundle.putString(BROWSE_PIC, imagesList.get(i).getImagePath());
            bundle.putBoolean(IS_SLIDER, bundle1.getBoolean(IS_SLIDER));
            fragment.add(ViewImagesFragment.instantiate(getActivity(), ViewImagesFragment.class.getName(), bundle));
        }

        return fragment;
    }
}
