package com.ohp.commonviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.activities.LoginActivity;
import com.ohp.utils.Constants;


public class SettingFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_setting, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        WebView webView = view.findViewById(R.id.webView);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        TextView toolbarTitle = view.findViewById(R.id.toolbar_title);

        try {
            if (getArguments() != null) {
                String url = getArguments().getString(Constants.URL);

                System.out.println("SettingFragment.onViewCreated URL----" + url);

                /*if (activity is OwnerDashboardActivity) {
                    (activity as OwnerDashboardActivity).setSupportActionBar(toolbar)

                    (activity as OwnerDashboardActivity).setSupportActionBar(toolbar)
                    (activity as OwnerDashboardActivity).title = ""
                    toolbar.title = ""
                    toolbarTitle.setText(R.string.title_appliance_list)
                    toolbar.setNavigationIcon(R.drawable.ic_navigation_back)

                    toolbar.setNavigationOnClickListener {
                        Utils.getInstance().hideSoftKeyboard(activity, getView()!!.rootView)
                        (activity as OwnerDashboardActivity).oneStepBack()
                    }

                }else */
                if (getActivity() instanceof LoginActivity) {
                    toolbar.setVisibility(View.VISIBLE);
                    ((LoginActivity) getActivity()).setSupportActionBar(toolbar);
                    ((LoginActivity) getActivity()).setSupportActionBar(toolbar);
                    ((LoginActivity) getActivity()).setTitle("");
                    toolbar.setTitle("");
                    toolbarTitle.setText(R.string.terms_and_conditions);
                    toolbar.setNavigationIcon(R.drawable.ic_navigation_back);

                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((LoginActivity) getActivity()).oneStepBack();
                        }
                    });

                } /*else if (activity is SPDashboardActivity) {
                    (activity as SPDashboardActivity).setSupportActionBar(toolbar)
                    (activity as SPDashboardActivity).title = ""
                    toolbar.title = ""
                    toolbarTitle.setText(R.string.title_appliance_list)
                    toolbar.setNavigationIcon(R.drawable.ic_navigation_back)

                    toolbar.setNavigationOnClickListener {
                        Utils.getInstance().hideSoftKeyboard(activity, getView()!!.rootView)
                        (activity as SPDashboardActivity).oneStepBack()
                    }

                }*/

                webView.getSettings().setJavaScriptEnabled(true);
                webView.loadUrl(url);
                webView.setWebViewClient(new WebViewController());


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private class WebViewController extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
