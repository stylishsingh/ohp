package com.ohp.commonviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.presenter.ReviewsFragmentPresenterImpl;
import com.ohp.commonviews.view.ReviewsView;

/**
 * @author Amanpal Singh.
 */

public class ReviewsFragment extends BaseFragment<ReviewsFragmentPresenterImpl> implements ReviewsView {

    private RecyclerView recyclerView;
    private RelativeLayout emptyView;
    private EditText etSearch;
    private TextView toolbarTitle;
    private Toolbar mToolbar;
    private LinearLayout btnClear;
    private NestedScrollView nsRecyclerView;
    private View rootView;

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
    }

    @Override
    protected ReviewsFragmentPresenterImpl onAttachPresenter() {
        return new ReviewsFragmentPresenterImpl(this, getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_reviews, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((HolderActivity) getActivity()).setSupportActionBar(mToolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        nsRecyclerView = (NestedScrollView) view.findViewById(R.id.ns_recycler_view);
        emptyView = (RelativeLayout) view.findViewById(R.id.empty_list_layout);
        etSearch = (EditText) view.findViewById(R.id.et_search);
        btnClear = (LinearLayout) view.findViewById(R.id.btn_clear);
        btnClear.setOnClickListener(getPresenter());
        recyclerView.setAdapter(getPresenter().initAdapter());

        if (getArguments() != null) {
            getPresenter().saveBundleInfo(getArguments());
        }
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public RelativeLayout getEmptyView() {
        return emptyView;
    }

    @Override
    public EditText getSearchEditText() {
        return etSearch;
    }

    @Override
    public LinearLayout getCloseBtn() {
        return btnClear;
    }

    @Override
    public NestedScrollView getNSRecyclerView() {
        return nsRecyclerView;
    }
}
