package com.ohp.commonviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.utils.Constants;
import com.ohp.utils.TouchImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;

import static com.ohp.utils.Constants.BROWSE_PIC;
import static com.ohp.utils.Constants.PIC_TITLE;


/**
 * @author Anuj Sharma.
 * @author Amanpal Singh.
 */

public class ViewImagesFragment extends Fragment {
    private View rootView;
    private Toolbar mToolbar;
    private TouchImageView mProfileImage;
    private ProgressBar mProgressBar;
    private String profilePicUrl = "", imageTitle = "";

    //request code
    private int IMAGE_CAPTURE_REQUEST_CODE = 0;
    private int IMAGE_CHOOSER_REQUEST_CODE = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile_pic, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        boolean isSlider = false;
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        try {
            if (getArguments() != null && getArguments().getString(BROWSE_PIC) != null) {
                profilePicUrl = getArguments().getString(BROWSE_PIC);
                imageTitle = getArguments().getString(PIC_TITLE);
                if (getArguments().containsKey(Constants.IS_SLIDER))
                    isSlider = getArguments().getBoolean(Constants.IS_SLIDER);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!isSlider) {
            mToolbar.setVisibility(View.VISIBLE);
            if (getActivity() instanceof HolderActivity) {
                ((HolderActivity) getActivity()).setSupportActionBar(mToolbar);
                mToolbar.setNavigationIcon(R.drawable.ic_navigation_back);
                if (((HolderActivity) getActivity()).getSupportActionBar() != null)
                    ((HolderActivity) getActivity()).getSupportActionBar().setTitle(imageTitle);
                mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((HolderActivity) getActivity()).oneStepBack();
                    }
                });
            }
        }


        mProfileImage = (TouchImageView) rootView.findViewById(R.id.profile_pic);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);


        if (!TextUtils.isEmpty(profilePicUrl)) {
            mProgressBar.setVisibility(View.VISIBLE);

            if (profilePicUrl.startsWith("http")) {
                Picasso.with(getActivity()).load(profilePicUrl).resize(500, 500).memoryPolicy(MemoryPolicy.NO_CACHE).into(mProfileImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        mProgressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
            } else {
                Picasso.with(getActivity()).load(new File(profilePicUrl)).resize(500, 500).memoryPolicy(MemoryPolicy.NO_CACHE).into(mProfileImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        mProgressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
            }

        }
    }

}
