package com.ohp.commonviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.presenter.ResetPwdFragmentImpl;
import com.ohp.commonviews.view.ResetPwdView;

/**
 * Created by Anuj Sharma on 5/25/2017.
 */

public class ResetPwdFragment extends BaseFragment<ResetPwdFragmentImpl> implements ResetPwdView{
    private View rootView;
    private Toolbar mToolbar;
    private TextView toolbarTitle;
    private TextInputEditText etNewPwd,etConfirmPwd;
    private AppCompatButton btnConfirm;

    @Override
    protected ResetPwdFragmentImpl onAttachPresenter() {
        return new ResetPwdFragmentImpl(this,getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_reset_pwd,container,false);
        return rootView;
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        etNewPwd = (TextInputEditText) view.findViewById(R.id.et_new_password);
        etConfirmPwd = (TextInputEditText) view.findViewById(R.id.et_confirm_password);
        btnConfirm = (AppCompatButton)view.findViewById(R.id.btn_confirm);
        btnConfirm.setOnClickListener(getPresenter());

        //get bundle data
        if(getArguments()!=null){
            Bundle bundle = getArguments();
            getPresenter().saveBundleInfo(bundle);
        }
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public TextInputEditText getNewPassword() {
        return etNewPwd;
    }

    @Override
    public TextInputEditText getConfirmPassword() {
        return etConfirmPwd;
    }

    @Override
    public AppCompatButton getConfirmBtn() {
        return btnConfirm;
    }
}
