package com.ohp.commonviews.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.presenter.LoginFragmentPresenterImpl;
import com.ohp.commonviews.view.LoginView;

public class LoginFragment extends BaseFragment<LoginFragmentPresenterImpl> implements LoginView {
    private View rootView;
    private TextInputEditText etEmail, etPassword;
    private TextView tvForgotPwd, tvSignup;
    private AppCompatButton btnLogin;
    private LinearLayout btnFacebook, btnGooglePlus;

    @Override
    protected LoginFragmentPresenterImpl onAttachPresenter() {
        return new LoginFragmentPresenterImpl(this, getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_login, container, false);
        return rootView;
    }

    @Override
    protected void initUI(View view) {
        etEmail = (TextInputEditText) view.findViewById(R.id.et_email);
        etPassword = (TextInputEditText) view.findViewById(R.id.et_password);
        btnLogin = (AppCompatButton) view.findViewById(R.id.btn_login);
        tvForgotPwd = (TextView) view.findViewById(R.id.tv_forgotPwd);
        tvSignup = (TextView) view.findViewById(R.id.tv_signup);
        btnFacebook = (LinearLayout) view.findViewById(R.id.btn_facebook);
        btnGooglePlus = (LinearLayout) view.findViewById(R.id.btn_google_plus);

        btnLogin.setOnClickListener(getPresenter());
        tvForgotPwd.setOnClickListener(getPresenter());
        tvSignup.setOnClickListener(getPresenter());
        btnFacebook.setOnClickListener(getPresenter());
        btnGooglePlus.setOnClickListener(getPresenter());

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public TextInputEditText getEmail() {
        return etEmail;
    }

    @Override
    public TextInputEditText getPassword() {
        return etPassword;
    }

    @Override
    public AppCompatButton getLoginBtn() {
        return btnLogin;
    }

    @Override
    public LinearLayout getFacebookBtn() {
        return btnFacebook;
    }

    @Override
    public LinearLayout getGooglePlusBtn() {
        return btnGooglePlus;
    }

    @Override
    public TextView getSignUpTextView() {
        return tvSignup;
    }
}
