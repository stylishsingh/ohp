package com.ohp.commonviews.fragment;

import android.os.Bundle;

import com.library.basecontroller.AppCompatFragment;
import com.library.mvp.FragmentPresenter;

public abstract class BaseFragment<T extends FragmentPresenter> extends AppCompatFragment<T> {


    Bundle mbundle;

    public Bundle getMbundle() {
        return mbundle;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setCustomArgus(Bundle customArgus) {
        mbundle = customArgus;
    }
}
