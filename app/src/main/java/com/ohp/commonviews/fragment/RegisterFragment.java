package com.ohp.commonviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.presenter.RegisterFragmentPresenterImpl;
import com.ohp.commonviews.view.RegisterView;

/**
 * @author Amanpal Singh.
 */

public class RegisterFragment extends BaseFragment<RegisterFragmentPresenterImpl> implements RegisterView {
    private View rootView;
    private Toolbar mToolbar;
    private TextView toolbarTitle, tvSignIn, tvTermsAndConditions;
    private TextInputEditText etEmail, etPassword, etConfirmPassword;
    private AppCompatButton btnRegister;
    private AppCompatCheckBox cbTermsAndConditions;

    @Override
    protected RegisterFragmentPresenterImpl onAttachPresenter() {
        return new RegisterFragmentPresenterImpl(this, getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_register, container, false);
        return rootView;
    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        etEmail = (TextInputEditText) view.findViewById(R.id.et_email);
        etPassword = (TextInputEditText) view.findViewById(R.id.et_password);
        etConfirmPassword = (TextInputEditText) view.findViewById(R.id.et_confirm_password);
        btnRegister = (AppCompatButton) view.findViewById(R.id.btn_register);
        tvSignIn = (TextView) view.findViewById(R.id.tv_login);
        tvTermsAndConditions = (TextView) view.findViewById(R.id.tv_terms_and_conditions);
        cbTermsAndConditions = (AppCompatCheckBox) view.findViewById(R.id.cb_terms_and_conditions);

        btnRegister.setOnClickListener(getPresenter());
        tvSignIn.setOnClickListener(getPresenter());
        tvTermsAndConditions.setOnClickListener(getPresenter());

        //Get data coming from login screen
        if (getArguments() != null && getArguments().getString("role") != null) {
            Bundle bundle = getArguments();
            getPresenter().saveBundleInfo(bundle);
        }
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }

    @Override
    public TextInputEditText getEmail() {
        return etEmail;
    }

    @Override
    public TextInputEditText getPassword() {
        return etPassword;
    }

    @Override
    public TextInputEditText getConfirmPassword() {
        return etConfirmPassword;
    }

    @Override
    public AppCompatButton getRegisterBtn() {
        return btnRegister;
    }

    @Override
    public TextView getSignInBtn() {
        return tvSignIn;
    }

    @Override
    public AppCompatCheckBox getTermsAndConditions() {
        return cbTermsAndConditions;
    }

    @Override
    public TextView getSignInView() {
        return tvSignIn;
    }

    @Override
    public TextView getTVTermsAndConditions() {
        return tvTermsAndConditions;
    }
}
