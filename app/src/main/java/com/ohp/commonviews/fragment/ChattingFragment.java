package com.ohp.commonviews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.presenter.ChattingFragmentPresenter;
import com.ohp.commonviews.view.ChattingFragmentView;
import com.ohp.utils.Constants;

/**
 * Created by vishal.sharma on 9/6/2017.
 */

public class ChattingFragment extends BaseFragment<ChattingFragmentPresenter> implements ChattingFragmentView {
    private View rootView;
    private RecyclerView recyclerView;
    private RelativeLayout relativeLayout, relativeLayoutBottomView;
    private SwipeRefreshLayout refreshLayout;
private String currentScreen = "";
    private EditText et_MessageText;
    private Toolbar mToolbar;
    private TextView toolbarTitle;
    private ImageView img_sendMessage;

    @Override
    protected ChattingFragmentPresenter onAttachPresenter() {
        return new ChattingFragmentPresenter(this, getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_chating_layout, container, false);
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_event, menu);

    }

    @Override
    protected void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        relativeLayoutBottomView = (RelativeLayout) view.findViewById(R.id.bottom_view);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.emptyLayout);
        et_MessageText = (EditText) view.findViewById(R.id.edt_sendText);
        img_sendMessage = (ImageView) view.findViewById(R.id.txt_send);
        img_sendMessage.setOnClickListener(getPresenter());
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        if (getArguments() != null) {
            //  getPresenter().getResource();
            getPresenter().saveBundleInfo(getArguments());
            currentScreen = getArguments().getString(Constants.DESTINATION, "");
        }
    }

    @Override
    public RelativeLayout getEmptyLayout() {
        return relativeLayout;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public RecyclerView getChatRecycler() {
        return recyclerView;
    }

    @Override
    public RelativeLayout getBottmView() {
        return relativeLayoutBottomView;
    }

    @Override
    public SwipeRefreshLayout getRefreshLayout() {
        return refreshLayout;
    }

    @Override
    public EditText getMessageText() {
        return et_MessageText;
    }

    @Override
    public ImageView getSendMessage() {
        return img_sendMessage;
    }


    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public TextView getToolbarTitle() {
        return toolbarTitle;
    }
}
