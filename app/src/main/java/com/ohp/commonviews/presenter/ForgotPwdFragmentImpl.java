package com.ohp.commonviews.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.transition.Explode;
import android.transition.TransitionManager;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.LoginActivity;
import com.ohp.commonviews.beans.LoginResponse;
import com.ohp.commonviews.view.ForgotPwdView;
import com.ohp.enums.ApiName;
import com.ohp.enums.CurrentScreen;
import com.ohp.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

/**
 * @author Amanpal Singh.
 */

public class ForgotPwdFragmentImpl extends FragmentPresenter<ForgotPwdView, APIHandler> implements APIResponseInterface.OnCallableResponse, View.OnClickListener {
    private String accessToken;

    public ForgotPwdFragmentImpl(ForgotPwdView forgotPwdView, Context context) {
        super(forgotPwdView, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && getView() != null) {
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            getView().getToolbarTitle().setText(R.string.title_forgot_password_fragment);
            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    ((LoginActivity) getActivity()).oneStepBack();
                }
            });

            //Initially hide verification layout, show after successfully email sent
            getView().getVerificationLayout().setVisibility(View.GONE);
        }

    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.btn_send_verification:
                    if (getView().getEmail().getText().toString().length() == 0) {
                        getActivity().showSnakBar(getView().getRootView(), "Please enter email");
                    } else if (!Patterns.EMAIL_ADDRESS.matcher(getView().getEmail().getText().toString()).matches()) {
                        getActivity().showSnakBar(getView().getRootView(), "Please enter valid email");
                    } else if (!Utils.getInstance().isInternetAvailable(getActivity())) {
                        getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                    } else {
                        //Hit LoginAPI on Success show verification view
                        Map<String, String> param = new HashMap<>();
                        param.put("email", getView().getEmail().getText().toString());
//
                        getActivity().showProgressDialog();
                        getInterActor().sendVerificationCode(param, ApiName.SEND_VERIFICATION_API);
                    }

                    break;
                case R.id.btn_confirm:
                    if (getActivity() != null && isViewAttached()) {
                        if (getView().getVerificationCode().getText().toString().length() == 0) {
                            getActivity().showSnakBar(getView().getRootView(), "Please enter Verification code");
                        } else if (!Utils.getInstance().isInternetAvailable(getActivity())) {
                            getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                        } else if (TextUtils.isEmpty(accessToken)) {
                            getActivity().showSnakBar(getView().getRootView(), "Otp is not sent yet!");
                        } else {
                            //Hit VERIFICATION API
                            Map<String, String> param = new HashMap<>();
                            param.put("access_token", accessToken);
                            param.put("navigate_type", "1"); // 1 for forgot password, 2 for signup
                            param.put("OTP", getView().getVerificationCode().getText().toString());
//
                            getActivity().showProgressDialog();
                            getInterActor().confirmVerificationCode(param, ApiName.CONFIRM_VERIFICATION_API);
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case SEND_VERIFICATION_API:
                        if (response.isSuccessful()) {
                            LoginResponse obj = (LoginResponse) response.body();
                            if (obj.getStatus() == 200) {
                                getActivity().showSnakBar(getView().getRootView(), obj.getMessage());
                                //Success, now show verification view
                                if (Utils.getInstance().isEqualLollipop()) {
                                    TransitionManager.beginDelayedTransition((ViewGroup) getView().getRootView(), new Explode());
                                }
                                accessToken = obj.getData().getAccessToken();
                                getView().getSendVerificationBtn().setText(R.string.title_resend_verification);
                                getView().getVerificationLayout().setVisibility(View.VISIBLE);
                            } else {
                                getActivity().showSnakBar(getView().getRootView(), obj.getMessage());

                            }
                        }
                        break;
                    case CONFIRM_VERIFICATION_API:
                        if (response.isSuccessful()) {
                            LoginResponse obj = (LoginResponse) response.body();
                            if (obj.getStatus() == 200) {
                                //Success, Move to Change Password Screen
                                Bundle bundle = new Bundle();
                                bundle.putString("access_token", obj.getData().getAccessToken());
                                getView().getVerificationCode().setText("");
                                getView().getEmail().setText("");
                                ((LoginActivity) getActivity()).changeScreen(R.id.login_container, CurrentScreen.CHANGE_PASSWORD_SCREEN, false, true, bundle);
                            } else {
                                getActivity().showSnakBar(getView().getRootView(), obj.getMessage());
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && getView() != null) {
                getActivity().hideProgressDialog();
                if (t != null) getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
