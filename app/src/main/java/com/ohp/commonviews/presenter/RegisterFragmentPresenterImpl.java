package com.ohp.commonviews.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.LoginActivity;
import com.ohp.commonviews.beans.RegisterResponse;
import com.ohp.commonviews.view.RegisterView;
import com.ohp.enums.ApiName;
import com.ohp.enums.CurrentScreen;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;
import com.ohp.utils.Validations;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

/**
 * @author Amanpal Singh.
 */

public class RegisterFragmentPresenterImpl extends FragmentPresenter<RegisterView, APIHandler> implements APIResponseInterface.OnCallableResponse, View.OnClickListener {
    private String role;

    public RegisterFragmentPresenterImpl(RegisterView view, Context context) {
        super(view, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && getView() != null) {
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            Spannable spannableSignIn = new SpannableString(getActivity().getString(R.string.already_have_account));
            spannableSignIn.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.colorAccent)), 22, 34, 0);
            spannableSignIn.setSpan(new UnderlineSpan(), 22, 34, 0);
            getView().getSignInView().setText(spannableSignIn);
            getView().getToolbarTitle().setText(R.string.fragment_registration);
            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);

            SpannableString termsAndConditionSpannable = new SpannableString(getActivity().getString(R.string.warning_terms_conditions));
            termsAndConditionSpannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.colorAccent)), 11, 30, 0);

            getView().getTVTermsAndConditions().setText(termsAndConditionSpannable);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    ((LoginActivity) getActivity()).oneStepBack();
                }
            });
        }
    }

    /*
    Check Bundle data
     */
    public void saveBundleInfo(Bundle bundle) {
        if (bundle != null && bundle.getString("role") != null) {
            role = bundle.getString("role");
        }
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && getView() != null) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.btn_register:
                    if (getActivity() != null && getView() != null) {
                        if (Utils.getInstance().isInternetAvailable(getActivity())) {
                            validateForm(getView().getEmail().getText().toString().trim(), getView().getPassword().getText().toString(), getView().getConfirmPassword().getText().toString());
                        } else {
                            getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                        }
                    }
                    break;
                case R.id.tv_login:
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    ((LoginActivity) getActivity()).oneStepBack();
                    break;

                case R.id.tv_terms_and_conditions:
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.NAVIGATE_FROM, "");
                    bundle.putString(Constants.URL, "http://onehomeportal.mobilytedev.com/pages/terms-and-conditions");
                    ((LoginActivity) getActivity()).changeScreen(R.id.login_container, CurrentScreen.SETTING, false, true, bundle);
                    break;
            }
        }
    }

    /**
     * clear data after register response.
     */
    private void clearData() {
        if (getActivity() != null && isViewAttached()) {
            getView().getEmail().setText("");
            getView().getPassword().setText("");
            getView().getConfirmPassword().setText("");
            getView().getTermsAndConditions().setChecked(false);
        }
    }

    /**
     * Validate registration form
     *
     * @param email      gives entered email by user while registering into app.
     * @param password   gives entered password by user while registering into app.
     * @param confirmPwd gives entered confirm password by user while registering into app.
     */
    private void validateForm(String email, String password, String confirmPwd) {

        String response = Validations.getInstance().validateSignUpFields(email, password, confirmPwd, role, getView().getTermsAndConditions().isChecked());

        if (!response.isEmpty()) {
            getActivity().showToast(response);
        } else {
            //Hit API
            Map<String, String> param = new HashMap<>();
            param.put("email", email);
            param.put("password", password);
            param.put("role", role);

            getActivity().showProgressDialog();
            getInterActor().register(param, ApiName.REGISTER_API);
        }
    }


    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (response.isSuccessful()) {
                    RegisterResponse obj = (RegisterResponse) response.body();
                    if (obj.getStatus() == 200) {
                        clearData();
                        //Success
                        //Move to Verification Screen
                        Bundle bundle = new Bundle();
                        bundle.putString("access_token", obj.getData().getAccessToken());
                        ((LoginActivity) getActivity()).changeScreen(R.id.login_container, CurrentScreen.VERIFY_OTP_SCREEN, true, true, bundle);
                    } else {
                        getActivity().showSnakBar(getView().getRootView(), obj.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && getView() != null) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
