package com.ohp.commonviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.adapter.MessageUsersListAdapter;
import com.ohp.commonviews.beans.GetChatListResponse;
import com.ohp.commonviews.view.MessageListingFragmentView;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static com.ohp.enums.ApiName.GET_CHAT_USERS;

/**
 * Created by vishal.sharma on 9/7/2017.
 */

public class MessageListingFragmentPresenter extends FragmentPresenter<MessageListingFragmentView, APIHandler> implements APIResponseInterface.OnCallableResponse, View.OnClickListener {
    public String searchQuery = "";
    private int page = 1;
    private boolean isSearched = false;
    private GetChatListResponse getChatListResponse;
    private List<GetChatListResponse.DataBean> ListChatResponse = new ArrayList<>();
    private MessageUsersListAdapter messageUsersListAdapter;

    public MessageListingFragmentPresenter(MessageListingFragmentView messageListingFragmentView, Context context) {
        super(messageListingFragmentView, context);
    }

    BroadcastReceiver updateMessageList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ListChatResponse.size() > 0) ListChatResponse.clear();
            ListChatResponse = new ArrayList<>();
            getMessageUser();
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            getActivity().registerReceiver(updateMessageList, new IntentFilter(Constants.UPDATE_MESSAGE_LIST));
            LinearLayoutManager lm = new LinearLayoutManager(getContext());
            lm.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getRecyclerTaskList().setLayoutManager(lm);
            getView().getRecyclerTaskList().setNestedScrollingEnabled(false);
            messageUsersListAdapter = new MessageUsersListAdapter(getActivity(), this, ListChatResponse);
            getView().getRecyclerTaskList().setAdapter(messageUsersListAdapter);

            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

                            if (getChatListResponse != null && getChatListResponse.getTotalRecord() > ListChatResponse.size()) {

                                System.out.println("total Records" + getChatListResponse.getTotalRecord());
                                page += 1;
                                getMessageUser();
                            }
                        }
                    }
                }
            });
            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });
            getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s != null && s.length() > 0) {
                        getView().getCloseBtn().setVisibility(View.VISIBLE);
                        searchQuery = s.toString();
                        page = 1;
                        isSearched = true;
                        getMessageUser();
                    } else {
                        searchQuery = "";
                        page = 1;
                        isSearched = true;
                        getMessageUser();
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        getView().getCloseBtn().setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });


        }

        getMessageUser();

    }

    private void getMessageUser() {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        if (Utils.getInstance().isInternetAvailable(getActivity())) {
                            if (TextUtils.isEmpty(searchQuery))
                                getActivity().showProgressDialog();
                            Map<String, String> params = new HashMap<>();
                            params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                            if (!TextUtils.isEmpty(searchQuery))
                                params.put(Constants.SEARCH_PARAM, searchQuery);
                            params.put(Constants.PAGE_PARAM, String.valueOf(page));
                            getInterActor().getChatUserList(params, GET_CHAT_USERS);

                        } else {
                            getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case GET_CHAT_USERS:
                        if (response.isSuccessful()) {
                            getChatListResponse = (GetChatListResponse) response.body();
                            switch (getChatListResponse.getStatus()) {
                                case 200:
                                    onSuccess(getChatListResponse);
                                    break;
                                case 201:

                                    break;
                                case 202:
                                    ((OwnerDashboardActivity) getActivity()).logout(getChatListResponse.getMessage());
                                    break;
                            }
                        }


                        break;

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(GetChatListResponse getChatListResponse) {
        if (getChatListResponse.getData() != null && getChatListResponse.getData().size() > 0) {
            getView().getEmptyLayout().setVisibility(View.GONE);
            if (isSearched || searchQuery.length() != 0) {
                ListChatResponse = new ArrayList<>();
                ListChatResponse.addAll(getChatListResponse.getData());
                isSearched = false;
            } else if (getChatListResponse.getTotalRecord() > ListChatResponse.size()) {
                ListChatResponse.addAll(getChatListResponse.getData());
            }
            messageUsersListAdapter.updateList(ListChatResponse);


        } else {
            if (ListChatResponse.isEmpty()) {
                messageUsersListAdapter.clearAll();
                getView().getEmptyLayout().setVisibility(View.VISIBLE);
            } else {
                if (isSearched || searchQuery.length() != 0) {
                    messageUsersListAdapter.clearAll();
                    getView().getEmptyLayout().setVisibility(View.VISIBLE);
                }
            }
        }


    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            switch (v.getId()) {
                case R.id.btn_clear:
                    searchQuery = "";
                    page = 1;
                    getView().getSearchEditText().setText("");
                    break;
            }

        }
    }
}
