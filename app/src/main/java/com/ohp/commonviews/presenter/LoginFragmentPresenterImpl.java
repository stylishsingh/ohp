package com.ohp.commonviews.presenter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.LoginActivity;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.beans.LoginResponse;
import com.ohp.commonviews.view.LoginView;
import com.ohp.enums.ApiName;
import com.ohp.enums.CurrentScreen;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.activities.OwnerProfileActivity;
import com.ohp.serviceproviderviews.activities.SPDashboardActivity;
import com.ohp.serviceproviderviews.activities.SPProfileActivity;
import com.ohp.utils.Constants;
import com.ohp.utils.SharedPreferencesHandler;
import com.ohp.utils.Utils;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;


public class LoginFragmentPresenterImpl extends FragmentPresenter<LoginView, APIHandler> implements APIResponseInterface.OnCallableResponse, View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    ProfileTracker mProfileTracker = null;
    /*
           Facebook Login
            */
    private CallbackManager callbackManager;
    private boolean isNormalSignup = false;
    private String userRole;
    private int RC_SIGN_IN = 2017;
    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;

    public LoginFragmentPresenterImpl(LoginView loginView, Context context) {
        super(loginView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            Spannable spannableSignUp = new SpannableString(getActivity().getString(R.string.don_t_have_account_yet));
            spannableSignUp.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.colorAccent)), 23, 31, 0);
            spannableSignUp.setSpan(new UnderlineSpan(), 23, 31, 0);
            getView().getSignUpTextView().setText(spannableSignUp);
        }
    }


    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getContext(), getView().getRootView());
            switch (v.getId()) {
                case R.id.btn_login:
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        checkValidation(getView().getEmail().getText().toString().trim(), getView().getPassword().getText().toString());
                    } else {
                        getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                    }
                    break;
                case R.id.tv_forgotPwd:
                    clearData();
                    ((LoginActivity) getActivity()).changeScreen(R.id.login_container, CurrentScreen.FORGOT_PWD_SCREEN, true, true, null);
                    break;
                case R.id.tv_signup:
                    //Show Choose Dialog and then navigate to Register screen.
                    //Move to Signup Fragment
                    isNormalSignup = true;
                    showChooseDialog();
                    break;
                case R.id.btn_facebook:
                    if (Utils.getInstance().isInternetAvailable(getActivity()))
                        fbLogin();
                    else
                        getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                    break;
                case R.id.btn_google_plus:
                    if (Utils.getInstance().isInternetAvailable(getActivity()))
                        googleLogin();
                    else
                        getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                    break;
            }
        }
    }

    /**
     * This method will execute facebook Api to get detail of user and then make a call to OHP server to register or login.
     */

    private void fbLogin() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();// lOGOUT BEFORE TRYING TO SIGNIN

        LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "user_friends", "email", "user_about_me", "user_birthday"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                System.out.println("FACEBOOK PERMISSION" + loginResult.getRecentlyGrantedPermissions());
                Utils.d("", "FACEBOOK USER ID: " + loginResult.getAccessToken().getUserId());

                final String facebookID = loginResult.getAccessToken().getUserId();


                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(final JSONObject jsonObject, GraphResponse graphResponse) {
                        try {

                            System.out.println("Graph response---->" + graphResponse);
                            // App code
                            final String fullName = jsonObject.optString("name");
                            final String email = jsonObject.optString("email");
                            if (getActivity() != null) {
                                if (TextUtils.isEmpty(email)) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                String profilePic = "https://graph.facebook.com/" + facebookID + "/picture?width=" + 300 + "&height=" + 300;
                                                System.out.println("LoginFragmentPresenterImpl.run--" + profilePic);
                                                socialLogin(facebookID, email, fullName, profilePic);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                    });
                                } else {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                String profilePic = "https://graph.facebook.com/" + facebookID + "/picture?width=" + 300 + "&height=" + 300;
                                                System.out.println("LoginFragmentPresenterImpl.run--" + profilePic);
                                                socialLogin(facebookID, email, fullName, profilePic);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                    });
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,link");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();


            }

            @Override
            public void onCancel() {
                if (getActivity() != null && isViewAttached()) {
                }
            }

            @Override
            public void onError(FacebookException error) {
                if (getActivity() != null && isViewAttached()) {
                    getActivity().showSnakBar(getView().getRootView(), error.getMessage());
                }
            }
        });
    }

    /**
     * This method will request to google plus for signIn.
     */
    private void googleLogin() {
        try {
            if (gso == null || mGoogleApiClient == null) {
                gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();

                mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                        .enableAutoManage(getActivity(), this)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build();
            }

            if (mGoogleApiClient != null && gso != null) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                getActivity().startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * This method will return user detail from google plus and signOut current instance , and then make a call to OHP server to register or login..
     */

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                // Get account information
                if (acct != null) {
                    String mFullName = acct.getDisplayName();
                    String mEmail = acct.getEmail();
                    String googleID = acct.getId();
                    String profilePic = "";
                    if (acct.getPhotoUrl() != null)
                        profilePic = acct.getPhotoUrl().toString();
                    socialLogin(googleID, mEmail, mFullName, profilePic);
                }
            }

            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {

                }
            });


        } else {
            if (callbackManager != null)
                callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    /**
     * This method will ask user to choose role to register into app i.e. Property Owner or Service Provider.
     */

    private void showChooseDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String[] options = {"Property Owner", "Service Provider", "Cancel"};
        final Bundle bundle = new Bundle();
        builder.setTitle("Please Choose User Role")
                .setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        switch (which) {
                            case 0:
                                bundle.putString("role", "1");
                                if (isNormalSignup) {
                                    ((LoginActivity) getActivity()).changeScreen(R.id.login_container, CurrentScreen.REGISTER_SCREEN, true, true, bundle);
                                    isNormalSignup = false;
                                } else {
                                    //came through social
                                    updateRole("1");
                                }
                                clearData();
                                break;
                            case 1:
//                                Utils.getInstance().showToast("Contractor Work in progress");
                                bundle.putString("role", "2");
                                if (isNormalSignup) {
                                    ((LoginActivity) getActivity()).changeScreen(R.id.login_container, CurrentScreen.REGISTER_SCREEN, true, true, bundle);
                                    isNormalSignup = false;
                                } else {
                                    //came through social
                                    updateRole("2");
                                }
                                clearData();
                                break;
                            case 2:
                                isNormalSignup = false;
                                dialog.dismiss();
                                break;
                        }
                    }
                });
        builder.setCancelable(false);
        builder.show();
    }

    /**
     * This method is used to clear data after login response.
     */
    private void clearData() {
        if (getActivity() != null && isViewAttached()) {
            getView().getEmail().setText("");
            getView().getPassword().setText("");
        }
    }

    /**
     * @param email    entered by user while login
     * @param password entered by user while login
     *                 This method will validate email and password,if its valid then make call to server to login in to app,.
     */

    private void checkValidation(String email, String password) {
        if (getActivity() != null && isViewAttached()) {
            if (email.length() == 0 || password.length() == 0) {
                getActivity().showSnakBar(getView().getRootView(), "Please enter all fields");
            } else if (email.length() == 0) {
                getActivity().showSnakBar(getView().getRootView(), "Please enter email");
            } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                getActivity().showSnakBar(getView().getRootView(), "Please enter valid email");
            } else if (password.length() == 0) {
                getActivity().showSnakBar(getView().getRootView(), "Please enter password");
            } else {
                //Hit LoginAPI
                Map<String, String> param = new HashMap<>();
                param.put("email", email);
                param.put("password", password);
                param.put("is_social", "2"); // login via email
                param.put("social_id", "");

                getActivity().showProgressDialog();
                getInterActor().checkLogin(param, ApiName.LOGIN_API);
            }
        }

    }

    private void socialLogin(String socialId, String email, String fullName, String profilePic) {
        Map<String, String> param = new HashMap<>();
        if (TextUtils.isEmpty(email))
            param.put("email", "");
        else
            param.put("email", email);

        if (TextUtils.isEmpty(fullName))
            param.put("full_name", "");
        else
            param.put("full_name", fullName);

        if (TextUtils.isEmpty(profilePic))
            param.put("profile_avatar", "");
        else
            param.put("profile_avatar", profilePic);

        param.put("password", "");
        param.put("is_social", "1");   // login via social id
        param.put("social_id", socialId);
        getActivity().showProgressDialog();
        getInterActor().checkLogin(param, ApiName.LOGIN_API);
    }

    private void updateRole(String role) {
        if (TextUtils.isEmpty(Utils.getInstance().getAccessToken(getActivity()))) {
            Utils.getInstance().showToast("Access Token not found");
            return;
        }
        this.userRole = role;
        Map<String, String> param = new HashMap<>();
        param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));   // login via social id
        param.put("role", role);

        getActivity().showProgressDialog();
        getInterActor().updateRole(param, ApiName.UPDATE_ROLE_API);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case LOGIN_API:
                        if (response.isSuccessful()) {
                            LoginResponse obj = (LoginResponse) response.body();
                            if (obj.getStatus() == 200) {
                                //Success, Move to Dashboard on basis of role
                                SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_access_token), obj.getData().getAccessToken());
                                SharedPreferencesHandler.setIntValues(getActivity(), getActivity().getString(R.string.pref_is_verified), obj.getData().getIsVerified());
                                SharedPreferencesHandler.setIntValues(getActivity(), getActivity().getString(R.string.pref_role), obj.getData().getRole());
                                SharedPreferencesHandler.setIntValues(getActivity(), getActivity().getString(R.string.pref_is_social), obj.getData().getIsSocial());
                                SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_fullname), obj.getData().getFullName());
                                SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_email), obj.getData().getEmail());
                                SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_address), obj.getData().getAddress());
                                SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_profile_pic), obj.getData().getProfileImage200X200());
                                SharedPreferencesHandler.setIntValues(getActivity(), getActivity().getString(R.string.pref_user_id), obj.getData().getUser_id());
                                if (obj.getData().getRole() == 0) {
                                    //Ask for Role and update
                                    showChooseDialog();
                                } else if (obj.getData().getIsVerified() == 0) {
                                    //Move to Verification Screen
                                    Bundle bundle = new Bundle();
                                    bundle.putString("access_token", obj.getData().getAccessToken());
                                    ((LoginActivity) getActivity()).changeScreen(R.id.login_container, CurrentScreen.VERIFY_OTP_SCREEN, true, true, bundle);
                                } else if (obj.getData().getRole() == 1 && obj.getData().getIsVerified() == 1) {
                                    //User is Property Owner
                                    if (TextUtils.isEmpty(obj.getData().getFullName())) {
                                        //User profile is not completed, Move to Profile Screen
                                        Intent intent = new Intent(getActivity(), OwnerProfileActivity.class);
                                        intent.putExtra("is_edit", true);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        getActivity().startActivity(intent);

                                    } else {
                                        //User profile is completed, Move to Dashboard Screen
                                        Intent intent = new Intent(getActivity(), OwnerDashboardActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        getActivity().startActivity(intent);
                                    }
                                } else if (obj.getData().getRole() == 2 && obj.getData().getIsVerified() == 1) {
                                    //User is Contractor
                                    if (TextUtils.isEmpty(Utils.getInstance().getUserName(getActivity()))) {
                                        //User profile is not completed, Move to Contractor Profile Screen
                                        Intent intent = new Intent(getActivity(), SPProfileActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.putExtra("is_edit", true);
                                        getActivity().startActivity(intent);
                                    } else {
                                        //User profile is not completed, Move to Contractor Dashboard Screen
                                        Intent intent = new Intent(getActivity(), SPDashboardActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        getActivity().startActivity(intent);
                                    }
                                }
                            } else {
                                getActivity().showSnakBar(getView().getRootView(), obj.getMessage());
                            }
                        }
                        break;
                    case UPDATE_ROLE_API:
                        if (response.isSuccessful()) {
                            GenericBean obj = (GenericBean) response.body();
                            if (obj.getStatus() == 200) {
//                                Utils.getInstance().showToast(obj.getMessage());
                                //check role from local variable
                                if (!TextUtils.isEmpty(userRole) && userRole.equals("1")) {
                                    //User is Property Owner
                                    SharedPreferencesHandler.setIntValues(getActivity(), getActivity().getString(R.string.pref_role), 1);
                                    Intent intent = new Intent(getActivity(), OwnerDashboardActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    getActivity().startActivity(intent);
                                } else if (!TextUtils.isEmpty(userRole) && userRole.equals("2")) {
                                    //User is Contractor
                                    SharedPreferencesHandler.setIntValues(getActivity(), getActivity().getString(R.string.pref_role), 2);
                                    if (TextUtils.isEmpty(Utils.getInstance().getAddress(getActivity()))) {
                                        //User profile is not completed, Move to Contractor Profile Screen
                                        Intent intent = new Intent(getActivity(), SPProfileActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.putExtra("is_edit", true);
                                        getActivity().startActivity(intent);
                                    } else {
                                        //User profile is not completed, Move to Contractor Dashboard Screen
                                        Intent intent = new Intent(getActivity(), SPDashboardActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        getActivity().startActivity(intent);
                                    }
                                }
                            } else {
                                getActivity().showSnakBar(getView().getRootView(), obj.getMessage());
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && getView() != null) {
                getActivity().hideProgressDialog();
                if (t != null) getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (getActivity() != null && isViewAttached()) {
            getActivity().showSnakBar(getView().getRootView(), connectionResult.getErrorMessage());
        }
    }
}

