package com.ohp.commonviews.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.OneToOneChattingAdapter;
import com.ohp.commonviews.beans.GetMessagesResponse;
import com.ohp.commonviews.view.ChattingFragmentView;
import com.ohp.enums.ApiName;
import com.ohp.notifications.MyFirebaseMessagingService;
import com.ohp.notifications.NotificationIntent;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.serviceproviderviews.beans.MessageStatusResponse;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static com.ohp.utils.Constants.BG_APP;
import static com.ohp.utils.Constants.NAVIGATE_FROM;
import static com.ohp.utils.Constants.NOTIFICATIONS;

/**
 * @author Vishal Sharma.
 */

public class ChattingFragmentPresenter extends FragmentPresenter<ChattingFragmentView, APIHandler> implements APIResponseInterface.OnCallableResponse, View.OnClickListener {
    public static boolean isRegistered = false;
    private String SendByName = "", SendToName = "";
    private int page = 1;
    private String UserIdSender = "", CreatedTime = "", message = "";
    private final BroadcastReceiver messageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    if (intent.getExtras() != null && intent.getExtras().containsKey(Constants.NAVIGATE_FROM)
                            && Constants.NOTIFICATIONS.equalsIgnoreCase(intent.getExtras().getString(NAVIGATE_FROM))) {
                        if (UserIdSender.equalsIgnoreCase(intent.getExtras().getString(Constants.SEND_BY_USERID))) {
                            page = 1;
                            getMessages();
                        } else {
                            Intent broadcastIntent = new Intent(context, HolderActivity.class);

                            broadcastIntent.putExtra(BG_APP, false);
                            broadcastIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            broadcastIntent.putExtra(NAVIGATE_FROM, NOTIFICATIONS);
                            broadcastIntent.putExtra(Constants.DESTINATION, Constants.CLICK_CHAT);
                            broadcastIntent.putExtra(Constants.SEND_BY_NAME, intent.getStringExtra(Constants.SEND_BY_NAME));
                            broadcastIntent.putExtra(Constants.SEND_BY_USERID, intent.getStringExtra(Constants.SEND_BY_USERID));
                            NotificationIntent.getInstance().generateNotification(intent.getStringExtra(Constants.MESSAGE)
                                    , broadcastIntent, getActivity(), MyFirebaseMessagingService.notificationID);
                        }
                    } else {
                        page = 1;
                        getMessages();
                    }
                    context.sendBroadcast(new Intent(Constants.UPDATE_MESSAGE_LIST));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };
    private GetMessagesResponse getMessagesResponse;
    private boolean bgApp = false;
    private OneToOneChattingAdapter chattingAdapter;
    private List<GetMessagesResponse.DataBean> getMessagesResponseList = new ArrayList<>();

    public ChattingFragmentPresenter(ChattingFragmentView chattingFragmentView, Context context) {
        super(chattingFragmentView, context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null && isViewAttached()) {
            isRegistered = false;
            getActivity().unregisterReceiver(messageReceiver);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            isRegistered = true;
            getActivity().registerReceiver(messageReceiver, new IntentFilter(Constants.GET_MESSAGE_LIST));
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            getView().getToolbarTitle().setText(SendToName);
            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    if (bgApp)
                        ((HolderActivity) getActivity()).moveToParentActivity();
                    else
                        ((HolderActivity) getActivity()).oneStepBack();
                }
            });
            final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getChatRecycler().setLayoutManager(mLayoutManager);
            getView().getChatRecycler().setNestedScrollingEnabled(false);
            chattingAdapter = new OneToOneChattingAdapter(getActivity(), this, null);
            getView().getChatRecycler().setAdapter(chattingAdapter);
            getView().getRefreshLayout().setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // Refresh items
                    page += 1;
                    getMessages();
                }
            });
            getMessages();

        }
    }

    private void getMessages() {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        if (Utils.getInstance().isInternetAvailable(getActivity())) {
                            getActivity().showProgressDialog();
                            Map<String, String> params = new HashMap<>();
                            params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                            params.put(Constants.PAGE_PARAM, String.valueOf(page));
                            // params.put(Constants.USER_ID, String.valueOf(Utils.getInstance().getUserId(getActivity())));
                            params.put(Constants.USER_ID, UserIdSender);
                            getInterActor().getMessages(params, ApiName.GET_MESSAGE_LISTING);

                        } else {
                            getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();

                switch (api) {
                    case GET_MESSAGE_LISTING:
                        if (response.isSuccessful()) {
                            getMessagesResponse = (GetMessagesResponse) response.body();
                            switch (getMessagesResponse.getStatus()) {
                                case 200:
                                    onSuccess(getMessagesResponse);
                                    break;
                                case 201:
                                    break;
                                case 202:
                                    ((OwnerDashboardActivity) getActivity()).logout(getMessagesResponse.getMessage());
                                    break;
                            }
                        }

                        break;
                    case SEND_MESSAGE:
                        if (response.isSuccessful()) {
                            MessageStatusResponse messageStatusResponse = (MessageStatusResponse) response.body();
                            if (response.isSuccessful()) {
                                switch (messageStatusResponse.getStatus()) {
                                    case 200:
                                        onSuccess(messageStatusResponse);
                                        break;
                                    case 201:
                                        break;
                                    case 202:
                                        break;
                                }
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(MessageStatusResponse messageStatusResponse) {
        //((HolderActivity)getActivity()).oneStepBack();
        GetMessagesResponse.DataBean getMessagesResponse = new GetMessagesResponse.DataBean();
        getMessagesResponse.setChatId(messageStatusResponse.getData().getChatId());
        getMessagesResponse.setSendBy(messageStatusResponse.getData().getSendBy());
        getMessagesResponse.setSendByName(messageStatusResponse.getData().getSendByName());
        getMessagesResponse.setSendToName(messageStatusResponse.getData().getSendToName());
        getMessagesResponse.setSendTo(messageStatusResponse.getData().getSendTo());
        getMessagesResponse.setMessageX(messageStatusResponse.getData().getMessageX());
        getMessagesResponse.setCreated(messageStatusResponse.getData().getCreated());
        getMessagesResponseList.add(getMessagesResponse);
        chattingAdapter.updateList(getMessagesResponseList);
        chattingAdapter.notifyDataSetChanged();
        getView().getChatRecycler().scrollToPosition((getMessagesResponseList.size() - 1));
    }

    public void saveBundleInfo(Bundle arguments) {
        if (arguments != null) {
            bgApp = arguments.getBoolean(Constants.BG_APP);
            SendToName = arguments.getString(Constants.SEND_BY_NAME);
            UserIdSender = arguments.getString(Constants.SEND_BY_USERID);
        }
    }

    private void onSuccess(GetMessagesResponse getMessagesResponse) {
        if (getActivity() != null && isViewAttached()) {
            if (getMessagesResponse.getData().size() > 0 && getMessagesResponse.getData() != null) {
               /* getView().getEmptyLayout().setVisibility(View.GONE);

                getView().getBottmView().setVisibility(View.VISIBLE);*/
                if (page != 1)
                    getMessagesResponseList.addAll(0, getMessagesResponse.getData());
                else {
                    getMessagesResponseList = getMessagesResponse.getData();
                }
                System.out.println("ChattingFragmentPresenter.onSuccess" + getMessagesResponseList.size());
                chattingAdapter.updateList(getMessagesResponseList);
                getView().getRefreshLayout().setRefreshing(false);
                getView().getRefreshLayout().setEnabled(true);
                if (page == 1)
                    getView().getChatRecycler().scrollToPosition(getMessagesResponseList.size() - 1);
            } else {
                // getMessagesResponseList.clear();
                getView().getRefreshLayout().setRefreshing(false);
                getView().getRefreshLayout().setEnabled(false);
                if (getMessagesResponseList.size() == 0) {
                   /* getView().getEmptyLayout().setVisibility(View.VISIBLE);
                    getView().getBottmView().setVisibility(View.GONE);*/
                    chattingAdapter.clearAll();
                }


            }
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            switch (v.getId()) {
                case R.id.txt_send:
                    if (TextUtils.isEmpty(getView().getMessageText().getText().toString().trim())) {
                        getActivity().showSnackBar(getView().getRootView(), "Please Enter your message");
                    } else {
                        message = getView().getMessageText().getText().toString();
                        SendMessage(message);
                        getView().getMessageText().setText("");
                    }
                    break;
            }

        }
    }

    private void SendMessage(String message) {
        if (getActivity() != null && isViewAttached()) {
            try {
                if (getActivity() != null && isViewAttached()) {
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        if (Utils.getInstance().isInternetAvailable(getActivity())) {
                            getActivity().showProgressDialog();
                            Map<String, String> params = new HashMap<>();
                            params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                            params.put(Constants.PAGE_PARAM, String.valueOf(page));
                            params.put(Constants.SEND_TO, UserIdSender);
                            params.put(Constants.MESSAGE, message);
                            getInterActor().sendMessages(params, ApiName.SEND_MESSAGE);

                        } else {
                            getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
