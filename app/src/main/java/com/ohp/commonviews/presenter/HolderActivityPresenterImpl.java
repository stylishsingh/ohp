package com.ohp.commonviews.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.library.mvp.ActivityPresenter;
import com.library.mvp.BaseView;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.enums.CurrentScreen;
import com.ohp.interfaces.OnFragmentInteractionListener;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.utils.Constants;

import static com.ohp.enums.CurrentScreen.ADD_EDIT_TEAM_SCREEN;
import static com.ohp.enums.CurrentScreen.OWNER_ADD_EDIT_EVENT_SCREEN;
import static com.ohp.enums.CurrentScreen.SP_ADD_EDIT_EVENT_SCREEN;
import static com.ohp.utils.Constants.ADD_EDIT_APPLIANCE_SCREEN;
import static com.ohp.utils.Constants.ADD_EDIT_DOCUMENT_SCREEN;
import static com.ohp.utils.Constants.ADD_EDIT_PROPERTY;
import static com.ohp.utils.Constants.ADD_PROJECT_SCREEN;
import static com.ohp.utils.Constants.APPLIANCE_LIST_SCREEN;
import static com.ohp.utils.Constants.BROWSE_PROJECTS;
import static com.ohp.utils.Constants.CLICK_ADD_APPLIANCE;
import static com.ohp.utils.Constants.CLICK_ADD_CONTACT;
import static com.ohp.utils.Constants.CLICK_ADD_DOCUMENT;
import static com.ohp.utils.Constants.CLICK_ADD_EDIT_PROJECT_SERVICE_PROVIDER;
import static com.ohp.utils.Constants.CLICK_ADD_PROJECT;
import static com.ohp.utils.Constants.CLICK_ADD_PROPERTY;
import static com.ohp.utils.Constants.CLICK_ADD_TASK;
import static com.ohp.utils.Constants.CLICK_ADD_TEAM;
import static com.ohp.utils.Constants.CLICK_ALL_COMPLETED_PROJECTS_SERVICEPROVIDER;
import static com.ohp.utils.Constants.CLICK_ALL_MY_PROJECTS_SERVICEPROVIDER;
import static com.ohp.utils.Constants.CLICK_ALL_PROJECTS;
import static com.ohp.utils.Constants.CLICK_ALL_WON_PROJECTS_SERVICEPROVIDER;
import static com.ohp.utils.Constants.CLICK_CHAT;
import static com.ohp.utils.Constants.CLICK_EDIT_APPLIANCE;
import static com.ohp.utils.Constants.CLICK_EDIT_BID;
import static com.ohp.utils.Constants.CLICK_EDIT_CONTACT;
import static com.ohp.utils.Constants.CLICK_EDIT_DOCUMENT;
import static com.ohp.utils.Constants.CLICK_EDIT_PROJECT;
import static com.ohp.utils.Constants.CLICK_EDIT_PROPERTY;
import static com.ohp.utils.Constants.CLICK_EDIT_TASK;
import static com.ohp.utils.Constants.CLICK_EDIT_TEAM;
import static com.ohp.utils.Constants.CLICK_OWNER_ADD_EVENT;
import static com.ohp.utils.Constants.CLICK_OWNER_EDIT_EVENT;
import static com.ohp.utils.Constants.CLICK_OWNER_VIEW_EVENT;
import static com.ohp.utils.Constants.CLICK_SP_ADD_EVENT;
import static com.ohp.utils.Constants.CLICK_SP_EDIT_EVENT;
import static com.ohp.utils.Constants.CLICK_SP_VIEW_EVENT;
import static com.ohp.utils.Constants.CLICK_VIEW_APPLIANCE;
import static com.ohp.utils.Constants.CLICK_VIEW_BID;
import static com.ohp.utils.Constants.CLICK_VIEW_CONTACT;
import static com.ohp.utils.Constants.CLICK_VIEW_DOCUMENT;
import static com.ohp.utils.Constants.CLICK_VIEW_ONGOING_PROJECT;
import static com.ohp.utils.Constants.CLICK_VIEW_PROJECT;
import static com.ohp.utils.Constants.CLICK_VIEW_PROPERTY;
import static com.ohp.utils.Constants.CLICK_VIEW_PROVIDER;
import static com.ohp.utils.Constants.CLICK_VIEW_REVIEWS;
import static com.ohp.utils.Constants.CLICK_VIEW_TASK;
import static com.ohp.utils.Constants.CLICK_VIEW_TEAM;
import static com.ohp.utils.Constants.CLICK_VIEW_UPCOMING_PROJECT;
import static com.ohp.utils.Constants.CLICK_VIEW__PAST_PROJECT;
import static com.ohp.utils.Constants.DOCUMENT_LIST_SCREEN;
import static com.ohp.utils.Constants.EVENT_LIST_SCREEN;
import static com.ohp.utils.Constants.MY_PROJECTS_SERVICE_PROVIDER_EDIT;
import static com.ohp.utils.Constants.MY_PROJECTS_SERVICE_PROVIDER_View;
import static com.ohp.utils.Constants.PROJECT_DETAIL_SERVICE_PROVIDER;
import static com.ohp.utils.Constants.PROJECT_DETAIL_WON_PROJECTS;

/**
 * @author Anuj Sharma.
 */

public class HolderActivityPresenterImpl extends ActivityPresenter<BaseView, APIHandler> implements OnFragmentInteractionListener {

    public HolderActivityPresenterImpl(BaseView baseView, Context context) {
        super(baseView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            if (getActivity().getIntent() != null && getActivity().getIntent().getStringExtra(Constants.DESTINATION) != null) {
                Bundle bundle;
                switch (getActivity().getIntent().getStringExtra(Constants.DESTINATION)) {
                    case CLICK_ADD_PROPERTY:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_PROPERTY_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_VIEW_PROPERTY:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.EDIT_PROPERTY_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_EDIT_PROPERTY:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.EDIT_PROPERTY_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_EDIT_BID:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.PROJECT_DETAIL_SCREEN_SERVICEPROVIDER, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_VIEW_BID:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.PROJECT_DETAIL_SCREEN_SERVICEPROVIDER, false, false, getActivity().getIntent().getExtras());
                        break;
                    case BROWSE_PROJECTS:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.PROJECT_DETAIL_SCREEN_SERVICEPROVIDER, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_ADD_APPLIANCE:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_EDIT_APPLIANCE_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_EDIT_APPLIANCE:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_EDIT_APPLIANCE_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_VIEW_APPLIANCE:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_EDIT_APPLIANCE_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_ADD_DOCUMENT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_EDIT_DOCUMENT_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_VIEW_DOCUMENT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_EDIT_DOCUMENT_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_EDIT_DOCUMENT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_EDIT_DOCUMENT_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_OWNER_ADD_EVENT:
                        bundle = new Bundle();
                        bundle.putString(Constants.DESTINATION, CLICK_OWNER_ADD_EVENT);
                        bundle.putString(Constants.EVENT_ID, "");
                        bundle.putString(Constants.PROPERTY_ID, getActivity().getIntent().getStringExtra(Constants.PROPERTY_ID));
                        bundle.putBoolean(Constants.OCCURRENCE, getActivity().getIntent().getBooleanExtra(Constants.OCCURRENCE, false));
                        bundle.putBoolean(Constants.EVENT_REPEAT_EMPTY, getActivity().getIntent().getBooleanExtra(Constants.EVENT_REPEAT_EMPTY, false));
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, OWNER_ADD_EDIT_EVENT_SCREEN, false, false, bundle);
                        break;
                    case CLICK_SP_ADD_EVENT:
                        bundle = new Bundle();
                        bundle.putString(Constants.DESTINATION, CLICK_SP_ADD_EVENT);
                        bundle.putString(Constants.EVENT_ID, "");
                        bundle.putString(Constants.PROPERTY_ID, getActivity().getIntent().getStringExtra(Constants.PROPERTY_ID));
                        bundle.putBoolean(Constants.OCCURRENCE, getActivity().getIntent().getBooleanExtra(Constants.OCCURRENCE, false));
                        bundle.putBoolean(Constants.EVENT_REPEAT_EMPTY, getActivity().getIntent().getBooleanExtra(Constants.EVENT_REPEAT_EMPTY, false));
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, SP_ADD_EDIT_EVENT_SCREEN, false, false, bundle);
                        break;
                    case CLICK_OWNER_VIEW_EVENT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, OWNER_ADD_EDIT_EVENT_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_SP_VIEW_EVENT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, SP_ADD_EDIT_EVENT_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_OWNER_EDIT_EVENT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, OWNER_ADD_EDIT_EVENT_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;

                    case CLICK_SP_EDIT_EVENT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, SP_ADD_EDIT_EVENT_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;

                    case CLICK_EDIT_TEAM:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, ADD_EDIT_TEAM_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;

                    case CLICK_VIEW_TEAM:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, ADD_EDIT_TEAM_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_ADD_PROJECT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_PROJECT_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_VIEW_PROJECT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.CLICK_EDIT_PROJECT, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_EDIT_PROJECT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.CLICK_EDIT_PROJECT, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_CHAT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.CLICK_CHAT, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_VIEW_UPCOMING_PROJECT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ALL_UPCOMING_PROJECT, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_VIEW_ONGOING_PROJECT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ALL_ONGOING_PROJECT, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_VIEW__PAST_PROJECT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ALL_completed_PROJECT, false, false, getActivity().getIntent().getExtras());
                        break;
                    case Constants.BROWSE_PIC:
                        if (getActivity() instanceof HolderActivity)
                            ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.PIC_DETAIL_SCREEN, false, false, getActivity().getIntent().getExtras());
                        else if (getActivity() instanceof OwnerDashboardActivity)
                            ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.PIC_DETAIL_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;

                    case Constants.BROWSE_SLIDER_PIC:
                        if (getActivity() instanceof HolderActivity)
                            ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.IMAGE_SLIDER_SCREEN, false, false, getActivity().getIntent().getExtras());
                        else if (getActivity() instanceof OwnerDashboardActivity)
                            ((OwnerDashboardActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.IMAGE_SLIDER_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;


                    case CLICK_ALL_PROJECTS:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.CLICK_ADD_PROJECT, false, false, getActivity().getIntent().getExtras());
                        break;
                    case ADD_EDIT_PROPERTY:
                        switch (getActivity().getIntent().getStringExtra(Constants.NAVIGATE_SCREEN)) {
                            case ADD_EDIT_DOCUMENT_SCREEN:
                                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_EDIT_DOCUMENT_SCREEN, false, false, getActivity().getIntent().getExtras());
                                break;
                            case DOCUMENT_LIST_SCREEN:
                                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.DOCUMENT_LIST_SCREEN, false, false, getActivity().getIntent().getExtras());
                                break;
                            case ADD_EDIT_APPLIANCE_SCREEN:
                                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_EDIT_APPLIANCE_SCREEN, false, false, getActivity().getIntent().getExtras());
                                break;
                            case APPLIANCE_LIST_SCREEN:
                                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.APPLIANCE_LIST_SCREEN, false, false, getActivity().getIntent().getExtras());
                                break;
                            case ADD_PROJECT_SCREEN:
                                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_PROJECT_SCREEN, false, false, getActivity().getIntent().getExtras());
                                break;
                            case EVENT_LIST_SCREEN:
                                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.OWNER_EVENT_LIST_SCREEN, false, false, getActivity().getIntent().getExtras());
                                break;
                            case Constants.ADD_EDIT_EVENT_SCREEN:
                                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, OWNER_ADD_EDIT_EVENT_SCREEN, false, false, getActivity().getIntent().getExtras());
                                break;
                        }
                        break;
                    case CLICK_ALL_WON_PROJECTS_SERVICEPROVIDER:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ALL_WON_PROJECT_SERVICE_PROVIDER, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_ALL_COMPLETED_PROJECTS_SERVICEPROVIDER:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ALL_COMPLETED_PROJECT_SERVICE_PROVIDER, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_ALL_MY_PROJECTS_SERVICEPROVIDER:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ALL_MYPROJECTS_SERVICE_PROVIDER, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_ADD_EDIT_PROJECT_SERVICE_PROVIDER:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_EDIT_PROJECT_SP, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_ADD_TEAM:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_EDIT_TEAM_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_ADD_TASK:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.CLICK_ADD_TASK, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_ADD_CONTACT:

                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.CLICK_ADD_CONTACT, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_VIEW_TASK:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.TASK_DETAIL_SERVICE_PROVIDER, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_VIEW_CONTACT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.CONTACT_DETAIL_PROPERTY_OWNER, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_EDIT_CONTACT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.CONTACT_EDIT_PROPERTY_OWNER, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_EDIT_TASK:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.TASK_EDIT_SERVICE_PROVIDER, false, false, getActivity().getIntent().getExtras());
                        break;
                    case PROJECT_DETAIL_SERVICE_PROVIDER:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.PROJECT_DETAIL_SCREEN_SERVICEPROVIDER, false, false, getActivity().getIntent().getExtras());
                        break;
                    case MY_PROJECTS_SERVICE_PROVIDER_View:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.MY_PROJECTS_SERVICE_PROVIDER_View, false, false, getActivity().getIntent().getExtras());
                        break;
                    case MY_PROJECTS_SERVICE_PROVIDER_EDIT:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.MY_PROJECTS_SERVICE_PROVIDER_EDIT, false, false, getActivity().getIntent().getExtras());
                        break;
                    case PROJECT_DETAIL_WON_PROJECTS:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.PROJECT_DETAIL_WON_PROJECTS, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_VIEW_PROVIDER:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.SP_DETAIL_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                    case CLICK_VIEW_REVIEWS:
                        ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.REVIEWS_SCREEN, false, false, getActivity().getIntent().getExtras());
                        break;
                }
            }
        }
    }

    @Override
    public void navigateTo(CurrentScreen tag, Bundle bundle) {
        switch (tag) {
            case ADD_EDIT_DOCUMENT_SCREEN:
                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_EDIT_DOCUMENT_SCREEN, true, true, bundle);
                break;
            case DOCUMENT_LIST_SCREEN:
                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.DOCUMENT_LIST_SCREEN, true, true, bundle);
                break;
            case ADD_EDIT_APPLIANCE_SCREEN:
                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_EDIT_APPLIANCE_SCREEN, true, true, bundle);
                break;
            case APPLIANCE_LIST_SCREEN:
                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.APPLIANCE_LIST_SCREEN, true, true, bundle);
                break;
            case ADD_PROJECT_SCREEN:
                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.ADD_PROJECT_SCREEN, true, true, bundle);
                break;
            case OWNER_EVENT_LIST_SCREEN:
                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, CurrentScreen.OWNER_EVENT_LIST_SCREEN, true, true, bundle);
                break;
            case OWNER_ADD_EDIT_EVENT_SCREEN:
                ((HolderActivity) getActivity()).changeScreen(R.id.holder_container, OWNER_ADD_EDIT_EVENT_SCREEN, true, true, bundle);
                break;
        }
    }
}