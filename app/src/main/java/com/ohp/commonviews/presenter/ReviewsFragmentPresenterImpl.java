package com.ohp.commonviews.presenter;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.adapter.ReviewsAdapter;
import com.ohp.commonviews.beans.ReviewModel;
import com.ohp.commonviews.view.ReviewsView;
import com.ohp.enums.ApiName;
import com.ohp.enums.ScreenNavigation;
import com.ohp.interfaces.AdapterPresenter;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;

/**
 * @author Amanpal Singh.
 */

public class ReviewsFragmentPresenterImpl extends FragmentPresenter<ReviewsView, APIHandler> implements APIResponseInterface.OnCallableResponse, View.OnClickListener,
        AdapterPresenter<ReviewsAdapter, ReviewModel.DataBean> {

    private String searchQuery = "", userID = "";
    private LinearLayoutManager linearLayoutManager;
    private int page = 1;
    private List<ReviewModel.DataBean> reviewsList = new ArrayList<>();
    private boolean isSearched = false;
    private ReviewsAdapter adapter;
    private ReviewModel listModelObj;

    public ReviewsFragmentPresenterImpl(ReviewsView reviewsView, Context context) {
        super(reviewsView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    public void saveBundleInfo(Bundle bundle) {
        if (getActivity() != null && isViewAttached()) {
            userID = bundle.getString(Constants.USER_ID);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {

            if (getActivity() != null && isViewAttached()) {
                getActivity().setSupportActionBar(getView().getToolbar());
                if (getActivity().getSupportActionBar() != null)
                    getActivity().getSupportActionBar().setTitle("");
                getView().getToolbar().setVisibility(View.VISIBLE);
                getView().getToolbarTitle().setText(R.string.title_reviews);
                getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
                getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        ((HolderActivity) getActivity()).oneStepBack();
                    }
                });
            }

            getView().getSearchEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                // Apply action which you want on search key press on keypad
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            case KeyEvent.KEYCODE_SEARCH:
                                Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });

            getView().getSearchEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(final CharSequence s, int start, int before, int count) {

                    if (s != null && s.length() > 0) {
                        getView().getCloseBtn().setVisibility(View.VISIBLE);
                        Handler handler = new Handler();
                        searchQuery = s.toString();
                        page = 1;
                        isSearched = true;
                        getReviewsList();


                    } else {
                        searchQuery = "";
                        page = 1;
                        isSearched = true;
                        reviewsList.clear();
                        adapter.update(reviewsList);
                        getReviewsList();
                        Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                        getView().getCloseBtn().setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
        getReviewsList();
    }

    private void getReviewsList() {
        try {
            if (getActivity() != null) {
                getActivity().showProgressDialog();
                Map<String, String> param = new HashMap<>();
                param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                param.put(Constants.USER_ID, userID);
                getInterActor().reviews(param, ApiName.REVIEWS_LIST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case REVIEWS_LIST:
                        if (response.isSuccessful()) {
                            ReviewModel dataResponse = (ReviewModel) response.body();
                            listModelObj = dataResponse;
                            switch (dataResponse.getStatus()) {
                                case Constants.STATUS_200:
                                    onSuccess(dataResponse);
                                    break;
                                case Constants.STATUS_201:
                                    break;
                                case Constants.STATUS_202:
                                    ((HolderActivity) getActivity()).logout(dataResponse.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(ReviewModel dataObj) {
        if (dataObj.getData() != null && dataObj.getData().size() > 0) {
            getView().getEmptyView().setVisibility(View.GONE);
            reviewsList = dataObj.getData();

//            if (dataObj.getTotalRecord() > reviewsList.size()) {
//                reviewsList.addAll(dataObj.getData());
//            }
            adapter.update(reviewsList);
        } else {
            if (reviewsList.isEmpty()) {
                adapter.clearAll();
                getView().getEmptyView().setVisibility(View.VISIBLE);
            } else {
                if (isSearched || searchQuery.length() != 0) {
                    adapter.clearAll();
                    getView().getEmptyView().setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onItemClicked(int position, ScreenNavigation click, CircleImageView imageView) {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public ReviewsAdapter initAdapter() {
        if (getActivity() != null && isViewAttached()) {
            adapter = new ReviewsAdapter(reviewsList, getActivity(), this);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            getView().getRecyclerView().setLayoutManager(linearLayoutManager);
            getView().getRecyclerView().setNestedScrollingEnabled(false);

            getView().getNSRecyclerView().setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            //code to fetch more data for endless scrolling

//                            if (listModelObj != null && listModelObj.getTotalRecord() > reviewsList.size()) {
//                                page += 1;
//                                getReviewsList();
//                            }

                        }
                    }
                }
            });
        }
        return adapter;
    }

    @Override
    public void updateList(List<ReviewModel.DataBean> filterList) {

    }


}
