package com.ohp.commonviews.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.commonviews.activities.LoginActivity;
import com.ohp.commonviews.beans.LoginResponse;
import com.ohp.commonviews.view.ResetPwdView;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.activities.OwnerProfileActivity;
import com.ohp.utils.Utils;
import com.ohp.utils.Validations;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

/**
 * Created by Anuj Sharma on 5/25/2017.
 */

public class ResetPwdFragmentImpl extends FragmentPresenter<ResetPwdView, APIHandler> implements APIHandler.OnCallableResponse, View.OnClickListener {
    private String accessToken;

    public ResetPwdFragmentImpl(ResetPwdView resetPwdView, Context context) {
        super(resetPwdView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            getView().getToolbarTitle().setText(R.string.title_reset_password);
            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    if (getActivity() != null && getActivity() instanceof LoginActivity)
                        ((LoginActivity) getActivity()).oneStepBack();
                    else if (getActivity() != null && getActivity() instanceof OwnerProfileActivity)
                        ((OwnerProfileActivity) getActivity()).oneStepBack();
                }
            });
        }
    }

    /*
    Check Bundle data
     */
    public void saveBundleInfo(Bundle bundle) {
        if (bundle != null && bundle.getString("access_token") != null) {
            accessToken = bundle.getString("access_token");
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.btn_confirm:
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {

                        String response = Validations.getInstance().validateResetPasswordFields(getView().getNewPassword().getText().toString().trim(), getView().getConfirmPassword().getText().toString().trim());

                        if (!response.isEmpty()) {
                            getActivity().showToast(response);
                        } else {
                            //Hit API
                            Map<String, String> param = new HashMap<>();
                            param.put("access_token", accessToken);
                            param.put("password", getView().getNewPassword().getText().toString().trim());
                            getActivity().showProgressDialog();
                            getInterActor().resetPassword(param, ApiName.RESET_PWD_API);

                        }
                    } else {
                        getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                    }
                    break;
            }
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case RESET_PWD_API:
                        if (response.isSuccessful()) {
                            LoginResponse obj = (LoginResponse) response.body();
                            if (obj.getStatus() == 200) {
                                //Success, Move to DashBoard Screen on basis of its role
                                Utils.getInstance().showToast(obj.getMessage().toString());
                                //Move user to Login Screen
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                getActivity().startActivity(intent);
                            } else {
                                getActivity().showSnakBar(getView().getRootView(), obj.getMessage().toString());
                            }
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && getView() != null) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
