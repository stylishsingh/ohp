package com.ohp.commonviews.presenter;

import android.content.Context;
import android.os.Bundle;

import com.library.mvp.ActivityPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.activities.LoginActivity;
import com.ohp.commonviews.view.CommonView;
import com.ohp.enums.ApiName;
import com.ohp.enums.CurrentScreen;

import retrofit2.Response;

/**
 * Created by Anuj Sharma on 5/19/2017.
 */

public class LoginActivityPresenterImpl extends ActivityPresenter<CommonView, APIHandler> implements APIResponseInterface.OnCallableResponse {

    public LoginActivityPresenterImpl(CommonView commonView, Context context) {
        super(commonView, context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            ((LoginActivity) getActivity()).changeScreen(R.id.login_container, CurrentScreen.LOGIN_SCREEN, false, false, null);
        }
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onSuccess(Response response, ApiName api) {

    }

    @Override
    public void onFailure(Throwable t, ApiName api) {

    }
}
