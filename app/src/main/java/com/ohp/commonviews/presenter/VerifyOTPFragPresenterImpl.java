package com.ohp.commonviews.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.commonviews.activities.LoginActivity;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.beans.LoginResponse;
import com.ohp.commonviews.view.VerifyOTPView;
import com.ohp.enums.ApiName;
import com.ohp.enums.CurrentScreen;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.activities.OwnerProfileActivity;
import com.ohp.serviceproviderviews.activities.SPDashboardActivity;
import com.ohp.serviceproviderviews.activities.SPProfileActivity;
import com.ohp.utils.SharedPreferencesHandler;
import com.ohp.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

/**
 * Created by Anuj Sharma on 5/24/2017.
 */

public class VerifyOTPFragPresenterImpl extends FragmentPresenter<VerifyOTPView, APIHandler> implements View.OnClickListener, APIHandler.OnCallableResponse {
    private String accessToken;

    public VerifyOTPFragPresenterImpl(VerifyOTPView confirmOTPView, Context context) {
        super(confirmOTPView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && isViewAttached()) {
            getActivity().setSupportActionBar(getView().getToolbar());
            if (getActivity().getSupportActionBar() != null)
                getActivity().getSupportActionBar().setTitle("");
            getView().getToolbarTitle().setText("Account verification");
            getView().getToolbar().setNavigationIcon(R.drawable.ic_navigation_back);
            getView().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
                    ((LoginActivity) getActivity()).oneStepBack();
                }
            });

            //initially hide resent otp button
            getView().getResendBtn().setVisibility(View.VISIBLE);
        }
    }

    public void saveBundleInfo(Bundle bundle) {
        accessToken = bundle.getString("access_token");
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && getView() != null) {
            Utils.getInstance().hideSoftKeyboard(getActivity(), getView().getRootView());
            switch (v.getId()) {
                case R.id.btn_confirm:
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        if (getView().getVerificationCode().getText().toString().length() == 0) {
                            getActivity().showSnakBar(getView().getRootView(), "Please enter verification code");
                        } else {
                            //Hit API
                            if (TextUtils.isEmpty(accessToken)) {
                                Utils.getInstance().showToast(getActivity().getString(R.string.error_accesstoken));
                                return;
                            }
                            Map<String, String> param = new HashMap<>();
                            param.put("access_token", accessToken);
                            param.put("OTP", getView().getVerificationCode().getText().toString());
                            param.put("navigate_type", "2"); // 1 for forgot password, 2 for signup
                            getActivity().showProgressDialog();
                            getInterActor().confirmVerificationCode(param, ApiName.CONFIRM_VERIFICATION_API);
                        }
                    } else {
                        getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                    }
                    break;
                case R.id.tv_resend:
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        //Hit API
                        if (TextUtils.isEmpty(accessToken)) {
                            Utils.getInstance().showToast(getActivity().getString(R.string.error_accesstoken));
                            return;
                        }
                        getView().getVerificationCode().setText("");
                        Map<String, String> param = new HashMap<>();
                        param.put("access_token", accessToken);

                        getActivity().showProgressDialog();
                        getInterActor().sendVerificationCode(param, ApiName.SEND_VERIFICATION_API);
                    } else {
                        getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
                    }
                    break;
            }
        }

    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case CONFIRM_VERIFICATION_API:
                        if (response.isSuccessful()) {
                            LoginResponse obj = (LoginResponse) response.body();
                            if (obj.getStatus() == 200) {
                                //Success, Move to Login Screen
                                //Move user to Login Screen
                                Utils.getInstance().showToast(obj.getMessage());
//                                Intent intent = new Intent(getActivity(), LoginActivity.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                getActivity().startActivity(intent);
                                getView().getVerificationCode().setText("");
                                SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_access_token), obj.getData().getAccessToken());
                                SharedPreferencesHandler.setIntValues(getActivity(), getActivity().getString(R.string.pref_is_verified), obj.getData().getIsVerified());
                                SharedPreferencesHandler.setIntValues(getActivity(), getActivity().getString(R.string.pref_role), obj.getData().getRole());
                                SharedPreferencesHandler.setIntValues(getActivity(), getActivity().getString(R.string.pref_is_social), obj.getData().getIsSocial());
                                SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_fullname), obj.getData().getFullName());
                                SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_email), obj.getData().getEmail());
                                SharedPreferencesHandler.setStringValues(getActivity(), getActivity().getString(R.string.pref_profile_pic), obj.getData().getProfileImage200X200());
                                SharedPreferencesHandler.setIntValues(getActivity(), getActivity().getString(R.string.pref_user_id), obj.getData().getUser_id());

                                if (obj.getData().getIsVerified() == 0) {
                                    //Move to Verification Screen
                                    Bundle bundle = new Bundle();
                                    bundle.putString("access_token", obj.getData().getAccessToken());
                                    ((LoginActivity) getActivity()).changeScreen(R.id.login_container, CurrentScreen.VERIFY_OTP_SCREEN, true, true, bundle);
                                } else if (obj.getData().getRole() == 1 && obj.getData().getIsVerified() == 1) {
                                    //User is Property Owner
                                    if (TextUtils.isEmpty(obj.getData().getFullName())) {
                                        //User profile is not completed, Move to Profile Screen
                                        Intent intent = new Intent(getActivity(), OwnerProfileActivity.class);
                                        intent.putExtra("is_edit", true);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        getActivity().startActivity(intent);

                                    } else {
                                        //User profile is completed, Move to Dashboard Screen
                                        Intent intent = new Intent(getActivity(), OwnerDashboardActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        getActivity().startActivity(intent);
                                    }
                                } else if (obj.getData().getRole() == 2 && obj.getData().getIsVerified() == 1) {
                                    //User is Contractor
                                    if (TextUtils.isEmpty(Utils.getInstance().getUserName(getActivity()))) {
                                        //User profile is not completed, Move to Contractor Profile Screen
                                        Intent intent = new Intent(getActivity(), SPProfileActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.putExtra("is_edit", true);
                                        getActivity().startActivity(intent);
                                    } else {
                                        //User profile is not completed, Move to Contractor Dashboard Screen
                                        Intent intent = new Intent(getActivity(), SPDashboardActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        getActivity().startActivity(intent);
                                        Utils.getInstance().showToast("Contractor Work in progress");
                                    }
                                }
                            } else {
                                getActivity().showSnakBar(getView().getRootView(), obj.getMessage());
                            }
                        }
                        break;
                    case SEND_VERIFICATION_API:
                        if (response.isSuccessful()) {
                            GenericBean obj = (GenericBean) response.body();
                            getView().getResendBtn().setVisibility(View.VISIBLE);
                            if (obj.getStatus() == 200) {
                                getActivity().showSnakBar(getView().getRootView(), obj.getMessage());
                            } else {
                                getActivity().showSnakBar(getView().getRootView(), obj.getMessage());
                            }
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && getView() != null) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
