package com.ohp.commonviews.presenter;

import android.content.Context;
import android.view.View;

import com.library.mvp.FragmentPresenter;
import com.ohp.R;
import com.ohp.api.APIHandler;
import com.ohp.api.APIResponseInterface;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.view.ChangePasswordView;
import com.ohp.enums.ApiName;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.serviceproviderviews.activities.SPDashboardActivity;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;
import com.ohp.utils.Validations;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

/**
 * @author Vishal Sharma.
 */

public class ChangePasswordPresenterImpl extends FragmentPresenter<ChangePasswordView, APIHandler> implements View.OnClickListener, APIResponseInterface.OnCallableResponse {


    private String old_password = "", password = "", new_password = "";

    public ChangePasswordPresenterImpl(ChangePasswordView changePasswordSpView, Context context) {
        super(changePasswordSpView, context);
    }

    @Override
    protected APIHandler createInterActor() {
        return new APIHandler(this);
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null && isViewAttached()) {
            getActivity().hideKeyboard();
            if (Utils.getInstance().isInternetAvailable(getActivity())) {

                old_password = getView().getOldPassword().getText().toString();
                password = getView().getNewPassword().getText().toString();
                new_password = getView().getConfirmPassword().getText().toString();
                String response = Validations.getInstance().validatePasswordFields(old_password, password, new_password);
                if (!response.isEmpty()) {
                    getActivity().showSnackBar(getView().getRootView(), response);
                } else {

                    getActivity().showProgressDialog();
                    Map<String, String> param = new HashMap<>();
                    param.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(getActivity()));
                    param.put(Constants.PASSWORD, old_password);
                    param.put(Constants.NEW_PASSWORD, new_password);
                    getInterActor().changePassword(param, ApiName.CHANGE_PASSWORD);
                }


            } else {
                getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_internet));
            }
        }
    }

    @Override
    public void onSuccess(Response response, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                switch (api) {
                    case CHANGE_PASSWORD:
                        if (response.isSuccessful()) {
                            GenericBean genericBean = (GenericBean) response.body();
                            switch (genericBean.getStatus()) {
                                case 200:
                                    onSuccess(genericBean);
                                    break;
                                case 201:
                                    getActivity().showSnakBar(getView().getRootView(), genericBean.getMessage());
                                    break;
                                case 202:
                                    if (getActivity() instanceof SPDashboardActivity)
                                        ((SPDashboardActivity) getActivity()).logout(genericBean.getMessage());
                                    else if (getActivity() instanceof OwnerDashboardActivity)
                                        ((OwnerDashboardActivity) getActivity()).logout(genericBean.getMessage());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSuccess(GenericBean genericBean) {
        if (getActivity() != null && isViewAttached()) {
            Utils.getInstance().showToast(genericBean.getMessage());
            getView().getOldPassword().setText("");
            getView().getNewPassword().setText("");
            getView().getConfirmPassword().setText("");
        }
    }

    @Override
    public void onFailure(Throwable t, ApiName api) {
        try {
            if (getActivity() != null && isViewAttached()) {
                getActivity().hideProgressDialog();
                if (t.getMessage() != null)
                    getActivity().showSnakBar(getView().getRootView(), t.getMessage());
                else
                    getActivity().showSnakBar(getView().getRootView(), getActivity().getString(R.string.error_api));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
