package com.ohp.commonviews.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.beans.ResourceBean;
import com.ohp.commonviews.beans.ResourcesValueModel;
import com.ohp.ownerviews.presenter.AddEditPropertyFragmentPresenterImpl;
import com.ohp.utils.Utils;

import java.util.List;

public class ResourcesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<ResourceBean.DataBean> resourceList;
    private AddEditPropertyFragmentPresenterImpl presenter;
    private List<ResourcesValueModel> listResourcesValue;
    private boolean isEdit;

    public ResourcesAdapter(Context ctx, List<ResourceBean.DataBean> list,AddEditPropertyFragmentPresenterImpl presenter, List<ResourcesValueModel> listResourcesValue, boolean isEdit) {
        this.context = ctx;
        this.resourceList = list;
        this.presenter = presenter;
        this.listResourcesValue = listResourcesValue;
        this.isEdit = isEdit;
    }





    public void updateResourceValues(List<ResourcesValueModel> listResourcesValue) {
        this.listResourcesValue = listResourcesValue;
        notifyItemRangeChanged(0, listResourcesValue.size());
    }

    public void updateResource(List<ResourceBean.DataBean> list) {
        this.resourceList = list;
        notifyItemRangeChanged(0, list.size());
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_resources, parent, false);
        return new ResourcesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ResourcesViewHolder) {
            //set data to spinner
            ResourcesViewHolder vh = (ResourcesViewHolder) holder;

            String title = resourceList.get(position).getTitle().replace("_", " ");
            System.out.println("ResourcesAdapter.onBindViewHolder--->" + resourceList.get(position).getTitle() + " " + listResourcesValue.get(position).getCount());
            if (isEdit) {
                vh.resourceTitle.setText(Utils.getInstance().capitalize(title) + " " + listResourcesValue.get(position).getCount());
            } else {
                vh.resourceDetailTitle.setText(Utils.getInstance().capitalize(title));
                vh.resourceCount.setText(listResourcesValue.get(position).getCount());
            }

        }
    }

    @Override
    public int getItemCount() {
        return (resourceList == null) ? 0 : resourceList.size();
    }

    private class ResourcesViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout resourceEditLayout, resourceDetailLayout;
        private TextView resourceTitle, resourceDetailTitle, resourceCount;

        private ResourcesViewHolder(View itemView) {
            super(itemView);
            resourceEditLayout = (LinearLayout) itemView.findViewById(R.id.resource_edit_container);
            resourceDetailLayout = (LinearLayout) itemView.findViewById(R.id.resource_detail_container);
            resourceTitle = (TextView) itemView.findViewById(R.id.resource_title);
            resourceDetailTitle = (TextView) itemView.findViewById(R.id.resource_detail_title);
            resourceCount = (TextView) itemView.findViewById(R.id.resource_count);

            if (isEdit) {
                resourceEditLayout.setVisibility(View.VISIBLE);
                resourceDetailLayout.setVisibility(View.GONE);
                resourceTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (presenter != null && presenter.getView() != null) {
                            Utils.getInstance().hideSoftKeyboard(context, presenter.getView().getRootView());
                        }
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        final String[] options = new String[resourceList.get(getAdapterPosition()).getInfo().size()];
                        for (int i = 0; i < resourceList.get(getAdapterPosition()).getInfo().size(); i++) {
                            options[i] = resourceList.get(getAdapterPosition()).getInfo().get(i).getValue();
                        }
                        builder.setTitle(Utils.getInstance().capitalize(resourceList.get(getAdapterPosition()).getTitle().replace("_", " ")))
                                .setItems(options,
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (presenter != null && presenter.getView() != null) {
                                                    // The 'which' argument contains the index position
                                                    resourceTitle.setText(Utils.getInstance().capitalize(resourceList.get(getAdapterPosition()).getTitle().replace("_", " "))
                                                            + " " + options[which]);
                                                    presenter.setResources(resourceList.get(getAdapterPosition()).getTitle(), options[which]);
                                                }
                                            }
                                        });
                        builder.show();
                    }
                });
            } else {
                resourceEditLayout.setVisibility(View.GONE);
                resourceDetailLayout.setVisibility(View.VISIBLE);
            }


        }
    }


}
