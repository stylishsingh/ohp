package com.ohp.commonviews.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class DashboardPagerAdapter extends FragmentStatePagerAdapter {

    public List<Fragment> mList = new ArrayList<>();
    private String tabTitles[];

    public DashboardPagerAdapter(FragmentManager fm, List<Fragment> list, String[] tabTitles) {
        super(fm);
        mList = list;
        this.tabTitles = tabTitles;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return mList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

}

