package com.ohp.commonviews.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class ImageSliderAdapter extends FragmentStatePagerAdapter {
    List<Fragment> fragment = new ArrayList<>();
    List<String> title = new ArrayList<>();

    public ImageSliderAdapter(FragmentManager fm, List<Fragment> fragment) {
        super(fm);
        this.fragment = fragment;
        for (int i = 0; i < fragment.size(); i++) {
            title.add("fragment " + i);
        }
    }

    @Override
    public Fragment getItem(int position) {

        return fragment.get(position);
    }

    @Override
    public int getCount() {
        return fragment.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title.get(position);
    }
}
