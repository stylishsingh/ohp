package com.ohp.commonviews.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.beans.JobTypeAndExpertise;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vishal.sharma on 7/13/2017.
 */

public class JobTypeExpertiseSpinnerAdapter extends ArrayAdapter<JobTypeAndExpertise.DataBean.InfoBean> {
    private Context context;
    private List<JobTypeAndExpertise.DataBean.InfoBean> mList;

    public JobTypeExpertiseSpinnerAdapter(Context context, int resource, int textViewResourceId,
                                          List<JobTypeAndExpertise.DataBean.InfoBean> objects) {
        super(context, resource, textViewResourceId, objects);
        this.context = context;
        this.mList = objects;
    }


    @Override
    public int getCount() {
        if (mList != null)
            return mList.size();
        else
            return 0;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return initView(convertView, position, Color.BLACK, 10);
    }

    private View initView(View convertView, int position, int color, int padding) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = LayoutInflater.from(context).inflate(R.layout.spinner_item, null);
            holder.title = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        JobTypeAndExpertise.DataBean.InfoBean model = mList.get(position);
        holder.title.setText(model.getValue());
        holder.title.setTextColor(color);
        holder.title.setPadding(padding, padding, padding, padding);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return initView(convertView, position, Color.BLACK, 10);
    }

    public void updateList(Context mContext, List<JobTypeAndExpertise.DataBean.InfoBean> mList) {

        this.context = mContext;
        this.mList = new ArrayList<>();
        this.mList.addAll(mList);
        notifyDataSetChanged();
    }

    private class Holder {
        TextView title;
    }
}
