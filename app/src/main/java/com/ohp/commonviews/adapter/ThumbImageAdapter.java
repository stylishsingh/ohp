package com.ohp.commonviews.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.ohp.R;
import com.ohp.commonviews.beans.ThumbnailBean;
import com.ohp.ownerviews.presenter.AddEditAppliancePresenterImpl;
import com.ohp.ownerviews.presenter.AddEditDocumentPresenterImpl;
import com.ohp.ownerviews.presenter.AddEditProjectFragmentPresenterImpl;
import com.ohp.serviceproviderviews.presenter.AddEditProjectServiceProviderPresenterImpl;
import com.ohp.serviceproviderviews.presenter.AddEditTaskSpPresenterImpl;
import com.ohp.serviceproviderviews.presenter.ProjectDetailServiceFragmentPresenterImpl;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

/**
 * @author Anuj Sharma.
 */

public class ThumbImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<ThumbnailBean> imageList;
    private AddEditAppliancePresenterImpl appliancePresenterImpl;
    private AddEditDocumentPresenterImpl docPresenterImpl;
    private AddEditProjectFragmentPresenterImpl projectPresenterImpl;
    private ProjectDetailServiceFragmentPresenterImpl projectDetailServicePresenter;
    private AddEditProjectServiceProviderPresenterImpl addEditProjectServiceProviderPresenter;
    private AddEditTaskSpPresenterImpl addEditTaskSpPresenter;
    private boolean isDocumentFile, isEdit;


    // For AddEditAppliancePresenterImpl Presenter
    public ThumbImageAdapter(Context ctx, List<ThumbnailBean> list, AddEditAppliancePresenterImpl appliancePresenterImpl, boolean isDocumentFile, boolean isEdit) {
        this.context = ctx;
        this.imageList = list;
        this.appliancePresenterImpl = appliancePresenterImpl;
        this.isDocumentFile = isDocumentFile;
        this.isEdit = isEdit;

        if (!isEdit && imageList != null && !imageList.isEmpty()) {
            imageList.remove(imageList.get(imageList.size() - 1));
        }
    }

    // For AddEditDocumentPresenterImpl Presenter
    public ThumbImageAdapter(Context ctx, List<ThumbnailBean> list, AddEditDocumentPresenterImpl docPresenterImpl, boolean isDocumentFile, boolean isEdit) {
        this.context = ctx;
        this.imageList = list;
        this.docPresenterImpl = docPresenterImpl;
        this.isDocumentFile = isDocumentFile;
        this.isEdit = isEdit;

//        if (!isEdit && imageList != null && !imageList.isEmpty()) {
//            imageList.remove(imageList.get(imageList.size() - 1));
//        }
    }

    // For AddEditProjectFragmentPresenterImpl Presenter
    public ThumbImageAdapter(Context ctx, List<ThumbnailBean> list, AddEditProjectFragmentPresenterImpl appliancePresenterImpl, boolean isDocumentFile, boolean isEdit) {
        this.context = ctx;
        this.imageList = list;
        this.projectPresenterImpl = appliancePresenterImpl;
        this.isDocumentFile = isDocumentFile;
        this.isEdit = isEdit;

        if (!isEdit && imageList != null && !imageList.isEmpty()) {
            imageList.remove(imageList.get(imageList.size() - 1));
        }
    }

    // For ProjectDetailServiceFragmentPresenterImpl Presenter
    public ThumbImageAdapter(Context ctx, List<ThumbnailBean> list, ProjectDetailServiceFragmentPresenterImpl projectDetailServicePresenter, boolean isDocumentFile, boolean isEdit) {
        this.context = ctx;
        this.imageList = list;
        this.projectDetailServicePresenter=projectDetailServicePresenter;
        this.isDocumentFile = isDocumentFile;
        this.isEdit = isEdit;
    }

    // For AddEditProjectServiceProviderPresenterImpl Presenter
    public ThumbImageAdapter(Context ctx, List<ThumbnailBean> list, AddEditProjectServiceProviderPresenterImpl addEditProjectServiceProviderPresenter, boolean isDocumentFile, boolean isEdit) {
        this.context = ctx;
        this.imageList = list;
        this.addEditProjectServiceProviderPresenter = addEditProjectServiceProviderPresenter;
        this.isDocumentFile = isDocumentFile;
        this.isEdit = isEdit;
    }

    public ThumbImageAdapter(Context ctx, List<ThumbnailBean> uploadImageList, AddEditTaskSpPresenterImpl addEditTaskSpPresenter, boolean isDocumentFile, boolean isEdit) {
        this.context = ctx;
        this.imageList = uploadImageList;
        this.addEditTaskSpPresenter = addEditTaskSpPresenter;
        this.isDocumentFile = isDocumentFile;
        this.isEdit = isEdit;
    }


    @Override
    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view ;

        ThumbnailBean obj = imageList.get(viewType);

        if (obj.isAddView()) {
            //Show Add View
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_add, parent, false);
            return new AddViewHolder(view);
        } else {
            //Show Image Thumbnails
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_image_thumb, parent, false);
            return new ThumbImageViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            if (holder instanceof ThumbImageViewHolder) {
                System.out.println("ThumbImageAdapter.onBindViewHolder===>" + position);
                System.out.println("ThumbImageAdapter.imageList = " + imageList.get(position).getImagePath());
                final ThumbImageViewHolder vh = (ThumbImageViewHolder) holder;
                if (imageList.get(position).getImagePath().startsWith("http")
                        && imageList.get(position).getImagePath().substring(imageList.get(position).getImagePath().lastIndexOf(".") + 1).equals("pdf")) {
                    vh.thumbImage.setImageResource(R.drawable.ic_doc_appliance);
                    vh.imgProgress.setVisibility(View.GONE);
                } else if (imageList.get(position).getImagePath().startsWith("http")) {
                    Picasso.with(context).load(imageList.get(position).getImagePath()).resize(200, 200).centerCrop().into(vh.thumbImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            vh.imgProgress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            vh.imgProgress.setVisibility(View.GONE);
                        }
                    });
                } else {
                    //Its image file from storage
                    Picasso.with(context).load(new File(imageList.get(position).getImagePath())).resize(200, 200).centerCrop().into(vh.thumbImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            vh.imgProgress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            vh.thumbImage.setImageResource(R.drawable.ic_doc_appliance);
                            vh.imgProgress.setVisibility(View.GONE);
                        }
                    });
                }

            } else if (holder instanceof AddViewHolder) {
                // No Work in this section

                System.out.println("AddViewHolder.onBindViewHolder===>" + position);
                System.out.println("AddViewHolder.imageList = " + imageList.get(position).getImagePath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateList(List<ThumbnailBean> list){
        this.imageList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return (imageList == null) ? 0 : imageList.size();
    }

    private class AddViewHolder extends RecyclerView.ViewHolder {
        private CardView addView;

        private AddViewHolder(View itemView) {
            super(itemView);
            addView = (CardView) itemView.findViewById(R.id.view_add);

            if (isDocumentFile) {
                addView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isEdit) {
                            if (appliancePresenterImpl != null) {
                                appliancePresenterImpl.onAddFileClick();
                            }/*else  if (addEditProjectServiceProviderPresenter != null) {
                                addEditProjectServiceProviderPresenter.onAddFileClick();
                            }*/
                        }

                    }
                });
            } else {
                addView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isEdit) {
                            if (appliancePresenterImpl != null) {
                                appliancePresenterImpl.onAddOptionClick();
                            } else if (docPresenterImpl != null) {
                                docPresenterImpl.onAddOptionClick();
                            } else if (projectPresenterImpl != null) {
                                projectPresenterImpl.onAddOptionClick();
                            } else if (addEditProjectServiceProviderPresenter != null) {
                                addEditProjectServiceProviderPresenter.onAddOptionClick();
                            } else if (addEditTaskSpPresenter != null) {
                                addEditTaskSpPresenter.onAddOptionClick();
                            }
                        }
                    }
                });
            }

        }
    }

    private class ThumbImageViewHolder extends RecyclerView.ViewHolder {
        private ImageView thumbImage, deleteBtn;
        private ProgressBar imgProgress;

        private ThumbImageViewHolder(View itemView) {
            super(itemView);
            thumbImage = (ImageView) itemView.findViewById(R.id.thumb_image);
            deleteBtn = (ImageView) itemView.findViewById(R.id.img_delete);
            imgProgress = (ProgressBar) itemView.findViewById(R.id.thumb_progress);

            if (isEdit) {
                deleteBtn.setVisibility(View.VISIBLE);
                deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (appliancePresenterImpl != null) {
                            if (isDocumentFile) {
                                appliancePresenterImpl.deleteDocImage(getLayoutPosition());
                            } else {
                                appliancePresenterImpl.deleteImage(getLayoutPosition());
                            }
                        } else if (docPresenterImpl != null) {
                            docPresenterImpl.deleteImage(getLayoutPosition());
                        }else if(projectPresenterImpl!=null){
                            projectPresenterImpl.deleteImage(getLayoutPosition());
                        } else if (addEditProjectServiceProviderPresenter != null) {
                            addEditProjectServiceProviderPresenter.deleteImage(getLayoutPosition());
                        } else if (addEditTaskSpPresenter != null) {
                            addEditTaskSpPresenter.deleteImage(getLayoutPosition());
                        }


                    }
                });

            } else {
                thumbImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (docPresenterImpl != null) {
                            docPresenterImpl.onItemClick(getLayoutPosition(), thumbImage);
                        }
                    }
                });

                deleteBtn.setVisibility(View.GONE);
            }

        }

    }
}
