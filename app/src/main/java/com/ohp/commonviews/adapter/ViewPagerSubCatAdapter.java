/*
 * Copyright (c) 2016.  Nimble91creations Inc. and/or its affiliates. All rights reserved.
 *
 *                              Redistribution and use in source and binary forms, with or without
 *                            modification, are permitted provided that the following conditions are met:
 *
 *                            - Redistributions of source code must retain the above copyright
 *                                notice, this list of conditions and the following disclaimer.
 *
 *                            - Redistributions in binary form must reproduce the above copyright
 *                         notice, this list of conditions and the following disclaimer in the
 *                            documentation and/or other materials provided with the distribution.
 */

package com.ohp.commonviews.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.ohp.R;
import com.ohp.commonviews.beans.ThumbnailBean;
import com.ohp.ownerviews.presenter.AddEditAppliancePresenterImpl;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class ViewPagerSubCatAdapter extends PagerAdapter {

    Context context;
    LayoutInflater inflater;
    private List<ThumbnailBean> list;

    // Declare Variables
    private AddEditAppliancePresenterImpl addEditAppliancePresenter;

    public ViewPagerSubCatAdapter(Context context, List<ThumbnailBean> model, AddEditAppliancePresenterImpl addEditAppliancePresenter) {
        this.context = context;
        this.list = new ArrayList<>(model);
        this.addEditAppliancePresenter = addEditAppliancePresenter;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        // Declare Variables
        final ImageView imgflag;
        final ProgressBar spinner;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_item, container,
                false);

        // Locate the ImageView in viewpager_item.xml
        imgflag = (ImageView) itemView.findViewById(R.id.flag);
        spinner = (ProgressBar) itemView.findViewById(R.id.spinner);
        imgflag.setOnClickListener(null);

        // touch.setImageBitmap(bitmap);
        final ThumbnailBean newsObj = list.get(position);
        if (newsObj.getImagePath() != null && !newsObj.getImagePath().isEmpty())
            if (newsObj.getImagePath().startsWith("http")) {
                if (newsObj.getImagePath().substring(newsObj.getImagePath().lastIndexOf(".") + 1).equals("pdf")) {
                    imgflag.setImageResource(R.drawable.default_projects);
                    spinner.setVisibility(View.GONE);
                } else {
                    Picasso.with(context).load(newsObj.getImagePath()).resize(500, 500).centerCrop().into(imgflag, new Callback() {
                        @Override
                        public void onSuccess() {
                            spinner.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                            spinner.setVisibility(View.GONE);
                        }
                    });
                }
            } else {
                if(newsObj.getImagePath().substring(newsObj.getImagePath().lastIndexOf(".") + 1).equals("pdf")){
                    imgflag.setImageResource(R.drawable.default_projects);
                    spinner.setVisibility(View.GONE);
                }else{
                    Picasso.with(context).load(new File(newsObj.getImagePath())).resize(500, 500).centerCrop().into(imgflag, new Callback() {
                        @Override
                        public void onSuccess() {
                            spinner.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                            spinner.setVisibility(View.GONE);
                        }
                    });
                }

            }
        else {
            spinner.setVisibility(View.GONE);
        }
        // Add viewpager_item.xml to ViewPager
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        container.removeView((RelativeLayout) object);

    }


}
