package com.ohp.commonviews.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.beans.GetMessagesResponse;
import com.ohp.commonviews.presenter.ChattingFragmentPresenter;
import com.ohp.utils.Utils;

import java.util.List;

/**
 * Created by vishal.sharma on 9/7/2017.
 */

public class OneToOneChattingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ChattingFragmentPresenter chattingFragmentPresenter;
    private List<GetMessagesResponse.DataBean> getMessagesResponseList;


    public OneToOneChattingAdapter(Context activity, ChattingFragmentPresenter chattingFragmentPresenter, List<GetMessagesResponse.DataBean> getMessagesResponseList) {
        this.context = activity;
        this.chattingFragmentPresenter = chattingFragmentPresenter;
        this.getMessagesResponseList = getMessagesResponseList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;
        if (getMessagesResponseList.get(i).getSendBy() == Utils.getInstance().getUserId(context)) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_layout_chat_sender, viewGroup, false);
            return new SenderHolder(view);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_chat_layout_reciever, viewGroup, false);
            return new RecieverHolder(view);
        }
    }

    @Override
    public int getItemCount() {
        return getMessagesResponseList == null ? 0 : getMessagesResponseList.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SenderHolder) {
            SenderHolder senderHolder = (SenderHolder) holder;
            senderHolder.txt_sender.setText(getMessagesResponseList.get(position).getMessageX());
            senderHolder.txt_date_sender.setText(Utils.getInstance().getYMDMessageFormattedDateTime(getMessagesResponseList.get(position).getCreated()));
        } else if (holder instanceof RecieverHolder) {
            RecieverHolder recieverHolder = (RecieverHolder) holder;
            recieverHolder.txt_reciever.setText(getMessagesResponseList.get(position).getMessageX());
            recieverHolder.txt_date_reciever.setText(Utils.getInstance().getYMDMessageFormattedDateTime(getMessagesResponseList.get(position).getCreated()));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;

    }

    public void updateList(List<GetMessagesResponse.DataBean> getMessagesResponseList) {
        this.getMessagesResponseList = getMessagesResponseList;
        notifyDataSetChanged();
    }

    public void clearAll() {
        this.getMessagesResponseList = null;
        notifyDataSetChanged();
    }

    class SenderHolder extends RecyclerView.ViewHolder {
        TextView txt_sender, txt_date_sender, tvUserNameSender;

        public SenderHolder(View itemView) {
            super(itemView);
            txt_sender = (TextView) itemView.findViewById(R.id.txt_chat_message_sender);
            txt_date_sender = (TextView) itemView.findViewById(R.id.txt_date_sender);
        }

    }

    class RecieverHolder extends RecyclerView.ViewHolder {
        TextView txt_reciever, txt_date_reciever, tvUserNameReciever;

        public RecieverHolder(View itemView) {
            super(itemView);
            txt_reciever = (TextView) itemView.findViewById(R.id.txt_chat_message_reciever);
            txt_date_reciever = (TextView) itemView.findViewById(R.id.txt_date_reciever);
        }
    }
}
