package com.ohp.commonviews.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.beans.ResourceBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class ReminderAdapter extends ArrayAdapter<ResourceBean.DataBean.InfoBean> {
    private Context context;
    private List<ResourceBean.DataBean.InfoBean> mList;

    public ReminderAdapter(Context context, int resource, int textViewResourceId,
                           List<ResourceBean.DataBean.InfoBean> objects) {
        super(context, resource, textViewResourceId, objects);
        this.context = context;
        this.mList = objects;
    }


    @Override
    public int getCount() {
        if (mList != null)
            return mList.size();
        else
            return 0;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return initView(convertView, position, Color.BLACK, 10);
    }

    private View initView(View convertView, int position, int color, int padding) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = LayoutInflater.from(context).inflate(R.layout.spinner_item, null);
            holder.title = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        ResourceBean.DataBean.InfoBean model = mList.get(position);
        holder.title.setText(model.getValue());
        holder.title.setTextColor(color);
        holder.title.setPadding(padding, padding, padding, padding);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return initView(convertView, position, Color.BLACK, 10);
    }

    public void updateList(Context mContext, List<ResourceBean.DataBean.InfoBean> mList) {


        ResourceBean.DataBean.InfoBean bean;
        if (mList.size() != 0) {
            bean = new ResourceBean.DataBean.InfoBean();
            bean.setValue("Choose Repeat");
            bean.setID(0);
        } else {
            bean = new ResourceBean.DataBean.InfoBean();
            bean.setValue("No Repeat");
            bean.setID(0);
        }

        this.context = mContext;
        this.mList = new ArrayList<>();
        this.mList.add(bean);
        this.mList.addAll(mList);
        notifyDataSetChanged();
    }

    public void updateListEvent(Context mContext, List<ResourceBean.DataBean.InfoBean> mList) {


        ResourceBean.DataBean.InfoBean bean;
        if (mList.size() != 0) {
            bean = new ResourceBean.DataBean.InfoBean();
            bean.setValue("Choose Event Reminder");
            bean.setID(0);
        } else {
            bean = new ResourceBean.DataBean.InfoBean();
            bean.setValue("No Event Reminder");
            bean.setID(0);
        }

        this.context = mContext;
        this.mList = new ArrayList<>();
        this.mList.add(bean);
        this.mList.addAll(mList);
        notifyDataSetChanged();
    }


    public void updateListEventTask(Context mContext, List<ResourceBean.DataBean.InfoBean> mList) {


        ResourceBean.DataBean.InfoBean bean;
        if (mList.size() != 0) {
            bean = new ResourceBean.DataBean.InfoBean();
            bean.setValue("Choose Task Repeat");
            bean.setID(0);
        } else {
            bean = new ResourceBean.DataBean.InfoBean();
            bean.setValue("No Task Repeat");
            bean.setID(0);
        }

        this.context = mContext;
        this.mList = new ArrayList<>();
        this.mList.add(bean);
        this.mList.addAll(mList);
        notifyDataSetChanged();
    }



    private class Holder {
        TextView title;
    }
}