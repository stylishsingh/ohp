package com.ohp.commonviews.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.beans.ResourceBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amanpal Singh.
 */


public class ApplianceTypeAndBrandAdapter extends ArrayAdapter<ResourceBean.DataBean.InfoBean> {
    private Context context;
    private List<ResourceBean.DataBean.InfoBean> mList;

    public ApplianceTypeAndBrandAdapter(Context context, int resource, int textViewResourceId,
                                        List<ResourceBean.DataBean.InfoBean> objects) {
        super(context, resource, textViewResourceId, objects);
        this.context = context;
        this.mList = objects;
    }


    @Override
    public int getCount() {
        if (mList != null)
            return mList.size();
        else
            return 0;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return initView(convertView, position, Color.BLACK, 10);
    }

    private View initView(View convertView, int position, int color, int padding) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = LayoutInflater.from(context).inflate(R.layout.spinner_item, null);
            holder.title = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        ResourceBean.DataBean.InfoBean model = mList.get(position);
        holder.title.setText(model.getValue());
        holder.title.setTextColor(color);
        holder.title.setPadding(padding, padding, padding, padding);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return initView(convertView, position, Color.BLACK, 10);
    }

    public void updateList(Context mContext, List<ResourceBean.DataBean.InfoBean> mList) {

        this.context = mContext;
        this.mList = new ArrayList<>();
        this.mList.addAll(mList);
        notifyDataSetChanged();
    }

    public void updateListWithOption(Context mContext, List<ResourceBean.DataBean.InfoBean> mList) {

        this.context = mContext;
        this.mList = new ArrayList<>();
        ResourceBean.DataBean.InfoBean bean = new ResourceBean.DataBean.InfoBean();
        if (mList.size() > 0) {
            bean.setValue("Choose Brand");
            bean.setID(0);
            this.mList.add(bean);
        } else {
            bean.setValue("No Brand");
            bean.setID(0);
            this.mList.add(bean);
        }
        this.mList.addAll(mList);
        notifyDataSetChanged();
    }

    public void updateListApplianceType(Context mContext, List<ResourceBean.DataBean.InfoBean> mList) {

        this.context = mContext;
        this.mList = new ArrayList<>();
        ResourceBean.DataBean.InfoBean bean = new ResourceBean.DataBean.InfoBean();
        if (mList.size() > 0) {
            bean.setValue("Choose Appliance Type");
            bean.setID(0);
            this.mList.add(bean);
        } else {
            bean.setValue("No Appliance Type");
            bean.setID(0);
            this.mList.add(bean);
        }
        this.mList.addAll(mList);
        notifyDataSetChanged();
    }

    public void updateListCategoryType(Context mContext, List<ResourceBean.DataBean.InfoBean> mList) {

        this.context = mContext;
        this.mList = new ArrayList<>();
        ResourceBean.DataBean.InfoBean bean = new ResourceBean.DataBean.InfoBean();
        /*if (mList.size() > 0) {
            bean.setValue("Choose Category Type");
            bean.setID(0);
            this.mList.add(bean);
        } else */

        if (mList.size() == 0) {
            bean.setValue("No Category Type");
            bean.setID(0);
            this.mList.add(bean);
        }
        this.mList.addAll(mList);
        notifyDataSetChanged();
    }


    private class Holder {
        TextView title;
    }
}