package com.ohp.commonviews.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.beans.ReviewModel;
import com.ohp.commonviews.presenter.ReviewsFragmentPresenterImpl;
import com.ohp.enums.ScreenNavigation;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * @author Amanpal Singh.
 */

public class ReviewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private List<ReviewModel.DataBean> listItems;
    private ReviewsFragmentPresenterImpl presenter;

    public ReviewsAdapter(List<ReviewModel.DataBean> items, Context context, ReviewsFragmentPresenterImpl presenter) {
        listItems = items;
        this.context = context;
        this.presenter = presenter;
    }

    public void update(List<ReviewModel.DataBean> items) {
        listItems = items;
        notifyDataSetChanged();
    }

    public void clearAll() {
        listItems = null;
        notifyDataSetChanged();
    }

    public void deleteItem(int deletedItemPos) {
        notifyItemRemoved(deletedItemPos);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate your layout and pass it to view holder
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_reviews, parent, false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        try {
            if (holder instanceof ViewHolder) {
                final ViewHolder dataHolder = (ViewHolder) holder;
                ReviewModel.DataBean dataObj = listItems.get(position);
                dataHolder.tvName.setText(dataObj.getRatingBy());
                if (dataObj.getRating() != 0)
                    dataHolder.rbUser.setRating(Float.parseFloat(String.valueOf(dataObj.getRating())));
                dataHolder.tvReviews.setText(dataObj.getFeedback());

                if (!TextUtils.isEmpty(dataObj.getProfileImage200X200())) {
                    Picasso.with(context).load(dataObj.getProfileImage200X200()).resize(300, 300).centerCrop().into(dataHolder.profilePic, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                            dataHolder.profilePic.setImageResource(R.drawable.profile_img);
                        }
                    });
                } else {
                    dataHolder.profilePic.setImageResource(R.drawable.profile_img);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return listItems == null ? 0 : listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName, tvReviews;
        private RatingBar rbUser;
        private CardView cvItem;
        private CircleImageView profilePic;

        public ViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvReviews = (TextView) view.findViewById(R.id.tv_reviews);
            rbUser = (RatingBar) view.findViewById(R.id.rating);
            cvItem = (CardView) view.findViewById(R.id.cv_item);
            profilePic = (CircleImageView) view.findViewById(R.id.avatar);

            cvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClicked(getAdapterPosition(), ScreenNavigation.DETAIL_ITEM, profilePic);
                }
            });
        }

    }
}
