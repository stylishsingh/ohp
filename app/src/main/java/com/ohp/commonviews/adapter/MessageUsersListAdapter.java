package com.ohp.commonviews.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ohp.R;
import com.ohp.commonviews.activities.HolderActivity;
import com.ohp.commonviews.beans.GetChatListResponse;
import com.ohp.commonviews.presenter.MessageListingFragmentPresenter;
import com.ohp.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by vishal.sharma on 9/8/2017.
 */

public class MessageUsersListAdapter extends RecyclerView.Adapter<MessageUsersListAdapter.Viewholder> {
    private Context context;
    private MessageListingFragmentPresenter messageListingFragmentPresenter;
    private List<GetChatListResponse.DataBean> listChatResponse;

    public MessageUsersListAdapter(Context context, MessageListingFragmentPresenter messageListingFragmentPresenter, List<GetChatListResponse.DataBean> listChatResponse) {
        this.context = context;
        this.messageListingFragmentPresenter = messageListingFragmentPresenter;
        this.listChatResponse = listChatResponse;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).from(context).inflate(R.layout.fraghment_message_user_list_layout, viewGroup, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(Viewholder viewholder, int position) {
        viewholder.txt_mesage.setText(listChatResponse.get(position).getMessageX());
        viewholder.txt_senderName.setText(listChatResponse.get(position).getFullName());
        if (!TextUtils.isEmpty(listChatResponse.get(position).getProfileImage200X200()))
            Picasso.with(context).load(listChatResponse.get(position).getProfileImage200X200()).into(viewholder.imageView);
    }

    public void updateList(List<GetChatListResponse.DataBean> listChatResponse) {
        this.listChatResponse = listChatResponse;
        notifyDataSetChanged();
    }

    public void clearAll() {
        listChatResponse = null;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listChatResponse == null ? 0 : listChatResponse.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView txt_senderName, txt_mesage;
        CardView cardView;

        public Viewholder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.profile_img);
            txt_senderName = (TextView) itemView.findViewById(R.id.txt_senderName);
            txt_mesage = (TextView) itemView.findViewById(R.id.txt_message);
            cardView = (CardView) itemView.findViewById(R.id.parent_cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra(Constants.DESTINATION, Constants.CLICK_CHAT);
                    intent.putExtra(Constants.SEND_BY_NAME, listChatResponse.get(getAdapterPosition()).getFullName());
                    intent.putExtra(Constants.SEND_BY_USERID, String.valueOf(listChatResponse.get(getAdapterPosition()).getUserId()));
                    // intent.putExtra(Constants.MESSAGE_OBJECT, listChatResponse.get(getLayoutPosition()));
                    context.startActivity(intent);
                }
            });
        }

    }

}
