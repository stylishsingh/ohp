package com.ohp.commonviews.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessaging;
import com.library.basecontroller.AppBaseCompatActivity;
import com.library.mvp.ActivityPresenter;
import com.ohp.commonviews.fragment.ChangePassword;
import com.ohp.commonviews.fragment.ChattingFragment;
import com.ohp.commonviews.fragment.ForgotPwdFragment;
import com.ohp.commonviews.fragment.ImageSliderFragment;
import com.ohp.commonviews.fragment.LoginFragment;
import com.ohp.commonviews.fragment.MessageListingFragment;
import com.ohp.commonviews.fragment.RegisterFragment;
import com.ohp.commonviews.fragment.ResetPwdFragment;
import com.ohp.commonviews.fragment.ReviewsFragment;
import com.ohp.commonviews.fragment.SettingFragment;
import com.ohp.commonviews.fragment.VerifyOTPFragment;
import com.ohp.commonviews.fragment.ViewImagesFragment;
import com.ohp.enums.CurrentScreen;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.fragment.AddEditApplianceFragment;
import com.ohp.ownerviews.fragment.AddEditContact;
import com.ohp.ownerviews.fragment.AddEditDocumentFragment;
import com.ohp.ownerviews.fragment.AddEditProjectFragment;
import com.ohp.ownerviews.fragment.AddEditPropertyFragment;
import com.ohp.ownerviews.fragment.AllProjectsFragments;
import com.ohp.ownerviews.fragment.ApplianceListFragment;
import com.ohp.ownerviews.fragment.DocumentListFragment;
import com.ohp.ownerviews.fragment.OwnerAddEditEventFragment;
import com.ohp.ownerviews.fragment.OwnerDashboardFragment;
import com.ohp.ownerviews.fragment.OwnerEventsListFragment;
import com.ohp.ownerviews.fragment.OwnerProfileFragment;
import com.ohp.ownerviews.fragment.ServiceProviderList;
import com.ohp.ownerviews.fragment.SpDetailFragment;
import com.ohp.serviceproviderviews.activities.SPDashboardActivity;
import com.ohp.serviceproviderviews.fragment.AddEditProjectServiceProvider;
import com.ohp.serviceproviderviews.fragment.AddEditTaskFragmentSp;
import com.ohp.serviceproviderviews.fragment.AddEditTeamMemberFragment;
import com.ohp.serviceproviderviews.fragment.AllCompletedViewProjectsServiceFragment;
import com.ohp.serviceproviderviews.fragment.AllMyProjectsViewFragment;
import com.ohp.serviceproviderviews.fragment.AllWonViewProjectFragment;
import com.ohp.serviceproviderviews.fragment.DashboardAllProjectsFragment;
import com.ohp.serviceproviderviews.fragment.GetNearestProjectsFragment;
import com.ohp.serviceproviderviews.fragment.MyBidsFragment;
import com.ohp.serviceproviderviews.fragment.ProjectDetailServiceFragment;
import com.ohp.serviceproviderviews.fragment.SPProfileFragment;
import com.ohp.serviceproviderviews.fragment.SpAddEditEventFragment;
import com.ohp.serviceproviderviews.fragment.SpDashboardFragment;
import com.ohp.serviceproviderviews.fragment.SpEventListFragment;
import com.ohp.serviceproviderviews.fragment.TaskFragment;
import com.ohp.utils.SharedPreferencesHandler;
import com.ohp.utils.Utils;

public abstract class BaseActivity<T extends ActivityPresenter> extends AppBaseCompatActivity<T> {

    public static BaseActivity refActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        refActivity = this;
    }

    public void navigateTo(int container, Fragment fragment, boolean isBackStack) {
        try {
            FragmentTransaction fts = getSupportFragmentManager().beginTransaction();
            fts.replace(container, fragment, fragment.getClass().getSimpleName());
            if (isBackStack)
                fts.addToBackStack(fragment.getClass().getSimpleName());
            fts.commit();

        } catch (Exception e) {
            Utils.e(e);
        }

    }

    public void navigateToWithBundle(int container, Fragment fragment, boolean isBackStack, Bundle bundle) {
        fragment.setArguments(bundle);
        FragmentTransaction fts = getSupportFragmentManager().beginTransaction();
        fts.replace(container, fragment, fragment.getClass().getSimpleName());
        if (isBackStack)
            fts.addToBackStack(fragment.getClass().getSimpleName());
        fts.commit();
    }

    public void navigateReplacingCurrentWithBundle(int container, Fragment currentFragment, Fragment fragmentToNavigate, Bundle bundle) {
        fragmentToNavigate.setArguments(bundle);
        FragmentTransaction fts = getSupportFragmentManager().beginTransaction();
        getSupportFragmentManager().popBackStack();
        fts.replace(container, fragmentToNavigate);
        fts.addToBackStack(fragmentToNavigate.getClass().getSimpleName());
        fts.remove(currentFragment).commit();
    }

    public void addFragment(int container, Fragment fragment, boolean isBackStack) {
        FragmentTransaction fts = getSupportFragmentManager().beginTransaction();
        fts.add(container, fragment, fragment.getClass().getSimpleName());
        if (isBackStack)
            fts.addToBackStack(fragment.getClass().getSimpleName());
        fts.commit();
    }

    public void addFragmentWithBundle(int container, Fragment fragment, boolean isBackStack, Bundle bundle) {
        FragmentTransaction fts = getSupportFragmentManager().beginTransaction();
        fragment.setArguments(bundle);
        fts.add(container, fragment, fragment.getClass().getSimpleName());
        if (isBackStack)
            fts.addToBackStack(fragment.getClass().getSimpleName());
        fts.commit();
    }

    public void oneStepBack() {
        FragmentTransaction fts = getSupportFragmentManager().beginTransaction();
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() >= 1) {
            fragmentManager.popBackStackImmediate();
            fts.commit();
        } else {
            supportFinishAfterTransition();
        }
    }

    public void moveToParentActivity() {
//        Intent upIntent = NavUtils.getParentActivityIntent(TaskDetailActivity.this);
//        NavUtils.navigateUpTo(TaskDetailActivity.this, upIntent);
        Intent intent;
        if (Utils.getInstance().getRole(this) == 1)
            intent = new Intent(this, OwnerDashboardActivity.class);
        else
            intent = new Intent(this, SPDashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * Generic method to change fragment
     */
    public void changeScreen(int container, CurrentScreen currentScreen, boolean isAddFragment, boolean isBackStack, Bundle bundle) {
        Fragment currentFragment = null;
        switch (currentScreen) {
            case LOGIN_SCREEN:
                currentFragment = new LoginFragment();
                break;
            case FORGOT_PWD_SCREEN:
                currentFragment = new ForgotPwdFragment();
                break;
            case CHANGE_PASSWORD_SCREEN:
                currentFragment = new ResetPwdFragment();
                currentFragment.setArguments(bundle);
                break;
            case REGISTER_SCREEN:
                currentFragment = new RegisterFragment();
                currentFragment.setArguments(bundle);
                break;
            case VERIFY_OTP_SCREEN:
                currentFragment = new VerifyOTPFragment();
                currentFragment.setArguments(bundle);
                break;
            case OWNER_PROFILE_SCREEN:
                currentFragment = new OwnerProfileFragment();
                currentFragment.setArguments(bundle);
                break;
            case ADD_PROPERTY_SCREEN:
                currentFragment = new AddEditPropertyFragment();
                break;
            case EDIT_PROPERTY_SCREEN:
                currentFragment = new AddEditPropertyFragment();
                currentFragment.setArguments(bundle);
                break;
            case PROPERTY_OWNER_DASHBOARD_SCREEN:
                currentFragment = new OwnerDashboardFragment();
                break;
            case ADD_EDIT_APPLIANCE_SCREEN:
                currentFragment = new AddEditApplianceFragment();
                currentFragment.setArguments(bundle);
                break;

            case APPLIANCE_LIST_SCREEN:
                currentFragment = new ApplianceListFragment();
                currentFragment.setArguments(bundle);
                break;
            case CHANGE_PASSWORD:
                currentFragment = new ChangePassword();
                currentFragment.setArguments(bundle);
                break;
            case DOCUMENT_LIST_SCREEN:
                currentFragment = new DocumentListFragment();
                break;

            case ADD_EDIT_DOCUMENT_SCREEN:
                currentFragment = new AddEditDocumentFragment();
                currentFragment.setArguments(bundle);
                break;

            case SP_PROFILE_SCREEN:
                currentFragment = new SPProfileFragment();
                currentFragment.setArguments(bundle);
                break;
            case BROWSER_SERVICE_PROVIDER:
                currentFragment = new ServiceProviderList();
                currentFragment.setArguments(bundle);
                break;

            case OWNER_ADD_EDIT_EVENT_SCREEN:
                currentFragment = new OwnerAddEditEventFragment();
                currentFragment.setArguments(bundle);
                break;

            case SP_ADD_EDIT_EVENT_SCREEN:
                currentFragment = new SpAddEditEventFragment();
                currentFragment.setArguments(bundle);
                break;
            case ADD_PROJECT_SCREEN:
                currentFragment = new AddEditProjectFragment();
                currentFragment.setArguments(bundle);
                break;
            case CLICK_EDIT_PROJECT:
                currentFragment = new AddEditProjectFragment();
                currentFragment.setArguments(bundle);
                break;
            case PIC_DETAIL_SCREEN:
                currentFragment = new ViewImagesFragment();
                currentFragment.setArguments(bundle);
                break;
            case IMAGE_SLIDER_SCREEN:
                currentFragment = new ImageSliderFragment();
                currentFragment.setArguments(bundle);
                break;
            case OWNER_EVENT_LIST_SCREEN:
                currentFragment = new OwnerEventsListFragment();
                currentFragment.setArguments(bundle);
                break;
            case SP_EVENT_LIST_SCREEN:
                currentFragment = new SpEventListFragment();
                currentFragment.setArguments(bundle);
                break;
            case CLICK_ADD_PROJECT:
                currentFragment = new AllProjectsFragments();
                currentFragment.setArguments(bundle);
                break;
            case CLICK_ADD_CONTACT:
                currentFragment = new AddEditContact();
                currentFragment.setArguments(bundle);
                break;
            case ALL_WON_PROJECT_SERVICE_PROVIDER:
                currentFragment = new AllWonViewProjectFragment();
                currentFragment.setArguments(bundle);
                break;
            case ALL_COMPLETED_PROJECT_SERVICE_PROVIDER:
                currentFragment = new AllCompletedViewProjectsServiceFragment();
                currentFragment.setArguments(bundle);
                break;
            case ALL_MYPROJECTS_SERVICE_PROVIDER:
                currentFragment = new AllMyProjectsViewFragment();
                currentFragment.setArguments(bundle);

                break;
            case MY_PROJECTS_SERVICE_PROVIDER_View:
                currentFragment = new AddEditProjectServiceProvider();
                currentFragment.setArguments(bundle);

                break;
            case MY_PROJECTS_SERVICE_PROVIDER_EDIT:
                currentFragment = new AddEditProjectServiceProvider();
                currentFragment.setArguments(bundle);

                break;
            case ADD_EDIT_PROJECT_SP:
                currentFragment = new AddEditProjectServiceProvider();
                currentFragment.setArguments(bundle);
                break;
            case DASHBOARD_ALL_PROJECT:
                currentFragment = new DashboardAllProjectsFragment();
                currentFragment.setArguments(bundle);
                break;
            case PROJECT_DETAIL_SCREEN_SERVICEPROVIDER:
                currentFragment = new ProjectDetailServiceFragment();
                currentFragment.setArguments(bundle);
                break;
            case PROJECT_DETAIL_WON_PROJECTS:
                currentFragment = new AddEditProjectServiceProvider();
                currentFragment.setArguments(bundle);
                break;
            case SERVICE_NEAREST_PROJECT:
                currentFragment = new GetNearestProjectsFragment();
                currentFragment.setArguments(bundle);
                break;
            case GET_MY_BIDS:
                currentFragment = new MyBidsFragment();
                currentFragment.setArguments(bundle);
                break;
            case SP_DASHBOARD_SCREEN:
                currentFragment = new SpDashboardFragment();
                break;
            case SP_DETAIL_SCREEN:
                currentFragment = new SpDetailFragment();
                currentFragment.setArguments(bundle);
                break;
            case ADD_EDIT_TEAM_SCREEN:
                currentFragment = new AddEditTeamMemberFragment();
                currentFragment.setArguments(bundle);
                break;
            case CLICK_ADD_TASK:
                currentFragment = new AddEditTaskFragmentSp();
                currentFragment.setArguments(bundle);
                break;
            case TASK_DETAIL_SERVICE_PROVIDER:
                currentFragment = new AddEditTaskFragmentSp();
                currentFragment.setArguments(bundle);
                break;
            case CONTACT_DETAIL_PROPERTY_OWNER:
                currentFragment = new AddEditContact();
                currentFragment.setArguments(bundle);
                break;
            case CONTACT_EDIT_PROPERTY_OWNER:
                currentFragment = new AddEditContact();
                currentFragment.setArguments(bundle);
                break;
            case TASK_EDIT_SERVICE_PROVIDER:
                currentFragment = new AddEditTaskFragmentSp();
                currentFragment.setArguments(bundle);
                break;

            case TASK_LIST_SCREEN:
                currentFragment = new TaskFragment();
                currentFragment.setArguments(bundle);
                break;
            case MESSAGE_LIST_SCREEN:
                currentFragment = new MessageListingFragment();
                currentFragment.setArguments(bundle);
                break;
            case REVIEWS_SCREEN:
                currentFragment = new ReviewsFragment();
                currentFragment.setArguments(bundle);
                break;
            case CLICK_CHAT:
                currentFragment = new ChattingFragment();
                currentFragment.setArguments(bundle);
                break;

            case SETTING:
                currentFragment = new SettingFragment();
                currentFragment.setArguments(bundle);
                break;
        }
        if (currentFragment == null) {
            return;
        }
        if (isAddFragment) {
            //Add Fragment
            if (isBackStack)
                addFragmentWithBundle(container, currentFragment, true, bundle);
            else
                addFragmentWithBundle(container, currentFragment, false, bundle);
        } else {
            //Replace Fragment
            if (isBackStack)
                navigateToWithBundle(container, currentFragment, true, bundle);
            else
                navigateToWithBundle(container, currentFragment, false, bundle);
        }


    }

    public void logout(String message) {
//        if (!message.equals("invalid"))
//            Utils.getInstance().showToast(message);

        //UnSubscribe topic to Firebase.
        if (!TextUtils.isEmpty(Utils.getInstance().getTopic(this)))
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Utils.getInstance().getTopic(this));

        SharedPreferencesHandler.clearAll(this);
        Intent logoutIntent = new Intent(this, LoginActivity.class);
        logoutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(logoutIntent);
    }

    /**
     * checks whether internet is enabled or not
     *
     * @return true if enabled otherwise false
     */

    public boolean isInternetAvailable() {
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                    && PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE)) {
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                return activeNetwork != null &&
                        activeNetwork.isConnected();
            } else
                return false;
        } else {
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            return activeNetwork != null &&
                    activeNetwork.isConnected();
        }
    }

}
