package com.ohp.commonviews.activities;

import android.content.Intent;
import android.os.Bundle;

import com.library.mvp.BaseView;
import com.ohp.R;
import com.ohp.commonviews.presenter.HolderActivityPresenterImpl;

/**
 * @author Amanpal Singh.
 */

public class HolderActivity extends BaseActivity<HolderActivityPresenterImpl> implements BaseView {

    @Override
    protected HolderActivityPresenterImpl createPresenter() {
        return new HolderActivityPresenterImpl(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_holder);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initUI() {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        System.out.println("HolderActivity.onNewIntent");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        (getSupportFragmentManager().findFragmentById(R.id.holder_container)).
                onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        oneStepBack();
    }
}
