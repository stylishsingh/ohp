package com.ohp.commonviews.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;

import com.google.firebase.messaging.FirebaseMessaging;
import com.ohp.notifications.NotificationIntent;
import com.ohp.ownerviews.activities.OwnerDashboardActivity;
import com.ohp.ownerviews.activities.OwnerProfileActivity;
import com.ohp.serviceproviderviews.activities.SPDashboardActivity;
import com.ohp.serviceproviderviews.activities.SPProfileActivity;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import org.json.JSONObject;

import java.security.MessageDigest;

import static com.ohp.utils.Constants.NOTIFY_DEACTIVATED_BY_ADMIN;

/**
 * @author Amanpal Singh.
 */

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        printHashKey(this);
        try {
            if (!TextUtils.isEmpty(Utils.getInstance().getTopic(this)))
                FirebaseMessaging.getInstance().unsubscribeFromTopic(Utils.getInstance().getTopic(this));
            /**
             *
             */
            if (!TextUtils.isEmpty(Utils.getInstance().getAccessToken(this))) {
                Bundle bundle = getIntent().getExtras();
                if (bundle != null && bundle.containsKey("meta_data")) {
                    String meta_data = bundle.getString("meta_data");
                    JSONObject object = new JSONObject(meta_data);
                    switch (object.getString(Constants.TYPE)) {
                        case NOTIFY_DEACTIVATED_BY_ADMIN:
                            Utils.getInstance().logout("", this);
                            break;
                        default:
                            startActivity(NotificationIntent.getInstance().createIntent(this, object, true));
                    }


                } else {
                    //check Role of User
                    if (Utils.getInstance().getUserRole(this) == 1 && Utils.getInstance().checkIsVerified(getApplication()) == 1) {
                        if (TextUtils.isEmpty(Utils.getInstance().getUserName(this))) {
                            //User profile is not completed, Move to Profile Screen
                            Intent intent = new Intent(this, OwnerProfileActivity.class);
                            intent.putExtra("is_edit", true);
                            startActivity(intent);

                        } else {
                            //User profile is completed, Move to Dashboard Screen
                            Intent intent = new Intent(this, OwnerDashboardActivity.class);
                            startActivity(intent);
                        }
                    } else if (Utils.getInstance().getUserRole(this) == 2 && Utils.getInstance().checkIsVerified(getApplication()) == 1) {
                        //User id Contractor
                        if (TextUtils.isEmpty(Utils.getInstance().getUserName(this)) || TextUtils.isEmpty(Utils.getInstance().getAddress(this))) {
                            //User profile is not completed, Move to Contractor Profile Screen
                            //User profile is not completed, Move to Profile Screen
                            Intent intent = new Intent(this, SPProfileActivity.class);
                            intent.putExtra("is_edit", true);
                            startActivity(intent);
                        } else {
                            //User profile is not completed, Move to Contractor Dashboard Screen
                            Intent intent = new Intent(this, SPDashboardActivity.class);
                            startActivity(intent);
                        }

                    } else {
                        //Role or verification is not updated Yet, Move to Login Screen and update Role
                        Intent intent = new Intent(this, LoginActivity.class);
                        startActivity(intent);
                    }
                }
            } else {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        Intent intent = new Intent(this, SPProfileActivity.class);
//        intent.putExtra("is_edit", true);
//        startActivity(intent);

        finish();
    }

    /**
     * @param pContext A Context of the application package implementing this class.
     *                 This method is used to create hash key.
     */

    public void printHashKey(Context pContext) {
        try {
            String packageName = pContext.getApplicationContext().getPackageName();
            PackageInfo info = getApplicationContext().getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Utils.d("Hash KEY", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
