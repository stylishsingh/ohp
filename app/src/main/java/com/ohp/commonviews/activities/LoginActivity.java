package com.ohp.commonviews.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ohp.R;
import com.ohp.commonviews.presenter.LoginActivityPresenterImpl;
import com.ohp.commonviews.view.CommonView;

/**
 * Created by root on 19/5/17.
 */

public class LoginActivity extends BaseActivity<LoginActivityPresenterImpl> implements CommonView{

    @Override
    protected LoginActivityPresenterImpl createPresenter() {
        return new LoginActivityPresenterImpl(this,this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initUI() {

    }

    @Override
    public void onBackPressed() {
        oneStepBack();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /**
         * return call back result to login fragment.
         */
        (getSupportFragmentManager().findFragmentById(R.id.login_container)).
                onActivityResult(requestCode,resultCode,data);
    }
}
