package com.ohp.commonviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by Anuj Sharma on 5/24/2017.
 */

public interface VerifyOTPView extends BaseView{
    View getRootView();
    Toolbar getToolbar();
    TextView getToolbarTitle();
    TextInputEditText getVerificationCode();
    AppCompatButton getConfirmBtn();
    TextView getResendBtn();
}
