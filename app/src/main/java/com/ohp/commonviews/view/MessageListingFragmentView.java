package com.ohp.commonviews.view;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.library.mvp.BaseView;

/**
 * Created by vishal.sharma on 9/7/2017.
 */

public interface MessageListingFragmentView extends BaseView {
    View getRootView();

    RecyclerView getRecyclerTaskList();

    RelativeLayout getEmptyLayout();


    NestedScrollView getNSRecyclerView();

    EditText getSearchEditText();

    LinearLayout getCloseBtn();
}
