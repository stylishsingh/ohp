package com.ohp.commonviews.view;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by vishal.sharma on 9/6/2017.
 */

public interface ChattingFragmentView extends BaseView {
    View getRootView();

    RecyclerView getChatRecycler();

    RelativeLayout getBottmView();

    SwipeRefreshLayout getRefreshLayout();

    EditText getMessageText();

    ImageView getSendMessage();

    Toolbar getToolbar();

    RelativeLayout getEmptyLayout();


    TextView getToolbarTitle();

}
