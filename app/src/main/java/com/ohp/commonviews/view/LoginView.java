package com.ohp.commonviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.library.mvp.BaseView;


public interface LoginView extends BaseView {
    View getRootView();

    TextInputEditText getEmail();

    TextInputEditText getPassword();

    AppCompatButton getLoginBtn();

    LinearLayout getFacebookBtn();

    LinearLayout getGooglePlusBtn();

    TextView getSignUpTextView();
}
