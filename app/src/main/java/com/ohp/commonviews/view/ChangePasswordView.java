package com.ohp.commonviews.view;

import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.library.mvp.BaseView;

/**
 * Created by vishal.sharma on 8/23/2017.
 */

public interface ChangePasswordView extends BaseView {
    View getRootView();

    Toolbar getToolbar();

    TextView getToolbarTitle();

    TextInputEditText getOldPassword();

    TextInputEditText getNewPassword();

    TextInputEditText getConfirmPassword();

    AppCompatButton getConfirmBtn();
}
