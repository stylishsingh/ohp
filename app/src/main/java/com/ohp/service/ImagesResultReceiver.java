package com.ohp.service;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * @author Amanpal Singh.
 */

public class ImagesResultReceiver extends ResultReceiver {
    private Receiver mReceiver;

    public ImagesResultReceiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }

    public interface Receiver {
        void onReceiveResult(int resultCode, Bundle resultData);
    }
}
