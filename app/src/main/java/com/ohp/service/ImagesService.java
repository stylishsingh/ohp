package com.ohp.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.ohp.R;
import com.ohp.api.RetrofitClient;
import com.ohp.commonviews.beans.GenericBean;
import com.ohp.commonviews.beans.ThumbnailBean;
import com.ohp.utils.Constants;
import com.ohp.utils.Utils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author AmanpalSingh.
 */

public class ImagesService extends IntentService {


    public ImagesService() {
        super("ImageService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        try {
            if (intent != null && intent.getExtras() != null) {
                ResultReceiver resultReceiver;
                Map<String, String> params;
                List<ThumbnailBean> imageList;
                List<ThumbnailBean> fileList;
                switch (intent.getStringExtra(Constants.SCREEN)) {
                    case Constants.CLICK_APPLIANCE:
                        String applicationId = intent.getStringExtra(Constants.APPLIANCE_ID);
                        resultReceiver = intent.getParcelableExtra(Constants.RECEIVER);
                        imageList = intent.getParcelableArrayListExtra(Constants.IMAGE_LIST);
                        fileList = intent.getParcelableArrayListExtra(Constants.FILE_LIST);
                        params = new HashMap<>();
                        params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(this));
                        params.put(Constants.APPLIANCE_ID, applicationId);
                        if (!imageList.isEmpty())
                            uploadApplianceImages(params, imageList, resultReceiver);
                        if (!fileList.isEmpty())
                            uploadDocumentImages(params, fileList, resultReceiver);
                        break;

                    case Constants.CLICK_DOCUMENT:
                        String documentId = intent.getStringExtra(Constants.DOCUMENT_ID);
                        resultReceiver = intent.getParcelableExtra(Constants.RECEIVER);
                        imageList = intent.getParcelableArrayListExtra(Constants.IMAGE_LIST);
                        params = new HashMap<>();
                        params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(this));
                        params.put(Constants.DOCUMENT_ID, documentId);
                        uploadDocumentImages(params, imageList, resultReceiver);
                        break;
                    case Constants.CLICK_ADD_PROJECT:
                        String projectId = intent.getStringExtra(Constants.PROJECT_ID);
                        resultReceiver = intent.getParcelableExtra(Constants.RECEIVER);
                        imageList = intent.getParcelableArrayListExtra(Constants.IMAGE_LIST);
                        params = new HashMap<>();
                        params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(this));
                        params.put(Constants.PROJECT_ID, projectId);
                        if (!imageList.isEmpty())
                            uploadProjectImages(params, imageList, resultReceiver);
                        break;
                    case Constants.ADD_EDIT_PROJECT_SP:
                        String projectId_sp = intent.getStringExtra(Constants.PROJECT_ID);
                        resultReceiver = intent.getParcelableExtra(Constants.RECEIVER);
                        imageList = intent.getParcelableArrayListExtra(Constants.IMAGE_LIST);
                        params = new HashMap<>();
                        params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(this));
                        params.put(Constants.PROJECT_ID, projectId_sp);
                        if (!imageList.isEmpty())
                            uploadProjectImages(params, imageList, resultReceiver);
                        break;
                    case Constants.CLICK_ADD_TASK:
                        String projectId_task_sp = intent.getStringExtra(Constants.TASK_ID);
                        resultReceiver = intent.getParcelableExtra(Constants.RECEIVER);
                        imageList = intent.getParcelableArrayListExtra(Constants.IMAGE_LIST);
                        params = new HashMap<>();
                        params.put(Constants.ACCESS_TOKEN, Utils.getInstance().getAccessToken(this));
                        params.put(Constants.TASK_ID, projectId_task_sp);
                        if (!imageList.isEmpty())

                            uploadTaskImages(params, imageList, resultReceiver);
                        break;
                }


            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadTaskImages(Map<String, String> param, List<ThumbnailBean> images, final ResultReceiver resultReceiver) {
        MultipartBody.Part[] body = new MultipartBody.Part[images.size()];

        for (int i = 0; i < images.size(); i++) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), new File(images.get(i).getImagePath()));
            body[i] = MultipartBody.Part.createFormData(String.format(Locale.ENGLISH, "document_avatar[%d]", i), images.get(i).getName(), requestBody);
        }
        HashMap<String, RequestBody> bodyParam = new HashMap<>();
        for (Map.Entry<String, String> obj : param.entrySet()) {
            bodyParam.put(obj.getKey(), RequestBody.create(MediaType.parse("multipart/form-data"), obj.getValue()));
        }
        Call<GenericBean> call = RetrofitClient.getRetrofitClient().uploadTaskImages(bodyParam, body, Constants.KEY);
        call.enqueue(new Callback<GenericBean>() {
            @Override
            public void onResponse(Call<GenericBean> call, Response<GenericBean> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        Bundle bundle = new Bundle();
                        bundle.putString("message", response.body().getMessage());
//                        resultReceiver.onReceiveResult(200, bundle);
                        generateNotification("Task Document Images uploaded successfully.");
                        resultReceiver.send(200, bundle);

                    }
                }
            }

            @Override
            public void onFailure(Call<GenericBean> call, Throwable t) {

            }
        });
    }

    public void uploadProjectImages(Map<String, String> param, List<ThumbnailBean> images,
                                    final ResultReceiver resultReceiver) {
        MultipartBody.Part[] body = new MultipartBody.Part[images.size()];

        for (int i = 0; i < images.size(); i++) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), new File(images.get(i).getImagePath()));
            body[i] = MultipartBody.Part.createFormData(String.format(Locale.ENGLISH, "project_image_avatar[%d]", i), images.get(i).getName(), requestBody);
        }
        HashMap<String, RequestBody> bodyParam = new HashMap<>();
        for (Map.Entry<String, String> obj : param.entrySet()) {
            bodyParam.put(obj.getKey(), RequestBody.create(MediaType.parse("multipart/form-data"), obj.getValue()));
        }
        Call<GenericBean> call = RetrofitClient.getRetrofitClient().uploadProjectImages(bodyParam, body, Constants.KEY);
        call.enqueue(new Callback<GenericBean>() {
            @Override
            public void onResponse(Call<GenericBean> call, Response<GenericBean> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        Bundle bundle = new Bundle();
                        bundle.putString("message", response.body().getMessage());
//                        resultReceiver.onReceiveResult(200, bundle);
                        generateNotification("Project Images uploaded successfully.");
                        resultReceiver.send(200, bundle);

                    }
                }
            }

            @Override
            public void onFailure(Call<GenericBean> call, Throwable t) {

            }
        });
    }

    public void uploadApplianceImages(Map<String, String> param, List<ThumbnailBean> images,
                                      final ResultReceiver resultReceiver) {
        MultipartBody.Part[] body = new MultipartBody.Part[images.size()];

        for (int i = 0; i < images.size(); i++) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), new File(images.get(i).getImagePath()));
            body[i] = MultipartBody.Part.createFormData(String.format(Locale.ENGLISH, "appliance_avatar[%d]", i), images.get(i).getName(), requestBody);
        }

        HashMap<String, RequestBody> bodyParam = new HashMap<>();
        for (Map.Entry<String, String> obj : param.entrySet()) {
            bodyParam.put(obj.getKey(), RequestBody.create(MediaType.parse("multipart/form-data"), obj.getValue()));
        }

        Call<GenericBean> call = RetrofitClient.getRetrofitClient().uploadApplianceImages(bodyParam, body, Constants.KEY);
        call.enqueue(new Callback<GenericBean>() {
            @Override
            public void onResponse(Call<GenericBean> call, Response<GenericBean> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        Bundle bundle = new Bundle();
                        bundle.putString("message", response.body().getMessage());
//                        resultReceiver.onReceiveResult(200, bundle);
                        generateNotification("Appliance images uploaded successfully.");
                        resultReceiver.send(200, bundle);
                    }
                }
            }

            @Override
            public void onFailure(Call<GenericBean> call, Throwable t) {

            }
        });
    }


    public void uploadDocumentImages(Map<String, String> param, List<ThumbnailBean> files,
                                     final ResultReceiver resultReceiver) {
        MultipartBody.Part[] body = new MultipartBody.Part[files.size()];


        for (int i = 0; i < files.size(); i++) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), new File(files.get(i).getImagePath()));
            body[i] = MultipartBody.Part.createFormData(String.format(Locale.ENGLISH, "document_avatar[%d]", i), files.get(i).getName(), requestBody);
        }


        HashMap<String, RequestBody> bodyParam = new HashMap<>();
        for (Map.Entry<String, String> obj : param.entrySet()) {
            bodyParam.put(obj.getKey(), RequestBody.create(MediaType.parse("multipart/form-data"), obj.getValue()));
        }

        Call<GenericBean> call = RetrofitClient.getRetrofitClient().uploadDocumentImages(bodyParam, body, Constants.KEY);
        call.enqueue(new Callback<GenericBean>() {
            @Override
            public void onResponse(Call<GenericBean> call, Response<GenericBean> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        Bundle bundle = new Bundle();
                        bundle.putString("message", response.body().getMessage());
//                        resultReceiver.onReceiveResult(200, bundle);
                        generateNotification("Documents files uploaded successfully.");
                        resultReceiver.send(200, bundle);

                    }
                }
            }

            @Override
            public void onFailure(Call<GenericBean> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    //This method is only generating push notification
    private void generateNotification(String messageBody) {

        try {
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            android.app.Notification notification = new NotificationCompat.Builder(this)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                    .setSmallIcon(getNotificationIcon())
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
                    .setDefaults(NotificationCompat.DEFAULT_ALL)/*
                    .setFullScreenIntent(pendingIntent, false)
                    .setContentIntent(pendingIntent)*/.build();

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0, notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // function to get the notification icon based on version lollipop or not
    private int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return whiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }

}
