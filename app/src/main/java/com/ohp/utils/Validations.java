package com.ohp.utils;

import android.text.TextUtils;
import android.util.Patterns;

import com.google.android.gms.location.places.Place;
import com.ohp.commonviews.beans.GetPropertyResponse;
import com.ohp.commonviews.beans.ThumbnailBean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class Validations {
    private static Validations instance = null;

    private Validations() {

    }

    public static Validations getInstance() {
        if (instance == null) return new Validations();
        else return instance;
    }

    public String validateAddPropertyFields(String propertyName, String propertyArea, String propertyAddress,
                                            String propertyDescription, String propertyConstructionYear,
                                            String pickerPath, Place place, boolean isImageSelected, String latitude,
                                            String longitude, String screenCheck) {

        /*if (screenCheck.equals(Constants.CLICK_ADD_PROPERTY) && pickerPath.trim().isEmpty()) {
            return "Please add property image";
        } else if ((screenCheck.equals(Constants.CLICK_VIEW_PROPERTY) || screenCheck.equals(Constants.CLICK_EDIT_PROPERTY))
                && isImageSelected && pickerPath.trim().isEmpty()) {
            return "Please add property image";
        } else */
        if (propertyName.trim().isEmpty()) {
            return "Please enter property name";
        } /*else if (propertyArea.trim().isEmpty()) {
            return "Please enter property area";
        } else if (propertyConstructionYear.trim().isEmpty()) {
            return "Please enter property construction year";
        }*/ else if (!propertyConstructionYear.trim().isEmpty() && (!validateYear(Integer.valueOf(propertyConstructionYear)))) {
            return "Please enter valid construction year";
        } else if (propertyAddress.trim().isEmpty()) {
            return "Please enter property address";
        } /*else if (propertyDescription.trim().isEmpty()) {
            return "Please enter property description";
        }*/ else if (!propertyArea.trim().isEmpty() && !validateArea(propertyArea)) {
            return "Please enter valid Sq. Ft.";
        } else if (place == null) {
            if (latitude.trim().isEmpty() && longitude.trim().isEmpty())
                return "Please choose address";
            else
                return "";
        } else
            return "";
    }

    public String validateAddProjectFields(String projectName, String propertyID, String Expertise, String jobType) {

        if (TextUtils.isEmpty(projectName)) {
            return "Please enter project name";
        } else if (TextUtils.isEmpty(propertyID)) {
            return "Please enter property ";
        } else if (TextUtils.isEmpty(Expertise)) {
            return "Please enter expertise";
        } else if (TextUtils.isEmpty(jobType)) {
            return "Please enter jobtype";
        } else
            return "";
    }

    public String validateAddTeamMember(String fullName, String email, String expertise, String address) {

        if (TextUtils.isEmpty(fullName)) {
            return "Please enter Team Member name";
        } else if (email.length() == 0) {
            return "Please enter email";
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return "Please enter valid email";
        } else if (TextUtils.isEmpty(expertise)) {
            return "Please enter expertise";
        } else if (TextUtils.isEmpty(address)) {
            return "Please enter Address";
        } else
            return "";
    }

    public String validateAddProjectFieldsForSp(String projectName, String Expertise, String jobType) {

        if (TextUtils.isEmpty(projectName)) {
            return "Please enter project name";
        } else if (TextUtils.isEmpty(Expertise)) {
            return "Please enter expertise";
        } else if (TextUtils.isEmpty(jobType)) {
            return "Please enter jobtype";
        } else
            return "";
    }


    public String addEditAppliance(GetPropertyResponse propertyObj, String applianceName, String applianceBrand,
                                   String applianceType, String purchaseDate, String warrantyDate, String extendedWarrantyDate,
                                   List<ThumbnailBean> uploadImageList, List<ThumbnailBean> uploadFileList, String modelName, String serialNumber) {
        if (propertyObj == null) {
            return "Couldn't get property list. Kindly refresh screen.";
        } else if (applianceName.isEmpty()) {
            return "Please enter appliance name";
        } else if (modelName.isEmpty()) {
            return "Please enter model name";
        } else if (serialNumber.isEmpty()) {
            return "Please enter serial number";
        } /*else if (applianceBrand.isEmpty()) {
            return "Please enter brand name";
        } else if (applianceType.isEmpty()) {
            return "Please enter appliance type";
        } else if (purchaseDate.isEmpty()) {
            return "Please enter purchase date";
        } else if (warrantyDate.isEmpty()) {
            return "Please enter warranty expiration date";
        } else if (extendedWarrantyDate.isEmpty()) {
            return "Please enter extended warranty date";
        }/* else if (uploadImageList == null || uploadImageList.size() == 1) {
            return "Please add at least one appliance pic";
        } else if (uploadFileList == null || uploadFileList.size() == 1) {
            return "Please add at least one appliance doc";
        } */ else if ((!purchaseDate.isEmpty() && !warrantyDate.isEmpty())
                && !isPurchaseDateValid(purchaseDate, warrantyDate)) {
            return "Purchase date should be less than warranty expiration date";
        } else {
            return "";
        }
    }

    private boolean validateYear(int selectedYear) {
        if (selectedYear < 1900)
            return false;
        else {
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);
            System.out.println(currentYear + "<---Validations.validateYear-->" + selectedYear);
            return selectedYear <= currentYear;
        }

    }


    private boolean isPurchaseDateValid(String purchaseDate, String warrantyDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date purchase, warranty;
        try {
            purchase = sdf.parse(purchaseDate);
            warranty = sdf.parse(warrantyDate);
            if (purchase.getTime() < warranty.getTime()) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    public String validatePasswordFields(String oldPassword, String password, String ConfirmPassword) {
        if (TextUtils.isEmpty(password)) {
            return "Please Enter Your Password";
        } else if (password.length() < 8 || !validatePassword(password)) {
            return "Your password must be 8 characters including minimum one upper case letter, one numeral and one special symbol.";
        } else if (TextUtils.isEmpty(ConfirmPassword)) {
            return "Please Enter New Password";
        } else if (!password.equals(ConfirmPassword)) {
            return "New Password and Confirm Password Doesn't Match";
        } else if (oldPassword.equals(password)) {
            return "New Password can not be same as the Old Password.";
        }
        return "";
    }

    public String validateSignUpFields(String email, String password, String confirmPwd, String role, boolean checked) {
        if (email.length() == 0 || password.length() == 0 || confirmPwd.length() == 0) {
            return "Please enter all fields";
        } else if (email.length() == 0) {
            return "Please enter email";
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return "Please enter valid email";
        } else if (password.length() == 0) {
            return "Please enter password";
        } else if (password.length() < 8 || !validatePassword(password)) {
            return "Your password must be 8 characters including minimum one upper case letter, one numeral and one special symbol.";
        } else if (confirmPwd.length() == 0) {
            return "Please enter confirm-password";
        } else if (!password.equals(confirmPwd)) {
            return "Password doesn't match";
        } else if (TextUtils.isEmpty(role)) {
            return "Error occurred while getting user type";
        } else if (!checked) {
            return "Please accept terms and conditions";
        } else {
            return "";
        }
    }

    public String validateResetPasswordFields(String newPassword, String confirmPassword) {
        if (newPassword.length() == 0 || confirmPassword.length() == 0) {
            return "Please enter all fields";
        } else if (newPassword.length() < 8 || !validatePassword(newPassword)) {
            return "Your password must be 8 characters including minimum one upper case letter, one numeral and one special symbol.";
        } else if (!newPassword.equals(confirmPassword)) {
            return "Password doesn't match";
        } else {
            return "";
        }
    }

    public String validateEventFields(String eventTitle, String eventTimeFrom, String eventTimeTo, String reminderDateTime) {
        if (eventTitle.isEmpty()) {
            return "Please enter event title.";
        } else if (eventTimeFrom.isEmpty()) {
            return "Please enter event from date & time.";
        } /*else if (eventTimeTo.isEmpty()) {
            return "Please enter event to date & time.";
        } */ else if (!eventTimeTo.isEmpty()
                && !isValidDate(eventTimeTo, eventTimeFrom, "from")) {
            return "Event from date should be equal to event to date and Event from time should be less than event to time.";
        } else if (!eventTimeTo.isEmpty() && !eventTimeFrom.isEmpty()
                && !isValidDate(eventTimeFrom, eventTimeTo, "to")) {
            return "Event from date should be equal to event to date and Event to time should be greater than event from time.";
        } else if (!eventTimeTo.isEmpty() && !reminderDateTime.isEmpty() && !isValidDate(eventTimeTo, reminderDateTime, "reminder")) {
            return "Event reminder date & time should be less than event from date & time.";
        } else if (!reminderDateTime.isEmpty() && !isValidDate(eventTimeFrom, reminderDateTime, "reminder")) {
            return "Event reminder date & time should be less than event from date & time.";
        }
        return "";
    }

    public String validateTaskFields(String taskTitle, String taskTimeFrom, String taskTimeTo, String reminderDateTime) {
        if (taskTitle.isEmpty()) {
            return "Please enter task title.";
        } else if (taskTimeFrom.isEmpty()) {
            return "Please enter task from date & time.";
        } /*else if (eventTimeTo.isEmpty()) {
            return "Please enter task to date & time.";
        } */ else if (!taskTimeTo.isEmpty()
                && !isValidDate(taskTimeTo, taskTimeFrom, "from")) {
            return "Task from date should be equal to task to date and Task from time should be less than task to time.";
        } else if (!taskTimeTo.isEmpty() && !taskTimeFrom.isEmpty()
                && !isValidDate(taskTimeFrom, taskTimeTo, "to")) {
            return "Task from date should be equal to task to date and Task to time should be greater than task from time.";
        } else if (!reminderDateTime.isEmpty() && !isValidDate(taskTimeFrom, reminderDateTime, "reminder")) {
            return "Task reminder date & time should be less than task from date & time.";
        }
        return "";
    }


    private boolean isValidDate(String date1, String date2, String check) {
        Calendar timeFrom, timeTo;
        try {
            String[] splitDate1, splitDate2;
            String[] arrayDate1, arrayDate2, arrayTime1, arrayTime2;
            timeFrom = Calendar.getInstance();
            timeTo = Calendar.getInstance();


            splitDate1 = date1.split(" ");
            splitDate2 = date2.split(" ");

            System.out.println("splitDate1 = " + splitDate1[0]);
            System.out.println("splitDate2 = " + splitDate2[0]);


            arrayDate1 = splitDate1[0].split("-");
            arrayDate2 = splitDate2[0].split("-");

            arrayTime1 = splitDate1[1].split(":");
            arrayTime2 = splitDate2[1].split(":");


            switch (check) {
                case "from":
                    timeFrom.set(Integer.parseInt(arrayDate1[0]), Integer.parseInt(arrayDate1[1]), Integer.parseInt(arrayDate1[2]),
                            Integer.parseInt(arrayTime1[0]), Integer.parseInt(arrayTime1[1]), Integer.parseInt(arrayTime1[2]));

                    timeTo.set(Integer.parseInt(arrayDate2[0]), Integer.parseInt(arrayDate2[1]), Integer.parseInt(arrayDate2[2]),
                            Integer.parseInt(arrayTime2[0]), Integer.parseInt(arrayTime2[1]), Integer.parseInt(arrayTime2[2]));
                    return !timeTo.getTime().equals(timeFrom.getTime()) && timeTo.getTime().before(timeFrom.getTime());
                case "to":
                    timeFrom.set(Integer.parseInt(arrayDate1[0]), Integer.parseInt(arrayDate1[1]), Integer.parseInt(arrayDate1[2]));
                    timeTo.set(Integer.parseInt(arrayDate2[0]), Integer.parseInt(arrayDate2[1]), Integer.parseInt(arrayDate2[2]));

                    return timeTo.getTime().equals(timeFrom.getTime());
                case "reminder":
                    timeFrom.set(Integer.parseInt(arrayDate1[0]), Integer.parseInt(arrayDate1[1]), Integer.parseInt(arrayDate1[2]),
                            Integer.parseInt(arrayTime1[0]), Integer.parseInt(arrayTime1[1]), Integer.parseInt(arrayTime1[2]));

                    timeTo.set(Integer.parseInt(arrayDate2[0]), Integer.parseInt(arrayDate2[1]), Integer.parseInt(arrayDate2[2]),
                            Integer.parseInt(arrayTime2[0]), Integer.parseInt(arrayTime2[1]), Integer.parseInt(arrayTime2[2]));
                    return !timeTo.getTime().equals(timeFrom.getTime()) && timeTo.getTime().before(timeFrom.getTime()) && !timeTo.getTime().after(timeFrom.getTime());
            }


        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return false;

    }

    private boolean validatePassword(String password) {
        boolean hasUppercase = !password.equals(password.toLowerCase());
        return password.matches(".*\\d+.*") && password.matches(".*[!&^%$#@()/.]+.*") && hasUppercase;
    }

    private boolean validateArea(String area) {
        if (area.equals(".")) {
            return false;
        } else if (area.startsWith(".")) {
            return false;
        } else if (area.lastIndexOf(".") == area.length() - 1) {
            return false;
        } else if (area.contains(".") && Float.parseFloat(area) == 0.0) {
            return false;
        } else if (!area.contains(".") && Integer.parseInt(area) == 0) {
            return false;
        }
        return true;
    }

    public String validateContactFields(String name, String email, String contactNumber) {

        if (TextUtils.isEmpty(name)) {
            return "Please Enter Name";
        }/* else if (email.length() == 0) {
            return "Please Enter Email";
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return "Please enter valid email";
        } else if (TextUtils.isEmpty(contactNumber)) {
            return "Please Enter Contact Number";
        }*/
        return "";
    }
}
