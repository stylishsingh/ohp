package com.ohp.utils;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.library.mvp.FragmentPresenter;
import com.ohp.ownerviews.presenter.AddEditPropertyFragmentPresenterImpl;
import com.ohp.ownerviews.presenter.OwnerAddEditEventFragmentPresenterImpl;
import com.ohp.serviceproviderviews.presenter.AddEditTaskSpPresenterImpl;
import com.ohp.serviceproviderviews.presenter.SpAddEditEventFragmentPresenterImpl;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Amanpal Singh.
 */

public class CalendarPicker {

    private static CalendarPicker calendarInstance = null;

    private CalendarPicker() {

    }

    public static CalendarPicker getInstance() {
        if (calendarInstance == null)
            return new CalendarPicker();
        else
            return calendarInstance;
    }


    public void showCalendar(final Context context, final FragmentPresenter presenter, final int viewId, String dateTime) {
        final Calendar calendar = Calendar.getInstance();

        System.out.println("CalendarPicker.showCalendar---" + dateTime);

        int year, month, day;
        final String[] parseDateTime, parseDate, parseTime;

        DatePickerDialog datepickerDialog = null;
        try {
            if (dateTime.isEmpty()) {
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                day = calendar.get(Calendar.DAY_OF_MONTH);
                parseDate = null;
                parseTime = null;
            } else {
                parseDateTime = dateTime.split(" ");
                parseDate = parseDateTime[0].split("-");
                parseTime = parseDateTime[1].split(":");
                calendar.set(Integer.parseInt(parseDate[0]), Integer.parseInt(parseDate[1]) - 1, Integer.parseInt(parseDate[2]));
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                day = calendar.get(Calendar.DAY_OF_MONTH);

            }

            datepickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    if (presenter instanceof AddEditPropertyFragmentPresenterImpl) {
                        AddEditPropertyFragmentPresenterImpl presenterObj = (AddEditPropertyFragmentPresenterImpl) presenter;
                        String date = String.valueOf(year) + "-" + String.valueOf(month)
                                + "-" + String.valueOf(dayOfMonth);
                        System.out.println("CalendarPicker.onDateSet--->" + date);
                        presenterObj.setSelectedDate(date);

                    } else if (presenter instanceof OwnerAddEditEventFragmentPresenterImpl) {
                        String date = String.valueOf(year) + "-" + String.valueOf(month + 1)
                                + "-" + String.valueOf(dayOfMonth);
                        System.out.println("CalendarPicker.onDateSet--->" + date);
                        Calendar selectedDate = Calendar.getInstance();
                        boolean isCurrentDate = false;


//                        if (view.getId() == R.id.et_date_time_to) {
                        selectedDate.set(year, month, dayOfMonth);
                        isCurrentDate = selectedDate.getTime().after(new Date());
//                        }
                        TimePicker(context, presenter, date, viewId, parseTime, isCurrentDate);
                    } else if (presenter instanceof SpAddEditEventFragmentPresenterImpl) {
                        String date = String.valueOf(year) + "-" + String.valueOf(month + 1)
                                + "-" + String.valueOf(dayOfMonth);
                        System.out.println("CalendarPicker.onDateSet--->" + date);
                        Calendar selectedDate = Calendar.getInstance();
                        boolean isCurrentDate = false;
                        selectedDate.set(year, month, dayOfMonth);
                        isCurrentDate = selectedDate.getTime().after(new Date());
                        TimePicker(context, presenter, date, viewId, parseTime, isCurrentDate);
                    } else if (presenter instanceof AddEditTaskSpPresenterImpl) {
                        String date = String.valueOf(year) + "-" + String.valueOf(month + 1)
                                + "-" + String.valueOf(dayOfMonth);
                        System.out.println("CalendarPicker.onDateSet--->" + date);
                        Calendar selectedDate = Calendar.getInstance();
                        boolean isCurrentDate = false;


//                        if (view.getId() == R.id.et_date_time_to) {
                        selectedDate.set(year, month, dayOfMonth);
                        isCurrentDate = selectedDate.getTime().after(new Date());
//                        }
                        TimePicker(context, presenter, date, viewId, parseTime, isCurrentDate);
                    }
                }
            }, year, month, day);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        if (datepickerDialog != null) {
            datepickerDialog.getDatePicker().setMinDate(new Date().getTime());
            datepickerDialog.show();
        }
    }

    private void TimePicker(final Context context, final FragmentPresenter presenter, final String date, final int viewId, String parseTime[], final boolean isCurrentDate) {
        TimePickerDialog timePickerDialog = null;
        try {
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            if (parseTime != null) {
                hour = Integer.parseInt(parseTime[0]);
                minute = Integer.parseInt(parseTime[1]);
            }


            timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    if (presenter instanceof OwnerAddEditEventFragmentPresenterImpl) {

                        System.out.println(hourOfDay + "---c.get(Calendar.HOUR_OF_DAY) = " + c.get(Calendar.HOUR_OF_DAY));
                        System.out.println(minute + "---c.get(Calendar.MINUTE) = " + c.get(Calendar.MINUTE));
                        System.out.println("isCurrentDate = " + isCurrentDate);


                        if (isCurrentDate) {
                            OwnerAddEditEventFragmentPresenterImpl presenterObj = (OwnerAddEditEventFragmentPresenterImpl) presenter;
                            presenterObj.setEventReminderDateTime(date + " " + getTime(hourOfDay, minute), viewId);
                        } else {

                            if (convertHourTo24(hourOfDay) < c.get(Calendar.HOUR_OF_DAY)) {
                                Toast.makeText(context, "select valid hour", Toast.LENGTH_SHORT).show();
                            } else if (convertHourTo24(hourOfDay) == c.get(Calendar.HOUR_OF_DAY) && minute <= c.get(Calendar.MINUTE)) {
                                Toast.makeText(context, "select valid minute", Toast.LENGTH_SHORT).show();
                            } else {
                                OwnerAddEditEventFragmentPresenterImpl presenterObj = (OwnerAddEditEventFragmentPresenterImpl) presenter;
                                presenterObj.setEventReminderDateTime(date + " " + getTime(hourOfDay, minute), viewId);
                            }
                        }
                    } else if (presenter instanceof SpAddEditEventFragmentPresenterImpl) {

                        System.out.println(hourOfDay + "---c.get(Calendar.HOUR_OF_DAY) = " + c.get(Calendar.HOUR_OF_DAY));
                        System.out.println(minute + "---c.get(Calendar.MINUTE) = " + c.get(Calendar.MINUTE));
                        System.out.println("isCurrentDate = " + isCurrentDate);


                        if (isCurrentDate) {
                            SpAddEditEventFragmentPresenterImpl presenterObj = (SpAddEditEventFragmentPresenterImpl) presenter;
                            presenterObj.setEventReminderDateTime(date + " " + getTime(hourOfDay, minute), viewId);
                        } else {

                            if (convertHourTo24(hourOfDay) < c.get(Calendar.HOUR_OF_DAY)) {
                                Toast.makeText(context, "select valid hour", Toast.LENGTH_SHORT).show();
                            } else if (convertHourTo24(hourOfDay) == c.get(Calendar.HOUR_OF_DAY) && minute <= c.get(Calendar.MINUTE)) {
                                Toast.makeText(context, "select valid minute", Toast.LENGTH_SHORT).show();
                            } else {
                                SpAddEditEventFragmentPresenterImpl presenterObj = (SpAddEditEventFragmentPresenterImpl) presenter;
                                presenterObj.setEventReminderDateTime(date + " " + getTime(hourOfDay, minute), viewId);
                            }
                        }
                    } else if (presenter instanceof AddEditTaskSpPresenterImpl) {
                        if (isCurrentDate) {
                            AddEditTaskSpPresenterImpl presenterObj = (AddEditTaskSpPresenterImpl) presenter;
                            presenterObj.setEventReminderDateTime(date + " " + getTime(hourOfDay, minute), viewId);
                        } else {

                            if (convertHourTo24(hourOfDay) < c.get(Calendar.HOUR_OF_DAY)) {
                                Toast.makeText(context, "select valid hour", Toast.LENGTH_SHORT).show();
                            } else if (convertHourTo24(hourOfDay) == c.get(Calendar.HOUR_OF_DAY) && minute <= c.get(Calendar.MINUTE)) {
                                Toast.makeText(context, "select valid minute", Toast.LENGTH_SHORT).show();
                            } else {
                                AddEditTaskSpPresenterImpl presenterObj = (AddEditTaskSpPresenterImpl) presenter;
                                presenterObj.setEventReminderDateTime(date + " " + getTime(hourOfDay, minute), viewId);
                            }
                        }
                    }
                }

            }, hour, minute, true);


        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (timePickerDialog != null)
            timePickerDialog.show();
    }

    private String getTime(int hourOfDay, int minute) {

        if (hourOfDay < 10 && minute < 10)
            return "0" + String.valueOf(hourOfDay) + ":0" + String.valueOf(minute)
                    + ":00";
        else if (hourOfDay < 10)
            return "0" + String.valueOf(hourOfDay) + ":" + String.valueOf(minute)
                    + ":00";
        else if (minute < 10)
            return String.valueOf(hourOfDay) + ":0" + String.valueOf(minute)
                    + ":00";
        else
            return String.valueOf(hourOfDay) + ":" + String.valueOf(minute)
                    + ":00";
    }

    private int convertHourTo24(int hourOfDay) {

        switch (hourOfDay) {
            case 0:
                return 24;
            default:
                return hourOfDay;
        }
    }

    private int convertMinuteTo24(int minute) {

        switch (minute) {
            case 0:
                return 60;
            default:
                return minute;
        }
    }

}


