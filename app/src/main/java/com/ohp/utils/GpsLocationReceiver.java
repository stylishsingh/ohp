package com.ohp.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;

/**
 * @author Amanpal Singh.
 */

public class GpsLocationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            Intent intentGps = new Intent("gpsDetectionIntent");
            intentGps.putExtra("isGpsEnabled", isGpsEnabled(context));
            context.sendBroadcast(intentGps);

            Intent intentGpsProjects = new Intent(Constants.BROADCAST_FOR_NEAR_PROJECTS);
            intentGpsProjects.putExtra("isGpsEnabled", isGpsEnabled(context));
            context.sendBroadcast(intentGpsProjects);

        }
    }

    private boolean isGpsEnabled(Context context) {


        boolean gpsEnabled = false;
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try {
            gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return gpsEnabled;
    }
}