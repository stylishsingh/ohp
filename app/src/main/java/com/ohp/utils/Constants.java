package com.ohp.utils;

/**
 * @author Amanpal Singh.
 */

public interface Constants {
    /*
    API BASE URL
     */
    int STATUS_200 = 200;
    int STATUS_201 = 201;
    int STATUS_202 = 202;
    int STATUS_203 = 203;


    String LIVE_APIURL = "https://onehomeportal.com/";
    String URL_PRIVACY_POLICY = "https://onehomeportal.com/privacy.html";
    String URL_TERMS_AND_CONDITIONS = "https://onehomeportal.com/terms.html";
    String URL_ABOUT_US = "https://onehomeportal.com/disclaimer.html";
    String KEY = "9058b1987d13ee8e6669566aba633a08";
    //    String DEV_APIURL = "http://onehomeportalqa.mobilytedev.com/";
//    String DEV_APIURL = "http://ohpstaging.mobilytedev.com/";
    String DEV_APIURL = LIVE_APIURL;
//            String DEV_APIURL = "http://onehomeportal.mobilytedev.com/";


    String API_URL = DEV_APIURL;
    String URL = "URL";

    String DEF_PROFILE_URL = "https://organicthemes.com/demo/profile/files/2012/12/profile_img.png";

    /**********NOTIFICATIONS PARAMS**********/

    String NOTIFICATIONS = "notifications";
    String BG_APP = "background_app";
    String NOTIFY_ID = "notifyId";
    String NOTIFY_NEW_PROJECT_ADDED = "newProjectAdded";
    String NOTIFY_BID_ACCEPTED = "bidAccepted";
    String NOTIFY_BID_REJECTED = "bidRejected";
    String NOTIFY_TEAM_MEMBER_INVITED = "teamMemberInvited";
    String NOTIFY_EVENT_TO_OWNER = "eventToOwner";
    String NOTIFY_EVENT_TO_PROVIDER = "eventToProvider";
    String NOTIFY_TASK_TO_OWNER = "taskToOwner";
    String NOTIFY_TASK_TO_PROVIDER = "taskToMember";
    String NOTIFY_BID_ADDED = "bidAdded";
    String NOTIFY_EVENT_REMINDER_TO_USER = "eventReminderToUser";
    String NOTIFY_EVENT_REMINDER_TO_OWNER = "eventReminderToOwner";
    String NOTIFY_EVENT_REMINDER_TO_PROVIDER = "eventReminderToProvider";
    String NOTIFY_TASK_REMINDER_TO_USER = "taskReminderToUser";
    String NOTIFY_TASK_REMINDER_TO_OWNER = "taskReminderToOwner";
    String NOTIFY_TASK_REMINDER_TO_MEMBER = "taskReminderToMember";
    String NOTIFY_DEACTIVATED_BY_ADMIN = "deactivatedByAdmin";
    String NOTIFY_ACTIVATED_BY_ADMIN = "activatedByAdmin";
    String NOTIFY_NEW_MESSAGE = "newMessage";
    String NOTIFY_END_PROJECT = "endProject";
    String NOTIFY_CANCEL_PROJECT = "cancelProject";
    String NOTIFY_MARK_PROJECT = "markProject";
    String NOTIFY_REJECT_COMPLETED_PROJECT = "rejectCompletedProject";
    String NOTIFY_MARK_AS_COMPLETE_TASK_USER = "markAsCompleteTaskUser";
    String NOTIFY_REOPEN_TASK_TO_USER = "reopenTaskToUser";
    String NOTIFY_REOPEN_TASK_TO_MEMBER = "reopenTaskToMember";
    String NOTIFY_COMPLETE_TASK_TO_OWNER = "completeTaskToOwner";
    String NOTIFY_COMPLETE_TASK_TO_USER = "completeTaskToUser";
    String NOTIFY_COMPLETE_TASK_TO_MEMBER = "completeTaskToMember";


    String NOTIFY_APPLIANCE_EXPIRE_NOTIFICATION = "applianceExpireNotification";
    String NOTIFY_APPLIANCE_EXTENDED_NOTIFICATION = "applianceExtendedNotification";

    /**********API URL**********/

    String LOGIN_API = "secureapi/login";
    String UPDATE_ROLE_API = "secureapi/update-role";
    String REGISTER_API = "secureapi/signup";
    String SEND_VERIFICATION_CODE = "secureapi/resend-otp";
    String CONFIRM_VERIFICATION_CODE = "secureapi/verify-otp";
    String RESET_PWD = "secureapi/reset-password";
    String ADD_EDIT_TASK_API = "secureapi/add-edit-task";
    String GET_USER_PROFILE = "secureapi/get-user-profile";
    String SAVE_USER_PROFILE = "secureapi/update-user-profile";

    //Property Related API
    String GET_RESOURCES = "secureapi/get-resources";
    String GET_PROPERTIES_LIST = "secureapi/get-properties";
    String GET_APPLIANCE_LIST = "secureapi/get-appliances";
    String ADD_EDIT_PROPERTY = "secureapi/add-edit-property";
    String GET_TEAM_MEMBER = "secureapi/get-team-members";
    String ADD_TEAM_MEMBER = " secureapi/add-team-member";
    String ADD_EDIT_TEAM_MEMBER = "secureapi/edit-team-member";
    String DELETE_PROPERTY = "secureapi/delete-property";
    String GET_PROPERTY_DETAIL = "secureapi/get-property-detail";
    String GET_PROPERTY = "secureapi/get-property-listing";
    String GET_PROJECTS = "secureapi/get-projects";
    String GET_CONTACT_LIST = "secureapi/get-user-contacts";
    String CHANGE_PASSWORD = "secureapi/change-password";
    String GET_PROJECTS_SP = "get_projects_sp";
    String GET_PROJECTS_SP_MYPROJECTS = "get_projects_sp_myprojects";
    String TASK_LIST_UPDATE = "task_list_update";
    String CONTACT_LIST_UPDATE = "contact_list_update";
    String GET_APPLIANCE = "secureapi/get-appliances-listing";
    String GET_TASK_LIST = "secureapi/get-task-listing";
    String GET_CHAT_USERS = "secureapi/get-chat-users-list";
    String GET_MESSAGE_LIST = "secureapi/get-messages";
    String SEND_MESSAGE = "secureapi/send-message";
    String GET_PROJECTS_LISTING = "secureapi/get-project-listing";
    String GET_TASK_DETAIL = "secureapi/get-task-detail";
    String ADD_EDIT_APPLIANCE = "secureapi/add-edit-appliance";
    String UPLOAD_APPLIANCE_IMAGE = "secureapi/upload-appliance-image";
    String UPLOAD_DOCUMENT_IMAGE = "secureapi/upload-document-image";
    String GET_APPLIANCE_DETAIL = "secureapi/get-appliance-detail";
    String DELETE_APPLIANCE = "secureapi/delete-appliance";
    String GET_DOCUMENTS_LIST = "secureapi/get-documents";
    String GET_DOCUMENTS_DETAIL = "secureapi/get-document-detail";
    String DELETE_DOCUMENTS = "secureapi/delete-document";
    String DELETE_PROJECTS = "secureapi/delete-project";
    String DELETE_TASK = "secureapi/delete-task";
    String DELETE_CONTACT = "secureapi/delete-user-contact";
    String DELETE_BID = "secureapi/delete-bid";
    String ADD_EDIT_PROJECT = "secureapi/add-edit-project";
    String ADD_EDIT_CONTACT = "secureapi/add-edit-user-contact";
    String UPLOAD_PROJECT_IMAGE = "secureapi/upload-project-images";
    String UPLOAD_TASK_IMAGE = "secureapi/upload-task-document";
    String ADD_EDIT_DOCUMENT = "secureapi/add-edit-document";
    String GET_EVENT_DETAILS = "secureapi/get-event-detail";
    String GET_EVENTS = "secureapi/get-events";
    String ADD_EDIT_EVENTS = "secureapi/add-edit-event";
    String DELETE_EVENT = "secureapi/delete-event";
    String PROJECT_DETAIL = "secureapi/get-project-detail";
    String GET_NEARES_PROJECTS = "secureapi/get-nearest-projects";
    String ADD_EDIT_BID = "secureapi/add-edit-bid";
    String MY_BIDS = "secureapi/my-bids";
    String ADD_EDIT_PROJECT_SP = "secureapi/add-edit-project-for-service-provider";
    String GET_PROVIDER_LIST = "secureapi/get-providers-list";

    String GET_SP_PROFILE = "secureapi/get-service-provider-profile";
    String SAVE_SP_PROFILE = "secureapi/update-service-provider-profile";
    String BROWSE_SERVICE_PROVIDER = "secureapi/browse-service-provider";
    String BID_ACCEPT_REJECT = "secureapi/bid-accept-reject";
    String GET_TEAM_MEMBERS = "secureapi/team-list";
    String DELETE_TEAM_MEMBER = "secureapi/delete-team-member";
    String END_PROJECT = "secureapi/end-project";
    String CANCEL_PROJECT = "secureapi/cancel-project";
    String GET_PROJECT_BIDS_PROVIDERS = "secureapi/get-project-bids-providers";
    String MARK_NOT_COMPLETED_PROJECT = "secureapi/won-status-project";
    String MARK_COMPLETED_PROJECT = "secureapi/mark-status-project";
    String MARK_COMPLETED_TASK = "secureapi/mark-complete-task";
    String GET_USER_REVIEWS = "secureapi/get-user-reviews";
    String REOPEN_TASK = "secureapi/reopen-task";
    String COMPLETE_TASK = "secureapi/complete-task";

    /********APIs Params*******/
    String ACCESS_TOKEN = "access_token";
    String SORT_ORDER = "sort_order";
    String SEARCH_PARAM = "search_param";
    String PAGE_PARAM = "page";
    String PROPERTY_NAME = "property_name";
    String PROPERTY_ID = "property_id";
    String MEMBER_ID = "member_id";
    String USER_ID = "user_id";
    String SEND_TO = "send_to";
    String USER_NAME = "user_name";
    String SEND_BY_NAME = "send_by_name";
    String ROLE = "role";
    String ADDRESS = "address";
    String EXPERTISE = "expertise";
    String LATITUDE = "latitude";
    String LONGITUDE = "longitude";
    String AREA = "area";
    String CONSTRUCTION_YEAR = "construction_year";
    String DESCRIPTION = "description";
    String TYPE = "type";
    String JOB_TYPE_ID = "job_type";
    String APPLIANCE_NAME = "appliance_name";
    String APPLIANCE_BRAND = "appliance_brand";
    String PURCHASE_DATE = "purchase_date";
    String WARRANTY_EXPIRATION_DATE = "warranty_expiration_date";
    String MODEL_NAME = "appliance_model";
    String SERIAL_NUMBER = "appliance_serial_no";
    String EXTENDED_WARRANTY_DATE = "extended_warranty_date";
    String APPLIANCE_AVATAR = "appliance_avatar";
    String APPLIANCE_ID = "appliance_id";
    String APPLIANCE_TYPE = "appliance_type";
    String APPLIANCE_OBJ = "appliance_obj";
    String DOCUMENT_NAME = "document_name";
    String CONTACT_LIST_OBJECT = "list_object";
    String MESSAGE_OBJECT = "mesage_object";
    String DOCUMENT_ID = "document_id";
    String IS_DELETED = "is_deleted";
    String IS_DOC_DELETED = "is_doc_deleted";
    String EVENT_NAME = "event_name";
    String PROJECT_NAME = "project_name";
    String PROJECT_ID = "project_id";
    String PROVIDER_ID = "provider_id";
    String TYPE_STATUS = "type_status";
    String EVENT_NOTES = "event_notes";
    String EVENT_ID = "event_id";
    String EVENT_STATUS = "event_status";
    String EVENT_DATE = "event_date";
    String EVENT_TYPE = "event_type";
    String EVENT_TIME_FROM = "event_time_from";
    String EVENT_TIME_TO = "event_time_to";
    String EVENT_REMINDER = "event_reminder";
    String EVENT_REPEAT = "event_repeat";
    String APP_TIME_ZONE = "app_time_zone";
    String CHANGE_IN_SERIES = "change_in_series";
    String PROJECT_DESCRIPTION = "project_description";
    String PROPERTY_ADDRESS = "property_address";
    String PROPERTY_Name = "property_name";
    String EXPERTISE_ID = "expertise";
    String JOB_TYPE = "job_type";
    String PROJECT_TYPE = "project_type";
    String PROJECT_TYPE_COMPLETED = "completed";
    String PROJECT_TYPE_WON = "won";
    String STATUS = "status";
    String PROJECT_TYPE_MY_PROJECT = "my";
    String UPDATE_MESSAGE_LIST = "update_message_list";

    /************Screen Click *************/
    String CLICK_VIEW_PROPERTY = "view_property";
    String CLICK_EDIT_PROPERTY = "edit_property";
    String CLICK_ADD_PROPERTY = "add_property";

    String CLICK_VIEW_APPLIANCE = "view_appliance";
    String CLICK_EDIT_APPLIANCE = "edit_appliance";
    String CLICK_ADD_APPLIANCE = "add_appliance";
    String CLICK_APPLIANCE = "appliance";
    String BROADCAST_FOR_UPCOMING = "all_upcoming";
    String BROADCAST_FOR_BIDS = "all_bids";
    String BROADCAST_FOR_NEAR_PROJECTS = "near_projects";

    String CLICK_VIEW_DOCUMENT = "view_document";
    String CLICK_EDIT_DOCUMENT = "edit_document";
    String CLICK_ADD_DOCUMENT = "add_document";
    String CLICK_DOCUMENT = "document";

    String CLICK_OWNER_VIEW_EVENT = "owner_view_event";
    String CLICK_OWNER_EDIT_EVENT = "owner_edit_event";
    String CLICK_OWNER_ADD_EVENT = "owner_add_event";

    String CLICK_SP_VIEW_EVENT = "sp_view_event";
    String CLICK_SP_EDIT_EVENT = "sp_edit_event";
    String CLICK_SP_ADD_EVENT = "sp_add_event";

    String CLICK_VIEW_TEAM = "view_team";
    String CLICK_EDIT_TEAM = "edit_team";
    String CLICK_ADD_TEAM = "add_team";

    String CLICK_VIEW_PROVIDER = "view_provider";
    String CLICK_VIEW_REVIEWS = "view_reviews";

    String DESTINATION = "destination";
    String PROJECT_STATUS = "project_status";
    String RECEIVER = "receiver";
    String SCREEN = "screen";
    String IMAGE_LIST = "image_list";
    String FILE_LIST = "file_list";
    String NAVIGATE_FROM = "from";

    String CLICK_ADD_PROJECT = "add_project";
    String CLICK_EDIT_PROJECT = "edit_project";
    String CLICK_VIEW_PROJECT = "view_project";
    String CLICK_VIEW_UPCOMING_PROJECT = "all_upcoming_project";
    String CLICK_VIEW_ONGOING_PROJECT = "all_ongoing_project";
    String CLICK_VIEW__PAST_PROJECT = "all_completed_project";
    String CLICK_ALL_PROJECTS = "all_projects_details_particular_property";
    String CLICK_ALL_WON_PROJECTS_SERVICEPROVIDER = "all_won_projects";
    String CLICK_ALL_MY_PROJECTS_SERVICEPROVIDER = "all_my_projects_service_provider";
    String CLICK_ALL_COMPLETED_PROJECTS_SERVICEPROVIDER = "all_completed_projects_service_provider";
    String CLICK_ADD_EDIT_PROJECT_SERVICE_PROVIDER = "add_edit_project_service_provider";
    String CLICK_EDIT_BID = "edit_bid";
    String CLICK_VIEW_BID = "view_bid";
    String CLICK_CHAT = "click_chat";
    String BID_ID = "bid_id";
    String BROWSE_PROJECTS = "browse_projects";
    String DOCUMENT_LIST_SCREEN = "document_list_request";
    String EVENT_LIST_SCREEN = "event_list_screen";
    String ADD_EDIT_DOCUMENT_SCREEN = "add_edit_document_screen";
    String ADD_EDIT_APPLIANCE_SCREEN = "add_edit_appliance_screen";
    String APPLIANCE_LIST_SCREEN = "appliance_list_screen";
    String ADD_PROJECT_SCREEN = "add_project_screen";
    String ADD_EDIT_EVENT_SCREEN = "add_edit_event_screen";
    String NAVIGATE_SCREEN = "NAVIGATE_SCREEN";
    String PROJECT_DETAIL_SERVICE_PROVIDER = "project_detail_service_provider";
    String TASK_DETAIL_SERVICE_PROVIDER = "task_detail";
    String Task_EDIT_SERVICE_PROVIDER = "task_edit";
    String MY_PROJECTS_SERVICE_PROVIDER_View = "my_project_service_provider_view";
    String MY_PROJECTS_SERVICE_PROVIDER_EDIT = "my_project_service_provider_edit";
    String PROJECT_DETAIL_WON_PROJECTS = "project_detail_won_projects";
    String BID_Amount = "bid_amount";
    String BID_NOTE = "bid_note";

    String RESOURCE_APPLIANCE = "appliance";
    String RESOURCE_BRAND = "brand";
    String OCCURRENCE = "event_occurrence";
    String EVENT_REPEAT_EMPTY = "event_repeat_empty";
    String FULL_NAME = "full_name";
    String EMAIL = "email";
    String GENDER = "gender";
    String LICENSED = "is_licensed";
    String INSURED = "insured";
    String BONDED = "bonded";
    String BUSINESS_NAME = "business_name";
    String LICENSE_NO = "license_no";
    String WEBSITE = "website";
    String PROVIDER_LIST = "provider_list";
    String WON_PROVIDER = "won_provider";
    String TEAM_MEMBER_OBJ = "team_member_obj";
    String POSITION = "position";
    String DISTANCE = "distance";
    String RATING = "rating";
    String LICENSE = "license";
    String MESSAGE = "message";
    String BROWSE_PIC = "browse_pic";
    String BROWSE_SLIDER_PIC = "browse_slider_pic";
    String TRANS_PROPERTY_IMAGE = "property_image";
    String TRANS_MILES = "miles";
    String TRANS_NAME = "name";
    String PIC_TITLE = "PIC_TITLE";
    String TITLE_PROPERTY_IMAGE = "Property Image";
    String TITLE_SERVICE_PROVIDER_IMAGE = "Service Provider Image";
    String TITLE = "title";

    String IS_SLIDER = "is_slider";


    /*filter provider dialog*/
    String FILTER_EXPERTISE = "filter_expertise";
    String FILTER_RATING = "filter_rating";
    String FILTER_DISTANCE = "filter_distance";
    String FILTER_LICENSED = "filter_licensed";
    String CLICK_VIEW_TASK = "view_task";
    String CLICK_EDIT_TASK = "edit_task";
    String CLICK_ADD_TASK = "add_task";
    String CLICK_ADD_CONTACT = "add_contact";
    String CLICK_EDIT_CONTACT = "edit_contact";
    String CLICK_VIEW_CONTACT = "view_contact";
    String TASK_NOTES = "task_notes";
    String TASK_NAME = "task_name";
    String TASK_TIME_FROM = "task_time_from";
    String TASK_TIME_TO = "task_time_to";
    String TASK_REMINDER = "task_reminder";
    String TASK_REPEAT = "task_repeat";
    String TASK_REPEAT_EMPTY = "task_repeat_empty";
    String TASK_ID = "task_id";
    String CONTACT_ID = "contact_id";
    String FEEDBACK = "feedback";
    String REASON = "reason";
    String IS_FEEDBACK = "isFeedback";
    String INSTANCE = "instance";
    String PASSWORD = "password";

    String NEW_PASSWORD = "new_password";
    String TASK_TYPE = "task_type";
    String PHONE = "phone";
    String DESIGNATION = "designation";
    String CATEGORY = "category";
    //String SEND_BY_NAME="send_by_name";
    String SEND_BY_USERID = "send_by";
}
