package com.ohp.utils;

import android.content.Context;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Created by Anuj Sharma on 6/6/2017.
 */

public class CustomSearchView extends android.support.v7.widget.AppCompatEditText {

    public CustomSearchView(Context context) {
        super(context);
    }

    @Override
    public void addTextChangedListener(TextWatcher watcher) {
        super.addTextChangedListener(watcher);
    }
}
