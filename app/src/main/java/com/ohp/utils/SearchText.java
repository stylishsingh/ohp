package com.ohp.utils;

import com.ohp.interfaces.AdapterPresenter;
import com.ohp.ownerviews.beans.EventListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amanpal Singh.
 */

public class SearchText<T> {

    private static SearchText instance;

    private SearchText() {

    }

    public static SearchText getInstance() {
        if (instance == null) return instance = new SearchText();
        else return instance;
    }


    @SuppressWarnings("unchecked")
    public void searchText(AdapterPresenter adapterPresenter, String text, List<T> list) {
        List<T> filterList = new ArrayList<>();

        if (list.get(0) instanceof EventListModel.DataBean) {
            List<EventListModel.DataBean> searchList = (List<EventListModel.DataBean>) list;
            for (EventListModel.DataBean dataObj : searchList) {
                System.out.println(dataObj.getEventDate() + "----is equal---" + text);
                if (dataObj.getEventDate().equals(text)) {
                    System.out.println("true");
                    filterList.add((T) dataObj);
                }
            }
            adapterPresenter.updateList(filterList);
        }
    }
}
