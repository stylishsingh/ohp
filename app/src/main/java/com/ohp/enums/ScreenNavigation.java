package com.ohp.enums;

/**
 * @author Amanpal Singh.
 */

public enum ScreenNavigation {
    EDIT_ITEM,
    ADD_ITEM,
    DETAIL_ITEM,
    ACCEPT,
    REJECT,
    DELETE_ITEM,
    OLD_ITEM,
    DUMMY_ITEM
}
